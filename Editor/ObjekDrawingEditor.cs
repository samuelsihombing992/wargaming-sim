using UnityEngine;
using UnityEditor;
using static Wargaming.Core.GlobalParam.DrawingHelper;

[CustomEditor(typeof(ObjekDrawing))]
public class ObjekDrawingEditor : Editor
{
    #region Variables
    protected ObjekDrawing targetDrawing;

    SerializedProperty _userID;
    SerializedProperty _documentID;
    SerializedProperty _nama;
    SerializedProperty _type;
    SerializedProperty _geometry;
    SerializedProperty _geometryPolygon;
    SerializedProperty _geometryPoint;
    SerializedProperty _properties;
    #endregion

    private void OnEnable()
    {
        targetDrawing = (ObjekDrawing)target;

        _userID = serializedObject.FindProperty("userID");
        _documentID = serializedObject.FindProperty("documentID");
        _nama = serializedObject.FindProperty("nama");
        _type = serializedObject.FindProperty("type");
        _geometry = serializedObject.FindProperty("geometry");
        _geometryPolygon = serializedObject.FindProperty("geometryPolygon");
        _geometryPoint = serializedObject.FindProperty("geometryPoint");
        _properties = serializedObject.FindProperty("properties");
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.PropertyField(_type, new GUIContent("Drawing Type"), false);
        EditorGUILayout.Space(5);
        _userID.longValue = EditorGUILayout.LongField("User ID", _userID.longValue);
        _documentID.longValue = EditorGUILayout.LongField("Document ID", _documentID.longValue);
        _nama.stringValue = EditorGUILayout.TextField("Nama", _nama.stringValue);

        switch (targetDrawing.type)
        {
            case DrawingType.POLYLINE:
                EditorGUILayout.PropertyField(_geometry, new GUIContent("Geometry"), true);
                break;
            case DrawingType.POLYGON:

                break;
            case DrawingType.RECTANGLE:

                break;
            case DrawingType.CIRCLE:

                break;
            case DrawingType.ELLIPSE:

                break;
            case DrawingType.ARROW:

                break;
            case DrawingType.MANOUVER:

                break;
            default: break;
        }

        EditorGUILayout.PropertyField(_properties, new GUIContent("Properties"), true);

        serializedObject.ApplyModifiedProperties();
    }
}
