using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using Colyseus;
using Wargaming.Core.GlobalParam;
using Colyseus.Schema;
using Wargaming.Components.Playback;
using Wargaming.Core;
using Wargaming.Core.GlobalParam.HelperColyseusTFG;
using Wargaming.Core.Network;
using static Wargaming.Components.Colyseus.ColyseusTFGController;

namespace Wargaming.Components.Colyseus
{
    public class ColyseusTFGSingleController : MonoBehaviour
    {
        private ColyseusClient client;
        private ColyseusSingleState state;

        public ColyseusRoom<StateTFGSingle> room;

        [Header("Sync Settings")]
        [SerializeField] public bool useSyncTiming = false;
        [SerializeField] public float syncTime = 5.0f; // Sync Waktu dari Colyseus tiap X-Time

        [NonSerialized] public float syncPeriod = 0.0f;
        [NonSerialized] public bool resyncTime = false;

        // TEMP USER digunakan untuk menampung sementara user yang login di mana CB-nya akan di load secara berurutan (tidak sekaligus)
        public List<long> TEMP_USER = new List<long>();

        #region INSTANCE
        public static ColyseusTFGSingleController instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private void Update()
        {
            if (useSyncTiming)
            {
                if (syncPeriod > syncTime) resyncTime = true;
                syncPeriod += Time.deltaTime;
            }
        }

        /// <summary>
        /// Preparing Client Unity Sebelum Join / Create Room Colyseus
        /// </summary>
        /// <returns></returns>
        public async Task<bool> PrepareClient()
        {
            Destroy(GetComponent<ColyseusTFGController>());

            var clientSettings = ColyseusDataDictionary.setClientUser();

            client = new ColyseusClient(ServerConfig.SERVER_COLYSEUS);
            state = new ColyseusSingleState();

            try
            {
                if (SessionUser.id == null)
                {
                    throw new ArgumentNullException(paramName: nameof(SessionUser.id), message: "User is not found!");
                }
                else
                {
                    if (SessionUser.id == 1 || SessionUser.name == "WASDAL")
                    {
                        // WASDAL Can Create or Join Room
                        room = await client.JoinOrCreate<StateTFGSingle>(StartupConfig.settings.SERVER_TFG_SINGLE, clientSettings);
                    }
                    else
                    {
                        // Kogas Can Only Join Room
                        room = await client.Join<StateTFGSingle>(StartupConfig.settings.SERVER_TFG_SINGLE, clientSettings);
                    }

                    if (room == null) return false;

                    SetupRoomHandlers();
                    SetupRoomMessage();

                    return true;
                }
            }
            catch (CSAMatchMakeException e) { ShowErrorMessage(e.Code, e.Message); return false; }
            catch (Exception e)
            {
                Debug.LogError("Error Occurs When Client Join or Create a Colyseus Room!");
                Debug.LogError(e.Message);

                Debug.Log("room");
                Debug.Log(room);
                room = null;

                return false;
            }
        }

        /// <summary>
        /// Show Error Message Generated By All Colyseus
        /// </summary>
        private void ShowErrorMessage(int? code, string message = null)
        {
            if (code == null) return;

            Debug.Log(code.Value);
            Debug.LogError(message);

            switch (code)
            {
                case 4210:
                    // Error Code 4210 = ROOM NOT FOUND
                    LoadingController.UpdateGameplayLoading(message + " Retry Connection...", 0);
                    break;
                case 4217:
                    // Error Code 4217 = SCHEMA MISMATCH
                    LoadingController.UpdateGameplayLoading("Failed To Load Server. Local Schema Mismatch!", 0);
                    break;
                case 4211:
                    // Error Code 4210 = NO ROOM FOUND
                    LoadingController.UpdateGameplayLoading(message + " Retry Connection...", 0);
                    break;
            }
        }

        private void SetupRoomHandlers()
        {
            SceneController.Instance.DestroyLoading("FadeOut");

            room.OnError += (code, message) => Debug.LogError("ERROR, code =>" + code + ", message => " + message);

            room.State.players.OnAdd += state.OnStatePlayerJoin;
            room.State.players.OnRemove += state.OnStatePlayerLeave;
            room.State.waktuKogas.OnAdd += state.OnStateWaktuKogasSync;
        }

        private void SetupRoomMessage()
        {
            var _message = new ColyseusSingleRoomMessage();
            _message.initMessage();
        }
    }

    #region ROOM STATE
    public class ColyseusSingleState
    {
        // <summary>
        /// Jalankan Sesuatu Jika Terdapat User JOIN
        /// </summary>
        /// <param name="key">ID User</param>
        /// <param name="playerTFG">Data User</param>
        public async void OnStatePlayerJoin(string key, players playerTFG)
        {
            // Hanya ROOM WASDAL yang dapat menampilkan data player yang join.
            if (SessionUser.id != 1) return;

            if (key != "" && key != "1" && key != "server")
            {
                Debug.Log(playerTFG.nama + "(" + key + ")" + " join the room!");

                var playerKey = int.Parse(key);
                /// playerKey 1000 = WASDAL 3D (Dikonversi Ulang KEY Playernya menjadi 0
                if (playerKey == 1000) playerKey -= 1000;


                // Jika Player sudah join, tidak boleh masuk lagi
                if (PlayerData.PLAYERS.FindIndex(x => x.id == playerKey) != -1) return;
                PlayerData.AddPlayer(new PData
                {
                    id = playerKey,
                    name = playerTFG.nama,
                    nama_asisten = playerTFG.asisten,
                    id_bagian = (playerTFG.bagian != null) ? long.Parse(playerTFG.bagian) : null,
                    jenis_user = playerTFG.jenisUser,
                    isLoaded = false
                });

                if (playerTFG.bagian != null)
                {
                    var dataCB = await WargamingAPI.GetCBTerbaik(playerTFG.bagian);

                    // Add CB Terbaik Dipilih ke Data CB Terpilih Player.
                    var indexPlayer = (PlayerData.PLAYERS.FindIndex(x => x.id == playerKey));
                    PlayerData.PLAYERS[indexPlayer].cbAktif = dataCB.data;

                    // Load Entity From CB
                    await EntityController.instance.loadEntityFromCB(long.Parse(key), dataCB.data.id_kogas, SkenarioAktif.ID_SKENARIO, dataCB.data.nama_document);
                }
            }

            //if (SessionUser.id == 1)
            //{
            //    // Hanya Wasdal yang dapat menerima notifikasi user JOIN ke room
            //    Debug.Log(playerTFG.nama + "(" + key + ")" + " join the room");

            //    if (key != "" && key != "1" && key != "server")
            //    {
            //        var keyParse = int.Parse(key);
            //        if (keyParse >= 1000) keyParse -= 1000;

            //        if (PlayerData.PLAYERS.FindIndex(x => x.id == keyParse) != -1) return;

            //        PlayerData.AddPlayer(new PData
            //        {
            //            id = long.Parse(key),
            //            name = playerTFG.nama,
            //            nama_asisten = playerTFG.asisten,
            //            id_bagian = (playerTFG.bagian != null) ? long.Parse(playerTFG.bagian) : null,
            //            jenis_user = playerTFG.jenisUser,
            //            isLoaded = false,
            //        });

            //        if (!ColyseusTFGSingleController.instance.TEMP_USER.Contains(long.Parse(key)))
            //        {
            //            ColyseusTFGSingleController.instance.TEMP_USER.Add(long.Parse(key));
            //        }

            //        // Jika ada user join, hanya KOGAS yang akan di load data CB-nya
            //        // (dalam kondisi jika login sebagai WASDAL)
            //        await WargamingAPI.GetCBTerbaik(playerTFG.bagian);
            //        await EntityController.instance.loadEntityFromCB(long.Parse(key), CBSendiri.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
            //    }
            //}
        }

        /// <summary>
        /// Jalankan Sesuatu Jika Terdapat User LEAVE
        /// </summary>
        /// <param name="key">ID User</param>
        /// <param name="playerTFG">Data User</param>
        public void OnStatePlayerLeave(string key, players playerTFG)
        {
            if (SessionUser.id == 1)
            {
                // Hanya Wasdal yang dapat menerima notifikasi user LEAVE dari room
                Debug.Log(playerTFG.nama + "(" + key + ")" + " leave the room");

                if (key != "1" && key != "server")
                {
                    EntityController.instance.RemoveEntityFromCB(key);

                    // Remove User From List
                    UserController.instance.RemoveUserList(key);

                    PlayerData.RemovePlayer(key);
                }
            }
        }

        public void OnStateWaktuKogasSync(string key, WaktuKogas waktu)
        {
            waktu.OnChange += (List<DataChange> changes) => OnStateWaktuKogasChange(changes, key);
        }

        private void OnStateWaktuKogasChange(List<DataChange> changes, string key)
        {
            var longKey = long.Parse(key);

            for (int i = 0; i < changes.Count; i++)
            {
                switch (changes[i].Field)
                {
                    case "timeString":
                        for (int j = 0; j < PlayerData.PLAYERS.Count; j++)
                        {
                            if (PlayerData.PLAYERS[j].id == longKey)
                            {
                                //var colyseusUserTime = DateTimeOffset.FromUnixTimeMilliseconds(Convert.ToInt64(Math.Floor(Convert.ToDouble(changes[i].Value.ToString())))).LocalDateTime;
                                //if (PlayerData.PLAYERS[j].timeMovement == null)
                                //{
                                //    PlayerData.PLAYERS[j].timeMovement = colyseusUserTime;
                                //}
                                //else
                                //{
                                //    TimeSpan timeDiff = colyseusUserTime - PlayerData.PLAYERS[j].timeMovement.GetValueOrDefault();
                                //    Debug.Log(timeDiff.TotalSeconds);
                                //    if(timeDiff.TotalSeconds >= 60)
                                //    {
                                //        PlayerData.PLAYERS[j].timeMovement = colyseusUserTime;
                                //    }
                                //}

                                PlayerData.PLAYERS[j].timeMovement = DateTimeOffset.FromUnixTimeMilliseconds(Convert.ToInt64(Math.Floor(Convert.ToDouble(changes[i].Value.ToString())))).LocalDateTime;
                                j = PlayerData.PLAYERS.Count;
                            }
                        }
                        break;
                }
            }
        }
    }
    #endregion

    #region ROOM MESSAGE
    public class ColyseusSingleRoomMessage
    {
        protected ColyseusRoom<StateTFGSingle> room;

        public void initMessage()
        {
            room = ColyseusTFGSingleController.instance.room;

            room.OnMessage<FocusToObject>("zoom3DTFG", (media) =>
            {
                Debug.Log("Zoom 3D TFG");

                if (media.id != null)
                {
                    var satuanIndex = EntityController.instance.SATUAN.FindIndex(x => x.nama == media.id);
                    if (satuanIndex != -1)
                    {
                        Debug.Log("Locating From ID " + media.id);
                        WargamingCam.instance.FollowEntityAtIndex(satuanIndex);
                    }
                    else
                    {
                        Debug.Log("ID NULL WHEN zoom3DTFG");
                    }
                }
            });

            room.OnMessage<ZoomToObject>("zoom3D", (media) =>
            {
                if (media.id != null)
                {
                    var satuanIndex = EntityController.instance.SATUAN.FindIndex(x => x.nama == media.id);
                    if (satuanIndex != -1)
                    {
                        Debug.Log("Locating From ID " + media.id);
                        WargamingCam.instance.FollowEntityAtIndex(satuanIndex);
                    }
                    else
                    {
                        Debug.Log("ID NULL WHEN zoom3D");
                    }
                }
            });
        }
    }
    #endregion
}