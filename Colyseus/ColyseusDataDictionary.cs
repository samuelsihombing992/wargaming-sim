using System.Collections.Generic;

using UnityEngine;

using Wargaming.Core.GlobalParam;

public class ColyseusDataDictionary : MonoBehaviour
{
    public static Dictionary<string, object> setClientUser()
    {
        if (SessionUser.id == 1 || SessionUser.name == "WASDAL")
        {
            // STRUKTUR USE SEBAGAI WASDAL
            return new Dictionary<string, object>
            {
                ["nama"] = SessionUser.name.ToString() + " (3D)",
                ["asisten"] = null,
                ["bagian"] = null,
                //["id_user"] = SessionUser.id.ToString(),
                ["id_user"] = "9999",
                ["jenisUser"] = "wasdal",
            };
        }
        else
        {
            // STRUKTUR USER SEBAGAI KOGAS
            return new Dictionary<string, object>
            {
                ["nama"] = SessionUser.name + " (3D)",
                ["asisten"] = SessionUser.nama_asisten,
                ["bagian"] = SessionUser.id_bagian.ToString(),
                //["id_user"] = SessionUser.id.ToString(),
                ["id_user"] = (1000 + SessionUser.id).ToString(),
                ["jenisUser"] = "asisten",
            };
        }
    }
}
