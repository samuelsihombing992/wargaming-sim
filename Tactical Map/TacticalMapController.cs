using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using SickscoreGames.HUDNavigationSystem;

using Wargaming.Core;

namespace Wargaming.TacticalMap.Component.Controller
{
    public class TacticalMapController : MonoBehaviour
    {
        [Header("References")]
        public OnlineMaps tacticalMap;
        public Camera tacticalCam;
        public Toggle toggler;

        [Header("UI Group")]
        public List<GameObject> mainUIGroup;
        public List<GameObject> tacticalUIGroup;
        public MenuTogglerMultistep tacticalIconToggler;

        [Header("Icon Data")]
        public Sprite iconAircraft;
        public Sprite iconShip;
        public Sprite iconVehicle;

        [NonSerialized] public bool isOn = false;

        #region INSTANCE
        public static TacticalMapController instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private void Start()
        {
            if (tacticalMap == null)
            {
                tacticalMap = OnlineMaps.instance;
            }

            DisableTacticalMap();
        }

        public void EnableTacticalMap()
        {
            OnToggleTacticalMap(true);
            //var mainCam = Camera.main.GetComponentInParent<WargamingCam>();

            //mainCam.dragMode = false;
            //mainCam.tiltMode = false;
            //mainCam.scrollMode = false;

            //HUDNavigationSystem.Instance.PlayerCamera = tacticalCam;

            //tacticalMap.GetComponent<OnlineMapsTileSetControl>().allowUserControl = true;
            //tacticalMap.GetComponent<OnlineMapsTileSetControl>().allowZoom = true;

            //tacticalCam.gameObject.SetActive(true);
            //ToggleEntityController.instance.ToggleTacticalMapIcon(true);

            //isOn = true;
        }

        public void DisableTacticalMap()
        {
            OnToggleTacticalMap(false);
        }

        public void ToggleSimbolTaktis(MenuTogglerMultistep step)
        {
            if (step == null) return;
            if (!instance.isOn) return;

            foreach (GameObject data in GameObject.FindGameObjectsWithTag("simbol-taktis-tacmap"))
            {
                var HUDElement = data.GetComponent<HUDNavigationElement>();
                if (HUDElement == null) continue;

                if (HUDElement.Indicator == null) continue;

                switch (step.activeIndex)
                {
                    case 0:
                        HUDElement.showIndicator = true;
                        HUDElement.Indicator.ToggleCustomTransform("label_nama", false);
                        HUDElement.Indicator.ToggleCustomTransform("icon", true);
                        break;
                    case 1:
                        HUDElement.showIndicator = true;
                        HUDElement.Indicator.ToggleCustomTransform("label_nama", true);
                        HUDElement.Indicator.ToggleCustomTransform("icon", true);
                        break;
                    default:
                        HUDElement.showIndicator = false;
                        break;
                }
            }
        }

        private void OnToggleTacticalMap(bool state)
        {
            WargamingCam.instance.dragMode      = !state;
            WargamingCam.instance.tiltMode      = !state;
            WargamingCam.instance.scrollMode    = !state;
            isOn = state;

            if(WargamingCam.instance.camFollower != null)
            {
                WargamingCam.instance.camFollower.GetComponentInChildren<Camera>().enabled = state;
                HUDNavigationSystem.Instance.PlayerCamera = (state) ? tacticalCam : WargamingCam.instance.camFollower.GetComponentInChildren<Camera>();
            }
            else
            {
                HUDNavigationSystem.Instance.PlayerCamera = (state) ? tacticalCam : Camera.main;
            }

            tacticalMap.GetComponent<OnlineMapsTileSetControl>().allowUserControl   = state;
            tacticalMap.GetComponent<OnlineMapsTileSetControl>().allowZoom          = state;

            tacticalCam.gameObject.SetActive(state);
            if (state)
            {
                ToggleSimbolTaktis(tacticalIconToggler);
            }
            else
            {
                foreach (GameObject data in GameObject.FindGameObjectsWithTag("simbol-taktis-tacmap"))
                {
                    var HUDElement = data.GetComponent<HUDNavigationElement>();
                    if (HUDElement == null) continue;

                    HUDElement.showIndicator = false;
                }
            }

            if (mainUIGroup != null)
            {
                for (int i = 0; i < mainUIGroup.Count; i++)
                {
                    mainUIGroup[i].SetActive(!state);
                }
            }

            if (tacticalUIGroup != null)
            {
                for (int i = 0; i < tacticalUIGroup.Count; i++)
                {
                    tacticalUIGroup[i].SetActive(state);
                }
            }
        }
    }
}