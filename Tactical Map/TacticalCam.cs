using UnityEngine;
using UnityEngine.UI;

using Wargaming.TacticalMap.Component.Controller;

namespace Wargaming.TacticalMap.Component.Cam
{
    public class TacticalCam : MonoBehaviour
    {
        [Header("Cam Settings")]
        public float resetCamDuration           = 5;
        public Vector3 resetCamCoordinate       = new Vector3(0, 0, 0);
        public float zoomSpeed                  = 1;
        public AnimationCurve zoomSensitive     = AnimationCurve.EaseInOut(2, 1, 20, 1f);

        private float motionTime;

        [Header("Cam State")]
        public bool dragMode        = true;
        public bool tiltMode        = true;
        public bool scrollMode      = true;

        private AnimationCurve lattitudeMotion = AnimationCurve.EaseInOut(0, 0, 1, 1);
        private AnimationCurve longtitudeMotion = AnimationCurve.EaseInOut(0, 0, 1, 1);
        private AnimationCurve altitudeMotion = AnimationCurve.EaseInOut(0, 0, 1, 1);

        private bool isOnReset      = false;
        private bool isOnZoomIn     = false;
        private bool isOnZoomOut    = false;

        #region INSTANCE
        public static TacticalCam instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private void Start()
        {
            if (TacticalMapController.instance.tacticalMap == null)
            {
                enabled = false;
                return;
            }
        }

        private void Update()
        {
            OnCameraReset();
            OnCameraZoom();
        }

        public void resetCam()
        {
            if (NoCurrentActionFromCam())
            {
                lattitudeMotion = AnimationCurve.EaseInOut(0, OnlineMaps.instance.position.y, resetCamDuration, resetCamCoordinate.x);
                longtitudeMotion = AnimationCurve.EaseInOut(0, OnlineMaps.instance.position.x, resetCamDuration, resetCamCoordinate.y);
                altitudeMotion = AnimationCurve.EaseInOut(0, OnlineMaps.instance.floatZoom, resetCamDuration, resetCamCoordinate.z);

                isOnReset = true;
            }
        }

        private void OnCameraReset()
        {
            if (!isOnReset) return;

            motionTime += Time.fixedDeltaTime * 2;
            if (motionTime >= resetCamDuration)
            {
                motionTime = 0;
                isOnReset = false;
                return;
            }

            // Update the camera
            float lat = lattitudeMotion.Evaluate(motionTime);
            float lng = longtitudeMotion.Evaluate(motionTime);
            float zoom = altitudeMotion.Evaluate(motionTime);

            var maps        = OnlineMaps.instance;
            var camOrbit    = OnlineMapsCameraOrbit.instance;

            var headingChange   = (camOrbit.rotation.x > 180) ? 360 : 0;
            var rollChange      = (camOrbit.rotation.y > 180) ? 360 : 0;

            maps.position = new Vector2(lng, lat);
            maps.floatZoom = zoom;
            camOrbit.rotation = new Vector2(
                Mathf.Lerp(camOrbit.rotation.x, headingChange, (motionTime / resetCamDuration)),
                Mathf.Lerp(camOrbit.rotation.y, rollChange, (motionTime / resetCamDuration))
            );
        }

        private void OnCameraZoom()
        {
            if (!isOnZoomIn && !isOnZoomOut) return;

            float zoomChange = zoomSensitive.Evaluate(OnlineMaps.instance.floatZoom);

            if (isOnZoomIn)
            {
                OnlineMaps.instance.floatZoom += Time.fixedDeltaTime * zoomChange;
            }
            else if (isOnZoomOut)
            {
                OnlineMaps.instance.floatZoom -= Time.fixedDeltaTime * zoomChange;
            }
        }

        #region ZOOM CAM
        public void OnZoomInHold()
        {
            if (NoCurrentActionFromCam())
            {
                isOnZoomIn = true;
            }
        }

        public void OnZoomInRelease()
        {
            isOnZoomIn = false;
        }

        public void OnZoomOutHold()
        {
            if (NoCurrentActionFromCam())
            {
                isOnZoomOut = true;
            }
        }

        public void OnZoomOutRelease()
        {
            isOnZoomOut = false;
        }
        #endregion

        public void DragMode(Toggle toggle)
        {
            OnlineMapsTileSetControl.instance.allowUserControl = toggle.isOn;
        }

        public void PanMode(Toggle toggle)
        {
            OnlineMapsCameraOrbit.instance.lockPan = !toggle.isOn;
        }

        private bool NoCurrentActionFromCam()
        {
            if (isOnReset == false && isOnZoomIn == false && isOnZoomOut == false)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}