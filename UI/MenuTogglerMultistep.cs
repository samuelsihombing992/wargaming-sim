using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(Button))]
public class MenuTogglerMultistep : MonoBehaviour
{
    [Header("Menu Icon")]
    public Image iconComponent;

    public Color activeColor;
    public Color disableColor = Color.white;

    public int activeIndex = 0;
    public List<Sprite> iconSteps;

    [Header("Events")]
    public UnityEvent onEnableEvent;
    public UnityEvent onDisableEvent;
    public List<UnityEvent> onChangeStepEvent;

    private Button btnComponent;

    private void Start()
    {
        if (iconSteps.Count <= 0) enabled = false;
        btnComponent = GetComponent<Button>();

        btnComponent.onClick.AddListener(delegate
        {
            ToggleIcon();
        });
    }

    public void ToggleIcon()
    {
        activeIndex++;

        if(activeIndex >= iconSteps.Count)
        {
            activeIndex = -1;
        }

        if (activeIndex < 0)
        {
            iconComponent.sprite = iconSteps.Last();

            iconComponent.color = disableColor;
            onDisableEvent.Invoke();
        }
        else
        {
            iconComponent.sprite = iconSteps[activeIndex];

            iconComponent.color = activeColor;
            if (activeIndex == 0)
            {
                onEnableEvent.Invoke();
            }

            onChangeStepEvent[activeIndex].Invoke();
        }
    }

    public void ToggleOff()
    {
        activeIndex = 0;
        iconComponent.color = disableColor;

        onDisableEvent.Invoke();
    }
}
