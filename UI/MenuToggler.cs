using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent(typeof(Toggle))]
public class MenuToggler : MonoBehaviour
{
    [Header("Menu Color")]
    public bool iconColor;

    public Image graphic;
    public Color activeMenu;
    public Color disableMenu = Color.white;
    public string iconName = "Icon";

    [Header("Sprite Swap")]
    public bool spriteSwap;
    public Sprite activeSprite;
    public Sprite disableSprite;

    [Header("Menu Icon")]
    public bool iconImage;
    public GameObject activeIcon;
    public GameObject disableIcon;

    [Header("On Toggle Events")]
    public UnityEvent onToggle;
    public UnityEvent onToggleOn;
    public UnityEvent onToggleOff;

    private Toggle toggler;

    private void Start()
    {
        toggler = GetComponent<Toggle>();
        if(toggler == null)
        {
            enabled = false;
            return;
        }

        toggler.onValueChanged.AddListener(delegate { ToggleIcon(); });

        if (toggler.isOn)
        {
            ToggleOnStyle();
        }
        else
        {
            ToggleOffStyle();
        }
    }

    public void ToggleIcon()
    {
        onToggle.Invoke();
        bool state = !toggler.isOn;

        //if(toggler.group != null)
        //{
        //    if (!toggler.group.allowSwitchOff)
        //    {
        //        state = toggler.isOn;
        //    }
        //    else
        //    {
        //        state = toggler.isOn;
        //    }
        //}

        if (state)
        {
            ToggleOff();
        }
        else
        {
            ToggleOn();
        }
    }

    public void ToggleOn()
    {
        onToggleOn.Invoke();

        ToggleOnStyle();
    }

    private void ToggleOnStyle()
    {
        if (iconColor)
        {
            var icon = (graphic != null) ? graphic : transform.Find(iconName).GetComponent<Image>();
            icon.color = activeMenu;
        }

        if (iconImage)
        {
            activeIcon.SetActive(true);
            disableIcon.SetActive(false);
        }

        if (spriteSwap)
        {
            GetComponent<Image>().sprite = activeSprite;
        }
    }

    public void ToggleOff()
    {
        onToggleOff.Invoke();

        ToggleOffStyle();
    }

    private void ToggleOffStyle()
    {
        if (iconColor)
        {
            var icon = (graphic != null) ? graphic : transform.Find(iconName).GetComponent<Image>();
            icon.color = disableMenu;
        }

        if (iconImage)
        {
            disableIcon.SetActive(true);
            activeIcon.SetActive(false);
        }

        if (spriteSwap)
        {
            GetComponent<Image>().sprite = disableSprite;
        }
    }

    public void SetOn()
    {
        toggler.isOn = true;
    }

    public void SetOff()
    {
        toggler.isOn = false;
    }
}
