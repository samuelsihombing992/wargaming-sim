using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;

using UnityEngine;
using UnityEngine.UI;

using Random = UnityEngine.Random;
using Wargaming.Core;
using Wargaming.Core.Network;
using UnityEngine.Networking;

public class LoadingEffect : MonoBehaviour
{
    public bool useLegacyLoading = false;

    [SerializeField] private float zoomIntensity;
    [SerializeField] private RawImage image;
    //[SerializeField] List<Sprite> backgrounds = new List<Sprite>();
    [SerializeField] List<Texture2D> backgrounds = new List<Texture2D>();

    //private string themeDir = @"\Data\Themes\Loading";

    private bool innerZoom = true;
    private int imageIndex;

    private void Awake()
    {
        //if(!useLegacyLoading) LoadImages();
    }

    private void Start()
    {
        zoomIntensity = zoomIntensity / 100;
        imageIndex = 0;

        image.transform.localScale = new Vector3(1, 1, 1);

        updateToNextImage();
    }

    private void Update()
    {
        zoomingEffect();
    }

    private async void updateToNextImage()
    {
        try
        {
            if (imageIndex < backgrounds.Count - 1)
            {
                imageIndex++;
            }
            else
            {
                imageIndex = 0;
            }

            image.texture = backgrounds[Random.Range(0, backgrounds.Count - 1)];

            //image.sprite = backgrounds[Random.Range(0, backgrounds.Count - 1)];

            if (image.transform.localScale.x >= 1.25f)
            {
                innerZoom = false;
            }

            await Task.Delay(5000);
            updateToNextImage();
        }
        catch (Exception e) { }
    }

    private void zoomingEffect()
    {
        if (image.transform.localScale.x <= 1f)
        {
            innerZoom = true;
        }

        if (innerZoom)
        {
            image.transform.localScale = new Vector3(image.transform.localScale.x + (Time.deltaTime * zoomIntensity), image.transform.localScale.y + (Time.deltaTime * zoomIntensity), image.transform.localScale.z + (Time.deltaTime * zoomIntensity));
        }
        else
        {
            image.transform.localScale = new Vector3(image.transform.localScale.x - (Time.deltaTime * zoomIntensity), image.transform.localScale.y - (Time.deltaTime * zoomIntensity), image.transform.localScale.z - (Time.deltaTime * zoomIntensity));
        }
    }

    public async void GetThemeLoading(ThemeData themeData)
    {
        var url = Path.Combine(StartupConfig.GetThemeDirectory(), "Loading");
        IOExtended.LoadOrCreateDirectory(url);

        string[] pngFiles = Directory.GetFiles(Path.Combine(url), "*.png");
        string[] jpgFiles = Directory.GetFiles(Path.Combine(url), "*.jpg");

        if(pngFiles.Length >= 1 || jpgFiles.Length >= 1)
        {
            backgrounds = new List<Texture2D>();

            for (int j = 0; j < pngFiles.Length; j++)
            {
                var texture = await NetworkRequest.GetRemoteTexture(Path.Combine(url, pngFiles[j]));
                if (texture.request != UnityWebRequest.Result.Success)
                {
                    Debug.Log("Failed to Load Theme Loading Data (" + pngFiles[j] + ")");
                    continue;
                }

                backgrounds.Add(texture.data);
            }

            for (int j = 0; j < jpgFiles.Length; j++)
            {
                var texture = await NetworkRequest.GetRemoteTexture(Path.Combine(url, jpgFiles[j]));
                if (texture.request != UnityWebRequest.Result.Success)
                {
                    Debug.Log("Failed to Load Theme Loading Data (" + jpgFiles[j] + ")");
                    continue;
                }

                backgrounds.Add(texture.data);
            }
        }
    }

    //void LoadImages()
    //{
    //    themeDir = Directory.GetCurrentDirectory() + themeDir;
    //    backgrounds = new List<Sprite>();

    //    if (Directory.Exists(themeDir))
    //    {
    //        DirectoryInfo d = new DirectoryInfo(themeDir);
    //        //".jpg,.png,*.jpeg"
    //        foreach (var file in d.GetFiles("*.jpg"))
    //        {
    //            //Debug.Log("LoadImages() " + file);
    //            byte[] bytes = File.ReadAllBytes(file.ToString());
    //            Texture2D texture = new Texture2D(Screen.width, Screen.height);
    //            texture.LoadImage(bytes);
    //            backgrounds.Add(Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0)));
    //        }
    //        foreach (var file in d.GetFiles("*.png"))
    //        {
    //            byte[] bytes = File.ReadAllBytes(file.ToString());
    //            Texture2D texture = new Texture2D(Screen.width, Screen.height);
    //            texture.LoadImage(bytes);
    //            backgrounds.Add(Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0)));
    //        }
    //        foreach (var file in d.GetFiles("*.jpeg"))
    //        {
    //            byte[] bytes = File.ReadAllBytes(file.ToString());
    //            Texture2D texture = new Texture2D(Screen.width, Screen.height);
    //            texture.LoadImage(bytes);
    //            backgrounds.Add(Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0)));
    //        }
    //    }
    //    else
    //    {
    //        File.Create(themeDir);
    //        return;
    //    }

    //    image.sprite = backgrounds[Random.Range(0, backgrounds.Count - 1)];
    //}
}