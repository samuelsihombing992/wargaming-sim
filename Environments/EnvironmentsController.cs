using UnityEngine;
using UnityEngine.Rendering;

using Expanse;
using Crest;
using Esri.HPFramework;
using Wargaming.Core.GlobalParam;
using Esri.ArcGISMapsSDK.Components;
using Esri.GameEngine.Map;
using Esri.GameEngine.Elevation.Base;

namespace Wargaming.Core
{
    public class EnvironmentsController : MonoBehaviour
    {
        [Header("Global Environments")]
        [SerializeField] private ArcGISMapComponent mapComponent;
        [SerializeField] private OceanRenderer ocean;
        [SerializeField] private GameObject cloudsGroup;
        [SerializeField] private Volume skyVolume;

        [Header("Datapacks")]
        public GameObject datapackGroup;

        private static ArcGISMap _map;
        private static ArcGISMapComponent _mapComponent;
        private static OceanRenderer _ocean;
        private static GameObject _cloudsGroup;
        private static Volume _skyVolume;

        #region AWAKE PREPARE
        void Awake()
        {
            _mapComponent   = mapComponent;
            _map = new ArcGISMap(_mapComponent.MapType);
            _map.Basemap = new ArcGISBasemap("https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer", Esri.GameEngine.Layers.Base.ArcGISLayerType.ArcGISImageLayer, "");
            _map.Elevation = new ArcGISMapElevation(new ArcGISElevationSource(_mapComponent.Elevation, ArcGISElevationSourceType.ArcGISImageElevationSource, ""));

            _ocean = ocean;
            _cloudsGroup = cloudsGroup;
            _skyVolume = skyVolume;
            _skyVolume = skyVolume;

            _mapComponent.View.Map = _map;
        }

        public static ArcGISMap GetMap() { return _mapComponent.View.Map; }
        public static ArcGISMapComponent GetMapComponent() { return _mapComponent; }
        public static HPRoot GetMapComponentRoot() { return _mapComponent.GetComponent<HPRoot>(); }
        public static OceanRenderer GetOcean() { return _ocean; }
        public static GameObject GetCloudsGroup() { return _cloudsGroup; }
        public static Volume GetSkyVolume() { return _skyVolume; }
        #endregion

        public static void SetSeaLevel(double level)
        {
            if (_ocean == null) return;

            var oceanTransform = _ocean.GetComponent<HPTransform>();
            oceanTransform.SetHPTransformPosition(null, level, null);
        }

        public static void SetCloudsAltitude(float alt)
        {
            _cloudsGroup.transform.position.SetX(alt);
        }
    }
}