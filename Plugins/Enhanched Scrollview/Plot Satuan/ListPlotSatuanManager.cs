using UnityEngine;
using UnityEngine.UI;

using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using Wargaming.Core.Network.Plotting;
using Wargaming.Core.GlobalParam.HelperDataAlutsista;
using Wargaming.Core.GlobalParam;
using WGSBundling.Core;
using Wargaming.Core.GlobalParam.HelperPlotting;

using TMPro;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Collections.Generic;

public class ListPlotSatuanManager : MonoBehaviour, IEnhancedScrollerDelegate
{

    [Header("References")]
    public EnhancedScroller scroller;
    public EnhancedScrollerCellView cellViewPrefab;
    public int numberOfCellsPerRow = 5;

    [Header("Loading Overlay")]
    public GameObject loadingOverlay;

    [Header("Search & Filter")]
    public TMP_InputField search;
    public Toggle filterDarat;
    public Toggle filterLaut;
    public Toggle filterUdara;

    private SmallList<ListPlotSatuanData> data;
    private SmallList<ListPlotSatuanData> _dataDarat;
    private SmallList<ListPlotSatuanData> _dataLaut;
    private SmallList<ListPlotSatuanData> _dataUdara;

    private bool daratAlreadyLoaded = false;
    private bool lautAlreadyLoaded = false;
    private bool udaraAlreadyLoaded = false;

    void Start()
    {
        // tell the scroller that this script will be its delegate
        if (data == null) data = new SmallList<ListPlotSatuanData>();
    }

    public async void OpenList()
    {
        if (GetComponent<CanvasGroup>().alpha == 1)
        {
            GetComponent<Animator>().Play("FadeOut");
        }
        else
        {
            GetComponent<Animator>().Play("FadeIn");

            // Request API untuk mengambil seluruh satuan ketika list satuan pertama kali dibuka
            if (filterDarat.isOn && !daratAlreadyLoaded)
            {
                await RequestDataDarat();
            }
            else if (filterLaut.isOn && !lautAlreadyLoaded)
            {
                await RequestDataLaut();
            }
            else if (filterUdara.isOn && !udaraAlreadyLoaded)
            {
                await RequestDataUdara();
            }

            scroller.Delegate = this;
            scroller.ReloadData();

            Debug.Log("Reload Data");
        }
    }

    private async Task RequestDataDarat()
    {
        loadingOverlay.SetActive(true);

        // -- Request Satuan Darat --
        var dataVehicle = await WargamingAPI.GetListSatuan("vehicle");
        if (dataVehicle != null) GetListSatuanDarat(dataVehicle);

        data = _dataDarat;
        daratAlreadyLoaded = true;

        loadingOverlay.SetActive(false);
    }

    private async Task RequestDataLaut()
    {
        loadingOverlay.SetActive(true);

        // -- Request Satuan Laut --
        var dataShip = await WargamingAPI.GetListSatuan("ship");
        if (dataShip != null) GetListSatuanLaut(dataShip);

        data = _dataLaut;
        lautAlreadyLoaded = true;

        loadingOverlay.SetActive(false);
    }

    private async Task RequestDataUdara()
    {
        loadingOverlay.SetActive(true);

        // -- Request Satuan Udara --
        var dataAircraft = await WargamingAPI.GetListSatuan("aircraft");
        if (dataAircraft != null) GetListSatuanUdara(dataAircraft);

        data = _dataUdara;
        udaraAlreadyLoaded = true;

        loadingOverlay.SetActive(false);
    }

    private ObjectLaut[] GetListSatuanLaut(string data)
    {
        var convertedData = ObjectLaut.FromJson(data);
        Debug.Log("List Asset Laut Berhasil di Parsing Dengan Jumlah Data Sebanyak: " + convertedData.Length);

        for(int i=0; i < convertedData.Length; i++)
        {
            AddNewData(convertedData[i]);
        }

        return convertedData;
    }

    private ObjectDarat[] GetListSatuanDarat(string data)
    {
        var convertedData = ObjectDarat.FromJson(data);
        Debug.Log("List Asset Darat Berhasil di Parsing Dengan Jumlah Data Sebanyak: " + convertedData.Length);

        for (int i = 0; i < convertedData.Length; i++)
        {
            AddNewData(convertedData[i]);
        }

        return convertedData;
    }

    private ObjectUdara[] GetListSatuanUdara(string data)
    {
        var convertedData = ObjectUdara.FromJson(data);
        Debug.Log("List Asset Udara Berhasil di Parsing Dengan Jumlah Data Sebanyak: " + convertedData.Length);

        for (int i = 0; i < convertedData.Length; i++)
        {
            AddNewData(convertedData[i]);
        }

        return convertedData;
    }

    public void AddNewData(ObjectLaut objLaut)
    {
        if(_dataLaut == null) _dataLaut = new SmallList<ListPlotSatuanData>();

        if (objLaut.id == null || objLaut.name == null || objLaut.name == "") return;

        long fontIndex = -1;
        long.TryParse(objLaut.fontIndex, out fontIndex);

        ObjekSatuanStyle style = new ObjekSatuanStyle()
        {
            fontTaktis      = objLaut.fontName,
            fontIndex       = NumberConvertionHelper.ConvertToLong(objLaut.fontIndex),
            keterangan      = objLaut.keterangan,
            grup            = NumberConvertionHelper.ConvertToLong(objLaut.grup),
            entityName      = objLaut.entity_name,
            entityKategori  = objLaut.entity_kategori,
            jenisRole       = objLaut.jenis_role,
            labelStyle      = objLaut.style_symbol
        };

        ObjekSatuanPersonil personil = new ObjekSatuanPersonil()
        {
            bintara = objLaut.jumlah_bintara.GetValueOrDefault(0),
            tamtama = objLaut.jumlah_tamtama.GetValueOrDefault(0),
            perwira = objLaut.jumlah_perwira.GetValueOrDefault(0)
        };

        ObjekSatuanWeapon[] weapon = new ObjekSatuanWeapon[1];
        weapon[0] = new ObjekSatuanWeapon
        {
            index = 0.ToString(),
            id = 100.ToString(),
            jenis = "missile"
        };

        ObjekSatuanInfo info = new ObjekSatuanInfo()
        {
            namaSatuan = objLaut.name,
            bahanBakar = objLaut.fuelLoad.GetValueOrDefault(0),
            bahanBakarLoad = 0, // STILL UNKNOWN DATA INI DIAMBIL DARIMANA
            kecepatan = NumberConvertionHelper.ConvertToDouble(objLaut.maxSpeed),
            kecepatanMax = NumberConvertionHelper.ConvertToDouble(objLaut.maxSpeed),
            jenisKecepatan = SpeedHelper.speedType.kmph,
            heading = 0,
            size = 30,
            //list_weapon = weapon,
            waypoint = new System.Collections.Generic.List<object>(),
            list_embarkasi = new System.Collections.Generic.List<object>(),
            personil = personil.bintara + personil.tamtama + personil.perwira,
            detailPersonil = personil,
            useInTfg = true,
            hideInWgs = false
        };

        ObjekSymbol symbol = new ObjekSymbol()
        {
            width = 30,
            fontFamily = objLaut.fontName,
            fontIndex = ((char)style.fontIndex).ToString()
        };

        WGSBundleCore obj3D = AssetPackageController.Instance.FindAsset(objLaut.model3D, EntityHelper.KategoriSatuan.SHIP);



        _dataLaut.Add(new ListPlotSatuanData()
        {
            objId   = objLaut.id.GetValueOrDefault(0),
            objName = objLaut.name,
            obj3D   = obj3D,

            data    = new ObjekSatuan
            {
                symbolID = objLaut.id.GetValueOrDefault(0),
                style = style,
                info = info,
                symbol = symbol,
                kategori = EntityHelper.KategoriSatuan.SHIP
            },
            selectedChanged = null,
        });
    }

    public void AddNewData(ObjectDarat objDarat)
    {
        if (_dataDarat == null) _dataDarat = new SmallList<ListPlotSatuanData>();

        long fontIndex = -1;
        long.TryParse(objDarat.fontIndex, out fontIndex);

        if (objDarat.id == null || objDarat.name == null || objDarat.name == "") return;

        ObjekSatuanStyle style = new ObjekSatuanStyle()
        {
            fontTaktis      = objDarat.fontName,
            fontIndex       = NumberConvertionHelper.ConvertToLong(objDarat.fontIndex),
            keterangan      = objDarat.keterangan,
            grup            = NumberConvertionHelper.ConvertToLong(objDarat.grup),
            entityName      = objDarat.entity_name,
            entityKategori  = objDarat.entity_kategori,
            jenisRole       = objDarat.jenis_role,
            labelStyle      = objDarat.style_symbol
        };

        ObjekSatuanPersonil personil = new ObjekSatuanPersonil()
        {
            bintara = objDarat.jumlah_bintara.GetValueOrDefault(0),
            tamtama = objDarat.jumlah_tamtama.GetValueOrDefault(0),
            perwira = objDarat.jumlah_perwira.GetValueOrDefault(0)
        };

        ObjekSatuanInfo info = new ObjekSatuanInfo()
        {
            namaSatuan          = objDarat.name,
            bahanBakar          = objDarat.fuelLoad,
            bahanBakarLoad      = 0, // STILL UNKNOWN DATA INI DIAMBIL DARIMANA
            kecepatan           = NumberConvertionHelper.ConvertToDouble(objDarat.maxSpeed),
            kecepatanMax        = NumberConvertionHelper.ConvertToDouble(objDarat.maxSpeed),
            jenisKecepatan      = SpeedHelper.speedType.kmph,
            heading             = 0,
            size                = 30,
            personil            = personil.bintara + personil.tamtama + personil.perwira,
            detailPersonil      = personil,
            useInTfg            = true,
            hideInWgs           = false
        };

        ObjekSymbol symbol = new ObjekSymbol()
        {
            width       = 30,
            fontFamily  = objDarat.fontName,
            fontIndex   = ((char)style.fontIndex).ToString()
        };

        WGSBundleCore obj3D = AssetPackageController.Instance.FindAsset(objDarat.model3D, EntityHelper.KategoriSatuan.VEHICLE);

        _dataDarat.Add(new ListPlotSatuanData()
        {
            objId       = objDarat.id.GetValueOrDefault(0),
            objName     = objDarat.name,
            obj3D       = obj3D,

            data = new ObjekSatuan
            {
                symbolID    = objDarat.id.GetValueOrDefault(0),
                style       = style,
                info        = info,
                symbol      = symbol,
                kategori = EntityHelper.KategoriSatuan.VEHICLE
            },
            selectedChanged = null,
        });
    }

    public void AddNewData(ObjectUdara objUdara)
    {
        if (_dataUdara == null) _dataUdara = new SmallList<ListPlotSatuanData>();

        if (objUdara.id == null || objUdara.name == null || objUdara.name == "") return;

        long fontIndex = -1;
        long.TryParse(objUdara.fontIndex, out fontIndex);

        ObjekSatuanStyle style = new ObjekSatuanStyle()
        {
            fontTaktis      = objUdara.fontName,
            fontIndex       = NumberConvertionHelper.ConvertToLong(objUdara.fontIndex),
            keterangan      = objUdara.keterangan,
            grup            = NumberConvertionHelper.ConvertToLong(objUdara.grup),
            entityName      = objUdara.entity_name,
            entityKategori  = objUdara.entity_kategori,
            jenisRole       = objUdara.jenis_role,
            labelStyle      = objUdara.style_symbol
        };

        ObjekSatuanPersonil personil = new ObjekSatuanPersonil()
        {
            bintara = objUdara.jumlah_bintara.GetValueOrDefault(0),
            tamtama = objUdara.jumlah_tamtama.GetValueOrDefault(0),
            perwira = objUdara.jumlah_perwira.GetValueOrDefault(0)
        };

        ObjekSatuanInfo info = new ObjekSatuanInfo()
        {
            namaSatuan          = objUdara.name,
            bahanBakar          = objUdara.fuelCapacity,
            bahanBakarLoad      = 0, // STILL UNKNOWN DATA INI DIAMBIL DARIMANA
            kecepatan           = NumberConvertionHelper.ConvertToDouble(objUdara.maxSpeed),
            kecepatanMax        = NumberConvertionHelper.ConvertToDouble(objUdara.maxSpeed),
            jenisKecepatan      = SpeedHelper.speedType.kmph,
            heading             = 0,
            size                = 30,
            personil            = personil.bintara + personil.tamtama + personil.perwira,
            detailPersonil      = personil,
            useInTfg            = true,
            hideInWgs           = false
        };

        ObjekSymbol symbol = new ObjekSymbol()
        {
            width       = 30,
            fontFamily  = objUdara.fontName,
            fontIndex   = ((char)style.fontIndex).ToString()
        };

        WGSBundleCore obj3D = AssetPackageController.Instance.FindAsset(objUdara.model3D, EntityHelper.KategoriSatuan.AIRCRAFT);

        _dataUdara.Add(new ListPlotSatuanData()
        {
            objId   = objUdara.id.GetValueOrDefault(0),
            objName = objUdara.name,
            obj3D   = obj3D,

            data = new ObjekSatuan
            {
                symbolID    = objUdara.id.GetValueOrDefault(0),
                style       = style,
                info        = info,
                symbol      = symbol,
                kategori    = EntityHelper.KategoriSatuan.AIRCRAFT
            },
            selectedChanged = null,
        });
    }

    private void CellViewSelected(RowPlotSatuanCellView cellView)
    {
        if (cellView == null)
        {
            // nothing was selected
        }
        else
        {
            // get the selected data index of the cell view
            var selectedDataIndex = cellView.DataIndex;

            // loop through each item in the data list and turn
            // on or off the selection state. This is done so that
            // any previous selection states are removed and new
            // ones are added.
            for (var i = 0; i < data.Count; i++)
            {
                data[i].Selected = (selectedDataIndex == i);
            }
        }
    }

    #region Search & Filter Handler
    public void OnSearch()
    {
        data = new SmallList<ListPlotSatuanData>();

        if (filterDarat.isOn)
        {
            if (_dataDarat == null) return;
            for(int i=0; i < _dataDarat.Count; i++)
            {
                Regex r = new Regex(search.text.ToLower());

                bool isActive = false;
                if (search.text == "" || search.text == null)
                {
                    isActive = true;
                }
                else if (r.IsMatch(_dataDarat[i].objName.ToLower()))
                {
                    isActive = true;
                }

                if (isActive) data.Add(_dataDarat[i]);
            }
        }else if (filterLaut.isOn)
        {
            if (_dataLaut == null) return;
            for (int i = 0; i < _dataLaut.Count; i++)
            {
                Regex r = new Regex(search.text.ToLower());

                bool isActive = false;
                if (search.text == "" || search.text == null)
                {
                    isActive = true;
                }
                else if (r.IsMatch(_dataLaut[i].objName.ToLower()))
                {
                    isActive = true;
                }

                if (isActive) data.Add(_dataLaut[i]);
            }
        }
        else if(filterUdara.isOn)
        {
            if (_dataUdara == null) return;
            for (int i = 0; i < _dataUdara.Count; i++)
            {
                Regex r = new Regex(search.text.ToLower());

                bool isActive = false;
                if (search.text == "" || search.text == null)
                {
                    isActive = true;
                }
                else if (r.IsMatch(_dataUdara[i].objName.ToLower()))
                {
                    isActive = true;
                }

                if (isActive) data.Add(_dataUdara[i]);
            }
        }

        scroller.ReloadData();
    }

    public async void OnFilter(string filter)
    {
        data = new SmallList<ListPlotSatuanData>();
        search.text = "";

        switch (filter)
        {
            case "darat":
                if (_dataDarat == null) await RequestDataDarat();
                data = _dataDarat;
                break;
            case "laut":
                if (_dataLaut == null) await RequestDataLaut();
                data = _dataLaut;
                break;
            case "udara":
                if (_dataUdara == null) await RequestDataUdara();
                data = _dataUdara;
                break;
            default:
                if (_dataDarat == null) await RequestDataDarat();
                data = _dataDarat;
                break;
        }

        scroller.ReloadData();
    }
    #endregion

    #region EnhancedScroller Handlers
    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return Mathf.CeilToInt((float) data.Count / (float)numberOfCellsPerRow);
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 250f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        // first, we get a cell from the scroller by passing a prefab.
        // if the scroller finds one it can recycle it will do so, otherwise
        // it will create a new cell.
        PlotSatuanCellView cellView = scroller.GetCellView(cellViewPrefab) as PlotSatuanCellView;

        // data index of the first sub cell
        var di = dataIndex * numberOfCellsPerRow;

        cellView.name = "Cell " + (di).ToString() + " to " + ((di) + numberOfCellsPerRow - 1).ToString();

        // pass in a reference to our data set with the offset for this cell
        cellView.SetData(ref data, di, CellViewSelected);

        // return the cell to the scroller
        return cellView;
    }
    #endregion
}
