using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wargaming.Core.GlobalParam.HelperDataPlotting;

using WGSBundling.Core;

public delegate void SelectedChangedDelegate(bool val);

public class ListPlotSatuanData
{
    //public PDataSatuan data;

    // DATA
    public ObjekSatuan data;

    // DATA Object 3D
    public int objId;
    public string objName;
    public WGSBundleCore obj3D;

    //public int id;
    //public string namaSatuan;

    //public WGSBundleCore obj3D;

    public SelectedChangedDelegate selectedChanged;

    /// <summary>
    /// The selection state
    /// </summary>
    private bool _selected;
    public bool Selected
    {
        get { return _selected; }
        set
        {
            // if the value has changed
            if (_selected != value)
            {
                // update the state and call the selection handler if it exists
                _selected = value;
                if (selectedChanged != null) selectedChanged(_selected);
            }
        }
    }
}
