using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using EnhancedUI.EnhancedScroller;
using System.Threading.Tasks;
using UnityEngine.UI;

[RequireComponent(typeof(EnhancedScroller))]
public class EnhanchedScrollerController : MonoBehaviour, IEnhancedScrollerDelegate
{
    private List<ScrollerData> _data;
    public EnhancedScroller myScroller;
    public GameObject layoutGroup;
    public AnimalCellView animalCellViewPrefab;

    private GameObject scrollerContainer;

    async void Start()
    {
        return;
        _data = new List<ScrollerData>();
        _data.Add(new ScrollerData() { animalName = "Lion" });
        _data.Add(new ScrollerData() { animalName = "Bear" });
        _data.Add(new ScrollerData() { animalName = "Eagle" });
        _data.Add(new ScrollerData() { animalName = "Dolphin" });
        _data.Add(new ScrollerData() { animalName = "Ant" });
        _data.Add(new ScrollerData() { animalName = "Cat" });
        _data.Add(new ScrollerData() { animalName = "Sparrow" });
        _data.Add(new ScrollerData() { animalName = "Dog" });
        _data.Add(new ScrollerData() { animalName = "Spider" });
        _data.Add(new ScrollerData() { animalName = "Elephant" });
        _data.Add(new ScrollerData() { animalName = "Falcon" });
        _data.Add(new ScrollerData() { animalName = "Mouse" });
        myScroller.Delegate = this;
        myScroller.ReloadData();

        await Task.Delay(100);
        var container = transform.Find("Container");
        if (container != null)
        {
            scrollerContainer = container.gameObject;
            scrollerContainer.transform.SetParent(GetComponent<ScrollRect>().viewport);

            var containerLayoutGroup = scrollerContainer.GetComponent<VerticalLayoutGroup>();
            var refLayoutGroup = layoutGroup.GetComponent<VerticalLayoutGroup>();

            containerLayoutGroup.padding = refLayoutGroup.padding;
            containerLayoutGroup.spacing = refLayoutGroup.spacing;
            containerLayoutGroup.childAlignment = refLayoutGroup.childAlignment;
            containerLayoutGroup.reverseArrangement = refLayoutGroup.reverseArrangement;
            containerLayoutGroup.childControlHeight = refLayoutGroup.childControlHeight;
            containerLayoutGroup.childControlWidth = refLayoutGroup.childControlWidth;
            containerLayoutGroup.childScaleHeight = refLayoutGroup.childScaleHeight;
            containerLayoutGroup.childScaleWidth = refLayoutGroup.childScaleWidth;
            containerLayoutGroup.childForceExpandHeight = refLayoutGroup.childForceExpandHeight;
            containerLayoutGroup.childForceExpandWidth = refLayoutGroup.childForceExpandWidth;

            var containerSizeFitter = containerLayoutGroup.gameObject.AddComponent<ContentSizeFitter>();
            var refSizeFitter = layoutGroup.GetComponent<ContentSizeFitter>();

            containerSizeFitter.horizontalFit = refSizeFitter.horizontalFit;
            containerSizeFitter.verticalFit = refSizeFitter.verticalFit;

            Destroy(layoutGroup);
        }
    }
    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return _data.Count;
    }
    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return 100f;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        AnimalCellView cellView = scroller.GetCellView(animalCellViewPrefab) as AnimalCellView;
        cellView.SetData(_data[dataIndex]);

        return cellView;
    }
}
