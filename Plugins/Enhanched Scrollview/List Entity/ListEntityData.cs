using UnityEngine;

using static Wargaming.Core.GlobalParam.EntityHelper;
using TMPro;

[System.Serializable]
public class ListEntityData
{
    public float cellSize;

    public Sprite image;
    public GameObject entity;

    public KategoriSatuan kategori;

    public string id;
    public long userID;
    public string nama;
    //public string keterangan;

    public TMP_FontAsset fontTaktis;
    public string simbolTaktis;
}