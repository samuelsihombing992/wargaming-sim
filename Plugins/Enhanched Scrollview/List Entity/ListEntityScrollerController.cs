using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

using EnhancedUI.EnhancedScroller;

[RequireComponent(typeof(EnhancedScroller))]
public class ListEntityScrollerController : MonoBehaviour, IEnhancedScrollerDelegate
{
    public List<ListEntityData> data;
    public List<ListEntityData> tempData;
    public ListEntityCellView cellViewPrefab;

    //private GameObject scrollerContainer;
    private bool _calculateLayout;

    private void Start()
    {
        data = new List<ListEntityData>();
        tempData = new List<ListEntityData>();
    }

    public void init()
    {
        GetComponent<EnhancedScroller>().Delegate = this;
        GetComponent<EnhancedScroller>().ReloadData();

        refreshContainer();
        ResizeScroller();

        Debug.Log(data.Count + " | " + tempData.Count);
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return data.Count;
    }
    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        return data[dataIndex].cellSize;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        ListEntityCellView cellView = scroller.GetCellView(cellViewPrefab) as ListEntityCellView;
        //switch (data[dataIndex].jenis)
        //{
        //    case DataSatuan.JenisSatuan.VEHICLE:
        //        if (!ListEntityController.instance.vehicleToggle.isOn) return null;
        //        break;
        //    case DataSatuan.JenisSatuan.SHIP:
        //        if (!ListEntityController.instance.shipToggle.isOn) return null;
        //        break;
        //    case DataSatuan.JenisSatuan.AIRCRAFT:
        //        if (!ListEntityController.instance.aircraftToggle.isOn) return null;
        //        break;
        //    default: break;
        //}

        cellView.SetData(data[dataIndex], _calculateLayout);
        return cellView;
    }

    public void refreshContainer()
    {
        var container = transform.Find("Container");
        if (container != null)
        {
            container.SetParent(GetComponent<ScrollRect>().viewport);
            container.GetComponent<RectTransform>().offsetMin = new Vector2(0, container.GetComponent<RectTransform>().offsetMin.y);
            container.GetComponent<RectTransform>().offsetMax = new Vector2(0, container.GetComponent<RectTransform>().offsetMax.y);
        }
    }

    public void ResizeScroller()
    {
        // capture the scroller dimensions so that we can reset them when we are done
        var rectTransform = GetComponent<RectTransform>();
        var size = rectTransform.sizeDelta;

        // set the dimensions to the largest size possible to acommodate all the cells
        //rectTransform.sizeDelta = new Vector2(size.x, float.MaxValue);

        // First Pass: reload the scroller so that it can populate the text UI elements in the cell view.
        // The content size fitter will determine how big the cells need to be on subsequent passes.
        _calculateLayout = true;
        GetComponent<EnhancedScroller>().ReloadData();

        // reset the scroller size back to what it was originally
        rectTransform.sizeDelta = size;

        // Second Pass: reload the data once more with the newly set cell view sizes and scroller content size
        _calculateLayout = false;
        GetComponent<EnhancedScroller>().ReloadData();

        Debug.Log("Data Length " + data.Count);
    }
}