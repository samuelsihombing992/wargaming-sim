using UnityEngine;
using UnityEngine.UI;

using TMPro;
using EnhancedUI.EnhancedScroller;

using Wargaming.Core;

public class ListEntityCellView : EnhancedScrollerCellView
{
    public RectTransform content;
    /// <summary>
    /// The space around the text label so that we
    /// aren't up against the edges of the cell
    /// </summary>
    public RectOffset textBuffer;

    public Image image;

    public TextMeshProUGUI labelNama;
    //public TextMeshProUGUI labelDesc;
    public TextMeshProUGUI fontTaktis;

    public Button buttonFollow;
    public Button buttonLocate;

    public void SetData(ListEntityData data, bool calculateLayout)
    {
        if (data == null) return;

        image.sprite = data.image;

        labelNama.text = (data.nama != null && data.nama != "") ? data.nama : " ";
        //labelDesc.text = (data.keterangan != null && data.keterangan != "") ? data.keterangan : " ";

        fontTaktis.font = data.fontTaktis;
        fontTaktis.text = data.simbolTaktis;

        Canvas.ForceUpdateCanvases();

        data.cellSize = content.rect.height + textBuffer.top + textBuffer.bottom;   

        if (calculateLayout)
        {
            // force update the canvas so that it can calculate the size needed for the text immediately
            
            //Debug.Log(data.cellSize);

            // set the data's cell size and add in some padding so the the text isn't up against the border of the cell
            //data.cellSize = labelNama.GetComponent<RectTransform>().rect.height + labelDesc.GetComponent<RectTransform>().rect.height + textBuffer.top + textBuffer.bottom;
            //data.cellSize = labelNama.GetComponent<RectTransform>().rect.height + labelDesc.GetComponent<RectTransform>().rect.height + 12 + 12;
        }

        GetComponent<Button>().onClick.RemoveAllListeners();
        GetComponent<Button>().onClick.AddListener(delegate { DetailEntityController.instance.initObjSatuan(data.entity.GetComponent<ObjekSatuan>()); });

        if(buttonFollow != null)
        {
            buttonFollow.onClick.RemoveAllListeners();
            buttonFollow.onClick.AddListener(delegate { WargamingCam.instance.FollowEntityByObject(data.entity); }); 
        }

        if(buttonLocate != null)
        {
            buttonLocate.onClick.RemoveAllListeners();
            buttonLocate.onClick.AddListener(delegate { WargamingCam.instance.LocateObject(data.entity); });
        }
    }
}