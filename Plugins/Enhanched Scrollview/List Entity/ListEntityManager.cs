using UnityEngine;
using UnityEngine.UI;

using WGSBundling.Core;
using static Wargaming.Core.GlobalParam.EntityHelper;

using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using TMPro;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Wargaming.Core.GlobalParam;
using System;

public class ListEntityManager : MonoBehaviour, IEnhancedScrollerDelegate
{
    [Header("References")]
    public EnhancedScroller scroller;
    public EnhancedScrollerCellView cellViewPrefab;

    [Header("Search & Filter")]
    public TMP_InputField search;
    public TMP_Dropdown filterKogas;
    public Toggle vehicleToggle;
    public Toggle shipToggle;
    public Toggle submarineToggle;
    public Toggle aircraftToggle;
    public Toggle helicopterToggle;
    public Toggle infantryToggle;
    public Button toggleAll;

    [System.NonSerialized] public SmallList<ListEntityData> data;
    [SerializeField] private SmallList<ListEntityData> _dataVehicle;
    [SerializeField] private SmallList<ListEntityData> _dataShip;
    [SerializeField] private SmallList<ListEntityData> _dataSubmarine;
    [SerializeField] private SmallList<ListEntityData> _dataAircraft;
    [SerializeField] private SmallList<ListEntityData> _dataHelicopter;
    [SerializeField] private SmallList<ListEntityData> _dataInfantry;
    [SerializeField] private SmallList<ListEntityData> _dataRadar;

    /// <summary>
    /// This member tells the scroller that we need
    /// the cell views to figure out how much space to use.
    /// This is only set to true on the first pass to reduce
    /// processing required.
    /// </summary>
    private bool _calculateLayout;

    #region INSTANCE
    public static ListEntityManager instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        // tell the scroller that this script will be its delegate
        if (data == null) data = new SmallList<ListEntityData>();

        if (_dataVehicle == null) _dataVehicle = new SmallList<ListEntityData>();
        if (_dataShip == null) _dataShip = new SmallList<ListEntityData>();
        if (_dataSubmarine == null) _dataSubmarine = new SmallList<ListEntityData>();
        if (_dataAircraft == null) _dataAircraft = new SmallList<ListEntityData>();
        if (_dataHelicopter == null) _dataHelicopter = new SmallList<ListEntityData>();
        if (_dataInfantry == null) _dataInfantry = new SmallList<ListEntityData>();

        if (_dataRadar == null) _dataRadar = new SmallList<ListEntityData>();

        scroller.Delegate = this;
    }

    public void AddData(ObjekSatuan objData)
    {
        var bundleCore = objData.GetComponent<WGSBundleCore>();

        var newList = new ListEntityData()
        {
            entity          = objData.gameObject,
            fontTaktis      = getFontTaktis(objData.symbol.fontFamily),
            simbolTaktis    = objData.symbol.fontIndex,
            image           = (bundleCore != null) ? bundleCore.icon : null,
            id              = objData.nama,
            userID          = objData.userID,
            nama            = objData.info.namaSatuan,
            //keterangan      = objData.info.keterangan,
            kategori        = objData.kategori
        };

        if (!data.Contains(newList)) data.Add(newList);

        switch (objData.kategori)
        {
            case KategoriSatuan.VEHICLE:
                if (!_dataVehicle.Contains(newList)) _dataVehicle.Add(newList);
                break;
            case KategoriSatuan.SHIP:
                if (!_dataShip.Contains(newList)) _dataShip.Add(newList);
                break;
            case KategoriSatuan.SUBMARINE:
                if (!_dataSubmarine.Contains(newList)) _dataSubmarine.Add(newList);
                break;
            case KategoriSatuan.AIRCRAFT:
                if (!_dataAircraft.Contains(newList)) _dataAircraft.Add(newList);
                break;
            case KategoriSatuan.HELICOPTER:
                if (!_dataHelicopter.Contains(newList)) _dataHelicopter.Add(newList);
                break;
            case KategoriSatuan.INFANTRY:
                if (!_dataInfantry.Contains(newList)) _dataInfantry.Add(newList);
                break;
            default:
                if (!_dataVehicle.Contains(newList)) _dataVehicle.Add(newList);
                break;
        }
    }

    public void RemoveData(List<string> listID)
    {
        removeDataIfFound(data, listID);

        removeDataIfFound(_dataVehicle, listID);
        removeDataIfFound(_dataShip, listID);
        removeDataIfFound(_dataSubmarine, listID);
        removeDataIfFound(_dataAircraft, listID);
        removeDataIfFound(_dataHelicopter, listID);
        removeDataIfFound(_dataInfantry, listID);

        RefreshData();
    }

    private void removeDataIfFound(SmallList<ListEntityData> _data, List<string> listID)
    {
        List<ListEntityData> dataToRemove = new List<ListEntityData>();

        for(int i=0; i < _data.Count; i++)
        {
            Debug.Log(_data[i].id);

            if (listID.Contains(_data[i].id)) dataToRemove.Add(_data[i]);
        }

        for(int i=0; i < dataToRemove.Count; i++)
        {
            _data.Remove(dataToRemove[i]);
        }
    }

    public void RefreshData()
    {
        // capture the scroller dimensions so that we can reset them when we are done
        var rectTransform = scroller.GetComponent<RectTransform>();
        var size = rectTransform.sizeDelta;

        // set the dimensions to the largest size possible to acommodate all the cells
        //rectTransform.sizeDelta = new Vector2(size.x, float.MaxValue);

        // First Pass: reload the scroller so that it can populate the text UI elements in the cell view.
        // The content size fitter will determine how big the cells need to be on subsequent passes.
        _calculateLayout = true;
        scroller.ReloadData();

        // reset the scroller size back to what it was originally
        rectTransform.sizeDelta = size;

        // Second Pass: reload the data once more with the newly set cell view sizes and scroller content size
        _calculateLayout = false;

        scroller.ReloadData();
    }

    #region Search & Filter Handler
    public void OnSearch()
    {
        if (!search.isFocused) return;

        var refData = new SmallList<ListEntityData>();
        var removeData = new SmallList<ListEntityData>();

        Regex r = new Regex(search.text.ToLower());
        CheckDataOnSearch(r, vehicleToggle, _dataVehicle, refData, removeData);
        CheckDataOnSearch(r, shipToggle, _dataShip, refData, removeData);
        CheckDataOnSearch(r, submarineToggle, _dataSubmarine, refData, removeData);
        CheckDataOnSearch(r, aircraftToggle, _dataAircraft, refData, removeData);
        CheckDataOnSearch(r, helicopterToggle, _dataHelicopter, refData, removeData);
        CheckDataOnSearch(r, infantryToggle, _dataInfantry, refData, removeData);

        for (int i = 0; i < removeData.Count; i++)
        {
            if (data.Contains(removeData[i])) data.Remove(removeData[i]);
        }

        for (int i = 0; i < refData.Count; i++)
        {
            if (!data.Contains(refData[i])) data.Add(refData[i]);
        }

        RefreshData();
    }

    private void CheckDataOnSearch(Regex r, Toggle dataToggle, SmallList<ListEntityData> sourceData, SmallList<ListEntityData> addedData, SmallList<ListEntityData> removedData)
    {
        if (dataToggle.isOn)
        {
            for (int i = 0; i < sourceData.Count; i++)
            {

                if (r.IsMatch(sourceData[i].nama.ToLower()) || search.text == "")
                {
                    addedData.Add(sourceData[i]);
                }
                else
                {
                    removedData.Add(sourceData[i]);
                }
            }
        }
    }

    public void OnToggleFilter(string filter)
    {
        var refData = new SmallList<ListEntityData>();
        bool isToggled = true;

        switch (filter)
        {
            case "vehicle":
                refData = _dataVehicle;
                isToggled = vehicleToggle.isOn;
                break;
            case "ship":
                refData = _dataShip;
                isToggled = shipToggle.isOn;
                break;
            case "submarine":
                refData = _dataSubmarine;
                isToggled = submarineToggle.isOn;
                break;
            case "aircraft":
                refData = _dataAircraft;
                isToggled = aircraftToggle.isOn;
                break;
            case "helicopter":
                refData = _dataHelicopter;
                isToggled = helicopterToggle.isOn;
                break;
            case "infantry":
                refData = _dataInfantry;
                isToggled = infantryToggle.isOn;
                break;
            default:
                vehicleToggle.isOn = true;
                shipToggle.isOn = true;
                submarineToggle.isOn = true;
                aircraftToggle.isOn = true;
                helicopterToggle.isOn = true;
                infantryToggle.isOn = true;

                return;
        }

        if (isToggled)
        {
            for (int i = 0; i < refData.Count; i++)
            {
                if (!data.Contains(refData[i])) data.Add(refData[i]);
            }
        }
        else
        {
            for (int i = 0; i < refData.Count; i++)
            {
                data.Remove(refData[i]);
            }
        }

        OnFilterKogas();
        OnSearch();

        RefreshData();
    }

    // Dropdown Filter KOGAS
    public void OnFilterKogas()
    {
        long idUser = -99;

        for(int i=0; i < PlayerData.PLAYERS.Count; i++)
        {
            if(PlayerData.PLAYERS[i].name.ToLower() == filterKogas.options[filterKogas.value].text.ToLower())
            {
                idUser = PlayerData.PLAYERS[i].id.GetValueOrDefault(-99);
                i = PlayerData.PLAYERS.Count;
            }
        }

        var refData = new SmallList<ListEntityData>();
        var removeData = new SmallList<ListEntityData>();

        CheckDataOnFilterKogas(idUser, vehicleToggle, _dataVehicle, refData, removeData);
        CheckDataOnFilterKogas(idUser, shipToggle, _dataShip, refData, removeData);
        CheckDataOnFilterKogas(idUser, submarineToggle, _dataSubmarine, refData, removeData);
        CheckDataOnFilterKogas(idUser, aircraftToggle, _dataAircraft, refData, removeData);
        CheckDataOnFilterKogas(idUser, helicopterToggle, _dataHelicopter, refData, removeData);
        CheckDataOnFilterKogas(idUser, infantryToggle, _dataInfantry, refData, removeData);

        for (int i = 0; i < removeData.Count; i++)
        {
            if (data.Contains(removeData[i])) data.Remove(removeData[i]);
        }

        for (int i = 0; i < refData.Count; i++)
        {
            if (!data.Contains(refData[i])) data.Add(refData[i]);
        }

        RefreshData();
    }

    private void CheckDataOnFilterKogas(long userID, Toggle dataToggle, SmallList<ListEntityData> sourceData, SmallList<ListEntityData> addedData, SmallList<ListEntityData> removedData)
    {
        if (dataToggle.isOn)
        {
            for (int i = 0; i < sourceData.Count; i++)
            {

                if (sourceData[i].userID == userID || filterKogas.value == 0)
                {
                    addedData.Add(sourceData[i]);
                }
                else
                {
                    removedData.Add(sourceData[i]);
                }
            }
        }
    }

    public void AddKogas(PData pData)
    {
        bool isExist = false;
        for(int i=0; i < filterKogas.options.Count; i++)
        {
            if(filterKogas.options[i].text.ToLower() == pData.name.ToLower())
            {
                isExist = true;
            }
        }

        if (!isExist)
        {
            var newOption = new List<TMP_Dropdown.OptionData>();
            newOption.Add(new TMP_Dropdown.OptionData() { text = pData.name.ToUpper() });

            filterKogas.AddOptions(newOption);
        }
    }

    public void RemoveKogas(PData pData)
    {
        for (int i = 0; i < filterKogas.options.Count; i++)
        {
            if (filterKogas.options[i].text.ToLower() == pData.name.ToLower())
            {
                filterKogas.options.RemoveAt(i);
            }
        }
    }
    // End of Dropdown Filter KOGAS
    #endregion

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        ListEntityCellView cellView = scroller.GetCellView(cellViewPrefab) as ListEntityCellView;

        // tell the cell view to calculate its layout on the first pass,
        // otherwise just use the size set in the data.
        cellView.SetData(data[dataIndex], _calculateLayout);

        return cellView;
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        // we pull the size of the cell from the model.
        // First pass (frame countdown 2): this size will be zero as set in the LoadData function
        // Second pass (frame countdown 1): this size will be set to the content size fitter in the cell view
        // Third pass (frmae countdown 0): this set value will be pulled here from the scroller
        return data[dataIndex].cellSize;
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        return data.Count;
    }
}
