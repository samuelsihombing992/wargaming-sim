using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Wargaming.Core.GlobalParam.HelperDataAlutsista;
using Wargaming.Core.GlobalParam.HelperPlotting;
using Wargaming.Core.Network.Plotting;

public class ListPlotRadarManager : MonoBehaviour, IEnhancedScrollerDelegate
{
    [Header("References")]
    public EnhancedScroller scroller;
    public EnhancedScrollerCellView cellViewPrefab;
    public int numberOfCellsPerRow = 5;

    [Header("Loading Overlay")]
    public GameObject loadingOverlay;

    [Header("Search & Filter")]
    public TMP_InputField search;
    public Toggle filterDarat;
    public Toggle filterLaut;
    public Toggle filterUdara;

    private SmallList<ListPlotRadarData> data;
    private SmallList<ListPlotRadarData> _dataDarat;
    private SmallList<ListPlotRadarData> _dataLaut;
    private SmallList<ListPlotRadarData> _dataUdara;

    private bool daratAlreadyLoaded = false;
    private bool lautAlreadyLoaded = false;
    private bool udaraAlreadyLoaded = false;

    void Start()
    {
        // tell the scroller that this script will be its delegate
        if (data == null) data = new SmallList<ListPlotRadarData>();
    }

    public async void OpenList()
    {
        if (GetComponent<CanvasGroup>().alpha == 1)
        {
            GetComponent<Animator>().Play("FadeOut");
        }
        else
        {
            GetComponent<Animator>().Play("FadeIn");

            // Request API untuk mengambil seluruh radar ketika list radar pertama kali dibuka
            await RequestData();

            scroller.Delegate = this;
            scroller.ReloadData();

            Debug.Log("Reload Data");
        }
    }

    private async Task RequestData()
    {
        loadingOverlay.SetActive(true);

        // -- Request Satuan Darat --
        var dataRadar = await WargamingAPI.GetListRadar();

        var convertedData = ObjectRadar.FromJson(dataRadar);

        for(int i=0; i < convertedData.Length; i++)
        {
            AddNewData(convertedData[i]);
        }


        if (filterDarat.isOn && !daratAlreadyLoaded)
        {
            
        }
        else if (filterLaut.isOn && !lautAlreadyLoaded)
        {
            
        }
        else if (filterUdara.isOn && !udaraAlreadyLoaded)
        {
            
        }

        //if (dataVehicle != null) GetListSatuanDarat(dataVehicle);

        data = _dataDarat;
        daratAlreadyLoaded = true;

        loadingOverlay.SetActive(false);
    }

    public void AddNewData(ObjectRadar objRadar)
    {
        if (objRadar.radarID == null || objRadar.name == null || objRadar.name == "") return;

        long fontIndex = -1;
        long.TryParse(objRadar.fontIndex, out fontIndex);

        //ObjekRadarInfo info = new ObjekRadarInfo()
        //{
        //    desc = objRadar.name,
        //    radius = objRadar.detectionRange,
        //    size = 30,
        //    newIndex = (int)fontIndex,
        //    armor = 500
        //};

        //ObjekSymbol symbol = new ObjekSymbol()
        //{
        //    width = 30,
        //    fontFamily = objRadar.fontName,
        //    fontIndex = ((char)fontIndex).ToString()
        //};

        //if(objRadar.matra == Wargaming.Core.GlobalParam.EntityHelper.JenisEntity.LAND && _dataDarat == null)
        //{
        //    _dataDarat = new SmallList<ListPlotRadarData>();

        //    _dataDarat.Add(new ListPlotRadarData()
        //    {
        //        objId = objRadar.radarID.GetValueOrDefault(0),
        //        objName = objRadar.name,

        //        data = new ObjekRadar
        //        {
        //            symbolID = objRadar.symbolID.GetValueOrDefault(0),
        //            symbol = symbol,
        //            info = info
        //        },
        //        selectedChanged = null
        //    });
        //}
        //else if(objRadar.matra == Wargaming.Core.GlobalParam.EntityHelper.JenisEntity.SEA && _dataLaut == null)
        //{
        //    _dataLaut = new SmallList<ListPlotRadarData>();

        //    _dataLaut.Add(new ListPlotRadarData()
        //    {
        //        objId = objRadar.radarID.GetValueOrDefault(0),
        //        objName = objRadar.name,

        //        data = new ObjekRadar
        //        {
        //            symbolID = objRadar.symbolID.GetValueOrDefault(0),
        //            symbol = symbol,
        //            info = info
        //        },
        //        selectedChanged = null
        //    });
        //}
        //else if(objRadar.matra == Wargaming.Core.GlobalParam.EntityHelper.JenisEntity.AIR && _dataUdara == null)
        //{
        //    _dataUdara = new SmallList<ListPlotRadarData>();

        //    _dataUdara.Add(new ListPlotRadarData()
        //    {
        //        objId = objRadar.radarID.GetValueOrDefault(0),
        //        objName = objRadar.name,

        //        data = new ObjekRadar
        //        {
        //            symbolID = objRadar.symbolID.GetValueOrDefault(0),
        //            symbol = symbol,
        //            info = info
        //        },
        //        selectedChanged = null
        //    });
        //}
    }

    public int GetNumberOfCells(EnhancedScroller scroller)
    {
        throw new System.NotImplementedException();
    }

    public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
    {
        throw new System.NotImplementedException();
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
    {
        throw new System.NotImplementedException();
    }
}
