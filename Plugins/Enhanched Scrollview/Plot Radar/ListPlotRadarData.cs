using WGSBundling.Core;

public delegate void SelectedRadarChangedDelegate(bool val);

public class ListPlotRadarData
{
    // DATA
    public ObjekRadar data;

    // DATA Asset
    public int objId;
    public string objName;

    public SelectedRadarChangedDelegate selectedChanged;

    /// <summary>
    /// The selection state
    /// </summary>
    private bool _selected;
    public bool Selected
    {
        get { return _selected; }
        set
        {
            // if the value has changed
            if (_selected != value)
            {
                // update the state and call the selection handler if it exists
                _selected = value;
                if (selectedChanged != null) selectedChanged(_selected);
            }
        }
    }
}
