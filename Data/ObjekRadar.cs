using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wargaming.Core.GlobalParam.HelperPlotting;
using static Wargaming.Core.GlobalParam.EntityHelper;

public class ObjekRadar : MonoBehaviour
{
    #region GENERIC DATA
    [Header("GENERIC DATA")]
    public long userID;
    public long documentID;
    public string nama;
    public JenisRadar jenis;
    public bool isActive;
    #endregion

    #region SYMBOL DATA
    [Header("SYMBOL DATA")]
    public long symbolID;
    public ObjekRadarSymbol symbol;
    #endregion

    #region INFO DATA
    [Header("INFO DATA")]
    [SerializeField]
    public ObjekRadarInfo info;

    public string info_symbol;
    #endregion


}
