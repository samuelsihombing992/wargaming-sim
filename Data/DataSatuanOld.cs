using SickscoreGames.HUDNavigationSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataSatuanOld : MonoBehaviour
{
    public enum JenisSatuan { VEHICLE, SHIP, AIRCRAFT, INFANTRY }
    public enum TypeSatuan { DEFAULT, TRACKS, SURFACE, SUB, JET, PROPS, HELICOPTER }


    public string id_entity;

    [Header("Author")]
    public string id_user;

    [Header("Data")]
    public JenisSatuan jenis;
    //public TypeSatuan type;

    public string namaSatuan;
    public string noSatuan;


    public string descSatuan;
    public string fontTaktis;
    public string simbolTaktis;

    [Header("References")]
    public HUDNavigationElement hudElement;
    public HUDNavigationElement hudElementTacmap;
}
