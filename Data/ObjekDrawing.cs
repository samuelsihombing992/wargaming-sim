using Esri.ArcGISMapsSDK.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wargaming.Core.GlobalParam.HelperDataAlutsista;
using static Wargaming.Core.GlobalParam.DrawingHelper;

public class ObjekDrawing : MonoBehaviour
{
    #region GENERIC DATA
    [Header("GENERIC DATA")]
    public long userID;
    public long documentID;
    public string nama;
    public DrawingType type;
    public DrawingArrowType arrowType;
    #endregion

    #region DATA
    [SerializeField]
    public List<GameObject> geometry = new List<GameObject>();

    [Header("PROPERTIES")]
    [SerializeField]
    public EntityDrawingProperty properties;
    #endregion
}
