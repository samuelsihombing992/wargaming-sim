using System;
using UnityEngine;

using Shapes;
using Wargaming.Core;

public class ObjekDrawArrow : MonoBehaviour
{
    public Polygon polygon;
    public Polyline outline;

    public Transform segments;

    private Transform tempSegment;
    [NonSerialized] public Vector3 tempPosition = Vector3.zero;

    public bool OnWidthEdit         = false;
    private float arrowWidth        = 0;
    private float arrowLength      = 0;

    private void Start()
    {
        tempSegment     = segments.GetChild(0).transform;
        tempPosition    = tempSegment.position;
    }

    private void Update()
    {
        if (tempSegment != null && tempSegment.position != tempPosition)
        {
            tempPosition = segments.transform.GetChild(0).position;
            return;
        }

        if (WargamingCam.instance.onRebase) return;
        RefreshAutoSegment();
        RefreshDrawSegment();
    }

    private void RefreshAutoSegment()
    {
        if (segments.childCount < 8) return;

        var startPoint          = segments.GetChild(0);
        var endPoint            = segments.GetChild(1);
        var arrowMiddle         = segments.GetChild(8);

        var arrowLeft           = segments.GetChild(2);
        var arrowLeftEnd        = segments.GetChild(3);
        var arrowRightEnd       = segments.GetChild(6);
        var arrowRight          = segments.GetChild(7);

        var segmentLeftEnd      = segments.GetChild(4);
        var segmentRightEnd     = segments.GetChild(5);

        endPoint.LookAt(startPoint);
        var angle = endPoint.eulerAngles.y;

        startPoint.SetEulerY(angle);
        arrowLeftEnd.SetEulerY(angle);
        arrowRightEnd.SetEulerY(angle);
        segmentLeftEnd.SetEulerY(angle);
        segmentRightEnd.SetEulerY(angle);
        arrowMiddle.SetEulerY(angle);

        if(arrowWidth == 0) { arrowWidth = Vector3.Distance(arrowLeft.position, arrowRight.position); }
        if(arrowLength == 0) { arrowLength = Vector3.Distance(startPoint.position, endPoint.position) / 5; }

        if (OnWidthEdit)
        {
            arrowLeft.LookAt(startPoint);
            var dist = Vector3.Distance(startPoint.position, arrowLeft.position);
            arrowRight.SetEulerY(startPoint.eulerAngles.y - arrowLeft.eulerAngles.y + angle);

            arrowRight.position = startPoint.position + -arrowRight.forward * dist;

            arrowLeft.SetEulerY(angle);
            arrowRight.SetEulerY(angle);

            arrowWidth = Vector3.Distance(arrowLeft.position, arrowRight.position);

            arrowMiddle.position = arrowLeft.position + (arrowLeft.right * ((arrowWidth * 50) / 100));
            arrowLeftEnd.position = arrowLeft.position + (arrowLeft.right * ((arrowWidth * 30) / 100));
            arrowRightEnd.position = arrowRight.position + (-arrowRight.right * ((arrowWidth * 30) / 100));

            arrowLength = Vector3.Distance(startPoint.position, arrowMiddle.position);
            arrowWidth = Vector3.Distance(arrowLeft.position, arrowRight.position);
        }

        arrowLeft.SetEulerY(angle);
        arrowRight.SetEulerY(angle);

        arrowMiddle.position = arrowLeft.position + (arrowLeft.right * ((arrowWidth * 50) / 100));

        if (!OnWidthEdit)
        {
            arrowLeft.position = startPoint.position + (-startPoint.forward * arrowLength) + (-startPoint.right * ((arrowWidth * 50) / 100));
        }

        arrowRight.position = startPoint.transform.position + (-startPoint.forward * arrowLength) + (startPoint.right * ((arrowWidth * 50) / 100));

        arrowLeftEnd.position = arrowLeft.position + (arrowLeft.right * ((arrowWidth * 30) / 100));
        arrowRightEnd.position = arrowRight.position + (-arrowRight.right * ((arrowWidth * 30) / 100));

        segmentLeftEnd.position = endPoint.position + (-endPoint.right * ((arrowWidth * 20) / 100));
        segmentRightEnd.position = endPoint.position + (endPoint.right * ((arrowWidth * 20) / 100));
    }

    private void RefreshDrawSegment()
    {
        if (segments.childCount < 8) return;

        outline.SetPointPosition(0, segments.GetChild(0).position);
        polygon.SetPointPosition(0, new Vector2(segments.GetChild(0).position.x, segments.GetChild(0).position.z));
        segments.GetChild(0).GetComponent<SphereCollider>().radius = (float)WargamingCam.segmentScale;

        for (int i = 1; i < outline.points.Count; i++)
        {
            var segmentPos = segments.GetChild(i + 1).position;

            outline.SetPointPosition(i, segmentPos);
            polygon.SetPointPosition(i, new Vector2(segmentPos.x, segmentPos.z));

            var collider = segments.GetChild(i).GetComponent<SphereCollider>();
            if (collider == null) continue;

            collider.radius = (float)WargamingCam.segmentScale;
        }
    }
}
