using System;
using UnityEngine;

using Shapes;
using Wargaming.Core;

public class ObjekDrawCircle : MonoBehaviour
{
    public Disc outline;
    public Disc fill;

    public Transform segments;

    private void Update()
    {
        if (WargamingCam.instance.onRebase) return;

        //RefreshCircleRadius();
        //RefreshDrawSegment();
    }

    private void RefreshCircleRadius()
    {
        var midPoint = transform;
        var radPoint = segments.GetChild(0);

        var distance = Vector3.Distance(midPoint.position, radPoint.position);
        outline.Radius = distance;
        fill.Radius = distance;
    }

    private void RefreshDrawSegment()
    {
        for (int i = 0; i < segments.childCount; i++)
        {
            segments.GetChild(i).GetComponent<SphereCollider>().radius = (float)WargamingCam.segmentScale;
        }
    }
}
