using System;
using UnityEngine;

using Shapes;
using Wargaming.Core;

public class ObjekDrawRectangle : MonoBehaviour
{
    public Polyline outline;
    public Polygon polygon;

    public Transform segments;

    private Transform tempSegment;
    [NonSerialized] public Vector3 tempPosition = Vector3.zero;

    private enum NodePos { ATAS, BAWAH, KIRI, KANAN }
    private NodePos HPos;
    private NodePos VPos;

    public int selectedIndex = 0;

    //private void Start()
    //{
    //    tempSegment = segments.GetChild(0).transform;
    //    tempPosition = tempSegment.position;
    //}

    public void Init()
    {
        tempSegment = transform.GetChild(0).transform;
    }

    private void Update()
    {
        if (outline.Count <= 0 || tempSegment == null) return;
        if (tempSegment.position != outline[0].point)
        {
            RefreshDrawSegment();
        }

        //if (tempSegment != null && tempSegment.position != tempPosition)
        //{
        //    tempPosition = segments.transform.GetChild(0).position;
        //    return;
        //}

        //if (WargamingCam.instance.onRebase) return;

        //RefreshAutoSegment();
        //RefreshDrawSegment();
    }

    private void RefreshAutoSegment()
    {
        if (segments.childCount != 4 || segments.GetChild(0) == null || segments.GetChild(2) == null) return;

        var startPoint = segments.GetChild(selectedIndex);
        var endPoint = segments.GetChild(GetSegmentIndex(selectedIndex + 2));

        HPos = (endPoint.localPosition.x >= startPoint.localPosition.x) ? NodePos.KANAN : NodePos.KIRI;
        VPos = (endPoint.localPosition.z >= startPoint.localPosition.z) ? NodePos.ATAS : NodePos.BAWAH;

        var autoSegment1 = segments.GetChild(GetSegmentIndex(selectedIndex + 1));
        var autoSegment2 = segments.GetChild(GetSegmentIndex(selectedIndex + 3));

        if (VPos == NodePos.ATAS)
        {
            autoSegment1.position = startPoint.position + startPoint.forward * (endPoint.localPosition.z - startPoint.localPosition.z);
        }
        else
        {
            autoSegment1.position = startPoint.position + -startPoint.forward * (startPoint.localPosition.z - endPoint.localPosition.z);
        }

        if(HPos == NodePos.KANAN)
        {
            autoSegment2.position = startPoint.position + startPoint.right * (endPoint.localPosition.x - startPoint.localPosition.x);
        }
        else
        {
            autoSegment2.position = startPoint.position + -startPoint.right * (startPoint.localPosition.x - endPoint.localPosition.x);
        }
    }

    private int GetSegmentIndex(int _index)
    {
        var lastChild = (segments.childCount - 1);

        if (_index > lastChild)
        {
            return _index - lastChild - 1;
        }
        else if(_index < 0)
        {
            return lastChild + _index;
        }
        else
        {
            return _index;
        }
    }

    private void RefreshDrawSegment()
    {
        for (int i = 0; i < outline.points.Count; i++)
        {
            var segmentPos = segments.GetChild(i).position;

            outline.SetPointPosition(i, segmentPos);
            polygon.SetPointPosition(i, new Vector2(segmentPos.x, segmentPos.z));
        }

        //for (int i = 0; i < segments.childCount; i++)
        //{
        //    var segmentPos = segments.GetChild(i).position;

        //    polygon.SetPointPosition(i, new Vector2(segmentPos.x, segmentPos.z));
        //    outline.SetPointPosition(i, segmentPos);

        //    segments.GetChild(i).GetComponent<SphereCollider>().radius = (float)WargamingCam.segmentScale;
        //}
    }
}
