using System;
using UnityEngine;

using Shapes;
using Wargaming.Core;

public class ObjekDrawPolygon : MonoBehaviour
{
    public bool isOnEdit = false;
    public Polygon polygon;
    public Polyline outline;

    public Transform segments;

    private Transform tempSegment;
    [NonSerialized] public Vector3 tempPosition = Vector3.zero;

    //private void Start()
    //{
    //    tempSegment     = segments.GetChild(0).transform;
    //    tempPosition    = tempSegment.position;
    //}

    public void Init()
    {
        tempSegment = transform.GetChild(0).transform;
    }

    private void Update()
    {
        if (outline.Count <= 0 || tempSegment == null) return;
        if (tempSegment.position != outline[0].point)
        {
            RefreshDrawSegment();
        }

        return;

        //if(tempSegment != null && tempSegment.position != tempPosition)
        //{
        //    tempPosition = segments.transform.GetChild(0).position;
        //    return;
        //}

        //if (WargamingCam.instance.onRebase) return;

        //RefreshDrawSegment();
    }

    private void RefreshDrawSegment()
    {
        for (int i = 0; i < outline.points.Count; i++)
        {
            var segmentPos = segments.GetChild(i).position;

            outline.SetPointPosition(i, segmentPos);
            polygon.SetPointPosition(i, new Vector2(segmentPos.x, segmentPos.z));
        }

        //for (int i = 0; i < polygon.points.Count; i++)
        //{
        //    var segmentPos = segments.GetChild(i).position;

        //    polygon.SetPointPosition(i, new Vector2(segmentPos.x, segmentPos.z));
        //    outline.SetPointPosition(i, segmentPos);

        //    segments.GetChild(i).GetComponent<SphereCollider>().radius = (float)WargamingCam.segmentScale;
        //}
    }
}
