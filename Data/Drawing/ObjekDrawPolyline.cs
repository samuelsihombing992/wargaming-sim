using System;
using UnityEngine;

using Shapes;
using Wargaming.Core;
using Wargaming.Core.ScenarioEditor.Drawing;

public class ObjekDrawPolyline : MonoBehaviour
{
    //public bool isOnEdit = false;
    public Polyline polyline;

    public Transform segments;

    private Transform tempSegment;
    [NonSerialized] public Vector3 tempPosition = Vector3.zero;

    private void Start()
    {
        //tempSegment     = transform.GetChild(0).transform;
        //tempPosition    = tempSegment.position;
    }

    public void Init()
    {
        tempSegment = transform.GetChild(0).transform;
    }

    private void Update()
    {
        if (polyline.Count <= 0 || tempSegment == null) return;
        if (tempSegment.position != polyline[0].point)
        {
            RefreshDrawSegment();
        }

        return;

        if (tempSegment != null && tempSegment.position != tempPosition)
        {
            //RefreshDrawSegment();

            // RESYNC TRANSFORM POSITION WHENEVER MAP IS REBASING (Only Referencing to 1 segment only)
            tempPosition = transform.GetChild(0).position;
            return;
        }

        if (WargamingCam.instance.onRebase) return;

        //RefreshDrawSegment();
    }

    private void RefreshDrawSegment()
    {
        for (int i = 0; i < polyline.points.Count; i++)
        {
            polyline.SetPointPosition(i, segments.GetChild(i).position);
        }

            //for(int i=0; i < polyline.points.Count; i++)
            //{
            //    polyline.SetPointPosition(i, transform.GetChild(i).position);

            //    if(GameController.instance == null)
            //    {
            //        transform.GetChild(i).GetComponent<SphereCollider>().radius = (float)WargamingCam.segmentScale;
            //    }
            //}
    }
}
