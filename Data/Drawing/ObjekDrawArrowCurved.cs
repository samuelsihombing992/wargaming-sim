using System;
using UnityEngine;

using Shapes;
using Wargaming.Core;

public class ObjekDrawArrowCurved : MonoBehaviour
{
    public Polygon polygon;
    public Polyline outline;

    public Transform segments;

    private Transform tempSegment;
    [NonSerialized] public Vector3 tempPosition = Vector3.zero;

    public enum ArrowDirection { UP, DOWN }
    public ArrowDirection direction;

    private void Start()
    {
        tempSegment = segments.GetChild(0).transform;
        tempPosition = tempSegment.position;
    }

    private void Update()
    {
        if(tempSegment != null && tempSegment.position != tempPosition)
        {
            tempPosition = segments.transform.GetChild(0).position;
            return;
        }

        if (WargamingCam.instance.onRebase) return;
        return;

        if(direction == ArrowDirection.UP)
        {
            RefreshAutoUpSegment();
        }
        else
        {
            RefreshAutoDownSegment();
        }

        RefreshDrawSegment();
    }

    private void RefreshAutoUpSegment()
    {
        if (segments.childCount < 2) return;

        var startPoint  = segments.GetChild(0);
        var endPoint    = segments.GetChild(1);

        var radius = Vector3.Distance(startPoint.position, endPoint.position) / 2f;
        var centerPos = (startPoint.position + endPoint.position) / 2f;
        var centerDirection = Quaternion.LookRotation((endPoint.position - startPoint.position).normalized);

        for(int i=0; i < 30; i++)
        {
            var angle = Mathf.PI * (i + 1) / (30 + 1f);
            var x = Mathf.Sin(angle) * radius;
            var z = Mathf.Cos(angle) * radius;
            var pos = new Vector3(x, 0, z);

            pos = centerDirection * pos;

            var segment = segments.GetChild(i);
            segment.position = centerPos + pos;
        }
    }

    private void RefreshAutoDownSegment()
    {

    }

    private void RefreshDrawSegment()
    {

    }
}
