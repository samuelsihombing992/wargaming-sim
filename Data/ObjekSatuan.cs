using UnityEngine;

using Wargaming.Core.GlobalParam.HelperPlotting;
using SickscoreGames.HUDNavigationSystem;
using System.Collections.Generic;
using static Wargaming.Core.GlobalParam.EntityHelper;
using Wargaming.Core.GlobalParam;

//[CreateAssetMenu(fileName = "ObjekSatuan", menuName = "WGS/Entities/Objek Satuan")]
public class ObjekSatuan : MonoBehaviour
{
    #region GENERIC DATA
    [Header("GENERIC DATA")]
    public long userID;
    public long documentID;
    public string nama;
    public KategoriSatuan kategori;
    public bool isActive;
    #endregion

    #region SYMBOL DATA
    [Header("SYMBOL DATA")]
    public long symbolID;
    public ObjekSatuanStyle style;
    public ObjekSymbol symbol;
    #endregion

    #region INFO DATA
    [Header("INFO DATA")]
    [SerializeField]
    public ObjekSatuanInfo info;

    [Header("WEAPON DATA")]
    [SerializeField]
    public List<List<ObjekSatuanWeapon>> weapon;
    #endregion

    #region MP PLAYER DATA
    public PData player;
    #endregion

    public string id_kegiatan;
    public string isi_logistik;

    [Header("HUD NAVIGATION DATA")]
    public HUDNavigationElement hudElement;
    public HUDNavigationElement hudElementTacmap;
}