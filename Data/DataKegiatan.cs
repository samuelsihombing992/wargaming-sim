using System;
using System.Collections.Generic;
using UnityEngine;

public class DataKegiatan : MonoBehaviour
{
    public DateTime timeKegiatan;

    [Header("Data")]
    public string id;
    public string keterangan;

    public int hariH;
    public int percepatan;

    public List<int> activeOn = new List<int>();
}