using Expanse;
using UnityEngine;

public class DataCloudPreset : MonoBehaviour
{
    public UniversalCloudLayer preset;
    public string presetUrl;
    public int weatherOverride = -1;
}
