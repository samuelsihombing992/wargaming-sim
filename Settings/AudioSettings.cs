using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;
using ProceduralWorlds.HDRPTOD;

public class AudioSettings : MonoBehaviour
{
    [Header("Dropdown")]
    [SerializeField] TMP_Dropdown audioOutputDD;

    [Header("Slider")]
    [SerializeField] Slider mainVolumeS;
    [SerializeField] Slider ambienceVolumeS;

    [Header("References")]
    [SerializeField] HDRPTimeOfDay hDRPTimeOfDay;

    float vol;
    int index;

    // Start is called before the first frame update
    public void Start() 
    {
        mainVolumeS.value = PlayerPrefs.GetFloat("mainVolume");
        ambienceVolumeS.value = PlayerPrefs.GetFloat("ambienceVolume");
        //LoadVolume();
    }


    public void ChangeAudioOtpt(int Level)
    {
        switch (Level)
        {
            case 0: //Enabled
                hDRPTimeOfDay.UseAmbientAudio = true;
                hDRPTimeOfDay.WeatherProfiles[0].WeatherFXData.m_weatherEffectVolume = PlayerPrefs.GetFloat("ambienceVolume");
                hDRPTimeOfDay.WeatherProfiles[1].WeatherFXData.m_weatherEffectVolume = PlayerPrefs.GetFloat("ambienceVolume");
                hDRPTimeOfDay.WeatherProfiles[2].WeatherFXData.m_weatherEffectVolume = PlayerPrefs.GetFloat("ambienceVolume");
                hDRPTimeOfDay.WeatherProfiles[3].WeatherFXData.m_weatherEffectVolume = PlayerPrefs.GetFloat("ambienceVolume");
                hDRPTimeOfDay.WeatherProfiles[4].WeatherFXData.m_weatherEffectVolume = PlayerPrefs.GetFloat("ambienceVolume");
                hDRPTimeOfDay.WeatherProfiles[5].WeatherFXData.m_weatherEffectVolume = PlayerPrefs.GetFloat("ambienceVolume");
                break;
            case 1: //Disabled
                hDRPTimeOfDay.UseAmbientAudio = false;
                hDRPTimeOfDay.WeatherProfiles[0].WeatherFXData.m_weatherEffectVolume = 0;
                hDRPTimeOfDay.WeatherProfiles[1].WeatherFXData.m_weatherEffectVolume = 0;
                hDRPTimeOfDay.WeatherProfiles[2].WeatherFXData.m_weatherEffectVolume = 0;
                hDRPTimeOfDay.WeatherProfiles[3].WeatherFXData.m_weatherEffectVolume = 0;
                hDRPTimeOfDay.WeatherProfiles[4].WeatherFXData.m_weatherEffectVolume = 0;
                hDRPTimeOfDay.WeatherProfiles[5].WeatherFXData.m_weatherEffectVolume = 0;
                break;
        }
        audioOutputDD.onValueChanged.AddListener(delegate { ChangeAudioOtpt(audioOutputDD.value); });
    }

    public void ChangeMainVol()
    {
        hDRPTimeOfDay.UseAmbientAudio = true;

        float volume = mainVolumeS.value * 0.01f;

        //hDRPWeatherProfile.WeatherFXData.m_weatherEffect. = volume;
        
        PlayerPrefs.SetFloat("mainVolume", volume);
    }
    
    public void ChangeAmbienceVol()
    {
        hDRPTimeOfDay.UseAmbientAudio = true;

        float volume = ambienceVolumeS.value * 0.01f;
        hDRPTimeOfDay.AudioProfile.m_masterVolume = volume;
        hDRPTimeOfDay.AudioProfile.m_weatherVolumeMultiplier = volume;

        hDRPTimeOfDay.WeatherProfiles[0].WeatherFXData.m_weatherEffectVolume = volume;
        hDRPTimeOfDay.WeatherProfiles[1].WeatherFXData.m_weatherEffectVolume = volume;
        hDRPTimeOfDay.WeatherProfiles[2].WeatherFXData.m_weatherEffectVolume = volume;
        hDRPTimeOfDay.WeatherProfiles[3].WeatherFXData.m_weatherEffectVolume = volume;
        hDRPTimeOfDay.WeatherProfiles[4].WeatherFXData.m_weatherEffectVolume = volume;
        hDRPTimeOfDay.WeatherProfiles[5].WeatherFXData.m_weatherEffectVolume = volume;

        PlayerPrefs.SetFloat("ambienceVolume", volume);
    }

    //private void LoadVolume()
    //{
    //    mainVolumeS.value = PlayerPrefs.GetFloat("mainVolume");
    //    ambienceVolumeS.value = PlayerPrefs.GetFloat("ambienceVolume");

    //    //ChangeMainVol();
    //    //ChangeAmbienceVol();
    //}
}
