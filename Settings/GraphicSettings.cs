using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using TMPro;

public class GraphicSettings : MonoBehaviour
{
    [Header("Dropdown")]
    [SerializeField] TMP_Dropdown textureQualityDD;
    [SerializeField] TMP_Dropdown shadowQualityDD;
    [SerializeField] TMP_Dropdown objectQualityDD;
    [SerializeField] TMP_Dropdown oceanQualityDD;
    [SerializeField] TMP_Dropdown volumetricCloudsDD;
    
    [Header("References")]
    [SerializeField] HDAdditionalLightData HDLight;
    [SerializeField] Expanse.ProceduralCloudVolume PCVolume;
    [SerializeField] Expanse.CreativeCloudVolume CCVolume;
    [SerializeField] GameObject vClouds;
    [SerializeField] Crest.OceanRenderer oceanRender;
    [SerializeField] GameObject lodObject;

    LODGroup lodGroup;
    LOD[] lods;

    public void Start()
    {

    }

    public void ChangeTextureQlt(int Level)
    {
        switch (Level)
        {
            case 0: // Ultra
                QualitySettings.masterTextureLimit = 0;
                break;
            case 1: // High
                QualitySettings.masterTextureLimit = 1;
                break;
            case 2: // Medium
                QualitySettings.masterTextureLimit = 2;
                break;
            case 3: // Low
                QualitySettings.masterTextureLimit = 3;
                break;
        }
        shadowQualityDD.onValueChanged.AddListener(delegate { ChangeShadowQlty(shadowQualityDD.value); });
    }

    public void ChangeObjectQlty(int Level)
    {
        switch (Level)
        {
            case 0: // Ultra
                lodGroup = lodObject.GetComponent<LODGroup>();

                lods = lodGroup.GetLODs();
                lods[0].screenRelativeTransitionHeight = 0.8f;
                lods[1].screenRelativeTransitionHeight = 0.5f;
                lods[2].screenRelativeTransitionHeight = 0.3f;
                lods[3].screenRelativeTransitionHeight = 0.01f;

                lodGroup.SetLODs(lods);
                lodGroup.RecalculateBounds();
                break;
            case 1: // High
                lodGroup = lodObject.GetComponent<LODGroup>();

                lods = lodGroup.GetLODs();
                lods[0].screenRelativeTransitionHeight = 1f;
                lods[1].screenRelativeTransitionHeight = 0.05f;
                lods[2].screenRelativeTransitionHeight = 0.03f;
                lods[3].screenRelativeTransitionHeight = 0.01f;

                lodGroup.SetLODs(lods);
                lodGroup.RecalculateBounds();
                break;
            case 2: // Medium
                lodGroup = lodObject.GetComponent<LODGroup>();

                lods = lodGroup.GetLODs();
                lods[0].screenRelativeTransitionHeight = 1f;
                lods[1].screenRelativeTransitionHeight = 0.999f;
                lods[2].screenRelativeTransitionHeight = 0.03f;
                lods[3].screenRelativeTransitionHeight = 0.01f;

                lodGroup.SetLODs(lods);
                lodGroup.RecalculateBounds();
                break;
            case 3: // Low
                lodGroup = lodObject.GetComponent<LODGroup>();

                lods = lodGroup.GetLODs();
                lods[0].screenRelativeTransitionHeight = 1f;
                lods[1].screenRelativeTransitionHeight = 0.999f;
                lods[2].screenRelativeTransitionHeight = 0.998f;
                lods[3].screenRelativeTransitionHeight = 0.01f;

                lodGroup.SetLODs(lods);
                lodGroup.RecalculateBounds();
                break;
        }
        objectQualityDD.onValueChanged.AddListener(delegate { ChangeObjectQlty(objectQualityDD.value); });
    }

    public void ChangeShadowQlty(int Level)
    {
        switch (Level)
        {
            case 0: // Ultra
                HDLight.EnableShadows(true);
                PCVolume.m_highQualityShadows = true;
                PCVolume.m_castShadows = true;
                PCVolume.m_selfShadowing = true;
                PCVolume.m_maxShadowIntensity = 1f;
                HDLight.shadowResolution.level = 3;
                HDLight.useContactShadow.level = 2;
                break;
            case 1: // High
                HDLight.EnableShadows(true);
                PCVolume.m_highQualityShadows = true;
                PCVolume.m_castShadows = true;
                PCVolume.m_selfShadowing = true;
                PCVolume.m_maxShadowIntensity = 0.8f;
                HDLight.shadowResolution.level = 2;
                HDLight.useContactShadow.level = 2;
                break;
            case 2: // Medium
                HDLight.EnableShadows(true);
                PCVolume.m_castShadows = true;
                PCVolume.m_highQualityShadows = false;
                PCVolume.m_selfShadowing = true;
                PCVolume.m_maxShadowIntensity = 0.5f;
                HDLight.shadowResolution.level = 1;
                HDLight.useContactShadow.level = 1;
                break;
            case 3: // Low
                HDLight.EnableShadows(true);
                PCVolume.m_castShadows = true;
                PCVolume.m_highQualityShadows = false;
                PCVolume.m_selfShadowing = true;
                PCVolume.m_maxShadowIntensity = 0.2f;
                HDLight.shadowResolution.level = 0;
                HDLight.useContactShadow.level = 0;
                break;
            case 4: // Disabled
                HDLight.EnableShadows(false);
                PCVolume.m_castShadows = false;
                PCVolume.m_highQualityShadows = false;
                PCVolume.m_selfShadowing = false;
                break;
        }
        shadowQualityDD.onValueChanged.AddListener(delegate { ChangeShadowQlty(shadowQualityDD.value); });
    }

    public void ChangeOceanQlty(int Level)
    {
        switch (Level)
        {
            case 0: //Ultra
                oceanRender._globalWindSpeed = 12;
                oceanRender._lodCount = 7;
                break;
            case 1: //High
                oceanRender._globalWindSpeed = 6;
                oceanRender._lodCount = 5;
                break;
            case 2: //Medium
                oceanRender._globalWindSpeed = 3;
                oceanRender._lodCount = 4;
                break;
            case 3: //Low
                oceanRender._globalWindSpeed = 0;
                oceanRender._lodCount = 2;
                break;
        }
        oceanQualityDD.onValueChanged.AddListener(delegate { ChangeOceanQlty(oceanQualityDD.value); });
    }
    
    public void ChangeVolClouds(int Level)
    {
        switch (Level)
        {
            case 0: //High
                vClouds.SetActive(true);
                CCVolume.m_quality = Expanse.Datatypes.Quality.High;
                PCVolume.m_reprojectionFrames = 3;
                break;
            case 1: // Ultra
                vClouds.SetActive(true);
                CCVolume.m_quality = Expanse.Datatypes.Quality.Ultra;
                PCVolume.m_reprojectionFrames = 4;
                break;
            case 2: //Medium
                vClouds.SetActive(true);
                CCVolume.m_quality = Expanse.Datatypes.Quality.Medium;
                PCVolume.m_reprojectionFrames = 2;
                break;
            case 3: //Low
                vClouds.SetActive(true);
                CCVolume.m_quality = Expanse.Datatypes.Quality.Low;
                PCVolume.m_reprojectionFrames = 1;
                break;
            case 4: //Disabled
                vClouds.SetActive(false);
                break;
        }
        volumetricCloudsDD.onValueChanged.AddListener(delegate { ChangeVolClouds(volumetricCloudsDD.value); });
    }
}

