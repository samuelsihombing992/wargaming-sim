using System;
using Wargaming.Core.GlobalParam;
using Wargaming.Core.GlobalParam.HelperSessionUser;
using Wargaming.Core.GlobalParam.HelperScenarioAktif;
using System.Collections.Generic;
using Newtonsoft.Json;
using Wargaming.Components.Playback;
using static Wargaming.Core.GlobalParam.EntityHelper;

namespace Wargaming.Core.GlobalParam
{
    // Server Config Data
    [Serializable]
    public class ServerConfig
    {
        public static int? ACTIVE_INDEX { get; set; }
        public static string SERVER_NAME { get; set; }
        public static string SERVICE_LOGIN { get; set; }
        public static string SERVICE_CB { get; set; }
        public static string SERVER_COLYSEUS { get; set; }

        public static int? OCEAN_BASE { get; set; }

        public ServerConfig(ServerConfigHelper config)
        {
            ACTIVE_INDEX = config.active_index;
            SERVER_NAME = config.server_name;
            SERVICE_LOGIN = config.service_login;
            SERVICE_CB = config.service_cb;
            SERVER_COLYSEUS = config.server_colyseus;
        }
    }

    // Application Settings
    [Serializable]
    public class AppSettings
    {
        public static string MONITOR { get; set; }
        public static string RESOLUTION { get; set; }
        public static int? DISPLAY { get; set; }
        public static int? VSYNC { get; set; }
        public static int? ANTI_ALIASING { get; set; }
        public static int? ANISOTROPIC_FILTERING { get; set; }

        public AppSettings(AppSettingsData data)
        {
            MONITOR = data.monitor;
            RESOLUTION = data.monitor;
            DISPLAY = data.display;
            VSYNC = data.vsync;
            ANTI_ALIASING = data.anti_aliasing;
            ANISOTROPIC_FILTERING = data.anisotropic_filtering;
        }
    }

    // Active User Data
    [Serializable]
    public class SessionUser
    {
        public static string username { get; set; }
        public static long? id { get; set; }
        public static string name { get; set; }
        public static string jenis_user { get; set; }
        public static string bagian { get; set; }
        public static long? id_bagian { get; set; }
        public static long? id_kogas { get; set; }
        public static long? asisten { get; set; }
        public static string nama_asisten { get; set; }
        public static string jabatan { get; set; }

        public SessionUser(SessionUserHelper user)
        {
            username = user.username;
            id = user.id;
            name = user.name;

            if (user.jenis_user != null)
            {
                jenis_user = user.jenis_user.jenis_user;
            }

            if (user.bagian == null)
            {
                id_bagian = 0;
                bagian = "";
            }
            else
            {
                id_bagian = user.bagian.ID;
                bagian = user.bagian.nama_bagian;
            }

            if (user.asisten == null)
            {
                asisten = 0;
                nama_asisten = "";
            }
            else
            {
                asisten = user.asisten.ID;

                if (user.asisten.nama_asisten != null)
                {
                    nama_asisten = user.asisten.nama_asisten;
                }
                else
                {
                    nama_asisten = "";
                }
            }

            if (user.jabatan != null)
            {
                jabatan = user.jabatan.nama_jabatan;
            }
        }

        public static void Reset()
        {
            username        = null;
            id              = null;
            nama_asisten    = null;
            jenis_user      = null;
            bagian          = null;
            id_bagian       = null;
            id_kogas        = null;
            asisten         = null;
            nama_asisten    = null;
            jabatan         = null;
        }
    }

    // Skenario Active
    [Serializable]
    public class SkenarioAktif
    {
        public static long? ID_SKENARIO { get; set; }
        public static string NAMA_SKENARIO { get; set; }
        public static string ID_DOCUMENT { get; set; }
        public static DateTime HARI_H { get; set; }
        public static DateTime WaktuMulai { get; set; }
        public static DateTime WaktuAkhir { get; set; }

        public SkenarioAktif(SkenarioAktifHelper skenario)
        {
            ID_SKENARIO = skenario.ID;
            NAMA_SKENARIO = skenario.nama_skenario;
        }
    }

    // CB Terbaik (Sendiri / BLUE FORCE)
    //public class CBSendiri
    //{
    //    public static long? id_cb { get; set; }
    //    public static long? id_cb_eppkm { get; set; }
    //    public static long? id_kogas { get; set; }
    //    public static long? id_user { get; set; }
    //    public static string nama_kogas { get; set; }
    //    public static string tipe_cb { get; set; }
    //    public static string? nama_document { get; set; }
    //    public static string hari { get; set; }

    //    //public static CBTerbaikDocumentHelper document { get; set; }
    //    //public static CBTerbaikUserHelper user { get; set; }

    //    public CBSendiri(CBTerbaikHelper cbTerbaik)
    //    {
    //        //document = cbTerbaik.document;
    //        //user = cbTerbaik.user;

    //        id_cb = cbTerbaik.id_cb;
    //        id_cb_eppkm = cbTerbaik.id_cb_eppkm;
    //        id_kogas = cbTerbaik.id_kogas;
    //        id_user = cbTerbaik.id_user;
    //        nama_kogas = cbTerbaik.nama_kogas;
    //        tipe_cb = cbTerbaik.tipe_cb;
    //        nama_document = cbTerbaik.nama_document;
    //        hari = cbTerbaik.hari;
    //    }
    //}

    // CB Terbaik (Musuh / RED FORCE)
    public class CBMusuh
    {
        //public static CBTerbaikDocumentHelper document { get; set; }
        //public static CBTerbaikUserHelper user { get; set; }

        //public CBMusuh(CBTerbaikHelper cbTerbaik)
        //{
        //    document = cbTerbaik.document;
        //    user = cbTerbaik.user;
        //}

        public static long? id_cb { get; set; }
        public static long? id_cb_eppkm { get; set; }
        public static long? id_kogas { get; set; }
        public static long? id_user { get; set; }
        public static string nama_kogas { get; set; }
        public static string tipe_cb { get; set; }
        public static string nama_document { get; set; }
        public static string hari { get; set; }

        public CBMusuh(CBTerbaikHelper cbTerbaik)
        {
            id_cb = cbTerbaik.id_cb;
            id_cb_eppkm = cbTerbaik.id_cb_eppkm;
            id_kogas = cbTerbaik.id_kogas;
            id_user = cbTerbaik.id_user;
            nama_kogas = cbTerbaik.nama_kogas;
            tipe_cb = cbTerbaik.tipe_cb;
            nama_document = cbTerbaik.nama_document;
            hari = cbTerbaik.hari;
        }
    }

    // CB Terbaik
    public class DataCB
    {
        public static List<CB> data = new List<CB>();

        public static CB GetYourCB()
        {
            if (data.Count <= 0) return null;
            return data[0];
        }

        public static CB AddNewCB(CBTerbaikHelper cb, EntityOpponent alliance = EntityOpponent.NEUTRAL)
        {
            var newData = new CB
            {
                id_cb = cb.id_cb,
                id_cb_eppkm = cb.id_cb_eppkm,
                id_kogas = cb.id_kogas,
                id_user = cb.id_user,
                nama_kogas = cb.nama_kogas,
                tipe_cb = cb.tipe_cb,
                nama_document = cb.nama_document,
                hari = cb.hari,
                alliance = alliance
            };

            data.Add(newData);

            return newData;
        }

        public static CB AddNewCB(CBHelper cb, EntityOpponent alliance = EntityOpponent.NEUTRAL)
        {
            var newData = new CB
            {
                id_cb = cb.document.id,
                id_kogas = cb.user.bagian,
                id_user = cb.user.id,
                nama_kogas = cb.user.name,
                tipe_cb = "CB Sendiri",
                nama_document = cb.document.nama_dokumen,
                alliance = alliance
            };

            data.Add(newData);

            return newData;
        }
    }

    public class CB
    {
        public long? id_cb { get; set; }
        public long? id_cb_eppkm { get; set; }
        public long? id_kogas { get; set; }
        public long? id_user { get; set; }
        public string nama_kogas { get; set; }
        public string tipe_cb { get; set; }
        public string nama_document { get; set; }
        public string hari { get; set; }

        public EntityOpponent alliance { get; set; }
    }

    // List User
    [Serializable]
    public class PlayerData
    {
        public static List<PData> PLAYERS { get; set; }

        public static void AddPlayer(PData pData)
        {
            if (PLAYERS.Contains(pData)) return;
            PLAYERS.Add(pData);

            if(ListEntityManager.instance != null) { ListEntityManager.instance.AddKogas(pData); }
        }

        public static void RemovePlayer(string key)
        {
            for (int i = 0; i < PLAYERS.Count; i++)
            {
                if (PLAYERS[i].id == long.Parse(key))
                {
                    if (ListEntityManager.instance != null) { ListEntityManager.instance.RemoveKogas(PLAYERS[i]); }

                    PLAYERS.RemoveAt(i);
                    i = PLAYERS.Count;
                }
            }
        }

        public PlayerData(SessionUserHelper user)
        {
            PLAYERS = new List<PData>();

            AddPlayer(new PData
            {
                username = user.username,
                id = user.id,
                name = user.name,
                jenis_user = (user.jenis_user != null) ? user.jenis_user.jenis_user : null,
                id_bagian = (user.bagian != null) ? user.bagian.ID : 0,
                bagian = (user.bagian != null) ? user.bagian.nama_bagian : "",
                asisten = (user.asisten != null) ? user.asisten.ID : 0,
                nama_asisten = (user.asisten != null) ? user.asisten.nama_asisten : "",
                jabatan = (user.jabatan != null) ? user.jabatan.nama_jabatan : "",
                //timeMovement = "0",
            });
        }
    }

    public class PData
    {
        public string username;
        public long? id;
        public string name;
        public string jenis_user;
        public string bagian;
        public long? id_bagian;
        public long? id_kogas;
        public long? asisten;
        public string nama_asisten;
        public string jabatan;

        public CB cbAktif;

        //public string timeString;
        public DateTime? timeMovement;
        public bool isLoaded;
    }

    public class SceneLoad
    {
        public static string returnTo { get; set; }
    }

    public class GlobalMapService
    {
        // Basemap Services
        public static bool useBasemapService { get; set; }
        public static List<BasemapData> BASEMAP_DATA { get; set; }

        // Layer Services
        public static bool useLayerService { get; set; }
        public static List<LayerData> LAYER_DATA { get; set; }


        // Terrain Services
        public static bool useTerrainService { get; set; }
        public static List<BasemapData> TERRAIN_DATA { get; set; }
    }

    public class DisplaySetup
    {
        public enum DisplayContent { NOTHING, SIM, TIMETABLE, HARIH, ENTITY_LIST, PLAYBACK }

        public static List<DisplayData> DISPLAYS { get; set; }
    }

    //// Basemap Services
    //[Serializable]
    //public class Basemaps
    //{
    //    public static List<BasemapData> DATA { get; set; }
    //}

    //// Layer Services
    //[Serializable]
    //public class Layers
    //{
    //    public static List<LayerData> DATA { get; set; }
    //}

    [Serializable]
    public class ThemeImage
    {
        public static List<ThemeImageConfig> settings { get; set; }
        public static bool themeLoaded { get; set; }
    }
}