using Esri.HPFramework;
using FIMSpace.GroundFitter;
using Unity.Mathematics;
using UnityEngine;

public class RootSync : MonoBehaviour
{
    public enum UpdateMode { UPDATE, FIXED_UPDATE, LATE_UPDATE }
    public UpdateMode mode;

    public HPTransform hpTransform;
    public HPTransform rootTransform;

    public bool syncAltitude = false;
    public FGroundFitter groundFitter;

    public void Init()
    {
        hpTransform = GetComponent<HPTransform>();
        if (hpTransform == null || rootTransform == null) enabled = false;
    }

    //private void Update()
    //{
    //    if (hpTransform == null || rootTransform == null) enabled = false;
    //    if (mode != UpdateMode.UPDATE) return;

    //    if(syncAltitude)
    //    {
    //        hpTransform.UniversePosition = new double3(rootTransform.UniversePosition.x, hpTransform.UniversePosition.y, rootTransform.UniversePosition.z);
    //    }
    //    else
    //    {
    //        hpTransform.UniversePosition = rootTransform.UniversePosition;
    //    }

    //    hpTransform.UniverseRotation = rootTransform.UniverseRotation;
    //    hpTransform.LocalScale = rootTransform.LocalScale;
    //}

    //private void FixedUpdate()
    //{
    //    if (hpTransform == null || rootTransform == null) enabled = false;
    //    if (mode != UpdateMode.FIXED_UPDATE) return;

    //    if (syncAltitude)
    //    {
    //        hpTransform.UniversePosition = new double3(rootTransform.UniversePosition.x, hpTransform.UniversePosition.y, rootTransform.UniversePosition.z);
    //    }
    //    else
    //    {
    //        hpTransform.UniversePosition = rootTransform.UniversePosition;
    //    }

    //    hpTransform.UniverseRotation = rootTransform.UniverseRotation;
    //    hpTransform.LocalScale = rootTransform.LocalScale;
    //}

    //private void LateUpdate()
    //{
    //    if (hpTransform == null || rootTransform == null) enabled = false;
    //    if (mode != UpdateMode.LATE_UPDATE) return;

    //    if (syncAltitude)
    //    {
    //        hpTransform.UniversePosition = new double3(rootTransform.UniversePosition.x, hpTransform.UniversePosition.y, rootTransform.UniversePosition.z);
    //    }
    //    else
    //    {
    //        hpTransform.UniversePosition = rootTransform.UniversePosition;
    //    }

    //    hpTransform.UniverseRotation = rootTransform.UniverseRotation;
    //    hpTransform.LocalScale = rootTransform.LocalScale;
    //}
}
