using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Networking;

using Wargaming.Core.GlobalParam;
using Wargaming.Core.GlobalParam.HelperDataAlutsista;
using Wargaming.Core.GlobalParam.HelperPlotting;
using Wargaming.Core.GlobalParam.HelperScenarioAktif;
using Wargaming.Core.GlobalParam.HelperSessionUser;
using Wargaming.Core.Network;

namespace Wargaming.Core.Network
{
    public class WargamingAPI : MonoBehaviour
    {
        //static bool enableDebug = true;

        /// <summary>
        /// Login Aplikasi
        /// </summary>
        /// <param name="form">WWWForm</param>
        /// <returns></returns>
        public static async Task<string> RequestLogin(WWWForm form, string server_file = null, int server = -99)
        {
            var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_LOGIN + "/api/login", form, WargamingAPIHelper.enableDebug);
            if (result.data != null)
            {
                try
                {
                    SessionUserHelper[] user = SessionUserHelper.FromJson(result.data);
                    SessionUserHelper userLogin = user[0];

                    new SessionUser(userLogin);
                    new PlayerData(userLogin);

                    return "done";

                }
                catch (Exception e)
                {
                    return "retry";
                }
            }

            return WargamingAPIHelper.checkRequestResult(result.request);
        }

        /// <summary>
        /// Logout Aplikasi
        /// </summary>
        /// <param name="form">New Empty Form ( karena metode post, perlu form )</param>
        /// <returns></returns>
        public static async Task RequestLogout()
        {
            var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_LOGIN + "/api/logout", null, WargamingAPIHelper.enableDebug);
        }

        /// <summary>
        /// Dapatkan Skenario Aktif
        /// </summary>
        /// <returns></returns>
        public static async Task<string> GetSkenarioAktif()
        {
            var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_LOGIN + "/api/getSkenarioAktif", null, WargamingAPIHelper.enableDebug);

            if (result.data != null)
            {
                try
                {
                    var skenario = SkenarioAktifHelper.FromJson(result.data);
                    new SkenarioAktif(skenario);

                    Debug.Log("Skenario Aktif : " + SkenarioAktif.ID_SKENARIO);
                    Debug.Log("Document Aktif : " + SkenarioAktif.ID_DOCUMENT);

                    return "done";

                }
                catch (Exception e)
                {
                    return "retry";
                }
            }

            return WargamingAPIHelper.checkRequestResult(result.request);
        }

        public static async Task GetCBAll(string id_bagian)
        {
            WWWForm form = new WWWForm();
            if (id_bagian != "0")
            {
                form.AddField("kogas", id_bagian);
            }

            Debug.Log(id_bagian);

            var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_LOGIN + "/api/getCB2D", form, WargamingAPIHelper.enableDebug);

            if(result.data != null)
            {
                try
                {
                    var cb = CBHelper.FromJson(result.data);
                    var checkFirstCB = DataCB.data.Count > 0;

                    for(int i=0; i < cb.Length; i++)
                    {
                        if (checkFirstCB)
                        {
                            if (DataCB.data.FindIndex(x => x.id_user == cb[i].user.id) != -1) continue; ;
                        }

                        var newData = DataCB.AddNewCB(cb[i], (cb[i].user.jenis_user == 1) ? EntityHelper.EntityOpponent.BLUE_FORCE : EntityHelper.EntityOpponent.RED_FORCE);
                    }

                    //for(int i=0; i < cbTerbaik.Length; i++)
                    //{
                    //    if ((cbTerbaik[i].document.type_login == "menu_cb" || i == cbTerbaik.Length - 1) && cbTerbaik[i].user.id == SessionUser.id)
                    //    {
                    //        if (cbTerbaik[i].user.jenis_user == 1)
                    //        {
                    //            new CBSendiri(cbTerbaik[i]);
                    //            SkenarioAktif.ID_DOCUMENT = cbTerbaik[i].document.id.ToString();

                    //            var cbtNow = cbTerbaik[i];
                    //            SessionUser.id_kogas = cbtNow.user.bagian;

                    //            i = cbTerbaik.Length;
                    //        }
                    //        else
                    //        {
                    //            new CBMusuh(cbTerbaik[i]);

                    //            i = cbTerbaik.Length;
                    //        }
                    //    }
                    //}
                }
                catch (Exception e)
                {
                    Debug.LogError("Failed to Get Any CB Data !");
                }
            }
        }

        public static async Task<GetCBTerbaikResult> GetCBTerbaik(string id_bagian)
        {
            WWWForm form = new WWWForm();
            form.AddField("bagian", id_bagian);

            var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_LOGIN + "/api/get/cb/getCB_Terbaik_all", form, WargamingAPIHelper.enableDebug);
            //var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_LOGIN + "/api/getCB2D", form, WargamingAPIHelper.enableDebug);

            Debug.Log(result.data);

            if (result.data != null)
            {
                try
                {
                    var cbTerbaik = CBTerbaikHelper.FromJson(result.data);

                    var newData = DataCB.AddNewCB(cbTerbaik[0], EntityHelper.EntityOpponent.BLUE_FORCE);
                    SkenarioAktif.ID_DOCUMENT = cbTerbaik[0].id_document.ToString();

                    var cbtNow = cbTerbaik[0];
                    SessionUser.id_kogas = cbtNow.id_kogas;

                    if (cbTerbaik.Length > 1)
                        foreach (CBTerbaikHelper item in cbTerbaik)
                        {
                            //if (item.user.jenis_user == 2) new CBMusuh(item);
                            if (item.tipe_cb == "CB Musuh") new CBMusuh(item);
                        }

                    return new GetCBTerbaikResult("done", newData);
                }
                catch (Exception e)
                {
                    return new GetCBTerbaikResult("cb_terbaik_belum_dipilih", null);
                }
            }

            return new GetCBTerbaikResult(WargamingAPIHelper.checkRequestResult(result.request), null);
        }

        public class GetCBTerbaikResult
        {
            public string result;
            public CB data;

            public GetCBTerbaikResult(string _result, CB _data)
            {
                result = _result;
                data = _data;
            }
        }

        /// <summary>
        /// Dapatkan Data Kegiatan / Timetable
        /// </summary>
        /// <param name="skenario">ID Skenario Aktif</param>
        /// <returns></returns>
        public static async Task<string> GetKegiatan(string skenario = null)
        {
            WWWForm form = new WWWForm();

            form.AddField("status", "ambil_kegiatan_tfg");
            form.AddField("id_scenario", (skenario != null) ? skenario : SkenarioAktif.ID_SKENARIO.ToString());

            var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_CB + "/docs/source/source_get.php", form, WargamingAPIHelper.enableDebug);

            if (result.data != null) return result.data;

            return WargamingAPIHelper.checkRequestResult(result.request);
        }

        /// <summary>
        /// Dapatkan Hari-H Skenario Aktif
        /// </summary>
        /// <returns></returns>
        public static async Task<string> GetHariH()
        {
            var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_LOGIN + "/api/getHariHSkenario", null, WargamingAPIHelper.enableDebug);

            if (result.data != null)
            {
                Debug.Log(result.data);
                try
                {
                    SkenarioAktif.HARI_H = DateTime.Parse(result.data.Replace("\"", ""));
                    return "done";

                }
                catch (Exception e)
                {
                    Debug.Log(e.Message.ToString());
                    return "retry";
                }
            }

            return WargamingAPIHelper.checkRequestResult(result.request);
        }

        /// <summary>
        /// Load Data Plotting CB Terbaik dari KOGAS dipillih
        /// </summary>
        /// <returns></returns>
        /// 
        public static async Task<JArray> loadDataCB(long? id_user, long? id_kogas, long? id_scenario, string nama_document)
        {
            //if (!id_kogas.HasValue || !id_kogas.HasValue || !id_scenario.HasValue) return null;
            if (!id_scenario.HasValue) return null;

            WWWForm form = new WWWForm();
            form.AddField("status", "ambil_load_dokumen");
            form.AddField("id_user", id_user.Value.ToString());
            form.AddField("nama_dokumen", nama_document);
            form.AddField("skenario", id_scenario.Value.ToString());
            form.AddField("type", id_kogas.GetValueOrDefault(0) == 0 ? "menu" : "menu_cb");

            var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_CB + "/docs/source/source_get.php", form, WargamingAPIHelper.enableDebug);

            if (result.data == null) return null;

            return JArray.Parse(result.data);
        }

        public static async Task<string> GetDetailSatuan(string ID, string grup)
        {
            switch (grup)
            {
                case "1":
                    grup = "getDetailVehicle";
                    break;
                case "2":
                    grup = "getDetailShip";
                    break;
                case "3":
                    grup = "getDetailAircraft";
                    break;
                case "10":
                    grup = "getDetailPasukan";
                    break;
            }

            var result = await NetworkRequest.GetRequest(ServerConfig.SERVICE_LOGIN + "/api/" + grup + "?id=" + ID);

            if (result.data == null) return null;
            return result.data;
        }

        public static async Task<OpenTopoData> OpenTopoDataRequest(float lng, float lat, string service = "aster30m")
        {
            var result = await NetworkRequest.GetRequest("https://api.opentopodata.org/v1/" + service + "?locations=" + lng + "," + lat, WargamingAPIHelper.enableDebug);
            Debug.Log(result.data);
            if (result.data == null) return null;

            return OpenTopoData.FromJson(result.data);
        }
    }

    namespace Plotting
    {
        public class WargamingAPI : MonoBehaviour
        {
            public static async Task<string> GetListSatuan(string jenis)
            {
                WWWForm form = new WWWForm();

                form.AddField("pasukan_check", "f");
                form.AddField("status", "ambil_object_" + jenis);
                form.AddField("dokumen", (SkenarioAktif.ID_SKENARIO.HasValue) ? SkenarioAktif.ID_SKENARIO.ToString() : null);

                var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_CB + "/docs/source/source_get.php", form, WargamingAPIHelper.enableDebug);

                if (result.data == null) return null;

                return result.data;
            }

            public static async Task<string> GetListRadar()
            {
                WWWForm form = new WWWForm();

                form.AddField("status", "ambil_object_radar");

                var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_CB + "/docs/source/source_get.php", form, WargamingAPIHelper.enableDebug);

                if (result.data == null) return null;

                return result.data;
            }

            public static async Task<string> SavePlotting(string satuan, string tool, List<ObjekTrash> trash = null)
            {
                //Debug.Log(SessionUser.id + "," + CBSendiri.nama_document + "," + "menu_cb" + "," + SkenarioAktif.ID_SKENARIO);

                WWWForm form = new WWWForm();
                form.AddField("status", "simpan");

                form.AddField("document[0]", SessionUser.id.ToString());
                form.AddField("document[1]", DataCB.GetYourCB().nama_document);
                form.AddField("document[2]", "menu_cb");
                form.AddField("document[3]", SkenarioAktif.ID_SKENARIO.ToString());

                form.AddField("satuan", satuan);
                form.AddField("tool", tool);

                if (trash != null)
                {
                    for (int i = 0; i < trash.Count; i++)
                    {
                        form.AddField("trash[" + i + "][value]", trash[i].value);
                        form.AddField("trash[" + i + "][key]", trash[i].key);
                    }
                }

                var result = await NetworkRequest.PostRequest(ServerConfig.SERVICE_CB + "/docs/source/source_post.php", form, WargamingAPIHelper.enableDebug);

                if (result.data == null) return null;

                return result.data;
            }
        }
    }

    public static class WargamingAPIHelper
    {
        public static bool enableDebug = true;

        public static string checkRequestResult(UnityWebRequest.Result result)
        {
            if (result == UnityWebRequest.Result.ConnectionError)
            {
                return "connection_failed";
            }
            else if (result == UnityWebRequest.Result.ProtocolError)
            {
                return "user_failed";
            }
            else
            {
                return "processing_failed";
            }
        }
    }
}