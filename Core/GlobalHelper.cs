using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Serialization;
using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.ArcGISMapsSDK.Utils.Math;
using Esri.GameEngine.Geometry;
using Esri.HPFramework;
using Expanse;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using ProceduralWorlds.HDRPTOD;
using TMPro;
using UnityEngine;
using UnityEngine.UIElements;
using static Wargaming.Core.GlobalParam.DrawingHelper;
using static Wargaming.Core.GlobalParam.EntityHelper;
using static Wargaming.Core.GlobalParam.SpeedHelper;

namespace Wargaming.Core.GlobalParam
{
    public static class GlobalHelper
    {
        public static void ExitApp() { Application.Quit(); }

        [Serializable]
        public class MissionWalkerParam
        {
            public string time;
            public DateTime datetime;
            public Vector3 coordinate;
        }
    }

    public static class SpeedHelper
    {
        public enum speedType { kmph = 0, mph = 1, knot = 2, mach = 3 }

        public static double GetKecepatanByType(string speed, string speed_type)
        {
            switch (speed_type)
            {
                case "km":
                    return double.Parse(speed);
                case "knot":
                    return double.Parse(speed) * 1.852;
                case "mach":
                    return double.Parse(speed) * 1192.68f;
                default:
                    return double.Parse(speed);
            }
        }

        public static speedType getSpeedType(string type)
        {
            switch (type)
            {
                case "km":
                    return speedType.kmph;
                case "m":
                    return speedType.mph;
                case "knot":
                    return speedType.knot;
                case "mach":
                    return speedType.mach;
                default:
                    return speedType.kmph;
            }
        }
    }

    public static class NumberConvertionHelper
    {
        public static long ConvertToLong(string value)
        {
            long data = 0;
            long.TryParse(value, out data);

            return data;
        }

        public static double ConvertToDouble(string value)
        {
            double data = 0;
            double.TryParse(value, out data);

            return data;
        }

        public static float ConvertToFloat(string value)
        {
            float data = 0;
            float.TryParse(value, out data);

            return data;
        }

        public static int ConvertToInt(string value)
        {
            int data = 0;
            int.TryParse(value, out data);

            return data;
        }
    }

    public static class TransformHelper
    {

        public static void SetHPTransformPosition(this HPTransform _transform, double? x = null, double? y = null, double? z = null)
        {
            _transform.LocalPosition = 
                new Unity.Mathematics.double3(
                    x.HasValue ? x.Value : _transform.LocalPosition.x,
                    y.HasValue ? y.Value : _transform.LocalPosition.y,
                    z.HasValue ? z.Value : _transform.LocalPosition.z
                );
        }

        public static void SetHPTransformPosition(this HPTransform _transform, float? x = null, float? y = null, float? z = null)
        {
            _transform.SetHPTransformPosition((double?) x, (double?) y, (double?) z);
        }

        public static void SetX(this Vector3 _transform, float x) { _transform.x = x; }
        public static void SetY(this Vector3 _transform, float y) { _transform.y = y; }
        public static void SetZ(this Vector3 _transform, float z) { _transform.z = z; }

        public static void ToLocationComponent(this HPTransform _hpTransform, out ArcGISPoint location, out ArcGISRotation rotation)
        {
            var universePosition = _hpTransform.UniversePosition;
            var universeRotation = _hpTransform.UniverseRotation;

            var cartesianPosition = universePosition;
            var cartesianRotation = universeRotation.ToQuaterniond();

            var convertedPosition = EnvironmentsController.GetMapComponent().View.WorldToGeographic(cartesianPosition);
            location = GeoUtils.ProjectToSpatialReference(convertedPosition, ArcGISSpatialReference.WGS84());

            rotation = GeoUtils.FromCartesianRotation(cartesianPosition, cartesianRotation, EnvironmentsController.GetMapComponent().View.SpatialReference, EnvironmentsController.GetMapComponent().View.Map.MapType);
        }
    }

    public static class WeatherHelper
    {
        [Serializable]
        public class WeatherPreset
        {
            public string name;
            public HDRPTimeOfDayWeatherProfile profile;
            public bool overrideWeatherParticle;
            public float particleCount;
            public bool overrideCloudPreset;
            //public string cloudPreset;
            public UniversalCloudLayer cloudPreset;
            public bool overrideCloudDensity;
            public float cloudDensity;
            public bool overrideFog;
            public float fogDensity;
            public float fogRadius;
            public float fogThickness;
        }
    }

    public static class EntityHelper
    {
        public enum JenisEntity { LAND, SEA, AIR }
        public enum KategoriSatuan { VEHICLE, SHIP, SUBMARINE, AIRCRAFT, HELICOPTER, INFANTRY }
        public enum JenisKendaraan { DEFAULT, TRACKS, SURFACE, SUB, JET, PROPS, HELICOPTER }
        public enum JenisRadar { BIASA = 0, ARHANUD = 1 }
        public enum JenisWeapon { GUN, MISSILE }
        public enum EntityOpponent { BLUE_FORCE, RED_FORCE, CIVILIAN, NEUTRAL } 

        public static TMP_FontAsset getFontTaktis(string font_name)
        {
            return Resources.Load<TMP_FontAsset>(AssetPackageController.Instance.simbolTaktisLocation + font_name);
        }

        public static Color getColorByUserType()
        {
            return getColorByUserType(SessionUser.nama_asisten);
        }

        public static Color getColorByUserType(string user)
        {
            if(user != null) user = user.ToLower();

            switch (user)
            {
                case "asops":
                    return Color.blue;
                case "asintel":
                    return Color.red;
                case "wasdal":
                    return Color.blue;
                default:
                    return Color.black;
            }
        }
    }

    public static class DrawingHelper
    {
        public enum DrawingType { POLYLINE, POLYLINE_CURVE, POLYGON, RECTANGLE, CIRCLE, ELLIPSE, ARROW, MANOUVER }
        public enum DrawingArrowType { UNDEFINED, ARROW, ARROW_UP, ARROW_DOWN }
        public enum DrawingManouverType { UNDEFINED, MANOUVER_UP, MANOUVER_DOWN }
    }

    public partial class ServerConfigHelper
    {
        public int? active_index { get; set; }
        public string server_name { get; set; }
        public string service_login { get; set; }
        public string service_cb { get; set; }
        public string server_colyseus { get; set; }
    }

    public partial class OpenTopoData
    {
        public List<OpenTopoDataResult> results { get; set; }
        public static OpenTopoData FromJson(string json) => JsonConvert.DeserializeObject<OpenTopoData>(json, HelperConverter.Converter.Settings);
    }

    public class OpenTopoDataResult
    {
        public string dataset { get; set; }
        public float elevation { get; set; }
        public OpenTopoDataLocation location { get; set; }
    }

    public class OpenTopoDataLocation
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    //[Serializable]
    //public class BundleConfig
    //{
    //    [XmlAttribute("name")]
    //    public string name { get; set; }
    //    [XmlElement("variant")]
    //    public string variant { get; set; }
    //}

    [Serializable]
    public class StartupConfigData
    {
        [XmlElement("DEBUG_VERSION")]
        public bool? DEBUG_VERSION { get; set; }
        [XmlElement("TIME_DIFF")]
        public float? TIME_DIFF { get; set; }
        [XmlElement("DEFAULT_OCEAN_HEIGHT")]
        public int? DEFAULT_OCEAN_HEIGHT { get; set; }
        [XmlElement("OCEAN_MAX_ALTITUDE")]
        public float? OCEAN_MAX_ALTITUDE { get; set; }
        [XmlElement("DEFAULT_CLOUDS_HEIGHT")]
        public float? DEFAULT_CLOUDS_HEIGHT { get; set; }
        [XmlElement("CLOUDS_MAX_ALTITUDE")]
        public float? CLOUDS_MAX_ALTITUDE { get; set; }
        [XmlElement("FOAM_MAX_ALTITUDE")]
        public float? FOAM_MAX_ALTITUDE { get; set; }
        [XmlElement("CLOUDS_QUALITY")]
        public int? CLOUDS_QUALITY { get; set; }
        [XmlElement("CLOUDS_REPROJECTION_QUALITY")]
        public int? CLOUDS_REPROJECTION_QUALITY { get; set; }
        [XmlElement("REBASE_MAX_POSITION")]
        public float? REBASE_MAX_POSITION { get; set; }
        [XmlElement("REBASE_MIN_ALTITUDE")]
        public float? REBASE_MIN_ALTITUDE { get; set; }
        [XmlElement("DEFAULT_AIRCRAFT_SCALE")]
        public float? DEFAULT_AIRCRAFT_SCALE { get; set; }
        [XmlElement("DEFAULT_AIRCRAFT_ALTITUDE")]
        public float? DEFAULT_AIRCRAFT_ALTITUDE { get; set; }
        [XmlElement("MAX_AIRCRAFT_ALTITUDE")]
        public float? MAX_AIRCRAFT_ALTITUDE { get; set; }
        [XmlElement("DEFAULT_SHIP_SCALE")]
        public float? DEFAULT_SHIP_SCALE { get; set; }
        [XmlElement("DEFAULT_VEHICLE_SCALE")]
        public float? DEFAULT_VEHICLE_SCALE { get; set; }
        [XmlElement("AIRCRAFT_DEF_TAKEOFF_LANDING")]
        public bool AIRCRAFT_DEF_TAKEOFF_LANDING { get; set; }
        [XmlElement("HELI_DEF_TAKEOFF_LANDING")]
        public bool HELI_DEF_TAKEOFF_LANDING { get; set; }
        [XmlElement("ENABLE_ASSET_PACKAGE")]
        public bool ENABLE_ASSET_PACKAGE { get; set; }
        [XmlElement("ENABLE_DATAPACK")]
        public bool ENABLE_DATAPACK { get; set; }
        [XmlElement("ENABLE_LOAD_SATUAN")]
        public bool ENABLE_LOAD_SATUAN { get; set; }
        [XmlElement("ENABLE_LOAD_PASUKAN")]
        public bool ENABLE_LOAD_PASUKAN { get; set; }
        [XmlElement("ENABLE_LOAD_DRAWING")]
        public bool ENABLE_LOAD_DRAWING { get; set; }
        [XmlElement("ENABLE_LOAD_RADAR")]
        public bool ENABLE_LOAD_RADAR { get; set; }
        [XmlElement("ENABLE_LOAD_KEKUATAN")]
        public bool ENABLE_LOAD_KEKUATAN { get; set; }
        [XmlElement("ENABLE_LOAD_SITUASI")]
        public bool ENABLE_LOAD_SITUASI { get; set; }
        [XmlElement("ENABLE_LOAD_BUNGUS")]
        public bool ENABLE_LOAD_BUNGUS { get; set; }
        
        [XmlElement("ENABLE_LOAD_ANIMATION")]
        public bool ENABLE_LOAD_ANIMATION { get; set; }
        [XmlElement("ENABLE_BASEMAPS")]
        public bool ENABLE_BASEMAPS { get; set; }
        [XmlElement("ENABLE_LAYERS")]
        public bool ENABLE_LAYERS { get; set; }
        [XmlElement("SERVER_TFG")]
        public string SERVER_TFG { get; set; }
        [XmlElement("SERVER_TFG_SINGLE")]
        public string SERVER_TFG_SINGLE { get; set; }
        [XmlElement("SERVER_WGS")]
        public string SERVER_WGS { get; set; }
    }

    [Serializable]
    public class ThemeImageConfig
    {
        //[XmlElement("ICON")]
        //public Vector3 iconImg { get; set; }
        //[XmlElement("BG")]
        //public Vector3 bgImg { get; set; }
        //[XmlElement("ICON_S")]
        //public Vector3 icon_SImg { get; set; }
        //public bool Active { get; set; }

        [XmlElement("image")]
        public string image { get; set; }
        [XmlIgnore]
        public Texture2D imageData { get; set; }
        [XmlElement("size")]
        public Vector2 size { get; set; }
    }

    [Serializable]
    public class ServiceData
    {
        [XmlAttribute("name")]
        public string ServiceName { get; set; }
        [XmlElement("ServiceLogin")]
        public string ServiceLogin { get; set; }
        [XmlElement("ServiceLoginPort")]
        public string ServiceLoginPort { get; set; }
        [XmlElement("ServiceCB")]
        public string ServiceCB { get; set; }
        [XmlElement("ServiceCBPort")]
        public string ServiceCBPort { get; set; }
        [XmlElement("ServerColyseus")]
        public string ServerColyseus { get; set; }
        [XmlElement("ServerColyseusPort")]
        public string ServerColyseusPort { get; set; }
        [XmlElement("Active")]
        public bool Active { get; set; }
    }

    [Serializable]
    public class AppSettingsData
    {
        [XmlAttribute("monitor")]
        public string monitor { get; set; }
        [XmlElement("resolution")]
        public string resolution { get; set; }
        [XmlElement("display")]
        public int display { get; set; }
        [XmlElement("vsync")]
        public int vsync { get; set; }
        [XmlElement("anti_aliasing")]
        public int anti_aliasing { get; set; }
        [XmlElement("anisotropic_filtering")]
        public int anisotropic_filtering { get; set; }
    }

    namespace HelperSessionUser
    {
        public partial class SessionUserHelper
        {
            public string username { get; set; }
            public long? id { get; set; }
            public string name { get; set; }
            public JenisUser jenis_user { get; set; }
            public Bagian bagian { get; set; }
            public Jabatan jabatan { get; set; }
            public Asisten asisten { get; set; }
            public long? atasan { get; set; }
            public long? status_login { get; set; }

            public static SessionUserHelper[] FromJson(string json) => JsonConvert.DeserializeObject<SessionUserHelper[]>(json, HelperConverter.Converter.Settings);
        }

        public partial class JenisUser
        {
            public long ID { get; set; }
            public string jenis_user { get; set; }
        }

        public partial class Bagian
        {
            public long ID { get; set; }
            public string nama_bagian { get; set; }
        }

        public partial class Jabatan
        {
            public long ID { get; set; }
            public string nama_jabatan { get; set; }
        }

        public partial class Asisten
        {
            public long ID { get; set; }
            public string nama_asisten { get; set; }
        }
    }

    namespace HelperScenarioAktif
    {
        public partial class SkenarioAktifHelper
        {
            public long? ID { get; set; }
            public string nama_skenario { get; set; }

            public static SkenarioAktifHelper FromJson(string json) => JsonConvert.DeserializeObject<SkenarioAktifHelper>(json, HelperConverter.Converter.Settings);
        }

        public partial class CBHelper
        {
            public CBDocumentHelper document { get; set; }
            public CBUserHelper user { get; set; }

            public static CBHelper[] FromJson(string json) => JsonConvert.DeserializeObject<CBHelper[]>(json, HelperConverter.Converter.Settings);
        }

        public partial class CBDocumentHelper
        {
            public long? id { get; set; }
            public long? id_user { get; set; }
            public string nama_dokumen { get; set; }
            public string type_login { get; set; }
            public long? scenario { get; set; }
            public string layer { get; set; }
        }

        public partial class CBUserHelper
        {
            public long? id { get; set; }
            public string name { get; set; }
            public long? jenis_user { get; set; }
            public long? bagian { get; set; }
        }

        public partial class CBTerbaikHelper
        {
            public long? id_cb { get; set; }
            public long? id_cb_eppkm { get; set; }
            public long? id_kogas { get; set; }
            public long? id_user { get; set; }
            public string nama_kogas { get; set; }
            public string tipe_cb { get; set; }
            public int? id_document { get; set; }
            public string nama_document { get; set; }
            public string hari { get; set; }

            public static CBTerbaikHelper[] FromJson(string json) => JsonConvert.DeserializeObject<CBTerbaikHelper[]>(json, HelperConverter.Converter.Settings);
        }

        public partial class DocumentActiveHelper
        {
            public string id { get; set; }
            public static DocumentActiveHelper FromJson(string json) => JsonConvert.DeserializeObject<DocumentActiveHelper>(json, HelperConverter.Converter.Settings);
        }
    }

    namespace HelperKegiatan
    {
        public partial class TimetableKegiatanHelper
        {
            public string id_kegiatan { get; set; }
            public string id_satuan { get; set; }
            public string id_user { get; set; }
            public string kegiatan { get; set; }
            public string keterangan { get; set; }
            //public DateTime waktu { get; set; }
            public string waktu { get; set; }
            public DateTime waktuDT { get; set; }
            //public DateTime waktu { get; set; }   
            public string x { get; set; }
            public string y { get; set; }
            public string kogas { get; set; }
            public string waypoint { get; set; }
            public bool detail { get; set; }
            public string nama_object { get; set; }
            public string type_kegiatan { get; set; }
            public string url_video { get; set; }
            public string percepatan { get; set; }

            public static TimetableKegiatanHelper[] FromJson(string json) => JsonConvert.DeserializeObject<TimetableKegiatanHelper[]>(json, HelperConverter.Converter.Settings);
        }
    }

    namespace HelperColyseusTFG
    {
        public partial class FocusToObject
        {
            public string id { get; set; }
            public string id_satuan { get; set; }
            public string id_video { get; set; }
        }

        public partial class JumpDateObj
        {
            public string changeTime { get; set; }
        }

        public partial class ResyncPosition
        {
            public string id_satuan { get; set; }
            public double lat { get; set; }
            public double lng { get; set; }
            public double heading { get; set; }
        }

        public partial class ZoomToObject
        {
            public string id { get; set; }
        }

        public partial class SendPenerjunan
        {
            public string id_induk { get; set; }
            public string startTime { get; set; }
            //public DateTime time { get; set; }
            public float lat { get; set; }
            public float lng { get; set; }
            public string objek { get; set; }
            public SendPenerjunanObject[] obj_list { get; set; }
            public static string ToString(SendPenerjunan json) => JsonConvert.SerializeObject(json);

        }

        public partial class SendPenerjunanObject
        {
            public string id { get; set; }
            public string name { get; set; }
            public string matra_pasukan { get; set; }
            public int? pasukan_sym_id { get; set; }
            public float? speed_pasukan { get; set; }
            public float? health_pasukan { get; set; }
            public int? jumlah_bintara { get; set; }
            public int? jumlah_tamtama { get; set; }
            public int? jumlah_perwira { get; set; }
            public string icon_wgs { get; set; }
            public string id_wgs { get; set; }
            public string style_symbol { get; set; }
            public string deskripsi { get; set; }
            public string titan { get; set; }
            [JsonProperty("nama")]
            public string font_taktis { get; set; }
            [JsonProperty("index")]
            public int? font_ascii { get; set; }
            public string keterangan { get; set; }
            public string grup { get; set; }

            public static string ToString(SendPenerjunanObject[] json) => JsonConvert.SerializeObject(json);
            public static string ToString(SendPenerjunanObject json) => JsonConvert.SerializeObject(json);
            public static SendPenerjunanObject[] FromJson(JToken json) => JsonConvert.DeserializeObject<SendPenerjunanObject[]>(json.ToString(), HelperConverter.Converter.Settings);
        }
    }

    namespace HelperPlotting
    {
        #region Entity Document
        public partial class EntityDocument
        {
            [JsonProperty("id")]
            public string id_document { get; set; }
            public static EntityDocument FromJson(JToken json) => JsonConvert.DeserializeObject<EntityDocument>(json.ToString(), HelperConverter.Converter.Settings);
        }
        #endregion

        #region Plotting Trash
        public partial class ObjekTrash
        {
            public string value;
            public string key;

            public static string ToString(List<ObjekTrash> json) => JsonConvert.SerializeObject(json);
        }
        #endregion

        #region USED ON SOME ENTITY TYPE
        [Serializable]
        public partial class ObjekSymbol
        {
            [JsonProperty("width")]
            private string _width { get { return width.ToString(); } set { int.TryParse(value, out width); } }
            [JsonIgnore]
            public int width;

            [JsonProperty("color")]
            private string _color
            {
                get
                {
                    string color = ColorUtility.ToHtmlStringRGBA(warna);

                    return color;
                }
                set
                {
                    Color converted;
                    if (value == "blue")
                    {
                        // IF Warna == blue
                        ColorUtility.TryParseHtmlString("#81E0FF96", out converted);
                    }
                    else if (value == "red")
                    {
                        // IF Warna == Red
                        ColorUtility.TryParseHtmlString("#FF808196", out converted);
                    }
                    else
                    {
                        if (!ColorUtility.TryParseHtmlString(value, out converted))
                        {
                            converted = Color.black;
                        }
                    }

                    warna = converted;
                }
            }
            [JsonIgnore]
            public Color warna;

            [JsonProperty("fontFamily")]
            private string _fontFamily { get { return fontFamily; } set { fontFamily = value; } }
            [JsonIgnore]
            public string fontFamily;

            [JsonProperty("fontIndex")]
            private string _fontIndex { get { return fontIndex; } set { fontIndex = value; } }
            [JsonIgnore]
            public string fontIndex;

            public static ObjekSymbol FromJson(string json) => JsonConvert.DeserializeObject<ObjekSymbol>(json, HelperConverter.Converter.Settings);
            public static string ToString(ObjekSymbol json) => JsonConvert.SerializeObject(json);
        }

        [Serializable]
        public partial class ObjekWeapon
        {
            [JsonProperty("index")]
            private int _index { get { return index; } set { index = value; } }
            [JsonIgnore]
            public int index;

            [JsonProperty("id")]
            private string _id { get { return id.ToString(); } set { int.TryParse(value, out id); } }
            [JsonIgnore]
            public int id;

            [JsonProperty("jenis")]
            private string _jenis
            {
                get
                {
                    switch (jenis)
                    {
                        case JenisWeapon.GUN:
                            return "gun";
                        case JenisWeapon.MISSILE:
                            return "missile";
                        default: return "gun";
                    }
                }
                set
                {
                    switch (value.ToLower())
                    {
                        case "gun":
                            jenis = JenisWeapon.GUN;
                            break;
                        case "missile":
                            jenis = JenisWeapon.MISSILE;
                            break;
                        default:

                            break;
                    }
                }
            }
            [JsonIgnore]
            public JenisWeapon jenis;
        }
        #endregion

        #region Entity Satuan
        public partial class EntitySatuan
        {
            [JsonProperty("id")]
            public string id_entity { get; set; }
            public string id_user { get; set; }
            public string id_symbol { get; set; }
            public string dokumen { get; set; }
            [JsonProperty("nama")]
            public string id_satuan { get; set; }
            [JsonProperty("lat_y")]
            public float lat { get; set; }
            [JsonProperty("lng_x")]
            public float lng { get; set; }
            [JsonProperty("style")]
            private string _styleString { set { data_style = EntitySatuanStyle.FromJson(value); } }
            public EntitySatuanStyle data_style { get; set; }
            [JsonProperty("info")]
            private string _infoString { set { data_info = EntitySatuanInfo.FromJson(value); } }
            public EntitySatuanInfo data_info { get; set; }
            public string id_kegiatan { get; set; }
            public string isi_logistik { get; set; }

            // CUSTOM DATA
            public string tipe_tni { get; set; }
            [JsonProperty("jenis")]
            public KategoriSatuan kategori { get; set; }
            public string path_object_3d { get; set; }
            public string object3D { get; set; }

            public static string ToString(EntitySatuan json) => JsonConvert.SerializeObject(json);
            public static EntitySatuan FromJson(JToken json) => JsonConvert.DeserializeObject<EntitySatuan>(json.ToString(), HelperConverter.Converter.Settings);
        }

        public partial class ObjekSatuanHelper
        {
            //[JsonProperty("id")]
            //public long? id;
            [JsonProperty("id_user")]
            public long? id_user;
            [JsonProperty("dokumen")]
            public long? dokumen;
            [JsonProperty("nama")]
            public string nama;

            [JsonProperty("lat_y")]
            public double lat_y;
            [JsonProperty("lng_x")]
            public double lng_x;

            [JsonProperty("id_symbol")]
            public long symbolID;

            [JsonProperty("style")]
            private string _style { get { return ObjekSatuanStyle.ToString(style); } set { style = ObjekSatuanStyle.FromJson(value); } }
            [JsonIgnore]
            public ObjekSatuanStyle style;

            [JsonIgnore]
            public ObjekSymbol symbol;
            //private string _symbol { get { return ObjekSatuanSymbol.ToString(symbol); } set { symbol = ObjekSatuanSymbol.FromJson(value); } }
            //[JsonIgnore]
            //public string symbol;

            [JsonProperty("info")]
            private string _info { get { return ObjekSatuanInfo.ToString(info); } set { info = ObjekSatuanInfo.FromJson(value); } }
            [JsonIgnore]
            public ObjekSatuanInfo info;

            [JsonProperty("id_kegiatan")]
            public string id_kegiatan;
            [JsonProperty("isi_logistik")]
            public string isi_logistik;

            // CUSTOM DATA
            [JsonIgnore]
            public string tipe_tni { get; set; }
            [JsonProperty("jenis")]
            public KategoriSatuan kategori { get; set; }
            [JsonIgnore]
            public string path_object_3d { get; set; }
            [JsonIgnore]
            public string object3D { get; set; }

            public static string ToString(ObjekSatuanHelper json) => JsonConvert.SerializeObject(json);
            public static string ToString(ObjekSatuanHelper[] json) => JsonConvert.SerializeObject(json);
            public static ObjekSatuanHelper FromJson(JToken json) => JsonConvert.DeserializeObject<ObjekSatuanHelper>(json.ToString(), HelperConverter.Converter.Settings);
        }

        [Serializable]
        public partial class ObjekSatuanStyle
        {
            [JsonProperty("nama")]
            private string _nama {get { return fontTaktis; } set { fontTaktis = value; } }
            [JsonIgnore]
            public string fontTaktis;

            [JsonProperty("index")]
            private string _index { get { return fontIndex.ToString(); } set { long.TryParse(value, out fontIndex); } }
            [JsonIgnore]
            public long fontIndex;

            [JsonProperty("keterangan")]
            private string _keterangan { get { return keterangan; } set { keterangan = value; } }
            [JsonIgnore]
            public string keterangan;

            [JsonProperty("grup")]
            private string _grup { get { return grup.ToString(); } set { long.TryParse(value, out grup); } }
            [JsonIgnore]
            public long grup;

            [JsonProperty("entity_name")]
            private string _entity_name { get { return entityName; } set { entityName = value; } }
            [JsonIgnore]
            public string entityName;

            [JsonProperty("entity_kategori")]
            private string _entity_kategori { get { return entityKategori; } set { entityKategori = value; } }
            [JsonIgnore]
            public string entityKategori;

            [JsonProperty("jenis_role")]
            private string _jenis_role { get { return jenisRole; } set { jenisRole = value; } }
            [JsonIgnore]
            public string jenisRole;

            [JsonProperty("label_style")]
            private string _label_style { get { return labelStyle; } set { labelStyle = value; } }
            [JsonIgnore]
            public string labelStyle;

            public static ObjekSatuanStyle FromJson(string json) => JsonConvert.DeserializeObject<ObjekSatuanStyle>(json, HelperConverter.Converter.Settings);
            public static string ToString(ObjekSatuanStyle json) => JsonConvert.SerializeObject(json);
        }

        [Serializable]
        public partial class ObjekSatuanInfo
        {
            [JsonProperty("kecepatan")]
            private string _kecepatan
            {
                get { return kecepatan.ToString() + "|" + jenisKecepatan.ToString(); }
                set
                {
                    var speedSplit = value.Split("|");
                    double tempKecepatan;
                    double.TryParse(speedSplit[0], out tempKecepatan);

                    if (speedSplit.Length <= 0)
                    {
                        kecepatan = GetKecepatanByType(tempKecepatan.ToString(), "km");
                    }
                    else
                    {
                        kecepatan = GetKecepatanByType(tempKecepatan.ToString(), speedSplit[1]);
                    }
                }
            }
            [JsonIgnore]
            public double kecepatan;
            [JsonIgnore]
            public speedType jenisKecepatan;

            [JsonProperty("nomer_satuan")]
            private string _nomer_satuan { get { return nomerSatuan; } set { nomerSatuan = value; } }
            [JsonIgnore]
            public string nomerSatuan;

            [JsonProperty("nama_satuan")]
            private string _nama_satuan { get { return namaSatuan; } set { namaSatuan = value; } }
            [JsonIgnore]
            public string namaSatuan;

            [JsonProperty("nomer_atasan")]
            private string _nomer_atasan { get { return nomerAtasan; } set { nomerAtasan = value; } }
            [JsonIgnore]
            public string nomerAtasan;

            [JsonProperty("tgl_selesai")]
            private string _tgl_selesai { get { return tanggalSelesai; } set { tanggalSelesai = value; } }
            [JsonIgnore]
            public string tanggalSelesai;


            [JsonProperty("warna")]
            public string _warna
            {
                get
                {
                    string color = ColorUtility.ToHtmlStringRGBA(warna);

                    if(color == "81E0FF96")
                    {
                        color = "blue";
                    }else if(color == "blue")
                    {
                        color = "red";
                    }
                    else
                    {
                        color = "#" + color;
                    }

                    return color;
                }
                set
                {
                    Color converted;
                    if(value == "blue")
                    {
                        // IF Warna == blue
                        ColorUtility.TryParseHtmlString("#81E0FF96", out converted);
                    }
                    else if(value == "red")
                    {
                        // IF Warna == Red
                        ColorUtility.TryParseHtmlString("#FF808196", out converted);
                    }
                    else
                    {
                        if(!ColorUtility.TryParseHtmlString(value, out converted))
                        {
                            converted = Color.black;
                        }
                    }

                    warna = converted;
                }
            }
            [JsonIgnore]
            public Color warna;

            [JsonProperty("size")]
            private string _size { get { return size.ToString(); } set { float.TryParse(value, out size); } }
            [JsonIgnore]
            public float size;

            //private string _weapon { get { return weapon; } set { weapon = value; } }
            //[JsonIgnore]
            //public string weapon;

            [JsonProperty("weapon")]
            public string weapon { get; set; }

            //private string _weapon { get { return ObjekSatuanWeapon.ToString(list_weapon); } set { list_weapon = ObjekSatuanWeapon.FromJson(value); } }
            //[JsonIgnore]
            //public List<List<ObjekSatuanWeapon>> list_weapon;

            //private string _weapon { get { return ObjekSatuanWeapon.ToString(list_weapon); } set { list_weapon = ObjekSatuanWeapon.FromJson(value); } }
            //[JsonIgnore]
            //public ObjekSatuanWeapon[] list_weapon;

            public List<object> waypoint { get; set; }
            public List<object> list_embarkasi { get; set; }

            [JsonProperty("armor")]
            private string _armor { get { return armor.ToString(); } set { float.TryParse(value, out armor); } }
            [JsonIgnore]
            public float armor;

            [JsonProperty("id_dislokasi")]
            private bool _id_dislokasi { get { return idDislokasi; } set { idDislokasi = value; } }
            [JsonIgnore]
            public bool idDislokasi;

            [JsonProperty("id_dislokasi_obj")]
            private bool _id_dislokasi_obj { get { return idDislokasiObj; } set { idDislokasiObj = value; } }
            [JsonIgnore]
            public bool idDislokasiObj;

            [JsonProperty("bahan_bakar")]
            private string _bahan_bakar { get { return bahanBakar.ToString(); } set { double.TryParse(value, out bahanBakar); } }
            [JsonIgnore]
            public double bahanBakar;

            [JsonProperty("bahan_bakar_load")]
            private string _bahan_bakar_load { get { return bahanBakarLoad.ToString(); } set { double.TryParse(value, out bahanBakarLoad); } }
            [JsonIgnore]
            public double bahanBakarLoad;

            [JsonProperty("kecepatan_maks")]
            private string _kecepatan_maks { get { return kecepatanMax.ToString(); } set { double.TryParse(value, out kecepatanMax); } }
            [JsonIgnore]
            public double kecepatanMax;

            [JsonProperty("heading")]
            private string _heading { get { return heading.ToString(); } set { double.TryParse(value, out heading); } }
            [JsonIgnore]
            public double heading;

            [JsonProperty("ket_satuan")]
            private string _ket_satuan { get { return keterangan; } set { keterangan = value; } }
            [JsonIgnore]
            public string keterangan;

            [JsonProperty("nama_icon_satuan")]
            private string _nama_icon_satuan { get { return namaIconSatuan; } set { namaIconSatuan = value; } }
            [JsonIgnore]
            public string namaIconSatuan;

            [JsonProperty("width_icon_satuan")]
            private string _width_icon_satuan { get { return widthIconSatuan; } set { widthIconSatuan = value; } }
            [JsonIgnore]
            public string widthIconSatuan;

            [JsonProperty("height_icon_satuan")]
            private string _height_icon_satuan { get { return heightIconSatuan; } set { heightIconSatuan = value; } }
            [JsonIgnore]
            public string heightIconSatuan;

            [JsonProperty("personil")]
            private string _personil { get { return personil.ToString(); } set { int.TryParse(value, out personil); } }
            [JsonIgnore]
            public int personil;

            [JsonProperty("detail_personil")]
            private ObjekSatuanPersonil _detail_personil { get { return detailPersonil; } set { detailPersonil = value; } }
            [JsonIgnore]
            public ObjekSatuanPersonil detailPersonil;

            [JsonProperty("tfg")]
            private bool _tfg { get { return useInTfg; } set { useInTfg = value; } }
            [JsonIgnore]
            public bool useInTfg;

            [JsonProperty("hidewgs")]
            private bool _hidewgs { get { return hideInWgs; } set { hideInWgs = value; } }
            [JsonIgnore]
            public bool hideInWgs;


            public static ObjekSatuanInfo FromJson(string json) => JsonConvert.DeserializeObject<ObjekSatuanInfo>(json, HelperConverter.Converter.Settings);
            public static string ToString(ObjekSatuanInfo json) => JsonConvert.SerializeObject(json);
        }

        [Serializable]
        public class ObjekSatuanPersonil
        {
            public int bintara;
            public int tamtama;
            public int perwira;
        }


        //[Serializable]
        //public partial class ObjekSatuanSymbol
        //{
        //    [JsonProperty("width")]
        //    private string _width { get { return width.ToString(); } set { int.TryParse(value, out width); } }
        //    [JsonIgnore]
        //    public int width;

        //    [JsonProperty("color")]
        //    private string _color
        //    {
        //        get
        //        {
        //            string color = ColorUtility.ToHtmlStringRGBA(warna);

        //            return color;
        //        }
        //        set
        //        {
        //            Color converted;
        //            if (value == "blue")
        //            {
        //                // IF Warna == blue
        //                ColorUtility.TryParseHtmlString("#81E0FF96", out converted);
        //            }
        //            else if (value == "red")
        //            {
        //                // IF Warna == Red
        //                ColorUtility.TryParseHtmlString("#FF808196", out converted);
        //            }
        //            else
        //            {
        //                if (!ColorUtility.TryParseHtmlString(value, out converted))
        //                {
        //                    converted = Color.black;
        //                }
        //            }

        //            warna = converted;
        //        }
        //    }
        //    [JsonIgnore]
        //    public Color warna;

        //    [JsonProperty("fontFamily")]
        //    private string _fontFamily { get { return fontFamily; } set { fontFamily = value; } }
        //    [JsonIgnore]
        //    public string fontFamily;

        //    [JsonProperty("fontIndex")]
        //    private string _fontIndex { get { return fontIndex; } set { fontIndex = value; } }
        //    [JsonIgnore]
        //    public string fontIndex;

        //    public static ObjekSatuanSymbol FromJson(string json) => JsonConvert.DeserializeObject<ObjekSatuanSymbol>(json, HelperConverter.Converter.Settings);
        //    public static string ToString(ObjekSatuanSymbol json) => JsonConvert.SerializeObject(json);
        //}

        [Serializable]
        public partial class ObjekSatuanWeapon
        {
            [JsonProperty("index")]
            public string index { get; set; }

            [JsonProperty("id")]
            public string id { get; set; }

            [JsonProperty("jenis")]
            public string jenis { get; set; }
            public static List<List<ObjekSatuanWeapon>> FromJson(string json) => JsonConvert.DeserializeObject<List<List<ObjekSatuanWeapon>>>(json, HelperConverter.Converter.Settings);
            public static List<ObjekSatuanWeapon> FromJson2(string json) => JsonConvert.DeserializeObject<List<ObjekSatuanWeapon>>(json, HelperConverter.Converter.Settings);
            public static string ToString(ObjekSatuanWeapon json) => JsonConvert.SerializeObject(json);
            public static string ToString(List<List<ObjekSatuanWeapon>> json) => JsonConvert.SerializeObject(json);
        }



        public partial class EntitySatuanStyle
        {
            [JsonProperty("nama")]
            public string font_taktis { get; set; }
            [JsonProperty("index")]
            public int? font_ascii { get; set; }
            public string grup { get; set; }
            public string label_style { get; set; }
            public string keterangan { get; set; }

            public static EntitySatuanStyle FromJson(string json) => JsonConvert.DeserializeObject<EntitySatuanStyle>(json, HelperConverter.Converter.Settings);
            public static string ToString(EntitySatuanStyle json) => JsonConvert.SerializeObject(json);
        }

        public partial class EntitySatuanInfo
        {
            [JsonProperty("kecepatan")]
            private string kecepatanString { set { jenis_kecepatan = (value.Split("|").Length > 1) ? value.Split("|")[1] : null; kecepatan_satuan = (value.Split("|").Length >= 1) ? float.Parse(value.Split("|")[0]) : float.Parse(value); } }
            public float? kecepatan_satuan { get; set; }
            public string jenis_kecepatan { get; set; }
            public string nomer_satuan { get; set; }
            public string nama_satuan { get; set; }
            public string nomer_atasan { get; set; }
            public string tgl_mulai { get; set; }
            public string tgl_selesai { get; set; }
            public string warna { get; set; }
            public string size { get; set; }
            //public string weapon { get; set; }
            //public List<object> waypoint { get; set; }
            //public List<object> list_embarkasi { get; set; }
            public int? armor { get; set; }
            public bool id_dislokasi { get; set; }
            public bool id_dislokasi_obj { get; set; }
            public string bahan_bakar { get; set; }
            public string kecepatan_maks { get; set; }
            public string heading { get; set; }
            public string ket_satuan { get; set; }
            public string nama_icon_satuan { get; set; }
            public string width_icon_satuan { get; set; }
            public string height_icon_satuan { get; set; }
            public int? personil { get; set; }
            public bool radarList { get; set; }
            public bool sonarList { get; set; }
            public bool tfg { get; set; }
            public string ketinggian { get; set; }

            public static EntitySatuanInfo FromJson(string json) => JsonConvert.DeserializeObject<EntitySatuanInfo>(json, HelperConverter.Converter.Settings);
            public static string ToString(EntitySatuanInfo json) => JsonConvert.SerializeObject(json);
        }



        #endregion

        #region Misi Satuan
        public partial class MisiSatuan
        {
            public string id { get; set; }
            public string id_mission { get; set; }
            public string id_object { get; set; }
            [JsonProperty("tgl_mulai")]
            private string tgl_mulai { set { startDate = DateTime.Parse(value); } }
            public DateTime startDate { get; set; }
            public string jenis { get; set; }
            [JsonProperty("properties")]
            private string _propertiesString { set { data_properties = MisiSatuanProperties.FromJson(value); } }
            public MisiSatuanProperties data_properties { get; set; }

            public static MisiSatuan FromJson(string json) => JsonConvert.DeserializeObject<MisiSatuan>(json, HelperConverter.Converter.Settings);
        }

        public partial class MisiSatuanProperties
        {
            public string nama_misi { get; set; }
            public List<JalurMisi> jalur { get; set; }
            public string kecepatan { get; set; }
            public string type { get; set; }
            public double? heading { get; set; }
            public double? distance { get; set; }
            public string typeMisi { get; set; }
            public string id_kegiatan { get; set; }
            public string idPrimary { get; set; }
            public string jenis { get; set; }
            public string objek { get; set; }
            public string misiEmDeb { get; set; }
            public double? altitude { get; set; }
            public bool takeOff { get; set; }
            public bool landing { get; set; }

            // EXTEND LINUD
            public string misi_utama { get; set; }
            public int? point_posisi { get; set; }
            [JsonProperty("tgl_mulai")]
            private string tgl_mulai { set { startDate = DateTime.Parse(value); } }
            public DateTime? startDate { get; set; }
            public int[] index_objek { get; set; }

            public static MisiSatuanProperties FromJson(string json) => JsonConvert.DeserializeObject<MisiSatuanProperties>(json, HelperConverter.Converter.Settings);
        }

        public partial class JalurMisi
        {
            public float lat { get; set; }
            public float lng { get; set; }
            public int? alt { get; set; }
        }
        #endregion

        #region Entity Radar


        [Serializable]
        public partial class ObjekRadarSymbol
        {
            [JsonProperty("className")]
            private string _class { get { return className; } set { className = value; } }
            [JsonIgnore]
            public string className;

            [JsonIgnore]
            public ObjekSymbol symbol;

            [JsonProperty("iconSize")]
            private int?[] _iconSize { 
                get {
                    int?[] sizeToArray = new int?[2];
                    sizeToArray[0] = (int) size.x;
                    sizeToArray[1] = (int) size.y;

                    return sizeToArray;
                } set {
                    if(value == null)
                    {
                        size = new Vector2(0, 0);
                        return;
                    }

                    size = new Vector2(value[0].GetValueOrDefault(0), value[1].GetValueOrDefault(0)); 
                }
            }
            [JsonIgnore]
            public Vector2 size;

            [JsonProperty("iconAnchor")]
            private int?[] _iconAnchor
            {
                get
                {
                    int?[] sizeToArray = new int?[2];
                    sizeToArray[0] = (int)anchor.x;
                    sizeToArray[1] = (int)anchor.y;

                    return sizeToArray;
                }
                set
                {
                    anchor = new Vector2(value[0].GetValueOrDefault(0), value[1].GetValueOrDefault(0));
                }
            }
            [JsonIgnore]
            public Vector2 anchor;

            [JsonProperty("id_point")]
            private string _idPoint { get { return idPoint; } set { idPoint = value; } }
            [JsonIgnore]
            public string idPoint;

            [JsonProperty("id_radar")]
            private string _idRadar { get { return idRadar; } set { idRadar = value; } }
            [JsonIgnore]
            public string idRadar;

            public static ObjekRadarSymbol FromJson(string json) => JsonConvert.DeserializeObject<ObjekRadarSymbol>(json, HelperConverter.Converter.Settings);
            public static string ToString(ObjekRadarSymbol json) => JsonConvert.SerializeObject(json);
        }

        [Serializable]
        public partial class ObjekRadarInfo
        {
            [JsonProperty("nama")]
            private string _nama { get { return nama; } set { nama = value; } }
            [JsonIgnore]
            public string nama;

            [JsonProperty("judul")]
            private string _judul { get { return desc; } set { desc = value; } }
            [JsonIgnore]
            public string desc;

            [JsonProperty("radius")]
            private float _radius { get { return radius; } set { radius = value; } }
            [JsonIgnore]
            public float radius;

            [JsonProperty("size")]
            private string _size { get { return size.ToString(); } set { float.TryParse(value, out size); } }
            [JsonIgnore]
            public float size;

            [JsonProperty("warna")]
            public string _warna
            {
                get
                {
                    string color = ColorUtility.ToHtmlStringRGBA(warna);

                    if (color == "81E0FF96")
                    {
                        color = "blue";
                    }
                    else if (color == "blue")
                    {
                        color = "red";
                    }
                    else
                    {
                        color = "#" + color;
                    }

                    return color;
                }
                set
                {
                    Color converted;
                    if (value == "blue")
                    {
                        // IF Warna == blue
                        ColorUtility.TryParseHtmlString("#81E0FF96", out converted);
                    }
                    else if (value == "red")
                    {
                        // IF Warna == Red
                        ColorUtility.TryParseHtmlString("#FF808196", out converted);
                    }
                    else
                    {
                        if (!ColorUtility.TryParseHtmlString(value, out converted))
                        {
                            converted = Color.black;
                        }
                    }

                    warna = converted;
                }
            }
            [JsonIgnore]
            public Color warna;

            [JsonProperty("id_user")]
            private string _id_user { get { return author; } set { author = value; } }
            [JsonIgnore]
            public string author;

            [JsonProperty("jenis_radar")]
            public string _jenis_radar
            {
                get
                {
                    switch (jenis)
                    {
                        case JenisRadar.BIASA:
                            return "Biasa";
                        case JenisRadar.ARHANUD:
                            return "ARHANUD";
                        default: return "Biasa";
                    }
                }
                set
                {
                    switch (value.ToLower())
                    {
                        case "biasa":
                            jenis = JenisRadar.BIASA;
                            break;
                        case "arhanud":
                            jenis = JenisRadar.ARHANUD;
                            break;
                        default:
                            jenis = JenisRadar.BIASA;
                            break;
                    }
                }
            }
            [JsonIgnore]
            public JenisRadar jenis;

            [JsonProperty("jml_peluru")]
            private string _jml_peluru { get { return ammo.ToString(); } set { int.TryParse(value, out ammo); } }
            [JsonIgnore]
            public int ammo;

            [JsonProperty("new_index")]
            private string _new_index { get { return newIndex.ToString(); } set { int.TryParse(value, out newIndex); } }
            [JsonIgnore]
            public int newIndex;

            [JsonProperty("_arhanud")]
            private string _arhanud { get { return ObjekArhanudData.ToString(arhanudData); } set { arhanudData = ObjekArhanudData.FromJson(value); } }
            [JsonIgnore]
            public ObjekArhanudData arhanudData;

            [JsonProperty("armor")]
            private string _armor { get { return armor.ToString(); } set { float.TryParse(value, out armor); } }
            [JsonIgnore]
            public float armor;
        }

        [Serializable]
        public partial class ObjekArhanudData
        {
            [JsonProperty("jumlah_pucuk")]
            private string _jumlah_pucuk { get { return totalLauncher.ToString(); } set { int.TryParse(value, out totalLauncher); } }
            [JsonIgnore]
            public int totalLauncher;

            [JsonProperty("radius_pucuk")]
            private string _radius_pucuk { get { return launcherRadius.ToString(); } set { float.TryParse(value, out launcherRadius); } }
            [JsonIgnore]
            public float launcherRadius;

            [JsonProperty("sudut_awal_pucuk")]
            private string _sudut_awal_pucuk { get { return launcherStartRotation.ToString(); } set { float.TryParse(value, out launcherStartRotation); } }
            [JsonIgnore]
            public float launcherStartRotation;

            [JsonProperty("jarak_komandan_pucuk")]
            private string _jarak_komandan_pucuk { get { return distanceToCommand.ToString(); } set { float.TryParse(value, out distanceToCommand); } }
            [JsonIgnore]
            public float distanceToCommand;

            [JsonProperty("array_pucuk")]
            public List<string> _array_pucuk { get { return launcherList; } set { launcherList = value; } }
            [JsonIgnore]
            public List<string> launcherList;

            [JsonProperty("komandan")]
            private string _komandan { get { return commandUnit; } set { commandUnit = value; } }
            [JsonIgnore]
            public string commandUnit;

            public static ObjekArhanudData FromJson(string json) => JsonConvert.DeserializeObject<ObjekArhanudData>(json, HelperConverter.Converter.Settings);
            public static string ToString(ObjekArhanudData json) => JsonConvert.SerializeObject(json);
        }
        #endregion
    }

    namespace HelperDataAlutsista
    {
        #region Detail Satuan Darat
        public class DetailSatuanDarat
        {
            [JsonProperty("object")]
            public ObjectDarat OBJ { get; set; }
            public ImageDarat images { get; set; }
            public string path_object_3d { get; set; }
            public string tipe_tni { get; set; }

            public static DetailSatuanDarat FromJson(string json) => JsonConvert.DeserializeObject<DetailSatuanDarat>(json, HelperConverter.Converter.Settings);
        }

        public class ObjectDarat
        {
            [JsonProperty("VEHICLE_ID")]
            public int? id { get; set; }
            [JsonProperty("VEHICLE_SYM_ID")]
            public int? symbolID { get; set; }
            [JsonProperty("VEHICLE_NAME")]
            public string name { get; set; }
            [JsonProperty("VEHICLE_MAX_SPEED")]
            public string maxSpeed { get; set; }
            [JsonProperty("vehicle_matra")]
            public string matra { get; set; }

            [JsonProperty("VEHICLE_FUEL")]
            private string _fuelLoad { get { return fuelLoad.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); fuelLoad = convert; } }
            [JsonIgnore]
            public float fuelLoad { get; set; }

            [JsonProperty("VEHICLE_FUEL_CAPACITY")]
            private string _fuelCapacity { get { return fuelCapacity.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); fuelCapacity = convert; } }
            [JsonIgnore]
            public float fuelCapacity { get; set; }

            [JsonProperty("VEHICLE_CONSUME_FUEL")]
            private string _fuelConsume { get { return fuelConsume.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); fuelConsume = convert; } }
            [JsonIgnore]
            public float fuelConsume { get; set; }

            [JsonProperty("VEHICLE_HEIGHT")]
            private string _height { get { return height.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); height = convert; } }
            [JsonIgnore]
            public float height { get; set; }

            [JsonProperty("VEHICLE_WIDTH")]
            private string _width { get { return width.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); width = convert; } }
            [JsonIgnore]
            public float width { get; set; }

            [JsonProperty("VEHICLE_WEIGHT")]
            private string _weight { get { return weight.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); weight = convert; } }
            [JsonIgnore]
            public float weight { get; set; }

            [JsonProperty("VEHICLE_HEALTH")]
            public int? health { get; set; }

            [JsonProperty("VEHICLE_LENGTH")]
            private string _length { get { return length.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); length = convert; } }
            [JsonIgnore]
            public float length { get; set; }

            [JsonProperty("image")]
            public ImageDarat image { get; set; }
            [JsonProperty("negara_pembuat")]
            public string negaraPembuat { get; set; }
            [JsonProperty("perusahaan_pembuat")]
            public string perusahaanPembuat { get; set; }
            [JsonProperty("tahun_pembuat")]

            public string tahunPembuat { get; set; }
            [JsonProperty("JUMLAH_PERWIRA")]
            public int? jumlah_perwira { get; set; }
            [JsonProperty("JUMLAH_TAMTAMA")]
            public int? jumlah_tamtama { get; set; }
            [JsonProperty("JUMLAH_BINTARA")]
            public int? jumlah_bintara { get; set; }
            [JsonProperty("nama")]
            public string fontName { get; set; }
            [JsonProperty("index")]
            public string fontIndex { get; set; }
            public string grup { get; set; }
            public string entity_name { get; set; }
            public string entity_kategori { get; set; }
            public string keterangan { get; set; }
            public string jenis_role { get; set; }

            [JsonProperty("model_3d_tfg")]
            public string model3D { get; set; }
            [JsonProperty("titan")]
            public string modelTitan { get; set; }
            public string jenis { get; set; }
            public string style_symbol { get; set; }

            public static string ToString(ObjectDarat json) => JsonConvert.SerializeObject(json);
            public static ObjectDarat[] FromJson(string json) => JsonConvert.DeserializeObject<ObjectDarat[]>(json, HelperConverter.Converter.Settings);
        }

        public class ImageDarat
        {
            public int? VEHICLE_ID { get; set; }
            public string VEHICLE_IMAGE_FNAME { get; set; }
            public string VEHICLE_IMAGE_DESC { get; set; }
        }
        #endregion

        #region Detail Satuan Laut
        public class DetailSatuanLaut
        {
            [JsonProperty("object")]
            public ObjectLaut OBJ { get; set; }
            //public ImageLaut? images { get; set; }
            public string path_object_3d { get; set; }
            public string tipe_tni { get; set; }

            public static DetailSatuanLaut FromJson(string json) => JsonConvert.DeserializeObject<DetailSatuanLaut>(json, HelperConverter.Converter.Settings);
        }

        public class ObjectLaut
        {
            [JsonProperty("SHIP_ID")]
            public int? id { get; set; }
            [JsonProperty("SHIP_SYM_ID")]
            public int? symbolID { get; set; }
            [JsonProperty("SHIP_NAME")]
            public string name { get; set; }
            [JsonProperty("SHIP_MAX_SPEED")]
            public string maxSpeed { get; set; }
            [JsonProperty("ship_matra")]
            public string matra { get; set; }
            [JsonProperty("SHIP_FUEL_LOAD")]
            public float? fuelLoad { get; set; }
            [JsonProperty("SHIP_FUEL_MAX")]
            public float? fuelMax { get; set; }
            [JsonProperty("SHIP_FUEL_CAPACITY")]
            public string fuelCapacity { get; set; }
            [JsonProperty("SHIP_CONSUME_FUEL")]
            public float? fuelConsume { get; set; }
            [JsonProperty("SHIP_DIM_LENGTH")]
            public float? length { get; set; }
            [JsonProperty("SHIP_DIM_HEIGHT")]
            public float? height { get; set; }
            [JsonProperty("SHIP_DIM_WIDTH")]
            public float? width { get; set; }

            [JsonProperty("image")]
            public ImageLaut image { get; set; }
            [JsonProperty("SHIP_CATEGORY_ID")]
            public string categoryID { get; set; }
            [JsonProperty("SHIP_CATEGORY_NAME")]
            public string categoryName { get; set; }
            [JsonProperty("SHIP_CATEGORY_DESC")]
            public string categoryDesc { get; set; }
            [JsonProperty("SHIP_CLASS_ID")]
            public int? classID { get; set; }
            [JsonProperty("SHIP_CLASS_NAME")]
            public string className { get; set; }
            [JsonProperty("SHIP_HEALTH")]
            public float? health { get; set; }
            [JsonProperty("SHIP_MAX_LOAD_QTY")]
            public string maxLoadQty { get; set; }
            [JsonProperty("negara_pembuat")]
            public string negaraPembuat { get; set; }
            [JsonProperty("perusahaan_pembuat")]
            public string perusahaanPembuat { get; set; }
            [JsonProperty("tahun_pembuat")]
            public string tahunPembuat { get; set; }
            [JsonProperty("JUMLAH_PERWIRA")]
            //public string jumlah_perwira { get; set; }
            public int? jumlah_perwira { get; set; }
            [JsonProperty("JUMLAH_TAMTAMA")]
            //public string jumlah_tamtama { get; set; }
            public int? jumlah_tamtama { get; set; }
            [JsonProperty("JUMLAH_BINTARA")]
            //public string jumlah_bintara { get; set; }
            public int? jumlah_bintara { get; set; }
            [JsonProperty("nama")]
            public string fontName { get; set; }
            [JsonProperty("index")]
            public string fontIndex { get; set; }
            public string grup { get; set; }
            public string entity_name { get; set; }
            public string entity_kategori { get; set; }
            public string keterangan { get; set; }
            public string jenis_role { get; set; }

            [JsonProperty("model_3d_tfg")]
            public string model3D { get; set; }
            public string jenis { get; set; }
            public string style_symbol { get; set; }

            public static string ToString(ObjectLaut json) => JsonConvert.SerializeObject(json);
            public static ObjectLaut[] FromJson(string json) => JsonConvert.DeserializeObject<ObjectLaut[]>(json, HelperConverter.Converter.Settings);
        }

        public class ImageLaut
        {
            public int? SHIP_ID { get; set; }
            public string SHIP_IMAGE_FNAME { get; set; }
            public string SHIP_IMAGE_DESC { get; set; }
        }
        #endregion

        #region Detail Satuan Udara
        public class DetailSatuanUdara
        {
            [JsonProperty("object")]
            public ObjectUdara OBJ { get; set; }
            public ImageUdara images { get; set; }
            public string path_object_3d { get; set; }
            public string tipe_tni { get; set; }

            public static DetailSatuanUdara FromJson(string json) => JsonConvert.DeserializeObject<DetailSatuanUdara>(json, HelperConverter.Converter.Settings);
        }

        public class ObjectUdara
        {
            [JsonProperty("AIRCRAFT_ID")]
            public int? id { get; set; }
            [JsonProperty("AIRCRAFT_SYM_ID")]
            public int? symbolID { get; set; }
            [JsonProperty("AIRCRAFT_NAME")]
            public string name { get; set; }
            [JsonProperty("AIRCRAFT_MAX_SPEED")]
            public string maxSpeed { get; set; }
            [JsonProperty("aircraft_matra")]
            public string matra { get; set; }

            [JsonProperty("AIRCRAFT_FUEL_CAPACITY")]
            private string _fuelCapacity { get { return fuelCapacity.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); fuelCapacity = convert; } }
            [JsonIgnore]
            public float fuelCapacity { get; set; }

            [JsonProperty("AIRCRAFT_CONSUME_FUEL")]
            private string _fuelConsume { get { return fuelConsume.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); fuelConsume = convert; } }
            [JsonIgnore]
            public float fuelConsume { get; set; }

            [JsonProperty("AIRCRAFT_HEIGHT")]
            private string _height { get { return height.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); height = convert; } }
            [JsonIgnore]
            public float height { get; set; }

            [JsonProperty("AIRCRAFT_WEIGHT")]
            private string _weight { get { return weight.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); weight = convert; } }
            [JsonIgnore]
            public float weight { get; set; }

            [JsonProperty("AIRCRAFT_HEALTH")]
            private string _health { get { return health.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); health = convert; } }
            [JsonIgnore]
            public float health { get; set; }

            [JsonProperty("AIRCRAFT_TYPE")]
            public int? type { get; set; }
            [JsonProperty("AIRCRAFT_TYPE_DESC")]
            public string typeDesc { get; set; }
            [JsonProperty("image")]
            public ImageUdara image { get; set; }
            [JsonProperty("negara_pembuat")]
            public string negaraPembuat { get; set; }
            [JsonProperty("perusahaan_pembuat")]
            public string perusahaanPembuat { get; set; }
            [JsonProperty("tahun_pembuat")]
            public string tahunPembuat { get; set; }

            [JsonProperty("JUMLAH_PERWIRA")]
            public int? jumlah_perwira { get; set; }
            [JsonProperty("JUMLAH_TAMTAMA")]
            public int? jumlah_tamtama { get; set; }
            [JsonProperty("JUMLAH_BINTARA")]
            public int? jumlah_bintara { get; set; }
            [JsonProperty("nama")]
            public string fontName { get; set; }
            [JsonProperty("index")]
            public string fontIndex { get; set; }
            public string grup { get; set; }
            public string entity_name { get; set; }
            public string entity_kategori { get; set; }
            public string keterangan { get; set; }
            public string jenis_role { get; set; }

            [JsonProperty("model_3d_tfg")]
            public string model3D { get; set; }
            public string jenis { get; set; }
            public string style_symbol { get; set; }

            public static string ToString(ObjectUdara json) => JsonConvert.SerializeObject(json);
            public static ObjectUdara[] FromJson(string json) => JsonConvert.DeserializeObject<ObjectUdara[]>(json, HelperConverter.Converter.Settings);
        }

        public class ImageUdara
        {
            public int? AIRCRAFT_ID { get; set; }
            public string AIRCRAFT_IMAGE_FNAME { get; set; }
            public string AIRCRAFT_IMAGE_DESC { get; set; }
        }
        #endregion

        #region Detail Radar AD
        public partial class EntityRadar
        {
            [JsonProperty("id")]
            public string id_entity { get; set; }
            public string id_user { get; set; }
            public string id_symbol { get; set; }
            public string dokumen { get; set; }
            [JsonProperty("nama")]
            public string id_satuan { get; set; }
            [JsonProperty("lat_y")]
            public float lat { get; set; }
            [JsonProperty("lng_x")]
            public float lng { get; set; }
            [JsonProperty("info")]
            private string _infoString { set { data_info = EntityRadarInfo.FromJson(value); } }
            public EntityRadarInfo data_info { get; set; }
            [JsonProperty("symbol")]
            private string _symbolString { set { data_symbol = HelperPlotting.ObjekRadarSymbol.FromJson(value); } }
            public HelperPlotting.ObjekRadarSymbol data_symbol { get; set; }
            public string info_symbol { get; set; }

            public static string ToString(EntityRadar json) => JsonConvert.SerializeObject(json);
            public static EntityRadar FromJson(JToken json) => JsonConvert.DeserializeObject<EntityRadar>(json.ToString(), HelperConverter.Converter.Settings);
        }

        public partial class ObjekRadarHelper {
            //[JsonProperty("id")]
            //public long? id;
            [JsonProperty("id_user")]
            public long? id_user;
            [JsonProperty("dokumen")]
            public long? dokumen;
            [JsonProperty("nama")]
            public string nama;

            [JsonProperty("lat_y")]
            public double lat_y;
            [JsonProperty("lng_x")]
            public double lng_x;

            [JsonProperty("info_radar")]
            private string _info { get { return EntityRadarInfo.ToString(info); } set { info = EntityRadarInfo.FromJson(value); } }
            [JsonIgnore]
            public EntityRadarInfo info;

            [JsonProperty("symbol")]
            private string _symbolString { set { symbol = HelperPlotting.ObjekRadarSymbol.FromJson(value); } }
            [JsonIgnore]
            public HelperPlotting.ObjekRadarSymbol symbol;
            public string info_symbol;

            public static string ToString(ObjekRadarHelper json) => JsonConvert.SerializeObject(json);
            public static string ToString(ObjekRadarHelper[] json) => JsonConvert.SerializeObject(json);
            public static ObjekRadarHelper FromJson(JToken json) => JsonConvert.DeserializeObject<ObjekRadarHelper>(json.ToString(), HelperConverter.Converter.Settings);
        }

        public partial class EntityRadarInfo
        {
            //public int? radius;
            //public string size;
            //public string warna;
            //public int? id_user;
            //public string id_symbol;
            //public string jenis_radar;
            //public string jml_peluru;
            //public string new_index;
            //public float? armor;

            [JsonProperty("nama")]
            private string _nama { get { return nama; } set { nama = value; } }
            [JsonIgnore]
            public string nama;

            [JsonProperty("judul")]
            private string _judul { get { return desc; } set { desc = value; } }
            [JsonIgnore]
            public string desc;

            [JsonProperty("radius")]
            private string _radius { get { return radius.ToString(); } set { int.TryParse(value, out radius); } }
            [JsonIgnore]
            public int radius;

            [JsonProperty("size")]
            private string _size { get { return size; } set { size = value; } }
            [JsonIgnore]
            public string size;

            [JsonProperty("warna")]
            private string _color
            {
                get
                {
                    string color = ColorUtility.ToHtmlStringRGBA(warna);

                    return color;
                }
                set
                {
                    Color converted;
                    if (value == "blue")
                    {
                        // IF Warna == blue
                        ColorUtility.TryParseHtmlString("#81E0FF96", out converted);
                    }
                    else if (value == "red")
                    {
                        // IF Warna == Red
                        ColorUtility.TryParseHtmlString("#FF808196", out converted);
                    }
                    else
                    {
                        if (!ColorUtility.TryParseHtmlString(value, out converted))
                        {
                            converted = Color.black;
                        }
                    }

                    warna = converted;
                }
            }
            [JsonIgnore]
            public Color warna;

            [JsonProperty("id_user")]
            private string _id_user { get { return idUser.ToString(); } set { int.TryParse(value, out idUser); } }
            [JsonIgnore]
            public int idUser;

            //[JsonProperty("id_symbol")]
            //private string id_symbol { get { return idSymbol; } set { idSymbol = value; } }
            //[JsonIgnore]
            //public string idSymbol;

            [JsonProperty("jenis_radar")]
            public string _jenis_radar
            {
                get
                {
                    switch (jenisRadar)
                    {
                        case JenisRadar.BIASA:
                            return "Biasa";
                        case JenisRadar.ARHANUD:
                            return "ARHANUD";
                        default: return "Biasa";
                    }
                }
                set
                {
                    switch (value.ToLower())
                    {
                        case "biasa":
                            jenisRadar = JenisRadar.BIASA;
                            break;
                        case "arhanud":
                            jenisRadar = JenisRadar.ARHANUD;
                            break;
                        default:
                            jenisRadar = JenisRadar.BIASA;
                            break;
                    }
                }
            }
            [JsonIgnore]
            public JenisRadar jenisRadar;

            [JsonProperty("jml_peluru")]
            private string _jml_peluru { get { return ammo.ToString(); } set { int.TryParse(value, out ammo); } }
            [JsonIgnore]
            public int ammo;

            [JsonProperty("new_index")]
            private string _new_index { get { return newIndex; } set { newIndex = value; } }
            [JsonIgnore]
            public string newIndex;

            [JsonProperty("armor")]
            private string _armor { get { return armor.ToString(); } set { float.TryParse(value, out armor); } }
            [JsonIgnore]
            public float armor;


            public static EntityRadarInfo FromJson(string json) => JsonConvert.DeserializeObject<EntityRadarInfo>(json, HelperConverter.Converter.Settings);
            public static string ToString(EntityRadarInfo json) => JsonConvert.SerializeObject(json);
        }

        public class ObjectRadar
        {
            [JsonProperty("RADAR_ID")]
            public int? radarID { get; set; }

            [JsonProperty("RADAR_SYM_ID")]
            public int? symbolID { get; set; }

            [JsonProperty("RADAR_NAME")]
            public string name { get; set; }

            [JsonProperty("RADAR_AZIMUTH")]
            public string azimuth { get; set; }

            [JsonProperty("RADAR_JAMM_RANGE")]
            public string jammingRange { get; set; }

            [JsonProperty("RADAR_TYPE")]
            public string type { get; set; }

            [JsonProperty("RADAR_DET_RANGE")]
            private string _radarDetRange { get { return detectionRange.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); detectionRange = convert; } }
            [JsonIgnore]
            public float detectionRange { get; set; }

            [JsonProperty("RADAR_MAX_RANGE")]
            private string _radarMaxRange { get { return maxRange.ToString(); } set { var convert = 0f; float.TryParse(value, out convert); maxRange = convert; } }
            [JsonIgnore]
            public float maxRange { get; set; }


            [JsonProperty("MATRA")]
            private string _matra
            {
                get
                {
                    switch (matra)
                    {
                        case JenisEntity.LAND:
                            return "1";
                        case JenisEntity.SEA:
                            return "2";
                        case JenisEntity.AIR:
                        default: return "3";
                    }
                }
                set
                {
                    switch (value)
                    {
                        case "1":
                            matra = JenisEntity.LAND;
                            break;
                        case "2":
                            matra = JenisEntity.SEA;
                            break;
                        case "3":
                            matra = JenisEntity.AIR;
                            break;
                        default:
                            matra = JenisEntity.LAND;
                            break;
                    }
                }
            }
            [JsonIgnore]
            public JenisEntity matra { get; set; }
            public string style_symbol { get; set; }
            public string id { get; set; }

            [JsonProperty("nama")]
            public string fontName { get; set; }
            [JsonProperty("index")]
            public string fontIndex { get; set; }
            public string keterangan { get; set; }
            public string grup { get; set; }
            public string entity_name { get; set; }
            public string entity_kategori { get; set; }
            public string jenis_role { get; set; }
            public int? health { get; set; }

            public static string ToString(ObjectRadar json) => JsonConvert.SerializeObject(json);
            public static ObjectRadar[] FromJson(string json) => JsonConvert.DeserializeObject<ObjectRadar[]>(json, HelperConverter.Converter.Settings);
        }

        public partial class ObjekTextHelper
        {
            [JsonProperty("id_user")]
            public long? id_user;
            [JsonProperty("dokumen")]
            public long? dokumen;
            [JsonProperty("nama")]
            public string nama;

            [JsonProperty("lat_y")]
            public double lat_y;
            [JsonProperty("lng_x")]
            public double lng_x;

            [JsonProperty("info_text")]
            public string info_text;

            [JsonProperty("info")]
            private string _symbol { get { return ObjekTextSymbol.ToString(symbol); } set { symbol = ObjekTextSymbol.FromJson(value); } }
            [JsonIgnore]
            public ObjekTextSymbol symbol;

            public static string ToString(ObjekTextHelper json) => JsonConvert.SerializeObject(json);
            public static string ToString(ObjekTextHelper[] json) => JsonConvert.SerializeObject(json);
            public static ObjekTextHelper FromJson(JToken json) => JsonConvert.DeserializeObject<ObjekTextHelper>(json.ToString(), HelperConverter.Converter.Settings);
        }

        [Serializable]
        public partial class ObjekTextSymbol
        {
            [JsonProperty("text")]
            private string _text { get { return text; } set { text = value; } }
            [JsonIgnore]
            public string text;

            [JsonProperty("angle")]
            private string _angle { get { return angle.ToString(); } set { int.TryParse(value, out angle); } }
            [JsonIgnore]
            public int angle;

            [JsonProperty("angle")]
            private string _size { get { return fontSize.ToString(); } set { int.TryParse(value, out fontSize); } }
            [JsonIgnore]
            public int fontSize;

            [JsonProperty("weight")]
            private string _weight { get { return weight; } set { weight = value; } }
            [JsonIgnore]
            public string weight;

            [JsonProperty("warna")]
            private string _color
            {
                get
                {
                    string color = ColorUtility.ToHtmlStringRGBA(warna);

                    return color;
                }
                set
                {
                    Color converted;
                    if (value == "blue")
                    {
                        // IF Warna == blue
                        ColorUtility.TryParseHtmlString("#81E0FF96", out converted);
                    }
                    else if (value == "red")
                    {
                        // IF Warna == Red
                        ColorUtility.TryParseHtmlString("#FF808196", out converted);
                    }
                    else
                    {
                        if (!ColorUtility.TryParseHtmlString(value, out converted))
                        {
                            converted = Color.black;
                        }
                    }

                    warna = converted;
                }
            }
            [JsonIgnore]
            public Color warna;

            public static ObjekTextSymbol FromJson(string json) => JsonConvert.DeserializeObject<ObjekTextSymbol>(json, HelperConverter.Converter.Settings);
            public static string ToString(ObjekTextSymbol json) => JsonConvert.SerializeObject(json);
        }

        public partial class ObjekDrawingHelper
        {
            [JsonProperty("id")]
            public long? id;
            [JsonProperty("id_user")]
            public long? id_user;
            [JsonProperty("dokumen")]
            public long? dokumen;
            [JsonProperty("nama")]
            public string nama;

            [JsonProperty("geometry")]
            private string _geometry {
                get {
                    if(geometryPolyline != null)
                    {
                        return EntityDrawingGeometryPoyline.ToString(geometryPolyline);
                    }
                    else
                    {
                        return EntityDrawingGeometryPolygon.ToString(geometryPolygon);
                    }
                }
                set
                {
                    try
                    {
                        geometryPolyline = EntityDrawingGeometryPoyline.FromJson(value);
                    }catch(Exception e)
                    {
                        try
                        {
                            geometryPolygon = EntityDrawingGeometryPolygon.FromJson(value);
                        }catch(Exception ex)
                        {
                            try
                            {
                                geometryPoint = EntityDrawingGeometryPoint.FromJson(value);
                            }catch(Exception exx)
                            {
                                geometryPolylineCurve = EntityDrawingGeometryPoylineCurve.FromJson(value);
                            }
                        }
                    }
                }
            }
            [JsonIgnore]
            public EntityDrawingGeometryPoyline geometryPolyline;
            [JsonIgnore]
            public EntityDrawingGeometryPoylineCurve geometryPolylineCurve;
            [JsonIgnore]
            public EntityDrawingGeometryPolygon geometryPolygon;
            [JsonIgnore]
            public EntityDrawingGeometryPoint geometryPoint;

            [JsonProperty("properties")]
            public string _properties
            {
                get
                {
                    return EntityDrawingProperty.ToString(properties);
                }
                set
                {

                    properties = EntityDrawingProperty.FromJson(value);
                }
            }
            [JsonIgnore]
            public EntityDrawingProperty properties;

            [JsonProperty("type")]
            private string _type
            {
                get
                {
                    return EntityController.instance.DrawingTypeToString(type);
                }
                set
                {
                    type = EntityController.instance.StringToDrawingType(value);
                }
            }
            [JsonIgnore]
            public DrawingType type;

            public static string ToString(ObjekDrawingHelper json) => JsonConvert.SerializeObject(json);
            public static string ToString(ObjekDrawingHelper[] json) => JsonConvert.SerializeObject(json);
            public static ObjekDrawingHelper FromJson(JToken json) => JsonConvert.DeserializeObject<ObjekDrawingHelper>(json.ToString(), HelperConverter.Converter.Settings);
        }
        
        [Serializable]
        public partial class EntityDrawingGeometryPoyline
        {
            [JsonProperty("type")]
            private string _type
            {
                get
                {
                    return EntityController.instance.DrawingTypeToString(type, true);
                }
                set
                {
                    type = EntityController.instance.StringToDrawingType(value);
                }
            }
            [JsonIgnore]
            public DrawingType type;

            [JsonProperty("coordinates")]
            private List<List<double>> _coordinates
            {
                get
                {
                    return coordinates;
                }
                set
                {
                    coordinates = value;
                }
            }
            [JsonIgnore]
            [SerializeField]
            public List<List<double>> coordinates;

            public static EntityDrawingGeometryPoyline FromJson(string json) => JsonConvert.DeserializeObject<EntityDrawingGeometryPoyline>(json, HelperConverter.Converter.Settings);
            public static string ToString(EntityDrawingGeometryPoyline json) => JsonConvert.SerializeObject(json);
        }

        [Serializable]
        public partial class EntityDrawingGeometryPoylineCurve
        {
            [JsonProperty("type")]
            private string _type
            {
                get
                {
                    return EntityController.instance.DrawingTypeToString(type, true);
                }
                set
                {
                    type = DrawingType.POLYLINE_CURVE;
                }
            }
            [JsonIgnore]
            public DrawingType type;

            [JsonProperty("coordinates")]
            private List<object> _coordinates
            {
                get
                {
                    return coordinates;
                }
                set
                {
                    coordinates = value;
                }
            }
            [JsonIgnore]
            [SerializeField]
            public List<object> coordinates;

            public static EntityDrawingGeometryPoylineCurve FromJson(string json) => JsonConvert.DeserializeObject<EntityDrawingGeometryPoylineCurve>(json, HelperConverter.Converter.Settings);
            public static string ToString(EntityDrawingGeometryPoylineCurve json) => JsonConvert.SerializeObject(json);
        }

        [Serializable]
        public partial class EntityDrawingGeometryPoint
        {
            [JsonProperty("type")]
            private string _type
            {
                get
                {
                    return EntityController.instance.DrawingTypeToString(type, true);
                }
                set
                {
                    type = EntityController.instance.StringToDrawingType(value);
                }
            }
            [JsonIgnore]
            public DrawingType type;

            [JsonProperty("coordinates")]
            public List<double> coordinates { get; set; }

            public static EntityDrawingGeometryPoint FromJson(string json) => JsonConvert.DeserializeObject<EntityDrawingGeometryPoint>(json, HelperConverter.Converter.Settings);
            public static string ToString(EntityDrawingGeometryPoint json) => JsonConvert.SerializeObject(json);
        }

        [Serializable]
        public partial class EntityDrawingGeometryPolygon
        {
            [JsonProperty("type")]
            private string _type
            {
                get
                {
                    return EntityController.instance.DrawingTypeToString(type, true);
                }
                set
                {
                    type = EntityController.instance.StringToDrawingType(value);
                }
            }
            [JsonIgnore]
            [NonSerialized] public DrawingType type;

            [JsonProperty("coordinates")]
            public List<List<List<double>>> coordinates { get; set; }

            public static EntityDrawingGeometryPolygon FromJson(string json) => JsonConvert.DeserializeObject<EntityDrawingGeometryPolygon>(json, HelperConverter.Converter.Settings);
            public static string ToString(EntityDrawingGeometryPolygon json) => JsonConvert.SerializeObject(json);
        }

        [Serializable]
        public partial class EntityDrawingProperty
        {
            [JsonProperty("stroke")]
            private bool _stroke
            {
                get{ return stroke; }
                set{ stroke = value; }
            }
            [JsonIgnore]
            public bool stroke;

            [JsonProperty("color")]
            private string _color
            {
                get
                {
                    string color = "#" + ColorUtility.ToHtmlStringRGBA(warna);

                    return color;
                }
                set
                {
                    Color converted;
                    if (value == "blue")
                    {
                        // IF Warna == blue
                        ColorUtility.TryParseHtmlString("#81E0FF96", out converted);
                    }
                    else if (value == "red")
                    {
                        // IF Warna == Red
                        ColorUtility.TryParseHtmlString("#FF808196", out converted);
                    }
                    else
                    {
                        if (!ColorUtility.TryParseHtmlString(value, out converted))
                        {
                            converted = Color.black;
                        }
                    }

                    warna = converted;
                }
            }
            [JsonIgnore]
            public Color warna;

            [JsonProperty("weight")]
            private float _weight
            {
                get { return weight; }
                set { weight = value; }
            }
            [JsonIgnore]
            public float weight;

            [JsonProperty("opacity")]
            private float _opacity
            {
                get { return opacity; }
                set { opacity = value; }
            }
            [JsonIgnore]
            public float opacity;

            [JsonProperty("fill")]
            private bool _fill
            {
                get { return fill; }
                set { fill = value; }
            }
            [JsonIgnore]
            public bool fill;

            [JsonProperty("fillColor")]
            private string _fillColor
            {
                get
                {
                    string color = "#" + ColorUtility.ToHtmlStringRGBA(fillColor);

                    return color;
                }
                set
                {
                    Color converted;
                    if (value == "blue")
                    {
                        // IF Warna == blue
                        ColorUtility.TryParseHtmlString("#81E0FF96", out converted);
                    }
                    else if (value == "red")
                    {
                        // IF Warna == Red
                        ColorUtility.TryParseHtmlString("#FF808196", out converted);
                    }
                    else
                    {
                        if (!ColorUtility.TryParseHtmlString(value, out converted))
                        {
                            converted = Color.black;
                        }
                    }

                    fillColor = converted;
                }
            }
            [JsonIgnore]
            public Color fillColor;

            [JsonProperty("fillOpacity")]
            private float _fillOpacity
            {
                get { return fillOpacity; }
                set { fillOpacity = value; }
            }
            [JsonIgnore]
            public float fillOpacity;

            [JsonProperty("showArea")]
            public bool showArea { get; set; }
            [JsonProperty("draggable")]
            public bool draggable { get; set; }
            [JsonProperty("radius")]
            private double _radius
            {
                get { return radius; }
                set { radius = value; }
            }
            [JsonIgnore]
            public double radius;

            [JsonProperty("jenisArrow")]
            private string _jenisArrow
            {
                get
                {
                    return EntityController.instance.DrawingArrowTypeToString(arrowType);
                }
                set
                {
                    arrowType = EntityController.instance.StringToDrawingArrowType(value);
                }
            }
            [JsonIgnore]
            [NonSerialized] public DrawingArrowType arrowType;

            [JsonProperty("interactive")]
            public bool interactive { get; set; }
            [JsonProperty("className")]
            public string className { get; set; }
            [JsonProperty("clickable")]
            public bool clickable { get; set; }
            [JsonProperty("dashArray")]
            public string dashArray { get; set; }

            [JsonProperty("lineJoin")]
            public string lineJoin { get; set; }

            [JsonProperty("transform")]
            public bool transform { get; set; }

            [JsonProperty("id_point")]
            public string id_point { get; set; }
            [JsonProperty("aksi")]
            public bool aksi { get; set; }
            [JsonProperty("typePolyline")]
            public string typePolyline { get; set; }

            [JsonProperty("waktuMulai")]
            private string _waktu_mulai
            {
                set
                {
                    DateTime converted;
                    if (DateTime.TryParse(value, out converted))
                    {
                        startDate = converted;
                    }
                }
            }
            public DateTime startDate { get; set; }

            [JsonProperty("waktuAkhir")]
            private string waktu_akhir
            {
                set
                {
                    DateTime converted;
                    if (DateTime.TryParse(value, out converted))
                    {
                        endDate = converted;
                    }
                }
            }
            public DateTime endDate { get; set; }

            public static EntityDrawingProperty FromJson(string json) => JsonConvert.DeserializeObject<EntityDrawingProperty>(json, HelperConverter.Converter.Settings);
            public static string ToString(EntityDrawingProperty json) => JsonConvert.SerializeObject(json, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }

        //public partial class EntityDrawingPropertyPolygon
        //{
        //    [JsonProperty("stroke")]
        //    public bool stroke { get; set; }

        //    [JsonProperty("color")]
        //    private string _color
        //    {
        //        get
        //        {
        //            string color = ColorUtility.ToHtmlStringRGBA(warna);

        //            return color;
        //        }
        //        set
        //        {
        //            Color converted;
        //            if (value == "blue")
        //            {
        //                // IF Warna == blue
        //                ColorUtility.TryParseHtmlString("#81E0FF96", out converted);
        //            }
        //            else if (value == "red")
        //            {
        //                // IF Warna == Red
        //                ColorUtility.TryParseHtmlString("#FF808196", out converted);
        //            }
        //            else
        //            {
        //                if (!ColorUtility.TryParseHtmlString(value, out converted))
        //                {
        //                    converted = Color.black;
        //                }
        //            }

        //            warna = converted;
        //        }
        //    }
        //    [JsonIgnore]
        //    public Color warna;

        //    [JsonProperty("weight")]
        //    public float weight { get; set; }
        //    [JsonProperty("opacity")]
        //    public float opacity { get; set; }
        //    [JsonProperty("fill")]
        //    public bool fill { get; set; }

        //    [JsonProperty("fillColor")]
        //    private string _fillColor
        //    {
        //        get
        //        {
        //            string color = ColorUtility.ToHtmlStringRGBA(fillColor);

        //            return color;
        //        }
        //        set
        //        {
        //            Color converted;
        //            if (value == "blue")
        //            {
        //                // IF Warna == blue
        //                ColorUtility.TryParseHtmlString("#81E0FF96", out converted);
        //            }
        //            else if (value == "red")
        //            {
        //                // IF Warna == Red
        //                ColorUtility.TryParseHtmlString("#FF808196", out converted);
        //            }
        //            else
        //            {
        //                if (!ColorUtility.TryParseHtmlString(value, out converted))
        //                {
        //                    converted = Color.black;
        //                }
        //            }

        //            fillColor = converted;
        //        }
        //    }
        //    [JsonIgnore]
        //    public Color fillColor;

        //    [JsonProperty("fillOpacity")]
        //    public float fillOpacity { get; set; }
        //    [JsonProperty("draggable")]
        //    public bool draggable { get; set; }
        //    [JsonProperty("interactive")]
        //    public bool interactive { get; set; }
        //    [JsonProperty("className")]
        //    public string className { get; set; }

        //    public static EntityDrawingPropertyPolygon FromJson(string json) => JsonConvert.DeserializeObject<EntityDrawingPropertyPolygon>(json, HelperConverter.Converter.Settings);
        //    public static string ToString(EntityDrawingPropertyPolygon json) => JsonConvert.SerializeObject(json);
        //}
        #endregion

        #region Detail Radar AL

        #endregion

        #region Detail Radar AU

        #endregion
    }

    namespace HelperDataPlotting
    {
        public static class PlottingHelper
        {
            public enum MenuMode { NONE, SATUAN, PASUKAN, MISI, MISI_NODE, FORMASI, OBSTACLE, RADAR, KEKUATAN, BUNGUS, SITUASI, LOGISTIK, DRAWING }
            public enum PlotMode { CREATE, EDIT, DELETE }

            public static Color GetColorByUser()
            {
                Color converted;
                if (SessionUser.nama_asisten == "ASOPS")
                {
                    // IF Warna == blue
                    ColorUtility.TryParseHtmlString("#81E0FF96", out converted);
                }
                else if (SessionUser.nama_asisten == "ASINTEL")
                {
                    // IF Warna == Red
                    ColorUtility.TryParseHtmlString("#FF808196", out converted);
                }
                else
                {
                    converted = Color.black;
                }

                return converted;
            }

            public static string GetColorName(Color color)
            {
                var stringColor = ColorUtility.ToHtmlStringRGBA(color);

                if(stringColor == "81E0FF96")
                {
                    return "blue";
                }else if(stringColor == "FF808196")
                {
                    return "red";
                }
                else
                {
                    return "#" + stringColor;
                }
            }
        }

        public class PDataSatuan
        {
            public long? id { get; set; }
            public long? id_user { get; set; }
            public int? id_symbol { get; set; }
            public string dokumen { get; set; }
            public string nama { get; set; }
            public double lat_y { get; set; }
            public double lng_x { get; set; }
            public PDataSatuanStyle style { get; set; }
            public PDataSatuanInfo info { get; set; }
            public PDataSatuanSymbol symbol { get; set; }
            public string id_kegiatan { get; set; }
            public string isi_logistik { get; set; }


            public static string ToString(PDataSatuan json) => JsonConvert.SerializeObject(json);
        }

        public class PDataSatuanStyle
        {
            public string nama { get; set; }
            public string index { get; set; }
            public string keterangan { get; set; }
            public string grup { get; set; }
            public string entity_name { get; set; }
            public string entity_kategori { get; set; }
            public string jenis_role { get; set; }
            public string label_style { get; set; }
        }

        public class PDataSatuanInfo
        {
            public string kecepatan { get; set; }
            public string nomer_satuan { get; set; }
            public string nama_satuan { get; set; }
            public string nomer_atasan { get; set; }
            public string tgl_selesai { get; set; }
            public string warna { get; set; }
            public string size { get; set; }
            public string weapon { get; set; }
            public string waypoint { get; set; }
            public string list_embarkasi { get; set; }
            public int? armor { get; set; }
            public object id_dislokasi { get; set; }
            public object id_dislokasi_obj { get; set; }
            public float? bahan_bakar { get; set; }
            public float? bahan_bakar_load { get; set; }
            public string kecepatan_maks { get; set; }
            public int? heading { get; set; }
            public string ket_satuan { get; set; }
            public string nama_icon_satuan { get; set; }
            public string width_icon_satuan { get; set; }
            public string height_icon_satuan { get; set; }
            public int? personil { get; set; }
            public PDataSatuanDetailPersonil detail_personil { get; set; }
            public bool tfg { get; set; }
            public bool hidewgs { get; set; }
        }

        public class PDataSatuanDetailPersonil
        {
            public int bintara { get; set; }
            public int tamtama { get; set; }
            public int perwira { get; set; }
        }

        public class PDataSatuanSymbol
        {
            public float? width { get; set; }
            public string color { get; set; }
            public string fontFamily { get; set; }
            public string fontIndex { get; set; }
        }
    }
}

namespace Wargaming.Tacview
{
    [Serializable]
    public class TacviewCSVData
    {
        public string Time { get; set; }
        public double Timestamp { get; set; }
        public double Longtitude { get; set; }
        public double Latitude { get; set; }
        public double Altitude { get; set; }
        public double Roll { get; set; }
        public double Pitch { get; set; }
        public double Yaw { get; set; }

        public string Type { get; set; }
        public string Color { get; set; }
        public string Coalition { get; set; }
        public string Name { get; set; }

    }
}

// --- CONVERTER HELPER (Untuk Seluruh Helper) ---
public partial class HelperConverter
{
    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };

    }
}
// -----------------------------------------------