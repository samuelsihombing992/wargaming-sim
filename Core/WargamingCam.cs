using System;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.Rendering;
using Unity.Mathematics;

using Crest;
using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils;
using Esri.ArcGISMapsSDK.Utils.Math;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.View;
using Esri.GameEngine.Map;
using Esri.GameEngine.Geometry;
using Esri.GameEngine.MapView;
using Esri.HPFramework;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.UI;
using Wargaming.Components.Controller;
using System.Collections.Generic;
using System.Linq;
using SickscoreGames.HUDNavigationSystem;
using Wargaming.TacticalMap.Component.Controller;
using Wargaming.Core.TFG;
using Expanse;
using Wargaming.Components.TimeWeather;
using Wargaming.Components.Playback;

namespace Wargaming.Core
{
    [DisallowMultipleComponent]
    [AddComponentMenu("Main Camera Controller")]
    public class WargamingCam : MonoBehaviour
    {
        public float SCA = 0.00025f;
        public Vector2 scaClamp = new Vector2(0, 0);

        [Header("Cam Settings")]
        public double MinSpeed                  = 1000.0;
        public double MaxSpeed                  = 2000000.0;
        public float zoomSpeed                  = 1;
        public float resetCamDuration           = 5;
        public Vector3 resetCamCoordinate       = new Vector3(0, 0, 0);
        public float camModeDuration            = 2;
        public Vector3 topdownCamRotation       = new Vector3(0, 0, 0);
        public Vector3 isoCamRotation           = new Vector3(0, 0, 0);
        public int edgeBoundary                 = 2;

        [Header("Touch Settings")]
        public Vector3 pinchScaleParam;

        [Header("Cam State")]
        public bool dragMode                    = true;
        public bool tiltMode                    = true;
        public bool scrollMode                  = true;
        public bool edgeScreenMode              = false;

        [Header("Clipping Planes")]
        public bool UpdateClippingPlanes        = true;
        public int ClippingUpdateAltitude       = 20000;

        [Header("Clipping Settings")]
        public float NearClippingPlane          = 0.01f;
        public float FarClippingPlane           = 2000000f;

        [Header("Ocean Renderer")]
        public float MaxOceanHeight             = 150000;
        public float FoamMaxAltitude            = 100;

        [Header("Rebaser")]
        public Camera rebaseCam;
        public float rebaseMaxPosition          = 100000;
        public float rebaseMinAltitude          = 10000;
        public bool onRebase = false;

        [Header("Sky Renderer")]
        //public Volume timeOfDayComponents;
        //public Volume skyVolume;
        public float MaxCloudsHeight            = 50000;
        public AnimationCurve CloudsFadeCurve   = AnimationCurve.EaseInOut(0, 0, 1, 1);

        //public float MaxHorizon;
        //public AnimationCurve SetCloudsOpacity  = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1,1));

        [Header("Camera Follower")]
        public GameObject prefabCamFollower;
        public Expanse.CameraSettings expanseCamSettings;

        [Header("Followed Object")]
        public ArcGISLocationComponent followedObj;
        private int followedObjIndex = 0;

        [NonSerialized] public Transform camFollower;
        private Vector3 camFollowerPosition;
        private Quaternion camFollowerRotation;

        public Camera cameraComponent;

        #if ENABLE_INPUT_SYSTEM
		    public ArcGISCameraControllerComponentActions CameraActions;
		    private InputAction UpControls;
		    private InputAction ForwardControls;
		    private InputAction RightControls;
        #endif

        private float TranslationSpeed = 0.0f;
        private float RotationSpeed = 100.0f;
        private double MouseScrollSpeed = 0.1f;

        private static double MaxCameraHeight = 11000000.0;
        private static double MinCameraHeight = 1.8;
        private static double MaxCameraLatitude = 85.0;

        private double3 lastCartesianPoint = double3.zero;
        private ArcGISPoint lastArcGISPoint = new ArcGISPoint(0, 0, 0, ArcGISSpatialReference.WGS84());
        private double lastDotVC = 0.0f;
        private bool firstDragStep = true;

        private Vector3 lastMouseScreenPosition;
        private bool firstOnFocus = true;

        private ArcGISMapComponent arcGISMapComponent;
        public static ArcGISLocationComponent location;
        private HPTransform hpTransform;
        private ArcGISCameraComponent cameraControllerComponent;

        private AnimationCurve lattitudeMotion = AnimationCurve.EaseInOut(0, 0, 1, 1);
        private AnimationCurve longtitudeMotion = AnimationCurve.EaseInOut(0, 0, 1, 1);
        private AnimationCurve altitudeMotion = AnimationCurve.EaseInOut(0, 0, 1, 1);
        private float motionTime;

        public static double segmentScale = 1;

        private bool isOnReset = false;
        private bool isOnZoomIn = false;
        private bool isOnZoomOut = false;
        private bool isOnSwitchCam = false;
        private bool isOnLocateObject = false;
        private bool onPlotTransform = false;

        private bool atHigherAltitude = false;

        public static WargamingCam instance;
        private void Awake()
        {
            lastMouseScreenPosition = GetMousePosition();

            Application.focusChanged += FocusChanged;

#if ENABLE_INPUT_SYSTEM
            CameraActions = new ArcGISCameraControllerComponentActions();
			UpControls = CameraActions.Move.Up;
			ForwardControls = CameraActions.Move.Forward;
			RightControls = CameraActions.Move.Right;
#endif

            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        async void Start()
        {
            if (arcGISMapComponent == null)
            {
                Debug.LogError("An ArcGISMapComponent could not be found. Please make sure this GameObject is a child of a GameObject with an ArcGISMapComponent attached");

                enabled = false;
                return;
            }

            // Setup Config Data Into Camera
            var startupConfig = StartupConfig.settings;
            if(StartupConfig.settings != null)
            {
                MaxOceanHeight = StartupConfig.settings.OCEAN_MAX_ALTITUDE.GetValueOrDefault(150000);
                FoamMaxAltitude = StartupConfig.settings.FOAM_MAX_ALTITUDE.GetValueOrDefault(100);
                rebaseMaxPosition = StartupConfig.settings.REBASE_MAX_POSITION.GetValueOrDefault(100000);
                rebaseMinAltitude = StartupConfig.settings.REBASE_MIN_ALTITUDE.GetValueOrDefault(10000);
                MaxCloudsHeight = StartupConfig.settings.CLOUDS_MAX_ALTITUDE.GetValueOrDefault(30000);
            }

            EnvironmentsController.GetOcean()._createFoamSim        = false;
            EnvironmentsController.GetOcean()._createDynamicWaveSim = false;

            rebaseCam.gameObject.SetActive(true);   
            GetComponent<ArcGISRebaseComponent>().enabled = false;

            Camera.main.transform.localRotation = Quaternion.Euler(0, 0, 0);

            CloudsFadeCurve.AddKey(new Keyframe { time = MaxCloudsHeight - 20000, value = 5000 });
            CloudsFadeCurve.AddKey(new Keyframe { time = MaxCloudsHeight, value = 1 });

            CloudsFadeCurve.RemoveKey(1);
            CloudsFadeCurve.RemoveKey(0);

            await Task.Delay(100);
            atHigherAltitude = true;

            RebasePosition();
            RebaseAltitude();
        }

        private void Initialize()
        {
            arcGISMapComponent = gameObject.GetComponentInParent<ArcGISMapComponent>();
            hpTransform = GetComponent<HPTransform>();
            location = GetComponent<ArcGISLocationComponent>();
            cameraControllerComponent = GetComponentInChildren<ArcGISCameraComponent>();

            #if ENABLE_INPUT_SYSTEM
			    UpControls.Enable();
			    ForwardControls.Enable();
			    RightControls.Enable();
#endif

            if (arcGISMapComponent == null)
            {
                Debug.LogError("Unable to find a parent ArcGISMapComponent.");

                enabled = false;
                return;
            }

            cameraComponent = GetComponentInChildren<Camera>();
        }

        private void OnEnable()
        {
            Initialize();
        }

        private void OnDisable()
        {
            #if ENABLE_INPUT_SYSTEM
                UpControls.Disable();
			    ForwardControls.Disable();
			    RightControls.Disable();
            #endif
        }

        private void OnTransformParentChanged()
        {
            Initialize();

            Update();
        }

        //private void Update()
        //{
            
           
        //}

        private void Update()
        {
            if (arcGISMapComponent == null) return;

            if (arcGISMapComponent.View.SpatialReference == null)
            {
                // Not functional until we have a spatial reference
                return;
            }

            //if (onRebase)
            //{
            //    GetComponent<ArcGISRebaseComponent>().enabled = false;
            //    onRebase = false;

            //    if(rebaseCam.active)
            //    return;
            //}

            KeyPressEvent();
            FollowCamEvent();

            OnCameraReset();
            OnCameraModeSwitch();
            OnLocateObject();

            if(followedObj == null && !onRebase && !onPlotTransform)
            {
                DragMouseEvent();
                UpdateNavigation();
            }

            UpdateClipping();
            UpdateSky();
            UpdateOceanRender();

            //UpdateCloudsRender();

            RebaseOceanOnNeed();
        }

        //private void LateUpdate()
        //{
        //    if (arcGISMapComponent == null) return;
        //    if(arcGISMapComponent.View.SpatialReference == null) { return; }

        //    RebaseOceanOnNeed();

        //    UpdateClipping();
        //    UpdateSky();
        //    UpdateOceanRender();

        //}

        private Vector3 GetMousePosition()
        {
            if (isOnZoomIn || isOnZoomOut)
            {
                return new Vector3(Screen.width / 2, Screen.height / 2, 0);
            }

            #if ENABLE_INPUT_SYSTEM
			    return Mouse.current.position.ReadValue();
            #else
            return Input.mousePosition;
            #endif
        }

        private double3 EdgeTotalTranslation()
        {
            if (!edgeScreenMode) return double3.zero;

            var forward     = double3.zero;
            var right       = double3.zero;
            var up          = double3.zero;

            var totalTranslation = double3.zero;

            if (Input.mousePosition.x > Screen.width - edgeBoundary)
            {
                // move on +horizontal Axis
                right = hpTransform.Right.ToDouble3() * TranslationSpeed * Time.deltaTime;
            }

            if (Input.mousePosition.x < 0 + edgeBoundary)
            {
                // move on -horizontal Axis
                right = -hpTransform.Right.ToDouble3() * TranslationSpeed * Time.deltaTime;
            }

            if (Input.mousePosition.y > Screen.height - edgeBoundary)
            {
                // move on +vertical Axis
                up = hpTransform.Forward.ToDouble3() * TranslationSpeed * Time.deltaTime;
            }

            if (Input.mousePosition.y < 0 + edgeBoundary)
            {
                // move on -vertical Axis
                up = -hpTransform.Forward.ToDouble3() * TranslationSpeed * Time.deltaTime;
            }

            //forward *= 0;
            totalTranslation += up + right + forward;

            return totalTranslation;
        }

        private double3 GetTotalTranslation()
        {
            var forward = hpTransform.Forward.ToDouble3();
            var right = hpTransform.Right.ToDouble3();
            var up = hpTransform.Up.ToDouble3();

            var totalTranslation = double3.zero;

            #if ENABLE_INPUT_SYSTEM
			    up *= UpControls.ReadValue<float>() * TranslationSpeed * Time.deltaTime;
			    right *= RightControls.ReadValue<float>() * TranslationSpeed * Time.deltaTime;
			    forward *= ForwardControls.ReadValue<float>() * TranslationSpeed * Time.deltaTime;
			    totalTranslation += up + right + forward;
            #else

                Action<string, double3> handleAxis = (axis, vector) =>
                {
                    if (Input.GetAxis(axis) != 0)
                    {
                        totalTranslation += vector * Input.GetAxis(axis) * TranslationSpeed * Time.fixedUnscaledDeltaTime;
                    }
                };

                handleAxis("Vertical", forward);
                handleAxis("Horizontal", right);
                handleAxis("Jump", up);
                handleAxis("Submit", -up);
#endif

            return totalTranslation + EdgeTotalTranslation();
        }

        private float GetMouseScollValue()
        {
            if (isOnZoomIn)
            {
                return zoomSpeed;
            }
            else if (isOnZoomOut)
            {
                return -zoomSpeed;
            }

#if ENABLE_INPUT_SYSTEM
			    return Mouse.current.scroll.ReadValue().y;
#else
            return Input.mouseScrollDelta.y;
            #endif
        }

        private bool IsMouseLeftClicked()
        {
            #if ENABLE_INPUT_SYSTEM
			    return Mouse.current.leftButton.ReadValue() == 1;
            #else
                return Input.GetMouseButton(0);
            #endif
        }

        private bool IsMouseRightClicked()
        {
            #if ENABLE_INPUT_SYSTEM
			    return Mouse.current.rightButton.ReadValue() == 1;
            #else
                return Input.GetMouseButton(1);
            #endif
        }

        #region RESET CAM
        private void OnCameraReset()
        {
            if (!isOnReset) return;

            motionTime += Time.fixedUnscaledDeltaTime * 2;
            if (motionTime >= resetCamDuration)
            {
                motionTime = 0;
                isOnReset = false;
                return;
            }

            // Update the camera
            float lat = lattitudeMotion.Evaluate(motionTime);
            float lng = longtitudeMotion.Evaluate(motionTime);
            float alt = altitudeMotion.Evaluate(motionTime);

            var headingChange = (location.Rotation.Heading > 180) ? 360 : 0;
            var pitchChange = (location.Rotation.Pitch > 180) ? 360 : 0;
            var rollChange = (location.Rotation.Roll > 180) ? 360 : 0;

            location.Position = new ArcGISPoint(lat, lng, alt, ArcGISSpatialReference.WGS84());
            location.Rotation = new ArcGISRotation(
                Mathf.Lerp((float)location.Rotation.Heading, headingChange, (motionTime / resetCamDuration)),
                Mathf.Lerp((float)location.Rotation.Pitch, pitchChange, (motionTime / resetCamDuration)),
                Mathf.Lerp((float)location.Rotation.Roll, rollChange, (motionTime / resetCamDuration))
            );
        }

        public void ResetCamera()
        {
            UnfollowAny();

            if (NoCurrentActionFromCam())
            {
                lattitudeMotion = AnimationCurve.EaseInOut(0, (float)location.Position.X, resetCamDuration, resetCamCoordinate.x);
                longtitudeMotion = AnimationCurve.EaseInOut(0, (float)location.Position.Y, resetCamDuration, resetCamCoordinate.y);
                altitudeMotion = AnimationCurve.EaseInOut(0, (float)location.Position.Z, resetCamDuration, resetCamCoordinate.z);

                isOnReset = true;
            }
        }
        #endregion

        #region LOCATE OBJECT
        private void OnLocateObject()
        {
            if (!isOnLocateObject) return;

            motionTime += Time.fixedUnscaledDeltaTime * 2;
            if (motionTime >= resetCamDuration)
            {
                motionTime = 0;
                isOnLocateObject = false;
                return;
            }

            // Update the camera
            float lat = lattitudeMotion.Evaluate(motionTime);
            float lng = longtitudeMotion.Evaluate(motionTime);
            float alt = altitudeMotion.Evaluate(motionTime);

            location.Position = new ArcGISPoint(lat, lng, alt, ArcGISSpatialReference.WGS84());
        }

        public void LocateObject(GameObject entity)
        {
            UnfollowAny();

            if (NoCurrentActionFromCam())
            {

                followedObjIndex = EntityController.instance.SATUAN.FindIndex(x => x.gameObject == entity);
                if (EntityController.instance.SATUAN[followedObjIndex] != null)
                {
                    var destination = EntityController.instance.SATUAN[followedObjIndex].GetComponent<ArcGISLocationComponent>().Position;

                    lattitudeMotion = AnimationCurve.EaseInOut(0, (float)location.Position.X, resetCamDuration, (float) destination.X);
                    longtitudeMotion = AnimationCurve.EaseInOut(0, (float)location.Position.Y, resetCamDuration, (float) destination.Y);
                    altitudeMotion = AnimationCurve.EaseInOut(0, (float)location.Position.Z, resetCamDuration, (float) destination.Z);

                    isOnLocateObject = true;
                }
            }
        }
        #endregion

        #region CAM MODE
        private void OnCameraModeSwitch()
        {
            if (!isOnSwitchCam) return;

            // Update the camera
            float heading = lattitudeMotion.Evaluate(motionTime);
            float pitch = longtitudeMotion.Evaluate(motionTime);
            float roll = altitudeMotion.Evaluate(motionTime);

            motionTime += Time.fixedUnscaledDeltaTime * 2;
            if (motionTime >= camModeDuration)
            {
                motionTime = 0;
                isOnSwitchCam = false;

                location.Rotation = new ArcGISRotation(lattitudeMotion.Evaluate(camModeDuration), longtitudeMotion.Evaluate(camModeDuration), altitudeMotion.Evaluate(camModeDuration));
                return;
            }

            location.Rotation = new ArcGISRotation(heading, pitch, roll);
        }

        public void SwitchCamMode()
        {
            if (NoCurrentActionFromCam())
            {
                float heading   = (float) location.Rotation.Heading;
                float pitch     = (float) location.Rotation.Pitch;
                float roll      = (float) location.Rotation.Roll;

                // Reuse lattitudeMotion, longtitudeMotion and altitudeMotion as RotationMotion because it's unused at this point;
                if (location.Rotation.Pitch != topdownCamRotation.y)
                {
                    // Jika sedang dalam mode kamera "3D" yang diketahui dari "pitch rotation" kamera tersebut dengan konfigurasi tidak sama
                    lattitudeMotion = AnimationCurve.EaseInOut(0, (heading > topdownCamRotation.x) ? heading : -heading, camModeDuration / 2, heading);
                    longtitudeMotion = AnimationCurve.EaseInOut(0, (pitch > topdownCamRotation.y) ? pitch : -pitch, camModeDuration, topdownCamRotation.y);
                    altitudeMotion = AnimationCurve.EaseInOut(0, (roll > topdownCamRotation.z) ? roll : -roll, camModeDuration / 2, topdownCamRotation.z);
                }
                else
                {
                    // Jika sedang dalam mode kamera "topdown" yang diketahui dari "pitch rotation" kamera tersebut dengan konfigurasi sama
                    lattitudeMotion = AnimationCurve.EaseInOut(0, (heading > isoCamRotation.x) ? heading : -heading, camModeDuration, heading + isoCamRotation.x);
                    longtitudeMotion = AnimationCurve.EaseInOut(0, (pitch > isoCamRotation.y) ? pitch : -pitch, camModeDuration, isoCamRotation.y);
                    altitudeMotion = AnimationCurve.EaseInOut(0, (roll > isoCamRotation.z) ? roll : -roll, camModeDuration, isoCamRotation.z);
                }

                isOnSwitchCam = true;
            }
        }

        public void KeyPressEvent()
        {
            //if (Input.GetKeyDown(KeyCode.M))
            //{
            //    if (TacticalMapController.instance.tacticalCam.gameObject.activeSelf)
            //    {
            //        TacticalMapController.instance.DisableTacticalMap();
            //    }
            //    else
            //    {
            //        TacticalMapController.instance.EnableTacticalMap();
            //    }
            //}
        }

        public void FollowCamEvent()
        {
            if(followedObj != null && !onRebase)
            {
                var camFollowerCamera = camFollower.GetComponentInChildren<Camera>();

                location.Position = followedObj.Position;
                cameraComponent.transform.position = camFollowerCamera.transform.position;
                cameraComponent.transform.rotation = camFollowerCamera.transform.rotation;

                camFollowerCamera.nearClipPlane     = cameraComponent.nearClipPlane;
                camFollowerCamera.farClipPlane      = cameraComponent.farClipPlane;
            }

            segmentScale = Mathf.Clamp((float)location.Position.Z * SCA, scaClamp.x, scaClamp.y);

            if (Input.GetKeyDown(KeyCode.F1))
            {
                FollowNextEntity(-1, EntityController.instance.VEHICLE);
            }
            else if (Input.GetKeyDown(KeyCode.F2))
            {
                FollowNextEntity(1, EntityController.instance.VEHICLE);
            }
            else if (Input.GetKeyDown(KeyCode.F3))
            {
                FollowNextEntity(-1, EntityController.instance.SHIP);
            }
            else if (Input.GetKeyDown(KeyCode.F4))
            {
                FollowNextEntity(1, EntityController.instance.SHIP);
            }
            else if (Input.GetKeyDown(KeyCode.F5))
            {
                FollowNextEntity(-1, EntityController.instance.AIRCRAFT);
            }
            else if (Input.GetKeyDown(KeyCode.F6))
            {
                FollowNextEntity(1, EntityController.instance.AIRCRAFT);
            }
            else if (Input.GetKeyDown(KeyCode.F12))
            {
                UnfollowAny();
            }else if (Input.GetKeyDown(KeyCode.I))
            {
                if (followedObj == null) return;
                DetailEntityController.instance.initObjSatuan(followedObj.GetComponent<ObjekSatuan>());
            }else if(Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.R))
            {
                Debug.Log("Refreshing Route...");
                GameController.instance.RefreshRoute();

                //if (GameplayTFGController.instance != null) GameplayTFGController.instance.RefreshRoute();
            }
        }

        public void FollowNextEntity(int next, List<ObjekSatuan> data)
        {
            if (data.Count <= 0) return;

            followedObjIndex += next;
            if (followedObjIndex < 0)
            {
                followedObjIndex = data.Count - 1;
            }
            else if (followedObjIndex > (data.Count - 1))
            {
                followedObjIndex = 0;
            };

            if (data[followedObjIndex] != null)
            {
                FollowEntity(data[followedObjIndex].transform);
            }
        }

        public void FollowEntityAtIndex(int index)
        {
            if (EntityController.instance.SATUAN.Count <= 0) return;

            followedObjIndex = index;
            if (EntityController.instance.SATUAN[followedObjIndex] != null)
            {
                FollowEntity(EntityController.instance.SATUAN[followedObjIndex].transform);

                HUDNavigationSystem.Instance.PlayerCamera = camFollower.GetComponentInChildren<Camera>();
                HUDNavigationSystem.Instance.PlayerController = camFollower;

                expanseCamSettings.m_ambientProbeCamera = camFollower.GetComponentInChildren<Camera>();

                cameraComponent.enabled = false;
                EnvironmentsController.GetOcean().ViewCamera = camFollower.GetComponentInChildren<Camera>();
            }
        }

        public void FollowEntityByObject(GameObject entity)
        {
            followedObjIndex = EntityController.instance.SATUAN.FindIndex(x => x.nama == entity.GetComponent<ObjekSatuan>().nama);
            if (EntityController.instance.SATUAN[followedObjIndex] != null)
            {
                FollowEntity(entity.transform);

                HUDNavigationSystem.Instance.PlayerCamera = camFollower.GetComponentInChildren<Camera>();
                HUDNavigationSystem.Instance.PlayerController = camFollower;

                expanseCamSettings.m_ambientProbeCamera = camFollower.GetComponentInChildren<Camera>();

                cameraComponent.enabled = false;
                EnvironmentsController.GetOcean().ViewCamera = camFollower.GetComponentInChildren<Camera>();
            }
        }

        public async void FollowEntity(Transform entity)
        {
            if(followedObj != null)
            {
                if (camFollower != null) Destroy(camFollower.gameObject);
            }

            followedObj         = entity.GetComponent<ArcGISLocationComponent>();
            camFollower         = Instantiate(prefabCamFollower, EnvironmentsController.GetMapComponent().transform).transform;

            camFollower.GetComponent<ArcGISLocationComponent>().Position = followedObj.Position;
            camFollower.GetComponent<ArcGISLocationComponent>().Rotation = followedObj.Rotation;
            //camFollower          = Instantiate(prefabCamFollower, entity.transform).transform;

            var camFollowerController = camFollower.GetComponent<FollowCamController>();

            camFollowerController.target = entity;
            camFollowerController.init();
            //camFollowerController.updateSettings = FollowCamController.UpdateSettings.LATE_UPDATE;

            camFollower.name    = "CAM_FOLLOWER";
            cameraComponent.tag = "Untagged";

            cameraComponent.GetComponent<AudioListener>().enabled = false;

            HUDNavigationSystem.Instance.PlayerCamera = camFollower.GetComponentInChildren<Camera>();
            HUDNavigationSystem.Instance.PlayerController = camFollower;

            expanseCamSettings.m_ambientProbeCamera = camFollower.GetComponentInChildren<Camera>();

            cameraComponent.enabled = false;
            EnvironmentsController.GetOcean().ViewCamera = camFollower.GetComponentInChildren<Camera>();

            RebasePosition();
            RebaseAltitude();
            RebaseClouds();

            // Task.Delay(10);
            //camFollower.parent = EnvironmentsController.GetMapComponent().transform;

            //CoordinateWatchController.instance.initEntityInfo();
        }

        public void UnfollowAny()
        {
            Vector3? camFollowerPos = null;
            quaternion? camFollowerRot = null;

            if(followedObj != null)
            {
                if (camFollower != null)
                {
                    camFollowerPos = camFollower.GetChild(0).transform.localPosition;
                    camFollowerRot = camFollower.GetChild(0).transform.localRotation;

                    Destroy(camFollower.gameObject);
                }
            }

            if (camFollowerPos.HasValue && camFollowerRot.HasValue)
            {
                var camHPPosition = GetComponent<HPTransform>().UniversePosition;

                GetComponent<HPTransform>().UniversePosition = new double3(camHPPosition.x + camFollowerPos.Value.x, camHPPosition.y + camFollowerPos.Value.y, camHPPosition.z + camFollowerPos.Value.z);
                GetComponent<HPTransform>().UniverseRotation = camFollowerRot.Value;
            }

            cameraComponent.enabled = true;
            cameraComponent.tag     = "MainCamera";

            cameraComponent.transform.localPosition = Vector3.zero;
            cameraComponent.transform.localRotation = Quaternion.identity;

            HUDNavigationSystem.Instance.PlayerCamera = Camera.main;
            HUDNavigationSystem.Instance.PlayerController = Camera.main.transform.parent;

            expanseCamSettings.m_ambientProbeCamera = Camera.main;

            EnvironmentsController.GetOcean().ViewCamera = Camera.main;

            followedObj = null;
            camFollower = null;

            cameraComponent.GetComponent<AudioListener>().enabled = true;

            //CoordinateWatchController.instance.destroyEntityInfo();
        }
        #endregion

        #region ZOOM CAM
        public void OnZoomInHold()
        {
            if (NoCurrentActionFromCam())
            {
                isOnZoomIn = true;
            }
        }

        public void OnZoomInRelease()
        {
            isOnZoomIn = false;
        }

        public void OnZoomOutHold()
        {
            if (NoCurrentActionFromCam())
            {
                isOnZoomOut = true;
            }
        }

        public void OnZoomOutRelease()
        {
            isOnZoomOut = false;
        }
        #endregion

        public void DragMode(Toggle toggle)
        {
            dragMode = toggle.isOn;
        }

        public void TiltMode(Toggle toggle)
        {
            tiltMode = toggle.isOn;
        }

        public void EdgeScreenMode(Toggle toggle)
        {
            edgeScreenMode = toggle.isOn;
        }

        private bool NoCurrentActionFromCam()
        {
            if (isOnReset == false && isOnZoomIn == false && isOnZoomOut == false && isOnSwitchCam == false)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
		/// Move the camera based on user input
		/// </summary>
		private void UpdateNavigation()
        {
            var altitude = arcGISMapComponent.View.AltitudeAtCartesianPosition(Position);
            UpdateSpeed(altitude);

            var totalTranslation = GetTotalTranslation();
            var scrollVal = GetMouseScollValue();

            if ((scrollVal != 0.0 && !UICanvasSettings.IsRaycastingWithUI() && scrollMode) || isOnZoomIn || isOnZoomOut)
            {
                var towardsMouse = GetMouseRayCastDirection();
                var delta = Math.Max(1.0, (altitude - MinCameraHeight)) * MouseScrollSpeed * GetMouseScollValue();
                totalTranslation += towardsMouse * delta;
            }

            if(pinchScaleParam.z != 0)
            {
                var towardsMouse = GetMouseRayCastDirection();
                var delta = Math.Max(1.0, (altitude - MinCameraHeight)) * MouseScrollSpeed * pinchScaleParam.z;
                totalTranslation += towardsMouse * delta;

                pinchScaleParam = Vector3.zero;
            }

            if (!totalTranslation.Equals(double3.zero))
            {
                MoveCamera(totalTranslation);
            }
        }

        /// <summary>
		/// Move the camera
		/// </summary>
		private void MoveCamera(double3 movDir)
        {
            var distance = math.length(movDir);
            movDir /= distance;

            var cameraPosition = Position;
            var cameraRotation = Rotation;

            if (arcGISMapComponent.MapType == Esri.GameEngine.Map.ArcGISMapType.Global)
            {
                var spheroidData = arcGISMapComponent.View.SpatialReference.SpheroidData;
                var nextArcGISPoint = arcGISMapComponent.View.WorldToGeographic(movDir + cameraPosition);

                if (nextArcGISPoint.Z > MaxCameraHeight)
                {
                    var point = new ArcGISPoint(nextArcGISPoint.X, nextArcGISPoint.Y, MaxCameraHeight, nextArcGISPoint.SpatialReference);
                    cameraPosition = arcGISMapComponent.View.GeographicToWorld(point);
                }
                else if (nextArcGISPoint.Z < MinCameraHeight)
                {
                    var point = new ArcGISPoint(nextArcGISPoint.X, nextArcGISPoint.Y, MinCameraHeight, nextArcGISPoint.SpatialReference);
                    cameraPosition = arcGISMapComponent.View.GeographicToWorld(point);
                }
                else
                {
                    cameraPosition += movDir * distance;
                }

                var newENUReference = arcGISMapComponent.View.GetENUReference(cameraPosition);
                var oldENUReference = arcGISMapComponent.View.GetENUReference(Position);

                cameraRotation = math.mul(math.inverse(oldENUReference.GetRotation()), cameraRotation);
                cameraRotation = math.mul(newENUReference.GetRotation(), cameraRotation);
            }
            else
            {
                cameraPosition += movDir * distance;
            }

            Position = cameraPosition;
            Rotation = cameraRotation;
        }

        private void DragMouseEvent()
        {

            var cartesianPosition = Position;
            var cartesianRotation = Rotation;

            var deltaMouse = GetMousePosition() - lastMouseScreenPosition;

            if (!firstOnFocus)
            {
                if (IsMouseLeftClicked() && !UICanvasSettings.IsRaycastingWithUI() && dragMode)
                {
                    if (deltaMouse != Vector3.zero)
                    {
                        if (arcGISMapComponent.MapType == ArcGISMapType.Global)
                        {
                            GlobalDragging(ref cartesianPosition, ref cartesianRotation);
                        }
                        else if (arcGISMapComponent.MapType == ArcGISMapType.Local)
                        {
                            LocalDragging(ref cartesianPosition);
                        }
                    }
                }
                else if (IsMouseRightClicked() && !UICanvasSettings.IsRaycastingWithUI() && tiltMode)
                {
                    if (!deltaMouse.Equals(Vector3.zero))
                    {
                        RotateAround(ref cartesianPosition, ref cartesianRotation, deltaMouse);
                    }
                }
                else
                {
                    firstDragStep = true;
                }
            }
            else
            {
                firstOnFocus = false;
            }

            Position = cartesianPosition;
            Rotation = cartesianRotation;

            lastMouseScreenPosition = GetMousePosition();
        }

        private void LocalDragging(ref double3 cartesianPosition)
        {
            var worldRayDir = GetMouseRayCastDirection();
            var isIntersected = Geometry.RayPlaneIntersection(cartesianPosition, worldRayDir, double3.zero, math.up(), out var intersection);

            if (isIntersected && intersection >= 0)
            {
                double3 cartesianCoord = cartesianPosition + worldRayDir * intersection;

                var delta = firstDragStep ? double3.zero : lastCartesianPoint - cartesianCoord;

                lastCartesianPoint = cartesianCoord + delta;
                cartesianPosition += delta;
                firstDragStep = false;
            }
        }

        private void GlobalDragging(ref double3 cartesianPosition, ref quaternion cartesianRotation)
        {
            var spheroidData = arcGISMapComponent.View.SpatialReference.SpheroidData;
            var worldRayDir = GetMouseRayCastDirection();
            var isIntersected = Geometry.RayEllipsoidIntersection(spheroidData, cartesianPosition, worldRayDir, 0, out var intersection);

            if (isIntersected && intersection >= 0)
            {
                var oldENUReference = arcGISMapComponent.View.GetENUReference(cartesianPosition);

                var geoPosition = arcGISMapComponent.View.WorldToGeographic(cartesianPosition);

                double3 cartesianCoord = cartesianPosition + worldRayDir * intersection;
                var currentGeoPosition = arcGISMapComponent.View.WorldToGeographic(cartesianCoord);

                var visibleHemisphereDir = math.normalize(arcGISMapComponent.View.GeographicToWorld(new ArcGISPoint(geoPosition.X, 0, 0, geoPosition.SpatialReference)));

                double dotVC = math.dot(cartesianCoord, visibleHemisphereDir);
                lastDotVC = firstDragStep ? dotVC : lastDotVC;

                double deltaX = firstDragStep ? 0 : lastArcGISPoint.X - currentGeoPosition.X;
                double deltaY = firstDragStep ? 0 : lastArcGISPoint.Y - currentGeoPosition.Y;

                deltaY = Math.Sign(dotVC) != Math.Sign(lastDotVC) ? 0 : deltaY;


                lastArcGISPoint = new ArcGISPoint(currentGeoPosition.X + deltaX, currentGeoPosition.Y + deltaY, lastArcGISPoint.Z, lastArcGISPoint.SpatialReference);


                var YVal = geoPosition.Y + (dotVC <= 0 ? -deltaY : deltaY);
                YVal = Math.Abs(YVal) < MaxCameraLatitude ? YVal : (YVal > 0 ? MaxCameraLatitude : -MaxCameraLatitude);

                geoPosition = new ArcGISPoint(geoPosition.X + deltaX, YVal, geoPosition.Z, geoPosition.SpatialReference);

                cartesianPosition = arcGISMapComponent.View.GeographicToWorld(geoPosition);

                var newENUReference = arcGISMapComponent.View.GetENUReference(cartesianPosition);
                cartesianRotation = math.mul(math.inverse(oldENUReference.GetRotation()), cartesianRotation);
                cartesianRotation = math.mul(newENUReference.GetRotation(), cartesianRotation);

                firstDragStep = false;
                lastDotVC = dotVC;
            }
        }

        private void RotateAround(ref double3 cartesianPosition, ref quaternion cartesianRotation, Vector3 deltaMouse)
        {
            var ENUReference = arcGISMapComponent.View.GetENUReference(cartesianPosition).ToMatrix4x4();

            Vector2 angles;

            angles.x = deltaMouse.x / (float)Screen.width * RotationSpeed;
            angles.y = deltaMouse.y / (float)Screen.height * RotationSpeed;

            angles.y = Mathf.Min(Mathf.Max(angles.y, -90.0f), 90.0f);

            var right = Matrix4x4.Rotate(cartesianRotation).GetColumn(0);

            var rotationY = Quaternion.AngleAxis(angles.x, ENUReference.GetColumn(1));
            var rotationX = Quaternion.AngleAxis(-angles.y, right);

            cartesianRotation = rotationY * rotationX * cartesianRotation;
        }

        private double3 GetMouseRayCastDirection()
        {
            var forward = hpTransform.Forward.ToDouble3();
            var right = hpTransform.Right.ToDouble3();
            var up = hpTransform.Up.ToDouble3();

            var camera = cameraComponent;

            var view = new double4x4
            (
                math.double4(right, 0),
                math.double4(up, 0),
                math.double4(forward, 0),
                math.double4(double3.zero, 1)
            );

            var proj = camera.projectionMatrix.inverse.ToDouble4x4();

            proj.c2.w *= -1;
            proj.c3.z *= -1;

            var MousePosition = GetMousePosition();
            double3 ndcCoord = new double3(2.0 * (MousePosition.x / Screen.width) - 1.0, 2.0 * (MousePosition.y / Screen.height) - 1.0, 1);
            double3 viewRayDir = math.normalize(proj.HomogeneousTransformPoint(ndcCoord));
            return view.HomogeneousTransformVector(viewRayDir);
        }

        private void FocusChanged(bool isFocus)
        {
            firstOnFocus = true;
        }

        private void UpdateSpeed(double height)
        {
            var msMaxSpeed = (MaxSpeed * 1000) / 3600;
            var msMinSpeed = (MinSpeed * 1000) / 3600;
            TranslationSpeed = (float)(Math.Pow(Math.Min((height / 100000.0), 1), 2.0) * (msMaxSpeed - msMinSpeed) + msMinSpeed);
        }

        private void UpdateClipping()
        {
            if (cameraControllerComponent == null) return;
            if (UpdateClippingPlanes && location.Position.Z >= ClippingUpdateAltitude)
            {
                cameraControllerComponent.UpdateClippingPlanes = true;
            }
            else
            {
                cameraControllerComponent.UpdateClippingPlanes = false;
                cameraComponent.nearClipPlane = NearClippingPlane;
                cameraComponent.farClipPlane = FarClippingPlane;
            }
        }

        private void UpdateSky()
        {
            if (EnvironmentsController.GetCloudsGroup() == null) return;
            if (TimeWeatherController.instance)
            {
                if (!TimeWeatherController.useClouds) return;
            }

            if (location.Position.Z >= MaxCloudsHeight + 20000)
            {
                EnvironmentsController.GetCloudsGroup().SetActive(false);
                EnvironmentsController.GetSkyVolume().enabled = false;
                return;
            }

            if (!EnvironmentsController.GetCloudsGroup().activeSelf)
            {
                EnvironmentsController.GetCloudsGroup().SetActive(true);
                EnvironmentsController.GetSkyVolume().enabled = true;
            }

            if (location.Position.Z >= (MaxCloudsHeight - 20000) && (location.Position.Z < MaxCloudsHeight + 20000))
            {

                var volumetricCloudVolume = EnvironmentsController.GetCloudsGroup().GetComponentInChildren<ProceduralCloudVolume>();
                if (volumetricCloudVolume != null)
                {
                    volumetricCloudVolume.m_density = CloudsFadeCurve.Evaluate((float)location.Position.Z);
                }

                var flatCloudVolume = EnvironmentsController.GetCloudsGroup().GetComponentInChildren<TextureCloudPlane>();
                if(flatCloudVolume != null)
                {
                    flatCloudVolume.m_density = CloudsFadeCurve.Evaluate((float)location.Position.Z);
                }

                return;
            }
        }

        //private void UpdateSky()
        //{
        //    if (timeOfDayComponents == null) return;

        //    var volumeProfile = timeOfDayComponents.sharedProfile;
        //    volumeProfile.TryGet<CloudLayer>(out var cloud);

        //    cloud.opacity.value = SetCloudsOpacity.Evaluate((float)location.Position.Z);
        //}

        private void UpdateOceanRender()
        {
            var ocean = EnvironmentsController.GetOcean();

            if (ocean == null) return;
            if(location.Position.Z >= MaxOceanHeight)
            {
                ocean.gameObject.SetActive(false);
                return;
            }
            else if(location.Position.Z >= FoamMaxAltitude)
            {
                ocean._createFoamSim = false;
                ocean._createDynamicWaveSim = false;
            }
            else
            {
                ocean._createFoamSim = true;
                ocean._createDynamicWaveSim = true;
            }

            if(!ocean.gameObject.activeSelf) ocean.gameObject.SetActive(true);
        }

        //private void UpdateCloudsRender()
        //{
        //    if (cloudsGroup == null) return;
        //    if (TimeWeatherController.instance)
        //    {
        //        if (!TimeWeatherController.instance.toggleClouds.isOn) return;
        //    }

        //    if (location.Position.Z >= MaxOceanHeight)
        //    {
        //        cloudsGroup.SetActive(false);
        //        return;
        //    }


        //    if (!cloudsGroup.activeSelf) cloudsGroup.SetActive(true);
        //}

        private void RebaseOceanOnNeed()
        {
            if (location.Position.Z > FoamMaxAltitude && !atHigherAltitude) atHigherAltitude = true;

            if ((transform.position.x > rebaseMaxPosition || transform.position.x < -rebaseMaxPosition || transform.position.z > rebaseMaxPosition || transform.position.z < -rebaseMaxPosition) && location.Position.Z <= rebaseMinAltitude)
            {
                //if (atHigherAltitude) RebaseAltitude();

                RebaseClouds();
                RebasePosition();
                RebaseAltitude();
                return;
            }

            if (location.Position.Z <= FoamMaxAltitude && atHigherAltitude)
            {
                RebaseAltitude();
            }
        }

        //private async void RebasePosition()
        //{
        //    if (onRebase) return;

        //    onRebase = true;
        //    bool s = PlaybackController.IsPlaying();
        //    PlaybackController.instance.OnPlaybackAction("pause");

        //    //Debug.Log("Rebase Position");
        //    GetComponent<ArcGISRebaseComponent>().enabled = true;

        //    await Task.Delay(10);
        //    GetComponent<ArcGISRebaseComponent>().enabled = false;
        //    onRebase = false;

        //    if (MissionController.instance != null)
        //    {
        //        MissionController.instance.RebaseMission();
        //    }

        //    onRebase = false;
        //    await Task.Delay(100);
        //    if (s) { PlaybackController.instance.OnPlaybackAction("play"); }
        //}

        private async void RebasePosition()
        {
            onRebase = true;

            GetComponent<ArcGISRebaseComponent>().enabled = true;

            await Task.Delay(10);
            GetComponent<ArcGISRebaseComponent>().enabled = false;

            if(MissionController.instance != null)
            {
                MissionController.instance.RebaseMission();
            }

            onRebase = false;
        }

        private async void RebaseAltitude()
        {
            onRebase = true;

            rebaseCam.gameObject.SetActive(true);

            EnvironmentsController.GetOcean().ViewCamera = rebaseCam;
            var rebaseCamPos = rebaseCam.GetComponent<ArcGISLocationComponent>();

            rebaseCamPos.Position = new ArcGISPoint(location.Position.X, location.Position.Y, 3, ArcGISSpatialReference.WGS84());
            rebaseCam.GetComponent<ArcGISRebaseComponent>().enabled = true;
            atHigherAltitude = false;

            await Task.Delay(10);
            rebaseCam.GetComponent<ArcGISRebaseComponent>().enabled = false;
            rebaseCam.gameObject.SetActive(false);

            if (MissionController.instance != null) MissionController.instance.RebaseMission();

            if(camFollower != null)
            {
                EnvironmentsController.GetOcean().ViewCamera = camFollower.GetComponentInChildren<Camera>();
            }
            else
            {
                EnvironmentsController.GetOcean().ViewCamera = Camera.main;
            }

            onRebase = false;
        }

        //private async void RebaseAltitude()
        //{
        //    if (onRebase) return;

        //    onRebase = true;
        //    bool s = PlaybackController.IsPlaying();
        //    PlaybackController.instance.OnPlaybackAction("pause");

        //    Debug.Log("Rebase Altitude");
        //    rebaseCam.gameObject.SetActive(true);

        //    EnvironmentsController.GetOcean().ViewCamera = rebaseCam;
        //    var rebaseCamPos = rebaseCam.GetComponent<ArcGISLocationComponent>();

        //    rebaseCamPos.Position = new ArcGISPoint(location.Position.X, location.Position.Y, 3, ArcGISSpatialReference.WGS84());
        //    rebaseCam.GetComponent<ArcGISRebaseComponent>().enabled = true;
        //    atHigherAltitude = false;

        //    await Task.Delay(10);
        //    rebaseCam.GetComponent<ArcGISRebaseComponent>().enabled = false;
        //    rebaseCam.gameObject.SetActive(false);

        //    if (MissionController.instance != null) MissionController.instance.RebaseMission();

        //    if (camFollower != null)
        //    {
        //        EnvironmentsController.GetOcean().ViewCamera = camFollower.GetComponentInChildren<Camera>();
        //    }
        //    else
        //    {
        //        EnvironmentsController.GetOcean().ViewCamera = Camera.main;
        //    }

        //    onRebase = false;
        //    await Task.Delay(100);
        //    if (s) { PlaybackController.instance.OnPlaybackAction("play"); }
        //    return;
        //}

        private void RebaseClouds()
        {
            //if (GlobalComponent.getCloudsGroup() != null)
            //{
            //    GlobalComponent.getCloudsGroup().GetComponentInParent<HPTransform>().LocalPosition =
            //        new double3(
            //            GetComponent<HPTransform>().LocalPosition.x,
            //            GlobalComponent.getCloudsGroup().GetComponentInParent<HPTransform>().LocalPosition.y,
            //            GetComponent<HPTransform>().LocalPosition.z
            //        );
            //}
        }

        /// <summary>
        /// Disable Main Camera ketika TransformHandles untuk Plotting Sedang Aktif
        /// (Sedang Aktif = Sedang Selecting Object)
        /// </summary>
        public void DisableOnEnterPlotTransform()
        {
            if (onPlotTransform) return;

            //Debug.Log("Enter Mode Transform Plotting");
            onPlotTransform = true;
        }


        /// <summary>
        /// Enable kembali Main Camera setelah TransformHandles untuk Plotting Mati
        /// </summary>
        public void EnableOnExitPlotTransform()
        {
            if (!onPlotTransform) return;

            //Debug.Log("Exit Mode Transform Plotting");
            onPlotTransform = false;
        }

        #region Properties
        /// <summary>
        /// Get/set the camera position in world coordinates
        /// </summary>
        private double3 Position
        {
            get
            {
                return hpTransform.UniversePosition;
            }
            set
            {
                hpTransform.UniversePosition = value;
            }
        }

        /// <summary>
        /// Get/set the camera rotation
        /// </summary>
        private quaternion Rotation
        {
            get
            {
                return hpTransform.UniverseRotation;
            }
            set
            {
                hpTransform.UniverseRotation = value;
            }
        }

        #endregion
    }
}
