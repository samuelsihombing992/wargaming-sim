using System;
using System.Collections.Generic;

using UnityEngine;

using FluffyUnderware.Curvy.Controllers;
using FluffyUnderware.Curvy;

using static Wargaming.Core.GlobalParam.GlobalHelper;
using Wargaming.Components.Playback;
using Wargaming.Components.CargoHold;
using WGSBundling.Bundles.CargoHold;
using WGSBundling.Bundles.Helipad;

public class MissionWalker : MonoBehaviour
{
    public enum RouteType { Pergerakan, Embarkasi, Debarkasi, Penerjunan_TFG, Penerjunan_WGS }
    //public bool syncNode = false;

    public string id;
    public string missionId;
    public RouteType jenis;
    public double speed;

    public SplineController objEntity;
    public List<MissionWalkerParam> DATA = new List<MissionWalkerParam>();

    public Transform insideEntity;

    public Transform START_AT_PARENT;
    public int? START_PARENT_NODE;

    public Transform END_AT_PARENT;
    public int? END_PARENT_NODE;

    public int cargoDoorInAction = 0;

    public bool takeoffStart = false;
    public bool landingEnd = false;

    public int multiplier = 1;

    //public float takeoffAlt = 0;
    //public float landingAlt = 0;

    private void Start()
    {
        multiplier = 550;
    }


    //public NodeCargoHold parent;

    //[SerializeField] private bool isActive = false;

    //public bool embarkPathGenerated = false;
    //public bool debarkPathGenerated = false;

    /// <summary>
    /// Tambahan Index Baru Ke Dalam Data Misi(X)
    /// </summary>
    /// <param name="time"></param>
    /// <param name="coordinate"></param>
    public void AddNewPos(string time, Vector3 coordinate)
    {
        var newPos = new MissionWalkerParam();
        newPos.time = time;
        newPos.datetime = DateTime.Parse(time);
        newPos.coordinate = coordinate;

        DATA.Add(newPos);
    }

    private void LateUpdate()
    {
        if (!PlaybackController.IsPlaying()) return;

        if (START_AT_PARENT != null)
        {
            var helipadData = START_AT_PARENT.GetComponent<NodeHelipad>();

            var hoverPoint = helipadData.nodes[END_PARENT_NODE.GetValueOrDefault(0)].nodeHoverArea;
            transform.GetChild(0).position = new Vector3(hoverPoint.position.x, hoverPoint.position.y - 1000, hoverPoint.position.z);
        }

        if(END_AT_PARENT != null)
        {
            var helipadData = END_AT_PARENT.GetComponent<NodeHelipad>();

            var hoverPoint = helipadData.nodes[END_PARENT_NODE.GetValueOrDefault(0)].nodeHoverArea;
            Vector3 lookDir = hoverPoint.forward;

            transform.GetChild(transform.childCount - 1).position = new Vector3(hoverPoint.position.x, hoverPoint.position.y - 1000, hoverPoint.position.z);
            transform.GetChild(transform.childCount - 2).position = hoverPoint.position + (lookDir * multiplier);
            transform.GetChild(transform.childCount - 3).position = transform.GetChild(transform.childCount - 2).position + (lookDir * multiplier);
        }
    }

    private void RefreshRouteSpline()
    {
        var spline = GetComponent<CurvySpline>();
        if (PlaybackController.CURRENT_TIME < DATA[0].datetime || PlaybackController.CURRENT_TIME > DATA[DATA.Count - 1].datetime)
        {
            if (spline.CacheDensity > 1)
            {
                spline.CacheDensity = 1;
            }

            return;
        }
        else
        {
            if (spline.CacheDensity < 2)
            {
                spline.CacheDensity = 2;
            }

            return;
        }
    }

    private void FixedUpdate()
    {
        RefreshRouteSpline();
        //CheckEmbarkDebark();

    //    if (syncNode && insideEntity != null && (jenis == RouteType.Embarkasi || jenis == RouteType.Debarkasi))
    //    {
    //        var cargoHold = insideEntity.GetComponent<NodeCargoHold>();
    //        if (cargoHold != null) SyncCargoHoldPath(cargoHold);
    //    }

            //return;
            //if (objEntity == null || DATA.Count < 2) return;

            //var cargoHoldController = GetComponent<CargoHoldController>();

            //var stepAlreadyPlayed = false;
            //if (cargoHoldController != null && parent != null)
            //{
            //    if (cargoHoldController.animStateRun != null)
            //    {
            //        if ((cargoHoldController.animStateRun <= PlaybackController.instance.CURRENT_TIME) && (PlaybackController.instance.CURRENT_TIME <= DATA[DATA.Count - 1].datetime))
            //        {
            //            if (jenis == RouteType.Debarkasi)
            //            {
            //                // Visualisasi Debarkasi Ketika Masuk pada Waktunya
            //                await cargoHoldController.PrepareDebarkasiStep(parent, this);
            //            }
            //            else if (jenis == RouteType.Embarkasi)
            //            {
            //                // Visualisasi Embarkasi Ketika Masuk pada Waktunya
            //                await cargoHoldController.PrepareEmbarkasiStep(parent, this);
            //            }

            //            stepAlreadyPlayed = true;
            //        }
            //    }
            //}

            //if ((DATA[0].datetime <= PlaybackController.instance.CURRENT_TIME) && (PlaybackController.instance.CURRENT_TIME <= DATA[DATA.Count - 1].datetime))
            //{
            //    if (PlaybackController.instance.isPlaying || PlaybackController.instance.isSliding)
            //    {
            //        if (parent != null && !stepAlreadyPlayed)
            //        {
            //            if (jenis == RouteType.Debarkasi)
            //            {
            //                // Visualisasi Debarkasi Ketika Masuk pada Waktunya
            //                await cargoHoldController.PrepareDebarkasiStep(parent, this);
            //            }
            //            else if (jenis == RouteType.Embarkasi)
            //            {
            //                // Visualisasi Embarkasi Ketika Masuk pada Waktunya
            //                await cargoHoldController.PrepareEmbarkasiStep(parent, this);
            //            }
            //        }

            //        objEntity.Spline = GetComponent<CurvySpline>();
            //        if (!isActive)
            //        {
            //            GetComponent<CurvySpline>().UpdateIn = CurvyUpdateMethod.FixedUpdate;

            //            isActive = true;

            //            DoDebarkasi();

            //            //if (jenis == RouteType.Debarkasi || jenis == RouteType.Pergerakan)
            //            //{

            //            //}
            //        }
            //    }
            //}
            //else if ((PlaybackController.instance.CURRENT_TIME < DATA[0].datetime || PlaybackController.instance.CURRENT_TIME > DATA[DATA.Count - 1].datetime))
            //{
            //    if(isActive)
            //    {
            //        isActive = false;

            //        if (PlaybackController.instance.isPlaying || PlaybackController.instance.isSliding)
            //        {
            //            if (PlaybackController.instance.CURRENT_TIME < DATA[0].datetime)
            //            {
            //                if (jenis == RouteType.Embarkasi)
            //                {
            //                    DoDebarkasi();
            //                }
            //                else if (jenis == RouteType.Debarkasi)
            //                {
            //                    DoEmbarkasi();
            //                }
            //            }
            //            else if (PlaybackController.instance.CURRENT_TIME > DATA[DATA.Count - 1].datetime)
            //            {
            //                if (jenis == RouteType.Embarkasi)
            //                {
            //                    DoEmbarkasi();
            //                }
            //                else if (jenis == RouteType.Debarkasi)
            //                {
            //                    DoDebarkasi();
            //                }
            //            }
            //        }

            //        GetComponent<CurvySpline>().UpdateIn = CurvyUpdateMethod.None;
            //    }
            //}
    }

    //private void SyncCargoHoldPath(NodeCargoHold cargoHold)
    //{
    //    for(int i=0; i < cargoHold.doors)
    //    {

    //    }

    //    if(jenis == RouteType.Embarkasi)
    //    {
                
    //    }else if(jenis == RouteType.Debarkasi)
    //    {

    //    }
    //}

    //private void DoEmbarkasi()
    //{
    //    EntityController.instance.EmbarkEntity(objEntity.gameObject);

    //    //if (GetComponent<DebarkEmbarkWalker>() != null)
    //    //{
    //    //    GetComponent<DebarkEmbarkWalker>().ProsesDebarkasi();
    //    //}
    //}

    //private void DoDebarkasi()
    //{
    //    EntityController.instance.DebarkEntity(objEntity.gameObject);

    //    //if (GetComponent<DebarkEmbarkWalker>() != null)
    //    //{
    //    //    GetComponent<DebarkEmbarkWalker>().ProsesDebarkasi();
    //    //}
    //}

    //public void RefreshMission()
    //{
    //    if (parent == null) return;

    //    var cargoHoldController = GetComponent<CargoHoldController>();
    //    if (cargoHoldController == null) return;
    //    if (cargoHoldController.animStateRun == null) return;

    //    var cargoHold = parent.GetComponent<NodeCargoHold>();
    //    if (cargoHoldController.animStateRun > PlaybackController.instance.CURRENT_TIME)
    //    {
    //        ResetCargoHoldAnimation(cargoHold, "Normal", false);
    //    }
    //    else if(PlaybackController.instance.CURRENT_TIME > DATA[DATA.Count - 1].datetime)
    //    {
    //        if (jenis != RouteType.Debarkasi)
    //        {
    //            ResetCargoHoldAnimation(cargoHold, "Normal", false);
    //        }
    //        else
    //        {
    //            ResetCargoHoldAnimation(cargoHold, "RampDown", true, true);
    //        }
    //    }
    //}

    //private void ResetCargoHoldAnimation(NodeCargoHold cargoHold, string anim, bool state, bool forced = false)
    //{
    //    if (cargoHold != null)
    //    {
    //        for (int i = 0; i < cargoHold.doors.Count; i++)
    //        {
    //            var animator = cargoHold.doors[i].obj.GetComponent<Animator>();
    //            if (!animator.GetCurrentAnimatorStateInfo(0).IsName(anim))
    //            {
    //                animator.Play(anim);
    //                cargoHold.doors[i].state = state;

    //                if (forced) animator.playbackTime = 9999;
    //            }
    //        }
    //    }
    //}
}
