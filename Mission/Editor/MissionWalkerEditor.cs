#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(MissionWalker))]
public class MissionWalkerEditor : Editor
{
    ReorderableList orderList;

    private void OnEnable()
    {
        if (target == null) return;

        orderList = new ReorderableList(serializedObject, serializedObject.FindProperty("data"), true, true, true, true);

        orderList.drawElementCallback = DrawListItems; // Delegate to draw the elements on the list
        orderList.drawHeaderCallback = DrawHeader;
    }

    void DrawListItems(Rect rect, int index, bool isActive, bool isFocused)
    {
        SerializedProperty element = orderList.serializedProperty.GetArrayElementAtIndex(index);

        EditorGUI.PropertyField(
          new Rect(rect.x, rect.y, 90, EditorGUIUtility.singleLineHeight),
          element.FindPropertyRelative("time"), GUIContent.none
        );

        EditorGUI.PropertyField(
          new Rect(rect.x + 92, rect.y, rect.width - 92, EditorGUIUtility.singleLineHeight),
          element.FindPropertyRelative("coordinate"), GUIContent.none
        );
    }

    void DrawHeader(Rect rect)
    {
        string name = "Parameter";
        EditorGUI.LabelField(rect, name);
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        serializedObject.Update(); // Update the array property's representation in the inspector

        orderList.DoLayoutList(); // Have the ReorderableList do its work

        // We need to call this so that changes on the Inspector are saved by Unity.
        serializedObject.ApplyModifiedProperties();
    }
}
#endif