using FluffyUnderware.Curvy;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wargaming.Components.Playback;
using WGSBundling.Bundles.CargoHold;
using WGSBundling.Bundles.Helipad;
using static Wargaming.Core.GlobalParam.GlobalHelper;

public class RootMissionWalker : MonoBehaviour
{
    public enum RouteType { Pergerakan, Embarkasi, Debarkasi, Linud, CDS, Penerjunan_TFG, Penerjunan_WGS }

    [Header("Data")]
    public string id;
    public string missionId;
    public RouteType jenis;
    public double speed;

    [Header("Waypoints")]
    public List<MissionWalkerParam> DATA = new List<MissionWalkerParam>();

    [Header("Sim Properties")]
    public bool takeoff = false;
    public bool landing = false;

    [Header("Embark Debark Properties")]
    public Transform target;

    private CurvySpline spline;

    private void Start()
    {
        spline = GetComponent<CurvySpline>();
    }

    public void AddNewPos(string time, Vector3 coordinate)
    {
        var newPos = new MissionWalkerParam();
        newPos.time = time;
        newPos.datetime = DateTime.Parse(time);
        newPos.coordinate = coordinate;

        DATA.Add(newPos);
    }

    private void FixedUpdate()
    {
        if (DATA == null || DATA.Count <= 0) return;

        CheckSplineRoute();
    }

    private void CheckSplineRoute()
    {
        if (PlaybackController.CURRENT_TIME < DATA[0].datetime || PlaybackController.CURRENT_TIME > DATA[DATA.Count - 1].datetime)
        {
            if (spline.CacheDensity > 1)
            {
                spline.CacheDensity = 1;
            }

            if (spline.enabled) { spline.enabled = false; }

            return;
        }
        else
        {
            if (spline.CacheDensity < 2)
            {
                spline.CacheDensity = 2;
            }

            if (!spline.enabled) { spline.enabled = true; }

            return;
        }

        if (target != null)
        {
            if(jenis == RouteType.Embarkasi || jenis == RouteType.Debarkasi)
            {
                var targetRamp = target.GetComponent<CargoRampWatcher>();
                if (PlaybackController.CURRENT_TIME >= DATA[0].datetime.AddMinutes(-1) && PlaybackController.CURRENT_TIME <= DATA[DATA.Count - 1].datetime.AddMinutes(1) && target != null && targetRamp != null)
                {
                    if (!targetRamp.usedByMission.Find(x => x == this))
                    {
                        targetRamp.usedByMission.Add(this);
                    }
                }
                else if (targetRamp != null)
                {
                    targetRamp.usedByMission.Remove(this);
                }
            }else if(jenis == RouteType.Pergerakan)
            {
                if(PlaybackController.CURRENT_TIME < DATA[0].datetime || PlaybackController.CURRENT_TIME > DATA[DATA.Count - 1].datetime)
                {
                    var targetHelipad = target.GetComponent<NodeHelipad>();
                    if (targetHelipad == null || targetHelipad.nodes == null || targetHelipad.nodes.Count <= 0) return;

                    var helipadPaths = targetHelipad.nodes[0].nodeHoverArea;
                    if (helipadPaths == null) return;

                    transform.GetChild(transform.childCount - 1).position = helipadPaths.position;
                    transform.GetChild(transform.childCount - 2).position = helipadPaths.position + (helipadPaths.forward * 100);
                }
            }
        }
    }
}
