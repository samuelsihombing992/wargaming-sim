using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;
using Esri.HPFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugBankingTest : MonoBehaviour
{
    public Transform guidance;
    public bool doLeftStuff = false;
    public bool doRightStuff = false;

    public float angle = 0;
    public float senstivity = 1;

    public float degreesPerSecond = 10f;
    private float totalRotation = 0;

    private ArcGISLocationComponent locationComponent;
    ArcGISPoint olderOne;

    //public Quaternion sss;


    void Start()
    {
        locationComponent = GetComponent<ArcGISLocationComponent>();
    }

    void Update()
    {
        if (locationComponent.Position == olderOne) return;
        if (guidance == null) return;

        var relativePos = guidance.position - transform.position;

        var forward = transform.forward;
        angle = Vector3.Angle(relativePos, forward);
        if (angle == 0)
        {
            doLeftStuff = false;
            doRightStuff = false;
            return;
        }

        if (Vector3.Cross(forward, relativePos).y < 0)
        {
            //Do left stuff
            doLeftStuff = true; 
            doRightStuff = false;
        }
        else
        {
            //Do right stuff
            doLeftStuff = false;
            doRightStuff = true;
        }

        var rot = locationComponent.Rotation;

        if (doLeftStuff && angle < 0 || doRightStuff && angle > 0)
        {
            angle *= -1;
        }

        locationComponent.Rotation = new ArcGISRotation(rot.Heading, rot.Pitch, angle);

        olderOne = locationComponent.Position;

        //var rot = GetComponent<ArcGISLocationComponent>().Rotation;
        //if(rot.Roll != angle)
        //{
        //    GetComponent<ArcGISLocationComponent>().Rotation = new Esri.ArcGISMapsSDK.Utils.GeoCoord.ArcGISRotation(rot.Heading, rot.Pitch, angle);
        //}


        //transform.rotation = Quaternion.Euler(transform.rotation.x, transform.rotation.y, angle);

        //transform.rotation =  Quaternion.Lerp(transform.rotation, Quaternion.Euler(transform.rotation.x, transform.rotation.y, angle), Time.deltaTime * degreesPerSecond);
    }
}
