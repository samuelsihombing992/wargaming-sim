using Esri.ArcGISMapsSDK.Components;
using System;
using System.Collections.Generic;
using UnityEngine;
using Wargaming.Core.GlobalParam.HelperDataPlotting;
using Wargaming.Core.GlobalParam.HelperPlotting;
using Wargaming.Core.Network.Plotting;

public class DebugSaveSatuan : MonoBehaviour
{
    private ObjekSatuan data;
    public string jsonStyle;
    public string jsonInfo;
    public string stringSymbol;

    public string converted;

    private async void Start()
    {
        data = GetComponent<ObjekSatuan>();
        if(data == null)
        {
            Debug.LogError("Failed to Save Satuan to Database. No ObjekSatuan ScriptableObject found!");
            return;
        }

        Debug.Log("[DEBUG] Saving Satuan to Database...");

        jsonStyle = ObjekSatuanStyle.ToString(data.style);
        jsonInfo = ObjekSatuanInfo.ToString(data.info);

        jsonStyle = jsonStyle.Replace("\"", "\\\"");
        jsonStyle = jsonStyle.Replace("\\\\\\\\", "\\\\\\");
        jsonStyle = "\"" + jsonStyle + "\"";

        jsonInfo = jsonInfo.Replace("\"", "\\\"");
        jsonInfo = jsonInfo.Replace("\\\\\\\\", "\\\\\\");
        jsonInfo = "\"" + jsonInfo + "\"";

        //jsonStyle = null;
        //jsonInfo = null;
        stringSymbol = "\"<div style='text-align:center'><span style='font-weight: bolder; margin-top:-15px; width: "+data.symbol.width+"px;color: "+ PlottingHelper.GetColorName(data.symbol.warna) + "; font-size: 21px; font-family: "+data.symbol.fontFamily+"'>"+data.symbol.fontIndex+"</span></div>\"";

        var objLocation = data.gameObject.GetComponent<ArcGISLocationComponent>();

        ObjekSatuanHelper convertedString = new ObjekSatuanHelper();
        convertedString.id_user         = data.userID;
        convertedString.dokumen         = data.documentID;
        convertedString.nama            = "\"" + data.nama + "\"";
        convertedString.lng_x           = objLocation.Position.X;
        convertedString.lat_y           = objLocation.Position.Y;
        convertedString.symbolID        = data.symbolID;
        convertedString.style           = data.style;
        convertedString.symbol          = data.symbol;
        convertedString.info            = data.info;
        convertedString.id_kegiatan     = data.id_kegiatan;
        convertedString.isi_logistik    = data.isi_logistik;

        List<List<object>> objekSatuan = new List<List<object>>();
        objekSatuan.Add(new List<object>());

        objekSatuan[0].Add(data.userID);
        objekSatuan[0].Add(data.symbolID);
        objekSatuan[0].Add(data.documentID);
        objekSatuan[0].Add("\"" + data.nama + "\"");
        objekSatuan[0].Add(objLocation.Position.Y);
        objekSatuan[0].Add(objLocation.Position.X);
        objekSatuan[0].Add(stringSymbol);
        objekSatuan[0].Add(jsonStyle);
        objekSatuan[0].Add(jsonInfo);
        objekSatuan[0].Add((data.id_kegiatan == null || data.id_kegiatan == "") ? "null" : data.id_kegiatan);
        objekSatuan[0].Add((data.isi_logistik == null || data.isi_logistik == "") ? "null" : data.id_kegiatan);

        converted += "[";
        for(int i=0; i < objekSatuan.Count; i++)
        {
            converted += "[";

            for(int j=0; j < objekSatuan[i].Count; j++)
            {
                if((j < objekSatuan[i].Count - 1))
                {
                    if(objekSatuan[i][j] != "" || objekSatuan[i][j] != null)
                    {
                        converted += objekSatuan[i][j];
                        converted += ",";
                    }
                }
                else
                {
                    converted += "]";
                }
            }

            converted += (i < objekSatuan.Count - 1) ? "]," : "]";
        }

        //converted = ObjekSatuanHelper.ToString(convertedString);

        //await WargamingAPI.SavePlotting(converted);
    }
}
