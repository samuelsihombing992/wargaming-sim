using FluffyUnderware.Curvy;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using WGSBundling.Core;
using static UnityEngine.Tilemaps.Tilemap;

public class DebugWeaponControl : MonoBehaviour
{
    [Header("Target")]
    public Transform target;
    private Transform lastTarget;

    [Header("Config")]
    [SerializeField] public Vector2 distanceLimit   = new Vector2(0, 100);
    [SerializeField] public Vector2 baseRotLimit    = new Vector2(-90, 90);
    [SerializeField] private Vector2 turretRotLimit = new Vector2(0, 45);

    [Header("Simulation - Debug")]
    [SerializeField] private float baseRotTarget;
    [SerializeField] private float turretRotTarget;
    [SerializeField] private float distToTarget;
    [SerializeField] private bool outOfRange    = false;
    [SerializeField] private bool readyToFire   = false;
    [SerializeField] private float lastTiming;

    private AnimationCurve curved;

    [Header("Spline")]
    private CurvySpline spline;
    private CurvySpline prefabSpline;
    private CurvySplineSegment prefabSplineSegment;

    [Header("Turret")]
    public Transform turretBase;
    public Transform turretGun;
    public Transform turretNode;
    public Transform vfxActive;

    [Header("Turret Config")]
    public float baseTraverse = 1;
    public float turretTraverse = 1;

    [Header("Firing Config")]
    public int ammo         = 1000;     // Total Ammunition
    public int magCapacity  = 0;        // Set to 0 if not need to reload every X capacity

    public int rateOfFire   = 100;      // Rate of Fire Per Minute
    public int reloadSpeed  = 30;       // Reload Speed After Last ROF

    [Header("VFX Config")]
    public GameObject debugEffect;

    private CurvySplineSegment startSegment;
    private CurvySplineSegment endSegment;

    private CurvySplineSegment startSmoother;
    private CurvySplineSegment endSmoother;

    public DateTime CURRENT_TIME = DateTime.Today;
    public float speedFactor = 1;

    [NonSerialized] public DateTime lastShot;

    private void Start()
    {
        CURRENT_TIME = DateTime.Today;
        GeneratePath();
    }

    private void Update()
    {
        float baseLerp;
        float turretLerp;

        CURRENT_TIME = CURRENT_TIME.AddSeconds(Time.deltaTime * speedFactor);

        UpdateTargetPos();
        UpdateTurretMovement(out baseLerp, out turretLerp);

        if (distToTarget < distanceLimit.x || distToTarget > distanceLimit.y || baseRotTarget > baseRotLimit.y || baseRotTarget < baseRotLimit.x || Mathf.Abs(baseLerp - baseRotTarget) > 1)
        {
            outOfRange = true;
        }
        else
        {
            outOfRange = false;
        }

        UpdateFiring();


        return;
        //if (target == null)
        //{
        //    turretBase.localRotation            = QuaternionExtension.Slerp(Quaternion.Euler(0, 0, turretBase.localRotation.eulerAngles.z), Quaternion.Euler(0, 0, 0), Time.deltaTime * baseRotationSpeed, false);
        //    turretGun.transform.localRotation   = QuaternionExtension.Slerp(turretGun.transform.localRotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * gunRotationSpeed, false);
                
        //    return;
        //}

        //if (target != lastTarget)
        //{
        //    GeneratePath();
        //}

        //target.LookAt(turretBase);
        //currentRotation = (target.eulerAngles.y) - transform.parent.rotation.eulerAngles.y;
        //if(currentRotation > 180) { currentRotation *= -1; }

        //if(currentRotation > dirLimit)
        //{
        //    currentRotation = dirLimit;
        //}

        //if(currentRotation < -dirLimit)
        //{
        //    currentRotation = -dirLimit;
        //}

        //spline.transform.position = transform.position;
        //spline.transform.rotation = Quaternion.Euler(0, currentRotation, 0);

        //turretBase.localRotation = Quaternion.Slerp(Quaternion.Euler(0, 0, turretBase.localRotation.eulerAngles.z), Quaternion.Euler(0, 0, currentRotation),  Time.deltaTime * baseRotationSpeed);

        //currentDistance = Vector3.Distance(startSegment.transform.position, target.position);
        //if (currentDistance > maxDistance)
        //{
        //    currentDistance = maxDistance;
        //}
        //else if (currentDistance < minDistance)
        //{
        //    currentDistance = minDistance;
        //}

        //var altDiff = target.position.y - startSegment.transform.position.y;

        //startSegment.transform.rotation = Quaternion.Euler(0, target.eulerAngles.y - 180, 0);
        //endSegment.transform.rotation   = Quaternion.Euler(0, target.eulerAngles.y, 0);

        //var endSegmentPos = startSegment.transform.position + (startSegment.transform.forward * currentDistance);
        //endSegment.transform.position = new Vector3(endSegmentPos.x, target.position.y, endSegmentPos.z);

        //currentAngle = curved.Evaluate(Vector3.Distance(transform.position, target.position) + altDiff);

        //startSegment.transform.rotation     = Quaternion.Euler(-currentAngle, startSegment.transform.eulerAngles.y, startSegment.transform.eulerAngles.z);
        //endSegment.transform.rotation       = Quaternion.Euler(-currentAngle, endSegment.transform.eulerAngles.y, endSegment.transform.eulerAngles.z);
        
        //startSmoother.transform.rotation    = startSegment.transform.rotation;
        //endSmoother.transform.rotation      = endSegment.transform.rotation;

        //startSmoother.transform.position    = startSegment.transform.position + (startSegment.transform.forward * (currentDistance / 3f)) + (startSegment.transform.up * altDiff);
        //endSmoother.transform.position      = endSegment.transform.position + (endSegment.transform.forward * (currentDistance / 3f));

        ////endSegment.transform.rotation       = Quaternion.Euler(-currentAngle, 180, 0);

        ////startSegment.transform.localPosition    = Vector3.zero;

        ////endSegment.transform.localPosition      = startSegment.transform.forward * upLimit;

        ////currentAngle = curved.Evaluate(Vector3.Distance(transform.position, target.position));

        ////startSegment.transform.localRotation    = Quaternion.Euler(-currentAngle, 0, 0);
        ////endSegment.transform.localRotation      = Quaternion.Euler(-currentAngle, 180, 0);

        ////startSmoother.transform.position = startSegment.transform.forward * (currentDistance / 4);
        ////endSmoother.transform.position = endSegment.transform.position + (endSegment.transform.forward * (currentDistance / 4));

        //turretGun.transform.localRotation = Quaternion.Slerp(turretGun.transform.localRotation, Quaternion.Euler(currentAngle, 0, 0), Time.deltaTime * gunRotationSpeed);

        ////startSmoother.transform.localRotation = Quaternion.Euler(-currentAngle, 0, 0);
        ////endSmoother.transform.localRotation = Quaternion.Euler(-currentAngle, 180, 0);
    }

    private void UpdateTargetPos()
    {
        if (target == null) return;
        if(target != lastTarget)
        {
            GeneratePath();
        }

        //parentRot = transform.parent.rotation.eulerAngles.y;
        var targetAngle = target.eulerAngles.y;

        target.LookAt(turretBase);
        baseRotTarget = (target.eulerAngles.y - 180) - GetComponentInParent<WGSBundleCore>().transform.rotation.eulerAngles.y;
        if(baseRotTarget < -180) { baseRotTarget = 360 + baseRotTarget; }
        if(baseRotTarget > 180) { baseRotTarget = baseRotTarget - 360; };

        //if(currentRotation > 180)
        //{
        //    currentRotation = 180 - currentRotation;
        //}else if(currentRotation < -180)
        //{
        //    currentRotation = currentRotation + 180;
        //}

        //currentRotation = (target.eulerAngles.y) - transform.parent.rotation.eulerAngles.y;
        //if (currentRotation > 180 || currentRotation < -180) { currentRotation *= -1; }

        if (baseRotTarget > baseRotLimit.y)
        {
            baseRotTarget = baseRotLimit.y;
        }
        else if (baseRotTarget < baseRotLimit.x)
        {
            baseRotTarget = baseRotLimit.x;
        }

        //currentRotation     = Mathf.Clamp(currentRotation, -90, 90);

        distToTarget = Vector3.Distance(transform.position, target.position);

        turretRotTarget = curved.Evaluate(distToTarget);
    }
        
    private void UpdateTurretMovement(out float baseLerp, out float turretLerp)
    {
        var baseAngle = turretBase.localRotation.eulerAngles.y;
        if (baseAngle < -180) { baseAngle = 360 + baseAngle; }
        if (baseAngle > 180) { baseAngle = baseAngle - 360; };

        baseLerp    = Mathf.Lerp(baseAngle, baseRotTarget, ((baseTraverse / 3600) * Mathf.Rad2Deg) * (Time.deltaTime * speedFactor));
        turretLerp  = Mathf.Lerp(turretGun.localRotation.eulerAngles.x, turretRotTarget, ((turretTraverse / 3600) * Mathf.Rad2Deg) * (Time.deltaTime * speedFactor));

        if (target == null)
        {
            baseRotTarget = 0;
            turretRotTarget = 0;

            return;
        }

        turretBase.localRotation    = Quaternion.Euler(turretBase.localEulerAngles.x, baseLerp, turretBase.localEulerAngles.z);
        turretGun.localRotation     = Quaternion.Euler(turretLerp, turretGun.localEulerAngles.y, turretGun.localEulerAngles.z);

        return;

        //if(turretBaseAngle >= 0)
        //{
        //    if(currentRotation < 0)
        //    {
        //        turretBase.localRotation = Quaternion.Lerp(Quaternion.Euler(0, 0, turretBaseAngle), Quaternion.Euler(0, 0, 0), Time.deltaTime * baseRotationSpeed);
        //    }
        //    else
        //    {
        //        turretBase.localRotation = Quaternion.Lerp(Quaternion.Euler(0, 0, turretBaseAngle), Quaternion.Euler(0, 0, currentRotation), Time.deltaTime * baseRotationSpeed);
        //    }
        //}
        //else
        //{
        //    if (currentRotation > 0)
        //    {
        //        turretBase.localRotation = Quaternion.Lerp(Quaternion.Euler(0, 0, turretBaseAngle), Quaternion.Euler(0, 0, 0), Time.deltaTime * baseRotationSpeed);
        //    }
        //    else
        //    {
        //        turretBase.localRotation = Quaternion.Lerp(Quaternion.Euler(0, 0, turretBaseAngle), Quaternion.Euler(0, 0, currentRotation), Time.deltaTime * baseRotationSpeed);
        //    }

        //}


        //if (target != lastTarget)
        //{
        //    //Destroy(spline.gameObject);

        //    //GeneratePath();
        //}
    }

    private void UpdateFiring()
    {
        if (ammo <= 0) return;

        //60 RPM = 60/60 = 1 RPS
        var shotEveryX = rateOfFire / 60;
        var diffInSeconds = (CURRENT_TIME - lastShot).TotalSeconds;

        if (readyToFire && !outOfRange && target != null)
        {
            if(vfxActive != null)
            {
                Destroy(vfxActive.gameObject);
            }

            var vfx = Instantiate(debugEffect, turretNode.transform);
            vfx.transform.localPosition = Vector3.zero;

            vfxActive = vfx.transform;

            ammo --;

            readyToFire = false;
        }

        if(diffInSeconds > shotEveryX)
        {
            lastShot = CURRENT_TIME;
            readyToFire = true;
        }
    }

    private void GeneratePath()
    {
        lastTarget = target;

        curved = new AnimationCurve(
            new Keyframe(distanceLimit.x, turretRotLimit.x),
            new Keyframe(distanceLimit.y, turretRotLimit.y)
        );

        //spline = Instantiate(prefabSpline);
        //spline.transform.position = turretBase.position;

        //startSegment = Instantiate(prefabSplineSegment, spline.transform);
        //startSmoother = Instantiate(prefabSplineSegment, spline.transform);
        //endSmoother = Instantiate(prefabSplineSegment, spline.transform);
        //endSegment = Instantiate(prefabSplineSegment, spline.transform);

        //startSegment.transform.localPosition = Vector3.zero;
    }
}