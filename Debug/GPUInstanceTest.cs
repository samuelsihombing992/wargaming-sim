using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using WGSBundling.Core;
using GPUInstancer;

public class GPUInstanceTest : MonoBehaviour
{
    public Vector2 spawnSize;
    public Transform objParent;
    public Transform objPrefab;
    public List<GPUInstancerPrefab> objInstances = new List<GPUInstancerPrefab>();

    [Header("Prefab Manager")]
    public GPUInstancerPrefabManager prefabManager;

    private void Start()
    {
        objInstances.Clear();

        for(int i=0; i < spawnSize.x; i++)
        {
            for(int j=0; j < spawnSize.y; j++)
            {
                var newObj = Instantiate(objPrefab, objParent);
                newObj.localPosition = new Vector3(j * 30, 2000, i * 60);

                objInstances.Add(newObj.GetComponent<GPUInstancerPrefab>());
            }
        }

        RegisterToGPUInstancer();
    }

    public void RegisterToGPUInstancer()
    {
        if (objInstances == null || objInstances.Count <= 0 || prefabManager == null) return;

        // TEST TO REGISTER TO GPU INSTANCER
        GPUInstancerAPI.RegisterPrefabInstanceList(prefabManager, objInstances);
        GPUInstancerAPI.InitializeGPUInstancer(prefabManager);
    }
}
