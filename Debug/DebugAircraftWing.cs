using Crest.Spline;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine;
using Wargaming.Components.Playback;
using WGSBundling.Bundles.Aircraft;

public class DebugAircraftWing : MonoBehaviour
{
    public Transform follower;

    [Header("Settings")]
    public float sensitivity = 1;

    public NodeAircraft node;

    private void Update()
    {
        if (follower == null || node == null) return;

        var rootWalker = GetComponent<RootWalker>();
        if (rootWalker.activeMission == null) return;

        var activeMission = rootWalker.activeMission;
        if (activeMission == null) return;

        var spline = GetComponent<SplineController>();

        if (spline.AbsolutePosition > 0 && spline.AbsolutePosition < rootWalker.altitudeProfile.keys[2].time)
        {
            SetFlaps();
        }

        SetFlyMovement();
    }

    private void SetFlaps()
    {
        var forward = transform.position - follower.forward;
        if (forward == Vector3.zero) return;

        var lookRot = Quaternion.LookRotation(forward);

        var currentRot = transform.localRotation.eulerAngles.y;
        var guidanceRot = follower.transform.localRotation.eulerAngles.y;

        var rollChange = ((currentRot - guidanceRot) * -1) * sensitivity;

        for (int i = 0; i < node.flaps.Count; i++)
        {
            var flap = node.flaps[i];

            Quaternion rotationChange = Quaternion.identity;
            rotationChange = Quaternion.Euler(lookRot.eulerAngles.x, 0, 0);

            flap.localRotation = Quaternion.Lerp(flap.localRotation, rotationChange, Time.deltaTime * (PlaybackController.instance.speedFactor));
        }
    }

    private void SetFlyMovement()
    {
        var forward = transform.position - follower.forward;
        if (forward == Vector3.zero) return;

        var lookRot = Quaternion.LookRotation(forward);

        var currentRot = transform.localRotation.eulerAngles.y;
        var guidanceRot = follower.transform.localRotation.eulerAngles.y;

        var rollChange = ((currentRot - guidanceRot) * -1) * sensitivity;

        var hChange = rollChange;
        var vChange = lookRot.eulerAngles.x;

        for(int i=0; i < node.elevators.Count; i++)
        {
            var elevator = node.elevators[i];

            Quaternion rotationChange = Quaternion.identity;
            if (i % 2 == 0)
            {
                rotationChange = Quaternion.Euler((vChange * sensitivity) + (hChange * sensitivity), 0, 0);
            }
            else
            {
                rotationChange = Quaternion.Euler((vChange * sensitivity) - (hChange * sensitivity), 0, 0);
            }
            
            elevator.localRotation = Quaternion.Lerp(elevator.localRotation, rotationChange, Time.deltaTime * (PlaybackController.instance.speedFactor));
        }

        for (int i = 0; i < node.ailerons.Count; i++)
        {
            var aileron = node.ailerons[i];

            Quaternion rotationChange = Quaternion.identity;
            if (i % 2 == 0)
            {
                rotationChange = Quaternion.Euler((vChange * sensitivity) + (hChange * sensitivity), 0, 0);
            }
            else
            {
                rotationChange = Quaternion.Euler((vChange * sensitivity) - (hChange * sensitivity), 0, 0);
            }

            aileron.localRotation = Quaternion.Lerp(aileron.localRotation, rotationChange, Time.deltaTime * (PlaybackController.instance.speedFactor));
        }
    }
}
