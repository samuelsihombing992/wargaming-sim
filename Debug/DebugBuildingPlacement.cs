using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugBuildingPlacement : MonoBehaviour
{
    [Header("Map")]
    public Transform parent;

    public List<BuildingPlacementData> DATA = new List<BuildingPlacementData>();

    // Start is called before the first frame update
    void Start()
    {
        SpawnDatapack();
    }

    public void SpawnDatapack()
    {
        for(int i=0; i < DATA.Count; i++)
        {
            for(int j=0; j < DATA[i].DATA.Count; j++)
            {
                var activeData = DATA[i].DATA[j];

                ArcGISPoint pos     = new ArcGISPoint(activeData.location.x, activeData.location.y, activeData.location.z, ArcGISSpatialReference.WGS84());
                ArcGISRotation rot  = new ArcGISRotation(activeData.rotation.x, activeData.rotation.y, activeData.rotation.z);

                var objPrefab   = Instantiate(activeData.obj, parent);
                objPrefab.name  = activeData.obj.name;

                var LocationComponent       = objPrefab.AddComponent<ArcGISLocationComponent>();
                LocationComponent.enabled   = true;
                LocationComponent.Position  = pos;
                LocationComponent.Rotation  = rot;
            }

        }
    }
}
