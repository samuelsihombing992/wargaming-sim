using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

using Wargaming.Components.DebugComponent;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;
using static LoadingControllerV2;

public class DebugEntityControl : MonoBehaviour
{
    [Header("UI Group")]
    public GameObject modalMode;

    public GameObject activeLoading;

    #region INSTANCE
    public static DebugEntityControl instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private async void Start()
    {
        if (DebugPlayGame.instance != null)
        {
            activeLoading = ShowOverlayLoading(LoadingOverlay.OVERLAY, modalMode.transform.parent, "Preparing Debug Gameplay...");

            var state = await DebugPlayGame.instance.Init();
            if (!state)
            {
                Debug.LogError("FAILED TO PLAY ON DEBUG MODE!");
                return;
            }

            StartGameplay(DebugPlayGame.instance.GetRoomActive());
        }
    }

    public async void StartGameplay(string room)
    {
        HideLoading(activeLoading);

        EnvironmentsController.SetSeaLevel(StartupConfig.settings.DEFAULT_OCEAN_HEIGHT.GetValueOrDefault(2) - 1);
        EnvironmentsController.SetCloudsAltitude(StartupConfig.settings.DEFAULT_CLOUDS_HEIGHT.GetValueOrDefault(7000));

        modalMode.PlayAnimation("FadeOut");

        Debug.Log("Preparing Gameplay...");
        activeLoading = ShowSideScreenLoading(LoadingScreenSide.SCREEN_SIDE_BOTTOM_RIGHT, "Preparing Gameplay...");
        await Task.Delay(1000);

        StartSingleplayer();
    }

    private async void StartSingleplayer()
    {
        Debug.Log("Starting Singleplayer...");

        activeLoading = ShowSideScreenLoading(LoadingScreenSide.SCREEN_SIDE_BOTTOM_RIGHT, "Starting Singleplayer...");
        await Task.Delay(1000);

        if (SessionUser.id != 1)
        {
            activeLoading = ShowSideScreenLoading(LoadingScreenSide.SCREEN_SIDE_BOTTOM_RIGHT, "Load CB...");

            PlayerData.PLAYERS = new List<PData>();

            // Add User Into List
            PlayerData.AddPlayer(new PData
            {
                id = SessionUser.id,
                name = SessionUser.name,
                nama_asisten = SessionUser.nama_asisten,
                id_bagian = SessionUser.id_bagian,
                jenis_user = SessionUser.jenis_user
            });

            await EntityControlLoader.instance.loadEntityFromCB(
                SessionUser.id,
                SessionUser.id_kogas,
                SkenarioAktif.ID_SKENARIO,
                DataCB.GetYourCB().nama_document
            );
        }

        HideLoading(activeLoading);
    }
}
