using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using Wargaming.Core.GlobalParam;
using Wargaming.Core.GlobalParam.HelperSessionUser;
using Wargaming.Core.Network;
using Wargaming.Core.TFG;

namespace Wargaming.Components.DebugComponent
{
    public class DebugPlayGame : MonoBehaviour
    {
        public enum GameMode { OFFLINE = 0, TFG = 1, TFG_SINGLE = 2, WGS = 3 }

        [Header("Mode")]
        public GameMode mode;

        [Header("User Profile")]
        public string username;
        public string password;

        [Header("Connection")]
        public string loginURL      = "http://localhost/eppkm/public";
        public string cbURL         = "http://localhost/eppkm_simulasi";
        public string colyseusURL   = "http://localhost:2567";

        [Header("Configuration")]
        public bool overrideConfig = false;
        public bool debugVersion = true;
        public bool loadAssetPackage = true;
        public bool loadDatapack = true;
        public int timeDiff = 0;

        public int oceanLevel = 2;
        public int showFoamMaxAlt = 100;
        public int showOceanMaxAlt = 150000;

        public int cloudsAlt = 7000;
        public int showCloudsMaxAlt = 35000;
        public int cloudsQuality = 2;
        public int cloudsRepQuality = 4;

        public int rebaseMaxPos = 100000;
        public int rebaseMinAlt = 15000;

        public int aircraftScale = 1;
        public int maxAircraftAlt = 10000;
        public bool aircraftTakeoffLand;
        public bool heliTakeoffLand;

        public int shipScale = 1;
        public int vehicleScale = 1;

        public string tfgServer = "tfg";
        public string tfgSingleServer = "tfg-single";
        public string wgsServer = "wgs";

        //[Header("Login Data")]
        //public bool useDebugUser = false;
        //public int userActive;
        //public List<string> userJSON;

        //[Header("Service Data")]
        //public bool useDebugService = false;

        //public string[] roomName;

        //public string serviceLogin = "http://localhost/eppkm/public";
        //public string serviceCB = "http://localhost/eppkm_simulasi/public";
        //public string serverColyseus = "http://localhost:2567";

        //[Header("Config Data")]
        //[Tooltip("Altitude = NUMBER / FOLLOW_TERRAIN")]
        //public bool useDebugConfig = false;

        //public string oceanLevel = "3";

        //public string aircraftAltitude = "1000";
        //public string aircraftScale = "1";

        //public string shipScale = "1";

        //public string vehicleScale = "1";

        //public string infantryAlt = "FOLLOW_TERRAIN";
        //public string infantryScale = "1";

        //public string timeDiff = "0";

        #region INSTANCE
        public static DebugPlayGame instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        public async Task<bool> Init()
        {
            try
            {
                GameController.activeLoading = LoadingControllerV2.ShowOverlayLoading(LoadingControllerV2.LoadingOverlay.OVERLAY, GameController.instance.modalUI, "Preparing Debug Gameplay...");
            }catch(Exception e) { }

            await Task.Delay(1000);

            if(username.Equals("") || password.Equals(""))
            {
                Debug.LogError("Error When Init Playing State. Please Provide Username & Password!");
                return false;
            }

            if (loginURL.Equals("") || cbURL.Equals("") || colyseusURL.Equals(""))
            {
                Debug.LogError("Service data are not fullfilled!");
                return false;
            }

            ServerConfig.SERVICE_LOGIN      = loginURL;
            ServerConfig.SERVICE_CB         = cbURL;
            ServerConfig.SERVER_COLYSEUS    = colyseusURL;

            var settings = StartupConfig.settings;

            settings.DEBUG_VERSION                  = debugVersion;
            settings.TIME_DIFF                      = timeDiff;
            settings.DEFAULT_OCEAN_HEIGHT           = oceanLevel;
            settings.FOAM_MAX_ALTITUDE              = showFoamMaxAlt;
            settings.OCEAN_MAX_ALTITUDE             = showOceanMaxAlt;
            settings.DEFAULT_CLOUDS_HEIGHT          = cloudsAlt;
            settings.CLOUDS_MAX_ALTITUDE            = showCloudsMaxAlt;
            settings.CLOUDS_QUALITY                 = cloudsQuality;
            settings.CLOUDS_REPROJECTION_QUALITY    = cloudsRepQuality;
            settings.REBASE_MAX_POSITION            = rebaseMaxPos;
            settings.REBASE_MIN_ALTITUDE            = rebaseMinAlt;
            settings.DEFAULT_AIRCRAFT_SCALE         = aircraftScale;
            settings.MAX_AIRCRAFT_ALTITUDE          = maxAircraftAlt;
            settings.AIRCRAFT_DEF_TAKEOFF_LANDING   = aircraftTakeoffLand;
            settings.HELI_DEF_TAKEOFF_LANDING       = heliTakeoffLand;
            settings.DEFAULT_SHIP_SCALE             = shipScale;
            settings.DEFAULT_VEHICLE_SCALE          = vehicleScale;
            settings.ENABLE_ASSET_PACKAGE           = loadAssetPackage;
            settings.ENABLE_DATAPACK                = loadDatapack;
            settings.SERVER_TFG                     = tfgServer;
            settings.SERVER_TFG_SINGLE              = tfgSingleServer;
            settings.SERVER_WGS                     = wgsServer;

            if(settings.ENABLE_ASSET_PACKAGE) await AssetPackageController.Instance.loadPackageAlutsista();

            if (settings.ENABLE_DATAPACK)
            {
                await DatapackPackageController.Instance.loadDatapack();
                await DatapackPackageController.Instance.loadDatapackTemplate();
                DatapackPackageController.Instance.SpawnDatapackOnTemplate();
            }

            return await DebugLogin();
        }

        private async Task<bool> DebugLogin()
        {
            WWWForm form = new WWWForm();
            form.AddField("username", username);
            form.AddField("password", password);

            var result = await WargamingAPI.RequestLogin(form);

            if (result == "done")
            {
                await WargamingAPI.GetSkenarioAktif();
                var getCBTerbaik = await WargamingAPI.GetCBTerbaik(SessionUser.id_bagian.ToString());
                if (getCBTerbaik.result == "cb_terbaik_belum_dipilih" && SessionUser.id != 1)
                {
                    await WargamingAPI.RequestLogout();
                    Debug.LogError("Panglima belum memilih metode analisa!");

                    return false;
                }

                await WargamingAPI.GetCBAll(SessionUser.id_bagian.ToString());
            }
            else if (result == "retry")
            {
                await WargamingAPI.RequestLogout();
                await Task.Delay(500);

                await DebugLogin();
            }
            else
            {
                if (result == "user_failed")
                {
                    Debug.Log("Username / Password Salah!");
                }else if(result == "connection_failed")
                {
                    Debug.LogError("Koneksi Gagal!");
                }

                return false;
            }

            try
            {
                LoadingControllerV2.HideLoading(GameController.activeLoading);
            }catch(Exception e) {}
            return true;
        }

        public string GetRoomActive()
        {
            switch (mode)
            {
                case GameMode.OFFLINE:
                    return "";
                case GameMode.TFG:
                    return "sync";
                case GameMode.TFG_SINGLE:
                    return "unsync";
                case GameMode.WGS:
                    return "wgs";
                default: return "";
            }
        }

            //public async Task<string> initGameplay()
            //{
            //    await Task.Delay(1000);

            //    if (useDebugUser)
            //    {
            //        SessionUserHelper[] user = SessionUserHelper.FromJson(userJSON[userActive]);
            //        new SessionUser(user[0]);
            //    }

            //    if (useDebugService)
            //    {
            //        ServerConfig.SERVICE_LOGIN      = serviceLogin;
            //        ServerConfig.SERVICE_CB         = serviceCB;
            //        ServerConfig.SERVER_COLYSEUS    = serverColyseus;
            //    }

            //    if (useDebugConfig)
            //    {
            //        var appSettings = StartupConfig.settings;

            //        appSettings.DEFAULT_AIRCRAFT_ALTITUDE       = GetConfigProperties(aircraftAltitude).GetValueOrDefault(0);
            //        appSettings.DEFAULT_AIRCRAFT_SCALE = GetConfigProperties(aircraftScale).GetValueOrDefault(0);

            //        appSettings.DEFAULT_OCEAN_HEIGHT = GetConfigProperties(oceanLevel).GetValueOrDefault(0);
            //        appSettings.DEFAULT_SHIP_SCALE = GetConfigProperties(shipScale).GetValueOrDefault(0);

            //        appSettings.DEFAULT_VEHICLE_SCALE = GetConfigProperties(vehicleScale).GetValueOrDefault(0);

            //        appSettings.TIME_DIFF = GetConfigProperties(timeDiff).GetValueOrDefault(0);

            //        if(FindObjectOfType<GameplayTFGController>() != null)
            //        {
            //            appSettings.SERVER_TFG = roomName[(int) mode];
            //        }else
            //        {
            //            appSettings.SERVER_WGS = roomName[(int)mode];
            //        }
            //    }

            //    await AssetPackageController.Instance.loadPackageAlutsista();
            //    await DatapackPackageController.Instance.loadDatapack();
            //    await DatapackPackageController.Instance.loadDatapackTemplate();
            //    DatapackPackageController.Instance.SpawnDatapackOnTemplate();

            //    await WargamingAPI.GetSkenarioAktif();
            //    await WargamingAPI.GetCBTerbaik(SessionUser.id_bagian.ToString());

            //    switch (mode)
            //    {
            //        case GameMode.OFFLINE:
            //            return "";
            //        case GameMode.TFG:
            //            return "sync";
            //        case GameMode.TFG_SINGLE:
            //            return "unsync";
            //        case GameMode.WGS:
            //            return "wgs";
            //        default: return "";
            //    }
            //}

            //public static int? GetConfigProperties(string _data)
            //{
            //    if (_data == "FOLLOW_TERRAIN")
            //    {
            //        return null;
            //    }
            //    else
            //    {
            //        int result;
            //        int.TryParse(_data, out result);

            //        return result;
            //    }
            //}
        }
}