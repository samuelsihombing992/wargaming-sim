using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using Wargaming.Core.GlobalParam;
using Wargaming.Core.GlobalParam.HelperSessionUser;
using Wargaming.Core.Network;
using Wargaming.Core.ScenarioEditor;
using Wargaming.Core.TFG;

namespace Wargaming.Components.DebugComponent
{
    public class DebugScenarioEditor : MonoBehaviour
    {
        [Header("User Profile")]
        public string username;
        public string password;

        [Header("Connection")]
        public string loginURL = "http://localhost/eppkm/public";
        public string cbURL = "http://localhost/eppkm_simulasi";
        public string colyseusURL = "http://localhost:2567";

        [Header("Configuration")]
        public bool overrideConfig = false;
        public bool debugVersion = true;
        public bool loadAssetPackage = true;
        public bool loadDatapack = true;
        public int timeDiff = 0;

        public int oceanLevel = 2;
        public int showFoamMaxAlt = 100;
        public int showOceanMaxAlt = 150000;

        public int rebaseMaxPos = 100000;
        public int rebaseMinAlt = 15000;

        #region INSTANCE
        public static DebugScenarioEditor instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        public async Task<bool> Init()
        {
            if (username.Equals("") || password.Equals(""))
            {
                Debug.LogError("Error When Init Playing State. Please Provide Username & Password!");
                return false;
            }

            if (loginURL.Equals("") || cbURL.Equals("") || colyseusURL.Equals(""))
            {
                Debug.LogError("Service data are not fullfilled!");
                return false;
            }

            ServerConfig.SERVICE_LOGIN = loginURL;
            ServerConfig.SERVICE_CB = cbURL;
            ServerConfig.SERVER_COLYSEUS = colyseusURL;

            var settings = StartupConfig.settings;

            settings.DEBUG_VERSION = debugVersion;
            settings.TIME_DIFF = timeDiff;
            settings.DEFAULT_OCEAN_HEIGHT = oceanLevel;
            settings.FOAM_MAX_ALTITUDE = showFoamMaxAlt;
            settings.OCEAN_MAX_ALTITUDE = showOceanMaxAlt;
            settings.REBASE_MAX_POSITION = rebaseMaxPos;
            settings.REBASE_MIN_ALTITUDE = rebaseMinAlt;

            if (loadAssetPackage) await AssetPackageController.Instance.loadPackageAlutsista();

            if (loadDatapack)
            {
                await DatapackPackageController.Instance.loadDatapack();
                await DatapackPackageController.Instance.loadDatapackTemplate();
                DatapackPackageController.Instance.SpawnDatapackOnTemplate();
            }

            return await DebugLogin();
        }

        private async Task<bool> DebugLogin()
        {
            WWWForm form = new WWWForm();
            form.AddField("username", username);
            form.AddField("password", password);

            var result = await WargamingAPI.RequestLogin(form);

            if (result == "done")
            {
                await WargamingAPI.GetSkenarioAktif();
                var getCBTerbaik = await WargamingAPI.GetCBTerbaik(SessionUser.id_bagian.ToString());
                if (getCBTerbaik.result == "cb_terbaik_belum_dipilih" && SessionUser.id != 1)
                {
                    await WargamingAPI.RequestLogout();
                    Debug.LogError("Panglima belum memilih metode analisa!");

                    return false;
                }
            }
            else if (result == "retry")
            {
                await WargamingAPI.RequestLogout();
                await Task.Delay(500);

                await DebugLogin();
            }
            else
            {
                if (result == "user_failed")
                {
                    Debug.Log("Username / Password Salah!");
                }
                else if (result == "connection_failed")
                {
                    Debug.LogError("Koneksi Gagal!");
                }

                return false;
            }

            LoadingControllerV2.HideLoading(GameController.activeLoading);
            return true;
        }
    }
}