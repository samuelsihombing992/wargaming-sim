using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

using TMPro;
using System;
using UnityEngine.UI;
using System.Linq;


using Esri.ArcGISMapsSDK.Components;



public class tampilanPetaController : MonoBehaviour
{

    [Header("SETTINGS")]
    public string serviceFileLocation;

    [Header("EVENTS")]
    public UnityEvent OnFormSubmitSuccess;

    private int? _SELECTED;
    public List<mapView> _Map;
    public GameObject defaultTog;
    public Transform container;
    
    GameObject togglesObject;
    


    public void Start()
    {
        ToggleGroup toggleGrup;
        //get xml element
        loadMapFromFile();
        toggleBuilder();
        toggleGrup = GetComponent<ToggleGroup>();
        var toggle = toggleGrup.GetComponentsInChildren<Toggle>();
        //make toggle active according xml file when first time running
        for (int i = 0; i < _Map.Count; i++)
        {
            if (_Map[i].Active == true)
            {             
                toggle[i+1].isOn = true;
            }
        }

    }

    //for build toggle button
    void toggleBuilder()
    {
        for (int temp = 0; temp < _Map.Count; temp++)
        {
            togglesObject = Instantiate(defaultTog, container);
            togglesObject.transform.GetComponentInChildren<TextMeshProUGUI>().text=_Map[temp].MapName;
            var index = temp;
            togglesObject.GetComponent<Toggle>().onValueChanged.AddListener(delegate {
                SelectService(index);
                var map = FindObjectOfType<ArcGISMapComponent>();
                map.Basemap = _Map[index].linkMap;
                map.BasemapType = BasemapTypes.ImageLayer;
            });

        }
        Destroy(defaultTog);
    }

    // Update is called once per frame
    void loadMapFromFile()
    {

        if (serviceFileLocation == null)
        {
            Debug.LogError("Error, Services Path is not defined. Please provide one!");
            return;
        }

        var services_path = Path.Combine(Directory.GetCurrentDirectory(), serviceFileLocation);
        
        if (File.Exists(services_path))
        {
            var _XMLSerialize = new XmlSerializer(typeof(List<mapView>));
            using (var reader = new StreamReader(services_path))
            {
                _Map = (List<mapView>)_XMLSerialize.Deserialize(reader);
            }
        }
        else
        {
            // Create new Service Document if unavailable     
            AddNewService("default", "DefaultMode", true,"https.www.example.com");
        }


    }

    //add new Service function
    private void AddNewService(string name, string map, bool active,string link)
    {
        try
        {
            _Map.Add(
            new mapView
            {
                MapName = name,
                MapMode = map,
                Active = active,
                linkMap = link
            }
        );
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        SaveChanged();
    }

    //change toggle status in xml file 
    public void SelectService(int index)
    {
        for (int j = 0; j < _Map.Count; j++)
        {
            if (j != index)
            {
                _Map[j].Active = false;
            }
            else
            {
                _Map[j].Active = true;
            }
        }
        SaveChanged();

    }

    //push data to xml file
    private void SaveChanged()
    {
        // Serialize and Write Changed into Service Document
        var _XMLSerialize = new XmlSerializer(typeof(List<mapView>));
        using (var writer = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), serviceFileLocation)))
        {
            _XMLSerialize.Serialize(writer, _Map);
        }
    }

}
[Serializable]
public class mapView
{
    [XmlAttribute("name")]
    public string MapName { get; set; }
    [XmlElement("Map")]
    public string MapMode { get; set; }
    [XmlElement("Active")]
    public bool Active { get; set; }
    [XmlElement("linkMap")]
    public string linkMap { get; set; }
}
