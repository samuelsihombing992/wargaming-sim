using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Windows.WebCam;
using Wargaming.Core;

public class DebugAircraftSound : MonoBehaviour
{
    public Transform cam;
    public float rot;

    public bool start = false;

    public enum SoundPhase { STARTER, TAKEOFF, FLY, LAND }
    public SoundPhase phase;

    [Header("Sound Data")]
    public AudioSource sideSound;
    public AudioSource frontSound;
    public AudioSource backSound;

    public float frontVolume;
    public float backVolume;
    public float sideVolume;

    private void Start()
    {
        cam = WargamingCam.instance.transform;
    }

    private void Update()
    {
        if(WargamingCam.instance.camFollower != null)
        {
            cam = WargamingCam.instance.camFollower.GetChild(0);
        }
        else
        {
            cam = WargamingCam.instance.transform;
        }

        ExecuteSound();
    }

    private void ExecuteSound()
    {
        if (Vector3.Distance(cam.position, transform.position) > 5000) return;

        Vector3 localPoint = transform.InverseTransformPoint(cam.transform.position);

        rot = (Mathf.Atan2(localPoint.x, localPoint.z) * Mathf.Rad2Deg) + 180;

        if(rot >= 0 && rot < 180)
        {
            float fv = (rot / 90) * 100;
            float bv = ((rot - 90) / 90) * 100;

            frontVolume = Mathf.Clamp(1 - (fv / 100), 0, 1);
            backVolume = Mathf.Clamp(bv / 100, 0, 1);
        }
        else
        {
            float fv = ((rot - 270) / 90) * 100;
            float bv = ((rot - 180) / 90) * 100;

            frontVolume = Mathf.Clamp((fv / 100), 0, 1);
            backVolume = Mathf.Clamp(1 - (bv / 100), 0, 1);
            //frontSound.volume = frontVolume;
        }

        if (frontVolume > 0)
        {
            sideVolume = 1 - frontVolume;
        }
        else
        {
            sideVolume = 1 - backVolume;
        }

        frontSound.volume = frontVolume;
        sideSound.volume = sideVolume;
        backSound.volume = backVolume;
    }
}
