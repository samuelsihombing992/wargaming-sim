using UnityEngine;
using FluffyUnderware.Curvy.Controllers;
using System.Collections.Generic;
using Wargaming.Components.Playback;
using Esri.HPFramework;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class DebugTireSync : MonoBehaviour
{
    public bool activateDebug = false;
    public SplineController splineController;
    public List<DebugTireSyncData> tires = new List<DebugTireSyncData>();

    public float distanceDifferent = 0;
    public Vector3 oldDistance;

    private void Start()
    {
        splineController = GetComponent<SplineController>();
    }

    private void Update()
    {
        if (!activateDebug || splineController == null || tires.Count <= 0) return;

        var hptransform = GetComponent<HPTransform>();

        distanceDifferent = Vector3.Distance(oldDistance, new Vector3((float)hptransform.LocalPosition.x, (float)hptransform.LocalPosition.y, (float)hptransform.LocalPosition.z));
        oldDistance = new Vector3((float)hptransform.LocalPosition.x, (float)hptransform.LocalPosition.y, (float)hptransform.LocalPosition.z);

        for(int i=0; i < tires.Count; i++)
        {
            tires[i].objTire.GetComponent<Animator>().speed = (PlaybackController.instance.speedFactor * distanceDifferent) * tires[i].tireRadius;
        }
    }
}

[System.Serializable]
public class DebugTireSyncData
{
    public Transform objTire;
    public float tireRadius;
    public bool steeringWheel;
}


#if UNITY_EDITOR
[CustomEditor(typeof(DebugTireSync))]
public class DebutTireSyncEditor : Editor
{
    #region Variables
    protected DebugTireSync bundleTarget;
    #endregion

    void OnEnable()
    {
        bundleTarget = (DebugTireSync)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        serializedObject.ApplyModifiedProperties();
    }


    protected virtual void OnSceneGUI()
    {
        if(bundleTarget.tires.Count > 0)
        {
            for(int i=0; i < bundleTarget.tires.Count; i++)
            {
                var tire = bundleTarget.tires[i];
                if (tire.objTire == null) continue;

                if (!tire.steeringWheel)
                {
                    Handles.color = Color.green;
                }
                else
                {
                    Handles.color = Color.blue;
                }
                Handles.CircleHandleCap(
                   0,
                   tire.objTire.position,
                   tire.objTire.rotation * Quaternion.LookRotation(Vector3.right),
                   tire.tireRadius,
                   EventType.Repaint
               );
            }
        }

        
    }
}
#endif