using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;
using FIMSpace.GroundFitter;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;
using GPUInstancer;
using Newtonsoft.Json.Linq;
using SickscoreGames.HUDNavigationSystem;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using Wargaming.Components.Walker;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;
using Wargaming.Core.GlobalParam.HelperDataAlutsista;
using Wargaming.Core.GlobalParam.HelperPlotting;
using Wargaming.Core.Network;
using WGSBundling.Core;
using static LoadingControllerV2;
using static Wargaming.Core.GlobalParam.EntityHelper;

public class EntityControlLoader : MonoBehaviour
{
    [Header("GPU Instancer")]
    public GPUInstancerPrefabManager instancerManager;

    [Header("Data Entity")]
    public List<ObjekSatuan> SATUAN = new List<ObjekSatuan>();

    public List<ObjekSatuan> VEHICLE = new List<ObjekSatuan>();
    public List<ObjekSatuan> SHIP = new List<ObjekSatuan>();
    public List<ObjekSatuan> SUBMARINE = new List<ObjekSatuan>();
    public List<ObjekSatuan> AIRCRAFT = new List<ObjekSatuan>();
    public List<ObjekSatuan> HELICOPTER = new List<ObjekSatuan>();
    public List<ObjekSatuan> INFANTRY = new List<ObjekSatuan>();

    #region INSTANCE
    public static EntityControlLoader instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    /// <summary>
    /// Load & Spawn Entity CB Terpilih dari Database
    /// </summary>
    /// 
    public async Task loadEntityFromCB(long? id_user, long? id_kogas, long? id_scenario, string nama_document, bool isScenarioEditor = false)
    {
        // Load Data CB dari Database
        var plotting = await WargamingAPI.loadDataCB(id_user, id_kogas, id_scenario, nama_document);

        // Spawn Entity Satuan
        await loadEntitySatuan(JArrayExtended.setJArrayResult(plotting, 0));
    }

    public async Task<bool> loadEntitySatuan(JArray data)
    {
        DebugEntityControl.instance.activeLoading = ShowSideScreenLoading(LoadingScreenSide.SCREEN_SIDE_TOP_RIGHT, "Loading Satuan...");

        if (!JArrayExtended.checkingJArrayData(data)) return false;

        for (int i = 0; i < data.Count; i++)
        {
            Debug.Log("Spawn Satuan " + i);
            Debug.Log(data[i].ToString());

            var satuan = ObjekSatuanHelper.FromJson(data[i]);
            await spawnEntitySatuan(satuan);
        }

        return true;
    }

    public async Task spawnEntitySatuan(ObjekSatuanHelper satuan)
    {
        if (satuan.style.grup == 10)
        {
            satuan.kategori = KategoriSatuan.INFANTRY;
            if (satuan.tipe_tni == null) await SetDetailSatuan(satuan);
        }
        else
        {
            if (satuan.tipe_tni == null) await SetDetailSatuan(satuan);
        }

        var objPosition = new ArcGISPoint(satuan.lng_x, satuan.lat_y, setSatuanHeight(satuan), ArcGISSpatialReference.WGS84());
        var objRotation = new ArcGISRotation(satuan.info.heading, 90, 0);

        // Create Entity Berdasarkan Object 3D-nya
        var OBJ = CreateEntity(objPosition, objRotation, AssetPackageController.Instance.FindAsset(satuan.path_object_3d, satuan.kategori, satuan.nama).gameObject);
        ChangePasukanSkin(OBJ, satuan);

        OBJ.name = satuan.nama;
        OBJ.tag = "entity";
        OBJ.layer = LayerMask.NameToLayer("Entities");

        GenerateBoundingBox(OBJ);

        PrepareRootWalker(satuan, OBJ, objPosition, objRotation);

        // Get Satuan Author's Data
        var findAuthorData = PlayerData.PLAYERS.FindIndex(x => x.id == satuan.id_user);

        PData authorData = null;
        if (findAuthorData != -1) authorData = PlayerData.PLAYERS[findAuthorData];

        if (OBJ.GetComponent<WGSBundleCore>().submersible) { satuan.kategori = KategoriSatuan.SUBMARINE; }

        var objData = OBJ.AddComponent<ObjekSatuan>();
        objData.userID = satuan.id_user.GetValueOrDefault(-99);
        objData.documentID = satuan.dokumen.GetValueOrDefault(-99);
        objData.symbolID = satuan.symbolID;
        objData.nama = satuan.nama;
        objData.kategori = satuan.kategori;
        objData.style = satuan.style;
        objData.info = satuan.info;
        //objData.weapon            = prepareSatuanWeapon(satuan.info.weapon);
        objData.id_kegiatan = satuan.id_kegiatan;
        objData.isi_logistik = satuan.isi_logistik;

        var objSymbol = new ObjekSymbol();
        objSymbol.width = (int)satuan.info.size;
        objSymbol.warna = satuan.info.warna;
        objSymbol.fontFamily = satuan.style.fontTaktis;
        objSymbol.fontIndex = getFontCharacter(satuan.style.fontIndex);

        objData.symbol = objSymbol;

        objData.hudElement = CreateHUDElementV2(
            OBJ.GetComponent<WGSBundleCore>().parent.transform,
            objData.symbol.warna,
            satuan.info.namaSatuan,
            getFontTaktis(objData.symbol.fontFamily.Replace("_BLUE", "").Replace("_RED", "")),
            getFontTaktis(objData.symbol.fontFamily.Replace("_BLUE", "").Replace("_RED", "") + "_BG"),
            objData.symbol.fontIndex
        );

        SATUAN.Add(objData);
        switch (satuan.kategori)
        {
            case KategoriSatuan.VEHICLE:
                VEHICLE.Add(objData);
                break;
            case KategoriSatuan.SHIP:
                SHIP.Add(objData);
                break;
            case KategoriSatuan.SUBMARINE:
                SUBMARINE.Add(objData);
                break;
            case KategoriSatuan.AIRCRAFT:
                AIRCRAFT.Add(objData);
                break;
            case KategoriSatuan.HELICOPTER:
                HELICOPTER.Add(objData);
                break;
            case KategoriSatuan.INFANTRY:
                INFANTRY.Add(objData);
                break;
        }

        Sprite imageSatuan = OBJ.GetComponent<WGSBundleCore>().icon;

        var splineController = OBJ.AddComponent<SplineController>();

        splineController.OrientationAxis = OrientationAxisEnum.Right;
        splineController.Clamping = CurvyClamping.Clamp;
        splineController.PlayAutomatically = true;
        splineController.UpdateIn = CurvyUpdateMethod.Update;

        if (OBJ.GetComponent<WGSBundleCore>().bundleType == WGSBundleCore.BundleType.LAND)
        {
            splineController.MotionConstraints = MotionConstraints.FreezePositionY | MotionConstraints.FreezeRotationZ | MotionConstraints.FreezeRotationX;
            //var terrainFollower = OBJ.AddComponent<FollowTerrain>();

            //GenerateTerrainFollower(OBJ, FIMSpace.Basics.EFUpdateClock.LateUpdate);
            GenerateTerrainFollower(OBJ, FIMSpace.Basics.EFUpdateClock.Update);
        }
        else if (OBJ.GetComponent<WGSBundleCore>().bundleType == WGSBundleCore.BundleType.AIR)
        {
            splineController.MotionConstraints = MotionConstraints.FreezePositionY | MotionConstraints.FreezeRotationZ | MotionConstraints.FreezeRotationX;
            //var terrainFollower = OBJ.AddComponent<FollowTerrain>();

            //GenerateTerrainFollower(OBJ);
        }
        else if (OBJ.GetComponent<WGSBundleCore>().bundleType == WGSBundleCore.BundleType.SEA)
        {
            splineController.MotionConstraints = MotionConstraints.FreezeRotationZ;
        }

        InitRootWalker(OBJ, satuan);

        // Add to List
        if (ListEntityManager.instance != null)
        {
            ListEntityManager.instance.AddData(objData);
        }
    }

    public GameObject CreateEntity(Vector3 position, Vector3 rotation, GameObject obj, Transform parent = null)
    {
        var newObj = Instantiate(obj, (parent != null) ? parent : EnvironmentsController.GetMapComponent().transform);

        var locationComp = newObj.AddComponent<ArcGISLocationComponent>();
        locationComp.Position = new ArcGISPoint(position.x, position.y, position.z + 3, ArcGISSpatialReference.WGS84());
        locationComp.Rotation = new ArcGISRotation(rotation.x, rotation.y, rotation.z);

        var objCore = newObj.GetComponent<WGSBundleCore>();
        if (objCore != null)
        {
            objCore.parent.localRotation = Quaternion.Euler(objCore.origin);
        }

        return newObj;
    }

    public GameObject CreateEntity(ArcGISPoint position, ArcGISRotation rotation, GameObject obj, Transform parent = null)
    {
        var newObj = Instantiate(obj, (parent != null) ? parent : EnvironmentsController.GetMapComponent().transform);

        var locationComp = newObj.AddComponent<ArcGISLocationComponent>();
        locationComp.Position = position;
        locationComp.Rotation = rotation;

        var objCore = newObj.GetComponent<WGSBundleCore>();
        if (objCore != null)
        {
            objCore.parent.localRotation = Quaternion.Euler(objCore.origin);
        }

        return newObj;
    }

    public GameObject CreateEntity(ArcGISLocationComponent locationComponent, GameObject obj, Transform parent = null)
    {
        var newObj = Instantiate(obj, (parent != null) ? parent : EnvironmentsController.GetMapComponent().transform);

        var locationComp = newObj.AddComponent<ArcGISLocationComponent>();
        locationComp.Position = locationComponent.Position;
        locationComp.Rotation = locationComponent.Rotation;

        var objCore = newObj.GetComponent<WGSBundleCore>();
        if (objCore != null)
        {
            objCore.parent.localRotation = Quaternion.Euler(objCore.origin);
        }

        return newObj;
    }

    public GameObject CreateEntity(ArcGISPoint position, Vector3 rotation, GameObject obj, Transform parent = null)
    {
        var newObj = Instantiate(obj, (parent != null) ? parent : EnvironmentsController.GetMapComponent().transform);

        var locationComp = newObj.AddComponent<ArcGISLocationComponent>();
        locationComp.Position = position;
        locationComp.Rotation = new ArcGISRotation(rotation.x, rotation.y, rotation.z);

        var objCore = newObj.GetComponent<WGSBundleCore>();
        if (objCore != null)
        {
            objCore.parent.localRotation = Quaternion.Euler(objCore.origin);
        }

        return newObj;
    }

    public void ChangePasukanSkin(GameObject obj, ObjekSatuanHelper satuan)
    {
        if (satuan.kategori == KategoriSatuan.INFANTRY)
        {
            if (satuan.path_object_3d == null) return;

            if (obj.GetComponent<PasukanAnim>() != null)
            {
                for (int i = 0; i < obj.GetComponent<PasukanAnim>().ListOrangan.Count; i++)
                {
                    obj.GetComponent<PasukanAnim>().ListOrangan[i].GetComponent<SkinChange>().ChangeSkin(satuan.path_object_3d);
                    //Debug.Log("Change Skin to " + satuan.path_object_3d);
                }
            }
        }
    }

    private float setSatuanHeight(ObjekSatuanHelper satuan)
    {

        switch (satuan.kategori)
        {
            case KategoriSatuan.VEHICLE:
                return 0;
            case KategoriSatuan.SHIP:
                return StartupConfig.settings.DEFAULT_OCEAN_HEIGHT.GetValueOrDefault(2);
            case KategoriSatuan.AIRCRAFT:
                return 0;
            //return StartupConfig.settings.DEFAULT_AIRCRAFT_ALTITUDE.GetValueOrDefault(4000);
            case KategoriSatuan.INFANTRY:
                return 0;
            default:
                return 0;
        }
    }

    private async Task SetDetailSatuan(ObjekSatuanHelper satuan)
    {
        var data = await WargamingAPI.GetDetailSatuan(satuan.symbolID.ToString(), satuan.style.grup.ToString());
        if (data == null) return;

        switch (satuan.style.grup)
        {
            case 1:
                // Jika Jenis Satuan = Angkatan Darat
                var detailDarat = DetailSatuanDarat.FromJson(data);

                satuan.tipe_tni = detailDarat.tipe_tni;
                satuan.kategori = KategoriSatuan.VEHICLE;
                satuan.path_object_3d = detailDarat.OBJ.model3D;
                break;
            case 2:
                // Jika Jenis Satuan = Angkatan Laut
                var detailLaut = DetailSatuanLaut.FromJson(data);

                satuan.tipe_tni = detailLaut.tipe_tni;
                satuan.kategori = KategoriSatuan.SHIP;
                satuan.path_object_3d = detailLaut.OBJ.model3D;
                break;
            case 3:
                // Jika Jenis Satuan = Angkatan Udara
                var detailUdara = DetailSatuanUdara.FromJson(data);

                satuan.tipe_tni = detailUdara.tipe_tni;
                satuan.path_object_3d = detailUdara.OBJ.model3D;

                if (detailUdara.OBJ.type.HasValue)
                {
                    satuan.kategori = (detailUdara.OBJ.type != 6) ? KategoriSatuan.AIRCRAFT : KategoriSatuan.HELICOPTER;
                }
                else
                {
                    satuan.kategori = KategoriSatuan.AIRCRAFT;
                }

                break;
            case 10:
                var detailPasukan = DetailSatuanDarat.FromJson(data);

                satuan.tipe_tni = detailPasukan.tipe_tni;
                satuan.path_object_3d = detailPasukan.OBJ.modelTitan;

                break;
        }
    }

    private static Bounds GetBoundingBox(GameObject obj)
    {
        var renderers = obj.GetComponentsInChildren<Renderer>();
        if (renderers.Length == 0) return new Bounds(obj.transform.position, Vector3.zero);

        var b = renderers[0].bounds;
        foreach (Renderer r in renderers)
        {
            b.Encapsulate(r.bounds);
        }
        return b;
    }

    public static void GenerateBoundingBox(GameObject obj)
    {
        if (obj.GetComponent<BoxCollider>() != null || obj.GetComponent<MeshCollider>() != null) return;

        var bounding = GetBoundingBox(obj);

        var collider = obj.AddComponent<BoxCollider>();
        collider.center = bounding.center - obj.transform.position;
        collider.size = bounding.size;
    }

    public GameObject PrepareRootWalker(ObjekSatuanHelper satuan, GameObject OBJ, ArcGISPoint objPosition, ArcGISRotation objRotation)
    {
        switch (satuan.kategori)
        {
            case KategoriSatuan.VEHICLE:
                OBJ.AddComponent<LRootWalker>();
                break;
            case KategoriSatuan.SHIP:
                OBJ.AddComponent<SRootWalker>();
                break;
            case KategoriSatuan.SUBMARINE:
                OBJ.AddComponent<SRootWalker>();
                break;
            case KategoriSatuan.AIRCRAFT:
                OBJ.AddComponent<ARootWalker>();
                break;
            case KategoriSatuan.HELICOPTER:
                OBJ.AddComponent<ARootWalker>();
                break;
            case KategoriSatuan.INFANTRY:
                OBJ.AddComponent<LRootWalker>();
                break;
            default: OBJ.AddComponent<RootWalker>(); break;
        }

        OBJ.GetComponent<RootWalkerBase>().entity = OBJ;

        return OBJ;
    }

    private void InitRootWalker(GameObject rootWalker, ObjekSatuanHelper objSatuan)
    {
        switch (objSatuan.kategori)
        {
            case KategoriSatuan.VEHICLE:
                rootWalker.GetComponent<LRootWalker>().Init();
                break;
            case KategoriSatuan.SHIP:
                rootWalker.GetComponent<SRootWalker>().Init();
                break;
            case KategoriSatuan.SUBMARINE:
                rootWalker.GetComponent<SRootWalker>().Init();
                break;
            case KategoriSatuan.AIRCRAFT:
                rootWalker.GetComponent<ARootWalker>().Init();
                break;
            case KategoriSatuan.HELICOPTER:
                rootWalker.GetComponent<ARootWalker>().Init();
                break;
            case KategoriSatuan.INFANTRY:
                rootWalker.GetComponent<LRootWalker>().Init();
                break;
            default: rootWalker.GetComponent<RootWalker>().Init(); break;
        }
    }

    private string getFontCharacter(long index)
    {
        return ((char)index).ToString();
    }

    public HUDNavigationElement CreateHUDElementV2(Transform parent, Color color, string label, TMP_FontAsset fontTaktis = null, TMP_FontAsset fontTaktisBG = null, string simbolTaktis = null)
    {
        //var bounding = GetBoundingBox(parent.GetComponent<WGSBundleCore>().parent.gameObject);

        var HUD = Instantiate(AssetPackageController.Instance.prefabHUD, parent).GetComponent<HUDNavigationElement>();
        HUD.name = "HUD_ELEMENT";
        HUD.tag = "simbol-taktis";
        //HUD.transform.localPosition = new Vector3(0, bounding.extents.y, 0);

        HUD.Prefabs.IndicatorPrefab = AssetPackageController.Instance.prefabIndicator;
        HUD.OnElementReady.AddListener(delegate { onHUDReadyV2(HUD, label, fontTaktis, fontTaktisBG, simbolTaktis, color); });
        return HUD;

    }

    public void onHUDReadyV2(HUDNavigationElement HUD, string nama, TMP_FontAsset fontTaktis, TMP_FontAsset fontTaktisBG, string simbolTaktis, Color warna)
    {
        // Resetting data HUDElement setiap kali reload element
        HUD.Indicator.ChangeLabelEntity(nama);
        HUD.Indicator.ToggleCustomTransform("simbol_taktis", true);
        HUD.Indicator.ToggleCustomTransform("simbol_taktis_bg", true);
        HUD.Indicator.ChangeSimbolTaktis(fontTaktis, simbolTaktis, fontTaktisBG);

        if (fontTaktisBG != null)
        {
            HUD.Indicator.ChangeSimbolTaktisBackground(warna);
        }
        else
        {
            HUD.Indicator.ChangeSimbolTaktisColor(warna);
        }
    }

    public static void GenerateTerrainFollower(GameObject obj, FIMSpace.Basics.EFUpdateClock updateMode = FIMSpace.Basics.EFUpdateClock.Update)
    {
        if (obj.GetComponent<FGroundFitter>() != null) return;

        var terrainFollower = obj.AddComponent<FGroundFitter>();
        terrainFollower.FittingSpeed = 5;
        terrainFollower.TotalSmoother = 0;
        terrainFollower.GlueToGround = true;
        terrainFollower.RaycastHeightOffset = 10000;
        terrainFollower.RaycastCheckRange = 20000;
        terrainFollower.UpdateClock = updateMode;
    }
}
