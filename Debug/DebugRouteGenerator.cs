using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

using Esri.ArcGISMapsSDK.Components;
using Esri.GameEngine.Geometry;

using Wargaming.Components.Controller;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;

public class DebugRouteGenerator : MonoBehaviour
{
    [Header("Prefabs")]
    public List<Vector3> waypoints;

    [Header("Settings")]
    public int speed;

    // Start is called before the first frame update
    async void Start()
    {
        await Task.Delay(2000);
        if (waypoints.Count < 2) return;

        // Create Mission Parent (MissionWalker)
        var objMisi = Instantiate(MissionController.instance.prefabRoute, MissionController.instance.missionContainer);
        objMisi.GetComponent<ArcGISLocationComponent>().Position = new ArcGISPoint(waypoints[0].x, waypoints[0].y, waypoints[0].z, ArcGISSpatialReference.WGS84());

        for (int i=0; i < waypoints.Count; i++)
        {
            var objNodeMisi = Instantiate(MissionController.instance.prefabNode, objMisi.transform);
            objNodeMisi.GetComponent<ArcGISLocationComponent>().Position = new ArcGISPoint(waypoints[i].x, waypoints[i].y, waypoints[i].z, ArcGISSpatialReference.WGS84());
        }

        if (GetComponent<SplineController>() == null) gameObject.AddComponent<SplineController>();

        var controller = GetComponent<SplineController>();
        controller.Spline = objMisi.GetComponent<CurvySpline>();
        controller.Speed = speed;
        controller.OrientationAxis = OrientationAxisEnum.Up;
    }
}
