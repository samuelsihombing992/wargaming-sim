using System;
using System.Collections.Generic;

using UnityEngine;

using Esri.GameEngine.Geometry;
using System.Threading;
using Wargaming.Core.Network;
using Wargaming.Core.GlobalParam;
using Wargaming.Core;
using Shapes;
using System.Threading.Tasks;
using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;

public class DrawArrowDebug : MonoBehaviour
{
    private enum ArrowType { STRAIGHT, UP, DOWN }
    [SerializeField] private ArrowType type;

    public List<Vector3> coordinates = new List<Vector3>();
    public float size = 5;
    public float orientation = 0;

    public GameObject selected;

    //public string namaMember;
    //public string password;

    public void TestSpawnArrow()
    {
        if(coordinates.Count <= 1)
        {
            Debug.LogError("Error Drawing Arrow. Must have 2 or more coordinates!");
        }

        if(type == ArrowType.STRAIGHT)
        {
            DrawArrow();
        }
    }

    public async void DrawArrow()
    {
        switch (type)
        {
            case ArrowType.STRAIGHT:    
                await DrawArrowTask();
                break;
        }
    }

    public async Task DrawArrowTask()
    {
        var firstSegment = EntityController.instance.CreateEntity(coordinates[0], new Vector3(0, 90, 0), AssetPackageController.Instance.prefabSegmentSelector, selected.transform);

        await Task.Delay(10);

        var endSegment = EntityController.instance.CreateEntity(coordinates[1], new Vector3(0, 90, 0), AssetPackageController.Instance.prefabSegmentSelector, selected.transform);

        await Task.Delay(10);

        endSegment.transform.LookAt(firstSegment.transform);

        //endSegment.transform.rotation = Quaternion.LookRotation(Vector3.zero, firstSegment.transform.position);
        //firstSegment.transform.rotation = endSegment.transform.rotation;

        orientation = endSegment.transform.eulerAngles.y;

        firstSegment.transform.rotation = Quaternion.Euler(firstSegment.transform.rotation.x, orientation, firstSegment.transform.rotation.z);

        //orientation = Vector3.SignedAngle(endSegment.transform.position, firstSegment.transform.position, Vector3.up);
        //if (orientation < 0) orientation = 360 - orientation * -1;

        //var orientation = Vector3.Angle(firstSegment.transform.position, endSegment.transform.position);
        //firstSegment.gameObject.GetComponent<ArcGISLocationComponent>().Rotation = new ArcGISRotation(orientation, 90, 0);
        //endSegment.gameObject.GetComponent<ArcGISLocationComponent>().Rotation = new ArcGISRotation(orientation, 90, 0);

        //Vector3 orientation = new Vector3(endSegment.transform.rotation.x, endSegment.transform.rotation.y, endSegment.transform.rotation.z);

        var leftSegment     = EntityController.instance.CreateEntity(coordinates[0], new Vector3(orientation, 90, 0), AssetPackageController.Instance.prefabSegmentSelector, selected.transform);

        await Task.Delay(10);

        var rightSegment    = EntityController.instance.CreateEntity(coordinates[0], new Vector3(orientation, 90, 0), AssetPackageController.Instance.prefabSegmentSelector, selected.transform);

        await Task.Delay(10);

        var leftSegmentEnd = EntityController.instance.CreateEntity(coordinates[0], new Vector3(orientation, 90, 0), AssetPackageController.Instance.prefabSegmentSelector, selected.transform);

        await Task.Delay(10);

        var rightSegmentEnd = EntityController.instance.CreateEntity(coordinates[0], new Vector3(orientation, 90, 0), AssetPackageController.Instance.prefabSegmentSelector, selected.transform);

        await Task.Delay(10);

        var endPointLeft = EntityController.instance.CreateEntity(coordinates[1], new Vector3(orientation, 90, 0), AssetPackageController.Instance.prefabSegmentSelector, selected.transform);

        await Task.Delay(10);

        var endPointRight = EntityController.instance.CreateEntity(coordinates[1], new Vector3(orientation, 90, 0), AssetPackageController.Instance.prefabSegmentSelector, selected.transform);

        await Task.Delay(10);

        var polyline = selected.GetComponent<Polyline>();
        //polyline.SetPointPosition(0, firstSegment.transform.position);

        leftSegment.transform.position = leftSegment.transform.position + (-leftSegment.transform.forward * 800) + (-leftSegment.transform.right * 800);
        //polyline.SetPointPosition(1, leftSegment.transform.position);

        rightSegment.transform.position = rightSegment.transform.position + (-rightSegment.transform.forward * 800) + (rightSegment.transform.right * 800);
        //polyline.SetPointPosition(2, rightSegment.transform.position);

        var arrowWidth = Vector3.Distance(leftSegment.transform.position, rightSegment.transform.position);

        leftSegmentEnd.transform.position = leftSegment.transform.position + (leftSegment.transform.right * ((arrowWidth * 30) / 100));
        rightSegmentEnd.transform.position = rightSegment.transform.position + (-rightSegment.transform.right * ((arrowWidth * 30) / 100));

        endPointLeft.transform.position = endPointLeft.transform.position + (-endSegment.transform.right * ((arrowWidth * 20) / 100));
        endPointRight.transform.position = endPointRight.transform.position + (endSegment.transform.right * ((arrowWidth * 20) / 100));


        polyline.SetPointPosition(0, firstSegment.transform.position);
        polyline.AddPoint(leftSegment.transform.position);
        polyline.AddPoint(leftSegmentEnd.transform.position);
        polyline.AddPoint(endPointLeft.transform.position);
        polyline.AddPoint(endSegment.transform.position);
        polyline.AddPoint(endPointRight.transform.position);
        polyline.AddPoint(rightSegmentEnd.transform.position);
        polyline.AddPoint(rightSegment.transform.position);

        //polyline.SetPointPosition(3, leftSegmentEnd.transform.position);
        //polyline.SetPointPosition(4, rightSegmentEnd.transform.position);

        //rightSegment.transform.position = rightSegment.transform.position + (-rightSegment.transform.forward * 500) + (rightSegment.transform.right * 500);
        //polyline.SetPointPosition(2, rightSegment.transform.position);
    }

    public void DrawStraightArrow()
    {
        //var firstPoint = coordinates[coordinates.Count - 2];
        //var lastPoint = coordinates[coordinates.Count - 1];

        //var angle = CalcDegree(firstPoint, lastPoint);
        //List<ArcGISPoint> arrPoint = new List<ArcGISPoint>();

        //var tmpx = (size * Math.Cos((angle + 90) * Math.PI / 180));
        //var tmpy = (size * Math.Sin((angle + 90) * Math.PI / 180));
        //var tmpx1 = ((size * 2) * Math.Cos((angle + 90) * Math.PI / 180));
        //var tmpy1 = ((size * 2) * Math.Sin((angle + 90) * Math.PI / 180));

        //// create first point
        //var _x = firstPoint.X + tmpx;
        //var _y = firstPoint.Y + tmpy;
        //arrPoint.Add(new ArcGISPoint(_x, _y, firstPoint.Z, ArcGISSpatialReference.WGS84()));

        //// create second point
        //_x = firstPoint.X - tmpx;
        //_y = firstPoint.Y - tmpy;
        //arrPoint.Add(new ArcGISPoint(_x, _y, firstPoint.Z, ArcGISSpatialReference.WGS84()));

        //// create 3rd point

    }

    private double CalcDegree(ArcGISPoint firstPoint, ArcGISPoint lastPoint)
    {
        var ax = lastPoint.X - firstPoint.X;
        var ay = lastPoint.Y - firstPoint.Y;

        var at = (Math.Atan2(ay, ax) * 180) / Math.PI;
        return at;
    }




    //private static SemaphoreSlim semaphore;

    //private void Start()
    //{
    //    ServerConfig.SERVICE_LOGIN = "http://localhost/eppkm/public";
    //       semaphore = new SemaphoreSlim(0,3);

    //    cekLogin(namaMember, password, null, false);
    //}

    //public async void cekLogin(string idmember, string idpassword, string roleX, bool rfid)
    //{
    //    string msg = "";
    //    string error = "";

    //    if (string.IsNullOrEmpty(idmember))
    //    {
    //        ShowWarningMessage("username required");
    //        return;
    //    }

    //    if(!rfid && string.IsNullOrEmpty(idpassword))
    //    {
    //        ShowWarningMessage("password required");
    //        return;
    //    }

    //    try
    //    {
    //        ShowWarningMessage("aaaabbbb");
    //        if (rfid == false)
    //        {
    //            ShowWarningMessage("if");
    //            //await semaphore.WaitAsync();

    //            // Requesting Login
    //            WWWForm form = new WWWForm();
    //            form.AddField("username", idmember);
    //            form.AddField("password", idpassword);

    //            var result = await WargamingAPI.RequestLogin(form);

    //            //semaphore.Release();

    //            if (result == "done")
    //            {
    //                Debug.Log("RUN FUNCTION AFTER DONE...");
    //            }
    //            else
    //            {
    //                ShowWarningMessage("Telah terjadi kesalahan");
    //            }
    //        }
    //        else
    //        {
    //            ShowWarningMessage("else");
    //            semaphore.Wait();

    //            // Requesting Login
    //            WWWForm form = new WWWForm();
    //            form.AddField("username", idmember);
    //            form.AddField("password", idpassword);

    //            var result = await WargamingAPI.RequestLogin(form);

    //            semaphore.Release();

    //            if (result == "done")
    //            {
    //                Debug.Log("RUN FUNCTION AFTER DONE...");
    //            }
    //            else
    //            {
    //                ShowWarningMessage("Telah terjadi kesalahan");
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        ShowWarningMessage("Telah Terjadi Kesalahan");
    //    }
    //}

    //private void ShowWarningMessage(string message)
    //{
    //    Debug.Log(message);
    //}


    //public async void cekLogin(string idmember, string idpassword, string roleX, bool rfid)
    //{
    //    try
    //    {

    //        if (rfid == false)
    //        {
                
    //        }
    //        else
    //        {
    //            semaphoreSlim.Wait();
    //            var JSONdatauser = await ApiHelper.LoginRFID(idmember, roleX);
    //            JObject obj = JObject.Parse(JSONdatauser);
    //            semaphoreSlim.Release();
    //            if ((string)obj["error"] == "0000")
    //            {
    //                GlobalVariable.name_member = (string)obj["data"]["member_name"];
    //                GlobalVariable.id_member = (string)obj["data"]["member_id"];
    //                loginForm.Visibility = Visibility.Hidden;
    //                rfidPage.Visibility = Visibility.Hidden;
    //                dashboard.Visibility = Visibility.Visible;
    //                labelNama.Content = GlobalVariable.name_member;
    //                labelIDPengguna.Content = GlobalVariable.id_member;
    //                GetSearchHistoryAPI();
    //            }
    //            else
    //            {
    //                ShowWarningMessage("Telah Terjadi Kesalahan");
    //            }
    //        }
    //        try
    //        {
    //            string[] columns = { "" };

    //            string JSONdatahistory = await ApiHelper.RiwayatPeminjaman(GlobalVariable.id_member, 10000, 1, columns, "");
    //            JObject objh = JObject.Parse(JSONdatahistory);
    //            msg = (string)objh["message"];

    //            if ((string)objh["error"] == "0000")
    //            {
    //                msg = "Error occur while collecting history data";
    //                for (int i = 0; i < (int)objh["data"]["total"]; i++)
    //                {
    //                    /* tableCatatanPeminjaman.Items.Add(new RiwayatPeminjaman
    //                     {
    //                         no = i + 1,
    //                         buku = (string)objh["data"]["filtered"][i]["title"],
    //                         pinjam = (string)objh["data"]["filtered"][i]["loan_date"],
    //                         kembali = (string)objh["data"]["filtered"][i]["returned_date"]
    //                     });*/
    //                }
    //                if ((int)objh["data"]["total"] <= 0)
    //                {
    //                    LogDataa("Tidak Ada Riwayat Peminjaman");
    //                }
    //            }
    //            else
    //            {
    //                messagesBox.txtAlert1.Text = "                  Peringatan !";
    //                messagesBox.txtAlert2.Text = "  Kesalahan Ketika menerima data";
    //                messagesBox.Show();
    //                return;
    //            }
    //        }
    //        catch { }

    //    }
    //    catch (Exception ex)
    //    {
    //        ShowWarningMessage("Telah Terjadi Kesalahan");
    //    }

    //}
}