using System.Collections.Generic;
using UnityEngine;

using Esri.ArcGISMapsSDK.Components;
using Wargaming.Core;
using Shapes;

public class DrawShapeDebug : MonoBehaviour
{
    public GameObject SegmentTransform;

    public enum ShapeType { RECTANGLE, ARROW, ARROW_UP, ARROW_DOWN, MANUVER_UP, MANUVER_DOWN }
    public ShapeType type;

    public enum NodePos { ATAS, BAWAH, KIRI, KANAN }
    public NodePos HPos;
    public NodePos VPos;

    public Transform startPoint;
    public Transform endPoint;
    public double camOrientation = 0;
    public float orientation = 0;

    public string nodePosition = "";

    public List<GameObject> segments = new List<GameObject>();


    private void Start()
    {
        if (startPoint == null || endPoint == null) enabled = false;
        
        switch (type)
        {
            case ShapeType.RECTANGLE:
                var objRectangle = GetComponent<ObjekDrawRectangle>();
                if (objRectangle == null) return;

                objRectangle.outline.SetPointPosition(0,startPoint.position);
                objRectangle.outline.SetPointPosition(1, endPoint.position);
                objRectangle.outline.SetPointPosition(2, endPoint.position);
                objRectangle.outline.SetPointPosition(3, endPoint.position);

                objRectangle.polygon.SetPointPosition(0, new Vector2(startPoint.position.x, startPoint.position.z));
                objRectangle.polygon.SetPointPosition(1, new Vector2(endPoint.position.x, endPoint.position.z));
                objRectangle.polygon.SetPointPosition(2, new Vector2(endPoint.position.x, endPoint.position.z));
                objRectangle.polygon.SetPointPosition(3, new Vector2(endPoint.position.x, endPoint.position.z));

                //var segment1 = Instantiate(SegmentTransform, objRectangle.segments);
                //segments.Add(segment1);

                //var segment2 = Instantiate(SegmentTransform, objRectangle.segments);
                //segments.Add(segment2);
                break;
            default: break;
        }
    }

    private void Update()
    {
        if (startPoint == null || endPoint == null) return;
        if (segments.Count <= 0) return;

        camOrientation = WargamingCam.instance.GetComponent<ArcGISLocationComponent>().Rotation.Heading;
        nodePosition = "";

        switch (type)
        {
            case ShapeType.RECTANGLE:
                startPoint.rotation = Quaternion.Euler(new Vector3(startPoint.rotation.x, orientation, startPoint.rotation.z));
                endPoint.rotation = Quaternion.Euler(new Vector3(endPoint.rotation.x, orientation, endPoint.rotation.z));

                // Z = ATAS/BAWAH
                // X = KIRI/KANAN
                HPos = (endPoint.localPosition.x >= startPoint.localPosition.x) ? NodePos.KANAN : NodePos.KIRI;
                VPos = (endPoint.localPosition.z >= startPoint.localPosition.z) ? NodePos.ATAS : NodePos.BAWAH;

                //Vector3 segment1Dir = (HPos == NodePos.KANAN) ? segments[0].transform.right : -segments[0].transform.right;
                //Vector3 segment2Dir = (HPos == NodePos.ATAS) ? segments[1].transform.forward : -segments[1].transform.forward;

                if(VPos == NodePos.ATAS)
                {
                    Vector3 segment1Dir = startPoint.forward * (endPoint.localPosition.z - startPoint.localPosition.z);
                    segments[1].transform.position = startPoint.position + segment1Dir;
                }
                else if (VPos == NodePos.BAWAH)
                {
                    Vector3 segment1Dir = -startPoint.forward * (startPoint.localPosition.z - endPoint.localPosition.z);
                    segments[1].transform.position = startPoint.position + segment1Dir;
                }

                if(HPos == NodePos.KANAN)
                {
                    Vector3 segment2Dir = startPoint.right * (endPoint.localPosition.x - startPoint.localPosition.x);
                    segments[3].transform.position = startPoint.position + segment2Dir;
                }else if(HPos == NodePos.KIRI)
                {
                    Vector3 segment2Dir = -startPoint.right * (startPoint.localPosition.x - endPoint.localPosition.x);
                    segments[3].transform.position = startPoint.position + segment2Dir;
                }

                break;
            default: break;
        }
        //Vector3 lookDir = bundleTarget.firingNodes[i].up;
        //Handles.color = Color.green;

       // Handles.DrawLine(bundleTarget.firingNodes[i].position, bundleTarget.firingNodes[i].position + lookDir);
    }

    //private GameObject CreateNewSegment(Transform _parent)
    //{
    //    // Create New Segment Object


    //    var segment =
    //        EntityController.instance.CreateEntity(
    //            _location, new Vector3(0, 90, 0), AssetPackageController.Instance.prefabSegmentSelector.gameObject, _parent
    //        );

    //    if (_parent.GetComponent<ObjekDrawing>() == null)
    //    {
    //        _parent.GetComponentInParent<ObjekDrawing>().geometry.Add(segment.gameObject);
    //    }
    //    else
    //    {
    //        _parent.GetComponent<ObjekDrawing>().geometry.Add(segment.gameObject);
    //    }
    //    return segment;
    //}
}
