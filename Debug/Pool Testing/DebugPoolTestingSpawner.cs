using Esri.ArcGISMapsSDK.Components;
//using GPUInstancer;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class DebugPoolTestingSpawner : MonoBehaviour
{
    [Header("Properties")]
    public Transform spawnPoint;
    public GameObject prefab;
    public bool useInstancer;

    //public GPUInstancerPrefabManager prefabManager;
    //public List<GPUInstancerPrefab> instancerPrefab = new List<GPUInstancerPrefab>();

    [Header("Sim Properties")]
    public int timeLimit;
    public float objMass;

    [Header("Data Counts")]
    public int objectLimit = 0;
    public int activeCount = 0;
    private int _total = 0;

    ObjectPool<DebugPoolObject> _pool;
    bool already = false;

    private void OnEnable()
    {
        Debug.Log("Resetting Instancer");
        //GPUInstancerAPI.ClearRegisteredPrefabInstances(prefabManager);
        //instancerPrefab.Clear();

        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        _total = 0;
        already = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(_total >= objectLimit)
        {
            if (already) return;
            if (useInstancer)
            {
                //GPUInstancerAPI.RegisterPrefabInstanceList(prefabManager, instancerPrefab);
                //GPUInstancerAPI.InitializeGPUInstancer(prefabManager);

            }

            already = true;
            return;
        }

        if (spawnPoint == null) return;
        if (prefab == null) return;


        var obj = Instantiate(prefab, spawnPoint);
        activeCount += 1;
        _total += 1;

        if (useInstancer)
        {
            //obj.AddComponent<ArcGISLocationComponent>();

            //var objRigidbody = obj.AddComponent<Rigidbody>();
            //objRigidbody.mass = objMass;

            ///var objProperties = obj.AddComponent<DebugPoolObject>();
            ///objProperties.timeLimit = timeLimit;
            ///objProperties.spawner = this;


            //var instancerData = obj.GetComponent<GPUInstancerPrefab>();
            //instancerData.prefabPrototype.startWithRigidBody = true;
            //instancerData.prefabPrototype.hasRigidBody = true;
            //instancerData.prefabPrototype.rigidbodyData.useGravity = true;
            //instancerData.prefabPrototype.rigidbodyData.angularDrag = objRigidbody.angularDrag;
            //instancerData.prefabPrototype.rigidbodyData.mass = objRigidbody.mass;
            //instancerData.prefabPrototype.rigidbodyData.constraints = objRigidbody.constraints;
            //instancerData.prefabPrototype.rigidbodyData.drag = objRigidbody.drag;
            //instancerData.prefabPrototype.rigidbodyData.isKinematic = objRigidbody.isKinematic;
            //instancerData.prefabPrototype.rigidbodyData.interpolation = objRigidbody.interpolation;



            //instancerPrefab.Add(instancerData);
        }
        else
        {


            //var obj = _pool.Get();
        }
    }

    private void Awake()
    {
        _pool = new ObjectPool<DebugPoolObject>(CreateObject, OnTakeObjectFromPool, OnReturnObjectToPool);
    }

    DebugPoolObject CreateObject()
    {
        var obj = Instantiate(prefab, spawnPoint);

        var objRigidbody = obj.AddComponent<Rigidbody>();
        objRigidbody.mass = objMass;

        var objProperties = obj.AddComponent<DebugPoolObject>();
        objProperties.timeLimit = timeLimit;
        objProperties.spawner = this;

        objProperties.SetPool(_pool);
        return objProperties;
    }

    void OnTakeObjectFromPool(DebugPoolObject obj)
    {
        obj.gameObject.SetActive(true);
        obj._timeCount = 0;
    }

    void OnReturnObjectToPool(DebugPoolObject obj)
    {
        obj.gameObject.SetActive(false);
    }
}
