using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class DebugPoolObject : MonoBehaviour
{
    public float timeLimit = 1;
    public float _timeCount = 0;

    public DebugPoolTestingSpawner spawner;
    Transform _transform;

    IObjectPool<DebugPoolObject> _pool;

    private void Update()
    {
        _timeCount += Time.deltaTime;
        if(_timeCount >= timeLimit)
        {
            if(spawner != null)
            {
                spawner.activeCount -= 1;
            }
            
            if(_pool != null)
            {
                _pool.Release(this);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    private void Awake()
    {
        _transform = transform;
    }

    public void SetPool(IObjectPool<DebugPoolObject> pool) => _pool = pool;
}
