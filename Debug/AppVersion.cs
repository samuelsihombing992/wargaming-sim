using UnityEngine;

using TMPro;

public class AppVersion : MonoBehaviour
{
    [Header("App Name")]
    public string appName;
    public string appAuthor;

    public static string _appName;
    public static string _appAuth;

    [Header("Versioning")]
    public string buildVersion;
    public string matraVersion;
    public string copyrightVersion;
    public string buildDate;

    private static string _buildVer;
    private static string _matraVer;
    private static string _copyVersion;
    private static string _buildDate;

    private void Awake()
    {
        _appName        = appName;
        _appAuth        = appAuthor;

        _buildVer       = buildVersion;
        _buildDate      = buildDate;
        _matraVer       = matraVersion;
        _copyVersion    = copyrightVersion;
    }

    private void Start()
    {
        GetBuildData();
    }

    public static void GetBuildData()
    {
        bool disableLabel = false;
        if (StartupConfig.settings == null || !StartupConfig.settings.DEBUG_VERSION.GetValueOrDefault(false))
        {
            disableLabel = true;
        }

        foreach (GameObject label in GameObject.FindGameObjectsWithTag("app-name"))
        {
            var labelText = label.GetComponent<TextMeshProUGUI>();
            if (labelText == null) continue;

            labelText.text = _appName;
        }

        foreach (GameObject label in GameObject.FindGameObjectsWithTag("app-author"))
        {
            var labelText = label.GetComponent<TextMeshProUGUI>();
            if (labelText == null) continue;

            labelText.text = _appAuth;
        }

        foreach (GameObject label in GameObject.FindGameObjectsWithTag("app-version-debug"))
        {
            if (!disableLabel)
            {
                var labelText = label.GetComponent<TextMeshProUGUI>();
                if (labelText == null) continue;

                labelText.text = "Version: " + _buildVer + ". Build: " + _buildDate + "\n" + SystemInfo.operatingSystem;
            }
            else
            {
                label.SetActive(false);
            }
        }

        foreach (GameObject label in GameObject.FindGameObjectsWithTag("app-version"))
        {
            var labelText = label.GetComponent<TextMeshProUGUI>();
            if (labelText == null) continue;

            labelText.text = "Ver. " + _buildVer + ". Build: " + _buildDate;
        }

        foreach(GameObject label in GameObject.FindGameObjectsWithTag("copyright-version"))
        {
            var labelText = label.GetComponent<TextMeshProUGUI>();
            if (labelText == null) continue;

            labelText.text = "Copyright � " + _copyVersion + " " + _matraVer + ". All Rights Reserved";
        }
    }
}
