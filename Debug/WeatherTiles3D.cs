using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.VFX;

public class WeatherTiles3D : MonoBehaviour
{
    [SerializeField] private string service = "https://tile.openweathermap.org/map/clouds_new/{z}/{x}/{y}.png?appid={ApiKey}";
    [SerializeField] private string ApiKey;
    [SerializeField] private Vector3 location;
    [SerializeField] private VisualEffect _visualEffect;
    [SerializeField] private Texture2D _texture;

    private void Start()
    {
        Debug.Log("test");
        StartCoroutine(DownloadImage((int) location.z, (int) location.x, (int) location.y));
    }

    private IEnumerator DownloadImage(int z, int x, int y)
    {
        var fullUrl = service.Replace("{z}", z.ToString());
        fullUrl = fullUrl.Replace("{x}", x.ToString());
        fullUrl = fullUrl.Replace("{y}", x.ToString());
        fullUrl = fullUrl.Replace("{ApiKey}", ApiKey);

        Debug.Log(fullUrl);

        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(fullUrl))
        {
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }
            else
            {
                _texture = DownloadHandlerTexture.GetContent(uwr);
                _visualEffect.SetTexture("CloudTexture", _texture);
            }
        }
    }
}
