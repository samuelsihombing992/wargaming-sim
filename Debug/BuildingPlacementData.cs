using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Placement Object", menuName = "WGS/Debug/Building Placement Data")]
public class BuildingPlacementData : ScriptableObject
{
    public string id;
    public List<PlacementObject> DATA = new List<PlacementObject>();
}

[System.Serializable]
public class PlacementObject
{
    public GameObject obj;

    public Vector3 location;
    public Vector3 rotation;
}
