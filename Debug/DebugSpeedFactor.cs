using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugSpeedFactor : MonoBehaviour
{
    public float speedFactor = 1;

    private void Update()
    {
        Time.timeScale = speedFactor; //* Time.deltaTime;
        //Time.fixedDeltaTime = Time.timeScale;
    }
}
