using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;


public class DebugRadarWatcher : MonoBehaviour
{
    public float viewRadius;
    public float viewAngle;
    public LayerMask entityMask;

    public List<Transform> targetOnRadar = new List<Transform>();
    public Transform shortestTarget;

    private void Start()
    {
        StartCoroutine(RefreshDetectTarget(0.2f));
    }

    IEnumerator RefreshDetectTarget(float delay)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            FindTargetOnRadar();
        }
    }

    private void Update()
    {
        viewRadius = GetComponent<DebugWeaponControl>().distanceLimit.y;
        //viewAngle = GetComponent<DebugWeaponControl>().baseRotLimit.y;

        if (shortestTarget != null)
        {
            GetComponent<DebugWeaponControl>().target = shortestTarget;
        }
    }

    void FindTargetOnRadar()
    {
        targetOnRadar.Clear();

        Collider[] targetInViewRadius = Physics.OverlapSphere(transform.position, viewRadius, entityMask);

        for(int i=0; i < targetInViewRadius.Length; i++)
        {
            Transform target = targetInViewRadius[i].transform;

            Vector3 dirToTarget = (target.position - transform.position).normalized;

            if(Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
            {
                float distToTarget = Vector3.Distance(transform.position, target.position);

                if(shortestTarget == null || Vector3.Distance(transform.position, shortestTarget.position) > distToTarget)
                {
                    shortestTarget = target;
                }

                targetOnRadar.Add(target);
            }
        }
    }

    public Vector3 DirFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal) { angleInDegrees += transform.eulerAngles.y; }

        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }
}

