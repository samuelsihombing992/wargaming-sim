using System;
using System.Collections.Generic;

using UnityEngine;

using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;

using Wargaming.Components.Playback;
using Esri.HPFramework;
using Esri.ArcGISMapsSDK.Components;
using SickscoreGames.HUDNavigationSystem;
using Wargaming.Components.Colyseus;
using Wargaming.Core.GlobalParam;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;
using WGSBundling.Core;

public class EntityWalker : MonoBehaviour
{
    [Header("Follower")]
    //public HPTransform objFollower;

    [Header("Rute Misi")]
    public List<MissionWalker> mission = new List<MissionWalker>();

    [Header("Sync Roll")]
    public bool syncRoll = false;
    //public float rollLimit;
    public float sensitivity = 1;
    public float guidanceDistance;

    [Header("SSS")]
    public float angle = 0;
    public bool doLeftStuff;
    public bool doRightStuff;

    [NonSerialized] public bool isMoving = false;
    [NonSerialized] public OnlineMapsMarker3D tacmapMarker;

    private SplineController splineController;
    public double routeProgress = 0;
    public int CURRENT_NODE = 0;
    public int CURRENT_MISSION = 0;

    //private ShipBundleData _shipBundle;
    //private VehicleBundleData _vehicleBundle;
    //private AircraftBundleData _aircraftBundle;

    private WGSBundleCore bundlingCore;

    private bool useTFGSingleTimeString = false;
    //private Transform physics;
    public SplineController guidance;
    private ArcGISPoint olderPoint;

    private FollowTerrain terrainFollower;

    private float oldAbsPos = 0;

    public AnimationCurve altitudeMotion = AnimationCurve.EaseInOut(0, 0, 1, 1);
    //public double absDistance = 0;

    public void init()
    {
        if (!PlaybackController.instance) Destroy(this);
        //if (objFollower == null)
        //{
        //    Debug.LogError("Walker not have any follower data. Please provide one!");
        //    return;
        //}

        if (mission[0].DATA.Count > 1)
        {
            CURRENT_NODE = 0;
            CURRENT_MISSION = 0;
        }

        splineController = GetComponent<SplineController>();

        bundlingCore = GetComponent<WGSBundleCore>();

        //if (GetComponent<AircraftBundleData>() != null)
        //{
        //    _aircraftBundle = GetComponent<AircraftBundleData>();
        //    rollLimit = _aircraftBundle.turnLimit;
        //    physics = _aircraftBundle.parent.transform;
        //}
        //else if (GetComponent<ShipBundleData>() != null)
        //{
        //    _shipBundle = GetComponent<ShipBundleData>();
        //    rollLimit = _shipBundle.turnLimit;
        //    physics = _shipBundle.parent.transform;
        //}
        //else if (GetComponentInChildren<VehicleBundleData>() != null)
        //{
        //    _vehicleBundle = GetComponent<VehicleBundleData>();
        //    rollLimit = _vehicleBundle.turnLimit;
        //    physics = _vehicleBundle.parent.transform;

        //    terrainFollower = GetComponent<FollowTerrain>();
        //}

        if(ColyseusTFGSingleController.instance != null)
        {
            useTFGSingleTimeString = true;
        }

        //GetComponent<SplineController>().Refresh();

        UpdateParticle();
    }

    private void FixedUpdate()
    {
        if (splineController == null) return;
        if (mission == null)
        {
            splineController.enabled = false;
            if (terrainFollower != null)
            {
                terrainFollower.enabled = false;
            }
            return;
        };

        if (mission.Count == 0)
        {
            splineController.enabled = false;
            if (terrainFollower != null)
            {
                terrainFollower.enabled = false;
            }
            return;
        };

        if(terrainFollower != null)
        {
            terrainFollower.enabled = true;
        }

        if (!useTFGSingleTimeString)
        {
            if (!PlaybackController.IsPlaying()) return;
        }

        if (!splineController.enabled) splineController.enabled = true;

        //if (objFollower == null) return;
        //SyncWithFollowerPos();

        DateTime CURRENT_TIME_FORMAT = PlaybackController.CURRENT_TIME;
        if (useTFGSingleTimeString)
        {
            for(int i=0; i < PlayerData.PLAYERS.Count; i++)
            {
                if(GetComponent<ObjekSatuan>().userID == PlayerData.PLAYERS[i].id)
                {
                    //var dateChange = DateTimeOffset.FromUnixTimeMilliseconds(Convert.ToInt64(Math.Floor(Convert.ToDouble(PlayerData.PLAYERS[i].timeString)))).LocalDateTime;
                    CURRENT_TIME_FORMAT = PlayerData.PLAYERS[i].timeMovement.GetValueOrDefault();

                    i = PlayerData.PLAYERS.Count;
                }
            }
        }

            for (int i = mission.Count - 1; i >= 0; i--)
        {
            if (CURRENT_TIME_FORMAT >= mission[i].DATA[0].datetime)
            {
                CURRENT_MISSION = i;
                i = 0;
            }
            else
            {
                CURRENT_MISSION = 0;
            }
        }

        bool isFound = false;
        if (CURRENT_MISSION == -1) return;

        if (mission[CURRENT_MISSION].DATA.Count == 0) return;

        for (int i = 0; i < mission[CURRENT_MISSION].DATA.Count - 1; i++)
        {
            var selectedMission = mission[CURRENT_MISSION].DATA[i].datetime;
            var nextMission = mission[CURRENT_MISSION].DATA[i + 1].datetime;

            if (CURRENT_TIME_FORMAT == selectedMission)
            {
                CURRENT_NODE = i;
                i = mission[CURRENT_MISSION].DATA.Count;
                isFound = true;
            }
            else if (CURRENT_TIME_FORMAT > selectedMission && CURRENT_TIME_FORMAT < nextMission)
            {
                CURRENT_NODE = i;
                i = mission[CURRENT_MISSION].DATA.Count;
                isFound = true;
            }
        }

        if (!isFound)
        {
            if (CURRENT_TIME_FORMAT < mission[CURRENT_MISSION].DATA[0].datetime)
            {
                CURRENT_NODE = 0;
                routeProgress = 0;
            }
            else
            {
                CURRENT_NODE = mission[CURRENT_MISSION].DATA.Count - 1;
                routeProgress = 1;
            }
        }

        if (splineController.Spline != mission[CURRENT_MISSION].GetComponent<CurvySpline>())
        {
            splineController.Spline = mission[CURRENT_MISSION].GetComponent<CurvySpline>();
        }

        if (guidance != null)
        {
            if(guidance.Spline != splineController.Spline)
            {
                guidance.Spline = splineController.Spline;
            }
        }

        var startDate   = mission[CURRENT_MISSION].DATA[0].datetime;
        var endDate     = mission[CURRENT_MISSION].DATA[mission[CURRENT_MISSION].DATA.Count - 1].datetime;
        var total       = (endDate - startDate).TotalSeconds;

        routeProgress = ((CURRENT_TIME_FORMAT.Subtract(startDate).TotalSeconds) / total);

        if (routeProgress < 0)
        {
            splineController.RelativePosition = 0;
            isMoving = false;

            if (syncRoll && guidance != null)
            {
                bundlingCore.parent.localRotation = Quaternion.Euler(0, 180, 0);
            }

            SyncWheel(0);
        }
        else if (routeProgress > 1)
        {
            splineController.RelativePosition = 1;
            isMoving = false;

            if (syncRoll && guidance != null)
            {
                bundlingCore.parent.localRotation = Quaternion.Euler(0, 180, 0);
            }

            SyncWheel(0);
        }
        else if (routeProgress >= 0 && routeProgress <= 1)
        {
            splineController.AbsolutePosition = (float) (splineController.Spline.Length * (routeProgress * 100) / 100);
            isMoving = true;

            if(oldAbsPos > splineController.AbsolutePosition)
            {
                oldAbsPos = 0;
            }
            var newAbsPos = Mathf.Clamp((splineController.AbsolutePosition - oldAbsPos), 0, (splineController.AbsolutePosition - oldAbsPos));
            oldAbsPos = splineController.AbsolutePosition;

            SyncWheel(newAbsPos);

            if (syncRoll && guidance != null)
            {
                SyncRoll();
            }

        }

        SyncAltitude(splineController.AbsolutePosition);

        UpdateTacmap();
        UpdateParticle();
    }

    public void SyncRoll()
    {
        if (guidance.RelativePosition < splineController.RelativePosition)
        {
            guidance.RelativePosition = 1;
        }
        else
        {
            guidance.AbsolutePosition = splineController.AbsolutePosition;
        }

        if (guidance.RelativePosition < 1)
        {
            guidance.AbsolutePosition += guidanceDistance;

            var locationComponent = GetComponent<ArcGISLocationComponent>();

            if (locationComponent.Position == olderPoint) return;
            var relativePos = guidance.transform.position - transform.position;

            var forward = transform.forward;
            angle = Vector3.Angle(relativePos, forward);
            if (angle == 0)
            {
                doLeftStuff = false;
                doRightStuff = false;
            }
            else
            {
                if (Vector3.Cross(forward, relativePos).y < 0)
                {
                    //Do left stuff
                    doLeftStuff = true;
                    doRightStuff = false;
                }
                else
                {
                    //Do right stuff
                    doLeftStuff = false;
                    doRightStuff = true;
                }

                //if (doLeftStuff && angle < 0 || doRightStuff && angle > 0)
                //{
                //    angle *= -1;
                //}

                if (doLeftStuff && angle > 0 || doRightStuff && angle < 0)
                {
                    angle *= -1;
                }

                if (angle * sensitivity < -bundlingCore.bankingRate) angle = -bundlingCore.bankingRate;
                if (angle * sensitivity > bundlingCore.bankingRate) angle = bundlingCore.bankingRate;

                var rot = locationComponent.Rotation;

                bundlingCore.parent.localRotation = Quaternion.Euler(bundlingCore.origin.x, bundlingCore.origin.y, bundlingCore.origin.z + (angle * sensitivity));
                olderPoint = locationComponent.Position;
            }
        }
    }

    public void SyncAltitude(double absoluteProgress)
    {
        altitudeMotion.Evaluate((float) absoluteProgress);
    }

    public void SyncWheel(float speed)
    {
        //if (_vehicleBundle == null) return;

        //for(int i = 0; i < _vehicleBundle.nodeTires.Count; i++)
        //{
        //    if(_vehicleBundle.nodeTires[i].type == VehicleBundleData.BundleNodeTires.orientation.DIRECTIONAL)
        //    {

        //    }

        //    _vehicleBundle.nodeTires[i].obj.GetComponent<Animator>().SetFloat("speed", speed);
        //}
    }

    public void UpdateTacmap()
    {
        if (tacmapMarker != null && EntityController.instance.tacmapOrientation)
        {
            var objPos = GetComponent<ArcGISLocationComponent>();
            tacmapMarker.SetPosition(objPos.Position.X, objPos.Position.Y);

            var tacmapHUD = tacmapMarker.instance.GetComponent<HUDNavigationElement>();
            if (tacmapHUD != null)
            {
                tacmapHUD.Indicator.RotateIcon(new Vector3(0, 180, (float)objPos.Rotation.Heading));
            }
        }
    }

    public void UpdateParticle()
    {
        //if (_aircraftBundle != null)
        //{
        //    for (int i = 0; i < _aircraftBundle.nodeParticles.Count; i++)
        //    {
        //        if(_aircraftBundle.nodeParticles[i].obj != null)
        //        {
        //            _aircraftBundle.nodeParticles[i].obj.SetActive(isMoving);
        //        }
        //    }
        //}
        //else if (_shipBundle != null)
        //{

        //}
        //else if (_vehicleBundle != null)
        //{

        //}
    }






    //altitudeMotion.AddKey(2512, 5);
    //altitudeMotion.AddKey(40000, 8000);





    //var length = 80000;

    //// Motion takeoff dan landing akan mengambil 20% dari panjang rute aktif (40% jalur takeoff dan landing, 60% jalur flight)
    //// Maximal Takeoff / Landing Length = 40000
    //var takeoffLandingLength = (20 / length) * 100;
    //if (takeoffLandingLength > 40000) takeoffLandingLength = 40000;

    //altitudeMotion.AddKey(2512, 5);

    //// 0,2 PER 1 length
    //altitudeMotion.AddKey(takeoffLandingLength, (0.2f * takeoffLandingLength));

    //takeoffPitch.AddKey(2012, 0);
    //takeoffPitch.AddKey(2512, 20);

    //// Set Takeoff Pitch Ending 20% from last Takeoff or Landing Length
    //takeoffPitch.AddKey(takeoffLandingLength - ((20 / length) * 100), 20);
    //takeoffPitch.AddKey(takeoffLandingLength, 0);





    //takeoffPitch.AddKey(2012, 0);
    //takeoffPitch.AddKey(2512, 20);
    //takeoffPitch.AddKey(16500, 20);
    //takeoffPitch.AddKey(20000, 0);

    //if (splineController.Spline.Length > 80000)
    //{



    //    altitudeMotion.AddKey((40000 / 2), 8000);
    //}

    //takeoffPitch.AddKey(2012, 0);
    //takeoffPitch.AddKey(2512, 20);
    //takeoffPitch.AddKey(16500, 20);
    //takeoffPitch.AddKey(20000, 0);

}
