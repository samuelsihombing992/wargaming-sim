using UnityEngine;

using Esri.ArcGISMapsSDK.Components;
using Esri.GameEngine.Geometry;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Wargaming.Core.Network;
using System.Collections.Generic;
using static Wargaming.Core.GlobalParam.EntityHelper;
using Esri.HPFramework;
using Wargaming.Core;
using Esri.ArcGISMapsSDK.Utils.Math;

public static class TransformExtended
{
    public static void SetScale(this Transform _transform, float scale)
    {
        _transform.localScale = new Vector3(scale, scale, scale);
    }

    public static void SetScaleByType(this Transform _transform, KategoriSatuan _type)
    {
        float scale = 1;
        switch (_type)
        {
            case KategoriSatuan.VEHICLE:
                scale = StartupConfig.settings.DEFAULT_VEHICLE_SCALE.GetValueOrDefault(1);
                break;
            case KategoriSatuan.SHIP:
                scale = StartupConfig.settings.DEFAULT_SHIP_SCALE.GetValueOrDefault(1);
                break;
            case KategoriSatuan.AIRCRAFT:
                scale = StartupConfig.settings.DEFAULT_AIRCRAFT_SCALE.GetValueOrDefault(1);
                break;
            default: break;
        }

        _transform.localScale = new Vector3(scale, scale, scale);
    }

    public static void SetAltitude(this Transform _transform, int altitude, int offset = 0)
    {
        var _location = _transform.GetComponent<ArcGISLocationComponent>();
        if (_location == null) return;

        _location.Position = new ArcGISPoint(_location.Position.X, _location.Position.Y, altitude, _location.Position.SpatialReference);
    }

    public static void SetCoordinate(this Transform _transform, Vector3 coordinate)
    {
        var _location = _transform.GetComponent<ArcGISLocationComponent>();
        if (_location == null) return;

        _location.Position = new ArcGISPoint(coordinate.x, coordinate.y, coordinate.z, _location.Position.SpatialReference);
    }

    public static void SetRootCoordinate(this Transform _transform, Vector3 coordinate)
    {
        var hpTransform = _transform.GetComponent<HPTransform>();
        if (hpTransform == null) return;

        var cartesianPosition = EnvironmentsController.GetMapComponent().View.GeographicToWorld(new ArcGISPoint(coordinate.x, coordinate.y, coordinate.z, ArcGISSpatialReference.WGS84()));
        hpTransform.UniversePosition = cartesianPosition;
    }

    public static void SetRootCoordinate(this Transform _transform, HPTransform transformCoordinate)
    {
        var hpTransform = _transform.GetComponent<HPTransform>();
        if (hpTransform == null) return;

        hpTransform.UniversePosition = transformCoordinate.UniversePosition;
    }

    public static ArcGISPoint GetHPPosition(this Transform _transform)
    {
        var hpTransform = _transform.GetComponent<HPTransform>();
        if (hpTransform == null) return new ArcGISPoint(0,0,0, ArcGISSpatialReference.WGS84());

        return EnvironmentsController.GetMapComponent().View.WorldToGeographic(hpTransform.UniversePosition);
    }

    public static ArcGISRotation GetHPRotation(this Transform _transform)
    {
        var hpTransform = _transform.GetComponent<HPTransform>();
        if (hpTransform == null) return new ArcGISRotation(0,0,0);

        var pos = EnvironmentsController.GetMapComponent().View.WorldToGeographic(hpTransform.UniversePosition);
        return GeoUtils.FromCartesianRotation(
            hpTransform.UniversePosition,
            hpTransform.UniverseRotation.ToQuaterniond(),
            EnvironmentsController.GetMapComponent().View.SpatialReference,
            EnvironmentsController.GetMapComponent().View.Map.MapType
        );
    }

    public static void SetOrientation(this Transform _transform, Vector3 orientation)
    {
        var _location = _transform.GetComponent<ArcGISLocationComponent>();
        if (_location == null) return;

        _location.Rotation = new ArcGISRotation(orientation.x, orientation.y, orientation.z);
    }

    public static void SetRootOrientation(this Transform _transform, Vector3 orientation)
    {
        var hpTransform = _transform.GetComponent<HPTransform>();
        if (hpTransform == null) return;

        var cartesianRotation = GeoUtils.ToCartesianRotation(hpTransform.UniversePosition, new ArcGISRotation(orientation.x, orientation.y, orientation.z), ArcGISSpatialReference.WGS84(), EnvironmentsController.GetMapComponent().MapType);

        hpTransform.UniverseRotation = cartesianRotation.ToQuaternion();
    }

    public static void SetReactivate(this GameObject _obj)
    {
        _obj.SetActive(false);
        _obj.SetActive(true);
    }

    public static void SetLayerRecursively(this GameObject obj, int newLayer)
    {
        if (null == obj)
        {
            return;
        }

        obj.layer = newLayer;

        foreach (Transform child in obj.transform)
        {
            if (null == child)
            {
                continue;
            }
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }
}