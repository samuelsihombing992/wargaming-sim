using Battlehub.RTCommon;
using Shapes;
using UnityEngine;

using Wargaming.Core;

public class DrawingSegmentWatcher : MonoBehaviour
{
    public int index;
    public Vector3 oldTransform;

    //private void Update()
    //{
    //    return;
    //    if(oldTransform == null)
    //    {
    //        oldTransform = transform.position;
    //    }

    //    if(transform.position != oldTransform && !WargamingCam.instance.onRebase)
    //    {
    //        oldTransform = transform.position;
    //        RefreshDrawSegment();
    //    }

    //    if(GetComponent<ExposeToEditor>() != null)
    //    {
    //        var distance = (Camera.main.transform.position - transform.position).magnitude;
    //        var size = distance * 0.00025f * Camera.main.fieldOfView;
    //        transform.localScale = Vector3.one * size;
    //    }
    //}

    public void RefreshDrawSegment()
    {
        if (GetComponentInParent<ObjekDrawPolyline>() != null)
        {
            var polyline = GetComponentInParent<ObjekDrawPolyline>().polyline;

            if (polyline.points.Count < (index + 1)) return;
            polyline.SetPointPosition(index, oldTransform);
            return;
        }

        if (GetComponentInParent<ObjekDrawPolygon>() != null)
        {
            var polygon = GetComponentInParent<ObjekDrawPolygon>().polygon;
            var polygonOutline = GetComponentInParent<ObjekDrawPolygon>().outline;

            if (polygon.points.Count < (index + 1) || polygonOutline.points.Count < (index + 1)) return;
            polygon.SetPointPosition(index, new Vector2(oldTransform.x, oldTransform.z));
            polygonOutline.SetPointPosition(index, oldTransform);
            return;
        }

        if(GetComponentInParent<ObjekDrawRectangle>() != null)
        {
            var polygon = GetComponentInParent<ObjekDrawRectangle>().polygon;
            var polygonOutline = GetComponentInParent<ObjekDrawRectangle>().outline;

            if (polygon.points.Count < (index + 1) || polygonOutline.points.Count < (index + 1)) return;
            polygon.SetPointPosition(index, new Vector2(oldTransform.x, oldTransform.z));
            polygonOutline.SetPointPosition(index, oldTransform);
            return;
        }

        //if (GetComponentInParent<ObjekDrawArrow>() != null)
        //{
        //    var polygon = GetComponentInParent<ObjekDrawArrow>().polygon;
        //    var polygonOutline = GetComponentInParent<ObjekDrawArrow>().outline;

        //    if (polygon.points.Count < (index + 1) || polygonOutline.points.Count < (index + 1)) return;
        //    polygon.SetPointPosition(index, new Vector2(oldTransform.x, oldTransform.z));
        //    polygonOutline.SetPointPosition(index, oldTransform);
        //    return;
        //}
    }
}
