using UnityEngine;
using Unity.Mathematics;

using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;
using Esri.HPFramework;

using Wargaming.Components.Playback;
using System;

public class FollowTerrain : MonoBehaviour
{
    public bool syncPlayback = true;
    public bool syncRotation = false;
    public bool floatingOnOcean = true;
    public LayerMask PlayerLayerMask;

    public float elevationOffset = 0f;
    public double altitude = 0;

    private ArcGISMapComponent arcGISMapComponent;
    private ArcGISLocationComponent locationComponent;
    //[SerializeField] private AnimationCurve animCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

    [SerializeField] public float syncTime = 1.0f; // Sync Waktu dari Colyseus tiap X-Time

    [SerializeField] public float syncPeriod = 0.0f;
    [SerializeField] public bool resyncTime = false;

    public Rigidbody physics;

    private void Start()
    {
        arcGISMapComponent = FindObjectOfType<ArcGISMapComponent>();
        if (arcGISMapComponent == null)
        {
            enabled = false;
            return;
        }

        locationComponent = GetComponent<ArcGISLocationComponent>();
        if (locationComponent == null)
        {
            enabled = false;
            return;
        }

        elevationOffset = 0;
        syncTime = 0f;

        //animCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);
    }

    private void FixedUpdate()
    {
        if (!SyncByPlayback()) return;

        if (syncPeriod > syncTime) resyncTime = true;
        syncPeriod += Time.deltaTime;

        if (!resyncTime) return;
        SetElevation();

        //if (PlaybackController.instance.speedFactor > 10)
        //{
        //    physics.isKinematic = true;
        //    SetElevation();
        //}
        //else
        //{

        //    physics.isKinematic = false;
        //}
    }

    private bool SyncByPlayback()
    {
        if (!syncPlayback) return true;
        if (!PlaybackController.IsPlaying())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // Does a raycast to find the ground
    private void SetElevation()
    {
        // start the raycast in the air at an arbitrary to ensure it is above the ground
        var raycastHeight = 8000;
        var position = gameObject.transform.position;

        var raycastStart = new Vector3(position.x, position.y + raycastHeight, position.z);
        var hitInfo = Physics.RaycastAll(raycastStart, Vector3.down);

        for(int i=0; i < hitInfo.Length; i++)
        {
            if (hitInfo[i].transform == this) continue;

            locationComponent.Position = HitToGeoPosition(hitInfo[i], elevationOffset);
        }

        //if (hitInfo)
        //{
        //    if (hitInfo.transform.tag != "entity")
        //    {
        //        //var tempRot = locationComponent.Rotation;

        //        locationComponent.Position = HitToGeoPosition(hitInfo, elevationOffset);
        //        //Debug.Log(s.X + " | " + s.Y + " | " + s.Z);

        //        //locationComponent.Position = new ArcGISPoint(locationComponent.Position.X, locationComponent.Position.Y, s.Z, ArcGISSpatialReference.WGS84());
        //        //locationComponent.Rotation = tempRot;

        //        //if (syncRotation)
        //        //{
        //        //    var rotationRef = Quaternion.Lerp(transform.rotation, Quaternion.FromToRotation(Vector3.up, hitInfo.normal), animCurve.Evaluate(0.25f));

        //        //    if (name == "satuan_0_184_997")
        //        //    {
        //        //        Debug.Log(rotationRef.eulerAngles.x + " | " + rotationRef.eulerAngles.y + " | " + rotationRef.eulerAngles.z);
        //        //    }

        //        //    locationComponent.Rotation = new ArcGISRotation(locationComponent.Rotation.Heading, 90 + rotationRef.eulerAngles.y + 8, rotationRef.eulerAngles.z);

        //        //    //locationComponent.Rotation = new ArcGISRotation(locationComponent.Rotation.Heading, 90 + rotationRef.eulerAngles.y, rotationRef.eulerAngles.z);
        //        //}
        //    }
        //}

        syncPeriod = 0;
        resyncTime = false;
    }

    /// <summary>
    /// Return GeoPosition Based on RaycastHit; I.E. Where the user clicked in the Scene.
    /// </summary>
    /// <param name="hit">Raycasted Object (Terrain)</param>
    /// <param name="offset">Altitude + Offset</param>
    /// <returns></returns>
    private ArcGISPoint HitToGeoPosition(RaycastHit hit, float yOffset = 0)
    {
        var worldPosition = math.inverse(arcGISMapComponent.WorldMatrix).HomogeneousTransformPoint(hit.point.ToDouble3());

        var geoPosition = arcGISMapComponent.View.WorldToGeographic(worldPosition);

        var tempAltitude = geoPosition.Z;
        if (floatingOnOcean)
        {
            if (tempAltitude < StartupConfig.settings.DEFAULT_OCEAN_HEIGHT - 1.5f)
            {
                altitude = StartupConfig.settings.DEFAULT_OCEAN_HEIGHT.GetValueOrDefault(3) - 1.5f;
            }
            else
            {
                altitude = tempAltitude;
            }
        }

        var offsetPosition = new ArcGISPoint(geoPosition.X, geoPosition.Y, altitude + yOffset, geoPosition.SpatialReference);
        return GeoUtils.ProjectToSpatialReference(offsetPosition, ArcGISSpatialReference.WGS84());
    }
}
