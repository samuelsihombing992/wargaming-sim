using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

public static class ContentSizeFitterExtended
{
    public static async Task Refresh(this ContentSizeFitter fitter)
    {
        fitter.enabled = false;

        await Task.Delay(100);
        fitter.enabled = true;
    }
}