using System;
using System.Collections.Generic;

using UnityEngine;

using Shapes;
using Esri.GameEngine.Geometry;
using Esri.ArcGISMapsSDK.Components;
using Wargaming.Core.GlobalParam;

namespace Wargaming.Core.ScenarioEditor.Drawing
{
    public class EDrawPredictor : MonoBehaviour
    {
        private ObjekDrawing selected;

        [NonSerialized] public ArcGISPoint mousePos;
        public List<Transform> segments = new List<Transform>();

        public Polyline polyline;
        public Polygon polygon;
        public Disc circle;

        public int testCurve = 22;
        public AnimationCurve testCurveAnim;

        private enum NodePos { ATAS, BAWAH, KIRI, KANAN }
        private NodePos HPos;
        private NodePos VPos;

        public void init()
        {
            selected = PlotDrawingController.instance.selectedDrawing;
            if (selected == null) return;

            mousePos = new ArcGISPoint(0, 0, 0, ArcGISSpatialReference.WGS84());
            segments = new List<Transform>();

            DestroyUnecessaryShape();

            if (selected.type == DrawingHelper.DrawingType.POLYLINE || selected.type == DrawingHelper.DrawingType.POLYGON)
            {
                var segment1 = CreateNewSegment();
                var segment2 = CreateNewSegment();

                segments.Add(segment1);
                segments.Add(segment2);

                polyline.points = new List<PolylinePoint>();
                polyline.AddPoint(Vector3.zero);
                polyline.AddPoint(Vector3.zero);
            }
            else if(selected.type == DrawingHelper.DrawingType.RECTANGLE)
            {
                var segment1 = CreateNewSegment();
                var segment2 = CreateNewSegment();
                var segment3 = CreateNewSegment();
                var segment4 = CreateNewSegment();

                segments.Add(segment1);
                segments.Add(segment2);
                segments.Add(segment3);
                segments.Add(segment4);

                polyline.points = new List<PolylinePoint>();
                polyline.Closed = true;
                polyline.AddPoint(Vector3.zero);
                polyline.AddPoint(Vector3.zero);
                polyline.AddPoint(Vector3.zero);
                polyline.AddPoint(Vector3.zero);

                polygon.points = new List<Vector2>();
                polygon.AddPoint(Vector2.zero);
                polygon.AddPoint(Vector2.zero);
                polygon.AddPoint(Vector2.zero);
                polygon.AddPoint(Vector2.zero);
            }
            else if(selected.type == DrawingHelper.DrawingType.CIRCLE || selected.type == DrawingHelper.DrawingType.ELLIPSE)
            {
                var segment1 = CreateNewSegment();
                var segment2 = CreateNewSegment();

                segments.Add(segment1);
                segments.Add(segment2);
            }else if(selected.type == DrawingHelper.DrawingType.ARROW && selected.arrowType == DrawingHelper.DrawingArrowType.ARROW)
            {
                var arrowLeft       = CreateNewSegment();
                var arrowLeftEnd    = CreateNewSegment();
                var arrowMiddle     = CreateNewSegment();
                var arrowRight      = CreateNewSegment();
                var arrowRightEnd   = CreateNewSegment();

                var segment1 = CreateNewSegment();
                var segment2 = CreateNewSegment();
                var segment3 = CreateNewSegment();
                var segment4 = CreateNewSegment();

                segments.Add(segment1);

                segments.Add(arrowLeft);
                segments.Add(arrowLeftEnd);
                segments.Add(segment3);
                segments.Add(segment2);
                segments.Add(segment4);
                segments.Add(arrowRightEnd);
                segments.Add(arrowRight);
                segments.Add(arrowRight);
                segments.Add(arrowMiddle);

                polyline.points = new List<PolylinePoint>();
                polyline.Closed = true;
                polyline.AddPoint(Vector3.zero);
                polyline.AddPoint(Vector3.zero);
                polyline.AddPoint(Vector3.zero);
                polyline.AddPoint(Vector3.zero);
                polyline.AddPoint(Vector3.zero);
                polyline.AddPoint(Vector3.zero);
                polyline.AddPoint(Vector3.zero);
                polyline.AddPoint(Vector3.zero);

                //polygon.points = new List<Vector2>();
                //polygon.AddPoint(Vector2.zero);
                //polygon.AddPoint(Vector2.zero);
                //polygon.AddPoint(Vector2.zero);
                //polygon.AddPoint(Vector2.zero);
                //polygon.AddPoint(Vector2.zero);
                //polygon.AddPoint(Vector2.zero);
                //polygon.AddPoint(Vector2.zero);
                //polygon.AddPoint(Vector2.zero);
            }
            else if (selected.type == DrawingHelper.DrawingType.ARROW && selected.arrowType == DrawingHelper.DrawingArrowType.ARROW_UP)
            {
                polyline.points = new List<PolylinePoint>();

                for (int i=0; i < (testCurve * 2) + 10; i++)
                {
                    var segment = CreateNewSegment();
                    segments.Add(segment);

                    polyline.Closed = false;
                    polyline.AddPoint(Vector3.zero);
                }
            }
        }

        private void DestroyUnecessaryShape()
        {
            switch (selected.type)
            {
                case DrawingHelper.DrawingType.POLYLINE:
                    Destroy(polygon.gameObject);
                    Destroy(circle.gameObject);
                    break;
                case DrawingHelper.DrawingType.POLYGON:
                    Destroy(circle.gameObject);
                    break;
                case DrawingHelper.DrawingType.RECTANGLE:
                    Destroy(circle.gameObject);
                    break;
                case DrawingHelper.DrawingType.CIRCLE:
                    Destroy(polyline.gameObject);
                    Destroy(polygon.gameObject);
                    break;
                case DrawingHelper.DrawingType.ARROW:
                    Destroy(polygon.gameObject);
                    Destroy(circle.gameObject);
                    break;
            }
        }

        private void Update()
        {
            if (segments.Count <= 0) return;
            if (selected == null || selected.geometry.Count <= 0) return;

            DrawPredictionLine();
        }

        private void DrawPredictionLine()
        {
            var objDrawGeometry = selected.geometry;

            if(selected.type == DrawingHelper.DrawingType.POLYLINE || selected.type == DrawingHelper.DrawingType.POLYGON)
            {
                SetSegmentPos((segments.Count - 1), mousePos);
                SetSegmentPos((segments.Count - 2), objDrawGeometry[objDrawGeometry.Count - 1].transform.position);

                SetPolylinePos(0, segments[segments.Count - 2].position);
                SetPolylinePos(1, segments[segments.Count - 1].position);

                return;
            }else if(selected.type == DrawingHelper.DrawingType.RECTANGLE)
            {
                SetSegmentPos(0, objDrawGeometry[0].transform.position);
                SetSegmentPos(2, mousePos);

                RefreshRectAutoSegment();

                return;
            }else if(selected.type == DrawingHelper.DrawingType.CIRCLE)
            {
                SetSegmentPos(0, objDrawGeometry[0].transform.position);
                circle.gameObject.transform.position = segments[0].position;

                SetSegmentPos(1, mousePos);
                SetSegmentRadius(segments[1].transform);
            }else if(selected.type == DrawingHelper.DrawingType.ARROW && selected.arrowType == DrawingHelper.DrawingArrowType.ARROW)
            {
                SetSegmentPos(0, objDrawGeometry[0].transform.position);
                SetSegmentPos(4, mousePos);

                RefreshArrowAutoSegment();
            }else if(selected.type == DrawingHelper.DrawingType.ARROW && selected.arrowType == DrawingHelper.DrawingArrowType.ARROW_UP)
            {
                SetSegmentPos(0, objDrawGeometry[0].transform.position);
                SetSegmentPos(1, mousePos);

                RefreshArrowCurveAutoSegment();
            }
        }

        private void SetSegmentPos(int _index, Vector3 _position)
        {
            segments[_index].position = _position;
        }

        private void SetSegmentPos(int _index, ArcGISPoint _position)
        {
            segments[_index].GetComponent<ArcGISLocationComponent>().Position = _position;
        }

        private void SetPolylinePos(int _index, Vector3 _position)
        {
            polyline.SetPointPosition(_index, _position);
        }

        private void SetPolygonPos(int _index, Vector3 _position)
        {
            polygon.SetPointPosition(_index, new Vector2(_position.x, _position.z));
        }

        private void SetSegmentRadius(Transform _endPos)
        {
            circle.Radius = Vector3.Distance(segments[0].position, _endPos.position);
        }

        private void RefreshRectAutoSegment()
        {
            if (polyline.Count != 4 || polygon.Count != 4) return;

            var startPoint  = segments[0];
            var endPoint    = segments[2];

            HPos = (endPoint.localPosition.x >= startPoint.localPosition.x) ? NodePos.KANAN : NodePos.KIRI;
            VPos = (endPoint.localPosition.z >= startPoint.localPosition.z) ? NodePos.ATAS : NodePos.BAWAH;

            var autoSegment1 = segments[1].transform;
            var autoSegment2 = segments[3].transform;

            if (VPos == NodePos.ATAS)
            {
                autoSegment1.position = startPoint.position + startPoint.forward * (endPoint.localPosition.z - startPoint.localPosition.z);
            }
            else
            {
                autoSegment1.position = startPoint.position + -startPoint.forward * (startPoint.localPosition.z - endPoint.localPosition.z);
            }

            if (HPos == NodePos.KANAN)
            {
                autoSegment2.position = startPoint.position + startPoint.right * (endPoint.localPosition.x - startPoint.localPosition.x);
            }
            else
            {
                autoSegment2.position = startPoint.position + -startPoint.right * (startPoint.localPosition.x - endPoint.localPosition.x);
            }

            SetPolylinePos(0, segments[0].position);
            SetPolylinePos(1, autoSegment1.position);
            SetPolylinePos(2, segments[2].position);
            SetPolylinePos(3, autoSegment2.position);

            SetPolygonPos(0, segments[0].position);
            SetPolygonPos(1, autoSegment1.position);
            SetPolygonPos(2, segments[2].position);
            SetPolygonPos(3, autoSegment2.position);
        }

        private void RefreshArrowAutoSegment()
        {
            if (segments.Count < 8) return;

            float angle = 0;

            var startPoint = segments[0];
            var endPoint = segments[4];
            var arrowMiddle = segments[9];

            var endPointLeft = segments[3];
            var endPointRight = segments[5];
 
            var arrowLeft = segments[1];
            var arrowLeftEnd = segments[2];
            var arrowRight = segments[6];
            var arrowRightEnd = segments[7];

            endPoint.LookAt(startPoint);
            angle = endPoint.eulerAngles.y;

            var arrowLength = Vector3.Distance(startPoint.position, endPoint.position) / 5;

            startPoint.rotation     = Quaternion.Euler(startPoint.rotation.x, angle, startPoint.rotation.z);
            arrowLeft.rotation      = Quaternion.Euler(arrowLeft.rotation.x, angle, arrowLeft.rotation.z);
            arrowRight.rotation     = Quaternion.Euler(arrowRight.rotation.x, angle, arrowRight.rotation.z);
            arrowLeftEnd.rotation   = Quaternion.Euler(arrowLeftEnd.rotation.x, angle, arrowLeftEnd.rotation.z);
            arrowRightEnd.rotation  = Quaternion.Euler(arrowRightEnd.rotation.x, angle, arrowRightEnd.rotation.z);
            endPointLeft.rotation   = Quaternion.Euler(endPointLeft.rotation.x, angle, endPointLeft.rotation.z);
            endPointRight.rotation  = Quaternion.Euler(arrowLeft.rotation.x, angle, endPointRight.rotation.z);
            arrowMiddle.rotation    = Quaternion.Euler(arrowMiddle.rotation.x, angle, arrowMiddle.rotation.z);

            arrowLeft.position      = startPoint.position + (-startPoint.forward * arrowLength) + (-startPoint.right * arrowLength);
            arrowRight.position     = startPoint.transform.position + (-startPoint.forward * arrowLength) + (startPoint.right * arrowLength);

            var arrowWidth = Vector3.Distance(arrowLeft.position, arrowRight.position);

            arrowLeftEnd.position   = arrowLeft.position + (arrowLeft.right * ((arrowWidth * 30) / 100));
            arrowRightEnd.position  = arrowRight.position + (-arrowRight.right * ((arrowWidth * 30) / 100));

            endPointLeft.position   = endPoint.position + (-endPoint.right * ((arrowWidth * 20) / 100));
            endPointRight.position  = endPoint.position + (endPoint.right * ((arrowWidth * 20) / 100));

            arrowMiddle.position = startPoint.position + (-startPoint.forward * arrowLength);

            SetPolylinePos(0, startPoint.position);
            SetPolylinePos(1, arrowLeft.position);
            SetPolylinePos(2, arrowLeftEnd.position);
            SetPolylinePos(3, endPointLeft.position);
            SetPolylinePos(4, endPoint.position);
            SetPolylinePos(5, endPointRight.position);
            SetPolylinePos(6, arrowRightEnd.position);
            SetPolylinePos(7, arrowRight.position);
        }

        private void RefreshArrowCurveAutoSegment()
        {
            float angle = 0;

            var startPoint = segments[0];
            var endPoint = segments[1];

            var startPointLeft = segments[2];

            startPoint.LookAt(endPoint);
            angle = startPoint.eulerAngles.y;

            var arrowLength = Vector3.Distance(startPoint.position, endPoint.position) / 10;

            startPointLeft.rotation = Quaternion.Euler(startPoint.rotation.x, angle, startPoint.rotation.z);

            startPointLeft.position = startPoint.position + (-startPoint.forward * arrowLength);

            SetPolylinePos(0, startPointLeft.position);
            SetPolylinePos(1, startPoint.position);

            var radius              = Vector3.Distance(startPoint.position, endPoint.position) / 2f;
            var centerPos           = (startPoint.position + endPoint.position) / 2f;
            var centerDirection     = Quaternion.LookRotation((startPoint.position - endPoint.position).normalized);



            for (int i=2; i < testCurve; i++)
            {
                var point = segments[i];
                point.rotation = Quaternion.Euler(endPoint.rotation.x, angle, endPoint.rotation.z);

                var angleOfCurve = -Mathf.PI * (i + 1) / (30 + 1f);
                var x = Mathf.Sin(angleOfCurve) * radius;
                var z = Mathf.Cos(angleOfCurve) * radius;
                var pos = new Vector3(x, 0, z);

                pos = centerDirection * pos;

                var segment = segments[i];
                segment.position = centerPos + pos;

                SetPolylinePos(i, segment.position);
            }

            // Arrow Segments
            var arrowLeftEnd    = segments[testCurve - 1];
            arrowLeftEnd.name   = "arrowLeftEnd";
            var arrowLeft       = segments[testCurve];
            arrowLeft.name   = "arrowLeft";
            var arrowEnd        = segments[testCurve + 1];
            arrowEnd.name   = "arrowEnd";
            var arrowRight      = segments[testCurve + 2];
            arrowRight.name   = "arrowRight";
            var arrowRightEnd   = segments[testCurve + 3];
            arrowRightEnd.name   = "arrowRightEnd";

            arrowLeftEnd.rotation   = Quaternion.Euler(endPoint.rotation.x, angle - 90, endPoint.rotation.z);
            arrowLeft.rotation      = Quaternion.Euler(endPoint.rotation.x, angle - 90, endPoint.rotation.z);
            arrowEnd.rotation       = Quaternion.Euler(endPoint.rotation.x, angle - 90, endPoint.rotation.z);
            arrowRight.rotation     = Quaternion.Euler(endPoint.rotation.x, angle - 90, endPoint.rotation.z);
            arrowRightEnd.rotation  = Quaternion.Euler(endPoint.rotation.x, angle - 90, endPoint.rotation.z);



            arrowLeft.position = arrowLeftEnd.position + (-arrowLeftEnd.right * ((arrowLength * 80) / 100));
            arrowRightEnd.position = arrowLeftEnd.position + (arrowLeftEnd.right * arrowLength);
            arrowRight.position = arrowRightEnd.position + (arrowRightEnd.right * ((arrowLength * 80) / 100));

            SetPolylinePos(testCurve, arrowLeft.position);
            SetPolylinePos(testCurve + 1, endPoint.position);
            SetPolylinePos(testCurve + 2, arrowRight.position);
            SetPolylinePos(testCurve + 3, arrowRightEnd.position);

            radius = Vector3.Distance(startPointLeft.position, endPoint.position) / 2f;
            centerPos = (startPointLeft.position + endPoint.position) / 2f;
            centerDirection = Quaternion.LookRotation((startPointLeft.position - endPoint.position).normalized);

            for (int i = (testCurve + 4); i < (testCurve + 4 + testCurve); i++)
            {
                var point = segments[i];
                point.rotation = Quaternion.Euler(endPoint.rotation.x, angle, endPoint.rotation.z);

                var angleOfCurve = -Mathf.PI * (i + 1) / (30 + 1f);
                var x = Mathf.Sin(angleOfCurve) * radius;
                var z = Mathf.Cos(angleOfCurve) * radius;
                var pos = new Vector3(x, 0, z);

                pos = centerDirection * pos;

                var segment = segments[i];
                segment.position = centerPos + pos;

                SetPolylinePos(i, segment.position);
            }

            //SetPolylinePos(32, endPoint.position);

            //var arrowLeft = segments[2];
            //var arrowLeftEnd = segments[3];
            //var arrowRight = segments[segments.Count - 1];
            //var arrowRightEnd = segments[segments.Count - 2];

            //var startPointRight = segments[34];

            //endPoint.LookAt(startPoint);
            //startPoint.LookAt(endPoint);
            //angle = endPoint.eulerAngles.y + 90;

            //var arrowLength = Vector3.Distance(startPoint.position, endPoint.position) / 10;

            //endPoint.rotation       = Quaternion.Euler(endPoint.rotation.x, angle, endPoint.rotation.z);
            //arrowLeft.rotation      = Quaternion.Euler(endPoint.rotation.x, angle, endPoint.rotation.z);
            //arrowLeftEnd.rotation   = Quaternion.Euler(endPoint.rotation.x, angle, endPoint.rotation.z);
            //arrowRight.rotation     = Quaternion.Euler(endPoint.rotation.x, angle, endPoint.rotation.z);
            //arrowRightEnd.rotation  = Quaternion.Euler(endPoint.rotation.x, angle, endPoint.rotation.z);

            //arrowLeft.position = endPoint.position + (-endPoint.forward * arrowLength) + (-endPoint.right * arrowLength);
            //arrowRight.position = endPoint.transform.position + (-endPoint.forward * arrowLength) + (endPoint.right * arrowLength);

            //var arrowWidth = Vector3.Distance(arrowLeft.position, arrowRight.position);

            //arrowLeftEnd.position = arrowLeft.position + (arrowLeft.right * ((arrowWidth * 30) / 100));
            ////arrowRightEnd.position = arrowRight.position + (-arrowRight.right * ((arrowWidth * 30) / 100));


            //SetPolylinePos(31, endPoint.position);
            //SetPolylinePos(2, arrowLeftEnd.position);

            return;

            //var radius = Vector3.Distance(startPoint.position, endPoint.position) / 2f;
            //var centerPos = (startPoint.position + endPoint.position) / 2f;
            //var centerDirection = Quaternion.LookRotation((endPoint.position - startPoint.position).normalized);

            for (int i = 1; i < 30; i++)
            {
                var point = segments[i];

                point.rotation = Quaternion.Euler(endPoint.rotation.x, angle, endPoint.rotation.z);

                var angleOfCurve = Mathf.PI * (i + 1) / (30 + 1f);
                var x = Mathf.Sin(angleOfCurve) * radius;
                var z = Mathf.Cos(angleOfCurve) * radius;
                var pos = new Vector3(x, 0, z);

                pos = centerDirection * pos;

                var segment = segments[i];
                segment.position = centerPos + pos;

                SetPolylinePos(i, segment.position);
            }

            //startPointRight.position = startPoint.position + (-startPoint.forward * ((arrowWidth * 20) / 100));
            //SetPolylinePos(34, startPointRight.position);

            return;
            
            //for(int i=2; i < 30; i++)
            //{
            //    var angle = Mathf.PI * (i + 1) / (30 + 1f);
            //    var x = Mathf.Sin(angle) * radius;
            //    var z = Mathf.Cos(angle) * radius;
            //    var pos = new Vector3(x, 0, z);

            //    pos = centerDirection * pos;

            //    var segment = segments[i];
            //    segment.position = centerPos + pos;

            //    SetPolylinePos(i, segment.position);
            //}

            //SetPolylinePos(0, startPoint.position);
            //SetPolylinePos(1, endPoint.position);

            //for (int i=2; i < 30; i++)
            //{
            //    SetPolylinePos(i, segments);
            //}

        }

        //private Polyline AddPolylineComponent()
        //{
        //    var polyline = gameObject.AddComponent<Polyline>();
        //    polyline.Closed = false;
        //    polyline.Geometry = PolylineGeometry.Billboard;
        //    polyline.ZTest = UnityEngine.Rendering.CompareFunction.NotEqual;

        //    polyline.Thickness = selected.properties.weight;
        //    polyline.ThicknessSpace = ThicknessSpace.Pixels;
        //    polyline.Color = selected.properties.warna;

        //    polyline.points.Clear();

        //    return polyline;
        //}

        private Transform CreateNewSegment()
        {
            var _segment = EntityController.instance.CreateEntity(
                mousePos,
                new Vector3(0, 90, 0),
                AssetPackageController.Instance.prefabDrawingSegment.gameObject,
                transform
            );

            return _segment.transform;
        }
    }
}