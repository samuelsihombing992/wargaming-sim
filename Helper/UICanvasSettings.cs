using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UICanvasSettings : MonoBehaviour
{
    private static GraphicRaycaster graphicRaycast;
    private static PointerEventData pointerEvent;
    private static EventSystem eventSystem;

    private void Awake()
    {
        graphicRaycast = GetComponent<GraphicRaycaster>();
        eventSystem = GetComponent<EventSystem>();
    }

    private void Start()
    {
        Canvas.ForceUpdateCanvases();
    }

    /// <summary>
    /// Cek apakah Cursor Sedang Berada Diatas UI
    /// </summary>
    public static bool IsRaycastingWithUI()
    {
        //Set up the new Pointer Event
        pointerEvent = new PointerEventData(eventSystem);

        //Set the Pointer Event Position to that of the mouse position
        pointerEvent.position = Input.mousePosition;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        graphicRaycast.Raycast(pointerEvent, results);

        if (results.Count > 0) return true;
        return false;
    }

    public static List<RaycastResult> GetGraphicRaycastData()
    {
        //Set up the new Pointer Event
        pointerEvent = new PointerEventData(eventSystem);

        //Set the Pointer Event Position to that of the mouse position
        pointerEvent.position = Input.mousePosition;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        graphicRaycast.Raycast(pointerEvent, results);

        return results;
    }
}
