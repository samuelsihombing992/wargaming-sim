using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

public class ContentFitterForceUpdate : MonoBehaviour
{
    public List<ContentSizeFitter> content = new List<ContentSizeFitter>();

    private async void Start()
    {
        await Task.Delay(2000);

        Canvas.ForceUpdateCanvases();
        for (int i = 0; i < content.Count; i++)
        {
            content[i].enabled = false;
            content[i].enabled = true;
        }

        Destroy(this);
    }
}
