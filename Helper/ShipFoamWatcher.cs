using UnityEngine;

using Crest;
using Esri.ArcGISMapsSDK.Components;
using WGSBundling.Bundles.DynamicWave;

public class ShipFoamWatcher : MonoBehaviour
{
    //private GameObject waveParent;
    private NodeDynamicWave waveComponent;
    private ArcGISLocationComponent mainCamLocation;

    void Start()
    {
        waveComponent = GetComponent<NodeDynamicWave>();
        if (waveComponent == null) return;

        //if (GetComponent<ShipBundleData>() == null) return;
        //waveParent = GetComponent<ShipBundleData>().nodeWakeGenerator;

        //if (waveParent == null)
        //{
        //    IncorrectWaveStructure();
        //}

        mainCamLocation = Camera.main.GetComponentInParent<ArcGISLocationComponent>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (mainCamLocation == null) return;

        var state = true;
        if(mainCamLocation.Position.Z >= 500)
        {
            state = false;
            //waveParent.SetActive(false);
        }

        for(int i=0; i < waveComponent.nodes.Count; i++)
        {
            waveComponent.nodes[i].node.gameObject.SetActive(state);
        }
    }

    private void IncorrectWaveStructure()
    {
        foreach(SphereWaterInteraction child in GetComponentsInChildren<SphereWaterInteraction>())
        {
            child.enabled = false;
        }

        enabled = false;
    }
}
