using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using Esri.ArcGISMapsSDK.Components;
using Esri.GameEngine.Geometry;

using Wargaming.Components.Controller;
using Wargaming.Components.Playback;
using WGSBundling.Bundles.CargoHold;
using static Wargaming.Core.GlobalParam.GlobalHelper;

public class EmbarkWalker : MonoBehaviour
{
    public GameObject objParent;
    public GameObject objWalker;

    MissionWalker mission;
    NodeCargoHold cargoHold;

    public bool alreadyAssigned = false;

    public bool inProgress = false;
    public bool recheckDoors = false;

    private void Start()
    {
        mission = GetComponent<MissionWalker>();
        inProgress = false;

        cargoHold = objParent.GetComponent<NodeCargoHold>();
    }

    private void FixedUpdate()
    {
        if ((mission.DATA[0].datetime <= PlaybackController.CURRENT_TIME) && (PlaybackController.CURRENT_TIME <= mission.DATA[mission.DATA.Count - 1].datetime))
        {
            if (inProgress) return;
            GetDoorsToOpen();

        }else if(PlaybackController.CURRENT_TIME > mission.DATA[mission.DATA.Count - 1].datetime)
        {
            CloseAllDoors();
        }
    }

    private void GetDoorsToOpen()
    {
        inProgress = true;
        recheckDoors = true;

        // Ambil posisi node pertama dari misi debarkasi dengan titik keluar/masuk terdekat
        float _distance = -99;
        int selectedDoors = -99;
        for (int i = 0; i < cargoHold.doors.Count; i++)
        {
            var entryExitPoint = cargoHold.doors[i].paths[cargoHold.doors[i].paths.Count - 1];
            var newDistance = Vector3.Distance(entryExitPoint.transform.position, GetComponent<MissionWalker>().transform.GetChild(GetComponent<MissionWalker>().transform.childCount - 1).position);

            if (_distance == -99)
            {
                _distance = newDistance;
                selectedDoors = i;
                continue;
            }

            if (_distance > newDistance)
            {
                _distance = newDistance;
                selectedDoors = i;
            }
        }

        if (selectedDoors != -99)
        {
            var animator = cargoHold.doors[selectedDoors].obj.GetComponent<Animator>();
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("Normal"))
            {
                Debug.Log("Open Pintu untuk embarkasi" + selectedDoors);
                cargoHold.doors[selectedDoors].obj.GetComponent<Animator>().Play("RampDown");

                if (!alreadyAssigned)
                {
                    OnDoorOpenForEmbarkasi(cargoHold.doors[selectedDoors]);
                }
            }
        }
    }

    private void CloseAllDoors()
    {
        if (!recheckDoors) return;

        Debug.Log("Recheck All Doors");

        for (int i = 0; i < cargoHold.doors.Count; i++)
        {
            var animator = cargoHold.doors[i].obj.GetComponent<Animator>();
            animator.Play("Normal");
        }

        recheckDoors = false;
    }

    public async void OnDoorOpenForEmbarkasi(NodeCargoDoorData data)
    {
        alreadyAssigned = true;

        Debug.Log("On Door Open For Embarkasi");

        if (mission == null) return;

        List<MissionWalkerParam> DATA = new List<MissionWalkerParam>();

        for (int i = (data.paths.Count - 1); i >= 0; i--)
        {
            Debug.Log("INSTANCING");
            var tempRoutePoint = Instantiate(data.paths[i].gameObject, transform.parent);
            tempRoutePoint.transform.position = data.paths[i].transform.position;

            Debug.Log("Add Location");
            var tempRouteLocation = tempRoutePoint.AddComponent<ArcGISLocationComponent>();

            await Task.Delay(100);
            Debug.Log("Set Location");
            tempRouteLocation.Position = new ArcGISPoint(tempRouteLocation.Position.X, tempRouteLocation.Position.Y, tempRouteLocation.Position.Z, ArcGISSpatialReference.WGS84());

            Debug.Log("Add Mission Walker Parameter");
            var newPos = new MissionWalkerParam();
            newPos.time = DateTime.Now.ToString();
            newPos.datetime = DateTime.Now;
            newPos.coordinate = new Vector3((float)tempRouteLocation.Position.X, (float)tempRouteLocation.Position.Y, (float)tempRouteLocation.Position.Z);

            Debug.Log("Add to DATA LIST");
            DATA.Add(newPos);

            await Task.Delay(100);
            Debug.Log("Destroy Temp Route Point");
            Destroy(tempRoutePoint.gameObject);
        }

        for (int i = 0; i < mission.DATA.Count; i++)
        {
            DATA.Add(mission.DATA[i]);
        }

        var dateAtNode = mission.DATA[0].datetime;
        for(int i = (mission.DATA.Count); i < DATA.Count; i++)
        {
            double dx, dy;
            if (i == (DATA.Count - 1))
            {
                OnlineMapsUtils.DistanceBetweenPoints(mission.DATA[mission.DATA.Count - 1].coordinate.x, mission.DATA[mission.DATA.Count - 1].coordinate.y, DATA[i].coordinate.x, DATA[i].coordinate.y, out dx, out dy);
            }
            else
            {
                OnlineMapsUtils.DistanceBetweenPoints(DATA[i - 1].coordinate.x, DATA[i - 1].coordinate.y, DATA[i].coordinate.x, DATA[i].coordinate.y, out dx, out dy);
            }

            double stepDistance = Math.Sqrt(dx * dx + dy * dy);

            // Total step time
            double totalTime = stepDistance / mission.speed * 3600;
            dateAtNode = dateAtNode.AddSeconds(totalTime);

            DATA[i].datetime = dateAtNode;
            DATA[i].time = dateAtNode.ToString();
        }

        //await Task.Delay(1000);
        Debug.Log("Setup Temp Node New Node");
        List<ArcGISLocationComponent> TEMP_PATH_NODE = new List<ArcGISLocationComponent>();
        for(int i = (mission.DATA.Count); i < DATA.Count; i++)
        {
            Debug.Log("Instantiate New Node");

            var splineNode = Instantiate(MissionController.instance.prefabNode, transform);
            splineNode.transform.SetCoordinate(DATA[i].coordinate);
            splineNode.transform.SetOrientation(new Vector3(0, 0, -90));
            splineNode.transform.SetAsFirstSibling();

            TEMP_PATH_NODE.Add(splineNode.GetComponent<ArcGISLocationComponent>());
        }

        await Task.Delay(100);
        // Remove ArcGISLocationComponent from each node
        for (int i = 0; i < TEMP_PATH_NODE.Count; i++)
        {
            Destroy(TEMP_PATH_NODE[i]);
        }

        //await Task.Delay(1000);
        Debug.Log("Clean Old Mission");

        mission.DATA.Clear();

        //await Task.Delay(1000);
        Debug.Log("Assign New Mission");
        mission.DATA = DATA;

        EntityController.instance.EmbarkEntity(objWalker);

        //await Task.Delay(1000);
        //Debug.Log("Destroy all Child in mission Transform");
        //foreach (Transform node in mission.transform)
        //{
        //    Destroy(node.gameObject);
        //}
    }
}
