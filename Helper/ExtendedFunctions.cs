using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;

using TMPro;
using System.IO;

namespace Wargaming.Core
{
    public static class TMP_Extended
    {
        public static void TogglePassword(this TMP_InputField _input)
        {
            _input.inputType = (_input.inputType == TMP_InputField.InputType.Standard) ?
                TMP_InputField.InputType.Password : 
                TMP_InputField.InputType.Standard;

            string _data = _input.text;
            _input.text = "";
            _input.text = _data;
        }

        public static TMP_Dropdown.OptionData GetSelected(this TMP_Dropdown _input)
        {
            return _input.options[_input.value];
        }

        public static bool isEmpty(this TMP_InputField _input){
            return (_input.text == "") ? true : false;
        }

        public static void reset(this TMP_InputField _input, string _defaultText = null)
        {
            _input.text = (_defaultText != null) ? _defaultText : "";
        }

        public static void reset(this TMP_Dropdown _input)
        {
            _input.value = 0;
        }
    }

    public static class GameObjectExtended
    {
        public static bool isEmpty(this GameObject _obj)
        {
            return (_obj == null) ? true : false;
        }

        public static void PlayAnimation(this GameObject _obj, string _animation, float _speed = -1)
        {
            var _animator = _obj.GetComponent<Animator>();
            if (_animator == null) return;

            _animator.speed = (_speed == -1 || _speed == 0) ? _animator.speed : _speed;
            _animator.Play(_animation);
        }

        public static async void WaitAndPlay(this GameObject _obj, string _animation, int _waitTime, float _speed = -1)
        {
            await Task.Delay(_waitTime);

            _obj.PlayAnimation(_animation, _speed);
        }

        public static Metadata GetMetadata(this GameObject _obj)
        {
            return _obj.GetComponent<Metadata>();
        }
    }

    public static class TransformExtended
    {
        public static void DestroyAllChild(this Transform _obj)
        {
            foreach (Transform item in _obj.transform)
            {
                Object.Destroy(item.gameObject);
            }
        }

        public static int GetIndex(this Transform _obj)
        {
            for(int i=0; i < _obj.childCount; i++)
            {
                if(_obj.GetChild(i) == _obj)
                {
                    return i;
                }
            }

            return -1;
        }

        public static void SetX(this Transform _obj, float x)
        {
            _obj.transform.position = new Vector3(x, _obj.position.y, _obj.position.z);
        }

        public static void SetY(this Transform _obj, float y)
        {
            _obj.transform.position = new Vector3(_obj.position.x, y, _obj.position.z);
        }

        public static void SetZ(this Transform _obj, float z)
        {
            _obj.transform.position = new Vector3(_obj.position.x, _obj.position.y, z);
        }

        public static void SetEulerX(this Transform _obj, float x)
        {
            _obj.transform.rotation = Quaternion.Euler(x, _obj.rotation.y, _obj.rotation.z);
        }

        public static void SetEulerY(this Transform _obj, float y)
        {
            _obj.transform.rotation = Quaternion.Euler(_obj.rotation.x, y, _obj.rotation.z);
        }

        public static void SetEulerZ(this Transform _obj, float z)
        {
            _obj.transform.rotation = Quaternion.Euler(_obj.rotation.x, _obj.rotation.y, z);
        }
    }

    public static class MetadataExtended
    {
        public static TMP_InputField GetMetadataInput(this Metadata _metadata, string _paramName)
        {
            var _data = _metadata.FindParameter(_paramName);
            if (_data == null) return null;

            return _data.GetComponent<TMP_InputField>();
        }

        public static TextMeshProUGUI GetMetadataLabel(this Metadata _metadata, string _paramName)
        {
            var _data = _metadata.FindParameter(_paramName);
            if (_data == null) return null;

            return _data.GetComponent<TextMeshProUGUI>();
        }

        public static Button GetMetadataButton(this Metadata _metadata, string _paramName)
        {
            var _data = _metadata.FindParameter(_paramName);
            if (_data == null) return null;

            return _data.GetComponent<Button>();
        }

        public static Toggle GetMetadataToggle(this Metadata _metadata, string _paramName)
        {
            var _data = _metadata.FindParameter(_paramName);
            if (_data == null) return null;

            return _data.GetComponent<Toggle>();
        }

        public static MenuToggler GetMetadataMenuToggler(this Metadata _metadata, string _paramName)
        {
            var _data = _metadata.FindParameter(_paramName);
            if (_data == null) return null;

            return _data.GetComponent<MenuToggler>();
        }
    }

    public static class IOExtended
    {
        public static void LoadOrCreateDirectory(string _path)
        {
            if (!Directory.Exists(_path)) Directory.CreateDirectory(_path);
        }
    }
}