using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Animator))]
public class AnimationState : MonoBehaviour
{
    public UnityEvent onAnimationStart;
    public UnityEvent onAnimationFinish;

    public void RunWhenAnimationEnd()
    {
        onAnimationFinish.Invoke();
    }

    public void RunWhenAnimationStart()
    {
        onAnimationStart.Invoke();
    }
}