using UnityEngine;
using UnityEngine.UI;

namespace Wargaming.Core.ScenarioEditor.ColorPalette
{
    public class CustomColorPalette : MonoBehaviour
    {
        [Header("References")]
        public Image viewer;

        public Color color;
        public float opacity;

        public void Setup(Color _color, float _opacity)
        {
            color = _color;
            color.a = _opacity;

            opacity = _opacity;

            viewer.color = color;
        }

        public void Show()
        {
            GetComponent<Animator>().Play("FadeIn");
        }

        public void Hide()
        {
            GetComponent<Animator>().Play("FadeOut");
        }
    }
}