using System;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

using Wargaming.Core;
using Wargaming.Core.GlobalParam;

public class StartupConfig : MonoBehaviour
{
    [Header("Settings")]
    public string configFile;

    [Header("Directories Manager")]
    [SerializeField] private string dataDir             = "Data";
    [SerializeField] private string themeDir            = @"Data\Theme";
    [SerializeField] private string assetPackageDir     = "AssetPackage";
    [SerializeField] private string datapackDir         = "Datapack";
    [SerializeField] private string tacmapRecord        = @"Tacmap\Record";
    [SerializeField] private string sceneryDir          = "Scenery";

    public static StartupConfigData settings;
    private static string _configFile, _dataDir, _themeDir, _assetPackageDir, _dataPackDir, _sceneryDir, _tacmapRecord;

    #region AWAKE
    private void Awake()
    {
        _configFile         = configFile;

        _dataDir            = Path.Combine(Directory.GetCurrentDirectory(), dataDir);
        _themeDir           = Path.Combine(Directory.GetCurrentDirectory(), themeDir);
        _assetPackageDir    = Path.Combine(Directory.GetCurrentDirectory(), assetPackageDir);
        _sceneryDir         = Path.Combine(Directory.GetCurrentDirectory(), sceneryDir);
        _dataPackDir        = Path.Combine(Directory.GetCurrentDirectory(), datapackDir);
        _tacmapRecord       = Path.Combine(Directory.GetCurrentDirectory(), tacmapRecord);

        // Load or Create Data Directory
        dataDir = GetDataDirectory();
        IOExtended.LoadOrCreateDirectory(dataDir);

        // Load or Create Theme Directory
        themeDir = GetThemeDirectory();
        IOExtended.LoadOrCreateDirectory(themeDir);

        // Load or Create AssetPackage Direcotry
        assetPackageDir = GetAssetPackageDirectory();
        IOExtended.LoadOrCreateDirectory(assetPackageDir);

        // Load or Create Scenery Directory
        sceneryDir = GetSceneryDirectory();
        IOExtended.LoadOrCreateDirectory(sceneryDir);

        // Load or Create Datapack Directory
        tacmapRecord = GetDatapackDirectory();
        IOExtended.LoadOrCreateDirectory(datapackDir);

        // Load or Create Tacmap Record Directory
        datapackDir = GetTacmapRecordDirectory();
        IOExtended.LoadOrCreateDirectory(datapackDir);

        LoadAppConfigFromFile();
    }
    #endregion

    #region GET DIRECTORY FUNCTIONS
    public static string GetDataDirectory() { return _dataDir; }
    public static string GetThemeDirectory() { return _themeDir; }
    public static string GetAssetPackageDirectory() { return _assetPackageDir; }
    public static string GetSceneryDirectory() { return _sceneryDir; }
    public static string GetDatapackDirectory() { return _dataPackDir; }
    public static string GetTacmapRecordDirectory() { return _tacmapRecord; }
    #endregion

    

    private void LoadAppConfigFromFile()
    {
        // If Startup Config File is not defined, throw error message
        if (configFile == null)
        {
            Debug.LogError("Error. Config File is not defined. Please provide one!");
            return;
        }

        var app_config_path = Path.Combine(Directory.GetCurrentDirectory(), configFile);
        if (File.Exists(app_config_path))
        {
            // Use Current Startup Config File if exists
            var _XMLSerialize = new XmlSerializer(typeof(StartupConfigData));
            using (var reader = new StreamReader(app_config_path))
            {
                settings = (StartupConfigData)_XMLSerialize.Deserialize(reader);
            }
        }
        else
        {
            StartupConfigData newConfig = new StartupConfigData();

            newConfig.DEBUG_VERSION = true;
            newConfig.TIME_DIFF = 0;

            newConfig.DEFAULT_OCEAN_HEIGHT = 2;
            newConfig.OCEAN_MAX_ALTITUDE = 150000;
            newConfig.FOAM_MAX_ALTITUDE = 100;
            newConfig.DEFAULT_CLOUDS_HEIGHT = 7000;
            newConfig.CLOUDS_MAX_ALTITUDE = 35000;
            newConfig.CLOUDS_QUALITY = 2;
            newConfig.CLOUDS_REPROJECTION_QUALITY = 4;
            newConfig.REBASE_MAX_POSITION = 100000;
            newConfig.REBASE_MIN_ALTITUDE = 15000;

            newConfig.DEFAULT_AIRCRAFT_SCALE = 1;
            newConfig.DEFAULT_SHIP_SCALE = 1;
            newConfig.DEFAULT_VEHICLE_SCALE = 1;

            newConfig.DEFAULT_AIRCRAFT_ALTITUDE = 1000;
            newConfig.MAX_AIRCRAFT_ALTITUDE = 10000;

            newConfig.AIRCRAFT_DEF_TAKEOFF_LANDING  = false;
            newConfig.HELI_DEF_TAKEOFF_LANDING      = false;

            newConfig.ENABLE_LOAD_SATUAN    = true;
            newConfig.ENABLE_LOAD_PASUKAN   = true;
            newConfig.ENABLE_LOAD_DRAWING   = true;
            newConfig.ENABLE_LOAD_RADAR     = false;
            newConfig.ENABLE_LOAD_KEKUATAN  = false;
            newConfig.ENABLE_LOAD_SITUASI   = false;
            newConfig.ENABLE_LOAD_BUNGUS    = false;
            newConfig.ENABLE_LOAD_ANIMATION = false;

            newConfig.ENABLE_ASSET_PACKAGE  = true;
            newConfig.ENABLE_DATAPACK       = true;

            newConfig.ENABLE_BASEMAPS       = true;
            newConfig.ENABLE_LAYERS         = true;

            newConfig.SERVER_TFG = "tfg";
            newConfig.SERVER_TFG_SINGLE = "tfg-single";
            newConfig.SERVER_WGS = "wgs";

            SaveStartupConfig(newConfig);
        }

        try
        {
            AppVersion.GetBuildData();
        }
        catch (Exception e) { }
    }

    public void SaveStartupConfig(StartupConfigData _config)
    {
        // Serialize and Write Changed into StartupConfig File
        var _XMLSerialize = new XmlSerializer(typeof(StartupConfigData));
        using (var writer = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), configFile)))
        {
            _XMLSerialize.Serialize(writer, _config);
        }

        settings = _config;
    }

    public static void SaveStartupConfig()
    {
        // Serialize and Write Changed into StartupConfig File
        var _XMLSerialize = new XmlSerializer(typeof(StartupConfigData));
        using (var writer = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), _configFile)))
        {
            _XMLSerialize.Serialize(writer, settings);
        }
    }
}