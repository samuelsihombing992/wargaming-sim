using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wargaming.Components.Playback;
using WGSBundling.Bundles.CargoHold;

public class CargoRampWatcher : MonoBehaviour
{
    public List<RootMissionWalker> usedByMission = new List<RootMissionWalker>();
    private NodeCargoHold node;
    private RootWalkerBase walker;

    private void Start()
    {
        node    = GetComponent<NodeCargoHold>();
        walker  = GetComponent<RootWalkerBase>();
    }

    private void Update()
    {
        if (node == null || node.doors == null || node.doors.Count <= 0) return;

        if (node.doors[0].state && walker.moving && usedByMission.Count <= 0)
        {
            node.doors[0].obj.GetComponent<Animator>().Play("Normal", 0, 0);
            node.doors[0].obj.GetComponent<Animator>().speed = Mathf.Clamp(PlaybackController.instance.speedFactor / 2, 1, 10);

            node.doors[0].state = false;
        }

        if (node.doors[0].state == (usedByMission.Count > 0)) return;

        node.doors[0].state = (usedByMission.Count > 0);
        if (node.doors[0].state)
        {
            node.doors[0].obj.GetComponent<Animator>().Play("RampDown");
            node.doors[0].obj.GetComponent<Animator>().speed = Mathf.Clamp(PlaybackController.instance.speedFactor / 2, 1, 10);
        }
        else
        {
            node.doors[0].obj.GetComponent<Animator>().Play("RampUp");
            node.doors[0].obj.GetComponent<Animator>().speed = Mathf.Clamp(PlaybackController.instance.speedFactor / 2, 1, 10);
        }
    }
}
