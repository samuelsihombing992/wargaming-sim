using UnityEngine;
using Unity.Mathematics;

using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;
using Esri.HPFramework;

using WGSBundling.Core;
public class FollowTerrainV2 : MonoBehaviour
{
    public float elevationOffset = 0f;

    WGSBundleCore bundlingCore;

    private ArcGISMapComponent arcGISMapComponent;
    private ArcGISLocationComponent locationComponent;

    private void Start()
    {
        arcGISMapComponent  = FindObjectOfType<ArcGISMapComponent>();
        locationComponent   = GetComponent<ArcGISLocationComponent>();
        bundlingCore        = GetComponent<WGSBundleCore>();
    }
    private void FixedUpdate()
    {
        SetElevation();
    }

    private void SetElevation()
    {// start the raycast in the air at an arbitrary to ensure it is above the ground
        var raycastHeight = 8000;

        var position = gameObject.transform.position;
        var raycastStart = new Vector3(position.x, position.y + raycastHeight, position.z);
        if (Physics.Raycast(raycastStart, Vector3.down, out RaycastHit hitInfo))
        {
            if (hitInfo.transform.tag == "entity")
            {
                locationComponent.Position = HitToGeoPosition(hitInfo, elevationOffset);

            }
        }
    }

    /// <summary>
    /// Return GeoPosition Based on RaycastHit; I.E. Where the user clicked in the Scene.
    /// </summary>
    /// <param name="hit">Raycasted Object (Terrain)</param>
    /// <param name="offset">Altitude + Offset</param>
    /// <returns></returns>
    private ArcGISPoint HitToGeoPosition(RaycastHit hit, float yOffset = 0)
    {
        var worldPosition = math.inverse(arcGISMapComponent.WorldMatrix).HomogeneousTransformPoint(hit.point.ToDouble3());

        var geoPosition = arcGISMapComponent.View.WorldToGeographic(worldPosition);

        var altitude = geoPosition.Z;
        if (bundlingCore.amphibious)
        {
            if (altitude < StartupConfig.settings.DEFAULT_OCEAN_HEIGHT - 1.5f) altitude = StartupConfig.settings.DEFAULT_OCEAN_HEIGHT.GetValueOrDefault(3) - 1.5f;
        }

        var offsetPosition = new ArcGISPoint(geoPosition.X, geoPosition.Y, altitude + yOffset, geoPosition.SpatialReference);
        return GeoUtils.ProjectToSpatialReference(offsetPosition, ArcGISSpatialReference.WGS84());
    }
}
