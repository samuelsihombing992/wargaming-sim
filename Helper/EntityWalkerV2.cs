using System;
using System.Collections.Generic;

using UnityEngine;

using Wargaming.Components.Playback;

using WGSBundling.Core;
using WGSBundling.Bundles.AircraftEngine;
using WGSBundling.Bundles.Animated;
using WGSBundling.Bundles.CargoHold;
using WGSBundling.Bundles.DynamicWave;
using WGSBundling.Bundles.Helipad;
using WGSBundling.Bundles.HeliRotor;
using WGSBundling.Bundles.TrackedVehicle;
using WGSBundling.Bundles.WheeledVehicle;

using Esri.ArcGISMapsSDK.Components;

using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;

using SickscoreGames.HUDNavigationSystem;
using Wargaming.Components.Colyseus;
using Wargaming.Core.GlobalParam;
using Wargaming.Core;
using FIMSpace.GroundFitter;
using WGSBundling.Bundles.VFX;
using Esri.HPFramework;
using WGSBundling.Bundles.Sound;
using Esri.GameEngine.Geometry;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Unity.Mathematics;

public class EntityWalkerV2 : MonoBehaviour
{
    public RootWalker root;

    [Header("Data Misi")]
    public List<MissionWalker> missions = new List<MissionWalker>();

    private SplineController splineController;

    [Header("Data Satuan")]
    private ObjekSatuan dataSatuan;
    private PData dataPlayer;
    private WGSBundleCore bundlingCore;
    private NodeAircraftEngine bundlingAircraftEngine;
    private NodeAnimated bundlingAnimated;
    private NodeCargoHold bundlingCargoHold;
    private NodeDynamicWave bundlingDynamicWave;
    private NodeHelipad bundlingHelipad;
    private NodeHeliRotor bundlingHeliRotor;
    private NodeWheeledVehicle bundlingWheel;
    private NodeVFX bundlingVFX;
    private NodeSound bundlingSound;
    //private FollowTerrain terrainFollower;
    private FGroundFitter terrainFollower;

    private ArcGISLocationComponent locationComponent;
    private ArcGISMapComponent arcGISMapComponent;

    private ArcGISPoint oldLocation;
    private ArcGISRotation oldRotation;
    private HPTransform hpTransformComponent;

    public bool showSpeed = false;
    public AnimationCurve speedInterpolation = AnimationCurve.EaseInOut(0,1, 1,0);

    [Header("Movement State")]
    public bool isMoving        = false;
    public bool isOnEmbark      = false;
    public bool doorNeedToClose = false;

    public int CURRENT_MISSION  = 0;
    public int CURRENT_NODE     = 0;

    public double progress      = 0;

    [Header("Rotation State")]
    public bool syncRoll = false;

    public float angle;
    private float oldOrientation;

    [Header("Guidance State")]
    public SplineController guidance;
    public SplineController takeoffGuidance;
    public SplineController landingGuidance;
    public float guidanceDistance;
    public float sensitivity = 1;

    [Header("Altitude State")]
    public bool syncAltitude = true;
    public float currentAltitude = 0;

    public AnimationCurve altitudeMotion    = AnimationCurve.EaseInOut(0, 5, 1, 5);
    public AnimationCurve takeoffPitch      = AnimationCurve.EaseInOut(0, 0, 1, 0);
    public AnimationCurve landingPitch      = AnimationCurve.EaseInOut(0, 0, 1, 0);
    public AnimationCurve helipadTakeOff    = new AnimationCurve(EntityController.instance.helipadTakeoffStep.keys);
    public AnimationCurve helipadLanding    = new AnimationCurve(EntityController.instance.helipadLandingStep.keys);

    [Header("Tactical Map Marker")]
    public OnlineMapsMarker3D tacmapMarker;

    [Header("Audio")]
    public AudioController audioController;

    public float takeoffLandingLength;
    public float length;
    public float wheelSpeed;
    public float smoothness = 1;
    //public float multiplier = 2;
    public float multiplier = 12;
    //public float wheelAngle = 0;
    //public float wheelAngleMultiplier = 1;
    //public float wheelRotationMultiplier = 1;

    //[SerializeField] public float syncTime = 1.0f; // Sync Waktu dari Colyseus tiap X-Time

    //[SerializeField] public float syncPeriod = 0.0f;
    //[SerializeField] public bool resyncTime = false;

    public double distanceDifferent = 0;
    public Vector3 oldPos;
    public double3 lastPosition;

    private float maxAltitude = 0;

    // Initialize EntityWalker (Version 2)
    public void init()
    {
        // EntityWalker need Playback to be working
        if (!PlaybackController.instance) Destroy(this);

        dataSatuan                  = GetComponent<ObjekSatuan>();
        dataPlayer                  = PlayerData.PLAYERS.Find(x => x.id == dataSatuan.userID);
        bundlingCore                = GetComponent<WGSBundleCore>();
        bundlingAircraftEngine      = GetComponent<NodeAircraftEngine>();
        bundlingAnimated            = GetComponent<NodeAnimated>();
        bundlingCargoHold           = GetComponent<NodeCargoHold>();
        bundlingDynamicWave         = GetComponent<NodeDynamicWave>();
        bundlingHelipad             = GetComponent<NodeHelipad>();
        bundlingHeliRotor           = GetComponent<NodeHeliRotor>();
        bundlingWheel               = GetComponent<NodeWheeledVehicle>();
        bundlingVFX                 = GetComponent<NodeVFX>();
        bundlingSound               = GetComponent<NodeSound>();
        locationComponent           = GetComponent<ArcGISLocationComponent>();
        arcGISMapComponent          = FindObjectOfType<ArcGISMapComponent>();
        hpTransformComponent        = GetComponent<HPTransform>();

        oldLocation                 = locationComponent.Position;
        //terrainFollower             = GetComponent<FollowTerrain>();
        terrainFollower             = GetComponent<FGroundFitter>();

        isMoving = false;
        isOnEmbark = false;

        CURRENT_NODE = 0;
        CURRENT_MISSION = 0;
        progress = 0;

        splineController    = GetComponent<SplineController>();
        audioController     = GetComponent<AudioController>();

        length = missions[0].GetComponent<CurvySpline>().Length;
    }

    private void Update()
    {
        if (splineController == null) return;
        if (missions == null) return;
        if (missions.Count <= 0) return;
        if (WargamingCam.instance.onRebase) return;

        CheckInvalidAltitude();

        var PLAYBACK_TIME = PlaybackController.CURRENT_TIME;
        if (ColyseusTFGSingleController.instance != null && dataSatuan.userID != null)
        {
            if (!dataPlayer.timeMovement.HasValue) return;
            PLAYBACK_TIME = dataPlayer.timeMovement.GetValueOrDefault();
            //if (!dataSatuan.authorData.timeMovement.HasValue) return;
            //PLAYBACK_TIME = dataSatuan.authorData.timeMovement.GetValueOrDefault();
        }
        else
        {
            // Walker Dalam Mode Default
            if (!PlaybackController.IsPlaying()) return;
        }

        for(int i = missions.Count - 1; i >= 0; i--)
        {
            if(PLAYBACK_TIME >= missions[i].DATA[0].datetime)
            {
                CURRENT_MISSION = i;
                i = 0;
                continue;
            }

            CURRENT_MISSION = 0;
        }

        var selectedMission     = missions[CURRENT_MISSION];
        var missionNodes        = selectedMission.DATA;

        bool inTime = false;
        if(PLAYBACK_TIME < missionNodes[0].datetime)
        {
            CURRENT_NODE = 0;
            progress = 0;
        }else if(PLAYBACK_TIME > missionNodes[missionNodes.Count - 1].datetime)
        {
            CURRENT_NODE = missionNodes.Count - 1;
            progress = 1;
        }
        else
        {
            for (int i = 0; i < missions[CURRENT_MISSION].DATA.Count - 2; i++)
            {
                var nodeTime        = selectedMission.DATA[i].datetime;
                var nextNodeTime    = selectedMission.DATA[i + 1].datetime;

                if (PLAYBACK_TIME >= nodeTime && PLAYBACK_TIME < nextNodeTime)
                {
                    // IF Waktu Playback sedang berada di antara 2 titik node
                    CURRENT_NODE = i;
                    i = missions[CURRENT_MISSION].DATA.Count - 1;
                }
            }

            inTime = true;
        }

        if (splineController.Spline != selectedMission.GetComponent<CurvySpline>())
        {
            splineController.Spline = selectedMission.GetComponent<CurvySpline>();
            length = splineController.Spline.Length;

            takeoffLandingLength = (length * 20) / 100;
            if (takeoffLandingLength > 40000) takeoffLandingLength = 40000;

            GenerateAircraftMotion(selectedMission);
            GenerateHelicopterMotion(selectedMission);
        }

        length = splineController.Spline.Length;

        takeoffLandingLength = (length * 20) / 100;
        if (takeoffLandingLength > 40000) takeoffLandingLength = 40000;

        // Motion takeoff dan landing akan mengambil 20% dari panjang rute aktif (40% jalur takeoff dan landing, 60% jalur flight)
        // Maximal Takeoff / Landing Length = 40000

        if (!inTime && progress == 0)
        {
            splineController.RelativePosition = 0;
            isMoving = false;

            if(selectedMission.jenis == MissionWalker.RouteType.Embarkasi)
            {
                isOnEmbark = false;
            }
            else if (selectedMission.jenis == MissionWalker.RouteType.Debarkasi)
            {
                isOnEmbark = true;
            }

            if ((PLAYBACK_TIME >= missions[CURRENT_MISSION].DATA[0].datetime.AddSeconds(-10)))
            {
                if (isOnEmbark) RunDebarkasiStep();
                if (doorNeedToClose) RunEmbarkasiStep();
                doorNeedToClose = false;
            }

            //if (takeoffGuidance != null) Destroy(takeoffGuidance.gameObject);
            if (landingGuidance != null) Destroy(landingGuidance.gameObject);
        }
        else if(!inTime && progress == 1)
        {
            splineController.RelativePosition = 1;
            isMoving = false;

            if (selectedMission.jenis == MissionWalker.RouteType.Embarkasi)
            {
                isOnEmbark = true;

                if (missions[CURRENT_MISSION].insideEntity != null)
                {
                    missions[CURRENT_MISSION].insideEntity.GetComponent<EntityWalkerV2>().doorNeedToClose = true;
                }

            }
            else if (selectedMission.jenis == MissionWalker.RouteType.Debarkasi)
            {
                isOnEmbark = false;
            }else if(selectedMission.jenis == MissionWalker.RouteType.Pergerakan)
            {
                if ((CURRENT_MISSION + 1) < missions.Count && doorNeedToClose)
                {
                    if ((PLAYBACK_TIME >= missions[CURRENT_MISSION + 1].DATA[0].datetime.AddSeconds(-10)))
                    {
                        RunEmbarkasiStep();
                    }
                }
            }

            if (takeoffGuidance != null) Destroy(takeoffGuidance.gameObject);
            //if (landingGuidance != null) Destroy(landingGuidance.gameObject);
        }
        else
        {
            isMoving = true;
            isOnEmbark = false;

            if (!splineController.enabled)
            {
                splineController.enabled = true;
                hpTransformComponent.enabled = true;
                locationComponent.enabled = true;
            }
            if (transform.parent != EnvironmentsController.GetMapComponent().transform) transform.parent = EnvironmentsController.GetMapComponent().transform;

            var startDate = selectedMission.DATA[0].datetime;
            var endDate = selectedMission.DATA[selectedMission.DATA.Count - 1].datetime;
            var totalTime = (endDate - startDate).TotalSeconds;

            progress = ((PLAYBACK_TIME.Subtract(startDate).TotalSeconds) / totalTime);
            splineController.AbsolutePosition = (float)(splineController.Spline.Length * (progress * 100) / 100);

            if (selectedMission.jenis == MissionWalker.RouteType.Embarkasi) RunDebarkasiStep();

            if (doorNeedToClose) CheckDoors(true);

            if (guidance != null && syncRoll)
            {
                if (guidance.Spline != splineController.Spline) guidance.Spline = splineController.Spline;

                guidanceDistance    = (selectedMission.speed < 100) ? 100 : (float)selectedMission.speed;
                sensitivity         = int.Parse(guidanceDistance.ToString().Substring(0, 1)) / 2;
            }
        }
    }

    private void LateUpdate()
    {
        if (WargamingCam.instance.onRebase || dataSatuan == null || splineController == null || splineController.Spline == null) return;

        SyncRoll();

        SyncAircraftTerrainFollower();
        SyncHeliTerrainFollower();

        SyncEntityToggleState();
        //SyncTacmapIcon();
        SyncVFX();
        SyncSound();
        SyncWheel();
        SyncGetAltitude();
        SyncAltitude();
    }

    private void CheckInvalidAltitude()
    {
        if(locationComponent.Position.Z < -250)
        {
            locationComponent.Position = new ArcGISPoint(locationComponent.Position.X, locationComponent.Position.Y, 0, locationComponent.Position.SpatialReference);
        }
    }

    private void GenerateAircraftMotion(MissionWalker selectedMission)
    {
        if (dataSatuan.kategori != EntityHelper.KategoriSatuan.AIRCRAFT) return;

        // Motion takeoff dan landing akan mengambil 20% dari panjang rute aktif (40% jalur takeoff dan landing, 60% jalur flight)
        // Maximal Takeoff / Landing Length = 40000
        //takeoffLandingLength = (length * 20) / 100;
        //if (takeoffLandingLength > 40000) takeoffLandingLength = 40000;

        // Maximum Ketinggian Pesawat (10000m)
        var altitude = (0.2f * takeoffLandingLength);

        var maxAltitude = StartupConfig.settings.MAX_AIRCRAFT_ALTITUDE.GetValueOrDefault(10000);
        if (altitude > maxAltitude) altitude = maxAltitude;

        //altitudeMotion.AddKey(2512, 5);   
        List<Keyframe> altitudeFrame = new List<Keyframe>();
        altitudeFrame.Add(new Keyframe { time = 0, value = (selectedMission.takeoffStart) ? 0 : altitude });
        altitudeFrame.Add(new Keyframe { time = 2312, value = (selectedMission.takeoffStart) ? 0 : altitude });

        // 0,2 PER 1 length
        //altitudeMotion.AddKey(takeoffLandingLength, (0.2f * takeoffLandingLength));
        altitudeFrame.Add(new Keyframe { time = takeoffLandingLength, value = altitude });
        altitudeFrame.Add(new Keyframe { time = length - takeoffLandingLength, value = altitude });
        altitudeFrame.Add(new Keyframe { time = length - 2312, value = (selectedMission.landingEnd) ? 0 + 5 : altitude });
        altitudeFrame.Add(new Keyframe { time = length - 1812, value = (selectedMission.landingEnd) ? 0 : altitude });
        altitudeFrame.Add(new Keyframe { time = length, value = (selectedMission.landingEnd) ? 0 : altitude });

        altitudeMotion = new AnimationCurve(EntityController.instance.aircraftFlightMotion.keys);

        //altitudeMotion.MoveKey(1, altitudeFrame[altitudeFrame.Count - 1]);
        //altitudeMotion.MoveKey(0, altitudeFrame[0]);

        //Debug.Log("-- Altitude Frame --");
        for (int hh = 0; hh < altitudeFrame.Count; hh++)
        {
            //altitudeMotion.RemoveKey(hh);
            //Debug.Log(altitudeFrame[hh].time + " | " + altitudeFrame[hh].value);

            altitudeMotion.AddKey(altitudeFrame[hh]);
        }

        altitudeMotion.RemoveKey(0);
        altitudeMotion.RemoveKey(1);
        altitudeMotion.RemoveKey(2);

        if (!selectedMission.takeoffStart)
        {
            altitudeMotion.MoveKey(0, altitudeFrame[0]);
            altitudeMotion.RemoveKey(1);
            altitudeMotion.RemoveKey(1);
            //altitudeMotion.MoveKey(0, altitudeFrame[0]);
        }

        // Motion Tilting ketika Takeoff
        if (selectedMission.takeoffStart)
        {
            List<Keyframe> takeOffFrame = new List<Keyframe>();
            takeOffFrame.Add(new Keyframe { time = 0, value = 0 });
            takeOffFrame.Add(new Keyframe { time = 2012, value = 0 });
            takeOffFrame.Add(new Keyframe { time = 2512, value = 15 });
            takeOffFrame.Add(new Keyframe { time = takeoffLandingLength - 2000, value = 15 });
            takeOffFrame.Add(new Keyframe { time = takeoffLandingLength, value = 0 });

            takeoffPitch = new AnimationCurve(EntityController.instance.aircraftTakeoffMotion.keys);

            //takeoffPitch.MoveKey(1, takeOffFrame[takeOffFrame.Count - 1]);
            //takeoffPitch.MoveKey(0, takeOffFrame[0]);

            //Debug.Log("-- Takeoff Frame --");
            for (int hh = 0; hh < takeOffFrame.Count; hh++)
            {
                //altitudeMotion.RemoveKey(hh);
                //Debug.Log(takeOffFrame[hh].time + " | " + takeOffFrame[hh].value);

                takeoffPitch.AddKey(takeOffFrame[hh]);
            }
        }

        // Motion Tilting ketika Landing
        if (selectedMission.landingEnd)
        {
            List<Keyframe> landingFrame = new List<Keyframe>();
            landingFrame.Add(new Keyframe { time = length - takeoffLandingLength, value = 0 });
            landingFrame.Add(new Keyframe { time = length - (takeoffLandingLength - 2000), value = -10 });
            landingFrame.Add(new Keyframe { time = length - 4512, value = -10 });
            landingFrame.Add(new Keyframe { time = length - 3512, value = 0 });
            landingFrame.Add(new Keyframe { time = length - 3012, value = 0 });
            landingFrame.Add(new Keyframe { time = length - 2012, value = 8 });
            landingFrame.Add(new Keyframe { time = length - 1512, value = 0 });
            landingFrame.Add(new Keyframe { time = length, value = 0 });

            landingPitch = new AnimationCurve(EntityController.instance.aircraftLandingMotion.keys);

            //Debug.Log("-- Landing Frame --");
            for (int hh = 0; hh < landingFrame.Count; hh++)
            {
                //altitudeMotion.RemoveKey(hh);
                //Debug.Log(landingFrame[hh].time + " | " + landingFrame[hh].value);

                landingPitch.AddKey(landingFrame[hh]);
            }
        }
    }

    private void GenerateHelicopterMotion(MissionWalker selectedMission)
    {
        if (dataSatuan.kategori != EntityHelper.KategoriSatuan.HELICOPTER) return;

        //// Motion takeoff dan landing akan mengambil 20% dari panjang rute aktif (40% jalur takeoff dan landing, 60% jalur flight)
        //// Maximal Takeoff / Landing Length = 40000
        //takeoffLandingLength = (length * 20) / 100;
        //if (takeoffLandingLength > 40000) takeoffLandingLength = 40000;

        // Maximum Ketinggian Pesawat (10000m)
        var altitude = (0.05f * takeoffLandingLength);

        //var maxAltitude = StartupConfig.settings.MAX_AIRCRAFT_ALTITUDE.GetValueOrDefault(10000);
        var maxAltitude = 3048;
        if (altitude > maxAltitude) altitude = maxAltitude;

        List<Keyframe> altitudeFrame = new List<Keyframe>();
        altitudeFrame.Add(new Keyframe { time = 0, value = (selectedMission.takeoffStart) ? 0 : altitude });
        altitudeFrame.Add(new Keyframe { time = Mathf.Clamp(takeoffLandingLength, 0, 5000), value = altitude });
        altitudeFrame.Add(new Keyframe { time = length - Mathf.Clamp(takeoffLandingLength, 0, 5000), value = altitude });
        altitudeFrame.Add(new Keyframe { time = length, value = (selectedMission.landingEnd) ? 0 : altitude });

        if(missions[CURRENT_MISSION].END_AT_PARENT != null)
        {
            helipadLanding = new AnimationCurve(EntityController.instance.helipadLandingStep.keys);
        }

        if(missions[CURRENT_MISSION].START_AT_PARENT != null)
        {
            helipadTakeOff = new AnimationCurve(EntityController.instance.helipadTakeoffStep.keys);
        }

        altitudeMotion = new AnimationCurve(EntityController.instance.heliFlightMotion.keys);

        for (int hh = 0; hh < altitudeFrame.Count; hh++)
        {
            //altitudeMotion.RemoveKey(hh);
            //Debug.Log(altitudeFrame[hh].time + " | " + altitudeFrame[hh].value);

            altitudeMotion.AddKey(altitudeFrame[hh]);
        }

        //altitudeMotion.RemoveKey(0);
        //altitudeMotion.RemoveKey(1);
        //altitudeMotion.RemoveKey(2);

        //if (!selectedMission.takeoffStart)
        //{
        //    altitudeMotion.MoveKey(0, altitudeFrame[0]);
        //    altitudeMotion.RemoveKey(1);
        //    altitudeMotion.RemoveKey(1);
        //    //altitudeMotion.MoveKey(0, altitudeFrame[0]);
        //}

        // Motion Tilting ketika Takeoff
        if (selectedMission.takeoffStart)
        {
            List<Keyframe> takeOffFrame = new List<Keyframe>();
            takeOffFrame.Add(new Keyframe { time = 0, value = 0 });
            takeOffFrame.Add(new Keyframe { time = 512, value = -20 });
            takeOffFrame.Add(new Keyframe { time = Mathf.Clamp(takeoffLandingLength, 0, 5000) - 512, value = -10 });
            takeOffFrame.Add(new Keyframe { time = Mathf.Clamp(takeoffLandingLength, 0, 5000), value = 0 });

            takeoffPitch = new AnimationCurve(EntityController.instance.heliTakeoffMotion.keys);

            //takeoffPitch.MoveKey(1, takeOffFrame[takeOffFrame.Count - 1]);
            //takeoffPitch.MoveKey(0, takeOffFrame[0]);

            //Debug.Log("-- Takeoff Frame --");
            for (int hh = 0; hh < takeOffFrame.Count; hh++)
            {
                //altitudeMotion.RemoveKey(hh);
                //Debug.Log(takeOffFrame[hh].time + " | " + takeOffFrame[hh].value);

                takeoffPitch.AddKey(takeOffFrame[hh]);
            }
        }

        // Motion Tilting ketika Landing
        if (selectedMission.landingEnd)
        {
            List<Keyframe> landingFrame = new List<Keyframe>();
            landingFrame.Add(new Keyframe { time = length - Mathf.Clamp(takeoffLandingLength, 0, 5000), value = 0 });
            landingFrame.Add(new Keyframe { time = length - (Mathf.Clamp(takeoffLandingLength, 0, 5000) - 1000), value = 10 });
            landingFrame.Add(new Keyframe { time = length - (Mathf.Clamp(takeoffLandingLength, 0, 5000) - 2500), value = 15 });
            //landingFrame.Add(new Keyframe { time = length - 2512, value = 0 });
            //landingFrame.Add(new Keyframe { time = length - 2012, value = 0 });
            //landingFrame.Add(new Keyframe { time = length - 1012, value = 8 });
            //landingFrame.Add(new Keyframe { time = length - 1512, value = 0 });
            landingFrame.Add(new Keyframe { time = length, value = 0 });

            landingPitch = new AnimationCurve(EntityController.instance.heliLandingMotion.keys);

            //Debug.Log("-- Landing Frame --");
            for (int hh = 0; hh < landingFrame.Count; hh++)
            {
                //altitudeMotion.RemoveKey(hh);
                //Debug.Log(landingFrame[hh].time + " | " + landingFrame[hh].value);

                landingPitch.AddKey(landingFrame[hh]);
            }
        }
    }

    //private void SyncEntityToggleState(bool nextEmbarkState, bool nextDebarkState)
    private void SyncEntityToggleState()
    {
        if (!dataSatuan.isActive == isOnEmbark) return;
        dataSatuan.isActive = !isOnEmbark;

        // Toggle Object's Parent
        try
        {
            bundlingCore.parent.gameObject.SetActive(dataSatuan.isActive);
        }
        catch (Exception e) { }

        // Toggle Object's Dynamic Wave
        try
        {
            for (int i = 0; i < bundlingDynamicWave.nodes.Count; i++)
            {
                bundlingDynamicWave.nodes[i].node.gameObject.SetActive(dataSatuan.isActive);
            }
        }
        catch (Exception e) { }

        // Toggle Object's Indicator
        try
        {
            dataSatuan.hudElement.showIndicator = dataSatuan.isActive;
            dataSatuan.hudElementTacmap.showIndicator = dataSatuan.isActive;
        }
        catch (Exception e) { }
    }

    private void CheckDoors(bool state)
    {
        //Cek kondisi pintu cargo jika satuan sedang berjalan
        try
        {
            var doorAnimated = bundlingCargoHold.doors[missions[CURRENT_MISSION].cargoDoorInAction].obj.GetComponent<Animator>();
            if (state)
            {
                // Jika pintu sedang dibuka
                if (!doorAnimated.GetCurrentAnimatorStateInfo(0).IsName("Normal") || doorAnimated.GetCurrentAnimatorStateInfo(0).IsName("RampDown"))
                {
                    // Force pintu untuk set to default / closed
                    doorAnimated.Play("Normal");
                    doorNeedToClose = false;
                }
            }
        }
        catch (Exception e) { }
    }

    private void RunDebarkasiStep()
    {
        // Jalankan Proses Animasi Debarkasi
        try
        {
            var parentEntity = missions[CURRENT_MISSION].insideEntity;
            var parentEntityCargoHold = parentEntity.GetComponent<NodeCargoHold>();

            var doorAnimated = parentEntityCargoHold.doors[missions[CURRENT_MISSION].cargoDoorInAction].obj.GetComponent<Animator>();
            if (doorAnimated.GetCurrentAnimatorStateInfo(0).IsName("Normal"))
            {
                // Jika pintu kargo belum dibuka, jalankan animasi
                // (Prevent multiple mission executed animated twice or more)
                doorAnimated.Play("RampDown");
                doorNeedToClose = true;
            }
        }catch(Exception e) { }
    }

    private void RunEmbarkasiStep()
    {
        try
        {
            // Jalankan Proses Animasi Embarkasi
            var doorAnimated = bundlingCargoHold.doors[missions[CURRENT_MISSION].cargoDoorInAction].obj.GetComponent<Animator>();
            if (doorAnimated.GetCurrentAnimatorStateInfo(0).IsName("RampDown"))
            {
                // Jika pintu kargo belum ditutup, jalankan animasi
                // (Prevent multiple mission executed animated twice or more)
                doorAnimated.Play("RampUp");
                doorNeedToClose = false;
            }
        }catch(Exception e) { }
    }

    private void SyncAltitude()
    {
        //lastPosition = hpTransformComponent.LocalPosition;
        if (terrainFollower == null) return;
        if (WargamingCam.instance.onRebase) return;

        if(terrainFollower.enabled && currentAltitude > 600)
        {
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
            currentAltitude = 0;
        }

        if (!isMoving || bundlingCore.bundleType != WGSBundleCore.BundleType.AIR || !syncAltitude || terrainFollower.enabled) return;
        //if (!bundlingAircraftEngine) return;

        //if (!syncAltitude) return;
        //if (terrainFollower.enabled) return;

        currentAltitude = altitudeMotion.Evaluate(splineController.AbsolutePosition);

        //if (locationComponent.Position.Z > currentAltitude && locationComponent.Position.Z > StartupConfig.settings.MAX_AIRCRAFT_ALTITUDE)
        //{
        //    locationComponent.Position = new ArcGISPoint(locationComponent.Position.X, locationComponent.Position.Y, currentAltitude, locationComponent.Position.SpatialReference);
        //    transform.position = new Vector3(transform.position.x, currentAltitude - StartupConfig.settings.MAX_AIRCRAFT_ALTITUDE.GetValueOrDefault(), transform.position.z);
        //}
        //else
        //{
        //    transform.position = new Vector3(transform.position.x, currentAltitude, transform.position.z);
        //}

        transform.position = new Vector3(transform.position.x, currentAltitude, transform.position.z);

        //hpTransformComponent.SetHPTransformPosition(lastPosition.x, currentAltitude, lastPosition.z);
    }
    private void SyncRoll()
    {
        if (guidance == null || !syncRoll) return;
        if (guidance.Spline == null) return;

        if(splineController.AbsolutePosition + guidanceDistance > guidance.Length)
        {
            guidance.RelativePosition = 1;
            bundlingCore.parent.localRotation = Quaternion.Lerp(bundlingCore.parent.localRotation, Quaternion.Euler(bundlingCore.origin.x, bundlingCore.origin.y, 0), Time.deltaTime * PlaybackController.instance.speedFactor);
            return;
        }
        else
        {
            guidance.AbsolutePosition = splineController.AbsolutePosition + guidanceDistance;
        }

        var currentOrientation = (float)GetComponent<ArcGISLocationComponent>().Rotation.Heading;
        var guidanceOrientation = (float)guidance.GetComponent<ArcGISLocationComponent>().Rotation.Heading;

        if (guidance.RelativePosition < 1)
        {
            if (oldOrientation == 0)
            {
                oldOrientation = guidanceOrientation;
            }

            angle = currentOrientation - oldOrientation;

            var angleChanges = (angle * -1) * sensitivity;
            //if (angleChanges < 0 && angleChanges < -bundlingCore.bankingRate)
            //{
            //    angleChanges = -bundlingCore.bankingRate;
            //}else if(angleChanges >= 0 && angleChanges > bundlingCore.bankingRate)
            //{
            //    angleChanges = bundlingCore.bankingRate;
            //}

            oldOrientation = guidanceOrientation;
            if (bundlingCore.bundleType == WGSBundleCore.BundleType.AIR)
            {
                if (splineController.AbsolutePosition <= takeoffLandingLength)
                {
                    // Sinkronisasi Motion Takeoff Ketika Masuk 20% Posisi Awal
                    bundlingCore.parent.localRotation = Quaternion.Lerp(bundlingCore.parent.localRotation, Quaternion.Euler(takeoffPitch.Evaluate(splineController.AbsolutePosition), bundlingCore.origin.y, angleChanges), Time.deltaTime * PlaybackController.instance.speedFactor);
                
                    
                }
                else if(splineController.AbsolutePosition >= length - takeoffLandingLength)
                {
                    // Sinkronisasi Motion Landing Ketika Masuk 20% Posisi Terakhir
                    bundlingCore.parent.localRotation = Quaternion.Lerp(bundlingCore.parent.localRotation, Quaternion.Euler(landingPitch.Evaluate(splineController.AbsolutePosition), bundlingCore.origin.y, angleChanges), Time.deltaTime * PlaybackController.instance.speedFactor);
                }
                else
                {
                    // Sinkronisasi Motion Landing Ketika Masuk 20% Posisi Terakhir
                    bundlingCore.parent.localRotation = Quaternion.Lerp(bundlingCore.parent.localRotation, Quaternion.Euler(bundlingCore.origin.x, bundlingCore.origin.y, angleChanges), Time.deltaTime * PlaybackController.instance.speedFactor);
                }
            }
        }
    }

    private void SyncAircraftTerrainFollower()
    {
        if (terrainFollower == null || dataSatuan.kategori != EntityHelper.KategoriSatuan.AIRCRAFT) return;
        if (WargamingCam.instance.onRebase) return;
        if (!isMoving) return;

        if (splineController.AbsolutePosition < 2312)
        {
            if (takeoffGuidance != null) Destroy(takeoffGuidance.gameObject);
            if (landingGuidance != null) Destroy(landingGuidance.gameObject);

            if (missions[CURRENT_MISSION].takeoffStart)
            {
                terrainFollower.enabled = true;
            }
            else
            {
                terrainFollower.enabled = false;
            }
        }
        else if(splineController.AbsolutePosition >= length - takeoffLandingLength)
        {
            terrainFollower.enabled = false;

            if (bundlingCore.bundleType == WGSBundleCore.BundleType.AIR && landingGuidance == null && missions[CURRENT_MISSION].landingEnd)
            {
                landingGuidance = EntityController.instance.CreateEntity(new Vector3((float)locationComponent.Position.X, (float)locationComponent.Position.Y, (float)locationComponent.Position.Z), Vector3.zero, AssetPackageController.Instance.prefabGuidance.gameObject).GetComponent<SplineController>();
                landingGuidance.Spline = missions[CURRENT_MISSION].GetComponent<CurvySpline>();
                landingGuidance.MotionConstraints = MotionConstraints.FreezePositionY;
                landingGuidance.gameObject.AddComponent<FollowTerrain>();
                return;
            }
        }
        else
        {
            if (takeoffGuidance != null) Destroy(takeoffGuidance.gameObject);
            if (landingGuidance != null) Destroy(landingGuidance.gameObject);

            terrainFollower.enabled = false;
        }

        if(splineController.AbsolutePosition < 2312)
        {
            if (!terrainFollower.enabled) return;
            if (!missions[CURRENT_MISSION].takeoffStart) return;

            var takeoffAltitude = terrainFollower.transform.position.y;

            var altitudeTakeoff = altitudeMotion.keys[0];
            altitudeTakeoff.value = takeoffAltitude;
            altitudeMotion.MoveKey(0, altitudeTakeoff);

            altitudeTakeoff = altitudeMotion.keys[1];
            altitudeTakeoff.value = takeoffAltitude;
            altitudeMotion.MoveKey(1, altitudeTakeoff);

            altitudeTakeoff = altitudeMotion.keys[2];
            altitudeTakeoff.value = takeoffAltitude;
            altitudeMotion.MoveKey(2, altitudeTakeoff);

            altitudeTakeoff = altitudeMotion.keys[3];
            altitudeTakeoff.value = takeoffAltitude;
            altitudeMotion.MoveKey(3, altitudeTakeoff);

        }else if(splineController.AbsolutePosition > length - 1812 - 4000)
        {
            if (!landingGuidance) return;
            if (!missions[CURRENT_MISSION].landingEnd) return;

            var landingAltitude = landingGuidance.transform.position.y;
            landingGuidance.AbsolutePosition = splineController.AbsolutePosition;

            var altitudeLanding = altitudeMotion.keys[altitudeMotion.keys.Length - 1];
            altitudeLanding.value = landingAltitude;
            altitudeMotion.MoveKey(altitudeMotion.keys.Length - 1, altitudeLanding);

            altitudeLanding = altitudeMotion.keys[altitudeMotion.keys.Length - 2];
            altitudeLanding.value = landingAltitude;
            altitudeMotion.MoveKey(altitudeMotion.keys.Length - 2, altitudeLanding);

            altitudeLanding = altitudeMotion.keys[altitudeMotion.keys.Length - 3];
            altitudeLanding.value = landingAltitude + 10;
            altitudeMotion.MoveKey(altitudeMotion.keys.Length - 3, altitudeLanding);
        }
    }

    private void SyncHeliTerrainFollower()
    {
        if (terrainFollower == null || dataSatuan.kategori != EntityHelper.KategoriSatuan.HELICOPTER || splineController == null) return;

        var activeMission = missions[CURRENT_MISSION];

        if(splineController.RelativePosition >= 1)
        {
            if (activeMission.END_AT_PARENT == null || !activeMission.END_PARENT_NODE.HasValue)
            {
                { terrainFollower.enabled = true; }
                return;
            }

            var nodeHelipad = activeMission.END_AT_PARENT.GetComponent<NodeHelipad>();
            if (nodeHelipad == null) return;

            var nodeToLand = nodeHelipad.nodes[activeMission.END_PARENT_NODE.GetValueOrDefault(0)];

            splineController.enabled = false;
            hpTransformComponent.enabled = false;
            locationComponent.enabled = false;

            transform.parent = nodeToLand.nodeHelipad.transform;

            var timediff = (PlaybackController.CURRENT_TIME - activeMission.DATA[activeMission.DATA.Count - 1].datetime).TotalMilliseconds;
            if (timediff >= 0 && timediff <= 15000)
            {
                var lastWPNode = activeMission.transform.GetChild(activeMission.transform.childCount - 1);

                var landingPosition = nodeToLand.nodeHelipad.position;
                var hoverPosition = nodeToLand.nodeHoverArea.position;

                var LANDING_STEP_1 = helipadLanding.keys[helipadLanding.keys.Length - 1];
                LANDING_STEP_1.value = landingPosition.y;
                LANDING_STEP_1.time = 15000;

                var LANDING_STEP_2 = helipadLanding.keys[helipadLanding.keys.Length - 2];
                LANDING_STEP_2.value = hoverPosition.y;
                LANDING_STEP_2.time = 0;

                helipadLanding.MoveKey(helipadLanding.keys.Length - 1, LANDING_STEP_1);
                helipadLanding.MoveKey(helipadLanding.keys.Length - 2, LANDING_STEP_2);

                lastWPNode.position = new Vector3(landingPosition.x, landingPosition.y - 1000, landingPosition.z);

                currentAltitude = helipadLanding.Evaluate((float)timediff);
                transform.localPosition = new Vector3(0, currentAltitude, 0);

                //transform.position = new Vector3(transform.position.x, currentAltitude, transform.position.z);
            }
            else
            {
                transform.localPosition = Vector3.zero;
            }

            
            transform.localRotation = Quaternion.Euler(bundlingCore.origin.x, bundlingCore.origin.y, bundlingCore.origin.z);

            return;
        }else if(splineController.RelativePosition <= 0)
        {

        }


        //if (splineController.RelativePosition <= 0)
        //{
        //    if (activeMission.START_AT_PARENT == null || !activeMission.START_PARENT_NODE.HasValue) { terrainFollower.enabled = true; }

        //    // MOTION TAKEOFF FROM SHIP'S HELIPAD
        //    if (activeMission.START_AT_PARENT != null)
        //    {
        //        var nodeHelipad = missions[CURRENT_MISSION].START_AT_PARENT.GetComponent<NodeHelipad>();
        //        if (nodeHelipad == null) return;

        //        var nodeToTakeOff = nodeHelipad.nodes[activeMission.START_PARENT_NODE.GetValueOrDefault(0)];
        //        var timediff = (activeMission.DATA[0].datetime - PlaybackController.CURRENT_TIME).TotalMilliseconds;

        //        if (timediff >= 0 && timediff <= 10000)
        //        {
        //            var firstWPNode = activeMission.transform.GetChild(0);

        //            var landingPosition = nodeToTakeOff.nodeHelipad.position;
        //            var hoverPosition = nodeToTakeOff.nodeHoverArea.position;

        //            var LANDING_STEP_1 = helipadTakeOff.keys[0];
        //            LANDING_STEP_1.value = landingPosition.y;
        //            LANDING_STEP_1.time = 0;

        //            var LANDING_STEP_2 = helipadTakeOff.keys[1];
        //            LANDING_STEP_2.value = hoverPosition.y;
        //            LANDING_STEP_2.time = 10000;

        //            helipadTakeOff.MoveKey(0, LANDING_STEP_1);
        //            helipadTakeOff.MoveKey(1, LANDING_STEP_2);

        //            firstWPNode.position = new Vector3(landingPosition.x, landingPosition.y, landingPosition.z);

        //            currentAltitude = helipadTakeOff.Evaluate((float)timediff);
        //            transform.position = new Vector3(transform.position.x, currentAltitude, transform.position.z);
        //        }
        //        else
        //        {
        //            var transformNodeTakeOff = nodeToTakeOff.nodeHoverArea.transform.localRotation;

        //            transform.position = nodeToTakeOff.nodeHoverArea.transform.position;
        //            transform.rotation = nodeToTakeOff.nodeHoverArea.transform.rotation;
        //            //transform.localRotation = Quaternion.Euler(transformNodeTakeOff.x + bundlingCore.origin.x, transformNodeTakeOff.y + bundlingCore.origin.y, transformNodeTakeOff.z + bundlingCore.origin.z);
        //        }

        //        return;
        //    }
        //}



        //else if(splineController.RelativePosition >= 1)
        //{
        //    if (activeMission.END_AT_PARENT == null || !activeMission.END_PARENT_NODE.HasValue) { terrainFollower.enabled = true; }

        //    // MOTION LANDING AT SHIP'S HELIPAD

        //    if(activeMission.END_AT_PARENT != null)
        //    {
        //        var nodeHelipad = missions[CURRENT_MISSION].END_AT_PARENT.GetComponent<NodeHelipad>();
        //        if (nodeHelipad == null) return;

        //        var nodeToLand = nodeHelipad.nodes[activeMission.END_PARENT_NODE.GetValueOrDefault(0)];

        //        var timediff = (PlaybackController.CURRENT_TIME - activeMission.DATA[activeMission.DATA.Count - 1].datetime).TotalMilliseconds;
        //        if (timediff >= 0 && timediff <= 15000)
        //        {
        //            var lastWPNode = activeMission.transform.GetChild(activeMission.transform.childCount - 1);

        //            var landingPosition = nodeToLand.nodeHelipad.position;
        //            var hoverPosition = nodeToLand.nodeHoverArea.position;

        //            var LANDING_STEP_1 = helipadLanding.keys[helipadLanding.keys.Length - 1];
        //            LANDING_STEP_1.value = hoverPosition.y;
        //            LANDING_STEP_1.time = 15000;

        //            var LANDING_STEP_2 = helipadLanding.keys[helipadLanding.keys.Length - 2];
        //            LANDING_STEP_2.value = landingPosition.y;
        //            LANDING_STEP_2.time = 0;

        //            helipadLanding.MoveKey(helipadLanding.keys.Length - 1, LANDING_STEP_1);
        //            helipadLanding.MoveKey(helipadLanding.keys.Length - 2, LANDING_STEP_2);

        //            lastWPNode.position = new Vector3(landingPosition.x, landingPosition.y - 1000, landingPosition.z);

        //            currentAltitude = helipadLanding.Evaluate((float)timediff);
        //            transform.position = new Vector3(transform.position.x, currentAltitude, transform.position.z);
        //        }
        //        else
        //        {
        //            var transformNodeLanding = nodeToLand.nodeHoverArea.transform.localRotation;

        //            //transform.position = nodeToLand.nodeHoverArea.transform.position;
        //            //transform.localRotation = Quaternion.Euler(transformNodeLanding.x + bundlingCore.origin.x, transformNodeLanding.y + bundlingCore.origin.y, transformNodeLanding.z + bundlingCore.origin.z);

        //            transform.position = nodeToLand.nodeHoverArea.transform.position;
        //            transform.rotation = nodeToLand.nodeHoverArea.transform.rotation;
        //        }

        //        return;
        //    }
        //}





        //if (splineController.RelativePosition <= 0)
        //{
        //    if(activeMission.START_AT_PARENT == null)
        //    {
        //        terrainFollower.enabled = true;
        //    }
        //    else
        //    {
        //        //var parentHelipad = activeMission.START_AT_PARENT.GetComponent<NodeHelipad>();
        //        //if (parentHelipad == null) return;

        //        //var timediff = (PlaybackController.CURRENT_TIME - activeMission.DATA[0].datetime).TotalMilliseconds;
        //        //if(timediff >= 0 && timediff <= 15000)
        //        //{
        //        //    var firstWPNode = activeMission.transform.GetChild(activeMission.transform.childCount - 1);
        //        //    for(int i=0; i < parentHelipad.nodes.Count; i++)
        //        //    {
        //        //        if (parentHelipad.nodes[i].isOccupied) continue;
        //        //        if (i >= parentHelipad.nodes.Count) return;

        //        //        var helipadPOS = parentHelipad.nodes[i].nodeHelipad.transform.position;
        //        //        var helipadHoverPOS = parentHelipad.nodes[i].nodeHelipad.transform.position;

        //        //        if (helipadPOS == null || helipadHoverPOS == null) continue;

        //        //        var TAKEOFF_STEP_ONE        = helipadTakeOff.keys[helipadTakeOff.keys.Length - 1];
        //        //        TAKEOFF_STEP_ONE.value      = helipadPOS.y;
        //        //        TAKEOFF_STEP_ONE.time       = 15000;

        //        //        var TAKEOFF_STEP_TWO        = helipadTakeOff.keys[helipadTakeOff.length - 2];
        //        //        TAKEOFF_STEP_TWO.value      = helipadHoverPOS.y;
        //        //        TAKEOFF_STEP_TWO.time       = 0;

        //        //        helipadTakeOff.MoveKey(helipadTakeOff.keys.Length - 1, TAKEOFF_STEP_ONE);
        //        //        helipadTakeOff.MoveKey(helipadTakeOff.keys.Length - 2, TAKEOFF_STEP_TWO);

        //        //        firstWPNode.transform.position = new Vector3(helipadPOS.x, helipadPOS.y - 1000, helipadPOS.z);
        //        //    }

        //        //    currentAltitude = helipadTakeOff.Evaluate((float)timediff);
        //        //    transform.position = new Vector3(transform.position.x, currentAltitude, transform.position.z);
        //        //}
        //        //else
        //        //{
        //        //    transform.position = parentHelipad.nodes[0].nodeHoverArea.transform.position;
        //        //    transform.rotation = parentHelipad.nodes[0].nodeHoverArea.transform.rotation;
        //        //}
        //    }
        //}else if(splineController.RelativePosition >= 1)
        //{
        //    if (missions[CURRENT_MISSION].END_AT_PARENT == null)
        //    {
        //        terrainFollower.enabled = true;
        //    }
        //    else
        //    {
        //        var parentHelipad = missions[CURRENT_MISSION].END_AT_PARENT.GetComponent<NodeHelipad>();
        //        if (parentHelipad == null) return;

        //        var timediff = (PlaybackController.CURRENT_TIME - missions[CURRENT_MISSION].DATA[missions[CURRENT_MISSION].DATA.Count - 1].datetime).TotalMilliseconds;
        //        //Debug.Log(timediff);

        //        var insideEntityLP = missions[CURRENT_MISSION].END_AT_PARENT.GetComponent<NodeHelipad>();
        //        if (timediff >= 0 && timediff <= 15000)
        //        {
                    
        //            var lastWPNode = missions[CURRENT_MISSION].transform.GetChild(missions[CURRENT_MISSION].transform.childCount - 1);
        //            for (int i = 0; i < insideEntityLP.nodes.Count; i++)
        //            {
        //                if (insideEntityLP.nodes[i].isOccupied) continue;
        //                if (i >= parentHelipad.nodes.Count) return;

        //                var helipadPos = insideEntityLP.nodes[i].nodeHelipad.transform.position;
        //                var helipadHoverPos = insideEntityLP.nodes[i].nodeHoverArea.transform.position;

        //                var helipadLandingFrame1 = helipadLanding.keys[helipadLanding.keys.Length - 1];
        //                helipadLandingFrame1.value = helipadHoverPos.y;
        //                helipadLandingFrame1.time = 15000;

        //                var helipadLandingFrame2 = helipadLanding.keys[helipadLanding.keys.Length - 2];
        //                helipadLandingFrame2.value = helipadPos.y;
        //                helipadLandingFrame2.time = 0;

        //                helipadLanding.MoveKey(helipadLanding.keys.Length - 1, helipadLandingFrame1);
        //                helipadLanding.MoveKey(helipadLanding.keys.Length - 2, helipadLandingFrame2);

        //                lastWPNode.transform.position = new Vector3(helipadPos.x, helipadPos.y - 1000, helipadPos.z);
        //            }

        //            currentAltitude = helipadLanding.Evaluate((float)timediff);
        //            transform.position = new Vector3(transform.position.x, currentAltitude, transform.position.z);
        //        }
        //        else
        //        {
        //            transform.position = insideEntityLP.nodes[0].nodeHoverArea.transform.position;
        //            transform.rotation = insideEntityLP.nodes[0].nodeHoverArea.transform.rotation;
        //        }
        //    }

        //    return;
        //}

        ////if (splineController.RelativePosition <= 0 || splineController.RelativePosition >= 1)
        ////{
            
        ////    return;
        ////}

        if ((splineController.AbsolutePosition > takeoffLandingLength && splineController.AbsolutePosition < length - takeoffLandingLength))
        {
            if (takeoffGuidance != null) Destroy(takeoffGuidance.gameObject);
            if (landingGuidance != null) Destroy(landingGuidance.gameObject);
            return;
        }

        if (splineController.AbsolutePosition <= takeoffLandingLength)
        {
            terrainFollower.enabled = false;

            if(bundlingCore.bundleType == WGSBundleCore.BundleType.AIR && takeoffGuidance == null && missions[CURRENT_MISSION].takeoffStart)
            {
                takeoffGuidance = EntityController.instance.CreateEntity(new Vector3((float)locationComponent.Position.X, (float)locationComponent.Position.Y, (float)locationComponent.Position.Z), Vector3.zero, AssetPackageController.Instance.prefabGuidance.gameObject).GetComponent<SplineController>();
                takeoffGuidance.Spline = missions[CURRENT_MISSION].GetComponent<CurvySpline>();
                takeoffGuidance.RelativePosition = 0;
                takeoffGuidance.MotionConstraints = MotionConstraints.FreezePositionY;

                var takeoffLandingTerrainFollower = takeoffGuidance.gameObject.AddComponent<FGroundFitter>();
                takeoffLandingTerrainFollower.FittingSpeed          = 5;
                takeoffLandingTerrainFollower.TotalSmoother         = 0;
                takeoffLandingTerrainFollower.GlueToGround          = true;
                takeoffLandingTerrainFollower.RaycastHeightOffset   = 4000;
                takeoffLandingTerrainFollower.RaycastCheckRange     = 8000;
                return;
            }

            if(takeoffGuidance != null)
            {
                var takeoffAltitude = takeoffGuidance.transform.position.y;

                var altitudeTakeoff     = altitudeMotion.keys[0];
                altitudeTakeoff.value   = takeoffAltitude;
                altitudeMotion.MoveKey(0, altitudeTakeoff);

                altitudeTakeoff         = altitudeMotion.keys[1];
                altitudeTakeoff.value   = takeoffAltitude;
                altitudeMotion.MoveKey(1, altitudeTakeoff);
            }
        }
        else if (splineController.AbsolutePosition >= length - takeoffLandingLength)
        {
            terrainFollower.enabled = false;

            if (bundlingCore.bundleType == WGSBundleCore.BundleType.AIR && landingGuidance == null && missions[CURRENT_MISSION].landingEnd)
            {
                landingGuidance = EntityController.instance.CreateEntity(new Vector3((float)locationComponent.Position.X, (float)locationComponent.Position.Y, (float)locationComponent.Position.Z), Vector3.zero, AssetPackageController.Instance.prefabGuidance.gameObject).GetComponent<SplineController>();
                landingGuidance.Spline = missions[CURRENT_MISSION].GetComponent<CurvySpline>();
                landingGuidance.RelativePosition = 1;
                landingGuidance.MotionConstraints = MotionConstraints.FreezePositionY;

                var takeoffLandingTerrainFollower = landingGuidance.gameObject.AddComponent<FGroundFitter>();
                takeoffLandingTerrainFollower.FittingSpeed = 5;
                takeoffLandingTerrainFollower.TotalSmoother = 0;
                takeoffLandingTerrainFollower.GlueToGround = true;
                takeoffLandingTerrainFollower.RaycastHeightOffset = 4000;
                takeoffLandingTerrainFollower.RaycastCheckRange = 8000;
                return;
            }

            if (landingGuidance != null && missions[CURRENT_MISSION].insideEntity == null)
            {
                var landingAltitude     = landingGuidance.transform.position.y;

                var altitudeLanding     = altitudeMotion.keys[altitudeMotion.keys.Length - 1];
                altitudeLanding.value   = landingAltitude;
                altitudeMotion.MoveKey(altitudeMotion.keys.Length - 1, altitudeLanding);
            }
        }

        if (missions[CURRENT_MISSION].END_AT_PARENT != null)
        {
            var insideEntityLP = missions[CURRENT_MISSION].END_AT_PARENT.GetComponent<NodeHelipad>();
            if (insideEntityLP == null) return;

            var lastWPNode = missions[CURRENT_MISSION].transform.GetChild(missions[CURRENT_MISSION].transform.childCount - 1);
            for (int i = 0; i < insideEntityLP.nodes.Count; i++)
            {
                if (insideEntityLP.nodes[i].isOccupied) continue;

                var helipadHoverPos = insideEntityLP.nodes[i].nodeHelipad.transform.position;
                lastWPNode.transform.position = new Vector3(helipadHoverPos.x, helipadHoverPos.y - 1000, helipadHoverPos.z);
            }

            if (landingGuidance != null)
            {
                var landingAltitude = lastWPNode.transform.position.y + 1000;

                var altitudeLanding = altitudeMotion.keys[altitudeMotion.keys.Length - 1];
                altitudeLanding.value = landingAltitude;
                altitudeLanding.time = length;

                var altitudeLanding2 = altitudeMotion.keys[altitudeMotion.keys.Length - 2];
                altitudeLanding2.time = length - Mathf.Clamp(takeoffLandingLength, 0, 5000);

                altitudeMotion.MoveKey(altitudeMotion.keys.Length - 2, altitudeLanding2);
                altitudeMotion.MoveKey(altitudeMotion.keys.Length - 1, altitudeLanding);

                var landingPitchKey1 = landingPitch.keys[landingPitch.keys.Length - 1];
                landingPitchKey1.time = length;

                var landingPitchKey2 = landingPitch.keys[landingPitch.keys.Length - 2];
                landingPitchKey2.time = length - (Mathf.Clamp(takeoffLandingLength, 0, 5000) - 2500);

                var landingPitchKey3 = landingPitch.keys[landingPitch.keys.Length - 3];
                landingPitchKey3.time = length - (Mathf.Clamp(takeoffLandingLength, 0, 5000) - 1000);

                var landingPitchKey4 = landingPitch.keys[landingPitch.keys.Length - 4];
                landingPitchKey4.time = length - Mathf.Clamp(takeoffLandingLength, 0, 5000);


                landingPitch.MoveKey(landingPitch.keys.Length - 1, landingPitchKey1);
                landingPitch.MoveKey(landingPitch.keys.Length - 2, landingPitchKey2);
                landingPitch.MoveKey(landingPitch.keys.Length - 3, landingPitchKey3);
                landingPitch.MoveKey(landingPitch.keys.Length - 4, landingPitchKey4);
            }

            return;
        }
    }

    private void SyncTacmapIcon()
    {
        if (tacmapMarker != null && EntityController.instance.tacmapOrientation)
        {
            tacmapMarker.SetPosition(locationComponent.Position.X, locationComponent.Position.Y);

            var tacmapHUD = tacmapMarker.instance.GetComponent<HUDNavigationElement>();
            if (tacmapHUD != null)
            {
                tacmapHUD.Indicator.RotateIcon(new Vector3(0, 180, (float)locationComponent.Rotation.Heading));
            }
        }
    }

    private void SyncVFX()
    {
        if (bundlingVFX == null) return;
        if ((isMoving && !bundlingVFX.state) || (!isMoving && bundlingVFX.state))
        {
            for(int i=0; i < bundlingVFX.nodes.Count; i++)
            {
                if (bundlingVFX.nodes[i].node == null) continue;
                bundlingVFX.nodes[i].node.gameObject.SetActive(isMoving);
            }
        }
    }

    private void SyncSound()
    {
        if(audioController == null) return;

        if(isMoving && !audioController.engineAudioState)
        {
            audioController.audioEnginePlay();
        }else if(!isMoving && audioController.engineAudioState)
        {
            audioController.audioEngineStop();
        }

        //if (!PlaybackController.instance.isPlaying || PlaybackController.instance.isSliding) return;

        var newSpeed = (PlaybackController.instance.speedFactor * ((float)distanceDifferent * multiplier));

        //wheelSpeed = (PlaybackController.instance.speedFactor * (float)distanceDifferent);
        if (newSpeed < wheelSpeed)
        {
            wheelSpeed -= (Time.deltaTime * smoothness);
        }
        else if (newSpeed > wheelSpeed)
        {
            wheelSpeed += (Time.deltaTime * smoothness);
        }

        //var wheel2 = wheelSpeed * multiplier;

        //for (int i=0; i < bundlingSound.engineSounds.Count; i++)
        //{
        //    if (bundlingSound.engineSounds[i].audioNode == null) continue;

        //    var audioSource = bundlingSound.engineSounds[i].audioNode.GetComponent<AudioSource>();
        //    audioSource.pitch = Mathf.Clamp((wheelSpeed / bundlingWheel.nodes[i].radius), 1, 2);
        //}
    }

    private void SyncWheel()
    {
        if (!PlaybackController.IsPlaying()) return;

        if (bundlingWheel == null) return;
        if (bundlingWheel.nodes.Count <= 0) return;

        //calculate distance from last point to this point
        distanceDifferent = Math.Round(ArcGISGeometryEngine.DistanceGeodetic(
            oldLocation,
            locationComponent.Position,
            new ArcGISLinearUnit((ArcGISLinearUnitId)9001),
            new ArcGISAngularUnit((ArcGISAngularUnitId)9102),
            ArcGISGeodeticCurveType.Geodesic).Distance, 2);

        oldLocation = locationComponent.Position;
        //oldRotation = locationComponent.Rotation;

        var wheelSpeed = (PlaybackController.instance.speedFactor * (float)distanceDifferent);

        //var currentOrientation = (float)GetComponent<ArcGISLocationComponent>().Rotation.Heading;

        //var angleChange = currentOrientation - oldOrientation;
        //wheelAngle = angleChange * wheelAngleMultiplier;

        //oldOrientation = currentOrientation;

        for (int i = 0; i < bundlingWheel.nodes.Count; i++)
        {
            bundlingWheel.nodes[i].node.GetComponent<Animator>().speed = wheelSpeed / bundlingWheel.nodes[i].radius;

            //var zz = bundlingWheel.nodes[i].node.parent.localRotation.z;

            //bundlingWheel.nodes[i].node.parent.localRotation = //Quaternion.Euler(0, 0, wheelAngle);
            //    Quaternion.Lerp(Quaternion.Euler(0, 0, zz), Quaternion.Euler(0, 0, zz + angleChange), Time.deltaTime * wheelRotationMultiplier);
        }
    }

    private void SyncGetAltitude()
    {
        return;
        if (terrainFollower == null) return;
        if (!terrainFollower.enabled) return;

        // start the raycast in the air at an arbitrary to ensure it is above the ground
        var raycastHeight = 8000;
        var position = gameObject.transform.position;

        var raycastStart = new Vector3(position.x, position.y + raycastHeight, position.z);
        var hitInfo = Physics.RaycastAll(raycastStart, Vector3.down);

        for (int i = 0; i < hitInfo.Length; i++)
        {
            if (hitInfo[i].transform == this) continue;

            var worldPosition = math.inverse(arcGISMapComponent.WorldMatrix).HomogeneousTransformPoint(hitInfo[i].point.ToDouble3());

            var geoPosition = arcGISMapComponent.View.WorldToGeographic(worldPosition);
            if(locationComponent.Position.Z < geoPosition.Z - 50)
            {
                locationComponent.Position = new ArcGISPoint(locationComponent.Position.X, locationComponent.Position.Y, geoPosition.Z, locationComponent.Position.SpatialReference);
            }
        }
    }
}