using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class TemplateAssets : MonoBehaviour
{
    [Header("Search")]
    public TMP_InputField searchDatapack;

    public void RefreshList()
    {
        searchDatapack.text = "";
    }
}
