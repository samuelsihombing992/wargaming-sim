using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using UIWidgets;
using System.Text.RegularExpressions;
using Wargaming.Components.UI;
using Esri.GameEngine.Geometry;
using WGSDatapack.Core;
using Battlehub.RTCommon;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.ArcGISMapsSDK.Components;
using System;
using static DatapackPackageController;
using System.Xml.Serialization;
using System.IO;

public class DatapackEditor : MonoBehaviour
{
    [Header("Form Data")]
    public TMP_InputField nama;

    [Header("3D Viewer")]
    public UI3D modelViewer;

    [Header("Datapack References")]
    [SerializeField]
    public TreeView DatapackList;

    public TMP_InputField searchDatapack;
    public TMP_Dropdown filterDatapack;

    public Transform datapackTempContainer;

    public GameObject selected;
    public DatapackTemplateData selectedTemplate;

    private ObservableList<TreeNode<TreeViewItem>> DatapackPackages;

    #region INSTANCE
    public static DatapackEditor instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        searchDatapack.onValueChanged.AddListener(delegate { SearchDatapack(searchDatapack.text, filterDatapack.value); });
        filterDatapack.onValueChanged.AddListener(delegate { SearchDatapack(searchDatapack.text, filterDatapack.value); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InitFilterDatapack()
    {
        List<string> filterOption = new List<string> { "ALL" };
        var package = DatapackPackageController.Instance;

        for (int i=0; i < package.DATAPACK_GROUP.Count; i++)
        {
            filterOption.Add(package.DATAPACK_GROUP[i].id);
        }

        filterDatapack.ClearOptions();
        filterDatapack.AddOptions(filterOption);
    }

    /// <summary>
    /// Load all datapack package to list
    /// </summary>
    public void LoadAllDatapack()
    {
        var package = DatapackPackageController.Instance;

        DatapackPackages = new ObservableList<TreeNode<TreeViewItem>>();
        DatapackPackages.BeginUpdate();

        int assetIndex = 0;
        for (int i=0; i < package.DATAPACK_GROUP.Count; i++)
        {
            var item = new TreeViewItem(package.DATAPACK_GROUP[i].id, null);
            item.Value = -99;

            if (package.DATAPACK_GROUP[i].datapack.Count > 0)
            {
                var SubitemPackages = new ObservableList<TreeNode<TreeViewItem>>();
                SubitemPackages.BeginUpdate();

                var datapack = package.DATAPACK_GROUP[i].datapack;

                for (int j = 0; j < datapack.Count; j++)
                {
                    var subitem = new TreeViewItem(datapack[j].name, null);
                    subitem.Value = assetIndex;

                    SubitemPackages.Add(new TreeNode<TreeViewItem>(subitem, null));

                    assetIndex++;
                }

                SubitemPackages.EndUpdate();

                DatapackPackages.Add(new TreeNode<TreeViewItem>(item, SubitemPackages));
            }
            else
            {
                DatapackPackages.Add(new TreeNode<TreeViewItem>(item));
            }
        }

        DatapackPackages.EndUpdate();

        DatapackList.Init();
        DatapackList.Nodes = DatapackPackages;
    }

    public void SearchDatapack(string search, int index = 0)
    {
        var package = DatapackPackageController.Instance;

        DatapackPackages = new ObservableList<TreeNode<TreeViewItem>>();
        DatapackPackages.BeginUpdate();

        Regex r = new Regex(search.ToLower());

        int assetIndex = 0;
        for (int i = 0; i < package.DATAPACK_GROUP.Count; i++)
        {
            if (index != 0 && (index - 1) != i) continue; 

            var datapack = package.DATAPACK_GROUP[i].datapack;

            var SubitemPackages = new ObservableList<TreeNode<TreeViewItem>>();
            SubitemPackages.BeginUpdate();

            TreeViewItem item = null;

            for (int j = 0; j < datapack.Count; j++)
            {
                bool isActive = false;
                if (search == "" || search == null || r.IsMatch(datapack[j].name.ToLower()))
                {
                    isActive = true;
                }

                if (isActive)
                {
                    var subitem = new TreeViewItem(datapack[j].name, null);
                    subitem.Value = assetIndex;

                    SubitemPackages.Add(new TreeNode<TreeViewItem>(subitem, null));

                    if(item == null)
                    {
                        item = new TreeViewItem(package.DATAPACK_GROUP[i].id, null);
                    }
                }

                assetIndex++;
            }
            SubitemPackages.EndUpdate();

            if (item != null)
            {
                DatapackPackages.Add(new TreeNode<TreeViewItem>(item, SubitemPackages));
            }

        }

        DatapackPackages.EndUpdate();

        DatapackList.Nodes = DatapackPackages;
    }

    public void SelectItem(TreeViewComponent itemComponent)
    {
        if (itemComponent.Item.Value == -99) return;

        selected = DatapackPackageController.Instance.DATAPACK_ASSET[itemComponent.Item.Value];
        modelViewer.Init(selected);
    }

    public void RemoveAsset(DatapackAssetList list)
    {
        //Destroy(list.asset);
    }

    public void AddAssetToTemplate(ArcGISPoint _location)
    {
        if (selectedTemplate == null)
        {
            var newTemplate                 = new GameObject();
            newTemplate.name                = nama.text;
            newTemplate.transform.parent    = datapackTempContainer;

            selectedTemplate            = newTemplate.AddComponent<DatapackTemplateData>();
            selectedTemplate.nama       = nama.text;
            selectedTemplate.assets     = new List<GameObject>();
        }

        CreateTempTemplate(_location);
    }

    public void CreateTempTemplate(ArcGISPoint _location)
    {
        var newAsset    = EntityController.instance.CreateEntity(_location, new ArcGISRotation(0, 90, 0), selected, selectedTemplate.transform);
        newAsset.name    = selected.name;

        var editorTransform                 = newAsset.AddComponent<ExposeToEditor>();
        editorTransform.CanTransform        = true;
        editorTransform.CanDuplicate        = true;
        editorTransform.CanDelete           = true;
        editorTransform.ShowSelectionGizmo  = true;

        selectedTemplate.assets.Add(newAsset);
    }

    public void CreateTemplate()
    {
        if (selectedTemplate == null)
        {
            Debug.LogError("Failed to Save Template. No Data Found!");
            return;
        }

        List<PlacementObjectExport> DATA = new List<PlacementObjectExport>();
        for(int i=0; i < selectedTemplate.assets.Count; i++)
        {
            var asset = selectedTemplate.assets[i].GetComponent<ArcGISLocationComponent>();

            try
            {
                DATA.Add(new PlacementObjectExport
                {
                    objID = asset.name,
                    posX = (float) asset.Position.X,
                    posY = (float) asset.Position.Y,
                    posZ = (float) asset.Position.Z,
                    rotX = (float)asset.Rotation.Heading,
                    rotY = (float)asset.Rotation.Pitch,
                    rotZ = (float)asset.Rotation.Roll
                });
            }
            catch (Exception e)
            {
                Debug.LogError("Failed to Save Template");
                break;
            }
        }

        if (Instance.DATAPACK_TEMPLATES.Find(r => r.name == selectedTemplate.name) != null) return;

        DatapackTemplate newTemplate = new DatapackTemplate { name = selectedTemplate.name, data = DATA };

        // Serialize and Write Changed into Datapack Template Document
        var _XMLSerialize = new XmlSerializer(typeof(DatapackTemplate));
        using (var writer = new StreamWriter(Path.Combine(StartupConfig.GetDatapackDirectory(), newTemplate.name) + "." + DatapackPackageController.Instance.templateFormat))
        {
            _XMLSerialize.Serialize(writer, newTemplate);
        }

        DatapackPackageController.Instance.DATAPACK_TEMPLATES.Add(newTemplate);

        // Move Template from Temp Container into Saved Template Container
        selectedTemplate.transform.parent = GameObject.FindGameObjectWithTag("datapack-group").transform;
        selectedTemplate.nama = nama.text;

        selected = null;
        selectedTemplate = null;

        Debug.Log("Template Saved");

        CleanDatapackTemp();
    }

    private void CleanDatapackTemp()
    {
        foreach(Transform item in datapackTempContainer.transform)
        {
            Destroy(item.gameObject);
        }
    }

    public void ResetForm()
    {
        nama.text = "";
        selected = null;
        selectedTemplate = null;
    }

    public void OnCreateNewTemplate()
    {
        ResetForm();
        CleanDatapackTemp();
    }
}
