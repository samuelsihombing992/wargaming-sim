using System;
using UIWidgets;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class DatapackAssetList : ListViewItem
{
	[SerializeField]
	public TextAdapter NameAdapter;

	/// <summary>
	/// Adds listeners.
	/// </summary>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "HAA0603:Delegate allocation from a method group", Justification = "Required")]
	protected override void Start()
	{
		base.Start();
	}

	public void SetData(DatapackAssetListItem item)
	{
		NameAdapter.text = name;

		
	}
}
