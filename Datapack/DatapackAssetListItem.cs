using System;
using UnityEngine;
using UnityEngine.Serialization;

public class DatapackAssetListItem
{
    [SerializeField]
    public string name;

    [SerializeField]
    public GameObject asset;
}
