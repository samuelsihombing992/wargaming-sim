#region IMPORTS
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

using TMPro;
using Shapes;
using Battlehub.RTCommon;
using Esri.GameEngine.Geometry;
using Esri.ArcGISMapsSDK.Components;
using SickscoreGames.HUDNavigationSystem;

using Wargaming.Core.GlobalParam;
using Wargaming.Core.GlobalParam.HelperPlotting;
using Wargaming.Core.GlobalParam.HelperDataAlutsista;
using Wargaming.Core.ScenarioEditor.ColorPalette;
using static Wargaming.Core.GlobalParam.DrawingHelper;
using static Wargaming.Core.GlobalParam.HelperDataPlotting.PlottingHelper;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
#endregion

namespace Wargaming.Core.ScenarioEditor.Drawing
{
    public class PlotDrawingController : PlottingBase
    {
        public static DrawingType mode;
        public static DrawingArrowType arrowType;
        public static DrawingManouverType manouverType;

        [Header("Properties Data")]
        public GameObject outlineGroup;
        public TMP_InputField outlineWeight;
        public TMP_InputField outlineOpacity;
        public TMP_Dropdown outlineColorType;
        public Transform outlineColorPreset;
        public CustomColorPalette customOutlineColor;

        public GameObject fillGroup;
        public TMP_InputField fillOpacity;
        public TMP_Dropdown fillColorType;
        public Transform fillColorPreset;
        public CustomColorPalette customFillColor;

        public GameObject rectGroup;

        public GameObject circleGroup;

        [Header("Vertex Data")]
        public GameObject vertexGroup;
        public Transform vertexContainer;
        public Toggle addVertex;
        public Toggle moveDrawing;

        [Header("UI References")]
        public TextMeshProUGUI title;

        [Header("Prefab References")]
        public GameObject prefabVertexList;

        // SELECTED DRAWING TEMP DATA
        [NonSerialized] public ObjekDrawing selectedDrawing;
        [NonSerialized] public List<Vector3> vertexData = new List<Vector3>();
        private EDrawPredictor tempPredictor;

        // CACHE Object Untuk di-Remove
        [NonSerialized] public List<ObjekTrash> trashData = new List<ObjekTrash>();

        #region INSTANCE
        public static PlotDrawingController instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private void Start()
        {
            outlineColorType.onValueChanged.AddListener(delegate { OnOutlineColorTypeChange(); });
            fillColorType.onValueChanged.AddListener(delegate { OnFillColorTypeChange(); });
        }

        private void Update()
        {
            if (selectedDrawing == null) return;
            ShowDrawingPrediction();
        }

        #region DRAWING PREDICTION
        private void ShowDrawingPrediction()
        {
            if (selectedDrawing.geometry.Count <= 0) return;

            if (!EntityPlacementController.instance.onUse || ScenarioEditorController.instance.mode != MenuMode.DRAWING) return;

            if (tempPredictor == null)
            {
                tempPredictor = Instantiate(AssetPackageController.Instance.drawingPredictor, EnvironmentsController.GetMapComponent().transform);
                tempPredictor.init();
                return;
            }

            //Get Rayacast and Map Coordinate of Next Predicted Segment
            var raycastHit = EntityPlacementController.instance.OnRaycastPlacement();
            if (!raycastHit.HasValue) return;

            var mapPoint = EntityPlacementController.instance.HitToGeoPosition(raycastHit.GetValueOrDefault());
            tempPredictor.mousePos = mapPoint;
        }
        #endregion



        #region COLOR PALETTE MANAGER
        public void OnOutlineColorChange(DataColorPalette data)
        {
            customOutlineColor.color    = data.color;
            customOutlineColor.opacity  = data.color.a;
        }

        public void OnFillColorChange(DataColorPalette data)
        {
            customFillColor.color       = data.color;
            customFillColor.opacity     = data.color.a;
        }

        private void RefreshSegmentSelectorColor()
        {
            for (int i = 0; i < selectedDrawing.geometry.Count; i++)
            {
                var geometry = selectedDrawing.geometry[i].GetComponent<Cuboid>();
                if (geometry == null) continue;

                geometry.Color = (selectedDrawing.properties.warna.a != 0) ? selectedDrawing.properties.warna : selectedDrawing.properties.fillColor;
            }
        }

        public void OnOutlineColorTypeChange()
        {
            customOutlineColor.gameObject.SetActive((outlineColorType.value == 0) ? false : true);
        }
        public void OnFillColorTypeChange()
        {
            customFillColor.gameObject.SetActive((fillColorType.value == 0) ? false : true);
        }

        /// <summary>
        /// Toggle One of The Color Group Based On SelectedDrawing Color Properties
        /// </summary>
        /// <param name="type">Color Group(outline/fill)</param>
        public void SyncSelectedColor(string type)
        {
            bool isCustomColor  = true;

            Color outlineColor  = (selectedDrawing == null) ? DrawingToolsController.instance.outlineColor : selectedDrawing.properties.warna;
            Color fillColor     = (selectedDrawing == null) ? DrawingToolsController.instance.fillColor : selectedDrawing.properties.fillColor;

            float outlineOpacity    = outlineColor.a;
            float fillOpacity       = fillColor.a;

            outlineColor.a  = 1;
            fillColor.a     = 1;

            if(type == "outline")
            {
                foreach (Transform color in outlineColorPreset.transform)
                {
                    var colorData = color.GetComponent<DataColorPalette>();
                    if (colorData == null) continue;

                    if (outlineColor == colorData.color)
                    {
                        color.GetComponent<Toggle>().isOn = true;
                        isCustomColor = false;
                        break;
                    }
                }

                outlineColorType.value = (isCustomColor) ? 1 : 0;
                customOutlineColor.Setup(
                    outlineColor,
                    outlineOpacity
                );
            }
            else if (type == "fill")
            {
                foreach (Transform color in fillColorPreset.transform)
                {
                    var colorData = color.GetComponent<DataColorPalette>();
                    if (colorData == null) continue;

                    if (fillColor == colorData.color)
                    {
                        color.GetComponent<Toggle>().isOn = true;
                        isCustomColor = false;
                        break;
                    }
                }

                fillColorType.value = (isCustomColor) ? 1 : 0;
                customFillColor.Setup(
                    fillColor,
                    fillOpacity
                );
            }
        }
        #endregion

        #region FORM HANDLE
        public void Reset()
        {
            // Disable Entire UI Group
            outlineGroup.SetActive(false);
            fillGroup.SetActive(false);
            rectGroup.SetActive(false);
            circleGroup.SetActive(false);

            vertexGroup.gameObject.SetActive(false);
            addVertex.gameObject.SetActive(false);
            moveDrawing.gameObject.SetActive(false);

            selectedDrawing     = null;

            if (tempPredictor != null) Destroy(tempPredictor.gameObject);
            tempPredictor       = null;
        }

        public void FillFormData()
        {
            if (selectedDrawing == null) return;

            // Set Form Fillable From New ObjekDrawing Data
            if (selectedDrawing.properties.stroke)
            {
                outlineWeight.text = selectedDrawing.properties.weight.ToString();
                outlineOpacity.text = selectedDrawing.properties.opacity.ToString();
                SyncSelectedColor("outline");
            }

            if (selectedDrawing.properties.fill)
            {
                fillOpacity.text = selectedDrawing.properties.fillOpacity.ToString();
                SyncSelectedColor("fill");
            }
        }

        public void ApplyFormChange()
        {
            if (selectedDrawing == null) return;

            var property        = selectedDrawing.properties;

            var outlineColor    = customOutlineColor.color;
            outlineColor.a      = 1; // make sure alpha value of color is not saved

            var fillColor       = customFillColor.color;
            fillColor.a         = 1; // make sure alpha value of color is not saved

            Debug.Log(selectedDrawing.type);

            switch (selectedDrawing.type)
            {
                case DrawingType.POLYLINE:
                    PlotDrawingControllerHelper.ApplyChangedData(
                        selectedDrawing,
                        NumberConvertionHelper.ConvertToFloat(outlineWeight.text),
                        customOutlineColor.color,
                        Mathf.Clamp(NumberConvertionHelper.ConvertToFloat(outlineOpacity.text), 0, 1)
                    );
                    break;
                case DrawingType.POLYGON:
                    PlotDrawingControllerHelper.ApplyChangedData(
                        selectedDrawing,
                        NumberConvertionHelper.ConvertToFloat(outlineWeight.text),
                        customOutlineColor.color,
                        Mathf.Clamp(NumberConvertionHelper.ConvertToFloat(outlineOpacity.text), 0, 1),
                        customFillColor.color,
                        Mathf.Clamp(NumberConvertionHelper.ConvertToFloat(fillOpacity.text), 0, 1)
                    );
                    break;
                case DrawingType.RECTANGLE:
                    PlotDrawingControllerHelper.ApplyChangedData(
                        selectedDrawing,
                        NumberConvertionHelper.ConvertToFloat(outlineWeight.text),
                        customOutlineColor.color,
                        Mathf.Clamp(NumberConvertionHelper.ConvertToFloat(outlineOpacity.text), 0, 1),
                        customFillColor.color,
                        Mathf.Clamp(NumberConvertionHelper.ConvertToFloat(fillOpacity.text), 0, 1)
                    );
                    break;
                case DrawingType.CIRCLE:
                    PlotDrawingControllerHelper.ApplyChangedData(
                        selectedDrawing,
                        NumberConvertionHelper.ConvertToFloat(outlineWeight.text),
                        customOutlineColor.color,
                        Mathf.Clamp(NumberConvertionHelper.ConvertToFloat(outlineOpacity.text), 0, 1),
                        customFillColor.color,
                        Mathf.Clamp(NumberConvertionHelper.ConvertToFloat(fillOpacity.text), 0, 1)
                    );
                    break;
                case DrawingType.ELLIPSE:

                    break;
                case DrawingType.ARROW:
                    PlotDrawingControllerHelper.ApplyChangedData(
                        selectedDrawing,
                        NumberConvertionHelper.ConvertToFloat(outlineWeight.text),
                        customOutlineColor.color,
                        Mathf.Clamp(NumberConvertionHelper.ConvertToFloat(outlineOpacity.text), 0, 1),
                        customFillColor.color,
                        Mathf.Clamp(NumberConvertionHelper.ConvertToFloat(fillOpacity.text), 0, 1)
                    );
                    break;
                case DrawingType.MANOUVER:

                    break;
            }

            RefreshSegmentSelectorColor();
        }

        public void ChangeDrawingMode(DrawingType _mode, bool togglePlacement = true, string message = null)
        {
            mode = _mode;
            addVertex.isOn = togglePlacement;

            ScenarioEditorController.instance.mode = MenuMode.DRAWING;
            EntityPlacementController.instance.ToggleEntityPlacement(true, togglePlacement, message);

            OnPropertyReset();
        }

        public void ChangeDrawingMode(DrawingArrowType _arrow = DrawingArrowType.UNDEFINED, bool togglePlacement = true, string message = null)
        {
            if (_arrow == DrawingArrowType.UNDEFINED) return;

            mode = DrawingType.ARROW;
            arrowType = _arrow;

            ScenarioEditorController.instance.mode = MenuMode.DRAWING;
            EntityPlacementController.instance.ToggleEntityPlacement(togglePlacement);

            OnPropertyReset();
        }

        public void ChangeDrawingMode(DrawingManouverType _manouver = DrawingManouverType.UNDEFINED, bool togglePlacement = true, string message = null)
        {
            if (_manouver == DrawingManouverType.UNDEFINED) return;

            mode = DrawingType.MANOUVER;
            manouverType = _manouver;

            ScenarioEditorController.instance.mode = MenuMode.DRAWING;
            EntityPlacementController.instance.ToggleEntityPlacement(togglePlacement);

            OnPropertyReset();
        }

        public void ToggleAddVertex(bool _state)
        {
            EntityPlacementController.instance.ToggleEntityPlacement(true, _state, (_state) ? null : "");

            if (!_state)
            {
                if (tempPredictor != null) Destroy(tempPredictor.gameObject);
                tempPredictor = null;
            }
        }

        private void OnPropertyReset()
        {
            foreach(Transform vertex in vertexContainer)
            {
                Destroy(vertex.gameObject);
            }

            outlineGroup.SetActive(true);

            if(mode != DrawingType.POLYLINE)
            {
                fillGroup.SetActive(true);
            }

            if(mode == DrawingType.RECTANGLE)
            {
                rectGroup.SetActive(true);
            }else if(mode == DrawingType.CIRCLE)
            {
                circleGroup.SetActive(true);
            }
            else
            {
                vertexGroup.gameObject.SetActive(true);
                addVertex.gameObject.SetActive(true);
            }

            moveDrawing.gameObject.SetActive(true);
        }
        #endregion

        

        #region OBJEK DRAWING HANDLE
        public async void CreateNewDrawing(ArcGISPoint location)
        {
            Debug.Log(mode);

            switch (mode)
            {
                case DrawingType.POLYLINE: CreateNewPolyline(location); break;
                case DrawingType.POLYGON: CreateNewPolygon(location); break;
                case DrawingType.RECTANGLE: CreateNewRectangle(location); break;
                case DrawingType.CIRCLE: CreateNewCircle(location); break;
                case DrawingType.ELLIPSE: break;
                case DrawingType.ARROW:
                    Debug.Log(arrowType);
                    if (arrowType == DrawingArrowType.ARROW)
                    {
                        CreateNewArrow(location);
                    }
                    else if(arrowType == DrawingArrowType.ARROW_UP)
                    {
                        CreateNewArrowUp(location);
                    }else if(arrowType == DrawingArrowType.ARROW_DOWN)
                    {

                    }
                    break;
                case DrawingType.MANOUVER: break;
            }
        }

        /// <summary>
        /// Create New Polyline / New Polyline Segment
        /// </summary>
        /// <param name="location"></param>
        private async void CreateNewPolyline(ArcGISPoint location)
        {
            // Create New Drawing Parent IF It's Plotted for the first time (CREATE MODE)
            if(selectedDrawing == null)
            {
                var id = GenerateIDDrawing();
                if (id == null) return;

                var OBJ    = Instantiate(SpawnDrawingParentByType(), EnvironmentsController.GetMapComponent().transform);
                OBJ.name   = id;
                OBJ.tag    = "entity";

                var color = DrawingToolsController.instance.outlineColor;
                color.a = 1f;

                var objPolyline         = OBJ.GetComponent<Polyline>();
                objPolyline.Color       = color;
                objPolyline.Thickness   = 5;

                selectedDrawing = OBJ.AddComponent<ObjekDrawing>();

                selectedDrawing.userID      = SessionUser.id.GetValueOrDefault(-99);
                selectedDrawing.documentID  = NumberConvertionHelper.ConvertToLong(SkenarioAktif.ID_DOCUMENT);
                selectedDrawing.nama        = id;
                selectedDrawing.type        = DrawingType.POLYLINE;

                // Configure New ObjekDrawing Properties
                var property =
                    CreateNewDrawingProperty(
                        id,
                        true,
                        objPolyline.Color,
                        objPolyline.Thickness,
                        1f,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        "leaflet-path-draggable",
                        null,
                        "round"
                    );

                selectedDrawing.properties = property;

                //// Set Form Fillable From New ObjekDrawing Data
                //outlineWeight.text = property.weight.ToString();
                //outlineOpacity.text = "1";
                //SyncSelectedColor("outline");

                // Add New ObjekDrawing into ENTITY LIST
                EntityController.instance.DRAWING.Add(selectedDrawing);
                addVertex.isOn = true;
            }

            FillFormData();

            var segment = CreateNewSegment(selectedDrawing.transform, location);

            var segmentEditor = segment.GetComponent<ExposeToEditor>();
            var objDrawPolyline = selectedDrawing.GetComponent<ObjekDrawPolyline>();

            //segmentEditor.Selected.AddListener(delegate { objDrawPolyline.isOnEdit = true; });
            //segmentEditor.Unselected.AddListener(delegate { objDrawPolyline.isOnEdit = false; });

            var selected = selectedDrawing;
            segmentEditor.Selected.AddListener(delegate {
                if (!DrawingToolsController.instance.isOn) return;

                Reset();
                ChangeDrawingMode(selected.type, false, "");
                AddVertexData(selected);

                selectedDrawing = selected;
                FillFormData();
            });
            //segmentEditor.Unselected.AddListener(delegate { removeAllListeners(); });
            await Task.Delay(10);

            var currentIndex = (selectedDrawing.transform.childCount - 1);

            var polyline = selectedDrawing.GetComponent<Polyline>();
            polyline.Closed = false;
            polyline.meshOutOfDate = true;

            segment.name = "SEGMENT_" + currentIndex;

            if ((currentIndex <= 0))
            {
                polyline.SetPointPosition(currentIndex, segment.transform.position);

                polyline.Color      = selectedDrawing.properties.warna;
                polyline.Thickness  = selectedDrawing.properties.weight;
            }
            else
            {
                polyline.AddPoint(segment.transform.position);
            }

            segmentEditor.GetComponent<Cuboid>().Color = selectedDrawing.properties.warna;
        }

        private async void CreateNewArrow(ArcGISPoint location)
        {
            // Create New Drawing Parent IF It's Plotted for the first time (CREATE MODE)
            if (selectedDrawing == null)
            {
                var id = GenerateIDDrawing();
                if (id == null) return;

                var OBJ = Instantiate(SpawnDrawingParentByType(), EnvironmentsController.GetMapComponent().transform);
                OBJ.name = id;
                OBJ.tag = "entity";

                var outlineColor = DrawingToolsController.instance.outlineColor;
                outlineColor.a = 1f;

                var fillColor = DrawingToolsController.instance.fillColor;
                fillColor.a = 0.7f;

                var objArrow = OBJ.GetComponent<ObjekDrawArrow>();
                objArrow.outline.Color = outlineColor;
                objArrow.outline.Thickness = 5;
                objArrow.polygon.Color = fillColor;

                objArrow.outline.points = new List<PolylinePoint>();
                objArrow.polygon.points = new List<Vector2>();

                selectedDrawing = OBJ.AddComponent<ObjekDrawing>();

                selectedDrawing.userID = SessionUser.id.GetValueOrDefault(-99);
                selectedDrawing.documentID = NumberConvertionHelper.ConvertToLong(SkenarioAktif.ID_DOCUMENT);
                selectedDrawing.name = OBJ.name;
                selectedDrawing.type = DrawingType.ARROW;

                // Configure New ObjekDrawing Properties
                var property = CreateNewDrawingProperty(
                        id,
                        true,
                        objArrow.outline.Color,
                        objArrow.outline.Thickness,
                        objArrow.outline.Color.a,
                        true,
                        objArrow.polygon.Color,
                        objArrow.polygon.Color.a,
                        null,
                        null,
                        null,
                        true,
                        "leaflet-path-draggable",
                        true,
                        null
                    );

                selectedDrawing.properties = property;

                // Add New ObjekDrawing into ENTITY LIST
                EntityController.instance.DRAWING.Add(selectedDrawing);
                addVertex.isOn = true;
            }

            FillFormData();

            var objDrawArrow = selectedDrawing.GetComponent<ObjekDrawArrow>();
            var segment = CreateNewSegment(objDrawArrow.segments.transform, location);

            var segmentEditor = segment.GetComponent<ExposeToEditor>();

            var selected = selectedDrawing;
            segmentEditor.Selected.AddListener(delegate {
                if (!DrawingToolsController.instance.isOn) return;

                Reset();
                ChangeDrawingMode(selected.type, false, "");
                AddVertexData(selected);

                selectedDrawing = selected;
                FillFormData();
            });
            await Task.Delay(10);

            var currentIndex = objDrawArrow.segments.childCount;

            objDrawArrow.outline.Closed = true;
            objDrawArrow.outline.meshOutOfDate = true;

            objDrawArrow.polygon.meshOutOfDate = true;

            segment.name = "SEGMENT_" + currentIndex;

            if (currentIndex >= 2)
            {
                segment.transform.SetY(selectedDrawing.geometry[0].transform.position.y);

                var arrowLeft = CreateNewSegment(objDrawArrow.segments.transform, location);
                var arrowLeftEnd = CreateNewSegment(objDrawArrow.segments.transform, location);
                var segmentLeftEnd = CreateNewSegment(objDrawArrow.segments.transform, location);
                //var segmentEnd = CreateNewSegment(objDrawArrow.segments.transform, location);
                var segmentRightEnd = CreateNewSegment(objDrawArrow.segments.transform, location);
                var arrowRightEnd = CreateNewSegment(objDrawArrow.segments.transform, location);
                var arrowRight = CreateNewSegment(objDrawArrow.segments.transform, location);
                var segmentMiddle = CreateNewSegment(objDrawArrow.segments.transform, location);

                await Task.Delay(10);

                arrowLeft.GetComponent<ExposeToEditor>().Selected.AddListener(delegate {
                    if (!DrawingToolsController.instance.isOn) return;

                    objDrawArrow.OnWidthEdit = true;

                    Reset();
                    ChangeDrawingMode(selected.type, false, "");

                    selectedDrawing = selected;
                    FillFormData();
                });

                arrowLeft.GetComponent<ExposeToEditor>().Unselected.AddListener(delegate {
                    objDrawArrow.OnWidthEdit = false;
                    if (selectedDrawing == null) DrawingToolsController.instance.arrowMenu.SetOff();
                });

                arrowLeft.transform.position = tempPredictor.segments[1].position;
                arrowLeftEnd.transform.position = tempPredictor.segments[2].position;
                segmentLeftEnd.transform.position = tempPredictor.segments[3].position;
                //segmentEnd.transform.position = tempPredictor.segments[4].position;
                segmentRightEnd.transform.position = tempPredictor.segments[5].position;
                arrowRightEnd.transform.position = tempPredictor.segments[7].position;
                arrowRight.transform.position = tempPredictor.segments[6].position;

                segmentMiddle.transform.position = tempPredictor.segments[9].position;

                objDrawArrow.outline.AddPoint(arrowLeft.transform.position);
                objDrawArrow.outline.AddPoint(arrowLeftEnd.transform.position);
                objDrawArrow.outline.AddPoint(segmentLeftEnd.transform.position);
                //objDrawArrow.outline.AddPoint(segmentEnd.transform.position);
                objDrawArrow.outline.AddPoint(segmentRightEnd.transform.position);
                objDrawArrow.outline.AddPoint(arrowRightEnd.transform.position);
                objDrawArrow.outline.AddPoint(arrowRight.transform.position);

                objDrawArrow.polygon.AddPoint(new Vector2(arrowLeft.transform.position.x, arrowLeft.transform.position.z));
                objDrawArrow.polygon.AddPoint(new Vector2(arrowLeftEnd.transform.position.x, arrowLeftEnd.transform.position.z));
                objDrawArrow.polygon.AddPoint(new Vector2(segmentLeftEnd.transform.position.x, segmentLeftEnd.transform.position.z));
                //objDrawArrow.polygon.AddPoint(new Vector2(segmentEnd.transform.position.x, segmentEnd.transform.position.z));
                objDrawArrow.polygon.AddPoint(new Vector2(segmentRightEnd.transform.position.x, segmentRightEnd.transform.position.z));
                objDrawArrow.polygon.AddPoint(new Vector2(arrowRightEnd.transform.position.x, arrowRightEnd.transform.position.z));
                objDrawArrow.polygon.AddPoint(new Vector2(arrowRight.transform.position.x, arrowRight.transform.position.z));

                selectedDrawing.geometry.Add(arrowLeft);
                selectedDrawing.geometry.Add(arrowLeftEnd);
                selectedDrawing.geometry.Add(segmentLeftEnd);
                //selectedDrawing.geometry.Add(segmentEnd);
                selectedDrawing.geometry.Add(segmentRightEnd);
                selectedDrawing.geometry.Add(arrowRightEnd);
                selectedDrawing.geometry.Add(arrowRight);

                Destroy(segmentMiddle.GetComponent<Cuboid>());
                Destroy(segmentMiddle.GetComponent<SphereCollider>());
                Destroy(segmentMiddle.GetComponent<HUDNavigationElement>());

                Destroy(arrowLeftEnd.GetComponent<Cuboid>());
                Destroy(arrowLeftEnd.GetComponent<SphereCollider>());
                Destroy(arrowLeftEnd.GetComponent<HUDNavigationElement>());

                Destroy(arrowRight.GetComponent<Cuboid>());
                Destroy(arrowRight.GetComponent<SphereCollider>());
                Destroy(arrowRight.GetComponent<HUDNavigationElement>());

                Destroy(arrowRightEnd.GetComponent<Cuboid>());
                Destroy(arrowRightEnd.GetComponent<SphereCollider>());
                Destroy(arrowRightEnd.GetComponent<HUDNavigationElement>());

                Destroy(segmentLeftEnd.GetComponent<Cuboid>());
                Destroy(segmentLeftEnd.GetComponent<SphereCollider>());
                Destroy(segmentLeftEnd.GetComponent<HUDNavigationElement>());

                Destroy(segmentRightEnd.GetComponent<Cuboid>());
                Destroy(segmentRightEnd.GetComponent<SphereCollider>());
                Destroy(segmentRightEnd.GetComponent<HUDNavigationElement>());

                //Destroy(segmentEnd);

                ToggleAddVertex(false);
                DrawingToolsController.instance.rectangleMenu.SetOff();
            }
            else
            {
                //ToggleAddVertex(false);

                objDrawArrow.outline.AddPoint(segment.transform.position);
                objDrawArrow.polygon.AddPoint(new Vector2(segment.transform.position.x, segment.transform.position.z));

                selectedDrawing.geometry.Add(segment);

                objDrawArrow.polygon.transform.SetY(segment.transform.position.y);
            }

            segmentEditor.GetComponent<Cuboid>().Color = selectedDrawing.properties.warna;
        }

        private async void CreateNewArrowUp(ArcGISPoint location)
        {
            // Create New Drawing Parent IF It's Plotted for the first time (CREATE MODE)
            if (selectedDrawing == null)
            {
                var id = GenerateIDDrawing();
                if (id == null) return;

                var OBJ = Instantiate(SpawnDrawingParentByType(), EnvironmentsController.GetMapComponent().transform);
                OBJ.name = id;
                OBJ.tag = "entity";

                var outlineColor = DrawingToolsController.instance.outlineColor;
                outlineColor.a = 1f;

                var fillColor = DrawingToolsController.instance.fillColor;
                fillColor.a = 0.7f;

                var objArrow = OBJ.GetComponent<ObjekDrawArrowCurved>();
                objArrow.outline.Color = outlineColor;
                objArrow.outline.Thickness = 5;
                objArrow.polygon.Color = fillColor;

                objArrow.outline.points = new List<PolylinePoint>();
                objArrow.polygon.points = new List<Vector2>();

                selectedDrawing = OBJ.AddComponent<ObjekDrawing>();

                selectedDrawing.userID = SessionUser.id.GetValueOrDefault(-99);
                selectedDrawing.documentID = NumberConvertionHelper.ConvertToLong(SkenarioAktif.ID_DOCUMENT);
                selectedDrawing.name = OBJ.name;
                selectedDrawing.type = DrawingType.ARROW;
                selectedDrawing.arrowType = DrawingArrowType.ARROW_UP;

                // Configure New ObjekDrawing Properties
                var property = CreateNewDrawingProperty(
                        id,
                        true,
                        objArrow.outline.Color,
                        objArrow.outline.Thickness,
                        objArrow.outline.Color.a,
                        true,
                        objArrow.polygon.Color,
                        objArrow.polygon.Color.a,
                        null,
                        null,
                        null,
                        true,
                        "leaflet-path-draggable",
                        true,
                        null
                    );

                selectedDrawing.properties = property;

                // Add New ObjekDrawing into ENTITY LIST
                EntityController.instance.DRAWING.Add(selectedDrawing);
                addVertex.isOn = true;
            }

            FillFormData();

            var objDrawArrow = selectedDrawing.GetComponent<ObjekDrawArrowCurved>();
            var segment = CreateNewSegment(objDrawArrow.segments.transform, location);

            var segmentEditor = segment.GetComponent<ExposeToEditor>();

            var selected = selectedDrawing;
            segmentEditor.Selected.AddListener(delegate {
                if (!DrawingToolsController.instance.isOn) return;

                Reset();
                ChangeDrawingMode(selected.type, false, "");
                AddVertexData(selected);

                selectedDrawing = selected;
                FillFormData();
            });
            await Task.Delay(10);

            var currentIndex = objDrawArrow.segments.childCount;

            objDrawArrow.outline.Closed = true;
            objDrawArrow.outline.meshOutOfDate = true;

            objDrawArrow.polygon.meshOutOfDate = true;

            segment.name = "SEGMENT_" + currentIndex;

            if (currentIndex >= 2)
            {
                segment.transform.SetY(selectedDrawing.geometry[0].transform.position.y);

                var arrowLeft = CreateNewSegment(objDrawArrow.segments.transform, location);
                var arrowLeftEnd = CreateNewSegment(objDrawArrow.segments.transform, location);
                var segmentLeftEnd = CreateNewSegment(objDrawArrow.segments.transform, location);
                //var segmentEnd = CreateNewSegment(objDrawArrow.segments.transform, location);
                var segmentRightEnd = CreateNewSegment(objDrawArrow.segments.transform, location);
                var arrowRightEnd = CreateNewSegment(objDrawArrow.segments.transform, location);
                var arrowRight = CreateNewSegment(objDrawArrow.segments.transform, location);
                var segmentMiddle = CreateNewSegment(objDrawArrow.segments.transform, location);

                await Task.Delay(10);

                ToggleAddVertex(false);
                DrawingToolsController.instance.rectangleMenu.SetOff();
            }
            else
            {
                objDrawArrow.outline.AddPoint(segment.transform.position);
                objDrawArrow.polygon.AddPoint(new Vector2(segment.transform.position.x, segment.transform.position.z));

                selectedDrawing.geometry.Add(segment);

                objDrawArrow.polygon.transform.SetY(segment.transform.position.y);
            }

            segmentEditor.GetComponent<Cuboid>().Color = selectedDrawing.properties.warna;
        }

        /// <summary>
        /// Create New Polygon / New Polygon Segment
        /// </summary>
        /// <param name="location"></param>
        private async void CreateNewPolygon(ArcGISPoint location)
        {
            // Create New Drawing Parent IF It's Plotted for the first time (CREATE MODE)
            if (selectedDrawing == null)
            {
                var id = GenerateIDDrawing();
                if (id == null) return;

                var OBJ = Instantiate(SpawnDrawingParentByType(), EnvironmentsController.GetMapComponent().transform);
                OBJ.name = id;
                OBJ.tag = "entity";

                var outlineColor    = DrawingToolsController.instance.outlineColor;
                outlineColor.a      = 1f;

                var fillColor       = DrawingToolsController.instance.fillColor;
                fillColor.a         = 0.7f;

                var objPolygon = OBJ.GetComponent<ObjekDrawPolygon>();
                objPolygon.outline.Color = outlineColor;
                objPolygon.outline.Thickness = 5;
                objPolygon.polygon.Color = fillColor;

                selectedDrawing = OBJ.AddComponent<ObjekDrawing>();

                selectedDrawing.userID = SessionUser.id.GetValueOrDefault(-99);
                selectedDrawing.documentID = NumberConvertionHelper.ConvertToLong(SkenarioAktif.ID_DOCUMENT);
                selectedDrawing.name = OBJ.name;
                selectedDrawing.type = DrawingType.POLYGON;

                // Configure New ObjekDrawing Properties
                var property = CreateNewDrawingProperty(
                        id,
                        true,
                        objPolygon.outline.Color,
                        objPolygon.outline.Thickness,
                        objPolygon.outline.Color.a,
                        true,
                        objPolygon.polygon.Color,
                        objPolygon.polygon.Color.a,
                        null,
                        null,
                        null,
                        true,
                        "leaflet-path-draggable",
                        true,
                        null
                    );

                selectedDrawing.properties = property;

                // Set Form Fillable From New ObjekDrawing Data
                //outlineWeight.text      = property.weight.ToString();
                //outlineOpacity.text     = property.opacity.ToString();
                //fillOpacity.text        = property.fillOpacity.ToString();
                //SyncSelectedColor("outline");
                //SyncSelectedColor("fill");

                // Add New ObjekDrawing into ENTITY LIST
                EntityController.instance.DRAWING.Add(selectedDrawing);
                addVertex.isOn = true;
            }

            FillFormData();

            var objDrawPolygon = selectedDrawing.GetComponent<ObjekDrawPolygon>();
            var segment = CreateNewSegment(objDrawPolygon.segments.transform, location);

            var segmentEditor = segment.GetComponent<ExposeToEditor>();

            var selected = selectedDrawing;
            segmentEditor.Selected.AddListener(delegate {
                if (!DrawingToolsController.instance.isOn) return;

                Reset();
                ChangeDrawingMode(selected.type, false, "");
                AddVertexData(selected);

                selectedDrawing = selected;
                FillFormData();
            });
            await Task.Delay(10);

            var currentIndex = (objDrawPolygon.segments.transform.childCount - 1);

            objDrawPolygon.outline.Closed = true;
            objDrawPolygon.outline.meshOutOfDate = true;

            objDrawPolygon.polygon.meshOutOfDate = true;

            segment.name = "SEGMENT_" + currentIndex;

            if ((currentIndex <= 0))
            {
                objDrawPolygon.outline.SetPointPosition(currentIndex, segment.transform.position);
                objDrawPolygon.polygon.SetPointPosition(currentIndex, new Vector2(segment.transform.position.x, segment.transform.position.z));

                objDrawPolygon.outline.Color = selectedDrawing.properties.warna;
                objDrawPolygon.outline.Thickness = selectedDrawing.properties.weight;

                objDrawPolygon.polygon.Color = selectedDrawing.properties.fillColor;
            }
            else
            {
                objDrawPolygon.outline.AddPoint(segment.transform.position);
                objDrawPolygon.polygon.AddPoint(new Vector2(segment.transform.position.x, segment.transform.position.z));
            }

            segmentEditor.GetComponent<Cuboid>().Color = selectedDrawing.properties.warna;
        }

        private async void CreateNewRectangle(ArcGISPoint location)
        {
            // Create New Drawing Parent IF It's Plotted for the first time (CREATE MODE)
            if (selectedDrawing == null)
            {
                var id = GenerateIDDrawing();
                if (id == null) return;

                var OBJ = Instantiate(SpawnDrawingParentByType(), EnvironmentsController.GetMapComponent().transform);
                OBJ.name = id;
                OBJ.tag = "entity";

                var outlineColor    = DrawingToolsController.instance.outlineColor;
                outlineColor.a      = 1f;

                var fillColor       = DrawingToolsController.instance.fillColor;
                fillColor.a         = 0.7f;

                var objRectangle = OBJ.GetComponent<ObjekDrawRectangle>();
                objRectangle.outline.Color          = outlineColor;
                objRectangle.outline.Thickness      = 5;
                objRectangle.polygon.Color          = fillColor;

                selectedDrawing = OBJ.AddComponent<ObjekDrawing>();

                selectedDrawing.userID = SessionUser.id.GetValueOrDefault(-99);
                selectedDrawing.documentID = NumberConvertionHelper.ConvertToLong(SkenarioAktif.ID_DOCUMENT);
                selectedDrawing.name = OBJ.name;
                selectedDrawing.type = DrawingType.RECTANGLE;

                // Configure New ObjekDrawing Properties
                var property = CreateNewDrawingProperty(
                        id,
                        true,
                        objRectangle.outline.Color,
                        objRectangle.outline.Thickness,
                        objRectangle.outline.Color.a,
                        true,
                        objRectangle.polygon.Color,
                        objRectangle.polygon.Color.a,
                        null,
                        null,
                        null,
                        true,
                        "leaflet-path-draggable",
                        true,
                        null
                    );

                selectedDrawing.properties = property;

                // Add New ObjekDrawing into ENTITY LIST
                EntityController.instance.DRAWING.Add(selectedDrawing);
                //btnAddVertex.isOn = true;
            }

            FillFormData();

            var objDrawRectangle = selectedDrawing.GetComponent<ObjekDrawRectangle>();

            if(selectedDrawing.geometry.Count <= 2)
            {
                var segment = CreateNewSegment(objDrawRectangle.segments.transform, location);
                var segmentEditor = segment.GetComponent<ExposeToEditor>();

                var currentIndex = (objDrawRectangle.segments.transform.childCount - 1);
                currentIndex = (currentIndex == 0) ? 0 : 2;

                var selected = selectedDrawing;
                segmentEditor.Selected.AddListener(delegate {
                    if (!DrawingToolsController.instance.isOn) return;

                    Reset();
                    ChangeDrawingMode(selected.type, false, "");
                    //AddVertexData(selected);

                    selected.GetComponent<ObjekDrawRectangle>().selectedIndex = currentIndex;

                    Debug.Log(currentIndex);

                    selectedDrawing = selected;
                    FillFormData();
                });

                segmentEditor.GetComponent<Cuboid>().Color = selectedDrawing.properties.warna;
                await Task.Delay(10);

                segment.name = "SEGMENT_" + currentIndex;

                if (selectedDrawing.geometry.Count == 2)
                {
                    var segmentSideOne = CreateNewSegment(objDrawRectangle.segments.transform, location);
                    await Task.Delay(10);

                    var segmentSideTwo = CreateNewSegment(objDrawRectangle.segments.transform, location);
                    await Task.Delay(10);

                    segmentSideOne.name = "SEGMENT_1";
                    segmentSideTwo.name = "SEGMENT_3";

                    var segmentEditorOne = segmentSideOne.GetComponent<ExposeToEditor>();
                    segmentEditorOne.Selected.AddListener(delegate {
                        if (!DrawingToolsController.instance.isOn) return;

                        Reset();
                        ChangeDrawingMode(selected.type, false, "");

                        selected.GetComponent<ObjekDrawRectangle>().selectedIndex = 1;

                        selectedDrawing = selected;
                        FillFormData();
                    });

                    var segmentEditorTwo = segmentSideTwo.GetComponent<ExposeToEditor>();
                    segmentEditorTwo.Selected.AddListener(delegate {
                        if (!DrawingToolsController.instance.isOn) return;

                        Reset();
                        ChangeDrawingMode(selected.type, false, "");

                        selected.GetComponent<ObjekDrawRectangle>().selectedIndex = 3;

                        selectedDrawing = selected;
                        FillFormData();
                    });

                    segmentEditorOne.GetComponent<Cuboid>().Color = selectedDrawing.properties.warna;
                    segmentEditorTwo.GetComponent<Cuboid>().Color = selectedDrawing.properties.warna;

                    selectedDrawing.geometry[1] = segmentSideOne;
                    selectedDrawing.geometry.Add(segment);
                    selectedDrawing.geometry.Add(segmentSideTwo);

                    ToggleAddVertex(false);
                    DrawingToolsController.instance.rectangleMenu.SetOff();
                }

                objDrawRectangle.outline.Closed = true;
                objDrawRectangle.outline.meshOutOfDate = true;
                objDrawRectangle.polygon.meshOutOfDate = true;

                objDrawRectangle.outline.SetPointPosition(currentIndex, segment.transform.position);
                objDrawRectangle.polygon.SetPointPosition(currentIndex, new Vector2(segment.transform.position.x, segment.transform.position.z));

                objDrawRectangle.outline.Color = selectedDrawing.properties.warna;
                objDrawRectangle.outline.Thickness = selectedDrawing.properties.weight;

                objDrawRectangle.polygon.Color = selectedDrawing.properties.fillColor;
            }
            else
            {
                ToggleAddVertex(false);
            }
        }

        private async void CreateNewCircle(ArcGISPoint location)
        {
            // Create New Drawing Parent IF It's Plotted for the first time (CREATE MODE)
            if (selectedDrawing == null)
            {
                var id = GenerateIDDrawing();
                if (id == null) return;

                var OBJ = EntityController.instance.CreateEntity(location, new ArcGISRotation(0,90,0), SpawnDrawingParentByType());
                OBJ.name = id;
                OBJ.tag = "entity";

                var outlineColor    = DrawingToolsController.instance.outlineColor;
                outlineColor.a      = 1f;

                var fillColor       = DrawingToolsController.instance.fillColor;
                fillColor.a         = 0.7f;

                var objCircle                   = OBJ.GetComponent<ObjekDrawCircle>();
                objCircle.outline.Color         = outlineColor;
                objCircle.outline.Thickness     = 5;
                objCircle.fill.Color            = fillColor;

                selectedDrawing = OBJ.AddComponent<ObjekDrawing>();

                selectedDrawing.userID          = SessionUser.id.GetValueOrDefault(-99);
                selectedDrawing.documentID      = NumberConvertionHelper.ConvertToLong(SkenarioAktif.ID_DOCUMENT);
                selectedDrawing.name            = OBJ.name;
                selectedDrawing.type            = DrawingType.CIRCLE;

                // Configure New ObjekDrawing Properties
                var property = CreateNewDrawingProperty(
                        id,
                        true,
                        objCircle.outline.Color,
                        objCircle.outline.Thickness,
                        objCircle.outline.Color.a,
                        true,
                        objCircle.fill.Color,
                        objCircle.fill.Color.a,
                        null,
                        0,
                        null,
                        true,
                        "leaflet-path-draggable",
                        true,
                        null
                    );

                selectedDrawing.properties = property;

                // Add New ObjekDrawing into ENTITY LIST
                EntityController.instance.DRAWING.Add(selectedDrawing);
            }

            FillFormData();

            var objDrawCircle = selectedDrawing.GetComponent<ObjekDrawCircle>();

            if (selectedDrawing.geometry.Count <= 2)
            {
                var segment = CreateNewSegment(objDrawCircle.segments.transform, location, true);
                var segmentEditor = segment.GetComponent<ExposeToEditor>();

                var selected = selectedDrawing;
                segmentEditor.Selected.AddListener(delegate {
                    if (!DrawingToolsController.instance.isOn) return;

                    Reset();
                    ChangeDrawingMode(selected.type, false, "");

                    selectedDrawing = selected;
                    FillFormData();
                });

                segmentEditor.GetComponent<Cuboid>().Color = selectedDrawing.properties.warna;
                await Task.Delay(10);

                var currentIndex = (objDrawCircle.segments.transform.childCount - 1);

                segment.name = "SEGMENT_" + currentIndex;

                if (selectedDrawing.geometry.Count == 2)
                {
                    Destroy(selected.geometry[0].gameObject);
                    selected.geometry.RemoveAt(0);

                    selected.geometry[0].transform.position = tempPredictor.segments[1].position;

                    objDrawCircle.outline.Radius    = Vector3.Distance(selected.transform.position, selected.geometry[0].transform.position);
                    objDrawCircle.fill.Radius       = objDrawCircle.outline.Radius;

                    ToggleAddVertex(false);
                    DrawingToolsController.instance.circleMenu.SetOff();
                }

                objDrawCircle.outline.meshOutOfDate = true;
                objDrawCircle.fill.meshOutOfDate = true;

                objDrawCircle.outline.Color         = selectedDrawing.properties.warna;
                objDrawCircle.outline.Thickness     = selectedDrawing.properties.weight;

                objDrawCircle.fill.Color            = selectedDrawing.properties.fillColor;
            }
            else
            {
                ToggleAddVertex(false);
            }
        }

        

        private EntityDrawingProperty CreateNewDrawingProperty(string _id_point, bool? _stroke = null, Color ? _warna = null, float? _weight = null, float? _opacity = null, bool? _fill = null, Color? _fillColor = null, float? _fillOpacity = null, bool? _showArea = null, double? _radius = null, DrawingArrowType? _arrowType = null, bool? _interactive = null, string _className = null, bool? _draggable = null, string _lineJoin = null)
        {
            var property            = new EntityDrawingProperty();

            if(_stroke.HasValue && _stroke == true)
            {
                property.stroke     = true;
                property.warna      = _warna.GetValueOrDefault(EntityHelper.getColorByUserType());
                property.weight     = _weight.GetValueOrDefault(5);
                property.opacity    = _opacity.GetValueOrDefault(0.7f);
            }

            if(_fill.HasValue && _fill == true)
            {
                property.fill           = true;
                property.fillColor      = _fillColor.GetValueOrDefault(EntityHelper.getColorByUserType());
                property.fillOpacity    = _fillOpacity.GetValueOrDefault(0.2f);
            }

            if (_showArea.HasValue)
            {
                property.showArea   = _showArea.Value;
            }

            if (_draggable.HasValue)
            {
                property.draggable = _draggable.GetValueOrDefault(false);
            }

            if (_radius.HasValue)
            {
                property.radius     = _radius.GetValueOrDefault(0);
            }

            if (_arrowType.HasValue)
            {
                property.arrowType  = _arrowType.GetValueOrDefault(DrawingArrowType.UNDEFINED);
            }

            if (_interactive.HasValue)
            {
                property.interactive    = _interactive.GetValueOrDefault(true);
            }

            if (_className != null)
            {
                property.className = (_className != "") ? _className : "leaflet-path-draggable";
            }
            
            property.clickable      = true;
            property.dashArray      = "0,0";

            if(_lineJoin != null)
            {
                property.lineJoin   = (_lineJoin != "") ? _lineJoin : "round";
            }

            property.transform      = true;
            property.id_point       = _id_point;

            return property;
        }

        private GameObject CreateNewSegment(Transform _parent, ArcGISPoint _location, bool noLocationComponent = false)
        {
            // Create New Segment Object
            var segment = 
                (noLocationComponent)?
                Instantiate(AssetPackageController.Instance.prefabSegmentSelector.gameObject, _parent) :
                EntityController.instance.CreateEntity(
                    _location, new Vector3(0, 90, 0), AssetPackageController.Instance.prefabSegmentSelector.gameObject, _parent
                );

            if (_parent.GetComponent<ObjekDrawing>() == null)
            {
                _parent.GetComponentInParent<ObjekDrawing>().geometry.Add(segment.gameObject);
            }
            else
            {
                _parent.GetComponent<ObjekDrawing>().geometry.Add(segment.gameObject);
            }

            // Create New Segment's HUDElement
            var HUD = segment.GetComponent<HUDNavigationElement>();
            HUD.Prefabs.IndicatorPrefab = AssetPackageController.Instance.prefabIndicator;
            HUD.OnElementReady.AddListener(delegate { onHUDReady(HUD, (_parent.childCount - 1).ToString()); });

            // Create List In UI
            var list = Instantiate(prefabVertexList, vertexContainer).GetComponent<Metadata>();

            list.FindParameter("label-index").GetComponent<TextMeshProUGUI>().text = (_parent.childCount - 1).ToString();
            list.FindParameter("label-coordinate").GetComponent<TextMeshProUGUI>().text = _location.X + ", " + _location.Y;
            list.FindParameter("btn-remove").GetComponent<Button>().onClick.AddListener(delegate { RemoveSegmentAtIndex(list.gameObject); });

            return segment;
        }

        private void onHUDReady(HUDNavigationElement _HUD, string _nama)
        {
            _HUD.Indicator.ChangeLabelEntity(_nama, Color.black);
        }

        public void DeleteDrawing()
        {
            if (selectedDrawing == null) return;

            EntityController.instance.DRAWING.Remove(selectedDrawing);
            trashData.Add(new ObjekTrash { value = selectedDrawing.nama, key = "polygon" });

            Destroy(selectedDrawing.gameObject);

            EntityPlacementController.instance.ToggleEntityPlacement(false);

            GetComponent<Animator>().Play("SlideOut");
        }

        public async void RemoveSegmentAtIndex(GameObject list)
        {
            if (selectedDrawing == null) return;
            if (selectedDrawing.geometry.Count <= 1) return;

            int index = 0;
            for(int i=0; i < vertexContainer.transform.childCount; i++)
            {
                if (vertexContainer.GetChild(i).gameObject == list.gameObject)
                {
                    index = i;
                    break;
                }
            }

            switch (mode)
            {
                case DrawingType.POLYLINE:
                    selectedDrawing.GetComponent<ObjekDrawPolyline>().polyline.points.RemoveAt(index);
                    break;
                case DrawingType.POLYGON:
                    selectedDrawing.GetComponent<ObjekDrawPolygon>().outline.points.RemoveAt(index);
                    selectedDrawing.GetComponent<ObjekDrawPolygon>().polygon.points.RemoveAt(index);
                    break;
            }

            Destroy(selectedDrawing.geometry[index].gameObject);
            selectedDrawing.geometry.RemoveAt(index);

            Destroy(vertexContainer.GetChild(index).gameObject);

            await Task.Delay(10);
            RefreshSegmentHUDFromIndex(index);
        }

        private void RefreshSegmentHUDFromIndex(int index)
        {
            for(int i=index; i < selectedDrawing.geometry.Count; i++)
            {
                selectedDrawing.geometry[i].GetComponent<HUDNavigationElement>().Indicator.ChangeLabelEntity(i.ToString());
            }

            for(int i=0; i < vertexContainer.childCount; i++)
            {
                vertexContainer.GetChild(i).GetComponent<Metadata>().FindParameter("label-index").GetComponent<TextMeshProUGUI>().text = i.ToString();
            }
        }

        public void AddVertexData(ObjekDrawing selected)
        {
            for(int i=0; i < selected.geometry.Count; i++)
            {
                int index = i;
                var _location = selected.geometry[i].GetComponent<ArcGISLocationComponent>().Position;

                var list = Instantiate(prefabVertexList, vertexContainer).GetComponent<Metadata>();
                list.FindParameter("label-index").GetComponent<TextMeshProUGUI>().text = index.ToString();
                list.FindParameter("label-coordinate").GetComponent<TextMeshProUGUI>().text = _location.X + ", " + _location.Y;
                list.FindParameter("btn-remove").GetComponent<Button>().onClick.AddListener(delegate { RemoveSegmentAtIndex(list.gameObject); });
            }
        }

        public string GenerateIDDrawing()
        {
            switch (mode)
            {
                case DrawingType.POLYLINE:
                    return "polyline_" + (EntityController.instance.DRAWING.Count + 1) + "_" + SessionUser.id + "_" + Random.Range(1, 1000);
                case DrawingType.POLYGON:
                    return "polygon_" + (EntityController.instance.DRAWING.Count + 1) + "_" + SessionUser.id + "_" + Random.Range(1, 1000);
                case DrawingType.RECTANGLE:
                    return "rectangle_" + (EntityController.instance.DRAWING.Count + 1) + "_" + SessionUser.id + "_" + Random.Range(1, 1000);
                case DrawingType.CIRCLE:
                    return "circle_" + (EntityController.instance.DRAWING.Count + 1) + "_" + SessionUser.id + "_" + Random.Range(1, 1000);
                case DrawingType.ARROW:
                    return "arrow_" + (EntityController.instance.DRAWING.Count + 1) + "_" + SessionUser.id + "_" + Random.Range(1, 1000);
                default:
                    return null;
            }
        }

        public GameObject SpawnDrawingParentByType()
        {
            switch (mode)
            {
                case DrawingType.POLYLINE:
                    return AssetPackageController.Instance.prefabPolyline.gameObject;
                case DrawingType.POLYGON:
                    return AssetPackageController.Instance.prefabPolygon.gameObject;
                case DrawingType.RECTANGLE:
                    return AssetPackageController.Instance.prefabRectangle.gameObject;
                case DrawingType.CIRCLE:
                    return AssetPackageController.Instance.prefabCircle.gameObject;
                case DrawingType.ARROW:
                    if(arrowType == DrawingArrowType.ARROW_UP || arrowType == DrawingArrowType.ARROW_DOWN)
                    {
                        return AssetPackageController.Instance.prefabArrowCurved.gameObject;
                    }
                    else
                    {
                        return AssetPackageController.Instance.prefabArrow.gameObject;
                    }
                default: return null;
            }
        }
        #endregion
    }

    public class PlotDrawingControllerHelper
    {
        public static void ApplyChangedData(ObjekDrawing _obj, float _outlineWeight, Color _outlineColor, float _outlineOpacity)
        {
            // Don't save alpha value of the color
            _outlineColor.a = 1;

            // Apply Outline Changes
            _obj.properties.weight  = _outlineWeight;
            _obj.properties.warna   = _outlineColor;
            _obj.properties.opacity = _outlineOpacity;

            if (_obj.GetComponent<ObjekDrawPolyline>() == null) return;

            RefreshDrawingObject(_obj.GetComponent<ObjekDrawPolyline>().polyline, _obj.properties);
        }

        public static void ApplyChangedData(ObjekDrawing _obj, float _outlineWeight, Color _outlineColor, float _outlineOpacity, Color _fillColor, float _fillOpacity)
        {
            ApplyChangedData(_obj, _outlineWeight, _outlineColor, _outlineOpacity);

            // Don't save alpha value of the color
            _fillColor.a = 1;

            _obj.properties.fillColor   = _fillColor;
            _obj.properties.fillOpacity = _fillOpacity;

            if (_obj.GetComponent<ObjekDrawPolygon>() != null)
            {
                RefreshDrawingObject(_obj.GetComponent<ObjekDrawPolygon>().outline, _obj.properties);
                RefreshDrawingObject(_obj.GetComponent<ObjekDrawPolygon>().polygon, _obj.properties);
            }else if(_obj.GetComponent<ObjekDrawRectangle>() != null)
            {
                RefreshDrawingObject(_obj.GetComponent<ObjekDrawRectangle>().outline, _obj.properties);
                RefreshDrawingObject(_obj.GetComponent<ObjekDrawRectangle>().polygon, _obj.properties);
            }else if(_obj.GetComponent<ObjekDrawCircle>() != null)
            {
                RefreshDrawingObject(_obj.GetComponent<ObjekDrawCircle>().outline, _obj.properties);
                RefreshDrawingObject(_obj.GetComponent<ObjekDrawCircle>().fill, _obj.properties);
            }else if(_obj.GetComponent<ObjekDrawArrow>() != null)
            {
                RefreshDrawingObject(_obj.GetComponent<ObjekDrawArrow>().outline, _obj.properties);
                RefreshDrawingObject(_obj.GetComponent<ObjekDrawArrow>().polygon, _obj.properties);
            }
        }

        public static void RefreshDrawingObject(Polyline _obj, EntityDrawingProperty _properties)
        {
            if (_obj == null || _properties == null) return;

            var _outlineColor   = _properties.warna;
            _outlineColor.a     = _properties.opacity;

            _obj.Thickness      = _properties.weight;
            _obj.Color          = _outlineColor;
        }

        public static void RefreshDrawingObject(Polygon _obj, EntityDrawingProperty _properties)
        {
            if (_obj == null || _properties == null) return;

            var _FillColor  = _properties.fillColor;
            _FillColor.a    = _properties.fillOpacity;

            _obj.Color      = _FillColor;
        }

        public static void RefreshDrawingObject(Disc _obj, EntityDrawingProperty _properties)
        {
            if (_obj == null || _properties == null) return;

            var _FillColor = _properties.fillColor;
            _FillColor.a = _properties.fillOpacity;

            _obj.Color = _FillColor;
        }
    }
}