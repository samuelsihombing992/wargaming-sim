using System.Collections.Generic;
using UnityEngine;

using Wargaming.Core.ScenarioEditor;
using static Wargaming.Core.GlobalParam.HelperDataPlotting.PlottingHelper;

public class PlottingBase : MonoBehaviour
{
    #region MODAL HANDLE
    public void OpenModal()
    {
        GetComponent<Animator>().Play("SlideIn");
    }
    public void OpenModal(string _anim = null)
    {
        GetComponent<Animator>().Play((_anim != null) ? _anim : "SlideIn");
    }

    public void CloseModal(string _anim = null)
    {
        GetComponent<Animator>().Play((_anim != null) ? _anim : "SlideOut");
        ResetMode();
    }

    public void CloseModal()
    {
        GetComponent<Animator>().Play("SlideOut");
        ResetMode();
    }

    public void ResetMode()
    {
        ScenarioEditorController.instance.mode = MenuMode.NONE;
    }
    #endregion
}