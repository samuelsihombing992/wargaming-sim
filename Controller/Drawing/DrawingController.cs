using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.ArcGISMapsSDK.Utils.Math;
using Esri.GameEngine.Geometry;
using Esri.HPFramework;
using Shapes;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using Unity.VisualScripting;
using UnityEngine;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;
using Wargaming.Core.GlobalParam.HelperDataAlutsista;

public class DrawingController : MonoBehaviour
{
    [Header("References")]
    public Transform drawingContainer;

    public static Transform drawContainer;

    private void Start()
    {
        drawContainer = drawingContainer;
    }

    public static async Task SpawnObjPolyline(ObjekDrawingHelper drawing)
    {
        var newSegments     = new List<GameObject>();

        var properties      = drawing.properties;
        var geometry        = drawing.geometryPolyline.coordinates;

        // Create Segments
        for (int i = 0; i < geometry.Count; i++)
        {
            var segment = EntityController.instance.CreateEntity(
                new ArcGISPoint(geometry[i][0], geometry[i][1], 0, ArcGISSpatialReference.WGS84()),
                new Vector3(0, 90, 0),
                AssetPackageController.Instance.prefabDrawingSegment,
                drawContainer
            ); ;

            segment.name = "SEGMENT_" + i;
            newSegments.Add(segment);
        }

        var OBJ = EntityController.instance.CreateEntity(
            new ArcGISPoint(geometry[0][0], geometry[0][1], 0, ArcGISSpatialReference.WGS84()),
            new Vector3(0,0,0),
            AssetPackageController.Instance.prefabPolyline.gameObject,
            drawContainer
        );

        await Task.Delay(10);

        OBJ.name    = drawing.nama;
        OBJ.tag     = "tools";

        var color       = properties.warna;
        color.a         = properties.opacity;

        var objPolyline         = OBJ.GetComponent<ObjekDrawPolyline>();
        objPolyline.polyline.Color       = color;
        objPolyline.polyline.Thickness   = properties.weight;

        var data = OBJ.GetComponent<ObjekDrawing>();
        SetDrawingGenericData(data, drawing);

        data.properties = properties;

        for (int i = 0; i < newSegments.Count; i++)
        {
            Destroy(newSegments[i].GetComponent<ArcGISLocationComponent>());

            newSegments[i].transform.parent = objPolyline.segments;
            data.geometry.Add(newSegments[i]);

            if (i == 0)
            {
                objPolyline.polyline.SetPointPosition(0, newSegments[i].transform.position);
            }
            else
            {
                objPolyline.polyline.AddPoint(newSegments[i].transform.position);
            }   
        }

        OBJ.GetComponent<ObjekDrawPolyline>().Init();
        objPolyline.polyline.meshOutOfDate = true;
        objPolyline.polyline.Closed = false;

        var timesetWalker = OBJ.AddComponent<TimesetWalker>();
        timesetWalker.Init(objPolyline.transform.GetChild(0).gameObject, drawing.properties.startDate, drawing.properties.endDate);

        EntityController.instance.DRAWING.Add(data);
    }

    public static async Task SpawnObjPolygon(ObjekDrawingHelper drawing)
    {
        var newSegments = new List<GameObject>();

        var properties = drawing.properties;
        var geometry = drawing.geometryPolygon.coordinates;

        for (int i = 0; i < geometry[0].Count; i++)
        {
            var segment = EntityController.instance.CreateEntity(
                new ArcGISPoint(geometry[0][i][0], geometry[0][i][1], 0, ArcGISSpatialReference.WGS84()),
                new Vector3(0, 90, 0),
                AssetPackageController.Instance.prefabDrawingSegment,
                drawContainer
            );

            segment.name = "SEGMENT_" + i;
            newSegments.Add(segment);
        }

        var OBJ = EntityController.instance.CreateEntity(
            new ArcGISPoint(geometry[0][0][0], geometry[0][0][1], 0, ArcGISSpatialReference.WGS84()),
            new Vector3(0, 0, 0),
            AssetPackageController.Instance.prefabPolygon.gameObject,
            drawContainer
        );

        await Task.Delay(10);

        OBJ.name            = drawing.nama;
        OBJ.tag             = "tools";

        var outlineColor    = properties.warna;
        outlineColor.a      = properties.opacity;

        var fillColor       = properties.fillColor;
        fillColor.a         = properties.fillOpacity;

        var objPolygon                  = OBJ.GetComponent<ObjekDrawPolygon>();
        objPolygon.outline.Color        = outlineColor;
        objPolygon.outline.Thickness    = properties.weight;
        objPolygon.polygon.Color        = fillColor;

        var data = OBJ.GetComponent<ObjekDrawing>();
        SetDrawingGenericData(data, drawing);

        data.properties = properties;

        for (int i = 0; i < newSegments.Count; i++)
        {
            Destroy(newSegments[i].GetComponent<ArcGISLocationComponent>());

            newSegments[i].transform.parent = objPolygon.segments;
            data.geometry.Add(newSegments[i]);

            if (i == 0)
            {
                objPolygon.outline.SetPointPosition(0, newSegments[i].transform.position);
                objPolygon.polygon.SetPointPosition(0, newSegments[i].transform.position);
            }
            else
            {
                objPolygon.outline.AddPoint(newSegments[i].transform.position);
                objPolygon.polygon.AddPoint(newSegments[i].transform.position);
            }
        }

        OBJ.GetComponent<ObjekDrawPolygon>().Init();

        objPolygon.outline.meshOutOfDate = true;
        objPolygon.outline.Closed = false;

        objPolygon.polygon.meshOutOfDate = true;

        var timesetWalker = OBJ.AddComponent<TimesetWalker>();
        timesetWalker.Init(objPolygon.transform.GetChild(0).gameObject, drawing.properties.startDate, drawing.properties.endDate);

        EntityController.instance.DRAWING.Add(data);
    }

    public static async Task SpawnObjRectangle(ObjekDrawingHelper drawing)
    {
        var newSegments = new List<GameObject>();

        var properties = drawing.properties;
        var geometry = drawing.geometryPolygon.coordinates;

        for (int i = 0; i < geometry[0].Count; i++)
        {
            var segment = EntityController.instance.CreateEntity(
                new ArcGISPoint(geometry[0][i][0], geometry[0][i][1], 0, ArcGISSpatialReference.WGS84()),
                new Vector3(0, 90, 0),
                AssetPackageController.Instance.prefabDrawingSegment,
                drawContainer
            );

            segment.name = "SEGMENT_" + i;
            newSegments.Add(segment);
        }

        var OBJ = EntityController.instance.CreateEntity(
            new ArcGISPoint(geometry[0][0][0], geometry[0][0][1], 0, ArcGISSpatialReference.WGS84()),
            new Vector3(0, 0, 0),
            AssetPackageController.Instance.prefabPolygon.gameObject,
            drawContainer
        );

        await Task.Delay(10);

        OBJ.name = drawing.nama;
        OBJ.tag = "tools";

        var outlineColor = properties.warna;
        outlineColor.a = properties.opacity;

        var fillColor = properties.fillColor;
        fillColor.a = properties.fillOpacity;

        var objRectangle = OBJ.GetComponent<ObjekDrawRectangle>();
        objRectangle.outline.Color = outlineColor;
        objRectangle.outline.Thickness = properties.weight;
        objRectangle.polygon.Color = fillColor;

        var data = OBJ.GetComponent<ObjekDrawing>();
        SetDrawingGenericData(data, drawing);

        data.properties = properties;

        for (int i = 0; i < newSegments.Count; i++)
        {
            Destroy(newSegments[i].GetComponent<ArcGISLocationComponent>());

            newSegments[i].transform.parent = objRectangle.segments;
            data.geometry.Add(newSegments[i]);

            if (i == 0)
            {
                objRectangle.outline.SetPointPosition(0, newSegments[i].transform.position);
                objRectangle.polygon.SetPointPosition(0, newSegments[i].transform.position);
            }
            else
            {
                objRectangle.outline.AddPoint(newSegments[i].transform.position);
                objRectangle.polygon.AddPoint(newSegments[i].transform.position);
            }
        }

        OBJ.GetComponent<ObjekDrawPolygon>().Init();

        objRectangle.outline.meshOutOfDate = true;
        objRectangle.outline.Closed = false;

        objRectangle.polygon.meshOutOfDate = true;

        var timesetWalker = OBJ.AddComponent<TimesetWalker>();
        timesetWalker.Init(objRectangle.transform.GetChild(0).gameObject, drawing.properties.startDate, drawing.properties.endDate);

        EntityController.instance.DRAWING.Add(data);
    }

    public static async Task SpawnObjCircle(ObjekDrawingHelper drawing)
    {
        var properties = drawing.properties;
        await Task.Delay(10);

        var OBJ = EntityController.instance.CreateEntity(
            new ArcGISPoint(drawing.geometryPoint.coordinates[0], drawing.geometryPoint.coordinates[1], 0, ArcGISSpatialReference.WGS84()),
            new Vector3(0, 90, 0),
            AssetPackageController.Instance.prefabCircle.gameObject,
            drawContainer
        );

        await Task.Delay(10);

        OBJ.name            = drawing.nama;
        OBJ.tag             = "tools";

        var outlineColor    = properties.warna;
        outlineColor.a      = properties.opacity;

        var fillColor       = properties.fillColor;
        fillColor.a         = properties.fillOpacity;

        var objCircle = OBJ.GetComponent<ObjekDrawCircle>();
        objCircle.outline.Color = outlineColor;
        objCircle.outline.Thickness = properties.weight;
        objCircle.fill.Color = fillColor;

        var data = OBJ.GetComponent<ObjekDrawing>();
        SetDrawingGenericData(data, drawing);

        data.properties = properties;

        objCircle.outline.Radius    = (float)properties.radius;
        objCircle.fill.Radius       = (float)properties.radius;

        objCircle.outline.meshOutOfDate = true;
        objCircle.fill.meshOutOfDate    = true;

        var timesetWalker = OBJ.AddComponent<TimesetWalker>();
        timesetWalker.Init(objCircle.transform.GetChild(0).gameObject, drawing.properties.startDate, drawing.properties.endDate);

        EntityController.instance.DRAWING.Add(data);
    }

    private static void SetDrawingGenericData(ObjekDrawing OBJ, ObjekDrawingHelper drawing)
    {
        OBJ.userID      = drawing.id_user.Value;
        OBJ.documentID  = drawing.dokumen.Value;
        OBJ.nama        = drawing.nama;
        OBJ.type        = drawing.type;
        OBJ.geometry    = new List<GameObject>();
    }

    public static GameObject CreateNewDrawSegment(Transform _parent, ArcGISPoint _location, ArcGISRotation _rotation)
    {
        var newSegment = Instantiate(AssetPackageController.Instance.prefabSegmentSelector.gameObject, _parent);

        var locationComp        = newSegment.AddComponent<ArcGISLocationComponent>();
        locationComp.Position   = _location;
        locationComp.Rotation   = _rotation;

        return newSegment;
    }
}
