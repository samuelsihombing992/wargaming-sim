using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;
using Shapes;
using Esri.GameEngine.Geometry;

using Wargaming.Core.ScenarioEditor;
using Wargaming.Core.ScenarioEditor.Drawing;
using Wargaming.Core.GlobalParam.HelperPlotting;
using static Wargaming.Core.GlobalParam.HelperDataPlotting.PlottingHelper;
using static Wargaming.Core.GlobalParam.DrawingHelper;
using Wargaming.Core.GlobalParam;
using Wargaming.Core.GlobalParam.HelperDataAlutsista;
using System.Threading.Tasks;
using Esri.ArcGISMapsSDK.Components;
using Wargaming.Core;
using Battlehub.RTCommon;
using SickscoreGames.HUDNavigationSystem;

namespace Wargaming.Core.ScenarioEditor.Drawing
{
    public class DrawingToolsController : PlottingBase
    {
        public bool isOn = false;

        [Header("Outline Color Group")]
        public MenuToggler outlineNoColor;
        public MenuToggler outlineWhite;
        public MenuToggler outlineRed;
        public MenuToggler outlineGreen;
        public MenuToggler outlineBlue;
        public MenuToggler outlineYellow;
        public MenuToggler outlineMagenta;
        public MenuToggler outlineBlack;

        [Header("Fill Color Group")]
        public MenuToggler fillNoColor;
        public MenuToggler fillWhite;
        public MenuToggler fillRed;
        public MenuToggler fillGreen;
        public MenuToggler fillBlue;
        public MenuToggler fillYellow;
        public MenuToggler fillMagenta;
        public MenuToggler fillBlack;

        [Header("Drawing Menu")]
        public MenuToggler polylineMenu;
        public MenuToggler polygonMenu;
        public MenuToggler rectangleMenu;
        public MenuToggler circleMenu;
        public MenuToggler arrowMenu;
        public MenuToggler arrowDownMenu;
        public MenuToggler arrowUpMenu;
        public MenuToggler manouverDownMenu;
        public MenuToggler manouverUpMenu;

        // TEMP Color
        public Color outlineColor;
        public Color fillColor;

        #region INSTANCE
        public static DrawingToolsController instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private void Start()
        {
            outlineColor    = GetColorByUser();
            fillColor       = GetColorByUser();
                
            AddDrawingListener();
        }

        public void PrepareDrawing(DrawingType mode)
        {
            ResetMode();

            PlotDrawingController.instance.Reset();
            PlotDrawingController.instance.ChangeDrawingMode(mode, true, GetMessageByDrawingType(mode));

            PlotDrawingController.instance.SyncSelectedColor("outline");
            PlotDrawingController.instance.SyncSelectedColor("fill");
        }

        public void PrepareDrawing(DrawingArrowType mode)
        {
            ResetMode();

            PlotDrawingController.instance.Reset();
            PlotDrawingController.instance.ChangeDrawingMode(mode, true, GetMessageByDrawingType(mode));

            PlotDrawingController.instance.SyncSelectedColor("outline");
            PlotDrawingController.instance.SyncSelectedColor("fill");
        }

        public void PrepareDrawing(DrawingManouverType mode)
        {
            ResetMode();

            PlotDrawingController.instance.Reset();
            PlotDrawingController.instance.ChangeDrawingMode(mode, true, GetMessageByDrawingType(mode));

            PlotDrawingController.instance.SyncSelectedColor("outline");
            PlotDrawingController.instance.SyncSelectedColor("fill");
        }

        public void EndDrawing()
        {
            PlotDrawingController.instance.CloseModal();
            PlotDrawingController.instance.Reset();
            ResetMode();

            polylineMenu.GetComponentInParent<ToggleGroup>().SetAllTogglesOff(true);
        }


        private string GetMessageByDrawingType(DrawingType mode)
        {
            if(mode == DrawingType.POLYLINE || mode == DrawingType.POLYGON)
            {
                return "Click on the map to place the segment...\nPress \"ESC\" to finish";
            }
            else
            {
                return "Click on the map and drag to draw the shape...\nPress \"ESC\" to cancel";
            }
        }

        private string GetMessageByDrawingType(DrawingArrowType mode)
        {
            return "Click on the map to place the segment...\nPress \"ESC\" to finish";
        }

        private string GetMessageByDrawingType(DrawingManouverType mode)
        {
            return "Click on the map and drag to draw the shape...\nPress \"ESC\" to cancel";
        }

        public void ToggleDrawingTools(bool state)
        {
            isOn = state;

            for (int i = 0; i < EntityController.instance.DRAWING.Count; i++)
            {
                for (int j = 0; j < EntityController.instance.DRAWING[i].geometry.Count; j++)
                {
                    var geometry = EntityController.instance.DRAWING[i].geometry[j];

                    var exposeToEditor = geometry.GetComponent<ExposeToEditor>();
                    if(exposeToEditor != null)
                    {
                        geometry.GetComponent<ExposeToEditor>().CanTransform = state;
                        geometry.GetComponent<ExposeToEditor>().ShowSelectionGizmo = state;
                    }

                    var hudNavigation = geometry.GetComponent<HUDNavigationElement>();
                    if(hudNavigation != null)
                    {
                        geometry.GetComponent<HUDNavigationElement>().enabled = state;
                    }

                    var selector = geometry.GetComponent<Cuboid>();
                    if(selector != null)
                    {
                        geometry.GetComponent<Cuboid>().enabled = state;
                    }
                }
            }
        }

        #region COLOR PALETTE MANAGER
        public void SetOutlineColor(DataColorPalette data) { outlineColor = data.color; }

        public void SetOutlineColor(Color color) { outlineColor = color; }

        public void SetFillColor(DataColorPalette data) { fillColor = data.color; }

        public void SetFillColor(Color color) { fillColor = color; }
        #endregion

        #region EVENT LISTENERS
        private void AddDrawingListener()
        {
            polylineMenu.onToggleOn.AddListener(delegate { PrepareDrawing(DrawingType.POLYLINE); });
            polygonMenu.onToggleOn.AddListener(delegate { PrepareDrawing(DrawingType.POLYGON); });
            rectangleMenu.onToggleOn.AddListener(delegate { PrepareDrawing(DrawingType.RECTANGLE); });
            circleMenu.onToggleOn.AddListener(delegate { PrepareDrawing(DrawingType.CIRCLE); });
            arrowMenu.onToggleOn.AddListener(delegate { PrepareDrawing(DrawingArrowType.ARROW); });
            arrowDownMenu.onToggleOn.AddListener(delegate { PrepareDrawing(DrawingArrowType.ARROW_DOWN); });
            arrowUpMenu.onToggleOn.AddListener(delegate { PrepareDrawing(DrawingArrowType.ARROW_UP); });
            manouverDownMenu.onToggleOn.AddListener(delegate { PrepareDrawing(DrawingManouverType.MANOUVER_DOWN); });
            manouverUpMenu.onToggleOn.AddListener(delegate { PrepareDrawing(DrawingManouverType.MANOUVER_UP); });

            polylineMenu.onToggleOff.AddListener(delegate { EndDrawing(); });
            polygonMenu.onToggleOff.AddListener(delegate { EndDrawing(); });
            rectangleMenu.onToggleOff.AddListener(delegate { EndDrawing(); });
            circleMenu.onToggleOff.AddListener(delegate { EndDrawing(); });
            arrowMenu.onToggleOff.AddListener(delegate { EndDrawing(); });
            arrowDownMenu.onToggleOff.AddListener(delegate { EndDrawing(); });
            arrowUpMenu.onToggleOff.AddListener(delegate { EndDrawing(); });
            manouverDownMenu.onToggleOff.AddListener(delegate { EndDrawing(); });
            manouverUpMenu.onToggleOff.AddListener(delegate { EndDrawing(); });
        }
        #endregion
    }
}