    using Crest;
using Esri.ArcGISMapsSDK.Components;
using Esri.HPFramework;
using Expanse;
using FluffyUnderware.Curvy.Controllers;
using Shapes;
using SickscoreGames.HUDNavigationSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Wargaming.Core.GlobalParam;
using Wargaming.Core.ScenarioEditor.Drawing;
using WGSBundling.Bundles.Aircraft;
using WGSBundling.Bundles.AircraftEngine;
using WGSBundling.Bundles.Animated;
using WGSBundling.Bundles.CargoHold;
using WGSBundling.Bundles.DynamicWave;
using WGSBundling.Bundles.DynamicWave.Helper;
using WGSBundling.Bundles.HeliRotor;
using WGSBundling.Bundles.Sound;
using WGSBundling.Bundles.Variant;
using WGSBundling.Bundles.VFX;
using WGSBundling.Bundles.WheeledVehicle;
using WGSBundling.Core;
using static Wargaming.Core.GlobalParam.EntityHelper;
using static WGSBundling.Core.Helpers.WGSBundlingHelper;
//using static WGSBundling.Core.WGSBundlingConfig;

public class AssetPackageController : MonoBehaviour
{
    public enum assetPackageLocation { DataPath }

    [Header("Root Walker")]
    public GameObject rootWalker;
    public GameObject lRootWalker;
    public GameObject sRootWalker;
    public GameObject aRootWalker;

    [Header("Default Satuan")]
    public GameObject defaultVehicle;
    public GameObject defaultShip;
    public GameObject defaultAircraft;
    public GameObject defaultHelicopter;
    public GameObject defaultPasukan;

    [Header("Prefab Objects")]
    public GameObject prefabRadar;
    public GameObject prefabRadar2D;
    public GameObject prefabBungus;
    public GameObject prefabSituasi;
    public GameObject prefabLogistikBandara;
    public GameObject prefabLogistikPelabuhan;
    public GameObject prefabLogistikBebas;
    public GameObject prefabText;
    public GameObject prefabObstacle;
    public GameObject prefabIconCustom;
    public GameObject prefabAnimasi;
    public GameObject prefabVideo;
    public GameObject prefabLogistik;
    public GameObject prefabParatrooper;

    [Header("Prefab Drawing")]
    public GameObject prefabDrawingSegment;
    public GameObject prefabSegmentSelector;
    public EDrawPredictor drawingPredictor;
    public ObjekDrawPolyline prefabPolyline;
    public ObjekDrawPolygon prefabPolygon;
    public ObjekDrawCircle prefabCircle;
    public ObjekDrawRectangle prefabRectangle;
    public ObjekDrawArrow prefabArrow;
    public ObjekDrawArrowCurved prefabArrowCurved;

    [Header("Prefab Missions")]
    public GameObject prefabRoute;
    public GameObject prefabNode;
    public GameObject prefabEditorNode;

    [Header("Prefab Weapons")]
    public GameObject prefabMissile;
    public GameObject prefabTorpedo;

    [Header("Prefab Guidance")]
    public SplineController prefabGuidance;
    public SplineController prefabRootGuidance;
    public Transform prefabAltitudeGuidance;

    [Header("HUD Element")]
    public string simbolTaktisLocation = "Fonts/Simbol Taktis/";
    public HUDNavigationElement prefabHUD;
    public HNSIndicatorPrefab prefabIndicator;
    public HNSIndicatorPrefab prefabMisiIndicator;

    [Header("HUD Element (TacMap)")]
    public GameObject prefabHUDTacmap;
    public HNSIndicatorPrefab prefabIndicatorTacmap;

    [Header("Prefab Wave Generator")]
    public GameObject frontWave;
    public GameObject rearWave;

    [Header("Prefab Sounds Template")]
    public NodeSound prefabVehicleSound;
    public NodeSound prefabShipSound;
    public NodeSound prefabAircraftSound;
    public NodeSound prefabAircraftPropSound;
    public NodeSound prefabHeliSound;
    public AudioClip radarSoundEffect;

    [Header("Clouds Preset")]
    public UniversalCloudLayer defaultCloudPreset;

    [Header("Config")]
    public assetPackageLocation assetLocation;
    public string fileFormat = "dec3d";
    public string configFormat = "dec3dml";


    //private string _dir;

    public List<string> ASSETS_NAME = new List<string>();
    public List<WGSBundleCore> ASSETS = new List<WGSBundleCore>();

    #region INSTANCE
    public static AssetPackageController Instance;
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            Instance.transform.parent = null;
            DontDestroyOnLoad(Instance);
        }
        else
        {
            Destroy(gameObject);
        }

        //locateAssetDirectory();
    }
    #endregion

    public async Task InitPasukanVariant()
    {
        if (PasukanSkinChanger.Instance == null) return;

        for(int i=0; i < PasukanSkinChanger.Instance.SkinMaterials.Count; i++)
        {
            if (PasukanSkinChanger.Instance.SkinMaterials[i] == null) continue;
            if (PasukanSkinChanger.Instance.SkinMaterials[i].ID_Pasukan == null || PasukanSkinChanger.Instance.SkinMaterials[i].ID_Pasukan == "") continue;

            var objPasukan = defaultPasukan.GetComponent<WGSBundleCore>();
            if (objPasukan == null) continue;
            //if (objPasukan.GetComponent<PasukanAnim>() == null) continue;

            //for(int j=0; j < objPasukan.GetComponent<PasukanAnim>().ListOrangan.Count; j++)
            //{
            //    var skinChange = objPasukan.GetComponent<PasukanAnim>().ListOrangan[j].GetComponent<SkinChange>();
            //    Debug.Log(objPasukan.GetComponent<PasukanAnim>().ListOrangan[j].name);
            //    //if (skinChange == null) continue;

            //    skinChange.ChangeSkin(PasukanSkinChanger.Instance.SkinMaterials[i].ID_Pasukan);
            //}

            ASSETS_NAME.Add(PasukanSkinChanger.Instance.SkinMaterials[i].ID_Pasukan);
            ASSETS.Add(objPasukan);
        }
    }

    /// <summary>
    /// -- Cari Lokasi Directory (folder) asset 3D disimpan --
    /// </summary>
    //private void locateAssetDirectory()
    //{
    //    switch (assetLocation)
    //    {
    //        case assetPackageLocation.DataPath:
    //            _dir = Directory.GetCurrentDirectory() + "/AssetPackage";
    //            break;
    //    }

    //    //loadPackageAlutsista();
    //}

    public async Task loadPackageAlutsista()
    {
        Debug.Log(StartupConfig.GetAssetPackageDirectory());

        if (!Directory.Exists(StartupConfig.GetAssetPackageDirectory()))
        {
            Debug.LogError("Directory tidak ditemukan!");
            return;
        }

        string[] bundleFolder = Directory.GetDirectories(StartupConfig.GetAssetPackageDirectory());

        // Set Loading Message & Progress
        Metadata loadingMessage = (SceneController.activeLoading) ? SceneController.activeLoading.GetComponent<Metadata>() : null;
        Image loadingProgress = null;

        if (loadingMessage)
        {
            loadingMessage.FindParameter("message").GetComponent<TextMeshProUGUI>().text = "Loading Asset Packages...";

            loadingProgress = loadingMessage.FindParameter("progress-bar").GetComponent<Image>();
            loadingProgress.fillAmount = 0;
        }

        if (bundleFolder.Length == 0)
        {
            Debug.Log("No Asset 3D Found");
            if (loadingProgress != null) loadingProgress.fillAmount += 0.25f;
        }
        else
        {
            for (int i = 0; i < bundleFolder.Length; i++)
            {
                string[] bundleFiles    = Directory.GetFiles(Path.Combine(bundleFolder[i]), "*." + fileFormat);
                string[] bundleConfig   = Directory.GetFiles(Path.Combine(bundleFolder[i]), "*." + configFormat);

                int bundleFinishedLoad = 0;

                if (bundleFiles.Length >= 0)
                {
                    // Load and instantiate seluruh pack asset ke "Don't Destroy" scene
                    for (int j = 0; j < bundleFiles.Length; j++)
                    {
                        if (loadingProgress != null)
                        {
                            loadingProgress.fillAmount += ((0.25f / bundleFiles.Length) / bundleFolder.Length);
                            loadingMessage.FindParameter("message").GetComponent<TextMeshProUGUI>().text = "Unpack " + bundleFiles[j].Replace(bundleFolder[i] + "\\","");
                        }

                        try
                        {
                            bundleFinishedLoad += await SyncLoad(bundleFiles[j], bundleConfig[j], Path.Combine(bundleFolder[i]));
                        }
                        catch (Exception e)
                        {
                            Debug.LogError("Failed To Load Asset " + bundleFiles[j]);
                            Debug.LogError(e.Message);
                        }
                    }
                }
                else
                {
                    if (loadingProgress != null) loadingProgress.fillAmount += (0.25f / bundleFolder.Length);
                }

                //if (loadingProgress != null) loadingProgress.fillAmount += (1f / bundleFolder.Length);

                if (bundleFinishedLoad == 0)
                {
                    // Use In-Game Default Bundle (tidak ada pack asset ditemukan)
                }
            }
        }


        ////string[] bundleFiles = Directory.GetFiles(_dir, "*." + fileFormat);
        ////int bundleFinishedLoad = 0;

        //if (bundleFiles.Length > 0)
        //{

        //    // Load and instantiate seluruh pack asset ke "DontDestroy" scene
        //    for (int i = 0; i < bundleFiles.Length; i++)
        //    {
        //        bundleFinishedLoad += await SyncLoad(bundleFiles[i], _dir);
        //    }
        //}
        //else
        //{
        //    Debug.Log("No Asset 3D Found");
        //}

        //if (bundleFinishedLoad == 0)
        //{
        //    // Use In-Game Default Bundle (tidak ada pack asset ditemukan)

        //}
    }

    public WGSBundleCore FindAsset(string _name, KategoriSatuan kategori, string _backupName = null)
    {
        for (int i = 0; i < ASSETS_NAME.Count; i++)
        {
            if (ASSETS_NAME[i].Equals(_name)) return ASSETS[i];
        }

        if(_backupName != null)
        {
            for (int i = 0; i < ASSETS_NAME.Count; i++)
            {
                if (ASSETS_NAME[i].Equals(_backupName)) return ASSETS[i];
            }
        }

        // Asset tidak memiliki Model 3D, gunakan default model
        switch (kategori)
        {
            case KategoriSatuan.VEHICLE:
                return defaultVehicle.GetComponent<WGSBundleCore>();
            case KategoriSatuan.SHIP:
                return defaultShip.GetComponent<WGSBundleCore>();
            case KategoriSatuan.AIRCRAFT:
                return defaultAircraft.GetComponent<WGSBundleCore>();
            case KategoriSatuan.HELICOPTER:
                return defaultHelicopter.GetComponent<WGSBundleCore>();
            case KategoriSatuan.INFANTRY:
                return defaultPasukan.GetComponent<WGSBundleCore>();
            default:
                return defaultAircraft.GetComponent<WGSBundleCore>();
        }
    }

    async Task<int> SyncLoad(string file, string config, string bundleUrl)
    {
        var fileStream = new FileStream(Path.Combine(bundleUrl, file), FileMode.Open, FileAccess.Read);
        var BundleLoaded = AssetBundle.LoadFromStreamAsync(fileStream);

        //var configStream = new FileStream(Path.Combine(bundleUrl, config), FileMode.Open, FileAccess.Read);
        List<BundleConfig> AssetConfig = new List<BundleConfig>();

        var _XMLSerialize = new XmlSerializer(typeof(List<BundleConfig>));
        using (var reader = new StreamReader(Path.Combine(bundleUrl, config)))
        {
            AssetConfig = (List<BundleConfig>)_XMLSerialize.Deserialize(reader);
        }

        //var loadingMessage = LevelManager.activeLoading.GetComponent<Metadata>();
        //if (loadingMessage)
        //{
        //    loadingMessage.FindParameter("message").parameter.GetComponent<TextMeshProUGUI>().text = "Loading " + Path.Combine(bundleUrl, file);
        //}
        //else
        //{
        //    var loadingObj = GameObject.Find("Scene Loading Overlay(Clone)");
        //    if (loadingObj) LoadingNotif = loadingObj.GetComponent<Metadata>().FindParameter("message").parameter.GetComponent<TextMeshProUGUI>();
        //}

        while (!BundleLoaded.isDone)
        {
            await Task.Yield();
        }

        if (BundleLoaded.assetBundle == null)
        {
            Debug.LogError("Failed to load Asset Package!");
            return 0;
        }
        else
        {
            var AssetInQueue = BundleLoaded.assetBundle.LoadAllAssetsAsync<GameObject>();

            while (!AssetInQueue.isDone)
            {
                await Task.Yield();
            }

            for (int i = 0; i < AssetInQueue.allAssets.Length; i++)
            {

                //Debug.Log("Load Asset ke " + i);

                var GO = AssetInQueue.allAssets[i] as GameObject;

                //Debug.Log(GO.GetComponent<AlutsistaBundleData>());

                // Assigning Asset into List
                var core = GO.GetComponent<WGSBundleCore>();
                string assetID = core.id.ToLower();
                if (AssetConfig != null)
                {
                    try
                    {
                        assetID = AssetConfig[0].name.ToLower();
                    }
                    catch (IndexOutOfRangeException) { }
                }


                if (!ASSETS_NAME.Contains(assetID))
                {
                    ASSETS_NAME.Add(assetID);
                    ASSETS.Add(core);
                }


                // -- Variant Module --
                var variants = ConfigVariantModule(GO.GetComponent<NodeVariant>(), AssetConfig);

                //for(int j = -1; j < variants.Count; j++)
                //{
                //    if(j > -1) core = variants[j];

                //    // -- Aircraft Engine Module
                //    ConfigAircraftEngineModule(core.GetComponent<NodeAircraftEngine>());

                //    // -- Heli Rotor Module --
                //    ConfigHeliRotorModule(core.GetComponent<NodeHeliRotor>());

                //    // -- Animated Module --
                //    ConfigAnimatedModule(core.GetComponent<NodeAnimated>());

                //    // -- Sound Module --
                //    //ConfigSoundModule(core.GetComponent<NodeSound>());

                //    // -- Wave Generator Module --
                //    ConfigDynamicWaveModule(core.GetComponent<NodeDynamicWave>());

                //    // -- Cargo Hold Module --
                //    ConfigCargoHoldModule(core.GetComponent<NodeCargoHold>());

                //    // -- Wheeled Vehicle Module --
                //    ConfigWheelModule(core.GetComponent<NodeWheeledVehicle>());
                //}

                //if (GO.GetComponent<VehicleBundleData>() != null)
                //{
                //    if (!ASSETS_NAME.Contains(GO.GetComponent<VehicleBundleData>().name))
                //    {
                //        var metadata = GO.GetComponent<VehicleBundleData>();
                //        ASSETS_NAME.Add(metadata.name);
                //        ASSETS.Add(core);
                //    }
                //}
                //else if (GO.GetComponent<ShipBundleData>() != null)
                //{
                //    if (!ASSETS_NAME.Contains(GO.GetComponent<ShipBundleData>().name))
                //    {
                //        var metadata = GO.GetComponent<ShipBundleData>();
                //        ASSETS_NAME.Add(metadata.name);
                //        ASSETS.Add(GO);

                //        ConfigWaveNodes(metadata);
                //        ConfigRadarNodes(metadata);
                //        ConfigShipPropNodes(metadata);
                //    }
                //}
                //else if (GO.GetComponent<AircraftBundleData>() != null)
                //{
                //    if (!ASSETS_NAME.Contains(GO.GetComponent<AircraftBundleData>().name))
                //    {
                //        var metadata = GO.GetComponent<AircraftBundleData>();
                //        ASSETS_NAME.Add(metadata.name);
                //        ASSETS.Add(GO);

                //        ConfigAircraftPropNodes(metadata);
                //        ConfigRotorNodes(metadata);
                //    }
                //}

                //if (!ASSETS_NAME.Contains(GO.GetComponent<AlutsistaBundleData>().name))
                //{
                //    var metadata = GO.GetComponent<AlutsistaBundleData>();
                //    ASSETS_NAME.Add(metadata.name);
                //    ASSETS.Add(GO);

                //    ConfigWaveNodes(metadata);

                //    ConfigRadarNodes(metadata);
                //    ConfigRotorNodes(metadata);
                //}

                //using (TextReader reader = new StreamReader(configStream))
                //{
                //    string contents = reader.ReadToEnd();
                //    contents = contents.Replace("{", "").Replace("}", "").Replace("\n", "");

                //    //foreach(string nama_satuan in contents.Split(','))
                //    //{
                //    //    if (!ASSETS_NAME.Contains(nama_satuan.Replace("\n", "")))
                //    //    {
                //    //        ASSETS_NAME.Add(nama_satuan);
                //    //        ASSETS.Add(GO);
                //    //    }
                //    //}

                //    // Add List Service Into ListView

                //}

                //for (int j = 0; j < _ASSETS.Count; j++)
                //{
                //    if (!ASSETS_NAME.Contains(_ASSETS[j].name))
                //    {
                //        ASSETS_NAME.Add(_ASSETS[j].name);
                //        ASSETS.Add(GO);
                //    }
                //}

                //if (!ASSETS_NAME.Contains(GO.GetComponent<AlutsistaBundleData>().assetName))
                //{
                //    var metadata = GO.GetComponent<AlutsistaBundleData>();
                //    ASSETS_NAME.Add(metadata.assetName);
                //    ASSETS.Add(GO);
                //}




                //var GO = Instantiate(AssetInQueue.allAssets[i]) as GameObject;

                //
                //GO.name = metadata.assetPrefab.name;
                //GO.transform.position = new Vector3(i * 50, 0, 0);

                //assetName.text = metadata.assetName;
                //assetTipeTNI.text = metadata.tipe_tni.ToString();
                //assetPrefab.text = metadata.assetPrefab.name;
                //assetImage.sprite = metadata.assetImage;
                //assetNumber++;


                //DontDestroyOnLoad(GO);
            }

            //for(int i=0; i < ASSETS.Count; i++)
            //{
            //    var GO = ASSETS[i];

            //    Instantiate(GO);

            //    Debug.Log(ASSETS[i].name);
            //    continue;
            //    // MODULE CONFIG
            //    // -- Aircraft Engine Module --
            //    ConfigAircraftEngineModule(GO.GetComponent<NodeAircraftEngine>());

            //    // -- Heli Rotor Module --
            //    ConfigHeliRotorModule(GO.GetComponent<NodeHeliRotor>());

            //    // -- Animated Module --
            //    ConfigAnimatedModule(GO.GetComponent<NodeAnimated>());

            //    // -- Wave Generator Module --
            //    ConfigDynamicWaveModule(GO.GetComponent<NodeDynamicWave>());

            //    // -- Cargo Hold Module --
            //    ConfigCargoHoldModule(GO.GetComponent<NodeCargoHold>());
            //}

            BundleLoaded.assetBundle.Unload(false);
            return 1;
        }
    }

    public static void ConfigAircraftModule(NodeAircraft _module)
    {
        try
        {
            if (_module == null) return;

            _module.properties = new NodeAircraftProperties();

            if (AnimationPackageController.Instance == null) return;

            for (int i = 0; i < _module.nodeEngine.Count; i++)
            {
                var node = _module.nodeEngine[i].engine;
                if (node == null) continue;

                var animator = node.gameObject.AddComponent<Animator>();
                animator.runtimeAnimatorController = AnimationPackageController.Instance.aircraftPropsAnim;
            }

            for (int i = 0; i < _module.nodeGear.Count; i++)
            {
                var node = _module.nodeGear[i];
                if (node.support == null) continue;
                if (node.useCustomAnim && (node.customAnim == null || node.customAnimName == "")) continue;

                var animator = node.support.gameObject.AddComponent<Animator>();
                animator.runtimeAnimatorController = node.customAnim;
            }
        }catch(Exception e) { Debug.LogError(e.Message); }
    }

    public static void ConfigAircraftEngineModule(NodeAircraft _module)
    {
        if (_module == null) return;
        if (AnimationPackageController.Instance == null) return;

        for (int i=0; i < _module.nodeEngine.Count; i++)
        {
            var node = _module.nodeEngine[i].engine;
            if (node == null) continue;

            var animator = node.gameObject.AddComponent<Animator>();
            animator.runtimeAnimatorController = AnimationPackageController.Instance.aircraftPropsAnim;

            //if (node != null)
            //{
            //    var animator = _module.nodes[i].node.gameObject.AddComponent<Animator>();
            //    if (_module.nodes[i].engineType == NodeAircraftEngineData.EngineType.Jet)
            //    {
            //        animator.runtimeAnimatorController = AnimationPackageController.Instance.aircraftPropsAnim;

            //        // ADD JET SOUND

            //    }
            //    else
            //    {
            //        animator.runtimeAnimatorController = AnimationPackageController.Instance.aircraftPropsAnim;

            //        // ADD PROPS SOUND
            //    }

            //}

            //if (_module.nodes[i].vfx.Count <= 0) continue;
            //if (VFXPackageController.Instance == null) continue;

            //// Config Engine VFX
            //for (int j = 0; j < _module.nodes[i].vfx.Count; j++)
            //{
            //    if (_module.nodes[i].vfx[j] == null) continue;

            //    var objParticle = VFXPackageController.Instance.FindParticles(assetLocation, _module.nodes)
            //}
        }

        //if(walker != null)
        //{
        //    walker.guidance = EntityController.instance.CreateEntity(walker.GetComponent<ArcGISLocationComponent>().Position, Vector3.zero, Instance.prefabGuidance.gameObject).GetComponent<SplineController>();
        //    walker.syncRoll = true;
        //    walker.sensitivity = 10;
        //    walker.guidanceDistance = 1000;
        //}

        //if (asset.nodeParticles.Count <= 0) return;
        //if (VFXPackageController.Instance == null) return;

        //for (int i = 0; i < asset.nodeParticles.Count; i++)
        //{
        //    if (asset.nodeParticles[i].obj == null) continue;

        //    var objParticle = VFXPackageController.Instance.FindParticles(asset.nodeParticles[i].name, asset.nodeParticles[i].type);
        //    if (objParticle == null) return;

        //    objParticle = Instantiate(objParticle, asset.nodeParticles[i].obj.transform);
        //    objParticle.transform.localPosition = Vector3.zero;
        //    objParticle.transform.localRotation = Quaternion.identity;
        //}
    }

    public static void ConfigHeliRotorModule(NodeHeliRotor _module)
    {
        try
        {
            if (_module == null) return;
            if (AnimationPackageController.Instance == null) return;

            for (int i = 0; i < _module.nodes.Count; i++)
            {
                if (_module.nodes[i].node == null) continue;

                var animator = _module.nodes[i].node.gameObject.AddComponent<Animator>();
                if (_module.nodes[i].rotorType == NodeHeliRotorData.RotorType.Top)
                {
                    // SET TOP ROTOR ANIMATION
                    animator.runtimeAnimatorController = AnimationPackageController.Instance.rotorTop;
                }
                else
                {
                    // SET TAIL ROTOR ANIMATION
                    animator.runtimeAnimatorController = AnimationPackageController.Instance.rotorTail;
                }
            }
        }catch(Exception e) { Debug.LogError(e.Message); }
    }

    public static void ConfigAnimatedModule(NodeAnimated _module)
    {
        try
        {
            if (_module == null) return;

            for (int i = 0; i < _module.nodes.Count; i++)
            {
                if (_module.nodes[i].node == null) continue;

                var animator = _module.nodes[i].node.gameObject.AddComponent<Animator>();
                animator.runtimeAnimatorController = _module.nodes[i].animator;
                animator.updateMode = _module.nodes[i].updateMode;

                animator.speed = _module.nodes[i].animatorSpeed;

                //animator.SetFloat("animSpeed", _module.nodes[i].animatorSpeed);
                animator.enabled = true;

                if (_module.nodes[i].animator.name == "NodeRadarAnim")
                {
                    var radarSound = _module.nodes[i].node.gameObject.AddComponent<AudioSource>();
                    radarSound.loop = true;
                    radarSound.clip = AssetPackageController.Instance.radarSoundEffect;

                    radarSound.minDistance = 0;
                    radarSound.maxDistance = 10;
                    radarSound.spatialBlend = 1;
                    radarSound.dopplerLevel = 0;
                    radarSound.spread = 180;
                    radarSound.rolloffMode = AudioRolloffMode.Linear;

                    radarSound.enabled = false;
                    radarSound.enabled = true;
                }
            }
        }catch(Exception e) { Debug.LogError(e.Message); }
    }

    public static void ConfigSoundModule(GameObject obj)
    {
        try
        {
            var _module = obj.GetComponent<NodeSound>();
            var _objSatuan = obj.GetComponent<ObjekSatuan>();

            if (_objSatuan.kategori == KategoriSatuan.INFANTRY) return;

            if (_module != null)
            {

            }
            else
            {
                _module = obj.AddComponent<NodeSound>();


                if (_objSatuan.kategori == KategoriSatuan.VEHICLE)
                {
                    var moduleTemplate = Instantiate(AssetPackageController.Instance.prefabVehicleSound.gameObject, obj.transform.GetChild(0));

                    _module.type = moduleTemplate.GetComponent<NodeSound>().type;
                    _module.entitySounds = moduleTemplate.GetComponent<NodeSound>().entitySounds;

                    Destroy(moduleTemplate.GetComponent<NodeSound>());
                    Destroy(moduleTemplate.GetComponent<WGSBundleCore>());
                }
                else if (_objSatuan.kategori == KategoriSatuan.SHIP)
                {
                    var moduleTemplate = Instantiate(AssetPackageController.Instance.prefabShipSound.gameObject, obj.transform.GetChild(0));

                    _module.type = moduleTemplate.GetComponent<NodeSound>().type;
                    _module.entitySounds = moduleTemplate.GetComponent<NodeSound>().entitySounds;

                    Destroy(moduleTemplate.GetComponent<NodeSound>());
                    Destroy(moduleTemplate.GetComponent<WGSBundleCore>());
                }
                else if (_objSatuan.kategori == KategoriSatuan.AIRCRAFT)
                {
                    GameObject moduleTemplate;
                    if (_objSatuan.GetComponent<NodeAircraft>() != null || _objSatuan.GetComponent<NodeAircraft>().nodeEngine.Count >= 1)
                    {
                        if (_objSatuan.GetComponent<NodeAircraft>().nodeEngine[0].type == AircraftEngine.EngineType.JET)
                        {
                            moduleTemplate = Instantiate(AssetPackageController.Instance.prefabAircraftSound.gameObject, obj.transform.GetChild(0));
                        }
                        else
                        {
                            moduleTemplate = Instantiate(AssetPackageController.Instance.prefabAircraftPropSound.gameObject, obj.transform.GetChild(0));
                        }
                    }
                    else
                    {
                        moduleTemplate = Instantiate(AssetPackageController.Instance.prefabAircraftSound.gameObject, obj.transform.GetChild(0));
                    }

                    _module.type = moduleTemplate.GetComponent<NodeSound>().type;
                    _module.entitySounds = moduleTemplate.GetComponent<NodeSound>().entitySounds;

                    Destroy(moduleTemplate.GetComponent<NodeSound>());
                    Destroy(moduleTemplate.GetComponent<WGSBundleCore>());

                    //var moveSound = new List<NodeSoundData>();
                    //moveSound.Add(new NodeSoundData
                    //{
                    //    activeState = NodeSoundData.SoundState.AlwaysActive,
                    //    activeSide = NodeSoundData.SoundSide.Front,
                    //    node = 
                    //});

                    //_module.entitySounds = new EntitySoundData({
                    //    move = new NodeSoundData
                    //    {
                    //        activeState = NodeSoundData.SoundState.AlwaysActive,
                    //        audioRange = 
                    //    }
                    //});
                }
                else if (_objSatuan.kategori == KategoriSatuan.HELICOPTER)
                {
                    var moduleTemplate = Instantiate(AssetPackageController.Instance.prefabHeliSound.gameObject, obj.transform.GetChild(0));

                    _module.type = moduleTemplate.GetComponent<NodeSound>().type;
                    _module.entitySounds = moduleTemplate.GetComponent<NodeSound>().entitySounds;

                    Destroy(moduleTemplate.GetComponent<NodeSound>());
                    Destroy(moduleTemplate.GetComponent<WGSBundleCore>());
                }
            }

            var walkerSound = obj.GetComponent<WalkerSound>();
            if (walkerSound != null)
            {
                walkerSound.nodeSound = _module;
            }

            for (int i = 0; i < _module.entitySounds.idle.Count; i++)
            {
                if (_module.entitySounds.idle[i].node == null || _module.entitySounds.idle[i].track == null) continue;

                var audioSource = _module.entitySounds.idle[i].node.gameObject.AddComponent<AudioSource>();
                audioSource.clip = _module.entitySounds.idle[i].track;

                audioSource.playOnAwake = true;
                audioSource.loop = true;
                audioSource.spatialBlend = 1;
                audioSource.dopplerLevel = 0;
                //audioSource.spread = 180;
                audioSource.rolloffMode = AudioRolloffMode.Logarithmic;
                audioSource.minDistance = _module.entitySounds.idle[i].audioRange.x;
                audioSource.maxDistance = _module.entitySounds.idle[i].audioRange.y;

                audioSource.enabled = false;
                audioSource.enabled = true;
            }

            for (int i = 0; i < _module.entitySounds.start.Count; i++)
            {
                if (_module.entitySounds.start[i].node == null || _module.entitySounds.start[i].track == null) continue;

                var audioSource = _module.entitySounds.start[i].node.gameObject.AddComponent<AudioSource>();
                audioSource.clip = _module.entitySounds.start[i].track;

                audioSource.playOnAwake = true;
                audioSource.loop = true;
                audioSource.spatialBlend = 1;
                audioSource.dopplerLevel = 0;
                ///audioSource.spread = 180;
                audioSource.rolloffMode = AudioRolloffMode.Logarithmic;
                audioSource.minDistance = _module.entitySounds.start[i].audioRange.x;
                audioSource.maxDistance = _module.entitySounds.start[i].audioRange.y;

                audioSource.enabled = false;
                audioSource.enabled = true;
            }

            for (int i = 0; i < _module.entitySounds.move.Count; i++)
            {
                if (_module.entitySounds.move[i].node == null || _module.entitySounds.move[i].track == null) continue;

                var audioSource = _module.entitySounds.move[i].node.gameObject.AddComponent<AudioSource>();
                audioSource.clip = _module.entitySounds.move[i].track;

                audioSource.playOnAwake = true;
                audioSource.loop = true;
                audioSource.spatialBlend = 1;
                audioSource.dopplerLevel = 0;
                //audioSource.spread = 180;
                audioSource.rolloffMode = AudioRolloffMode.Logarithmic;
                audioSource.minDistance = _module.entitySounds.move[i].audioRange.x;
                audioSource.maxDistance = _module.entitySounds.move[i].audioRange.y;

                audioSource.enabled = false;
                audioSource.enabled = true;
            }

            for (int i = 0; i < _module.entitySounds.stop.Count; i++)
            {
                if (_module.entitySounds.stop[i].node == null || _module.entitySounds.stop[i].track == null) continue;

                var audioSource = _module.entitySounds.stop[i].node.gameObject.AddComponent<AudioSource>();
                audioSource.clip = _module.entitySounds.stop[i].track;

                audioSource.playOnAwake = true;
                audioSource.loop = true;
                audioSource.spatialBlend = 1;
                audioSource.dopplerLevel = 0;
                audioSource.spread = 180;
                audioSource.rolloffMode = AudioRolloffMode.Logarithmic;
                audioSource.minDistance = _module.entitySounds.stop[i].audioRange.x;
                audioSource.maxDistance = _module.entitySounds.stop[i].audioRange.y;

                audioSource.enabled = false;
                audioSource.enabled = true;
            }

            //if (_module.engineSounds.Count > 0) _module.gameObject.AddComponent<AudioController>();
        }catch(Exception e) { Debug.LogError(e.Message); }
    }

    public static void ConfigVFXModule(NodeVFX _module)
    {
        try
        {
            if (_module == null) return;

            for (int i = 0; i < _module.nodes.Count; i++)
            {
                if (_module.nodes[i].node == null) continue;

                if (_module.nodes[i].objVFX != null)
                {
                    var objVFX = Instantiate(_module.nodes[i].objVFX, _module.nodes[i].node);
                    objVFX.gameObject.SetActive(false);
                }
                else
                {
                    switch (_module.nodes[i].type)
                    {
                        case NodeVFXData.VFXType.Engine:
                            Instantiate(VFXPackageController.Instance.jetEngine, _module.nodes[i].node).SetActive(false);
                            break;
                        case NodeVFXData.VFXType.Smoke:
                            Instantiate(VFXPackageController.Instance.jetTrail, _module.nodes[i].node).SetActive(false);
                            break;
                        default: break;
                    }
                }

                //if (_module.nodes[i].type == NodeVFXData.VFXType.Engine)
                //{
                //    if (_module.nodes[i].objVFX == null)
                //    {
                //        var VFXController = VFXPackageController.Instance;

                //        Instantiate(VFXController.jetEngine, _module.nodes[i].node);
                //    }
                //    else
                //    {
                //        Instantiate(_module.nodes[i].objVFX, _module.nodes[i].node);
                //    }
                //}else if (_module.nodes[i].type == NodeVFXData.VFXType.Smoke)
                //{
                //    if (_module.nodes[i].objVFX == null)
                //    {
                //        var VFXController = VFXPackageController.Instance;

                //        Instantiate(VFXController.jetEngine, _module.nodes[i].node);
                //    }
                //    else
                //    {
                //        Instantiate(_module.nodes[i].objVFX, _module.nodes[i].node);
                //    }
                //}

                //if(_module.nodes[i].vfxType == NodeVFXData.VFXType.DEFAULT)
                //{
                //    var VFXController = VFXPackageController.Instance;

                //    switch (_module.nodes[i].defaultVFX)
                //    {
                //        case NodeVFXData.DefaultVFX.JET_ENGINE:
                //            Instantiate(VFXController.jetEngine, _module.nodes[i].node);
                //            break;
                //        case NodeVFXData.DefaultVFX.JET_TRAIL:
                //            Instantiate(VFXController.jetTrail, _module.nodes[i].node);
                //            break;
                //        case NodeVFXData.DefaultVFX.PROPS_TRAIL:
                //            Instantiate(VFXController.propsTrail, _module.nodes[i].node);
                //            break;
                //        case NodeVFXData.DefaultVFX.WING_TRAIL:
                //            Instantiate(VFXController.wingsTrail, _module.nodes[i].node);
                //            break;
                //        case NodeVFXData.DefaultVFX.TIRE_DUST:
                //            Instantiate(VFXController.tireDust, _module.nodes[i].node);
                //            break;
                //        case NodeVFXData.DefaultVFX.SHIP_FUNNEL:
                //            Instantiate(VFXController.shipFunnel, _module.nodes[i].node);
                //            break;
                //        default: break;
                //    }

                //    _module.nodes[i].node.gameObject.SetActive(false);
                //}
                //else
                //{

                //}
            }
        }catch(Exception e) { Debug.LogError(e.Message); }
    }

    public static void ConfigDynamicWaveModule(NodeDynamicWave _module)
    {
        try
        {
            if (_module == null) return;

            for (int i = 0; i < _module.nodes.Count; i++)
            {
                if (_module.nodes[i].node == null) continue;
                if (_module.nodes[i].node.gameObject.GetComponent<SphereWaterInteraction>()) continue;

                // SETUP WATER INTERACTION
                var waterInteraction = _module.nodes[i].node.gameObject.AddComponent<SphereWaterInteraction>();

                switch (_module.nodes[i].preset)
                {
                    case NodeWavePresetData.PresetData.small:
                        waterInteraction._radius = NodeDynamicWaveHelper.NodeWaveSmallPreset.radius;
                        waterInteraction._weight = NodeDynamicWaveHelper.NodeWaveSmallPreset.weight;
                        waterInteraction._weightUpDownMul = NodeDynamicWaveHelper.NodeWaveSmallPreset.weightMultiplier;
                        waterInteraction._innerSphereMultiplier = NodeDynamicWaveHelper.NodeWaveSmallPreset.innerSphereMultiplier;
                        waterInteraction._innerSphereOffset = NodeDynamicWaveHelper.NodeWaveSmallPreset.innerSphereOffset;
                        waterInteraction._velocityOffset = NodeDynamicWaveHelper.NodeWaveSmallPreset.velocityOffset;
                        waterInteraction._compensateForWaveMotion = NodeDynamicWaveHelper.NodeWaveSmallPreset.compensateForWaveMotion;
                        waterInteraction._boostLargeWaves = NodeDynamicWaveHelper.NodeWaveSmallPreset.boostLargeWave;

                        waterInteraction._teleportSpeed = NodeDynamicWaveHelper.NodeWaveSmallPreset.teleportSpeed;
                        waterInteraction._warnOnTeleport = NodeDynamicWaveHelper.NodeWaveSmallPreset.warmOnTeleport;
                        waterInteraction._maxSpeed = NodeDynamicWaveHelper.NodeWaveSmallPreset.maxSpeed;
                        waterInteraction._warnOnSpeedClamp = NodeDynamicWaveHelper.NodeWaveSmallPreset.warmOnSpeedClamp;
                        break;
                    case NodeWavePresetData.PresetData.medium:
                        waterInteraction._radius = NodeDynamicWaveHelper.NodeWaveMediumPreset.radius;
                        waterInteraction._weight = NodeDynamicWaveHelper.NodeWaveMediumPreset.weight;
                        waterInteraction._weightUpDownMul = NodeDynamicWaveHelper.NodeWaveMediumPreset.weightMultiplier;
                        waterInteraction._innerSphereMultiplier = NodeDynamicWaveHelper.NodeWaveMediumPreset.innerSphereMultiplier;
                        waterInteraction._innerSphereOffset = NodeDynamicWaveHelper.NodeWaveMediumPreset.innerSphereOffset;
                        waterInteraction._velocityOffset = NodeDynamicWaveHelper.NodeWaveMediumPreset.velocityOffset;
                        waterInteraction._compensateForWaveMotion = NodeDynamicWaveHelper.NodeWaveMediumPreset.compensateForWaveMotion;
                        waterInteraction._boostLargeWaves = NodeDynamicWaveHelper.NodeWaveMediumPreset.boostLargeWave;

                        waterInteraction._teleportSpeed = NodeDynamicWaveHelper.NodeWaveMediumPreset.teleportSpeed;
                        waterInteraction._warnOnTeleport = NodeDynamicWaveHelper.NodeWaveMediumPreset.warmOnTeleport;
                        waterInteraction._maxSpeed = NodeDynamicWaveHelper.NodeWaveMediumPreset.maxSpeed;
                        waterInteraction._warnOnSpeedClamp = NodeDynamicWaveHelper.NodeWaveMediumPreset.warmOnSpeedClamp;
                        break;
                    case NodeWavePresetData.PresetData.large:
                        waterInteraction._radius = NodeDynamicWaveHelper.NodeWaveLargePreset.radius;
                        waterInteraction._weight = NodeDynamicWaveHelper.NodeWaveLargePreset.weight;
                        waterInteraction._weightUpDownMul = NodeDynamicWaveHelper.NodeWaveLargePreset.weightMultiplier;
                        waterInteraction._innerSphereMultiplier = NodeDynamicWaveHelper.NodeWaveLargePreset.innerSphereMultiplier;
                        waterInteraction._innerSphereOffset = NodeDynamicWaveHelper.NodeWaveLargePreset.innerSphereOffset;
                        waterInteraction._velocityOffset = NodeDynamicWaveHelper.NodeWaveLargePreset.velocityOffset;
                        waterInteraction._compensateForWaveMotion = NodeDynamicWaveHelper.NodeWaveLargePreset.compensateForWaveMotion;
                        waterInteraction._boostLargeWaves = NodeDynamicWaveHelper.NodeWaveLargePreset.boostLargeWave;

                        waterInteraction._teleportSpeed = NodeDynamicWaveHelper.NodeWaveLargePreset.teleportSpeed;
                        waterInteraction._warnOnTeleport = NodeDynamicWaveHelper.NodeWaveLargePreset.warmOnTeleport;
                        waterInteraction._maxSpeed = NodeDynamicWaveHelper.NodeWaveLargePreset.maxSpeed;
                        waterInteraction._warnOnSpeedClamp = NodeDynamicWaveHelper.NodeWaveLargePreset.warmOnSpeedClamp;
                        break;
                    default:
                        waterInteraction._radius = _module.nodes[i].radius;
                        waterInteraction._weight = _module.nodes[i].weight;
                        waterInteraction._weightUpDownMul = _module.nodes[i].weightMultipier;
                        waterInteraction._innerSphereMultiplier = _module.nodes[i].innerSphereMultiplier;
                        waterInteraction._innerSphereOffset = _module.nodes[i].innerSphereOffset;
                        waterInteraction._velocityOffset = _module.nodes[i].velocityOffset;
                        waterInteraction._compensateForWaveMotion = _module.nodes[i].compensateForWaveMotion;
                        waterInteraction._boostLargeWaves = _module.nodes[i].boostLargeWave;

                        waterInteraction._teleportSpeed = _module.nodes[i].teleportSpeed;
                        waterInteraction._warnOnTeleport = _module.nodes[i].warmOnTeleport;
                        waterInteraction._maxSpeed = _module.nodes[i].maxSpeed;
                        waterInteraction._warnOnSpeedClamp = _module.nodes[i].warmOnSpeedClamp;
                        break;
                }

                // SETUP RUDDER COMPONENT


                // SETUP PROPELLER COMPONENT
            }

            _module.gameObject.AddComponent<ShipFoamWatcher>();
        }catch(Exception e) { Debug.LogError(e.Message); }
    }

    private List<WGSBundleCore> ConfigVariantModule(NodeVariant _module, List<BundleConfig> AssetConfig)
    {
        var TEMP_VARIANT = new List<WGSBundleCore>();

        if (_module == null) return TEMP_VARIANT;

        for (int i = 0; i < _module.variants.Count; i++)
        {
            if (_module.variants[i].id == "" || _module.variants[i].id == null) continue;
            if (_module.variants[i].obj == null) continue;

            string variantName = _module.variants[i].id.ToLower();
            if(AssetConfig != null)
            {
                try
                {
                    variantName = AssetConfig[0].variants[i].name.ToLower();
                }
                catch (IndexOutOfRangeException e) {
                    continue;
                }
            }

            if (!ASSETS_NAME.Contains(variantName))
            {
                ASSETS_NAME.Add(variantName);
                ASSETS.Add(_module.variants[i].obj.GetComponent<WGSBundleCore>());

                TEMP_VARIANT.Add(_module.variants[i].obj.GetComponent<WGSBundleCore>());
            }
        }

        return TEMP_VARIANT;
    }

    public static void ConfigCargoHoldModule(NodeCargoHold _module)
    {
        try
        {
            if (_module == null) return;

            for (int i = 0; i < _module.doors.Count; i++)
            {
                if (_module.doors[i].obj == null) continue;

                var animator = _module.doors[i].obj.gameObject.AddComponent<Animator>();

                // SET DOORS ANIMATION
                animator.runtimeAnimatorController = _module.doors[i].animation;
                animator.updateMode = AnimatorUpdateMode.Normal;
            }

            _module.gameObject.AddComponent<CargoRampWatcher>();
        }catch(Exception e) { Debug.LogError(e.Message); }
    }

    public static void ConfigWheelModule(NodeWheeledVehicle _module)
    {
        if (_module == null) return;

        for (int i = 0; i < _module.nodes.Count; i++)
        {
            if (_module.nodes[i].node == null) continue;
            if (_module.nodes[i].node.GetComponent<Animator>() != null) continue;

            var animator = _module.nodes[i].node.gameObject.AddComponent<Animator>();

            // SET WHEEL ANIMATION
            animator.runtimeAnimatorController = AnimationPackageController.Instance.vehicleWheel;
            animator.updateMode = AnimatorUpdateMode.Normal;
            animator.speed = 0;
        }
    }

    //private void ConfigWaveNodes(ShipBundleData asset)
    //{

    //    if (asset.nodeFrontWave != null)
    //    {
    //        asset.nodeFrontWave.AddComponent<SphereWaterInteraction>();
    //        asset.nodeFrontWave.GetComponent<SphereWaterInteraction>()._radius = frontWave.GetComponent<SphereWaterInteraction>()._radius;
    //        asset.nodeFrontWave.GetComponent<SphereWaterInteraction>()._weight = frontWave.GetComponent<SphereWaterInteraction>()._weight;
    //        asset.nodeFrontWave.GetComponent<SphereWaterInteraction>()._weightUpDownMul = frontWave.GetComponent<SphereWaterInteraction>()._weightUpDownMul;
    //        asset.nodeFrontWave.GetComponent<SphereWaterInteraction>()._innerSphereMultiplier = frontWave.GetComponent<SphereWaterInteraction>()._innerSphereMultiplier;
    //        asset.nodeFrontWave.GetComponent<SphereWaterInteraction>()._innerSphereOffset = frontWave.GetComponent<SphereWaterInteraction>()._innerSphereOffset;
    //        asset.nodeFrontWave.GetComponent<SphereWaterInteraction>()._velocityOffset = frontWave.GetComponent<SphereWaterInteraction>()._velocityOffset;
    //        asset.nodeFrontWave.GetComponent<SphereWaterInteraction>()._compensateForWaveMotion = frontWave.GetComponent<SphereWaterInteraction>()._compensateForWaveMotion;
    //        asset.nodeFrontWave.GetComponent<SphereWaterInteraction>()._boostLargeWaves = frontWave.GetComponent<SphereWaterInteraction>()._boostLargeWaves;

    //        //var waveEffect = Instantiate(frontWave, asset.nodeWakeGenerator.transform);
    //        //waveEffect.transform.localPosition = asset.nodeFrontWave.transform.localPosition;

    //        //asset.nodeFrontWave = waveEffect;
    //    }

    //    if (asset.nodeRearWave != null)
    //    {
    //        asset.nodeRearWave.AddComponent<SphereWaterInteraction>();

    //        asset.nodeRearWave.AddComponent<SphereWaterInteraction>();
    //        asset.nodeRearWave.GetComponent<SphereWaterInteraction>()._radius = rearWave.GetComponent<SphereWaterInteraction>()._radius;
    //        asset.nodeRearWave.GetComponent<SphereWaterInteraction>()._weight = rearWave.GetComponent<SphereWaterInteraction>()._weight;
    //        asset.nodeRearWave.GetComponent<SphereWaterInteraction>()._weightUpDownMul = rearWave.GetComponent<SphereWaterInteraction>()._weightUpDownMul;
    //        asset.nodeRearWave.GetComponent<SphereWaterInteraction>()._innerSphereMultiplier = rearWave.GetComponent<SphereWaterInteraction>()._innerSphereMultiplier;
    //        asset.nodeRearWave.GetComponent<SphereWaterInteraction>()._innerSphereOffset = rearWave.GetComponent<SphereWaterInteraction>()._innerSphereOffset;
    //        asset.nodeRearWave.GetComponent<SphereWaterInteraction>()._velocityOffset = rearWave.GetComponent<SphereWaterInteraction>()._velocityOffset;
    //        asset.nodeRearWave.GetComponent<SphereWaterInteraction>()._compensateForWaveMotion = rearWave.GetComponent<SphereWaterInteraction>()._compensateForWaveMotion;
    //        asset.nodeRearWave.GetComponent<SphereWaterInteraction>()._boostLargeWaves = rearWave.GetComponent<SphereWaterInteraction>()._boostLargeWaves;

    //        //Debug.Log("Node REAR Wave = " + asset.nodeRearWave.name);

    //        //var waveEffect = Instantiate(rearWave, asset.nodeWakeGenerator.transform);
    //        //waveEffect.transform.localPosition = asset.nodeRearWave.transform.localPosition;

    //        //asset.nodeRearWave = waveEffect;
    //    }
    //}

    //private void ConfigRadarNodes(ShipBundleData asset)
    //{
    //    if (asset.nodeRadar.Count <= 0) return;
    //    if (AnimationPackageController.Instance == null) return;

    //    for (int i = 0; i < asset.nodeRadar.Count; i++)
    //    {
    //        if (asset.nodeRadar[i].obj == null) continue;

    //        var animator = asset.nodeRadar[i].obj.AddComponent<Animator>();
    //        animator.runtimeAnimatorController = AnimationPackageController.Instance.radarAnim;
    //        animator.SetFloat("animSpeed", asset.nodeRadar[i].animationSpeed);
    //    }
    //}

    //private void ConfigShipPropNodes(ShipBundleData asset)
    //{
    //    //if (asset.nodePropeller.Count <= 0) return;
    //    //if (GetComponent<AnimationPackageManager>() == null) return;

    //    //for (int i = 0; i < asset.nodePropeller.Count; i++)
    //    //{
    //    //    if (asset.nodePropeller[i] == null) continue;

    //    //    var animator = asset.nodePropeller[i].AddComponent<Animator>();
    //    //    animator.runtimeAnimatorController = GetComponent<AnimationPackageManager>().shipPropsAnim;
    //    //    animator.enabled = false;
    //    //}
    //}

    //private void ConfigRotorNodes(AircraftBundleData asset)
    //{
    //    if (asset.nodeRotor.Count <= 0) return;
    //    if (AnimationPackageController.Instance == null) return;

    //    for (int i = 0; i < asset.nodeRotor.Count; i++)
    //    {
    //        if (asset.nodeRotor[i].obj == null) continue;

    //        var animator = asset.nodeRotor[i].obj.AddComponent<Animator>();

    //        if (asset.nodeRotor[i].type == AircraftBundleData.BundleNodeRotor.rotorType.TOP)
    //        {
    //            animator.runtimeAnimatorController = AnimationPackageController.Instance.rotorTop;
    //        }
    //        else if (asset.nodeRotor[i].type == AircraftBundleData.BundleNodeRotor.rotorType.TAIL)
    //        {
    //            animator.runtimeAnimatorController = AnimationPackageController.Instance.rotorTail;
    //        }
    //    }
    //}

    //private void ConfigAircraftPropNodes(AircraftBundleData asset)
    //{
    //    if (asset.nodeEngine.Count <= 0) return;
    //    if (AnimationPackageController.Instance == null) return;

    //    for (int i = 0; i < asset.nodeEngine.Count; i++)
    //    {
    //        if (asset.nodeEngine[i].obj == null) continue;

    //        var animator = asset.nodeEngine[i].obj.AddComponent<Animator>();

    //        if (asset.nodeEngine[i].type == AircraftBundleData.BundleNodeEngine.propsType.PROPS)
    //        {
    //            animator.runtimeAnimatorController = AnimationPackageController.Instance.aircraftPropsAnim;
    //        }
    //        else if (asset.nodeEngine[i].type == AircraftBundleData.BundleNodeEngine.propsType.JET)
    //        {
    //            animator.runtimeAnimatorController = AnimationPackageController.Instance.aircraftPropsAnim;
    //        }
    //    }
    //}

    //public void ConfigAircraftParticles(AircraftBundleData asset)
    //{
    //    if (asset.nodeParticles.Count <= 0) return;
    //    if (VFXPackageController.Instance == null) return;

    //    for (int i = 0; i < asset.nodeParticles.Count; i++)
    //    {
    //        if (asset.nodeParticles[i].obj == null) continue;

    //        var objParticle = VFXPackageController.Instance.FindParticles(asset.nodeParticles[i].name, asset.nodeParticles[i].type);
    //        if (objParticle == null) return;

    //        objParticle = Instantiate(objParticle, asset.nodeParticles[i].obj.transform);
    //        objParticle.transform.localPosition = Vector3.zero;
    //        objParticle.transform.localRotation = Quaternion.identity;
    //    }
    //}
}