using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WGSBundling.Bundles.Sound;

public class AudioController : MonoBehaviour
{
    //public AudioUnit sound;
    public List<AudioSource> engineSoundSources = new List<AudioSource>();
    public bool engineAudioState;

    // Start is called before the first frame update
    void Start()
    {
        //AnimationCurve am = new AnimationCurve();

        //am.AddKey(0, 1);
        //am.AddKey(200, .5f);
        //am.AddKey(300, .15f);
        //am.AddKey(400, 0);
        //gameObject.TryGetComponent(out audioSource);

        //if (audioSource == null)
        //{
        //    audioSource = gameObject.AddComponent<AudioSource>();
        //}
        //audioSource.spatialBlend = 1;
        //audioSource.rolloffMode = AudioRolloffMode.Linear;
        ////audioSource.SetCustomCurve(AudioSourceCurveType.CustomRolloff, ;
        //audioSource.maxDistance = 300;
        //playAudioEngine();
        NodeSound nSound = gameObject.GetComponent<NodeSound>();

        //foreach (NodeSoundData nDat in nSound.engineSounds)
        //{
        //    if (nDat.audioTrack == null) continue;
        //    ////var animator = _module.doors[i].obj.gameObject.AddComponent<Animator>();

        //    //    // SET DOORS ANIMATION
        //    //    //animator.runtimeAnimatorController = _module.doors[i].animation;
        //    //    //animator.updateMode = AnimatorUpdateMode.Normal;

        //    //AudioSource aSource;

        //    //if (!nDat.audioNode.gameObject.TryGetComponent(out aSource))
        //    //if 
        //    var aSource = (nDat.audioNode == null) ? gameObject.AddComponent<AudioSource>() : nDat.audioNode.gameObject.AddComponent<AudioSource>();
        //    //else
        //        //var aSource = nDat.audioNode.gameObject.AddComponent<AudioSource>();

        //    aSource.playOnAwake = true;
        //    aSource.clip = nDat.audioTrack;
        //    aSource.dopplerLevel = 0;
        //    aSource.spatialBlend = 1;
        //    aSource.rolloffMode = AudioRolloffMode.Linear;
        //    aSource.minDistance = nDat.audioRange.x;
        //    aSource.maxDistance = nDat.audioRange.y;
        //    aSource.loop = true;
        //    //aSource.loop = (nDat.playbackCount < 1) ? true : false;
        //    aSource.volume = 1;
        //    aSource.rolloffMode = AudioRolloffMode.Custom;
        //    //if(aSource.isPlaying) aSource.Pause();

        //    engineSoundSources.Add(aSource);
        //}

        engineAudioState = false;
    }

    // Update is called once per frame
    void Update()
    {

    }

    //public void playAudioStart()
    //{
    //    if (!audioSource || !sound.getSoundStart())
    //        return;

    //    audioSource.clip = sound.getSoundStart();
    //    audioSource.loop = false;
    //    audioSource.Play();
    //}

    //public void playAudioEngine()
    //{
    //    if (!audioSource || !sound.getSoundEngine())
    //        return;

    //    audioSource.clip = sound.getSoundEngine();
    //    audioSource.loop = true;
    //    audioSource.Play();
    //}

    //public void playAudioStop()
    //{
    //    if (!audioSource || !sound.getSoundStop())
    //        return;

    //    audioSource.clip = sound.getSoundStop();
    //    audioSource.loop = false;
    //    audioSource.Play();
    //}

    //public void stopAudio()
    //{
    //    if (!audioSource)
    //        return;

    //    audioSource.Stop();
    //}

    //public void pauseAudio()
    //{
    //    if (!audioSource)
    //        return;

    //    audioSource.Pause();
    //}

    //public void resumeAudio()
    //{
    //    if (!audioSource)
    //        return;

    //    audioSource.Play();
    //}

    //public bool isAudioPlaying()
    //{
    //    if (!audioSource)
    //        return false;

    //    return audioSource.isPlaying;
    //}

    public void audioEnginePlay()
    {
        if (engineSoundSources == null) return;

        engineAudioState = true;

        foreach (AudioSource ac in engineSoundSources)
        {
            if (ac == null || ac.clip == null)
                continue;

            ac.volume = 1;
            if (!ac.isPlaying) ac.Play();
        }

        //Debug.Log("audioEnginePlay() Invoked");
    }

    public void audioEngineStop()
    {
        if (engineSoundSources == null) return;

        engineAudioState = false;

        foreach (AudioSource ac in engineSoundSources)
        {
            if (ac == null || ac.clip == null)
                continue;

            ac.volume = 0;
            if (ac.isPlaying) ac.Stop();
        }
    }
}
