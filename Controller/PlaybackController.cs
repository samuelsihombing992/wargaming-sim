using System;
using System.Collections.Generic;
using System.Globalization;

using UnityEngine;
using UnityEngine.UI;

using TMPro;
using Wargaming.Core.GlobalParam.HelperKegiatan;
using Wargaming.Core.GlobalParam;
using Wargaming.Components.Colyseus;
using Wargaming.Components.Controller;
using FluffyUnderware.Curvy;
using Crest;

namespace Wargaming.Components.Playback
{
    public class PlaybackController : MonoBehaviour
    {
        [Header("TimeLine Scroll")]
        [SerializeField]
        public Slider sliderPlayback;

        [Header("Playback Buttons")]
        [SerializeField] private Button btnPreviousDay;
        [SerializeField] private Button btnStop;
        [SerializeField] private Button btnNextDay;
        [SerializeField] private Toggle btnPlayPause;
        [SerializeField] private Toggle btnSpeedFactor;

        [Header("Header Labels")]
        [SerializeField] private TextMeshProUGUI headerClock;
        [SerializeField] private TextMeshProUGUI headerDate;
        [SerializeField] public TextMeshProUGUI headerHariH;

        [SerializeField] private Toggle toggleTimeCylce;
        [SerializeField] private Toggle toggleMultiplayerSync;

        [Min(0)]
        public int speedFactor = 1;
        private float scale;
        private static bool isPlaying = false;
        private static bool isSliding = false;

        [Header("Multiplayer Non-Sync Mode")]
        public bool isMultiplayerSync = true;
        public Toggle multiplayerSyncToggle;

        [Header("Ocean Timing")]
        public float oceanTimingOffset = 0.25f;
        public bool disableFoamBySpeedFactor;

        public static DateTime CURRENT_TIME = DateTime.Now;

        #region INSTANCE
        public static PlaybackController instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private void Start()
        {
            if (toggleTimeCylce != null)
            {
                isMultiplayerSync = toggleMultiplayerSync.isOn;
                toggleMultiplayerSync.onValueChanged.AddListener(delegate
                {
                    onMultiplayerSyncChange(toggleMultiplayerSync.isOn);
                });
            }
        }

        private void onMultiplayerSyncChange(bool state)
        {
            isMultiplayerSync = state;
            updateHeaderLabel();

            //var missions = MissionController.instance.OBJ_MISSION;
            //for (int i = 0; i < missions.Count; i++)
            //{
            //    if (missions[i] == null) continue;
            //    missions[i].gameObject.SetActive(state);
            //}
        }

        public void Update()
        {
            if (!isPlaying || isSliding || ColyseusTFGSingleController.instance != null) return;
            if (ColyseusTFGController.instance == null && ColyseusTFGSingleController.instance == null && ColyseusWGSController.instance == null)
            {
                // Only Update Itselt When in Singleplayer
                CURRENT_TIME = CURRENT_TIME.AddSeconds(Time.deltaTime * speedFactor);
            }
            //else if(ColyseusTFGSingleController.instance != null)
            //{
            //    for(int i=0; i < PlayerData.PLAYERS.Count; i++)
            //    {
            //        if (PlayerData.PLAYERS[i].timeMovement == null) continue;
            //        PlayerData.PLAYERS[i].timeMovement = PlayerData.PLAYERS[i].timeMovement.GetValueOrDefault().AddSeconds(Time.deltaTime * speedFactor);
            //    }
            //}

            // Update Timeline Slider Value
            updateSlider();
            // Update Header Labels
            updateHeaderLabel();
            // Update Time of Day Value
            //TimeWeatherController.SyncTimeCycle();
            // Update Ocean Timing (For Wave Generator)
            updateOceanTiming();
            // Update Timescale (Speed Factor) Value
            updateTimeScale();

            //if (isPlaying && !isSliding)
            //{
            //    if(ColyseusTFGController.instance == null && ColyseusTFGSingleController.instance == null)
            //    {
            //        CURRENT_TIME = CURRENT_TIME.AddSeconds(Time.deltaTime * speedFactor);
            //        if (sliderPlayback != null) sliderPlayback.value = (float)CURRENT_TIME.Subtract(SkenarioAktif.WaktuMulai).TotalMilliseconds;
            //    }

            //    updateTimeCycle();
            //    updateOceanTiming();
            //    updateTimeScale();
            //}
        }

        public static string GetDateInID(DateTime currentTime)
        {
            //return currentTime.ToString("dd MM yyyy");
            return currentTime.ToString("D", new CultureInfo("id-ID"));
        }

        public static string GetTime(DateTime currentTime, bool useSeconds = true)
        {
            return (!useSeconds ? currentTime.ToString("HH:mm") ?? "" : currentTime.ToString("HH:mm:ss") ?? "");
        }

        public string GetHariH(DateTime startTime)
        {
            var hariH = (int)(startTime - SkenarioAktif.HARI_H.Date).TotalDays;
            return TimetableController.instance.SetLabelHariH(hariH);
        }

        /// <summary>
        /// Preparing Awal Playback
        /// </summary>
        public void InitPlayback(TimetableKegiatanHelper[] kegiatan)
        {
            Debug.Log("Init Playback");
            List<long> list_waktu = new List<long>();

            // Dapatkan Waktu Mulai Dan Berakhir Kegiatan
            for (int i = 0; i < kegiatan.Length; i++)
            {
                if (kegiatan[i].kogas != "false" && kegiatan[i].waktu != "")
                {
                    if (!list_waktu.Contains(kegiatan[i].waktuDT.Ticks)) list_waktu.Add(kegiatan[i].waktuDT.Ticks);
                }
            }

            if (list_waktu.Count >= 2)
            {
                SkenarioAktif.WaktuMulai = new DateTime(list_waktu[0]);
                SkenarioAktif.WaktuAkhir = new DateTime(list_waktu[list_waktu.Count - 1]);

                //CURRENT_TIME = DateTime.Parse("4/29/2022 12:29:30 PM");
                CURRENT_TIME = SkenarioAktif.HARI_H;
                speedFactor = 1;

                var timeDiff = SkenarioAktif.WaktuAkhir.Subtract(SkenarioAktif.WaktuMulai);

                // Setup Slider Timing
                if (sliderPlayback != null)
                {
                    sliderPlayback.minValue = 0;
                    sliderPlayback.maxValue = (float) timeDiff.TotalMilliseconds;
                }

                // Update Header Time UI
                updateHeaderLabel();
            }

            if(ColyseusTFGSingleController.instance != null)
            {
                headerClock.SetText("-");
                headerDate.SetText("-");
            }
        }

        public void updateSlider()
        {
            if (sliderPlayback == null) return;
            sliderPlayback.value = (float)CURRENT_TIME.Subtract(SkenarioAktif.WaktuMulai).TotalMilliseconds;
        }

        public void updateHeaderLabel()
        {
            //if (isMultiplayerSync)
            //{
            //    headerClock.SetText("-");
            //    headerDate.SetText("-");
            //}

            if(ColyseusTFGController.instance != null)
            {
                headerClock.SetText(GetTime(CURRENT_TIME, true));
                //headerDate.SetText(CURRENT_TIME.ToString("dd MM yyyy - HH:mm:ss"));
                return;
            }else if(ColyseusTFGSingleController.instance != null)
            {
                
            }
            else if(ColyseusWGSController.instance != null)
            {

            }
            else
            {
                headerClock.SetText(GetTime(CURRENT_TIME, true));
                headerDate.SetText(GetDateInID(CURRENT_TIME));
            }

            

            //if(ColyseusTFGController.instance != null)
            //{
            //    if(ColyseusTFGController.instance.room == null)
            //    {
            //        headerHariH.SetText(GetHariH(CURRENT_TIME));
            //    }
            //}else if(ColyseusWGSController.instance != null)
            //{

            //}
            //else
            //{
            //    //Debug.Log("Update Header Label");
            //    headerHariH.SetText(GetHariH(CURRENT_TIME));
            //}
        }

        //public void updateTimeCycle()
        //{
        //    if (!TimeWeatherController.useDayCycle) return;

        //    var TIME_WEATHER = TimeWeatherController.instance.TIME_WEATHER;

        //    TIME_WEATHER.m_timeLocal.year = CURRENT_TIME.Year;
        //    TIME_WEATHER.m_timeLocal.month = CURRENT_TIME.Month;
        //    TIME_WEATHER.m_timeLocal.day = CURRENT_TIME.Day;

        //    TIME_WEATHER.m_timeLocal.hour = CURRENT_TIME.Hour;
        //    TIME_WEATHER.m_timeLocal.minute = CURRENT_TIME.Minute;
        //    TIME_WEATHER.m_timeLocal.second = CURRENT_TIME.Second;
        //    TIME_WEATHER.m_timeLocal.millisecond = CURRENT_TIME.Millisecond;

        //    //TimeWeatherController.instance.TIME_WEATHER.TimeOfDay = (float)TimeSpan.Parse(CURRENT_TIME.ToString("HH:mm:ss")).TotalHours;
        //}

        public void updateOceanTiming()
        {
            //return;
            var timeProvider = GetComponent<TimeProviderCustom>();
            if (!timeProvider) return;

            timeProvider._paused = !isPlaying;
            if (isPlaying == false)
            {
                timeProvider._overrideDeltaTime = true;
                timeProvider._deltaTime = 0;
            }
            else if (speedFactor > 30)
            {
                disableFoamBySpeedFactor = true;
            }
            else if (speedFactor > 1)
            {
                disableFoamBySpeedFactor = false;

                if (!timeProvider._overrideDeltaTime) timeProvider._overrideDeltaTime = true;
                timeProvider._deltaTime = oceanTimingOffset * speedFactor;
            }
            else
            {
                if (timeProvider._overrideDeltaTime) timeProvider._overrideDeltaTime = false;
                timeProvider._deltaTime = 0;
            }

            //if (OceanRenderer.Instance._createFoamSim != disableFoamBySpeedFactor)
            //{
            //    OceanRenderer.Instance._createFoamSim = disableFoamBySpeedFactor;
            //}
        }

        public void updateTimeScale(float? custom = null)
        {
            return;
            scale = custom.GetValueOrDefault(speedFactor);

            if(scale > 5)
            {
                scale = 5;
            }

            Time.timeScale = scale;
        }

        /// <summary>
        /// Playback Action dari Colyseus
        /// </summary>
        /// <param name="action"></param>
        public void OnPlaybackAction(string action)
        {
            switch (action)
            {
                case "stop":
                    btnStop.onClick.Invoke();
                    //CURRENT_TIME = SkenarioAktif.WaktuMulai;
                    //isPlaying = false;
                    break;
                case "play":
                    btnPlayPause.isOn = true;
                    //isPlaying = true;
                    break;
                case "start":
                    btnPlayPause.isOn = true;
                    //isPlaying = true;
                    break;
                case "pause":
                    btnPlayPause.isOn = false;
                    //isPlaying = false;
                    //updateTimeScale(0);
                    break;
                case "back":
                    CURRENT_TIME.AddDays(-1);
                    break;
                case "next":
                    CURRENT_TIME.AddDays(1);
                    break;
            }

            updateOceanTiming();
            updateHeaderLabel();
            //TimeWeatherController.SyncTimeCycle();
        }

        public static bool IsPlaying()
        {
            if(!isPlaying || isSliding) { return false; }

            return true;
        }

        #region -- PLAYBACK BUTTON REGION --
        public void OnPreviousDayClick()
        {
            CURRENT_TIME = CURRENT_TIME.AddDays(-1);
            Update();
        }

        public void OnNextDayClick()
        {
            CURRENT_TIME = CURRENT_TIME.AddDays(1);
            Update();
        }

        public void OnStopClick()
        {
            CURRENT_TIME = SkenarioAktif.WaktuMulai;
            Update();
        }

        public void OnPlayPauseToggle()
        {
            isPlaying = btnPlayPause.isOn;
            //if(!isPlaying) updateTimeScale(0);
            Update();
        }
        #endregion

        #region -- SLIDER REGION --
        /// <summary>
        /// Event Ketika Slider Di-Slide Secara Manual
        /// </summary>
        /// <param name="value"></param>
        public void SliderOnValueChanged()
        {
            if (!isSliding) return;
            CURRENT_TIME = SkenarioAktif.WaktuMulai.AddMilliseconds(sliderPlayback.value);

            //TimeWeatherController.SyncTimeCycle();
            updateHeaderLabel();
        }

        public void OnSliderBeginDrag()
        {
            isSliding = true;
        }

        public void OnSliderEndDrag()
        {
            isSliding = false;
        }
        #endregion

        #region -- SPEED FACTOR REGION --
        /// <summary>
        /// Set Percepatan
        /// </summary>
        /// <param name="speed"></param>
        public void SetSpeedFactor(int speed)
        {
            speedFactor = speed;
        }

        public void SetSpeedFactor(TMP_InputField input)
        {
            int speed = 0;
            int.TryParse(input.text, out speed);

            speedFactor = speed;
        }

        public void increaseSpeedFactor(int speed)
        {
            speedFactor += speed;

            if (speedFactor <= 0) speedFactor = 1;
        }
        #endregion
    }

    public class UserPlayback
    {
        public DateTime? CURRENT_TIME;
        public long? id;
    }
}