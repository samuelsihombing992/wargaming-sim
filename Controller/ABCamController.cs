using UnityEngine;

using WGSBundling.Core;

public class ABCamController : MonoBehaviour
{
    [Header("Settings")]
    public Vector2 rotationLimit;

    public Vector2 zoomLimit;
    public float zoomSensitive;

    [System.NonSerialized] public WGSBundleCore _target;

    public Vector3 lastPosition;
    public Vector3 direction;

    public Vector3 movement;
    public Vector3 rotation;
    public Vector2 rotLimit;

    private GameObject camNode;
    private Transform floorParent;

    private Vector3 resetDirection;
    private Vector3 resetRotation;

    #region INSTANCE
    public static ABCamController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    void Start()
    {
        camNode = transform.GetChild(0).gameObject;
        floorParent = transform.parent;

        direction = new Vector3(0, 0, 100);
        lastPosition = Input.mousePosition;

        rotation = new Vector3(140, -30, 20);
        rotLimit = new Vector2(rotationLimit.x * -1, rotationLimit.y * -1);

        resetDirection = direction;
        resetRotation = rotation;
    }

    void Update()
    {
        Vector3 mouseDelta = Input.mousePosition - lastPosition;
        if (Input.GetMouseButton(0) && !UICanvasSettings.IsRaycastingWithUI())
        {
            movement += new Vector3(mouseDelta.x * 0.1f, mouseDelta.y * 0.05f, 0F);
        }

        if (!UICanvasSettings.IsRaycastingWithUI())
        {
            movement.z += Input.GetAxis("Mouse ScrollWheel") * -2.5F * zoomSensitive;
        }

        rotation += movement;
        rotation.x = rotation.x % 360.0f;
        CamRotation();

        direction.z = Mathf.Clamp(movement.z + direction.z, zoomLimit.x, zoomLimit.y);
        camNode.transform.position = floorParent.transform.position + Quaternion.Euler(180F - rotation.y, rotation.x, 0) * direction;
        camNode.transform.LookAt(floorParent.transform.position);

        lastPosition = Input.mousePosition;
        movement *= 0.5F;

        zoomLimit = new Vector2(5, zoomLimit.y);
    }

    private void CamRotation()
    {
        rotation.y = Mathf.Clamp(rotation.y, rotLimit.x, rotLimit.y);
    }

    public void ResetCam()
    {
        direction = resetDirection;
        rotation = resetRotation;
    }
}
