using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;
using System.IO;
using Wargaming.Core.GlobalParam;
using System.Xml.Serialization;
using Wargaming.Core;

public class SettingsController : MonoBehaviour
{
    [Header("Settings File")]
    public string settingsFile;

    [Header("Form Data")]
    public TMP_Dropdown monitor;
    public TMP_Dropdown resolution;
    public TMP_Dropdown display;
    public TMP_Dropdown vsync;
    public TMP_Dropdown antiAliasing;

    private AppSettingsData settings;
    private Resolution[] resolutionData;

    #region INSTANCE
    public static SettingsController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        //GetSavedSettings();

        GetMonitorList();
        GetResolutionList();
        GetVSync();
        GetAntiAliasing();
    }

    private void GetMonitorList()
    {
        List<string> data = new List<string>();

        for (int i = 0; i < Display.displays.Length; i++)
        {
            data.Add("Monitor " + (i+1));
        }

        data.Add("Dual Monitor");
        data.Add("Triple Monitor");

        monitor.ClearOptions();
        monitor.AddOptions(data);
    }

    private void GetResolutionList()
    {
        int used = -1;

        List<string> data = new List<string>();
        resolutionData = Screen.resolutions;

        for(int i=0; i < resolutionData.Length; i++)
        {
            if(resolutionData[i].width == Screen.width && resolutionData[i].height == Screen.height)
            {
                used = i;
            }

            data.Add(resolutionData[i].width + "x" + resolutionData[i].height + " (" + resolutionData[i].refreshRate + "Hz)");
        }

        resolution.ClearOptions();
        resolution.AddOptions(data);

        resolution.value = (used != -1) ? used : resolution.options.Count - 1;
    }

    private FullScreenMode GetDisplay(int? display)
    {
        switch (display.GetValueOrDefault(0))
        {
            case 0:
                return FullScreenMode.ExclusiveFullScreen;
            case 1:
                return FullScreenMode.Windowed;
            case 2:
                return FullScreenMode.FullScreenWindow;
            default:
                return FullScreenMode.ExclusiveFullScreen;

        }
    }

    private void GetVSync()
    {
        vsync.value = Mathf.Clamp(QualitySettings.vSyncCount, 0, 1);
    }

    private void GetAntiAliasing()
    {
        if (QualitySettings.antiAliasing == 0)
        {
            antiAliasing.value = 0;
            return;
        }

        if(QualitySettings.antiAliasing == 2)
        {
            antiAliasing.value = 1;
        }else if(QualitySettings.antiAliasing == 4)
        {
            antiAliasing.value = 2;
        }else if(QualitySettings.antiAliasing == 8)
        {
            QualitySettings.anisotropicFiltering = AnisotropicFiltering.Enable;
            antiAliasing.value = 3;
        }
        else
        {
            antiAliasing.value = 0;
        }
    }

    private int GetSelectedAntiAliasing()
    {
        if(antiAliasing.value == 0) return 0;

        if(antiAliasing.value == 1)
        {
            return 2;
        }else if(antiAliasing.value == 2)
        {
            return 4;
        }else if(antiAliasing.value == 3)
        {
            return 8;
        }
        else
        {
            return 0;
        }
    }

    public void ApplySettings()
    {
        Screen.SetResolution(resolutionData[resolution.value].width, resolutionData[resolution.value].height, GetDisplay(display.value));
        QualitySettings.vSyncCount = vsync.value;
        QualitySettings.antiAliasing = GetSelectedAntiAliasing();
    }

    //private void GetMonitorDisplay()
    //{
    //    List<string> monitorOption = new List<string>();
    //    Debug.Log("Total Display Connection : " + Display.displays.Length);

    //    for(int i=0; i < Display.displays.Length; i++)
    //    {
    //        monitorOption.Add("Monitor " + i + 1);
    //    }

    //    monitorOption.Add("Dual Monitor");
    //    monitorOption.Add("Triple Monitor");

    //    monitor.ClearOptions();
    //    monitor.AddOptions(monitorOption);
    //}

    //private void GetResolutionDisplay()
    //{
    //    List<string> resolutionOption = new List<string>();
    //    resolutionData = Screen.resolutions;

    //    foreach (var res in resolutionData)
    //    {
    //        Debug.Log(res.width + "x" + res.height + "(" + res.refreshRate + "Hz)");

    //        resolutionOption.Add(res.width + "x" + res.height + " (" + res.refreshRate + "Hz)");
    //    }


    //    resolution.ClearOptions();
    //    resolution.AddOptions(resolutionOption);
    //}

    //private void OnResolutionChange()
    //{
    //    var selected = display.options[display.value].text;
    //}

    //private void GetSavedSettings()
    //{
    //    // If Services File is not defined, throw error message
    //    if (settingsFile == null)
    //    {
    //        Debug.LogError("Error, Settings File is not defined. Please provide one!");
    //        return;
    //    }

    //    var path = Path.Combine(Directory.GetCurrentDirectory(), settingsFile);
    //    if (File.Exists(path))
    //    {
    //        // Use Current Connection Document if exists
    //        var _XMLSerialize = new XmlSerializer(typeof(AppSettingsData));
    //        using (var reader = new StreamReader(path))
    //        {
    //            settings = (AppSettingsData)_XMLSerialize.Deserialize(reader);
    //            new AppSettings(settings);
    //        }
    //    }
    //    else
    //    {
    //        settings = new AppSettingsData
    //        {
    //            monitor = "MONITOR 1",
    //            resolution = Screen.width + "x" + Screen.height,
    //            display = 0,
    //            vsync = 0,
    //            anti_aliasing = 4,
    //            anisotropic_filtering = 16
    //        };

    //        SaveChanged();
    //    }

    //    new AppSettings(settings);
    //    Refresh();
    //}

    //private void Refresh()
    //{
    //    var resActive = GetResolution(AppSettings.RESOLUTION);
    //    var displayActive = GetDisplay(AppSettings.DISPLAY);

    //    Screen.SetResolution(
    //        NumberConvertionHelper.ConvertToInt(resActive[0]),
    //        NumberConvertionHelper.ConvertToInt(resActive[1]),
    //        displayActive
    //    );

    //    QualitySettings.vSyncCount = vsync.value;
    //    QualitySettings.antiAliasing = antiAliasing.value;
    //}

    //private string[] GetResolution(string _res)
    //{
    //    char[] splitter = { 'x', ' '};

    //    return _res.Split(splitter);
    //}

    //private int GetAnisotropicFiltering(string anisotropicFiltering)
    //{
    //    if (anisotropicFiltering.ToLower().Equals("off"))
    //    {
    //        return 0;
    //    }

    //    return NumberConvertionHelper.ConvertToInt(anisotropicFiltering.Split('x')[0]);
    //}



    //public void ApplySettings()
    //{
    //    var resSelected = GetResolution(resolution.GetSelected().text);
    //    var aniFilSeleced = GetAnisotropicFiltering(anisotropicFiltering.GetSelected().text);

    //    settings = new AppSettingsData
    //    {
    //        monitor = monitor.GetSelected().text,
    //        resolution = resSelected[0] + "x" + resSelected[1],
    //        display = display.value,
    //        vsync = vsync.value,
    //        anti_aliasing = vsync.value,
    //        anisotropic_filtering = aniFilSeleced
    //    };

    //    new AppSettings(settings);

    //    SaveChanged();
    //    Refresh();
    //}

    //public void SetVSync()
    //{

    //}

    //public void SetAntiAliasing()
    //{
    //    if (vsync.value == 0)
    //    {
    //        //Use Anti-Aliasing
    //    }
    //    else
    //    {
    //        // Disable
    //    }
    //}

    //public void SetAnisotropicFiltering()
    //{

    //}

    //private void SaveChanged()
    //{
    //    // Serialize and Write Changed into Service Document
    //    var _XMLSerialize = new XmlSerializer(typeof(AppSettingsData));
    //    using (var writer = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), settingsFile)))
    //    {
    //        _XMLSerialize.Serialize(writer, settings);
    //    }
    //}
}
