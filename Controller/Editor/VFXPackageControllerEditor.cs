#if UNITY_EDITOR
using UnityEngine;

#region Inspector (Editor)
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(VFXPackageController))]
public class VFXPackageControllerEditor : BundlingEditor
{
    #region Variables
    protected VFXPackageController controller;

    bool _defaultVFX = true;
    bool _customVFX = false;

    SerializedProperty _jetEngine;
    SerializedProperty _jetTrail;
    SerializedProperty _propsTrail;
    SerializedProperty _wingsTrail;
    SerializedProperty _tireDust;
    SerializedProperty _shipFunnel;

    ReorderableList OL_CustomVFX;
    #endregion
    //ReorderableList OL_JetEngine, OL_Trail;

    private void OnEnable()
    {
        //if (target == null) return;
        controller = (VFXPackageController)target;

        _jetEngine = serializedObject.FindProperty("jetEngine");
        _jetTrail = serializedObject.FindProperty("jetTrail");
        _propsTrail = serializedObject.FindProperty("propsTrail");
        _wingsTrail = serializedObject.FindProperty("wingsTrail");
        _tireDust = serializedObject.FindProperty("tireDust");
        _shipFunnel = serializedObject.FindProperty("shipFunnel");

        OL_CustomVFX = new ReorderableList(serializedObject, serializedObject.FindProperty("customVFX"), true, true, true, true);
        OL_CustomVFX.drawHeaderCallback     = DrawHeader;
        OL_CustomVFX.drawElementCallback    = DrawListItems_CustomVFX;

        //OL_JetEngine = new ReorderableList(serializedObject, serializedObject.FindProperty("jetEngine"), true, true, true, true);
        //OL_Trail = new ReorderableList(serializedObject, serializedObject.FindProperty("trail"), true, true, true, true);

        //OL_JetEngine.drawHeaderCallback = DrawHeader;
        //OL_JetEngine.drawElementCallback = DrawListItems_JetEngine;

        //OL_Trail.drawHeaderCallback = DrawHeader;
        //OL_Trail.drawElementCallback = DrawListItems_Trail;
    }

    void DrawHeader(Rect rect)
    {
        EditorGUI.LabelField(rect, "Parameters");
    }

    void DrawListItems_CustomVFX(Rect rect, int index, bool isActive, bool isFocused)
    {
        SerializedProperty element = OL_CustomVFX.serializedProperty.GetArrayElementAtIndex(index);

        EditorGUI.PropertyField(
          new Rect(rect.x, rect.y, (rect.width - 32) / 2, EditorGUIUtility.singleLineHeight),
          element.FindPropertyRelative("name"), GUIContent.none
        );

        EditorGUI.PropertyField(
          new Rect(rect.x + ((rect.width - 32) / 2) + 2, rect.y, (rect.width) / 2 + 16, EditorGUIUtility.singleLineHeight),
          element.FindPropertyRelative("prefab"), GUIContent.none
        );
    }

    //void DrawListItems_Trail(Rect rect, int index, bool isActive, bool isFocused)
    //{
    //    SerializedProperty element = OL_Trail.serializedProperty.GetArrayElementAtIndex(index);

    //    EditorGUI.PropertyField(
    //      new Rect(rect.x, rect.y, (rect.width - 32) / 2, EditorGUIUtility.singleLineHeight),
    //      element.FindPropertyRelative("name"), GUIContent.none
    //    );

    //    EditorGUI.PropertyField(
    //      new Rect(rect.x + ((rect.width - 32) / 2) + 2, rect.y, (rect.width) / 2 + 16, EditorGUIUtility.singleLineHeight),
    //      element.FindPropertyRelative("prefab"), GUIContent.none
    //    );
    //}

    public override void OnInspectorGUI()
    {
        PrepareInspector();
        DrawDefaultInspector();

        //EditorGUILayout.Space();
        //serializedObject.Update(); // Update the array property's representation in the inspector

        //EditorGUILayout.LabelField("Jet Engine", EditorStyles.boldLabel);
        //OL_JetEngine.DoLayoutList(); // Have the ReorderableList do its work
        //EditorGUILayout.LabelField("Trail", EditorStyles.boldLabel);
        //OL_Trail.DoLayoutList();


        EditorGUILayout.Space();
        serializedObject.Update(); // Update the array property's representation in the inspector

        EditorGUILayout.BeginVertical("HelpBox");
        _defaultVFX = SetContainerTitle(_defaultVFX, "Default VFX");
        if (_defaultVFX)
        {
            EditorGUILayout.BeginVertical(boxStyle);
                EditorGUILayout.BeginVertical(helpBoxStyle);
                     SetSubtitle("Aircraft VFX", true);
                    _jetEngine.objectReferenceValue = EditorGUILayout.ObjectField("Jet Engine", _jetEngine.objectReferenceValue, typeof(GameObject), true) as GameObject;
                    _jetTrail.objectReferenceValue = EditorGUILayout.ObjectField("Jet Trail", _jetTrail.objectReferenceValue, typeof(GameObject), true) as GameObject;
                    _propsTrail.objectReferenceValue = EditorGUILayout.ObjectField("Props Trail", _propsTrail.objectReferenceValue, typeof(GameObject), true) as GameObject;
                    _wingsTrail.objectReferenceValue = EditorGUILayout.ObjectField("Wings Trail", _wingsTrail.objectReferenceValue, typeof(GameObject), true) as GameObject;
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical(helpBoxStyle);
                     SetSubtitle("Vehicle VFX", true);
                    _tireDust.objectReferenceValue = EditorGUILayout.ObjectField("Tire Dust", _tireDust.objectReferenceValue, typeof(GameObject), true) as GameObject;
                EditorGUILayout.EndVertical();
                EditorGUILayout.BeginVertical(helpBoxStyle);
                     SetSubtitle("Ship VFX", true);
                    _shipFunnel.objectReferenceValue = EditorGUILayout.ObjectField("Funnel Smoke", _shipFunnel.objectReferenceValue, typeof(GameObject), true) as GameObject;
                EditorGUILayout.EndVertical();
            EditorGUILayout.EndVertical();
        }
        EditorGUILayout.EndVertical();

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Custom VFX", EditorStyles.boldLabel);
        OL_CustomVFX.DoLayoutList();


        // We need to call this so that changes on the Inspector are saved by Unity.
        serializedObject.ApplyModifiedProperties();
    }
}
#endregion
#endif