using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Serialization;

using UnityEngine;
using UnityEngine.UI;

using TMPro;
using WGSDatapack.Core;
using Wargaming.Core;

using Esri.ArcGISMapsSDK.Components;
using Esri.GameEngine.Geometry;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;

public class DatapackPackageController : MonoBehaviour
{
    [Header("Sample Template")]
    [SerializeField] private BuildingPlacementData sampleTemplate;

    [Header("Format")]
    public string datapackFormat = "datapack";
    public string templateFormat = "dptemplate";

    [Header("DATA")]
    [SerializeField]
    public List<string> DATAPACK_NAME = new List<string>();

    [SerializeField]
    public List<WGSDatapackCore> DATAPACK_GROUP = new List<WGSDatapackCore>();
    public List<GameObject> DATAPACK_ASSET = new List<GameObject>();

    [SerializeField]
    public List<DatapackTemplate> DATAPACK_TEMPLATES = new List<DatapackTemplate>();
    public List<BuildingPlacementData> DEBUG_BUILD_DATA = new List<BuildingPlacementData>();
    
    //[SerializeField]
    //private string _dir;

    #region INSTANCE
    public static DatapackPackageController Instance;
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            Instance.transform.parent = null;
            DontDestroyOnLoad(Instance);
        }
        else
        {
            Destroy(gameObject);
        }

        //locateDatapackDirectory();
    }
    #endregion

    /// <summary>
    /// -- Cari Lokasi Directory (folder) asset 3D disimpan --
    /// </summary>
    private async void locateDatapackDirectory()
    {
        //if (datapackRef == null) return;

        //switch (datapackLocation)
        //{
        //    case DatapackLocation.DataPath:
        //        _dir = Directory.GetCurrentDirectory() + "/Datapacks";
        //        break;
        //}

        //await loadDatapack();
        //await loadDatapackTemplate();

        for (int i = 0; i < DEBUG_BUILD_DATA.Count; i++)
        {
            SaveDatapackTemplate(DEBUG_BUILD_DATA[i]);
        }
    }

    public async Task loadDatapackTemplate()
    {
        Debug.Log(StartupConfig.GetDatapackDirectory());

        if (!Directory.Exists(StartupConfig.GetDatapackDirectory()))
        {
            Debug.LogError("Directory tidak ditemukan!");
            return;
        }

        string[] bundleFolder = Directory.GetDirectories(StartupConfig.GetDatapackDirectory());

        // Set Loading Message & Progress
        Metadata loadingMessage = (SceneController.activeLoading) ? SceneController.activeLoading.GetComponent<Metadata>() : null;
        Image loadingProgress = null;

        if (loadingMessage)
        {
            loadingMessage.FindParameter("message").GetComponent<TextMeshProUGUI>().text = "Loading Datapack Templates...";

            loadingProgress = loadingMessage.FindParameter("progress-bar").GetComponent<Image>();
            loadingProgress.fillAmount = 0.75f;
        }

        if (bundleFolder.Length == 0)
        {
            Debug.Log("No Datapack Template Found");
            SaveDatapackTemplate(sampleTemplate);

            if (loadingProgress != null) loadingProgress.fillAmount += 1f;

            return;
        }

        // LOADING ALL DATAPACK TEMPLATE
        string[] bundleFiles = Directory.GetFiles(StartupConfig.GetDatapackDirectory(), "*." + templateFormat);

        int bundleFinishedLoad = 0;
        if (bundleFiles.Length >= 0)
        {
            for (int j = 0; j < bundleFiles.Length; j++)
            {
                if (loadingProgress != null) loadingProgress.fillAmount += (0.25f / bundleFiles.Length);

                try
                {
                    bundleFinishedLoad += await SyncLoadTemplate(bundleFiles[j], StartupConfig.GetDatapackDirectory());
                }
                catch (Exception e)
                {
                    Debug.LogError("Failed To Load Datapack Template " + bundleFiles[j]);
                    Debug.LogError(e.Message);
                }

            }
        }
        else
        {
            if (loadingProgress != null) loadingProgress.fillAmount += (0.5f / bundleFolder.Length);
        }

        if (loadingProgress != null) loadingProgress.fillAmount += (1f / bundleFolder.Length);
    }

    async Task<int> SyncLoad(string file, string url)
    {
        var fileStream = new FileStream(Path.Combine(url, file), FileMode.Open, FileAccess.Read);
        var BundleLoaded = AssetBundle.LoadFromStreamAsync(fileStream);

        while (!BundleLoaded.isDone) await Task.Yield();

        if (BundleLoaded.assetBundle == null)
        {
            Debug.LogError("Failed to load Datapack!");
            return 0;
        }

        var AssetInQueue = BundleLoaded.assetBundle.LoadAllAssetsAsync<GameObject>();
        while (!AssetInQueue.isDone) await Task.Yield();

        for (int i = 0; i < AssetInQueue.allAssets.Length; i++)
        {
            var GO = AssetInQueue.allAssets[i] as GameObject;
            var core = GO.GetComponent<WGSDatapackCore>();
            string datapackID = core.id.ToLower();

            if (!DATAPACK_NAME.Contains(datapackID))
            {
                DATAPACK_NAME.Add(datapackID);
                DATAPACK_GROUP.Add(core);

                for (int j = 0; j < core.datapack.Count; j++)
                {
                    DATAPACK_ASSET.Add(core.datapack[j].gameObject);
                }
            }
        }

        BundleLoaded.assetBundle.Unload(false);
        return 1;
    }

    public async Task loadDatapack()
    {
        Debug.Log(StartupConfig.GetDatapackDirectory());

        if (!Directory.Exists(StartupConfig.GetDatapackDirectory()))
        {
            Debug.LogError("Directory tidak ditemukan!");
            return;
        }

        string[] bundleFolder = Directory.GetDirectories(StartupConfig.GetDatapackDirectory());

        // Set Loading Message & Progress
        Metadata loadingMessage = (SceneController.activeLoading) ? SceneController.activeLoading.GetComponent<Metadata>() : null;
        Image loadingProgress = null;

        if (loadingMessage)
        {
            loadingMessage.FindParameter("message").GetComponent<TextMeshProUGUI>().text = "Loading Datapacks...";

            loadingProgress = loadingMessage.FindParameter("progress-bar").GetComponent<Image>();
            loadingProgress.fillAmount = 0.25f;
        }

        if (bundleFolder.Length == 0)
        {
            Debug.Log("No Datapack Found");
            if (loadingProgress != null) loadingProgress.fillAmount += 0.75f;

            return;
        }

        // LOADING ALL DATAPACK ASSETS
        for (int i = 0; i < bundleFolder.Length; i++)
        {
            string[] bundleFiles = Directory.GetFiles(Path.Combine(bundleFolder[i]), "*." + datapackFormat);

            int bundleFinishedLoad = 0;
            if (bundleFiles.Length >= 0)
            {
                // Load and instantiate seluruh pack asset ke "Don't Destroy" scene
                for (int j = 0; j < bundleFiles.Length; j++)
                {
                    if (loadingProgress != null) loadingProgress.fillAmount += ((0.5f / bundleFiles.Length) / bundleFolder.Length);

                    try
                    {
                        bundleFinishedLoad += await SyncLoad(bundleFiles[j], Path.Combine(bundleFolder[i]));
                    }
                    catch (Exception e)
                    {
                        Debug.LogError("Failed To Load Datapack " + bundleFiles[j]);
                        Debug.LogError(e.Message);
                    }

                }
            }
            else
            {
                if (loadingProgress != null) loadingProgress.fillAmount += (0.5f / bundleFolder.Length);
            }

            //if (loadingProgress != null) loadingProgress.fillAmount += (1f / bundleFolder.Length);
        }
    }

    async Task<int> SyncLoadTemplate(string file, string url)
    {
        if(File.Exists(file))
        {
            DatapackTemplate loaded;

            // Use Current Datapack Template Document if exists
            var _XMLSerialize = new XmlSerializer(typeof(DatapackTemplate));
            using (var reader = new StreamReader(Path.Combine(url, file)))
            {
                loaded = (DatapackTemplate) _XMLSerialize.Deserialize(reader);
            }

            // Add Datapack Template Document to Datapack Template List
            if (!DATAPACK_TEMPLATES.Contains(loaded))
            {
                DATAPACK_TEMPLATES.Add(loaded);
            }

            return 1;
        }
        else
        {
            return 0;
        }
    }

    public void SpawnDatapackOnTemplate()
    {
        var templateGroup = GameObject.FindGameObjectWithTag("datapack-group");
        if (templateGroup == null)
        {
            Debug.LogError("Failed to Spawn Datapack because there is no templateGroup provided!");
            return;
        }

        for (int i=0; i < DATAPACK_TEMPLATES.Count; i++)
        {
            SpawnDatapackOnTemplate(DATAPACK_TEMPLATES[i], templateGroup.transform);
        }
    }

    public void SpawnDatapackOnTemplate(DatapackTemplate template, Transform parent)
    {
        var templateParent = new GameObject();
        templateParent.transform.parent = parent;

        templateParent.name = template.name;

        for (int i=0; i < template.data.Count; i++)
        {
            var dataFound = DATAPACK_ASSET.Find(r => r.name == template.data[i].objID);
            if (dataFound == null)
            {
                Debug.LogError("Cannot load datapack asset " + template.data[i].objID);
                continue;
            }

            var objOnTemplate = Instantiate(dataFound, templateParent.transform);
            var objOnTemplateLocation = objOnTemplate.AddComponent<ArcGISLocationComponent>();

            objOnTemplateLocation.Position = new ArcGISPoint(template.data[i].posX, template.data[i].posY, template.data[i].posZ, ArcGISSpatialReference.WGS84());
            objOnTemplateLocation.Rotation = new ArcGISRotation(template.data[i].rotX, template.data[i].rotY, template.data[i].rotZ);
        }
    }

    private void SaveDatapackTemplate(BuildingPlacementData template)
    {
        if(template == null)
        {
            Debug.LogError("Failed to Save Template. No Data Found!");
            return;
        }

        List<PlacementObjectExport> DATA = new List<PlacementObjectExport>();
        for (int i = 0; i < template.DATA.Count; i++)
        {
            try
            {
                DATA.Add(new PlacementObjectExport
                {
                    objID   = template.DATA[i].obj.name,
                    posX    = template.DATA[i].location.x,
                    posY    = template.DATA[i].location.y,
                    posZ    = template.DATA[i].location.z,
                    rotX    = template.DATA[i].rotation.x,
                    rotY    = template.DATA[i].rotation.y,
                    rotZ    = template.DATA[i].rotation.z
                });
            }
            catch (Exception e)
            {
                Debug.LogError("Failed to Save Template");
                break;
            }
        }

        if (DATAPACK_TEMPLATES.Find(r => r.name == template.id) != null) return;

        DatapackTemplate newTemplate = new DatapackTemplate { name = template.id, data = DATA };

        // Serialize and Write Changed into Datapack Template Document
        var _XMLSerialize = new XmlSerializer(typeof(DatapackTemplate));
        using (var writer = new StreamWriter(Path.Combine(StartupConfig.GetDatapackDirectory(), newTemplate.name) + "." + templateFormat))
        {
            _XMLSerialize.Serialize(writer, newTemplate);
        }

        DATAPACK_TEMPLATES.Add(newTemplate);
    }

    //public void BuildTemplate(BuildingPlacementData template)
    //{
    //    if (template == null) return;

    //    List<PlacementObjectExport> tempDataExport = new List<PlacementObjectExport>();
    //    for(int i=0; i < template.DATA.Count; i++)
    //    {
    //        try
    //        {
    //            tempDataExport.Add(new PlacementObjectExport
    //            {
    //                objID = template.DATA[i].obj.name,
    //                posX = template.DATA[i].location.x,
    //                posY = template.DATA[i].location.y,
    //                posZ = template.DATA[i].location.z,
    //                rotX = template.DATA[i].rotation.x,
    //                rotY = template.DATA[i].rotation.y,
    //                rotZ = template.DATA[i].rotation.z
    //            });
    //        }catch(Exception e)
    //        {
    //            Debug.LogError("Failed to Save Template");
    //            return;
    //        }
    //    }

    //    var dataFound = DATAPACK_TEMPLATES.Find(r => r.name == template.id);
    //    bool isFound = true;

    //    if(dataFound == null)
    //    {
    //        isFound = false;
    //        dataFound = new DatapackTemplate
    //        {
    //            name = template.id,
    //            data = tempDataExport
    //        };
    //    }

    //    string _dir = "";
    //    switch (datapackLocation)
    //    {
    //        case DatapackLocation.DataPath:
    //            _dir = Directory.GetCurrentDirectory() + "/Datapacks";
    //            break;
    //    }

    //    if (!Directory.Exists(_dir)){
    //        Directory.CreateDirectory(_dir);
    //    }

    //    // Serialize and Write Changed into Datapack Template Document
    //    var _XMLSerialize = new XmlSerializer(typeof(DatapackTemplate));
    //    using (var writer = new StreamWriter(Path.Combine(_dir, dataFound.name) +  "." + TemplateFormat))
    //    {
    //        _XMLSerialize.Serialize(writer, dataFound);
    //    }

    //    if (!isFound)
    //    {
    //        DATAPACK_TEMPLATES.Add(dataFound);
    //    }
    //}

    [Serializable]
    public class DatapackTemplate
    {
        [XmlAttribute("template_name")]
        public string name { get; set; }
        [XmlElement("data")]
        public List<PlacementObjectExport> data { get; set; }
    }

    [System.Serializable]
    public class PlacementObjectExport
    {
        [XmlAttribute("objID")]
        public string objID { get; set; }

        [XmlAttribute("posX")]
        public float posX { get; set; }
        [XmlAttribute("posY")]
        public float posY { get; set; }
        [XmlAttribute("posZ")]
        public float posZ { get; set; }

        [XmlAttribute("rotX")]
        public float rotX { get; set; }
        [XmlAttribute("rotY")]
        public float rotY { get; set; }
        [XmlAttribute("rotZ")]
        public float rotZ { get; set; }
    }
}
