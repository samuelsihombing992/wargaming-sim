using System;
using UnityEngine;
using UnityEngine.UI;

using TMPro;

using CoordinateSharp;
using Esri.ArcGISMapsSDK.Components;
using FluffyUnderware.Curvy.Controllers;
using Esri.HPFramework;
using WGSBundling.Core;
using static Wargaming.Core.GlobalParam.EntityHelper;
using SickscoreGames.HUDNavigationSystem;
using UnityEngine.SceneManagement;

public class DetailEntityController : MonoBehaviour
{
    [Header("Header Group")]
    public TextMeshProUGUI headerTitle;

    [Header("References Detail Satuan")]
    public RectTransform uiSatuanGroup;

    [Header("Data Satuan")]
    public TextMeshProUGUI namaSatuan;
    public TextMeshProUGUI jenisSatuan;
    public TextMeshProUGUI userSatuan;

    [Header("Info & Coordinate Satuan")]
    public TextMeshProUGUI latLngSatuan;
    public TextMeshProUGUI mgrsSatuan;
    public TextMeshProUGUI utmSatuan;
    public TextMeshProUGUI speedSatuan;
    public TextMeshProUGUI speedTypeSatuan;
    public TextMeshProUGUI headingSatuan;
    public RectTransform headingSatuanRotator;
    public TextMeshProUGUI altitudeSatuan;
    public Slider altitudeSliderSatuan;
    public TextMeshProUGUI bankSatuan;
    public RectTransform bankSatuanRotator;
    public TextMeshProUGUI tiltSatuan;
    public RectTransform tiltSatuanRotator;

    [Header("Icon Satuan Sideview")]
    public Metadata bankIndicator;
    public Metadata tiltIndicator;

    [Header("Viewer 3D")]
    public RawImage viewer3DImage;
    public Transform viewer3DContainer;
    public View3DCamController viewer3DCam;

    public ObjekSatuan objSatuan;
    private Transform parentObjSatuan;

    public bool openByPointer = false;

    private EagerLoad el;

    #region INSTANCE
    public static DetailEntityController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        openByPointer = false;
        el = new EagerLoad(EagerLoadType.UTM_MGRS);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && openByPointer)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if(hit.transform.GetComponent<ObjekSatuan>() != null)
                {

                    initObjSatuan(hit.transform.GetComponent<ObjekSatuan>());
                }
            }
        }

        if (objSatuan != null)
        {
            updateDetailSatuan();
        }
    }

    private void updateDetailSatuan()
    {
        if (!uiSatuanGroup.gameObject.activeSelf)
        {
            uiSatuanGroup.gameObject.SetActive(true);

            headerTitle.text = objSatuan.nama;

            namaSatuan.text = objSatuan.info.namaSatuan;
            jenisSatuan.text = objSatuan.kategori.ToString();
            userSatuan.text = objSatuan.userID.ToString();

            altitudeSliderSatuan.minValue = 0;
            altitudeSliderSatuan.maxValue = 10000;
            return;
        }

        var objLocation = objSatuan.GetComponent<ArcGISLocationComponent>();
        Coordinate c = new Coordinate(objLocation.Position.Y, objLocation.Position.X, DateTime.Now, el);

        latLngSatuan.text = c.Latitude.ToString() + "," + c.Longitude.ToString();
        mgrsSatuan.text = c.MGRS.ToString();
        utmSatuan.text = c.UTM.ToString();

        var objMission = objSatuan.GetComponent<SplineController>();
        if(objMission.Spline != null)
        {
            speedSatuan.text = ((int) objMission.Spline.GetComponent<RootMissionWalker>().speed).ToString();
            speedTypeSatuan.text = "kmh";
        }

        var heading = (int) objLocation.Rotation.Heading;
        headingSatuan.text = heading.ToString();
        headingSatuanRotator.rotation = Quaternion.Euler(0, 180, heading);

        var altitude = (int) objLocation.Position.Z;
        altitudeSatuan.text = altitude.ToString();
        altitudeSliderSatuan.value = altitude;

        //var bank = (int)objLocation.Rotation.Pitch;
        //bankSatuan.text = bank.ToString();
        //bankSatuanRotator.rotation = Quaternion.Euler(0, 0, bank);

        if(parentObjSatuan != null)
        {
            var bank = (int) objLocation.Rotation.Roll;
            bankSatuan.text = bank.ToString();
            bankSatuanRotator.rotation = Quaternion.Euler(0, 0, bank);

            var tilt = (int) objLocation.Rotation.Pitch;
            tiltSatuan.text = tilt.ToString();
            tiltSatuanRotator.rotation = Quaternion.Euler(0, 180, tilt);
        }
    }

    public void initObjSatuan(ObjekSatuan obj)
    {
        destroyObjSatuan();

        objSatuan = obj;

        // Spawn duplicate of selected object satuan into Viewer 3D container
        var objViewer3D = CreateViewer3DObject();

        objViewer3D.transform.position = Vector3.zero;
        objViewer3D.transform.rotation = Quaternion.Euler(0, -90, 0);
        objViewer3D.gameObject.SetLayerRecursively(10);

        resetIconIndicator();
        switch (objSatuan.kategori)
        {
            case KategoriSatuan.VEHICLE:
                toggleIconIndicator("vehicle", true);
                break;
            case KategoriSatuan.INFANTRY:
                toggleIconIndicator("infantry", true);
                break;
            case KategoriSatuan.SHIP:
                toggleIconIndicator("ship", true);
                break;
            case KategoriSatuan.SUBMARINE:
                toggleIconIndicator("sub", true);
                break;
            case KategoriSatuan.AIRCRAFT:
                toggleIconIndicator("aircraft", true);
                break;
            case KategoriSatuan.HELICOPTER:
                toggleIconIndicator("helicopter", true);
                break;
            default:
                toggleIconIndicator("vehicle", true);
                break;
        }

        // Initialize viewer3D cam
        var camViewer3D = Instantiate(viewer3DCam, viewer3DContainer).GetComponent<View3DCamController>();
        camViewer3D.transform.position = Vector3.zero;

        camViewer3D.init(objViewer3D.gameObject, viewer3DImage.gameObject, uiSatuanGroup.parent.parent.GetComponent<ScrollRect>());

        GetComponent<Animator>().Play("SlideIn");
    }

    private GameObject CreateViewer3DObject()
    {
        var objViewer3D = Instantiate(objSatuan, viewer3DContainer);

        // Destroy Unecessary Data
        Destroy(objViewer3D.GetComponent<EntityWalker>());
        Destroy(objViewer3D.GetComponent<SplineController>());
        Destroy(objViewer3D.GetComponent<ArcGISLocationComponent>());
        Destroy(objViewer3D.GetComponent<HPTransform>());

        if (objViewer3D.GetComponentInChildren<FollowCamController>())
        {
            Destroy(objViewer3D.GetComponentInChildren<FollowCamController>().gameObject);
        }

        //if (objViewer3D.GetComponentInChildren<HUDNavigationElement>())
        //{
        //    Destroy(objViewer3D.GetComponentInChildren<HUDNavigationElement>().gameObject);
        //}

        var synchronizer            = objViewer3D.gameObject.AddComponent<Watcher3DViewer>();
        synchronizer.parent         = objViewer3D.GetComponent<WGSBundleCore>().parent;
        synchronizer.references     = objSatuan.GetComponent<WGSBundleCore>().parent;

        //switch (objSatuan.jenis)
        //{
        //    case DataSatuan.JenisSatuan.VEHICLE:
        //        synchronizer.parent         = objViewer3D.GetComponent<VehicleBundleData>().parent.transform;
        //        synchronizer.references     = objSatuan.GetComponent<VehicleBundleData>().parent.transform;
        //        break;
        //    case DataSatuan.JenisSatuan.SHIP:
        //        synchronizer.parent         = objViewer3D.GetComponent<ShipBundleData>().parent.transform;
        //        synchronizer.references     = objSatuan.GetComponent<ShipBundleData>().parent.transform;
        //        break;
        //    case DataSatuan.JenisSatuan.AIRCRAFT:
        //        synchronizer.parent = objViewer3D.GetComponent<AircraftBundleData>().parent.transform;
        //        synchronizer.references = objSatuan.GetComponent<AircraftBundleData>().parent.transform;

        //        foreach(var node in objSatuan.GetComponent<AircraftBundleData>().nodeParticles)
        //        {
        //            Destroy(node.obj);
        //        }
        //        break;
        //    default: break;
        //}

        parentObjSatuan = synchronizer.references;

        return objViewer3D.gameObject;
    }

    public void destroyObjSatuan()
    {
        objSatuan = null;
        parentObjSatuan = null;

        uiSatuanGroup.gameObject.SetActive(false);

        foreach(Transform child in viewer3DContainer)
        {
            Destroy(child.gameObject);
        }
    }

    private void resetIconIndicator()
    {
        for(int i=0; i < tiltIndicator.data.Count; i++)
        {
            tiltIndicator.data[i].parameter.gameObject.SetActive(false);
        }

        for (int i = 0; i < bankIndicator.data.Count; i++)
        {
            bankIndicator.data[i].parameter.gameObject.SetActive(false);
        }
    }

    private void toggleIconIndicator(string param, bool state)
    {
        tiltIndicator.FindParameter(param).gameObject.SetActive(state);
        bankIndicator.FindParameter(param).gameObject.SetActive(state);
    }

    public void TogglePointer(bool state)
    {
        openByPointer = state;
    }
}
