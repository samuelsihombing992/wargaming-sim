using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using TMPro;

using Wargaming.Core;
using Wargaming.Core.Network;
using Wargaming.Core.GlobalParam;
using System;

public class LoginController : MonoBehaviour
{
    [Header("Menu Group")]
    public GameObject pageLogin;
    public GameObject pageMainMenu;
    public GameObject pageService;
    public GameObject pageSettings;

    [Header("Header Group")]
    public GameObject headerLogin;
    public GameObject headerMainMenu;
    public GameObject headerSettings;

    [Header("Background Group")]
    public GameObject backgroundLogin;
    public GameObject backgroundMainMenu;
    public GameObject backgroundSettings;

    [Header("Login Notif")]
    public Transform loginNotif;

    [Header("Login Form")]
    public TMP_InputField inputUsername;
    public TMP_InputField inputPassword;
    public Toggle togglePassword;

    [Header("Time Counter")]
    public TextMeshProUGUI timeCounter;

    [Header("Events")]
    public UnityEvent OnLoginSuccess;
    public UnityEvent OnLogoutSuccess;

    private GameObject loginPageLoading;

    #region INSTANCE
    public static LoginController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Update()
    {
        if(timeCounter != null)
        {
            timeCounter.text = DateTime.Now.ToString("HH:mm:ss");
        }

        FormTabSwitch();
    }

    #region FORM HANDLER
    public void TogglePassword() { inputPassword.TogglePassword(); }

    /// <summary>
    /// Reset Input Form
    /// </summary>
    public void ResetForm()
    {
        inputUsername.reset();
        inputPassword.reset();

        HideAnyInputMessage();
    }

    /// <summary>
    /// On Form Required Input Not Filled
    /// </summary>
    /// <param name="input">GameObject Input</param>
    /// <param name="message">Custom Message (optional)</param>
    public bool OnInputError(string message)
    {
        var label = loginNotif.GetComponentInChildren<TextMeshProUGUI>();
        label.text = message;

        loginNotif.gameObject.SetActive(true);
        return false;
    }

    /// <summary>
    /// Hide or Reset All Form Input Error Message to Hidden
    /// </summary>
    private void HideAnyInputMessage() { loginNotif.gameObject.SetActive(false); }

    private void FormTabSwitch()
    {
        // When on Login Form or not in Login Process...
        // - Can Use Form Input Switch by Pressing "TAB"
        if (SessionUser.id != null || !loginPageLoading.isEmpty()) return;

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (inputUsername.isFocused)
            {
                inputPassword.Select();
            }
            else
            {
                inputUsername.Select();
            }
        }

        if (Input.GetKeyDown(KeyCode.Return))
        {
            CheckForm();
        }
    }
    #endregion

    #region LOGIN HANDLER
    public void LoginAuthenticate() { CheckForm(); }

    public void LoginAsGuest()
    {

    }

    /// <summary>
    /// Validating Login Form Input
    /// </summary>
    public async void CheckForm()
    {
        if (!inputUsername.isEmpty() && !inputPassword.isEmpty())
        {
            if (loginPageLoading.isEmpty())
            {
                loginPageLoading = LoadingControllerV2.ShowOverlayLoading(LoadingControllerV2.LoadingOverlay.OVERLAY, GameObject.FindGameObjectWithTag("UI-CANVAS").transform, "Logging In...");
            }

            await Login();
        }
        else
        {
            if (inputUsername.isEmpty() && inputPassword.isEmpty())
            {
                OnInputError("Username & Password belum diisi!");
            }
            else if (inputUsername.isEmpty())
            {
                OnInputError("Username belum diisi!");
            }
            else if (inputUsername.isEmpty())
            {
                OnInputError("Password belum diisi!");
            }
        }
    }

    private async Task Login()
    {
        HideAnyInputMessage();

        // Set Delay to allow loading animation start before executing login request
        await Task.Delay(500);

        // Requesting Login
        WWWForm form = new WWWForm();
        form.AddField("username", inputUsername.text);
        form.AddField("password", inputPassword.text);

        var result = await WargamingAPI.RequestLogin(form);

        if (result == "done")
        {
            /* 
             * ==> If Login Request Success <==
             * - Request Skenario Aktif to get ID Skenario
             * - Request CB Terbaik to get CB Terbaik of current user
             * - Show Main Menu Section
             * - Clear Login Form
            */
            await WargamingAPI.GetSkenarioAktif();
            var getCBTerbaik = await WargamingAPI.GetCBTerbaik(SessionUser.id_bagian.ToString());
            if (getCBTerbaik.result == "cb_terbaik_belum_dipilih" && SessionUser.id != 1)
            {
                await WargamingAPI.RequestLogout();

                LoadingControllerV2.HideLoading(loginPageLoading);
                OnInputError("Panglima belum memilih metode analisa!");
                return;
            }

            await WargamingAPI.GetCBAll(SessionUser.id_bagian.ToString());

            LoadingControllerV2.HideLoading(loginPageLoading);

            ResetForm();
            OnLoginSuccess.AddListener(delegate { GoToMainMenu(); });

            OnLoginSuccess.Invoke();
        }
        else if (result == "retry")
        {
            /* ==> If Login Request Need to Retry <==
             * - Request Logout and Set Delay to allow request refreshing first
             * - Retry Checking Form again to re-executing request login
             */
            await WargamingAPI.RequestLogout();
            await Task.Delay(500);

            CheckForm();
        }
        else
        {
            /* ==> If Login Request Failed <==
             * - Check What Caused Request Failed
             * - Show Message Based On the Failed
             */
            if (result == "user_failed")
            {
                OnInputError("Username / Password Salah!");
            }
            else if (result == "connection_failed")
            {
                OnInputError("Koneksi Gagal!");
            }

            LoadingControllerV2.HideLoading(loginPageLoading);
        }
    }
    #endregion

    #region MAIN MENU HANDLER
    public async void LogoutOrExitApp()
    {
        if (SessionUser.id == null)
        {
            GlobalHelper.ExitApp();
            return;
        }

        var loading = LoadingControllerV2.ShowOverlayLoading(LoadingControllerV2.LoadingOverlay.OVERLAY, GameObject.FindGameObjectWithTag("UI-CANVAS").transform, "Logging Out...");

        await Task.Delay(500);
        await WargamingAPI.RequestLogout();

        LoadingControllerV2.HideLoading(loading);

        SessionUser.Reset();
        GotoLogin();

        OnLogoutSuccess.Invoke();
    }

    public void ExitApp()
    {
        GlobalHelper.ExitApp();
    }

    private void GoToMainMenu()
    {
        pageLogin.PlayAnimation("FadeOut");
        //headerLogin.PlayAnimation("FadeOut");
        headerLogin.gameObject.SetActive(false);

        pageMainMenu.PlayAnimation("FadeIn");
        headerMainMenu.gameObject.SetActive(true);
        //headerMainMenu.PlayAnimation("FadeIn");

        foreach (GameObject label in GameObject.FindGameObjectsWithTag("label-username"))
        {
            label.GetComponent<TextMeshProUGUI>().text = SessionUser.username;
        }

        backgroundMainMenu.WaitAndPlay("FadeIn", 1000, 0.1f);
    }

    private void GotoLogin()
    {
        pageLogin.PlayAnimation("FadeIn");
        headerLogin.gameObject.SetActive(true);
        //headerLogin.PlayAnimation("FadeIn");

        pageMainMenu.PlayAnimation("FadeOut");
        headerMainMenu.gameObject.SetActive(false);
        //headerMainMenu.PlayAnimation("FadeOut");

        backgroundMainMenu.WaitAndPlay("FadeOut", 1000, 0.1f);
    }

    /// <summary>
    /// Go To TFG Room Scene
    /// </summary>
    public async void JoinTFGRoom()
    {
        var loading = LoadingControllerV2.ShowOverlayLoading(LoadingControllerV2.LoadingOverlay.OVERLAY, GameObject.FindGameObjectWithTag("UI-CANVAS").transform);
        await Task.Delay(2000);

        LoadingControllerV2.HideLoading(loading);

        SceneLoad.returnTo = SceneController.Instance.loginScene;
        if (await SceneController.Instance.LoadScene(SceneController.Instance.tfgGameplayScene, null, "FadeIn"))
        {
            SceneController.Instance.DestroyLoading("FadeOut");
            await Task.Delay(1000);

            DatapackPackageController.Instance.SpawnDatapackOnTemplate();
            Destroy(this);
        }
    }

    /// <summary>
    /// Go To WGS Room Scene
    /// </summary>
    public async void JoinWGSRoom()
    {
        var loading = LoadingControllerV2.ShowOverlayLoading(LoadingControllerV2.LoadingOverlay.OVERLAY, GameObject.FindGameObjectWithTag("UI-CANVAS").transform);
        await Task.Delay(2000);

        LoadingControllerV2.HideLoading(loading);

        SceneLoad.returnTo = SceneController.Instance.loginScene;
        if (await SceneController.Instance.LoadScene(SceneController.Instance.wgsGameplayScene, null, "FadeIn"))
        {
            SceneController.Instance.DestroyLoading("FadeOut");
            await Task.Delay(1000);

            Destroy(this);
        }
    }

    /// <summary>
    /// Go To Asset Browser Scene
    /// </summary>
    public async void GotoAssetBrowser()
    {
        var loading = LoadingControllerV2.ShowOverlayLoading(LoadingControllerV2.LoadingOverlay.OVERLAY, GameObject.FindGameObjectWithTag("UI-CANVAS").transform);
        await Task.Delay(2000);

        LoadingControllerV2.HideLoading(loading);

        SceneLoad.returnTo = SceneController.Instance.loginScene;
        if (await SceneController.Instance.LoadScene(SceneController.Instance.assetScene, null, "FadeIn"))
        {
            SceneController.Instance.DestroyLoading("FadeOut");
            await Task.Delay(1000);

            Destroy(this);
        }
    }

    public async void GotoScenarioEditor()
    {
        var loading = LoadingControllerV2.ShowOverlayLoading(LoadingControllerV2.LoadingOverlay.OVERLAY, GameObject.FindGameObjectWithTag("UI-CANVAS").transform);
        await Task.Delay(2000);

        LoadingControllerV2.HideLoading(loading);

        SceneLoad.returnTo = SceneController.Instance.editorScene;
        if (await SceneController.Instance.LoadScene(SceneController.Instance.editorScene, null, "FadeIn"))
        {
            SceneController.Instance.DestroyLoading("FadeOut");
            await Task.Delay(1000);

            DatapackPackageController.Instance.SpawnDatapackOnTemplate();
            Destroy(this);
        }
    }
    #endregion

    #region SETTINGS HANDLER
    public void GotoSettingsOrReturn()
    {
        pageSettings.PlayAnimation((pageSettings.GetComponent<CanvasGroup>().alpha == 1) ? "LoginSlideOut" : "LoginSlideIn");
        headerSettings.PlayAnimation((pageSettings.GetComponent<CanvasGroup>().alpha == 1) ? "FadeOut" : "FadeIn");

        if (SessionUser.id == null)
        {
            pageLogin.PlayAnimation((pageSettings.GetComponent<CanvasGroup>().alpha == 1) ? "FadeIn" : "FadeOut");
            headerLogin.PlayAnimation((pageSettings.GetComponent<CanvasGroup>().alpha == 1) ? "FadeIn" : "FadeOut");
        }
        else
        {
            pageMainMenu.PlayAnimation((pageSettings.GetComponent<CanvasGroup>().alpha == 1) ? "FadeIn" : "FadeOut");
            headerMainMenu.PlayAnimation((pageSettings.GetComponent<CanvasGroup>().alpha == 1) ? "FadeIn" : "FadeOut");
        }
    }
    #endregion
}
