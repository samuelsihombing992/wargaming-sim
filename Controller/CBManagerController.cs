using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Wargaming.Components.Playback;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;

public class CBManagerController : MonoBehaviour
{
    public Metadata prefabList;
    public GameObject prefabListTitle;
    public Transform listContainer;

    #region INSTANCE
    public static CBManagerController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void Init()
    {
        try
        {
            CleanContainer();

            List<long> userDone = new List<long>();

            for (int i = 0; i < DataCB.data.Count; i++)
            {
                if (!userDone.Contains(DataCB.data[i].id_user.Value))
                {
                    var listTitle = Instantiate(prefabListTitle, listContainer);
                    listTitle.GetComponent<TextMeshProUGUI>().text = DataCB.data[i].nama_kogas;

                    userDone.Add(DataCB.data[i].id_user.Value);
                }

                var list = Instantiate(prefabList, listContainer);
                list.GetMetadataLabel("name").text = DataCB.data[i].nama_document;

                var idUser = DataCB.data[i].id_user;
                var idKogas = DataCB.data[i].id_kogas;
                var namaDocument = DataCB.data[i].nama_document;

                var toggler = list.GetComponent<MenuToggler>();
                toggler.onToggleOn.AddListener(async delegate
                {
                    GameController.activeLoading = LoadingControllerV2.ShowOverlayLoading(LoadingControllerV2.LoadingOverlay.OVERLAY, GameController.instance.modalUI, "Loading CB...");
                    PlaybackController.instance.OnPlaybackAction("pause");

                    await EntityController.instance.loadEntityFromCB(idUser, idKogas, SkenarioAktif.ID_SKENARIO, namaDocument);

                    LoadingControllerV2.HideLoading(GameController.activeLoading);
                });

                toggler.onToggleOff.AddListener(delegate {
                    EntityController.instance.RemoveEntityFromCB(idUser.ToString());
                });
            }
        }catch(Exception e)
        {
            Debug.Log("Failed to Get CB Data. ");
            Debug.LogError(e.Message);
        }
    }

    public void CleanContainer()
    {
        listContainer.DestroyAllChild();
    }

    public void UnloadAll()
    {
        foreach(Transform objList in listContainer)
        {
            var toggle = objList.GetComponent<Toggle>();
            if (toggle == null) continue;

            toggle.isOn = false;
        }
    }
}
