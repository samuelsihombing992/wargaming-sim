using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using TMPro;

using WGSBundling.Core;
using System.Threading.Tasks;
using Wargaming.Core.Network;
using Wargaming.Core.GlobalParam;
using System;
using System.Text.RegularExpressions;

public class AssetBrowserController : MonoBehaviour
{
    [Header("References")]
    public Toggle listToggler;

    [Header("GRID DATA")]
    public Transform gridFloor;
    public GameObject activeObject;

    [Header("Labels")]
    public TextMeshProUGUI labelName;
    public TextMeshProUGUI labelVariant;

    [Header("Asset List")]
    public Transform listContent;
    public GameObject prefabList;

    [Header("Debug")]
    public bool debugLoad = false;

    private List<Metadata> listData = new List<Metadata>();

    async void Start()
    {
        if (debugLoad)
        {
            await AssetPackageController.Instance.loadPackageAlutsista();
        }

        InitAssetList();
    }

    public void InitAssetList()
    {
        for(int i=0; i < AssetPackageController.Instance.ASSETS.Count; i++)
        {
            var newList = Instantiate(prefabList, listContent).GetComponent<Metadata>();
            var selectedAsset = AssetPackageController.Instance.ASSETS[i];

            newList.FindParameter("asset-name").GetComponent<TextMeshProUGUI>().text = AssetPackageController.Instance.ASSETS_NAME[i];

            if(selectedAsset.icon != null)
            {
                Destroy(newList.FindParameter("icon-missing"));
                newList.FindParameter("background").GetComponent<Image>().sprite = selectedAsset.icon;
            }

            var variantName = AssetPackageController.Instance.ASSETS_NAME[i];

            newList.GetComponent<Button>().onClick.AddListener(delegate { RespawnSelectedList(selectedAsset.gameObject, selectedAsset.id, variantName); });
            listData.Add(newList);
        }
    }

    public void RespawnSelectedList(GameObject obj, string bundleName, string bundleVariant)
    {
        Destroy(activeObject);

        var selecedObject = Instantiate(obj, gridFloor);
        var coreData = selecedObject.GetComponent<WGSBundleCore>();

        selecedObject.transform.position = Vector3.zero;
        selecedObject.transform.rotation = Quaternion.identity;

        coreData.parent.transform.rotation = Quaternion.Euler(coreData.origin.x, coreData.origin.y, coreData.origin.z);

        activeObject = selecedObject;

        if (selecedObject.GetComponent<PasukanAnim>() != null)
        {
            for (int i = 0; i < selecedObject.GetComponent<PasukanAnim>().ListOrangan.Count; i++)
            {

                selecedObject.GetComponent<PasukanAnim>().ListOrangan[i].GetComponent<SkinChange>().ChangeSkin(bundleVariant);
                selecedObject.GetComponent<PasukanAnim>().StopMoveAnimation();
            }
        }

        //labelName.text = bundleName;
        //labelVariant.text = (bundleName.Equals(bundleVariant)) ? "Base Variant" : bundleVariant;
        labelName.text = (bundleName.Equals(bundleVariant)) ? bundleName : bundleVariant;
        labelVariant.text = "";

        listToggler.isOn = false;
        ABCamController.instance.ResetCam();
    }

    public async void logout()
    {
        LoadingController.instance.ShowLoading("loading-blur", "Logging Out...");
        await Task.Delay(500);
        await WargamingAPI.RequestLogout();

        LoadingController.instance.HideLoading();
        await Task.Delay(500);

        if (await SceneController.Instance.LoadScene(SceneController.Instance.loginScene, null, "FadeIn"))
        {
            SceneController.Instance.DestroyLoading("FadeOut");
            await Task.Delay(1000);

            Destroy(this);
        }
    }

    public void SearchList(TMP_InputField search)
    {

        for(int i=0; i < listData.Count; i++)
        {
            Regex r = new Regex(search.text);

            bool isActive = false;
            if (search.text == "" || search.text == null)
            {
                isActive = true;
            }else if(r.IsMatch(listData[i].FindParameter("asset-name").GetComponent<TextMeshProUGUI>().text))
            {
                isActive = true;
            }

            listData[i].gameObject.SetActive(isActive);
    }
    }
}
