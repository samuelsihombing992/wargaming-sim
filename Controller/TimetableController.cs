using System;
using System.Threading;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using TMPro;

using Wargaming.Core.GlobalParam;
using Wargaming.Core.GlobalParam.HelperKegiatan;
using Wargaming.Core.Network;
using Wargaming.Components.Playback;
using Wargaming.Components.Colyseus;
using Wargaming.Components.Controller;
using Wargaming.Core.ScenarioEditor;

public class TimetableController : MonoBehaviour
{
    [Header("Prefabs")]
    [SerializeField] private GameObject prefabHariH;
    [SerializeField] private GameObject prefabWaktu;
    [SerializeField] private GameObject prefabWaktuGroup;
    [SerializeField] private GameObject prefabKegiatan;
    [SerializeField] private GameObject prefabKegiatanGroup;

    [Header("Container")]
    [SerializeField] private Transform containerHariH;
    [SerializeField] private Transform containerWaktu;
    [SerializeField] private Transform containerKegiatan;

    [Header("Label & Button")]
    [SerializeField] private TextMeshProUGUI labelDate;
    [SerializeField] private Button btnKegiatan;

    [Header("Event")]
    [SerializeField] private UnityEvent onPlayKegiatan;
        
    [NonSerialized] public List<int> LIST_HARI_H;
    [NonSerialized] public List<DateTime> LIST_DATETIME_HARI_H;
    [NonSerialized] public List<DataKegiatan> LIST_KEGIATAN;
    [NonSerialized] public List<GameObject> LIST_WAKTU;

    private int SELECTED_HARI_H;
    public GameObject SELECTED_KEGIATAN;

    #region INSTANCE
    public static TimetableController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        cleanTimetableContent();
    }

    public async Task<TimetableKegiatanHelper[]> initTimetable()
    {
        LIST_HARI_H = new List<int>();
        LIST_DATETIME_HARI_H = new List<DateTime>();

        await WargamingAPI.GetHariH();

        var kegiatan = TimetableKegiatanHelper.FromJson(await WargamingAPI.GetKegiatan(SkenarioAktif.ID_SKENARIO.ToString()));
        if (kegiatan == null) return null;

        for(int i=0; i < kegiatan.Length; i++)
        {
            DateTime waktuKegiatan;
            if(DateTime.TryParseExact(kegiatan[i].waktu, "MM-dd-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out waktuKegiatan))
            {
                kegiatan[i].waktuDT = waktuKegiatan;
            }
            else
            {
                kegiatan[i].waktu = "";
            }
        }

        // Get Hari-H Kegiatan
        await AddHariHBaru(kegiatan);

        // Get Waktu Kegiatan
        await AddWaktuBaru(kegiatan);

        // Get Data Kegiatan
        await AddKegiatanBaru(kegiatan);

        changeHariH(SELECTED_HARI_H);

        return kegiatan;
    }

    private async Task AddHariHBaru(TimetableKegiatanHelper[] kegiatan)
    {

        // Dapatkan Hari-H Kegiatan Dari Perbandingagn Waktu Kegiatan dengan Hari-H Skenario
        for(int i=0; i < kegiatan.Length; i++)
        {
            // Jika user sebagai kogas dan kegiatan tersebut bukan miliknya, maka di skip
            if (SessionUser.id != 1) if (kegiatan[i].kogas != SessionUser.bagian) continue;

            if (kegiatan[i].kogas != "false" && kegiatan[i].waktu != "")
            {
                var difference = (int)(kegiatan[i].waktuDT.Date - SkenarioAktif.HARI_H.Date).TotalDays;

                if (!LIST_HARI_H.Contains(difference)) LIST_HARI_H.Add(difference);

                if (!LIST_DATETIME_HARI_H.Contains(kegiatan[i].waktuDT.Date)) LIST_DATETIME_HARI_H.Add(kegiatan[i].waktuDT.Date);
            }
        }

        LIST_HARI_H.Sort();

        Debug.Log("Counting " + LIST_HARI_H.Count);
        // Instantiate Hari-H Kegiatan
        for (int i = 0; i < LIST_HARI_H.Count; i++)
        {
            var obj = Instantiate(prefabHariH, containerHariH);
            if(i == 0)
            {
                // Set first Hari-H as selected
                Color selectedColor;
                ColorUtility.TryParseHtmlString("#EEB24AFF", out selectedColor);

                obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = selectedColor;
                SELECTED_HARI_H = LIST_HARI_H[i];
            }

            obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = SetLabelHariH(LIST_HARI_H[i]);

            var index = LIST_HARI_H[i];
            obj.GetComponent<Button>().onClick.AddListener(delegate { changeHariH(index, obj); });
        }
    }

    private async Task AddWaktuBaru(TimetableKegiatanHelper[] kegiatan)
    {
        LIST_WAKTU = new List<GameObject>();
        List<string> waktu = new List<string>();

        // Dapatkan List Waktu dari Seluruh Kegiatan
        for (int i = 0; i < kegiatan.Length; i++)
        {
            if (SessionUser.id != 1)
            {
                if (kegiatan[i].kogas != SessionUser.bagian) continue;
            }

            if (kegiatan[i].kogas != "false" && kegiatan[i].waktu != "")
            {
                var waktu_kegiatan = kegiatan[i].waktuDT.ToString("HH:mm:ss");
                if (!waktu.Contains(waktu_kegiatan)) waktu.Add(waktu_kegiatan);
            }
        }

        // Sort List Waktu Dari Terkecil ke Terbesar
        waktu.Sort();

        // Masukkan List Waktu ke dalam Timetable
        for (int i = 0; i < waktu.Count; i++)
        {
            var obj = Instantiate(prefabWaktu, containerWaktu);

            obj.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = waktu[i];
            obj.name = waktu[i];

            LIST_WAKTU.Add(obj);
        }

        //// Masukkan Data Di Hari-H Apa Waktu Akan Tampil
        //for (int i = 0; i < kegiatan.Length; i++)
        //{
        //    if (SessionUser.id != 1)
        //    {
        //        if (kegiatan[i].kogas != SessionUser.bagian) continue;
        //    }

        //    if (kegiatan[i].kogas != "false" && kegiatan[i].waktu != "")
        //    {
        //        var objWaktu = containerKegiatan.Find(kegiatan[i].waktuDT.ToString("HH:mm:ss"));
        //        if (objWaktu != null)
        //        {
        //            var difference = (int)(kegiatan[i].waktuDT.Date - SkenarioAktif.HARI_H.Date).TotalDays;

        //            if (!objWaktu.GetComponent<DataKegiatan>().activeOn.Contains(difference))
        //            {
        //                objWaktu.GetComponent<DataKegiatan>().activeOn.Add(difference);
        //            }
        //        }
        //    }
        //}
    }

    public async Task AddKegiatanBaru(TimetableKegiatanHelper[] kegiatan)
    {
        LIST_KEGIATAN = new List<DataKegiatan>();

        List<TimetableKegiatanHelper> sortedKegiatan = kegiatan.OrderBy(o => o.waktuDT.ToString("HH:mm:ss")).ToList();

        // Sorting Kegiatan Berdasarkan Waktu & Hari-H
        for (int i = 0; i < sortedKegiatan.Count; i++)
        {
            if (sortedKegiatan[i].waktu == "") continue;

            if (SessionUser.id != 1)
            {
                if (sortedKegiatan[i].kogas != SessionUser.bagian) continue;
            }

            if (sortedKegiatan[i].kogas != "false" && sortedKegiatan[i].waktu != "")
            {
                // Masukkan Kegiatan Berdasarkan Waktu
                var parent = containerKegiatan.Find(sortedKegiatan[i].waktuDT.ToString("HH:mm:ss"));
                if (parent == null)
                {
                    parent = Instantiate(prefabKegiatanGroup, containerKegiatan).transform;
                    parent.name = sortedKegiatan[i].waktuDT.ToString("HH:mm:ss");
                }

                // Configure Hari-H Kegiatan
                var objKegiatan = Instantiate(prefabKegiatan, parent.GetChild(1));
                objKegiatan.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = sortedKegiatan[i].kogas;
                objKegiatan.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = sortedKegiatan[i].kegiatan;

                objKegiatan.GetComponent<Button>().onClick.AddListener(delegate { SelectKegiatan(objKegiatan); });

                var dataKegiatan = objKegiatan.GetComponent<DataKegiatan>();

                dataKegiatan.id = sortedKegiatan[i].id_kegiatan;
                dataKegiatan.keterangan = sortedKegiatan[i].kegiatan;
                dataKegiatan.hariH = (int)(sortedKegiatan[i].waktuDT.Date - SkenarioAktif.HARI_H.Date).TotalDays;
                dataKegiatan.timeKegiatan = sortedKegiatan[i].waktuDT;
                dataKegiatan.percepatan = int.Parse(sortedKegiatan[i].percepatan);

                LIST_KEGIATAN.Add(dataKegiatan);

                var difference = (int)(sortedKegiatan[i].waktuDT.Date - SkenarioAktif.HARI_H.Date).TotalDays;
                if (!objKegiatan.GetComponent<DataKegiatan>().activeOn.Contains(difference))
                {
                    objKegiatan.GetComponent<DataKegiatan>().activeOn.Add(difference);
                }

                //dataKegiatan.gameObject.SetActive(dataKegiatan.GetComponent<DataKegiatan>().hariH == SELECTED_HARI_H ? true : false);

                    //dataKegiatan.gameObject.SetActive(dataKegiatan.GetComponent<DataKegiatan>().activeOn.Contains(SELECTED_HARI_H) ? true : false);
            }
        }

        await containerKegiatan.GetComponent<ContentSizeFitter>().Refresh();

        for (int i = 0; i < LIST_KEGIATAN.Count; i++)
        {
            if (!LIST_KEGIATAN[i].gameObject.activeSelf) return;
            if(LIST_KEGIATAN[i].GetComponent<RectTransform>().sizeDelta.x > 600 && SessionUser.id == 1)
            {
                var layoutElement = LIST_KEGIATAN[i].gameObject.AddComponent<LayoutElement>();
                layoutElement.minWidth = 300;
                layoutElement.preferredWidth = 600;
            }
        }

        await containerKegiatan.GetComponent<ContentSizeFitter>().Refresh();

        await Task.Delay(100);
        for (int i = 0; i < containerWaktu.childCount; i++)
        {
            containerWaktu.GetChild(i).GetComponent<RectTransform>().sizeDelta = new Vector2(containerKegiatan.GetChild(i).GetComponent<RectTransform>().sizeDelta.x, containerKegiatan.GetChild(i).GetComponent<RectTransform>().sizeDelta.y);
        }
    }

    public async void changeHariH(int hariH, GameObject objComponent = null)
    {
        SELECTED_HARI_H = hariH;

        if (objComponent != null)
        {
            // Set Date Label
            var namaHari = Thread.CurrentThread.CurrentUICulture.DateTimeFormat.GetDayName(SkenarioAktif.HARI_H.Date.AddDays(SELECTED_HARI_H).DayOfWeek);
            labelDate.text = namaHari + ", " + SkenarioAktif.HARI_H.Date.AddDays(SELECTED_HARI_H).ToString("dd MMMM yyyy") + " (" + SetLabelHariH(SELECTED_HARI_H) + ")";

            // Set Hari-H Color To Default
            Color labelColor;
            for (int i = 0; i < containerHariH.childCount; i++)
            {
                ColorUtility.TryParseHtmlString("#C3C3C3FF", out labelColor);

                var obj = containerHariH.GetChild(i);
                obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = labelColor;
            }

            // Set Selected Hari-H Color
            ColorUtility.TryParseHtmlString("#EEB24AFF", out labelColor);
            objComponent.transform.GetChild(0).GetComponent<TextMeshProUGUI>().color = labelColor;
        }

        for (int i = 0; i < containerKegiatan.childCount; i++)
        {
            containerKegiatan.GetChild(i).gameObject.SetActive(false);
        }

        var WAKTU_USE = new List<string>();

        for (int i = 0; i < LIST_KEGIATAN.Count; i++)
        {
            if (LIST_KEGIATAN[i].activeOn.Contains(SELECTED_HARI_H))
            {
                LIST_KEGIATAN[i].gameObject.SetActive(true);

                var waktuKegiatan = LIST_KEGIATAN[i].timeKegiatan.ToString("HH:mm:ss");
                if (!WAKTU_USE.Contains(waktuKegiatan)) WAKTU_USE.Add(waktuKegiatan);

                LIST_KEGIATAN[i].transform.parent.parent.gameObject.SetActive(true);
            }
            else
            {
                LIST_KEGIATAN[i].gameObject.SetActive(false);
            }
        }

        await containerKegiatan.GetComponent<ContentSizeFitter>().Refresh();

        for (int i=0; i < LIST_WAKTU.Count; i++)
        {
            if (WAKTU_USE.Contains(LIST_WAKTU[i].name))
            {
                LIST_WAKTU[i].SetActive(true);
            }
            else
            {
                LIST_WAKTU[i].SetActive(false);
            }
        }

        for (int i = 0; i < containerWaktu.childCount; i++)
        {
            containerWaktu.GetChild(i).gameObject.SetActive(WAKTU_USE.Contains(containerWaktu.GetChild(i).name) ? true : false);

            if (!containerWaktu.GetChild(i).gameObject.activeSelf) continue;

            containerWaktu.GetChild(i).GetComponent<RectTransform>().sizeDelta = new Vector2(containerWaktu.GetChild(i).GetComponent<RectTransform>().sizeDelta.x, containerKegiatan.GetChild(i).GetComponent<RectTransform>().sizeDelta.y);
        }
    }

    public void SelectKegiatan(GameObject objComponent = null)
    {
        if (objComponent != null)
        {
            Color labelColor;
            if (SELECTED_KEGIATAN != null)
            {
                ColorUtility.TryParseHtmlString("#215176FF", out labelColor);
                SELECTED_KEGIATAN.GetComponent<Image>().color = labelColor;
            }

            SELECTED_KEGIATAN = objComponent;

            ColorUtility.TryParseHtmlString("#4D5154FF", out labelColor);
            objComponent.GetComponent<Image>().color = labelColor;

            if (ScenarioEditorController.instance != null)
            {
                onPlayKegiatan.Invoke();
                return;
            }

            btnKegiatan.interactable = true;
        }
        else
        {
            btnKegiatan.interactable = false;
        }
    }

    public async void PlayKegiatan()
    {
        if (SELECTED_KEGIATAN == null) return;

        var kegiatanAktif = SELECTED_KEGIATAN.GetComponent<DataKegiatan>();

        var playback = PlaybackController.instance;
        PlaybackController.CURRENT_TIME = kegiatanAktif.timeKegiatan.AddHours(StartupConfig.settings.TIME_DIFF.GetValueOrDefault(0));

        if(playback.sliderPlayback != null) playback.sliderPlayback.value = (float) PlaybackController.CURRENT_TIME.Subtract(SkenarioAktif.WaktuMulai).TotalMilliseconds;

        playback.SetSpeedFactor(kegiatanAktif.percepatan);
        playback.updateHeaderLabel();
        //TimeWeatherController.SyncTimeCycle();

        onPlayKegiatan.Invoke();

        if (ColyseusTFGController.instance != null)
        {
            if (ColyseusTFGController.instance.room == null) return;

        }else if(ColyseusWGSController.instance != null)
        {

        }

        //for(int i=0; i < MissionController.instance.OBJ_MISSION.Count; i++)
        //{
        //    MissionController.instance.OBJ_MISSION[i].GetComponent<MissionWalker>().RefreshMission();
        //}
    }

    public void syncScrollKegiatanWaktu()
    {
        containerWaktu.parent.parent.GetComponent<ScrollRect>().verticalNormalizedPosition = containerKegiatan.parent.parent.GetComponent<ScrollRect>().verticalNormalizedPosition;
    }

    public void syncScrollWaktuKegiatan()
    {
        containerKegiatan.parent.parent.GetComponent<ScrollRect>().verticalNormalizedPosition = containerWaktu.parent.parent.GetComponent<ScrollRect>().verticalNormalizedPosition;
    }

    public string SetLabelHariH(int hari)
    {
        if(hari == 0)
        {
            return "H";
        }else if(hari < 0)
        {
            return "H" + hari.ToString();
        }
        else
        {
            return "H+" + hari.ToString();
        }
    }

    private void cleanTimetableContent()
    {
        foreach(GameObject child in containerHariH)
        {
            Destroy(child);
        }

        foreach (GameObject child in containerWaktu)
        {
            Destroy(child);
        }

        foreach (GameObject child in containerKegiatan)
        {
            Destroy(child);
        }
    }
}
