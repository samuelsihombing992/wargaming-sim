using System;

using UnityEngine;
using UnityEngine.UI;

using TMPro;
using Wargaming.Components.Colyseus;
using Wargaming.Core.GlobalParam;

namespace Wargaming.Components.Playback
{
    public class PlaybackSingleController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI headerClock;
        [SerializeField] private TextMeshProUGUI headerDate;
        [SerializeField] public TextMeshProUGUI headerHariH;

        [SerializeField] private Toggle toggleTimeCylce;
        [SerializeField] private Toggle toggleMultiplayerSync;

        #region INSTANCE
        public static PlaybackSingleController instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion
    }

}