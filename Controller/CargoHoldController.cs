using Esri.ArcGISMapsSDK.Components;
using Esri.GameEngine.Geometry;
using FluffyUnderware.Curvy;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Wargaming.Components.Controller;
using WGSBundling.Bundles.CargoHold;
using static Wargaming.Core.GlobalParam.GlobalHelper;

namespace Wargaming.Components.CargoHold
{
    public class CargoHoldController : MonoBehaviour
    {
        public bool onGenerateProcess = false;
        public Vector3 originalFirstPath;
        public DateTime? animStateRun = null;

        public async Task PrepareEmbarkasiStep(NodeCargoHold parent, MissionWalker mission)
        {
            //mission.embarkPathGenerated = true;
        }

        public async Task PrepareDebarkasiStep(NodeCargoHold parent, MissionWalker mission)
        {
            if (onGenerateProcess) return;

            var door = GetDoorsToOpen(parent, mission);
            if (door != -99)
            {
                if (!parent.doors[door].state)
                {
                    var animator = parent.doors[door].obj.GetComponent<Animator>();
                    if (!animator.GetCurrentAnimatorStateInfo(0).IsName("RampDown"))
                    {
                        parent.doors[door].state = true;
                        animator.Play("RampDown");
                    }
                }

                //if (mission.debarkPathGenerated) return;
                GenerateDebarkasiPath(parent.doors[door], mission);
            }
        }

        private int GetDoorsToOpen(NodeCargoHold cargoHold, MissionWalker mission)
        {
            // Ambil posisi node pertama dari misi debarkasi dengan titik keluar/masuk terdekat
            float _distance = -99;
            int selectedDoors = -99;
            for (int i = 0; i < cargoHold.doors.Count; i++)
            {
                var entryExitPoint = cargoHold.doors[i].paths[cargoHold.doors[i].paths.Count - 1];
                var newDistance = Vector3.Distance(entryExitPoint.transform.position, originalFirstPath);

                if (_distance == -99)
                {
                    _distance = newDistance;
                    selectedDoors = i;
                    continue;
                }

                if (_distance > newDistance)
                {
                    _distance = newDistance;
                    selectedDoors = i;
                }
            }

            return selectedDoors;
        }

        public async void GenerateDebarkasiPath(NodeCargoDoorData activeDoor, MissionWalker mission)
        {
            onGenerateProcess = true;

            if (mission == null) return;
            if (mission.DATA.Count == 0) return;

            if (originalFirstPath == null)
            {
                originalFirstPath = mission.transform.GetChild(mission.transform.childCount - 1).position;
            }

            List<MissionWalkerParam> DATA = new List<MissionWalkerParam>();
            List<ArcGISLocationComponent> TEMP_PATH_NODE = new List<ArcGISLocationComponent>();

            foreach(Transform path in mission.transform)
            {
                Destroy(path.gameObject);
            }

            animStateRun = mission.DATA[0].datetime;
            var dateAtNode = mission.DATA[0].datetime.AddSeconds(60);

            for (int i=0; i < activeDoor.paths.Count; i++)
            {   
                var activePath = activeDoor.paths[i];

                var tempRoutePoint = Instantiate(activePath.gameObject, mission.transform.parent);
                tempRoutePoint.transform.position = activePath.transform.position;

                var tempRouteLocation = tempRoutePoint.AddComponent<ArcGISLocationComponent>();

                await Task.Delay(100);
                tempRouteLocation.Position = new ArcGISPoint(tempRouteLocation.Position.X, tempRouteLocation.Position.Y, tempRouteLocation.Position.Z, ArcGISSpatialReference.WGS84());

                if (i != 0)
                {
                    double dx, dy;
                    OnlineMapsUtils.DistanceBetweenPoints(
                        DATA[i - 1].coordinate.x, DATA[i - 1].coordinate.y,
                        tempRouteLocation.Position.X, tempRouteLocation.Position.Y,
                        out dx, out dy);

                    double stepDistance = Math.Sqrt(dx * dx + dy * dy);

                    // Total step time
                    double totalTime = stepDistance / mission.speed * 3600;
                    dateAtNode = dateAtNode.AddSeconds(totalTime);
                }

                var newPos          = new MissionWalkerParam();
                newPos.time         = dateAtNode.ToString();
                newPos.datetime     = dateAtNode;
                newPos.coordinate   = 
                    new Vector3(
                        (float)tempRouteLocation.Position.X,
                        (float)tempRouteLocation.Position.Y,
                        (float)tempRouteLocation.Position.Z
                    );

                DATA.Add(newPos);

                await Task.Delay(100);
                Destroy(tempRoutePoint.gameObject);

                var splineNode = Instantiate(MissionController.instance.prefabNode, mission.transform);
                splineNode.transform.SetCoordinate(DATA[i].coordinate);
                splineNode.transform.SetOrientation(new Vector3(0, 0, -90));
                splineNode.transform.SetAsLastSibling();

                TEMP_PATH_NODE.Add(splineNode.GetComponent<ArcGISLocationComponent>());
            }

            for(int i=0; i < mission.DATA.Count; i++)
            {
                var splineNode = Instantiate(MissionController.instance.prefabNode, mission.transform);
                
                splineNode.transform.SetCoordinate(mission.DATA[i].coordinate);
                splineNode.transform.SetOrientation(new Vector3(0, 0, -90));
                splineNode.transform.SetAsLastSibling();

                TEMP_PATH_NODE.Add(splineNode.GetComponent<ArcGISLocationComponent>());
            }

            await Task.Delay(100);
            mission.GetComponent<CurvySpline>().Refresh();

            for (int i=0; i < mission.DATA.Count; i++)
            {
                double dx, dy;
                if (i != 0)
                {
                    OnlineMapsUtils.DistanceBetweenPoints(
                        mission.DATA[i - 1].coordinate.x, mission.DATA[i - 1].coordinate.y,
                        mission.DATA[i].coordinate.x, mission.DATA[i].coordinate.y,
                        out dx, out dy
                    );
                }
                else
                {
                    OnlineMapsUtils.DistanceBetweenPoints(
                        DATA[DATA.Count - 1].coordinate.x, DATA[DATA.Count - 1].coordinate.y,
                        mission.DATA[i].coordinate.x, mission.DATA[i].coordinate.y,
                        out dx, out dy
                    );
                }

                DATA.Add(mission.DATA[i]);

                double stepDistance = Math.Sqrt(dx * dx + dy * dy);

                // Total step time
                double totalTime = stepDistance / mission.speed * 3600;
                dateAtNode = dateAtNode.AddSeconds(totalTime);

                DATA[i + activeDoor.paths.Count].datetime    = dateAtNode;
                DATA[i + activeDoor.paths.Count].time        = dateAtNode.ToString();

            }

            mission.DATA.Clear();
            mission.DATA = DATA;

            await Task.Delay(100);
            for (int i = 0; i < TEMP_PATH_NODE.Count; i++)
            {
                Destroy(TEMP_PATH_NODE[i]);
            }

            //mission.debarkPathGenerated = true;
            onGenerateProcess = false;
        }
    }
}
