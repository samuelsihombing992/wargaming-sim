using System;
using System.Linq;
using System.Collections.Generic;

using UnityEngine;
using WGSBundling.Bundles.AircraftEngine;

public class VFXPackageController : MonoBehaviour
{
    //[HideInInspector] public List<ParticleEffect> jetEngine = new List<ParticleEffect>();
    //[HideInInspector] public List<ParticleEffect> trail = new List<ParticleEffect>();

    [Header("Default VFX Data")]
    [HideInInspector] public GameObject jetEngine;
    [HideInInspector] public GameObject jetTrail;
    [HideInInspector] public GameObject propsTrail;
    [HideInInspector] public GameObject wingsTrail;

    [HideInInspector] public GameObject tireDust;
    [HideInInspector] public GameObject shipFunnel;

    [HideInInspector] public List<ParticleEffect> customVFX = new List<ParticleEffect>();

    #region INSTANCE
    public static VFXPackageController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Instance.transform.parent = null;
            DontDestroyOnLoad(Instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void SetDefaultVFX(NodeAircraftEngine module)
    {
        for(int i=0; i < module.nodes.Count; i++)
        {
            if (module.nodes[i].node == null) continue;

            if(module.nodes[i].engineType == NodeAircraftEngineData.EngineType.Jet)
            {
                Instantiate(jetEngine, module.nodes[i].node);
            }
            else
            {
                Instantiate(jetTrail, module.nodes[i].node);
            }
        }
    }

    //public GameObject FindParticles(string key, AircraftBundleData.BundleParticles.ParticleType type)
    //{
    //    switch (type)
    //    {
    //        case AircraftBundleData.BundleParticles.ParticleType.JET_ENGINE:
    //            return GetParticleByType(jetEngine, key);
    //        case AircraftBundleData.BundleParticles.ParticleType.TRAIL:
    //            return GetParticleByType(trail, key);
    //    }

    //    return null;
    //}

    //public GameObject FindParticles(string key, AircraftBundleData.BundleParticles.ParticleType type)
    //{
    //    switch (type)
    //    {
    //        case AircraftBundleData.BundleParticles.ParticleType.JET_ENGINE:
    //            return GetParticleByType(jetEngine, key);
    //        case AircraftBundleData.BundleParticles.ParticleType.TRAIL:
    //            return GetParticleByType(trail, key);
    //    }

    //    return null;
    //}

    //private GameObject GetParticleByType(List<ParticleEffect> effect, string key)
    //{
    //    if (effect == null) return null;
    //    if (effect.Count <= 0) return null;

    //    IEnumerable<string> keys = from meta in effect where meta.name == key select meta.name;
    //    IEnumerable<GameObject> prefabs = from meta in effect where meta.name == key select meta.prefab;

    //    foreach (string keyselect in keys)
    //    {
    //        foreach (GameObject paramselect in prefabs)
    //        {
    //            return paramselect;
    //        }
    //    }

    //    return null;
    //}

    #region CLASS VARIABLES
    [Serializable]
    public class ParticleEffect
    {
        public string name;
        public GameObject prefab;
    }
    #endregion
}
