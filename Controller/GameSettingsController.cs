using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

using TMPro;

public class GameSettingsController : MonoBehaviour
{
    public Volume volumeProfile;

    #region DISPLAY MENU
    [Header("Display Menu")]
    public TMP_Dropdown resolution;
    public TMP_Dropdown display;
    public TMP_Dropdown antialiasing;
    public Toggle vSync;

    public static List<Resolution> resolutionData;

    private static float refreshRate;
    private static int resolutionSelected;

    private bool displayMenuChange = false;
    #endregion

    #region GRAPHICS MENU
    [Header("Graphics Menu")]
    public TMP_Dropdown textureQuality;
    public TMP_Dropdown oceanQuality;
    public TMP_Dropdown shadowQuality;
    public TMP_Dropdown volumetricClouds;
    public TMP_Dropdown bloomQuality;
    public Toggle motionBlur;
    public Toggle ambientOcclusion;

    private bool graphicsMenuChange = false;
    #endregion


    #region GAMEPLAY MENU
    [Header("Gameplay Menu")]
    public Toggle toggleSatuan;
    public Toggle togglePasukan;
    public Toggle toggleDrawing;
    public Toggle toggleRadar;
    public Toggle toggleKekuatan;
    public Toggle toggleSituasi;
    public Toggle toggleBungus;
    public Toggle toggleAnimation;

    private bool gameplayMenuChange = false;
    #endregion

    #region BASEMAP MENU
    [Header("Basemap Menu")]
    public Transform basemapContainer;
    public GameObject prefabListBasemap;

    public Toggle toggleUseBasemap;
    public TMP_Dropdown basemapGroup;

    private bool basemapMenuChange = false;
    #endregion

    #region LAYER MENU
    [Header("Layer Menu")]
    public Transform layerContainer;
    public GameObject prefabListLayer;

    public Toggle toggleUseLayer;
    public TMP_Dropdown layerGroup;

    private bool layerMenuChange = false;
    #endregion

    #region ASSET PACKAGE MENU
    [Header("Asset Package Menu")]
    public Toggle toggleLoadAssetPackage;

    private bool assetPackageMenuChange = false;
    #endregion

    #region LAYER MENU
    [Header("Layer Menu")]
    public Toggle toggleLoadDatapack;

    private bool datapackMenuChange = false;
    #endregion

    #region INSTANCE
    public static GameSettingsController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        PrepareDisplaySettings();
        PrepareGraphicSettings();

        PrepareGameplaySettings();

        PrepareLayerSettings();
        PrepareBasemapSettings();

        PrepareAssetPackageSettings();
        PrepareDatapackSettings();

        // Hide Modal UI On Start
        //gameObject.SetActive(false);
    }

    #region PREPARATION
    private void PrepareDisplaySettings()
    {
        resolution.onValueChanged.AddListener(delegate { displayMenuChange = true; });
        display.onValueChanged.AddListener(delegate { displayMenuChange = true; });

        var resData = Screen.resolutions;
        resolutionData = new List<Resolution>();

        resolution.ClearOptions();
        refreshRate = Screen.currentResolution.refreshRate;

        for(int i=0; i < resData.Length; i++)
        {
            if (resData[i].refreshRate == refreshRate)
            {
                resolutionData.Add(resData[i]);
            }
        }

        List<string> resolutionOptions = new List<string>();
        for(int i=0; i < resolutionData.Count; i++)
        {
            string option = resolutionData[i].width + "x" + resolutionData[i].height + " " + resolutionData[i].refreshRate + " Hz";
            resolutionOptions.Add(option);

            if (resolutionData[i].width == Screen.width && resolutionData[i].height == Screen.height)
            {
                resolutionSelected = i;
            }
        }

        resolution.AddOptions(resolutionOptions);
        resolution.value = resolutionSelected;
        resolution.RefreshShownValue();

        if (Screen.fullScreen)
        {
            display.value = 0;
        }
        else
        {
            display.value = (Screen.fullScreenMode == FullScreenMode.FullScreenWindow) ? 1 : 2;
        }

        vSync.isOn = (QualitySettings.vSyncCount == 1) ? true : false;

        
    }

    private void PrepareGraphicSettings()
    {
        textureQuality.onValueChanged.AddListener(delegate { graphicsMenuChange = true; });
        shadowQuality.onValueChanged.AddListener(delegate { graphicsMenuChange = true; });
        oceanQuality.onValueChanged.AddListener(delegate { graphicsMenuChange = true; });
        volumetricClouds.onValueChanged.AddListener(delegate { graphicsMenuChange = true; });
        bloomQuality.onValueChanged.AddListener(delegate { graphicsMenuChange = true; });
        motionBlur.onValueChanged.AddListener(delegate { graphicsMenuChange = true; });
        ambientOcclusion.onValueChanged.AddListener(delegate { graphicsMenuChange = true; });

        textureQuality.value    = QualitySettings.masterTextureLimit;

        volumeProfile.sharedProfile.TryGet<Bloom>(out var bloomComponent);
        if (bloomComponent != null)
        {
            bloomQuality.value = (!bloomComponent.active) ? 0 : (bloomComponent.quality.levelAndOverride.level + 1);
        }

        volumeProfile.sharedProfile.TryGet<MotionBlur>(out var motionBlurComponent);
        if (motionBlurComponent != null)
        {
            motionBlur.isOn = motionBlurComponent.active;
        }

        volumeProfile.sharedProfile.TryGet<AmbientOcclusion>(out var ambientOcclusionComponent);
        if (ambientOcclusionComponent != null)
        {
            ambientOcclusion.isOn = ambientOcclusionComponent.active;
        }

        shadowQuality.value     = GetShadowResIndex();
    }

    private void PrepareGameplaySettings()
    {
        // Prepare Gameplay Settings
        toggleSatuan.onValueChanged.AddListener(delegate { gameplayMenuChange = true; });
        togglePasukan.onValueChanged.AddListener(delegate { gameplayMenuChange = true; });
        toggleDrawing.onValueChanged.AddListener(delegate { gameplayMenuChange = true; });
        toggleRadar.onValueChanged.AddListener(delegate { gameplayMenuChange = true; });
        toggleKekuatan.onValueChanged.AddListener(delegate { gameplayMenuChange = true; });
        toggleSituasi.onValueChanged.AddListener(delegate { gameplayMenuChange = true; });
        toggleBungus.onValueChanged.AddListener(delegate { gameplayMenuChange = true; });
        toggleAnimation.onValueChanged.AddListener(delegate { gameplayMenuChange = true; });

        toggleSatuan.isOn       = StartupConfig.settings.ENABLE_LOAD_SATUAN;
        togglePasukan.isOn      = StartupConfig.settings.ENABLE_LOAD_PASUKAN;
        toggleDrawing.isOn      = StartupConfig.settings.ENABLE_LOAD_DRAWING;
        toggleRadar.isOn        = StartupConfig.settings.ENABLE_LOAD_RADAR;
        toggleKekuatan.isOn     = StartupConfig.settings.ENABLE_LOAD_KEKUATAN;
        toggleSituasi.isOn      = StartupConfig.settings.ENABLE_LOAD_SITUASI;
        toggleBungus.isOn       = StartupConfig.settings.ENABLE_LOAD_BUNGUS;
        toggleAnimation.isOn    = StartupConfig.settings.ENABLE_LOAD_ANIMATION;
    }

    private void PrepareBasemapSettings()
    {
        // Prepare Basemap Settings
        toggleUseBasemap.onValueChanged.AddListener(delegate { BasemapManagerController.SetUseBasemapService(toggleUseBasemap.isOn); basemapMenuChange = true; });

        toggleUseBasemap.isOn = StartupConfig.settings.ENABLE_BASEMAPS;
    }

    private void PrepareLayerSettings()
    {
        // Prepare Layer Settings
        toggleUseLayer.onValueChanged.AddListener(delegate { LayerManagerController.SetUseLayerService(toggleUseLayer.isOn); layerMenuChange = true; });

        toggleUseLayer.isOn = StartupConfig.settings.ENABLE_LAYERS;
    }

    private void PrepareAssetPackageSettings()
    {
        // Prepare Asset Package Settings
        toggleLoadAssetPackage.onValueChanged.AddListener(delegate { assetPackageMenuChange = true; });

        toggleLoadAssetPackage.isOn = StartupConfig.settings.ENABLE_ASSET_PACKAGE;
    }

    private void PrepareDatapackSettings()
    {
        // Prepare Datapack Settings
        toggleLoadDatapack.onValueChanged.AddListener(delegate { datapackMenuChange = true; });

        toggleLoadDatapack.isOn = StartupConfig.settings.ENABLE_DATAPACK;
    }
    #endregion

    #region DISPLAY_ACTION
    private void SetDisplaySettings()
    {
        resolutionSelected = resolution.value;

        Resolution res = resolutionData[resolutionSelected];
        Screen.SetResolution(res.width, res.height, Screen.fullScreen);

        if(display.value == 0)
        {
            Screen.fullScreen = true;
        }
        else
        {
            Screen.fullScreen = (display.value == 1) ? true : false;
            Screen.fullScreenMode = (display.value == 1) ? FullScreenMode.FullScreenWindow : FullScreenMode.Windowed;
        }
    }
    #endregion

    #region GRAPHIC_ACTION
    private void SetGraphicSettings()
    {
        QualitySettings.masterTextureLimit = textureQuality.value;

        volumeProfile.sharedProfile.TryGet<Bloom>(out var bloomComponent);
        if(bloomComponent != null)
        {
            if(bloomQuality.value != 0) { bloomComponent.quality.levelAndOverride = new((bloomQuality.value - 1), false); }
            bloomComponent.active = (bloomQuality.value == 0) ? false : true;
        }

        volumeProfile.sharedProfile.TryGet<MotionBlur>(out var motionBlurComponent);
        if(motionBlurComponent != null)
        {
            motionBlurComponent.active = motionBlur.isOn;
        }

        volumeProfile.sharedProfile.TryGet<AmbientOcclusion>(out var ambientOcclusionComponent);
        if (ambientOcclusionComponent != null)
        {
            ambientOcclusionComponent.active = ambientOcclusion.isOn;
        }

        QualitySettings.shadowResolution = GetShadowResByIndex(shadowQuality.value);
        QualitySettings.shadows = (shadowQuality.value == 0) ? ShadowQuality.Disable : ShadowQuality.All;
    }

    private ShadowResolution GetShadowResByIndex(int index)
    {
        switch (index)
        {
            case 0:
                return ShadowResolution.Low;
            case 1:
                return ShadowResolution.Low;
            case 2:
                return ShadowResolution.Medium;
            case 3:
                return ShadowResolution.High;
            case 4:
                return ShadowResolution.VeryHigh;
            default: return ShadowResolution.Medium;
        }
    }

    private int GetShadowResIndex()
    {
        Debug.Log((QualitySettings.shadows == ShadowQuality.Disable) ? "Disabled" : QualitySettings.shadowResolution);

        switch (QualitySettings.shadowResolution)
        {
            case ShadowResolution.Low:
                return (QualitySettings.shadows == ShadowQuality.Disable) ? 0 : 1;
            case ShadowResolution.Medium:
                return 1;
            case ShadowResolution.High:
                return 2;
            case ShadowResolution.VeryHigh:
                return 3;
            default: return 1;
        }
    }

    private ScalableSettingLevelParameter.Level GetBloomQualityByIndex(int index)
    {
        switch (index)
        {
            case 0: return ScalableSettingLevelParameter.Level.High;
            case 1: return ScalableSettingLevelParameter.Level.Medium;
            case 2: return ScalableSettingLevelParameter.Level.Low;
            default: return ScalableSettingLevelParameter.Level.High;
        }
    }
    #endregion

    public void ApplySettings()
    {
        if (displayMenuChange)
        {
            SetDisplaySettings();
            displayMenuChange = false;

            Debug.Log("Display Settings Saved...");
        }

        if (graphicsMenuChange)
        {
            SetGraphicSettings();
            graphicsMenuChange = false;

            Debug.Log("Graphic Settings Saved...");
        }

        if (basemapMenuChange)
        {
            StartupConfig.settings.ENABLE_BASEMAPS = toggleUseBasemap.isOn;
            StartupConfig.SaveStartupConfig();
            basemapMenuChange = false;

            Debug.Log("Basemap Settings Saved...");
        }

        if (layerMenuChange)
        {
            StartupConfig.settings.ENABLE_LAYERS = toggleUseLayer.isOn;
            StartupConfig.SaveStartupConfig();
            layerMenuChange = false;

            Debug.Log("Layer Settings Saved...");
        }

        if (assetPackageMenuChange)
        {
            StartupConfig.settings.ENABLE_ASSET_PACKAGE = toggleLoadAssetPackage.isOn;
            StartupConfig.SaveStartupConfig();
            assetPackageMenuChange = false;

            Debug.Log("Asset Package Settings Saved...");
        }

        if (datapackMenuChange)
        {
            StartupConfig.settings.ENABLE_DATAPACK = toggleLoadDatapack.isOn;
            StartupConfig.SaveStartupConfig();
            datapackMenuChange = false;

            Debug.Log("Datapack Settings Saved...");
        }

        if (gameplayMenuChange)
        {
            StartupConfig.settings.ENABLE_LOAD_SATUAN       = toggleSatuan.isOn;
            StartupConfig.settings.ENABLE_LOAD_PASUKAN      = togglePasukan.isOn;
            StartupConfig.settings.ENABLE_LOAD_DRAWING      = toggleDrawing.isOn;
            StartupConfig.settings.ENABLE_LOAD_RADAR        = toggleRadar.isOn;
            StartupConfig.settings.ENABLE_LOAD_KEKUATAN     = toggleKekuatan.isOn;
            StartupConfig.settings.ENABLE_LOAD_SITUASI      = toggleSituasi.isOn;
            StartupConfig.settings.ENABLE_LOAD_BUNGUS       = toggleBungus.isOn;
            StartupConfig.settings.ENABLE_LOAD_ANIMATION    = toggleAnimation.isOn;

            StartupConfig.SaveStartupConfig();
            gameplayMenuChange = false;

            Debug.Log("Gameplay Settings Saved...");

        }
    }
}
