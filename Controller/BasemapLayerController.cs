using ch.sycoforge.Decal;
using Esri.ArcGISMapsSDK.Components;
using Esri.GameEngine.Layers;
using Esri.GameEngine.Layers.Base;
using Esri.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;

public class BasemapLayerController : MonoBehaviour
{
    [Header("References")]
    [SerializeField]
    private Transform listContainer;

    [SerializeField]
    private GameObject prefabListBasemap;

    [SerializeField]
    private GameObject prefabListLayer;

    private async void Start()
    {
        await InitBasemap();
        InitLayer();
        LoadBasemapContent();
        base.gameObject.SetActive(value: false);
    }

    private async Task InitBasemap()
    {
        await Task.Delay(1000);

        if (GlobalMapService.BASEMAP_DATA == null)
        {
            GlobalMapService.BASEMAP_DATA = new List<BasemapData>();
        }
        BasemapData item = new BasemapData
        {
            basemapId = Guid.NewGuid().ToString(),
            basemapName = "Default (Satellite)",
            basemapSource = "https://services.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer",
            basemapType = BasemapTypes.ImageLayer,
            file = false,
            is_default = (GlobalMapService.BASEMAP_DATA.Count <= 0),
            active = true
        };
        GlobalMapService.BASEMAP_DATA.Insert(0, item);
        if (!GlobalMapService.useBasemapService)
        {
            return;
        }
        for (int i = 0; i < GlobalMapService.BASEMAP_DATA.Count; i++)
        {
            BasemapData basemapData = GlobalMapService.BASEMAP_DATA[i];
            if (basemapData.active && basemapData.is_default)
            {
                GlobalMapService.BASEMAP_DATA[0].is_default = false;
                ToggleBasemap(i);
            }
        }
    }

    public void LoadBasemapContent()
    {
        listContainer.DestroyAllChild();
        for (int i = 0; i < GlobalMapService.BASEMAP_DATA.Count; i++)
        {
            BasemapData basemapData = GlobalMapService.BASEMAP_DATA[i];
            if (!basemapData.active)
            {
                continue;
            }
            GameObject gameObject = UnityEngine.Object.Instantiate(prefabListBasemap, listContainer);
            Metadata metadata = gameObject.GetMetadata();
            int index = i;
            metadata.GetMetadataLabel("name").text = basemapData.basemapName;
            MenuToggler component = gameObject.GetComponent<MenuToggler>();
            if (component != null)
            {
                component.onToggleOn.AddListener(delegate
                {
                    ToggleBasemap(index);
                });
            }
            Toggle component2 = gameObject.GetComponent<Toggle>();
            if (component2 != null)
            {
                component2.group = gameObject.GetComponentInParent<ToggleGroup>();
                if (basemapData.is_default)
                {
                    component2.isOn = true;
                }
            }
        }
    }

    private void InitLayer()
    {
        if (GlobalMapService.LAYER_DATA == null)
        {
            return;
        }
        ArcGISCollection<ArcGISLayer> layers = EnvironmentsController.GetMap().Layers;
        if (!GlobalMapService.useLayerService)
        {
            return;
        }
        GlobalMapService.LAYER_DATA = GlobalMapService.LAYER_DATA.OrderBy((LayerData o) => o.index).ToList();
        for (int i = 0; i < GlobalMapService.LAYER_DATA.Count; i++)
        {
            LayerData layerData = GlobalMapService.LAYER_DATA[i];
            switch (layerData.layerType)
            {
                case LayerTypes.ArcGISImageLayer:
                    layers.Add(new ArcGISImageLayer(layerData.layerSource, "", 1f, layerData.active, ""));
                    break;
                case LayerTypes.ArcGIS3DObjectSceneLayer:
                    layers.Add(new ArcGIS3DObjectSceneLayer(layerData.layerSource, "", 1f, layerData.active, ""));
                    break;
                case LayerTypes.ArcGISIntegratedMeshLayer:
                    layers.Add(new ArcGISIntegratedMeshLayer(layerData.layerSource, "", 1f, layerData.active, ""));
                    break;
                case LayerTypes.ArcGISVectorTileLayer:
                    layers.Add(new ArcGISVectorTileLayer(layerData.layerSource, "", 1f, layerData.active, ""));
                    break;
            }
        }
    }

    public void LoadLayerContent()
    {
        listContainer.DestroyAllChild();
        GlobalMapService.LAYER_DATA = GlobalMapService.LAYER_DATA.OrderBy((LayerData o) => o.index).ToList();
        for (int i = 0; i < GlobalMapService.LAYER_DATA.Count; i++)
        {
            LayerData layerData = GlobalMapService.LAYER_DATA[i];
            Metadata metadata = UnityEngine.Object.Instantiate(prefabListLayer, listContainer).GetMetadata();
            int index = i;
            metadata.GetMetadataLabel("name").text = layerData.layerName;
            if (i <= 0)
            {
                metadata.GetMetadataButton("btn-up").gameObject.SetActive(value: false);
            }
            if (i >= GlobalMapService.LAYER_DATA.Count - 1)
            {
                metadata.GetMetadataButton("btn-down").gameObject.SetActive(value: false);
            }
            Toggle listToggleActive = metadata.GetMetadataToggle("toggle");
            if (listToggleActive != null)
            {
                listToggleActive.isOn = layerData.active;
                listToggleActive.onValueChanged.AddListener(delegate
                {
                    ToggleLayer(index, listToggleActive);
                });
            }
            Button metadataButton = metadata.GetMetadataButton("btn-up");
            if (metadataButton != null)
            {
                metadataButton.onClick.AddListener(delegate
                {
                    MoveLayerUp(index);
                });
            }
            Button metadataButton2 = metadata.GetMetadataButton("btn-down");
            if (metadataButton2 != null)
            {
                metadataButton2.onClick.AddListener(delegate
                {
                    MoveLayerDown(index);
                });
            }
        }
    }

    public void LoadTerrainContent()
    {
        if (GlobalMapService.TERRAIN_DATA != null)
        {
            listContainer.DestroyAllChild();
        }
    }

    public void ToggleBasemap(int index)
    {
        for (int i = 0; i < GlobalMapService.BASEMAP_DATA.Count; i++)
        {
            if (GlobalMapService.BASEMAP_DATA[i].is_default)
            {
                GlobalMapService.BASEMAP_DATA[i].is_default = false;
                break;
            }
        }
        GlobalMapService.BASEMAP_DATA[index].is_default = true;
        EnvironmentsController.GetMapComponent().Basemap = GlobalMapService.BASEMAP_DATA[index].basemapSource;
        EnvironmentsController.GetMapComponent().BasemapType = GlobalMapService.BASEMAP_DATA[index].basemapType;
    }

    public void ToggleLayer(int index, Toggle toggler = null)
    {
        GlobalMapService.LAYER_DATA[index].active = toggler.isOn;
        EnvironmentsController.GetMap().Layers.At((ulong)index).IsVisible = GlobalMapService.LAYER_DATA[index].active;
    }

    public void MoveLayerUp(int index)
    {
        GlobalMapService.LAYER_DATA[index].index--;
        GlobalMapService.LAYER_DATA[index - 1].index++;
        EnvironmentsController.GetMap().Layers.RemoveAll();
        InitLayer();
        LoadLayerContent();
    }

    public void MoveLayerDown(int index)
    {
        GlobalMapService.LAYER_DATA[index].index++;
        GlobalMapService.LAYER_DATA[index + 1].index--;
        EnvironmentsController.GetMap().Layers.RemoveAll();
        InitLayer();
        LoadLayerContent();
    }
}
