using System.Xml.Serialization;
using static Wargaming.Core.GlobalParam.DisplaySetup;

public class DisplayData
{
    [XmlAttribute("number")]
    public int displayNumber { get; set; }
    [XmlElement("content")]
    public DisplayContent content { get; set; }
}
