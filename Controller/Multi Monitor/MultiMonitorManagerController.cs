using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

using UnityEngine;
using UnityEngine.Events;

using TMPro;

using Wargaming.Core;
using static Wargaming.Core.GlobalParam.DisplaySetup;

public class MultiMonitorManagerController : MonoBehaviour
{
    [Header("Config File")]
    public string monitorConfig;

    [Header("Display Content")]
    public TextMeshProUGUI contentTitle;
    public TMP_Dropdown displayContent;

    [Header("Events")]
    public UnityEvent OnEdit;
    public UnityEvent AfterContentOk;
    public UnityEvent AfterSubmit;

    public int? selected = null;

    private void Start()
    {
        // Load MONITOR Data from "File"
        if (DISPLAYS == null)
        {
            DISPLAYS = new List<DisplayData>();
        }

        LoadMonitorConfig();

        gameObject.SetActive(false);
    }

    #region REQUEST
    private void LoadMonitorConfig()
    {
        // If Monitor Config File is not defined, throw error message
        if (monitorConfig == null)
        {
            Debug.LogError("Error, Monitor COnfig File is not defined. Please provide one!");
            return;
        }

        var path = Path.Combine(Directory.GetCurrentDirectory(), monitorConfig);
        if (File.Exists(path))
        {
            // Use Current Connection Document if exists
            var _XMLSerialize = new XmlSerializer(typeof(List<DisplayData>));
            using (var reader = new StreamReader(path))
            {
                DISPLAYS = (List<DisplayData>)_XMLSerialize.Deserialize(reader);
            }
        }
        else
        {
            OnResetAll();
        }
    }
    #endregion

    #region SETTINGS_DISPLAY_MANAGER
    public void OnEditCurrentDisplay(int index)
    {
        if (index > DISPLAYS.Count - 1) return;
        ResetDisplayContentForm();

        selected = index;
        contentTitle.text = "Monitor " + (selected.Value + 1) + " Setup";
        var display = DISPLAYS[selected.Value];

        displayContent.value = GetContentIndexByType(display.content);
    }

    public void OnApplyDisplayContent()
    {
        if (selected == null) return;

        var newDisplayContent = GetContentByIndex(displayContent.value);

        for (int i=0; i < DISPLAYS.Count; i++)
        {
            if (DISPLAYS[i].content == DisplayContent.NOTHING) continue;

            if (DISPLAYS[i].content == newDisplayContent)
            {
                DISPLAYS[i].content = DisplayContent.NOTHING;
                break;
            }
        }

        DISPLAYS[selected.Value].content = newDisplayContent;

        AfterContentOk.Invoke();
    }

    public void OnResetAll()
    {
        DISPLAYS = new List<DisplayData>();

        for (int i = 0; i < 4; i++)
        {
            var displayData = new DisplayData();
            displayData.displayNumber = i;
            displayData.content = (i == 0) ? DisplayContent.SIM : DisplayContent.NOTHING;

            DISPLAYS.Add(displayData);
        }

        SaveChanged();
    }

    public void OnSubmit()
    {
        if (DISPLAYS.Count <= 0) return;

        bool isSimFound = false;
        for(int i=0; i < DISPLAYS.Count; i++)
        {
            if (DISPLAYS[i].content == DisplayContent.SIM)
            {
                isSimFound = true;
                break;
            }
        }

        if (!isSimFound)
        {
            DISPLAYS[0].content = DisplayContent.SIM;
        }

        SaveChanged();
        AfterSubmit.Invoke();
    }

    private void ResetDisplayContentForm()
    {
        contentTitle.text = "-";
        displayContent.reset();
    }

    private int GetContentIndexByType(DisplayContent content)
    {
        switch (content)
        {
            case DisplayContent.NOTHING:
                return 0;
            case DisplayContent.SIM:
                return 1;
            case DisplayContent.TIMETABLE:
                return 2;
            case DisplayContent.HARIH:
                return 3;
            case DisplayContent.ENTITY_LIST:
                return 4;
            case DisplayContent.PLAYBACK:
                return 5;
            default: return 0;
        }
    }

    private DisplayContent GetContentByIndex(int index)
    {
        switch (index)
        {
            case 0: return DisplayContent.NOTHING;
            case 1: return DisplayContent.SIM;
            case 2: return DisplayContent.TIMETABLE;
            case 3: return DisplayContent.HARIH;
            case 4: return DisplayContent.ENTITY_LIST;
            case 5: return DisplayContent.PLAYBACK;
            default: return DisplayContent.NOTHING;
        }
    }

    private void SaveChanged()
    {
        // Serialize and Write Changed into Layer Document
        var _XMLSerialize = new XmlSerializer(typeof(List<DisplayData>));
        using (var writer = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), monitorConfig)))
        {
            _XMLSerialize.Serialize(writer, DISPLAYS);
        }
    }
    #endregion
}
