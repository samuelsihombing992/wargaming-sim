using System.Threading.Tasks;
using UnityEngine;

using TMPro;
using Wargaming.Core.GlobalParam;
using Wargaming.Components.DebugComponent;
using Wargaming.Components.Playback;
using Wargaming.Components.Colyseus;
using Wargaming.Core.Network;

namespace Wargaming.Core.WGS
{
    public class GameplayWGSController : MonoBehaviour
    {
        [Header("UI Group")]
        public RectTransform mainUI;
        public RectTransform tacticalMapUI;
        public RectTransform hudUI;
        public RectTransform modalUI;
        public RectTransform logoutUI;

        #region INSTANCE
        public static GameplayWGSController instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private async void Start()
        {
            if (DebugPlayGame.instance != null)
            {
                var state = await DebugPlayGame.instance.Init();
                if (!state)
                {
                    Debug.LogError("FAILED TO PLAY ON DEBUG MODE!");
                    return;
                }

                prepareWGSGameplay();
            }
            else
            {
                prepareWGSGameplay();
            }
        }

        public async void prepareWGSGameplay()
        {
            Debug.Log("Preparing Gameplay...");
            LoadingController.UpdateGameplayLoading("Preparing Gameplay...");
            await Task.Delay(1000);

            // Set username and bagian to any text object with "label-username" and "label-asisten" tag
            setLabelUserAsisten();

            // Set Timetable data
            Debug.Log("Load Timetable Kegiatan...");
            LoadingController.UpdateGameplayLoading("Load Timetable Kegiatan...");
            var kegiatan = await TimetableController.instance.initTimetable();

            Debug.Log("Prepare Playback Timing...");
            LoadingController.UpdateGameplayLoading("Prepare Playback Timing...", 0.05f);
            PlaybackController.instance.InitPlayback(kegiatan);

            
        }

        private void setLabelUserAsisten()
        {
            foreach (var label in GameObject.FindGameObjectsWithTag("label-username"))
            {
                label.GetComponent<TextMeshProUGUI>().text = SessionUser.name;
            }

            foreach (var label in GameObject.FindGameObjectsWithTag("label-asisten"))
            {
                label.GetComponent<TextMeshProUGUI>().text = SessionUser.nama_asisten;
            }
        }
    }
}
