using System;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using TMPro;
using Wargaming.Core.GlobalParam.HelperDataPlotting;
using Wargaming.Core.ScenarioEditor;
using Wargaming.Core.GlobalParam;

using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;

using Battlehub.RTCommon;

using static Wargaming.Core.GlobalParam.HelperDataPlotting.PlottingHelper;
using Random = UnityEngine.Random;
using System.Collections.Generic;
using Wargaming.Core.GlobalParam.HelperPlotting;

public class PlotSatuanController : MonoBehaviour
{
    [Header("Menu Mode")]
    public PlotMode menuMode;

    [Header("Form Menu")]
    public Toggle menuDetail;
    public Toggle menuMisi;
    public Toggle menuFormasi;
    public Toggle menuAvailability;
    public Toggle menuOthers;

    [Header("Form Data")]
    public TMP_InputField nama;
    public TMP_InputField noSatuan;
    public TMP_InputField noAtasan;
    public TMP_InputField kecepatan;
    public TMP_Dropdown typeKecepatan;
    public TMP_InputField bahanBakar;
    public TMP_InputField heading;
    public TMP_InputField keterangan;
    public Toggle useInWGS;
    public Toggle useInTFG;

    public TMP_InputField iconSize;

    [Header("Button References")]
    public Button createButton;
    public Button editButton;
    public Button deleteButton;

    public Toggle missionToggle;
    public Toggle formasiToggle;

    [Header("Asset 3D References")]
    public RawImage assetViewer;
    public TextMeshProUGUI labelSelectedAsset;
    public Animator modalListSatuan;

    [NonSerialized] public ListPlotSatuanData newSatuan;
    [NonSerialized] public ObjekSatuan selectedSatuan;

    // CACHE Object Untuk di-Remove
    [NonSerialized] public List<ObjekTrash> trashData = new List<ObjekTrash>();

    #region INSTANCE
    public static PlotSatuanController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Update()
    {
        if (ScenarioEditorController.instance.mode != MenuMode.SATUAN) return;
        if (menuMode != PlotMode.EDIT) return;
        if (selectedSatuan == null) return;

        // Keep Track of Selected Satuan Location and Rotation
        var location = selectedSatuan.GetComponent<ArcGISLocationComponent>();
        selectedSatuan.info.heading = (int) location.Rotation.Heading;

        heading.text = selectedSatuan.info.heading.ToString();
    }

    #region MODAL HANDLE
    public void CreateMode()
    {
        ScenarioEditorController.instance.mode = MenuMode.SATUAN;
        menuMode = PlotMode.CREATE;

        ResetForm();
        OnModeChange(true);
    }

    public void EditMode()
    {
        ResetForm();
        OnModeChange(false);

        ScenarioEditorController.instance.mode = MenuMode.SATUAN;
        menuMode = PlotMode.EDIT;
    }

    private void OnModeChange(bool isCreate)
    {
        createButton.gameObject.SetActive(isCreate);
        editButton.gameObject.SetActive(!isCreate);
        deleteButton.gameObject.SetActive(!isCreate);
        missionToggle.gameObject.SetActive(!isCreate);
        formasiToggle.gameObject.SetActive(!isCreate);

        menuDetail.isOn = true;
        menuMisi.gameObject.SetActive(!isCreate);
        menuFormasi.gameObject.SetActive(!isCreate);
    }

    public void Close()
    {
        ScenarioEditorController.instance.mode = MenuMode.NONE;
    }
    #endregion

    #region FORM HANDLE
    public void ResetForm()
    {
        nama.text           = "";
        noSatuan.text       = "";
        noAtasan.text       = "";
        kecepatan.text      = "0";
        typeKecepatan.value = 0;
        bahanBakar.text     = "0";
        heading.text        = "0";
        keterangan.text     = "";
        useInWGS.isOn       = true;
        useInTFG.isOn       = true;
        iconSize.text       = "30";

        labelSelectedAsset.text = "PILIH SATUAN";

        newSatuan = null;
        selectedSatuan = null;
    }

    public void SubmitForm()
    {
        if (!CheckFormBeforeSubmit()) return;
        if (newSatuan == null && menuMode == PlotMode.CREATE)
        {
            modalListSatuan.Play("FadeIn");
            return;
        }

        var data = (menuMode == PlotMode.CREATE) ? newSatuan.data : selectedSatuan;
        data.userID = SessionUser.id.GetValueOrDefault(-99);
        data.documentID = NumberConvertionHelper.ConvertToInt(SkenarioAktif.ID_DOCUMENT);

        var info = data.info;
        info.kecepatan          = NumberConvertionHelper.ConvertToDouble(kecepatan.text);
        info.jenisKecepatan     = (SpeedHelper.speedType) typeKecepatan.value;
        info.namaSatuan         = nama.text;
        info.nomerSatuan        = noSatuan.text;
        info.nomerAtasan        = noAtasan.text;
        info.warna              = GetColorByUser();
        info.size               = NumberConvertionHelper.ConvertToFloat(iconSize.text);
        info.armor              = 500; // TIDAK TAU DAPAT DATANYA DARIMANA, SET DEFAULT TO "500"
        info.idDislokasi        = false; // TIDAK TAU DAPAT DATANYA DARIMANA, SET DEFAULT TO "FALSE"
        info.idDislokasiObj     = false; // TIDAK TAU DAPAT DATANYA DARIMANA, SET DEFAULT TO "FALSE"
        info.bahanBakar         = NumberConvertionHelper.ConvertToDouble(bahanBakar.text);
        info.bahanBakarLoad     = 0; // TIDAK TAU DAPAT DATANYA DARIMANA, SET DEFAULT TO "0"
        info.heading            = NumberConvertionHelper.ConvertToInt(heading.text);
        info.keterangan         = keterangan.text;
        info.namaIconSatuan     = "";
        info.widthIconSatuan    = "";
        info.heightIconSatuan   = "";
        info.useInTfg           = useInTFG.isOn;
        info.hideInWgs          = !useInWGS.isOn;

        modalListSatuan.Play("FadeOut");
        if(menuMode == PlotMode.CREATE)
        {
            // Submit Form Create Satuan
            EntityPlacementController.instance.ToggleEntityPlacement(true);
        }
        else
        {
            // Submit Form Update Satuan
            if (newSatuan != null)
            {
                CreateNewSatuan(selectedSatuan.GetComponent<ArcGISLocationComponent>().Position);
                Destroy(selectedSatuan);
            }

            // Refresh Heading on Update Submit
            var location = selectedSatuan.GetComponent<ArcGISLocationComponent>();
            location.Rotation = new ArcGISRotation(double.Parse(heading.text), location.Rotation.Pitch, location.Rotation.Roll);

            GetComponent<Animator>().Play("SlideOut");
        }
    }

    public void DeleteSatuan()
    {
        if (selectedSatuan == null) return;

        EntityController.instance.SATUAN.Remove(selectedSatuan);
        trashData.Add(new ObjekTrash{ value = selectedSatuan.nama, key = "satuan" });

        Destroy(selectedSatuan.gameObject);

        GetComponent<Animator>().Play("SlideOut");
    }

    public void FillForm()
    {
        if (menuMode == PlotMode.CREATE && selectedSatuan != null) return;
        if (menuMode == PlotMode.EDIT && selectedSatuan == null) return;

        var info = (menuMode == PlotMode.CREATE) ? newSatuan.data.info : selectedSatuan.info;

        nama.text               = info.namaSatuan;
        kecepatan.text          = info.kecepatan.ToString();
        typeKecepatan.value     = (int)info.jenisKecepatan;
        noSatuan.text           = info.nomerSatuan;
        noAtasan.text           = info.nomerAtasan;
        bahanBakar.text         = info.bahanBakar.ToString();
        heading.text            = info.heading.ToString();
        keterangan.text         = info.keterangan;
        useInWGS.isOn           = !info.hideInWgs;
        useInTFG.isOn           = info.useInTfg;
        iconSize.text           = info.size.ToString();
        labelSelectedAsset.text = (newSatuan != null) ? newSatuan.objName : "PILIH SATUAN";
    }

    /// <summary>
    /// Cek apakah required field pada form sudah diisikan
    /// </summary>
    private bool CheckFormBeforeSubmit()
    {
        if (isEmpty(nama.text) || isEmpty(noSatuan.text) || isEmpty(noAtasan.text) || isEmpty(kecepatan.text))
        {
            return false;
        }

        return true;
    }

    public bool isEmpty(string text)
    {
        if (text == null || text == "") return true;
        return false;
    }

    /// <summary>
    /// Ketika memilih Satuan Baru dari List
    /// </summary>
    /// <param name="selected"></param>
    public void SelectFromList(ListPlotSatuanData selected)
    {
        if (menuMode == PlotMode.CREATE)
        {
            newSatuan          = new ListPlotSatuanData();
            newSatuan.objId    = selected.objId;
            newSatuan.objName  = selected.objName;
            newSatuan.obj3D    = selected.obj3D;
            newSatuan.data     = selected.data;
        }
        else
        {
            newSatuan           = new ListPlotSatuanData();
            newSatuan.objId    = selected.objId;
            newSatuan.objName  = selected.objName;
            newSatuan.obj3D    = selected.obj3D;
            newSatuan.data     = selectedSatuan;
        }

        FillForm();
    }

    public void OnInputHeadingChange()
    {
        if (selectedSatuan == null) return;

        try
        {
            var objLocationData = selectedSatuan.GetComponent<ArcGISLocationComponent>();
            objLocationData.Rotation = new ArcGISRotation(double.Parse(heading.text), objLocationData.Rotation.Pitch, objLocationData.Rotation.Roll);
        }
        catch (Exception e) { }
    }
    #endregion

    #region OBJECT SATUAN HANDLE
    public void CreateNewSatuan(ArcGISPoint _location)
    {
        var dataSource = (newSatuan != null) ? newSatuan.data : selectedSatuan;

        var objSatuan = EntityController.instance.CreateEntity(_location, new Vector3((float)dataSource.info.heading, 90f, 0), (newSatuan != null) ? newSatuan.obj3D.gameObject : selectedSatuan.gameObject);
        objSatuan.name = GenerateIDSatuan();

        // Generate Bounding Box Collider Untuk Satuan
        EntityController.GenerateBoundingBox(objSatuan);

        // Generate FollowTerrain Script Jika Satuan Adalah Darat atau Udara
        if(dataSource.kategori != EntityHelper.KategoriSatuan.SHIP)
        {
            //var terrainFollower = objSatuan.AddComponent<FollowTerrain>();
            //terrainFollower.syncPlayback = false;

            EntityController.GenerateTerrainFollower(objSatuan);
        }

        var data            = objSatuan.AddComponent<ObjekSatuan>();
        data.userID         = SessionUser.id.GetValueOrDefault(-99);
        data.documentID     = NumberConvertionHelper.ConvertToLong(SkenarioAktif.ID_DOCUMENT);
        data.symbolID       = dataSource.symbolID;
        data.nama           = objSatuan.name;
        data.kategori       = dataSource.kategori;
        data.style          = dataSource.style;
        data.symbol         = dataSource.symbol;
        data.info           = dataSource.info;
        data.id_kegiatan    = dataSource.id_kegiatan;
        data.isi_logistik   = dataSource.isi_logistik;

        data.symbol.warna   = data.info.warna;

        

        data.hudElement = EntityController.instance.CreateHUDElementV2(
            objSatuan.transform,
            dataSource.info.warna,
            dataSource.info.namaSatuan,
            EntityHelper.getFontTaktis(dataSource.symbol.fontFamily),
            EntityHelper.getFontTaktis(dataSource.symbol.fontFamily + "_BG"),
            dataSource.symbol.fontIndex
        );

        var editorTransform = objSatuan.AddComponent<ExposeToEditor>();
        editorTransform.CanTransform         = true;
        editorTransform.CanDuplicate         = true;
        editorTransform.CanDelete            = true;
        editorTransform.ShowSelectionGizmo   = true;
        editorTransform.Selected             = new ExposeToEditorUnityEvent();
        editorTransform.Unselected           = new ExposeToEditorUnityEvent();

        editorTransform.Selected.AddListener(delegate { SelectSatuan(data); data.hudElement.gameObject.SetActive(false); });
        editorTransform.Unselected.AddListener(delegate { UnselectSatuan(); data.hudElement.gameObject.SetActive(true); });

        EntityController.instance.SATUAN.Add(data);
    }

    public string GenerateIDSatuan()
    {
        return "satuan_" + (EntityController.instance.SATUAN.Count + 1) + "_" + SessionUser.id + "_" + Random.Range(1, 1000);
    }

    public void SelectSatuan(ObjekSatuan _data)
    {
        EditMode();
        selectedSatuan = _data;

        PlotTransformController.instance.toggleTransformMenu(true);

        FillForm();
        labelSelectedAsset.text = "GANTI SATUAN";
    }

    public void UnselectSatuan()
    {
        if(ScenarioEditorController.instance.mode == MenuMode.SATUAN)
        {
            ScenarioEditorController.instance.mode = MenuMode.NONE;
        }

        menuMode = PlotMode.CREATE;
        selectedSatuan = null;

        PlotTransformController.instance.toggleTransformMenu(false);
        GetComponent<Animator>().Play("SlideOut");
    }
    #endregion
}