using System.Threading.Tasks;

using UnityEngine;
using Unity.Mathematics;

using TMPro;
using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;
using Esri.HPFramework;

using static Wargaming.Core.GlobalParam.HelperDataPlotting.PlottingHelper;
using Wargaming.Core.ScenarioEditor.Drawing;

namespace Wargaming.Core.ScenarioEditor
{
    public class EntityPlacementController : MonoBehaviour
    {
        [Header("State")]
        public bool onUse = false;

        [Header("References")]
        public ArcGISMapComponent mapComponent;
        public TextMeshProUGUI labelMessage;

        private ScenarioEditorController controller;

        public float lastClickTime;
        public const float DOUBLE_CLICK_TIME = 0.2f;

        private string defaultMessage = "Click on the map to place the object...\nPress \"ESC\" to cancel";

        #region INSTANCE
        public static EntityPlacementController instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private void Start()
        {
            controller = ScenarioEditorController.instance;
        }

        public void ToggleEntityPlacement(bool state)
        {
            ToggleEntityPlacement(state, null, null);
        }

        public void ToggleEntityPlacement(bool state, bool? overrideState = null, string _message = null)
        {
            GetComponent<Animator>().Play((state) ? "FadeIn" : "FadeOut");

            SetMessage((_message == null) ? defaultMessage : _message);

            switch (controller.mode)
            {
                case MenuMode.SATUAN:
                    PlotSatuanController.instance.GetComponent<Animator>().Play((state) ? "SlideOut" : "SlideIn");
                    onUse = state;
                    break;
                case MenuMode.PASUKAN:

                    break;
                case MenuMode.MISI:
                    PlotMisiController.instance.GetComponent<Animator>().Play((state) ? "SlideOut" : "SlideIn");
                    onUse = state;
                    break;
                case MenuMode.FORMASI:

                    break;
                case MenuMode.OBSTACLE:

                    break;
                case MenuMode.RADAR:

                    break;
                case MenuMode.KEKUATAN:

                    break;
                case MenuMode.BUNGUS:

                    break;
                case MenuMode.SITUASI:

                    break;
                case MenuMode.LOGISTIK:

                    break;
                case MenuMode.DRAWING:
                    if (state)
                    {
                        PlotDrawingController.instance.OpenModal();
                    }
                    else
                    {
                        DrawingToolsController.instance.EndDrawing();
                    }

                    onUse = (overrideState.HasValue) ? overrideState.GetValueOrDefault() : state;
                    break;
            }
        }

        public void ToggleModal()
        {
            bool state = false;
            switch (controller.mode)
            {
                case MenuMode.SATUAN:
                    state = PlotSatuanController.instance.GetComponent<CanvasGroup>().alpha == 0;
                    PlotSatuanController.instance.GetComponent<Animator>().Play((state) ? "SlideIn" : "SlideOut");
                    break;
                case MenuMode.PASUKAN:

                    break;
                case MenuMode.MISI:

                    break;
                case MenuMode.FORMASI:

                    break;
                case MenuMode.OBSTACLE:

                    break;
                case MenuMode.RADAR:

                    break;
                case MenuMode.KEKUATAN:

                    break;
                case MenuMode.BUNGUS:

                    break;
                case MenuMode.SITUASI:

                    break;
                case MenuMode.LOGISTIK:

                    break;
            }
        }

        private async void Update()
        {
            if (!onUse) return;
            if (onEscape()) { ToggleEntityPlacement(false); return; }

            if (await onNonHold())
            {
                Debug.Log("Double Click on Map...");

                var raycastHit = OnRaycastPlacement();
                if (!raycastHit.HasValue) return;

                var mapPoint = HitToGeoPosition(raycastHit.GetValueOrDefault());
                PlotObject(mapPoint);
            }
        }

        private void PlotObject(ArcGISPoint _location)
        {
            switch (controller.mode)
            {
                case MenuMode.SATUAN:
                    PlotSatuanController.instance.CreateNewSatuan(_location);

                    GetComponent<Animator>().Play("FadeOut");
                    onUse = false;

                    ScenarioEditorController.instance.mode = MenuMode.NONE;
                    break;
                case MenuMode.PASUKAN:

                    break;
                case MenuMode.MISI:
                    PlotMisiController.instance.CreateNewNode(_location);
                    break;
                case MenuMode.FORMASI:

                    break;
                case MenuMode.OBSTACLE:

                    break;
                case MenuMode.RADAR:

                    break;
                case MenuMode.KEKUATAN:

                    break;
                case MenuMode.BUNGUS:

                    break;
                case MenuMode.SITUASI:

                    break;
                case MenuMode.LOGISTIK:

                    break;
                case MenuMode.DRAWING:
                    PlotDrawingController.instance.CreateNewDrawing(_location);
                    break;
            }
        }

        private void SetMessage(string _message)
        {
            labelMessage.text = _message;
        }

        //private async void LateUpdate()
        //{
        //    if (!onUse) return;

        //    if (onEscape()) return;

        //    if (Input.GetKeyDown(KeyCode.Escape))
        //    {
        //        ToggleEntityPlacements(false);
        //    }else if (Input.GetMouseButtonDown(0))
        //    {
        //        await Task.Delay(100);
        //        if (Input.GetMouseButton(0)) return;

        //        //Debug.Log("Click on Map");

        //        var raycastHit = OnRaycastPlacement();
        //        if (!raycastHit.HasValue) return;

        //        var mapPoint = HitToGeoPosition(raycastHit.GetValueOrDefault());
        //        var objRotation = new Vector3(PlotSatuanController.instance.selectedSatuan.data.info.heading.GetValueOrDefault(0), 90f, 0);
        //        //var objRotation = new Vector3((float) WargamingCam.instance.GetComponent<ArcGISLocationComponent>().Rotation.Heading, 90f, 0);

        //        //Debug.Log(direction);
        //        if (ScenarioEditorController.instance.onPlotSatuanMenu)
        //        {
        //            CreateNewSatuan(PlotSatuanController.instance.selectedSatuan.obj3D.gameObject, mapPoint, objRotation);
        //        }

        //        GetComponent<Animator>().Play("FadeOut");
        //        onUse = false;
        //    }
        //}

        private bool onEscape() { return Input.GetKeyDown(KeyCode.Escape); }
        private bool onClick() { return Input.GetMouseButtonDown(0); }
        private bool onHold() { return Input.GetMouseButton(0); }
        private async Task<bool> onNonHold()
        {
            if (!onClick()) return false;

            await Task.Delay(100);
            if (onHold()) return false;

            return true;
        }

        public RaycastHit? OnRaycastPlacement()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit) && !UICanvasSettings.IsRaycastingWithUI())
            {
                //Debug.Log("Raycast Hit");
                return hit;
            }
            else
            {
                //Debug.Log("Raycast Not Hit");
                return null;
            }
        }

        /// <summary>
        /// Return GeoPosition for an engine location hit by a raycast.
        /// </summary>
        /// <param name="hit"></param>
        /// <returns></returns>
        public ArcGISPoint HitToGeoPosition(RaycastHit hit, float yOffset = 0)
        {
            var worldPosition = math.inverse(mapComponent.WorldMatrix).HomogeneousTransformPoint(hit.point.ToDouble3());
            var geoPosition = mapComponent.View.WorldToGeographic(worldPosition);

            var altitude = geoPosition.Z + yOffset;
            if (altitude < StartupConfig.settings.DEFAULT_OCEAN_HEIGHT)
            {
                altitude = (long) StartupConfig.settings.DEFAULT_OCEAN_HEIGHT;
            }
           
            var offsetPosition = new ArcGISPoint(geoPosition.X, geoPosition.Y, altitude, geoPosition.SpatialReference);


            return GeoUtils.ProjectToSpatialReference(offsetPosition, new ArcGISSpatialReference(4326));
        }
    }
}
