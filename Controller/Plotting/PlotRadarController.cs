using Battlehub.RTCommon;
using Esri.ArcGISMapsSDK.Components;
using Esri.GameEngine.Geometry;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Wargaming.Core.GlobalParam;
using Wargaming.Core.GlobalParam.HelperPlotting;
using Wargaming.Core.ScenarioEditor;
using static Wargaming.Core.GlobalParam.EntityHelper;
using static Wargaming.Core.GlobalParam.HelperDataPlotting.PlottingHelper;
using Random = UnityEngine.Random;

public class PlotRadarController : MonoBehaviour
{
    [Header("Menu Mode")]
    public PlotMode menuMode;

    [Header("Form Menu")]
    public Toggle menuDetail;
    public Toggle menuOthers;

    [Header("Form Data")]
    public TMP_InputField nama;
    public TMP_InputField radius;
    public TMP_Dropdown jenis;

    public TMP_InputField iconSize;

    [Header("Button References")]
    public Button createButton;
    public Button editButton;
    public Button deleteButton;

    [Header("Asset References")]
    public TextMeshProUGUI labelSelectedAsset;
    public Animator modalListRadar;

    [NonSerialized] public ListPlotRadarData newRadar;
    [NonSerialized] public ObjekRadar selectedRadar;

    // CACHE Object Untuk di-Remove
    [NonSerialized] public List<ObjekTrash> trashData = new List<ObjekTrash>();

    #region INSTANCE
    public static PlotRadarController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Update()
    {
        if (ScenarioEditorController.instance.mode != MenuMode.RADAR) return;
        if (menuMode != PlotMode.EDIT) return;
        if (selectedRadar == null) return;
    }

    #region MODAL HANDLE
    public void CreateMode()
    {
        ScenarioEditorController.instance.mode = MenuMode.RADAR;
        menuMode = PlotMode.CREATE;

        ResetForm();
        OnModeChange(true);
    }

    public void EditMode()
    {
        ResetForm();
        OnModeChange(false);

        ScenarioEditorController.instance.mode = MenuMode.RADAR;
        menuMode = PlotMode.EDIT;
    }

    private void OnModeChange(bool isCreate)
    {
        createButton.gameObject.SetActive(isCreate);
        editButton.gameObject.SetActive(!isCreate);
        deleteButton.gameObject.SetActive(!isCreate);

        menuDetail.isOn = true;
    }

    public void Close()
    {
        ScenarioEditorController.instance.mode = MenuMode.NONE;
    }
    #endregion

    #region FORM HANDLE
    public void ResetForm()
    {
        nama.text       = "";
        radius.text     = "0";
        jenis.value     = 0;
        iconSize.text   = "30";

        labelSelectedAsset.text = "PILIH RADAR";

        newRadar = null;
        selectedRadar = null;
    }

    public void SubmitForm()
    {
        if (!CheckFormBeforeSubmit()) return;
        if(newRadar == null && menuMode == PlotMode.CREATE)
        {
            modalListRadar.Play("FadeIn");
            return;
        }

        var data = (menuMode == PlotMode.CREATE) ? newRadar.data : selectedRadar;
        data.userID = SessionUser.id.GetValueOrDefault(-99);
        data.documentID = NumberConvertionHelper.ConvertToInt(SkenarioAktif.ID_DOCUMENT);

        var info = data.info;
        info.nama           = nama.text;
        info.radius         = NumberConvertionHelper.ConvertToInt(radius.text);
        info.jenis          = (JenisRadar) jenis.value;
        info.size           = NumberConvertionHelper.ConvertToFloat(iconSize.text);

        modalListRadar.Play("FadeOut");
        if(menuMode == PlotMode.CREATE)
        {
            // Submit Form Create Radar
            EntityPlacementController.instance.ToggleEntityPlacement(true);
        }
        else
        {
            // Submit Form Update Radar
            if(newRadar != null){
                CreateNewRadar(selectedRadar.GetComponent<ArcGISLocationComponent>());
                Destroy(selectedRadar);
            }

            // Refresh Radius on Update Submit
            selectedRadar.transform.SetScale(NumberConvertionHelper.ConvertToFloat(radius.text));

            GetComponent<Animator>().Play("SlideOut");
        }
    }

    public void DeleteRadar()
    {
        if (selectedRadar == null) return;

        EntityController.instance.RADAR.Remove(selectedRadar);
        trashData.Add(new ObjekTrash { value = selectedRadar.nama, key = "radar" });

        Destroy(selectedRadar.gameObject);

        GetComponent<Animator>().Play("SlideOut");
    }

    public void FillForm()
    {
        if (menuMode == PlotMode.CREATE && selectedRadar != null) return;
        if (menuMode == PlotMode.EDIT && selectedRadar == null) return;

        var info = (menuMode == PlotMode.CREATE) ? newRadar.data.info : selectedRadar.info;

        nama.text                   = info.nama;
        radius.text                 = info.radius.ToString();
        jenis.value                 = (int) info.jenis;
        iconSize.text               = info.size.ToString();
        labelSelectedAsset.text = (newRadar != null) ? newRadar.objName : "PILIH RADAR";
    }

    /// <summary>
    /// Cek apakah required field pada form sudah diisikan
    /// </summary>
    private bool CheckFormBeforeSubmit()
    {
        if(isEmpty(nama.text) || isEmpty(radius.text) || isEmpty(iconSize.text))
        {
            return false;
        }

        return true;
    }

    public bool isEmpty(string text)
    {
        if (text == null || text == "") return true;
        return false;
    }

    /// <summary>
    /// Ketika memilih Radar Baru dari List
    /// </summary>
    /// <param name="selected"></param>
    public void SelectFromList(ListPlotRadarData selected)
    {
        if(menuMode == PlotMode.CREATE)
        {
            newRadar            = new ListPlotRadarData();
            newRadar.objId      = selected.objId;
            newRadar.objName    = selected.objName;
            newRadar.data       = selected.data;
        }
        else
        {
            newRadar            = new ListPlotRadarData();
            newRadar.objId      = selected.objId;
            newRadar.objName    = selected.objName;
            newRadar.data       = selectedRadar;
        }

        FillForm();
    }

    public void OnInputRadiusChange()
    {
        if (selectedRadar == null) return;

        try
        {
            selectedRadar.transform.SetScale(int.Parse(radius.text));
        }catch(Exception e) { }
    }
    #endregion

    #region OBJECT RADAR HANDLE
    public void CreateNewRadar(ArcGISLocationComponent _location)
    {
        var dataSource  = (newRadar != null) ? newRadar.data : selectedRadar;

        var objRadar    = EntityController.instance.CreateEntity(_location, (newRadar != null) ? AssetPackageController.Instance.prefabRadar : selectedRadar.gameObject);
        objRadar.name   = GenerateIDRadar();

        // Generate Follow Terrain Script
        var terrainFollower = objRadar.AddComponent<FollowTerrain>();
        terrainFollower.syncPlayback = true;

        EntityController.instance.CreateHUDElementV2(
            objRadar.transform,
            dataSource.info.warna,
            dataSource.info.nama,
            EntityHelper.getFontTaktis(dataSource.symbol.symbol.fontFamily),
            EntityHelper.getFontTaktis(dataSource.symbol.symbol.fontFamily + "_BG"),
            dataSource.symbol.symbol.fontIndex
        );

        var data            = objRadar.AddComponent<ObjekRadar>();
        data.userID         = SessionUser.id.GetValueOrDefault(-99);
        data.documentID     = NumberConvertionHelper.ConvertToLong(SkenarioAktif.ID_DOCUMENT);
        data.symbolID       = dataSource.symbolID;
        data.nama           = objRadar.name;
        data.jenis          = dataSource.jenis;
        data.symbol         = dataSource.symbol;
        data.info           = dataSource.info;
        data.info_symbol    = dataSource.info_symbol;

        data.symbol.symbol.warna = data.info.warna;

        var editorTransform = objRadar.AddComponent<ExposeToEditor>();
        editorTransform.CanTransform = true;
        editorTransform.CanDuplicate = true;
        editorTransform.CanDelete = true;
        editorTransform.ShowSelectionGizmo = true;
        editorTransform.Selected = new ExposeToEditorUnityEvent();
        editorTransform.Unselected = new ExposeToEditorUnityEvent();

        editorTransform.Selected.AddListener(delegate { SelectRadar(data); });
        editorTransform.Unselected.AddListener(delegate { UnselectRadar(); });

        EntityController.instance.RADAR.Add(data);
    }

    public string GenerateIDRadar()
    {
        return "radar_" + (EntityController.instance.RADAR.Count + 1) + "_" + SessionUser.id + "_" + Random.Range(1, 1000);
    }

    public void SelectRadar(ObjekRadar _data)
    {
        EditMode();
        selectedRadar = _data;

        PlotTransformController.instance.toggleTransformMenu(true);

        FillForm();
        labelSelectedAsset.text = "GANTI RADAR";
    }

    public void UnselectRadar()
    {
        if(ScenarioEditorController.instance.mode == MenuMode.RADAR)
        {
            ScenarioEditorController.instance.mode = MenuMode.NONE;
        }

        menuMode = PlotMode.CREATE;
        selectedRadar = null;

        PlotTransformController.instance.toggleTransformMenu(false);
        GetComponent<Animator>().Play("SlideOut");
    }
    #endregion
}