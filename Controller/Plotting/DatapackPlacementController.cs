using System.Threading.Tasks;

using UnityEngine;
using Unity.Mathematics;

using TMPro;

using Esri.ArcGISMapsSDK.Components;
using Esri.GameEngine.Geometry;
using Esri.HPFramework;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;

public class DatapackPlacementController : MonoBehaviour
{
    [Header("State")]
    public bool onUse = false;

    [Header("References")]
    public ArcGISMapComponent mapComponent;
    public TextMeshProUGUI labelMessage;

    public float lastClickTime;
    public const float DOUBLE_CLICK_TIME = 0.2f;

    #region INSTANCE
    public static DatapackPlacementController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void TogglePlacement(bool state)
    {
        onUse = state;
        labelMessage.GetComponent<Animator>().Play((state) ? "FadeIn" : "FadeOut");
    }

    private async void Update()
    {
        if (!onUse) return;
        if (onEscape()) { TogglePlacement(false); return; }

        if (await onNonHold())
        {
            Debug.Log("Double Click on Map...");

            var raycastHit = OnRaycastPlacement();
            if (!raycastHit.HasValue) return;

            var mapPoint = HitToGeoPosition(raycastHit.GetValueOrDefault());
            PlotObject(mapPoint);

            TogglePlacement(false);
        }
    }

    private void PlotObject(ArcGISPoint _location)
    {
        //if (DatapackEditor.instance == null || DatapackEditor.instance.selected == null) return;

        DatapackEditor.instance.AddAssetToTemplate(_location);
    }

    #region RAYCAST PLACEMENT
    private bool onEscape() { return Input.GetKeyDown(KeyCode.Escape); }
    private bool onClick() { return Input.GetMouseButtonDown(0); }
    private bool onHold() { return Input.GetMouseButton(0); }
    private async Task<bool> onNonHold()
    {
        if (!onClick()) return false;

        await Task.Delay(100);
        if (onHold()) return false;

        return true;
    }

    public RaycastHit? OnRaycastPlacement()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit) && !UICanvasSettings.IsRaycastingWithUI())
        {
            //Debug.Log("Raycast Hit");
            return hit;
        }
        else
        {
            //Debug.Log("Raycast Not Hit");
            return null;
        }
    }

    /// <summary>
    /// Return GeoPosition for an engine location hit by a raycast.
    /// </summary>
    /// <param name="hit"></param>
    /// <returns></returns>
    public ArcGISPoint HitToGeoPosition(RaycastHit hit, float yOffset = 0)
    {
        var worldPosition = math.inverse(mapComponent.WorldMatrix).HomogeneousTransformPoint(hit.point.ToDouble3());
        var geoPosition = mapComponent.View.WorldToGeographic(worldPosition);

        var altitude = geoPosition.Z + yOffset;
        if (altitude < StartupConfig.settings.DEFAULT_OCEAN_HEIGHT)
        {
            altitude = (long)StartupConfig.settings.DEFAULT_OCEAN_HEIGHT;
        }

        var offsetPosition = new ArcGISPoint(geoPosition.X, geoPosition.Y, altitude, geoPosition.SpatialReference);


        return GeoUtils.ProjectToSpatialReference(offsetPosition, new ArcGISSpatialReference(4326));
    }
    #endregion
}
