using System;

using UnityEngine;

using Battlehub.RTHandles;
using Battlehub.RTCommon;
using FIMSpace.GroundFitter;

public class PlotTransformController : MonoBehaviour
{
    [Header("Transform Menu")]
    public GameObject transformMenu;

    #region INSTANCE
    public static PlotTransformController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void toggleTransformMenu(bool state)
    {
        transformMenu.SetActive(state);
    }

    public void OnTransformClick()
    {
        try
        {
            RuntimeToolsInput.m_editor.Tools.Current = RuntimeTool.Move;
        }catch(Exception e) { }
    }

    public void OnRotateClick()
    {
        try
        {
            RuntimeToolsInput.m_editor.Tools.Current = RuntimeTool.Rotate;
        }catch(Exception e) { }
    }

    public void UpdateGroundFitterRotation(GameObject objSelected)
    {
        var terrainFollower = objSelected.GetComponent<FGroundFitter>();
        if (terrainFollower == null) return;

        var objData = objSelected.GetComponent<ObjekSatuan>();

        terrainFollower.UpAxisRotation  = (float) objData.info.heading;
        terrainFollower.enabled         = true;
    }
}
