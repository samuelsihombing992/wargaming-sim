using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
using static Wargaming.Core.GlobalParam.HelperDataPlotting.PlottingHelper;
using UnityEngine.UI;
using Wargaming.Core.ScenarioEditor;
using Esri.GameEngine.Geometry;
using System;
using Wargaming.Core.GlobalParam;
using Esri.ArcGISMapsSDK.Components;
using Wargaming.Components.Controller;
using System.Threading.Tasks;
using Wargaming.Core;
using FluffyUnderware.Curvy.Components;
using SickscoreGames.HUDNavigationSystem;

public class PlotMisiController : MonoBehaviour
{
    [Header("Menu Mode")]
    public PlotMode menuMode;

    [Header("Form Data")]
    public TMP_InputField nama;
    public TMP_InputField kecepatan;
    public TMP_Dropdown typeKecepatan;
    public TMP_Dropdown startPosition;
    public TMP_Dropdown endPosition;
    public TMP_InputField waktuSampai;

    [Header("Button References")]
    public Button createButton;
    public Button editButton;
    public Button deleteButton;
    public Button embarkasiParentButton;
    public Button debarkasiParentButton;
    public Button kegiatanButton;
    public Button waktuButton;

    [Header("Label References")]
    public TextMeshProUGUI labelSatuan;
    public TextMeshProUGUI labelEmbarkasiParennt;
    public TextMeshProUGUI labelDebarkasiParent;
    public TextMeshProUGUI labelKegiatan;
    public TextMeshProUGUI labelWaktu;

    [Header("Other References")]
    public GameObject prefabNodeList;
    public Transform nodeListContainer;
    public Transform missionContainer;
    public Material lineRenderer;

    //public PlotDataSatuan selectedSatuanObj;
    public ObjekSatuan satuanOwned;
    public PlotMissionData kegiatanData;
    public MissionWalker selectedKegiatan;
    public Sprite iconWaypoint;

    #region INSTANCE
    public static PlotMisiController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    ///// <summary>
    ///// Cek apakah required field pada form sudah diisikan
    ///// </summary>
    //public bool CheckFormBeforeSubmit()
    //{
    //    if (isEmpty(nama.text) || isEmpty(nama.text) || isEmpty(kecepatan.text))
    //    {
    //        return false;
    //    }

    //    return true;
    //}

    public bool isEmpty(string text)
    {
        if (text == null || text == "") return true;
        return false;
    }

    public void EnterCreateMode()
    {
        ScenarioEditorController.instance.mode = MenuMode.MISI;
        menuMode = PlotMode.CREATE;

        DiscardForm();
        OnModeChange();

        satuanOwned = PlotSatuanController.instance.selectedSatuan;
        labelSatuan.text = satuanOwned.nama;
    }

    public void EnterEditMode()
    {
        ScenarioEditorController.instance.mode = MenuMode.MISI;
        menuMode = PlotMode.EDIT;

        DiscardForm();

        OnModeChange();
    }

    public void OnModeChange()
    {
        createButton.gameObject.SetActive(menuMode == PlotMode.CREATE);

        editButton.gameObject.SetActive(menuMode == PlotMode.EDIT);
        deleteButton.gameObject.SetActive(menuMode == PlotMode.EDIT);
    }

    public void ExitMenu()
    {
        ScenarioEditorController.instance.mode = MenuMode.NONE;
    }

    public void DiscardForm()
    {
        nama.text               = "";
        kecepatan.text          = "";
        typeKecepatan.value     = 0;

        labelSatuan.text                = "No Data";
        labelEmbarkasiParennt.text      = "-- Pilih Satuan --";
        labelDebarkasiParent.text       = "-- Pilih Satuan --";
        labelKegiatan.text              = "-- Pilih Kegiatan --";
        labelWaktu.text                 = "-- Pilih Kegiatan --";

        satuanOwned = null;
        kegiatanData = null;
        selectedKegiatan = null;
    }

    /// <summary>
    /// Ubah isi Bagian Kegiatan dan Waktu pada Form ketika kegiatan diubah / dipilih
    /// </summary>
    public void OnKegiatanChangeFillForm()
    {
        if (TimetableController.instance == null) return;
        if (kegiatanData == null) return;

        labelKegiatan.text      = kegiatanData.keterangan;
        labelWaktu.text         = kegiatanData.timeKegiatan.ToString("dd-MM-yyyy HH:mm:ss");
        kecepatan.text          = satuanOwned.info.kecepatan.ToString();
        typeKecepatan.value     = (int) satuanOwned.info.jenisKecepatan;

        CalculateWaktuSampai();
    }

    /// <summary>
    /// Ketika memilih Kegiatan Baru dari List
    /// </summary>
    /// <param name="selected"></param>
    public void SelectKegiatan()
    {
        if (TimetableController.instance == null) return;

        if (menuMode == PlotMode.CREATE)
        {
            var kegiatan = TimetableController.instance.SELECTED_KEGIATAN.GetComponent<DataKegiatan>();

            kegiatanData                = new PlotMissionData();
            kegiatanData.timeKegiatan   = kegiatan.timeKegiatan;
            kegiatanData.id             = kegiatan.id;
            kegiatanData.keterangan     = kegiatan.keterangan;

            OnKegiatanChangeFillForm();
        }
    }

    private void CalculateWaktuSampai()
    {

    }

    private void Update()
    {
        if (satuanOwned == null) return;


    }

    #region OBJECT MISI HANDLE
    public async void CreateNewNode(ArcGISPoint _location)
    {
        if(selectedKegiatan == null)
        {
            selectedKegiatan = Instantiate(AssetPackageController.Instance.prefabRoute, missionContainer).GetComponent<MissionWalker>();
            selectedKegiatan.GetComponent<ArcGISLocationComponent>().Position = _location;

            selectedKegiatan.gameObject.AddComponent<CurvyLineRenderer>();
            selectedKegiatan.GetComponent<LineRenderer>().material = lineRenderer;

            selectedKegiatan.name = GenerateIDMission();
        }

        var nodeLocation = new Vector3((float)_location.X, (float)_location.Y, (float)_location.Z);

        //var objNodeMission = EntityController.instance.CreateEntity(_location, Vector3.zero, AssetPackageController.Instance.prefabEditorNode, selectedKegiatan.transform);
        var objNodeMission = Instantiate(AssetPackageController.Instance.prefabNode, selectedKegiatan.transform);
        objNodeMission.transform.SetCoordinate(nodeLocation);
        objNodeMission.transform.SetOrientation(new Vector3(0, 0, -90));

        selectedKegiatan.DATA.Add(
            new GlobalHelper.MissionWalkerParam
            {
                coordinate  = nodeLocation,
            }
        );

        var HUD = Instantiate(AssetPackageController.Instance.prefabHUD, objNodeMission.transform).GetComponent<HUDNavigationElement>();
        HUD.name = "HUD_ELEMENT";
        HUD.transform.localPosition = new Vector3(0, 0, 5);
        //HUD.transform.localPosition = new Vector3(0, bounding.extents.y, 0);

        HUD.Prefabs.IndicatorPrefab = AssetPackageController.Instance.prefabMisiIndicator;
        HUD.OnElementReady.AddListener(delegate { onHUDReadyV2(HUD, "Waypoint " + selectedKegiatan.DATA.Count); });

        EntityController.GenerateBoundingBox(objNodeMission);

        await Task.Delay(10);
        Destroy(objNodeMission.GetComponent<ArcGISLocationComponent>());

        RefreshNodeList();
    }

    public void onHUDReadyV2(HUDNavigationElement HUD, string nama)
    {
        // Resetting data HUDElement setiap kali reload element
        HUD.Indicator.ChangeLabelEntity(nama);
        HUD.Indicator.ChangeIcon(iconWaypoint);
        HUD.Indicator.ToggleCustomTransform("simbol_taktis", false);
        HUD.Indicator.ToggleCustomTransform("simbol_taktis_bg", false);
        HUD.Indicator.ToggleCustomTransform("icon-image", true);

        //if (fontTaktisBG != null)
        //{
        //    HUD.Indicator.ChangeSimbolTaktisBackground(warna);
        //}
        //else
        //{
        //    HUD.Indicator.ChangeSimbolTaktisColor(warna);
        //}
    }

    private void RefreshNodeList()
    {
        foreach(Transform list in nodeListContainer.transform)
        {
            Destroy(list.gameObject);
        }

        for(int i=0; i < selectedKegiatan.DATA.Count; i++)
        {
            var index = i;

            var nodeList        = Instantiate(prefabNodeList, nodeListContainer);
            var mCoordinate     = nodeList.GetComponent<Metadata>().FindParameter("label-coordinate");
            var mRemove         = nodeList.GetComponent<Metadata>().FindParameter("btn-remove");

            mCoordinate.GetComponent<TextMeshProUGUI>().text = selectedKegiatan.DATA[i].coordinate.x + ", " + selectedKegiatan.DATA[i].coordinate.y + ", " + selectedKegiatan.DATA[i].coordinate.z;

            //nodeList.GetComponent<Button>().onClick.AddListener(delegate { WargamingCam.instance.LocateObject(nodeList.gameObject); });
            mRemove.GetComponent<Button>().onClick.AddListener(delegate { DeleteNode(index); });
        }
    }

    private void DeleteNode(int index)
    {
        selectedKegiatan.DATA.RemoveAt(index);

        var nodeObj = selectedKegiatan.transform.GetChild(index).gameObject;
        Destroy(nodeObj);

        RefreshNodeList();
    }

    //public void CreateNewSatuan(ArcGISPoint _location)
    //{
    //    var dataSource = (newSatuan != null) ? newSatuan.data : selectedSatuan;

    //    var objSatuan = EntityController.instance.CreateEntity(_location, new Vector3((float)dataSource.info.heading, 90f, 0), (newSatuan != null) ? newSatuan.obj3D.gameObject : selectedSatuan.gameObject);
    //    objSatuan.name = GenerateIDSatuan();

    //    // Generate Bounding Box Collider Untuk Satuan
    //    EntityController.instance.GenerateBoundingBox(objSatuan);

    //    // Generate FollowTerrain Script Jika Satuan Adalah Darat atau Udara
    //    if (dataSource.kategori != EntityHelper.KategoriSatuan.SHIP)
    //    {
    //        var terrainFollower = objSatuan.AddComponent<FollowTerrain>();
    //        terrainFollower.syncPlayback = false;
    //    }

    //    EntityController.instance.CreateHUDElementV2(
    //        objSatuan.transform,
    //        dataSource.info.warna,
    //        dataSource.info.namaSatuan,
    //        EntityHelper.getFontTaktis(dataSource.symbol.fontFamily),
    //        EntityHelper.getFontTaktis(dataSource.symbol.fontFamily + "_BG"),
    //        dataSource.symbol.fontIndex
    //    );

    //    var data = objSatuan.AddComponent<ObjekSatuan>();
    //    data.userID = SessionUser.id.GetValueOrDefault(-99);
    //    data.documentID = NumberConvertionHelper.ConvertToLong(SkenarioAktif.ID_DOCUMENT);
    //    data.symbolID = dataSource.symbolID;
    //    data.nama = objSatuan.name;
    //    data.kategori = dataSource.kategori;
    //    data.style = dataSource.style;
    //    data.symbol = dataSource.symbol;
    //    data.info = dataSource.info;
    //    data.id_kegiatan = dataSource.id_kegiatan;
    //    data.isi_logistik = dataSource.isi_logistik;

    //    data.symbol.warna = data.info.warna;

    //    var editorTransform = objSatuan.AddComponent<ExposeToEditor>();
    //    editorTransform.CanTransform = true;
    //    editorTransform.CanDuplicate = true;
    //    editorTransform.CanDelete = true;
    //    editorTransform.ShowSelectionGizmo = true;
    //    editorTransform.Selected = new ExposeToEditorUnityEvent();
    //    editorTransform.Unselected = new ExposeToEditorUnityEvent();

    //    editorTransform.Selected.AddListener(delegate { SelectSatuan(data); });
    //    editorTransform.Unselected.AddListener(delegate { UnselectSatuan(); });

    //    EntityController.instance.SATUAN.Add(data);
    //}
    #endregion


    #region OBJECT SATUAN HANDLE
    public string GenerateIDMission()
    {
        return "mission_" + UnityEngine.Random.Range(1, 1000) + "_" + satuanOwned.nama + "_" + kegiatanData.timeKegiatan.Millisecond;
    }
    #endregion
}

[Serializable]
public class PlotMissionData
{
    public DateTime timeKegiatan;

    [Header("Data")]
    public string id;
    public string keterangan;
}