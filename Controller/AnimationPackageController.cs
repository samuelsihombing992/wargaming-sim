using UnityEngine;

public class AnimationPackageController : MonoBehaviour
{
    [Header("Radar")]
    public RuntimeAnimatorController radarAnim;

    [Header("Ship Propeller")]
    public RuntimeAnimatorController shipPropsAnim;

    [Header("Aircraft Propeller / Engine")]
    public RuntimeAnimatorController aircraftPropsAnim;

    [Header("Rotor")]
    public RuntimeAnimatorController rotorTop;
    public RuntimeAnimatorController rotorTail;

    [Header("Wheel")]
    public RuntimeAnimatorController vehicleWheel;

    public static AnimationPackageController Instance;
    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
            Instance.transform.parent = null;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
