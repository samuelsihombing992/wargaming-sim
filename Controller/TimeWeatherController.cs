using System;

using UnityEngine;
using UnityEngine.UI;

using TMPro;
using Expanse;

using Wargaming.Core;
using Wargaming.Components.Playback;
using ProceduralWorlds.HDRPTOD;
using static Wargaming.Core.GlobalParam.WeatherHelper;
using System.Collections.Generic;
using UnityEngine.VFX;

namespace Wargaming.Components.TimeWeather
{
    public class TimeWeatherController : MonoBehaviour
    {
        [Header("Menu Components")]
        public Toggle tDayCycle;
        public Toggle tVolumeClouds;
        public Toggle tClouds;
        public Toggle tOcean;
        public Toggle tWeatherOverride;
        public TMP_Dropdown ddWeather;

        //[Header("UI")]
        //public Toggle toggleDayCycle;
        //public Toggle toggleClouds;
        //public TMP_Dropdown dropdownWeather;

        //public HDRPTimeOfDay TIME_WEATHER;

        [Header("References")]
        [SerializeField] private DateTimeController TIME_WEATHER;
        [SerializeField] private Image imgPreset;

        [Header("Cam Light Helper")]
        public Light camLight;
        public AnimationCurve camlightTransition = AnimationCurve.EaseInOut(0, 0, 23.59f, 0);

        [Header("Clouds & Weather")]
        //public CloudLayerInterpolator cloudInterpolator;
        public CreativeCloudVolume cloudsVolume;
        public CreativeFog fogVolume;
        public HDRPTimeOfDay weatherController;
        public Transform presetList;
        public List<WeatherPreset> weatherData = new List<WeatherPreset>();

        // Expanse components
        //public Expanse.AtmosphereLayer m_fog;
        //public UniversalCloudLayer cloudLayer;
        //public CreativeCloudVolume volumetricClouds;

        public static DateTimeController timeWeather { get; private set; }
        public static bool useDayCycle { get; set; }
        public static bool useVolumetricClouds { get; set; }
        public static bool useClouds { get; set; }
        public static bool useOcean { get; set; }
        public static float oceanAltitude { get; set; }

        #region INSTANCE
        public static TimeWeatherController instance;

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        // Class representing the state of the weather.
        [Serializable]
        public class WeatherState
        {
            public string cloudPreset;     // File path to cloud preset
            public float fogDensity;       // How much fog there is
            public float fogHeight;        // How high the fog extends
        }

        // Random generator for selecting new states
        private System.Random m_rnd = new System.Random();

        private void Start()
        {
            // Set TimeWeather Component to Static
            timeWeather = TIME_WEATHER;

            // PUSH Available Weather to Dropdown
            PrepareWeatherPreset();

            // Add Toggle Day Cycle Listener
            tDayCycle.onValueChanged.AddListener(delegate { OnToggleDayCycle(tDayCycle.isOn); });
            useDayCycle = tDayCycle.isOn;

            // Add Toggle Volumetric Clouds Listener
            tVolumeClouds.onValueChanged.AddListener(delegate { OnToggleVolumeClouds(tVolumeClouds.isOn); });
            useVolumetricClouds = tVolumeClouds.isOn;

            // Add Toggle Clouds Listener
            tClouds.onValueChanged.AddListener(delegate { OnToggleClouds(tClouds.isOn); });
            useClouds = tClouds.isOn;

            // Add Toggle Ocean Listener
            tOcean.onValueChanged.AddListener(delegate { OnToggleOcean(tOcean.isOn); });
            useOcean = tOcean.isOn;
            oceanAltitude = StartupConfig.settings.OCEAN_MAX_ALTITUDE.GetValueOrDefault(0);

            //if (dropdownWeather != null)
            //{
            //    //PrepareWeatherPreset();
            //    //dropdownWeather.onValueChanged.AddListener(delegate { OnWeatherChange(dropdownWeather.value); });
            //}

            prepareCloudPreset();
        }

        #region UPDATE PER-FRAME
        void Update()
        {
            SyncTimeCycle();
            SyncCamLight();
        }

        public void prepareCloudPreset()
        {
            if (presetList == null) return;

            foreach(Transform preset in presetList)
            {
                var presetButton = preset.GetComponent<Button>();
                if(presetButton != null)
                {
                    var presetImage = preset.GetComponent<Image>();
                    var presetData = preset.GetComponent<DataCloudPreset>();

                    presetButton.onClick.AddListener(delegate { ChangeCloudPreset(presetData.preset, presetData.weatherOverride, presetImage.sprite); });
                }
            }
        }

        public void ChangeCloudPreset(UniversalCloudLayer preset, int weatherOverride, Sprite presetImage)
        {
            Debug.Log("Change Cloud Preset to " + preset.name);

            //if (cloudInterpolator.IsInterpolating()) return;
            //volumetricClouds.FromUniversal();

            imgPreset.sprite = presetImage;

            cloudsVolume.FromUniversal(UniversalCloudLayer.loadFromResources(preset));

            cloudsVolume.m_cloudVolume.m_reprojectionFrames = Mathf.Clamp(StartupConfig.settings.CLOUDS_REPROJECTION_QUALITY.GetValueOrDefault(0), 0, 4);
            switch (StartupConfig.settings.CLOUDS_QUALITY.GetValueOrDefault(-99))
            {
                case 0:
                    cloudsVolume.m_quality = Datatypes.Quality.Potato;
                    break;
                case 1:
                    cloudsVolume.m_quality = Datatypes.Quality.Low;
                    break;
                case 2:
                    cloudsVolume.m_quality = Datatypes.Quality.Medium;
                    break;
                case 3:
                    cloudsVolume.m_quality = Datatypes.Quality.High;
                    break;
                case 4:
                    cloudsVolume.m_quality = Datatypes.Quality.Ultra;
                    break;
                case 5:
                    cloudsVolume.m_quality = Datatypes.Quality.RippingThroughTheMetaverse;
                    break;
                default:
                    cloudsVolume.m_quality = Datatypes.Quality.Medium;
                    break;
            }

            cloudsVolume.m_cloudVolume.m_reprojectionFrames = Mathf.Clamp(StartupConfig.settings.CLOUDS_REPROJECTION_QUALITY.GetValueOrDefault(4),1,4);

            //cloudInterpolator.LoadPreset(UniversalCloudLayer.load(url), url);

            if (weatherController.WeatherActive()) weatherController.StopWeather();
            if (weatherOverride >= 0) weatherController.StartWeather(weatherOverride);
        }

        private void ChangeWeather(int weatherOverride)
        {
            if (weatherController.WeatherActive()) weatherController.StopWeather();
            if (weatherOverride <= 0)
            {
                if (tWeatherOverride.isOn)
                {
                    cloudsVolume.FromUniversal
                        //(UniversalCloudLayer.loadFromResources("Assets/Expanse/blocks/presets/procedural cloud volume/Stratus/Minerva Optimized.asset")
                        //(UniversalCloudLayer.loadFromResources("Profile/Clouds/Stratus/Minerva Optimized.asset")
                        (UniversalCloudLayer.loadFromResources(AssetPackageController.Instance.defaultCloudPreset)
                     );

                    fogVolume.m_density         = 5;
                    fogVolume.m_radius          = 50000;
                    fogVolume.m_thickness       = 800;
                }

                return;
            }

            weatherOverride -= 1;
            var selectedWeather = weatherData[weatherOverride];

            var weatherParticles = selectedWeather.profile.WeatherFXData.m_weatherEffect;
            if (weatherParticles != null && selectedWeather.overrideWeatherParticle)
            {
                try
                {
                    weatherParticles.GetComponent<VisualEffect>().SetFloat("ParticleAmount", selectedWeather.particleCount);
                }catch(Exception e) { }
            }

            //GetComponent<VisualEffect>().SetFloat("ParticleAmount", 1000);
            weatherController.StartWeather(weatherOverride);

            // Override Cloud Preset If Enabled
            if (selectedWeather.overrideCloudPreset && tWeatherOverride.isOn)
            {
                cloudsVolume.FromUniversal
                    (UniversalCloudLayer.loadFromResources(selectedWeather.cloudPreset)
                 );
            }

            // Override Cloud Density if Enabled
            if (selectedWeather.overrideCloudDensity && tWeatherOverride.isOn)
            {
                cloudsVolume.m_coverage = weatherData[weatherOverride].cloudDensity;
            }

            // Override Fog Settings if Enabled
            if (selectedWeather.overrideFog && tWeatherOverride.isOn)
            {
                fogVolume.m_density     = weatherData[weatherOverride].fogDensity;
                fogVolume.m_radius      = weatherData[weatherOverride].fogRadius;
                fogVolume.m_thickness   = weatherData[weatherOverride].fogThickness;
            }
        }

        private static void SyncTimeCycle()
        {
            if (!useDayCycle) return;
            if (PlaybackController.CURRENT_TIME == null) return;

            timeWeather.m_timeLocal.SetFromDateTime(PlaybackController.CURRENT_TIME);
        }

        private void SyncCamLight()
        {
            var sunLight = TIME_WEATHER.m_sun.transform.parent.GetComponentInChildren<Light>();
            camLight.color = sunLight.color;

            var intensity = TIME_WEATHER.m_timeLocal.hour + (TIME_WEATHER.m_timeLocal.minute / 60);
            camLight.intensity = camlightTransition.Evaluate(intensity);
        }
        #endregion

        private void PrepareWeatherPreset()
        {
            ddWeather.ClearOptions();

            var weatherOptions = new List<string>();
            weatherOptions.Add("Clear Sky");

            for (int i = 0; i < weatherData.Count; i++)
            {
                weatherOptions.Add(weatherData[i].name);
                weatherController.WeatherProfiles.Add(weatherData[i].profile);
            }

            ddWeather.AddOptions(weatherOptions);
            ddWeather.onValueChanged.AddListener(delegate { ChangeWeather(ddWeather.value); });
        }

        /// <summary>
        /// Set Toggle Pergantian Waktu (Day Cycle)
        /// </summary>
        /// <param name="isActive">Toggle State</param>
        private void OnToggleDayCycle(bool isActive)
        {
            useDayCycle = isActive;

            if (isActive)
            {
                timeWeather.m_timeLocal.SetFromDateTime(PlaybackController.CURRENT_TIME);
            }
            else
            {
                timeWeather.m_timeLocal.SetFromDateTime(DateTime.Today.AddHours(12));
            }
        }

        public static void SetTimeCycle(DateTime time)
        {
            timeWeather.m_timeLocal.SetFromDateTime(time);
        }

        /// <summary>
        /// Set Toggle Volumetric Clouds (Heavy Performance Impact)
        /// </summary>
        /// <param name="isActive">Toggle State</param>
        private void OnToggleVolumeClouds(bool isActive)
        {
            useVolumetricClouds = isActive;

            var clouds = EnvironmentsController.GetCloudsGroup().GetComponent<Metadata>();
            clouds.FindParameter("volumetric").SetActive(isActive);
        }

        /// <summary>
        /// Set Toggle All Clouds
        /// </summary>
        /// <param name="isActive">Toggle State</param>
        private void OnToggleClouds(bool isActive)
        {
            useClouds = isActive;

            EnvironmentsController.GetCloudsGroup().SetActive(isActive);
            tVolumeClouds.isOn = isActive;
        }

        /// <summary>
        /// Set Toggle Ocean
        /// </summary>
        /// <param name="isActive">Toggle State</param>
        private void OnToggleOcean(bool isActive)
        {
            useOcean = isActive;

            EnvironmentsController.GetOcean().enabled = isActive;
            tOcean.isOn = isActive;

            if (!isActive)
            {
                StartupConfig.settings.OCEAN_MAX_ALTITUDE = 0;
            }
            else
            {

            }
            {
                StartupConfig.settings.OCEAN_MAX_ALTITUDE = oceanAltitude;
            }
        }
    }
}