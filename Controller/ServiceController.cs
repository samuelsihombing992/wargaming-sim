using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using TMPro;

using Wargaming.Core.GlobalParam;

public class ServiceController : MonoBehaviour
{
    [Header("SETTINGS")]
    public string serviceFileLocation;

    [Header("REFERENCES")]
    [SerializeField] private GameObject prefabList;
    [SerializeField] private Transform serviceList;

    [Header("FORM DATA")]
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI btnLabel;
    [SerializeField] private TMP_InputField serverName;
    [SerializeField] private TMP_InputField serviceLogin;
    [SerializeField] private TMP_InputField serviceCB;
    [SerializeField] private TMP_InputField serverColyeus;

    [Header("EVENTS")]
    public UnityEvent OnFormSubmitSuccess;

    private int? _SELECTED;
    private List<ServiceData> _SERVICES;

    #region INSTANCE
    public static ServiceController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            _SERVICES = new List<ServiceData>();
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        loadServicesFromFile();
    }

    private void loadServicesFromFile()
    {
        List<string> SERVICES_NAME = new List<string>();

        // If Services File is not defined, throw error message
        if (serviceFileLocation == null)
        {
            Debug.LogError("Error, Services Path is not defined. Please provide one!");
            return;
        }

        var services_path = Path.Combine(Directory.GetCurrentDirectory(), serviceFileLocation);
        if (File.Exists(services_path))
        {
            // Use Current Service Document if exists
            var _XMLSerialize = new XmlSerializer(typeof(List<ServiceData>));
            using (var reader = new StreamReader(services_path))
            {
                _SERVICES = (List<ServiceData>)_XMLSerialize.Deserialize(reader);
            }

            // Add List Service Into ListView
            for (int i = 0; i < _SERVICES.Count; i++)
            {
                SERVICES_NAME.Add(_SERVICES[i].ServiceName);
                AddServiceIntoList(i, _SERVICES[i].ServiceName, _SERVICES[i].ServiceLogin, _SERVICES[i].ServiceCB, _SERVICES[i].ServerColyseus, _SERVICES[i].Active);
            }

        }
        else
        {
            // Create new Service Document if unavailable
            AddNewService("Local Server", "http://localhost/eppkm/public", "http://localhost/eppkm_simulasi", "ws://localhost:2567/colyseus", true);
            SERVICES_NAME.Add("Local Server");
        }

        RefreshLoginServerList(SERVICES_NAME);

        // Add Active Service Into Global Config
        ServerConfigHelper config = new ServerConfigHelper()
        {
            server_name = _SERVICES[_SELECTED.Value].ServiceName,
            service_login = _SERVICES[_SELECTED.Value].ServiceLogin,
            service_cb = _SERVICES[_SELECTED.Value].ServiceCB,
            server_colyseus = _SERVICES[_SELECTED.Value].ServerColyseus
        };
        new ServerConfig(config);
    }

    #region UI INTERRACTION
    public void SubmitChanges()
    {
        if (serverName.text != "" && serviceLogin.text != "" && serviceCB.text != "" && serverColyeus.text != "")
        {
            if (_SELECTED == null)
            {
                AddNewService();
            }
            else
            {
                EditService(_SELECTED.Value);
            }

            OnFormSubmitSuccess.Invoke();
        }
    }

    public void EditSelectedService()
    {
        if (_SELECTED == null) return;
        ResetForm();

        serverName.text = _SERVICES[_SELECTED.Value].ServiceName;
        serviceLogin.text = _SERVICES[_SELECTED.Value].ServiceLogin;
        serviceCB.text = _SERVICES[_SELECTED.Value].ServiceCB;
        serverColyeus.text = _SERVICES[_SELECTED.Value].ServerColyseus;
    }

    public void RemoveSelectedService()
    {
        if (_SELECTED == null) return;
        if (_SERVICES.Count == 1) return;

        _SERVICES.RemoveAt(_SELECTED.Value);

        Destroy(serviceList.GetChild(_SELECTED.Value).gameObject);

        ResetSelected();
        RefreshLoginServerList();
        SaveChanged();
    }

    public void SelectService(int index)
    {
        _SELECTED = index;

        for (int i = 0; i < _SERVICES.Count; i++)
        {
            if (_SERVICES[i].Active && index != i)
            {
                _SERVICES[i].Active = false;
            }

            if (i == index)
            {
                _SERVICES[i].Active = true;
            }
        }

        SaveChanged();
    }

    public void ResetForm()
    {
        serverName.text = "";
        serviceLogin.text = "";
        serviceCB.text = "";
        serverColyeus.text = "";
    }

    public void ResetSelected()
    {
        _SELECTED = null;
    }
    #endregion

    #region LIST PREFAB MODIFICATION
    public void AddServiceIntoList(int index, string name, string login, string cb, string colyseus, bool active)
    {
        var item_list = Instantiate(prefabList, serviceList).GetComponent<Metadata>();

        try
        {
            ChangeListMetadata(index, name, login, cb, colyseus, item_list);

            var item_toggle = item_list.GetComponent<Toggle>();
            item_toggle.onValueChanged.AddListener(delegate { SelectService(index); });

            if (active)
            {
                _SELECTED = index;
            };

            if (_SELECTED != null)
            {
                serviceList.GetChild(_SELECTED.Value).GetComponent<Toggle>().isOn = true;
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e.Message);
            Destroy(item_list.gameObject);
        }
    }

    public void ChangeListMetadata(int index, string name, string login, string cb, string colyseus, Metadata item_list = null)
    {
        if (item_list == null) item_list = serviceList.GetChild(index).GetComponent<Metadata>();

        item_list.FindParameter("index").GetComponent<TextMeshProUGUI>().text = index.ToString();
        item_list.FindParameter("server-name").GetComponent<TextMeshProUGUI>().text = name;
        item_list.FindParameter("service-login").GetComponent<TextMeshProUGUI>().text = login;
        item_list.FindParameter("service-cb").GetComponent<TextMeshProUGUI>().text = cb;
        item_list.FindParameter("server-colyseus").GetComponent<TextMeshProUGUI>().text = colyseus;
    }
    #endregion

    #region SERVICE DOCUMENT MODIFICATION
    private void AddNewService(string server = null, string login = null, string cb = null, string colyseus = null, bool active = false)
    {
        // Check If All Required Input is Filled
        if (server == null) if (serverName.text == "") return;
        if (login == null) if (serviceLogin.text == "") return;
        if (cb == null) if (serviceCB.text == "") return;
        if (colyseus == null) if (serverColyeus.text == "") return;

        server = (server != null) ? server : serverName.text;
        login = (login != null) ? login : serviceLogin.text;
        cb = (cb != null) ? cb : serviceCB.text;
        colyseus = (colyseus != null) ? colyseus : serverColyeus.text;

        // Create new Service Data
        _SERVICES.Add(
            new ServiceData
            {
                ServiceName = server,
                ServiceLogin = new[] { "http://", "https://" }.Any(c => login.Contains(c)) ? login : "http://" + login,
                ServiceCB = new[] { "http://", "https://" }.Any(c => cb.Contains(c)) ? cb : "http://" + cb,
                ServerColyseus = new[] { "http://", "https://" }.Any(c => colyseus.Contains(c)) ? colyseus : "http://" + colyseus,
                Active = active
            }
        );

        AddServiceIntoList(_SERVICES.Count - 1, server, login, cb, colyseus, active);

        RefreshLoginServerList();
        SaveChanged();
    }

    private void EditService(int index)
    {
        _SERVICES[index] = new ServiceData
        {
            ServiceName = serverName.text,
            ServiceLogin = new[] { "http://", "https://" }.Any(c => serviceLogin.text.Contains(c)) ? serviceLogin.text : "http://" + serviceLogin.text,
            ServiceCB = new[] { "http://", "https://" }.Any(c => serviceLogin.text.Contains(c)) ? serviceCB.text : "http://" + serviceCB.text,
            ServerColyseus = new[] { "http://", "https://" }.Any(c => serviceLogin.text.Contains(c)) ? serverColyeus.text : "http://" + serverColyeus.text,
            Active = false
        };

        ChangeListMetadata(index, _SERVICES[index].ServiceName, _SERVICES[index].ServiceLogin, _SERVICES[index].ServiceCB, _SERVICES[index].ServerColyseus);

        RefreshLoginServerList();
        SaveChanged();
    }

    private void SaveChanged()
    {
        // Serialize and Write Changed into Service Document
        var _XMLSerialize = new XmlSerializer(typeof(List<ServiceData>));
        using (var writer = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), serviceFileLocation)))
        {
            _XMLSerialize.Serialize(writer, _SERVICES);
        }
    }

    public void SelectServer()
    {
       // _SELECTED = LoginController.instance.dropdownServer.value;

        ServerConfig.SERVICE_LOGIN = _SERVICES[_SELECTED.Value].ServiceLogin;
        ServerConfig.SERVICE_CB = _SERVICES[_SELECTED.Value].ServiceCB;
        ServerConfig.SERVER_COLYSEUS = _SERVICES[_SELECTED.Value].ServerColyseus;
    }

    private void RefreshLoginServerList(List<string> LIST = null)
    {
        List<string> SERVICES_NAME = new List<string>();
        if (LIST == null)
        {
            for (int i = 0; i < _SERVICES.Count; i++)
            {
                SERVICES_NAME.Add(_SERVICES[i].ServiceName);
            }
        }
        else
        {
            SERVICES_NAME = LIST;
        }

        //LoginController.instance.dropdownServer.ClearOptions();
        //LoginController.instance.dropdownServer.AddOptions(SERVICES_NAME);
        //LoginController.instance.dropdownServer.value = _SELECTED.Value;
    }
    #endregion
}