using Esri.ArcGISMapsSDK.Components;
using System.Xml.Serialization;

public class LayerData
{
    [XmlAttribute("attrid")]
    public string layerId { get; set; }
    [XmlAttribute("name")]
    public string layerName { get; set; }
    [XmlElement("source")]
    public string layerSource { get; set; }
    [XmlElement("file")]
    public bool file { get; set; }
    [XmlElement("type")]
    public LayerTypes layerType { get; set; }

    [XmlElement("index")]
    public int index { get; set; }
    [XmlElement("active")]
    public bool active { get; set; }
}
