using System;
using System.Linq;
using System.IO;
using System.Xml.Serialization;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using TMPro;
using Esri.ArcGISMapsSDK.Components;

using Wargaming.Core;
using Wargaming.Core.GlobalParam;

public class LayerManagerController : MonoBehaviour
{
    [Header("Config File")]
    public string layerFile;

    [Header("Form Data")]
    [SerializeField] private TMP_InputField layerName;
    [SerializeField] private TMP_InputField source;
    [SerializeField] private TMP_Dropdown sourceType;
    [SerializeField] private TMP_Dropdown layerType;
    [SerializeField] private TMP_Dropdown status;

    [SerializeField] private Button submitButton;
    [SerializeField] private Button fileBrowseButton;
    [SerializeField] private Button deleteButton;

    [Header("Manager References")]
    [SerializeField] private Transform listManagerContainer;
    [SerializeField] private GameObject prefabListManager;

    [Header("Events")]
    public UnityEvent OnCreate;
    public UnityEvent OnEdit;
    public UnityEvent AfterSubmit;

    public int? selected = null;

    private void Start()
    {
        // Load LAYER Data from "File"
        if (GlobalMapService.LAYER_DATA == null)
        {
            //new Layers();
            GlobalMapService.LAYER_DATA = new List<LayerData>();
        }

        LoadLayerFromFile();

        gameObject.SetActive(false);
    }

    #region REQUEST
    private void LoadLayerFromFile()
    {
        // If Layer File is not defined, throw error message
        if (layerFile == null)
        {
            Debug.LogError("Error, Layer Service File is not defined. Please provide one!");
            return;
        }

        var path = Path.Combine(Directory.GetCurrentDirectory(), layerFile);
        if (File.Exists(path))
        {
            // Use Current Connection Document if exists
            var _XMLSerialize = new XmlSerializer(typeof(List<LayerData>));
            using (var reader = new StreamReader(path))
            {
                GlobalMapService.LAYER_DATA = (List<LayerData>)_XMLSerialize.Deserialize(reader);
            }
        }
        else
        {
            // Creating New Sample
            AddLayerToList("New York Pop Density", "https://tiles.arcgis.com/tiles/4yjifSiIG17X0gW4/arcgis/rest/services/NewYorkCity_PopDensity/MapServer", LayerTypes.ArcGISImageLayer, 0);
        }

        // Add List Content Into ListView
        RefreshLayerManagerList();
    }
    #endregion

    #region SETTINGS_LAYER_MANAGER
    public void ResetLayerManagerForm()
    {
        selected = null;

        layerName.reset();
        source.reset();
        sourceType.reset();
        layerType.reset();
        status.reset();
    }

    public void OnCreateNewLayer()
    {
        ResetLayerManagerForm();

        fileBrowseButton.GetComponentInChildren<TextMeshProUGUI>().text = "Browse File...";

        OnCreate.Invoke();
    }

    private void OnEditCurrentLayer(int? _index)
    {
        ResetLayerManagerForm();

        if (!_index.HasValue) return;

        selected = _index.Value;
        var layer = GlobalMapService.LAYER_DATA[selected.Value];

        layerName.text      = layer.layerName;
        source.text         = layer.layerSource;
        layerType.value     = GetLayerIndexByType(layer.layerType);
        status.value        = (layer.active) ? 0 : 1;

        if (!layer.file)
        {
            sourceType.value = 0;
        }

        fileBrowseButton.GetComponentInChildren<TextMeshProUGUI>().text = "Browse File...";

        OnEdit.Invoke();
    }

    public void OnSubmit()
    {
        if (layerName.text == "" || source.text == "") return;

        var layer = (selected.HasValue) ? GlobalMapService.LAYER_DATA[selected.Value] : new LayerData();

        layer.layerId       = (selected.HasValue) ? layer.layerId : Guid.NewGuid().ToString();
        layer.layerName     = layerName.text;
        layer.layerSource   = source.text;
        layer.file          = (sourceType.value == 0) ? false : true;
        layer.layerType     = GetLayerTypeByValue(layerType.value);
        layer.active        = (status.value == 0) ? true : false;

        if (!selected.HasValue)
        {
            GlobalMapService.LAYER_DATA.Add(layer);
        }

        SaveChanged();
        RefreshLayerManagerList();

        AfterSubmit.Invoke();
    }
    
    public void DeleteLayer()
    {
        if (!selected.HasValue) return;

        GlobalMapService.LAYER_DATA.RemoveAt(selected.Value);

        for(int i= selected.Value; i < GlobalMapService.LAYER_DATA.Count; i++)
        {
            GlobalMapService.LAYER_DATA[i].index -= 1;
        }

        SaveChanged();
        RefreshLayerManagerList();

        AfterSubmit.Invoke();
    }

    public void OnSourceTypeChange()
    {
        source.reset();

        if(sourceType.value == 0)
        {
            source.transform.parent.gameObject.SetActive(true);
            fileBrowseButton.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            source.transform.parent.gameObject.SetActive(false);
            fileBrowseButton.transform.parent.gameObject.SetActive(true);
        }
    }

    public void BrowseLayerFile()
    {
        //string path = EditorUtility.OpenFilePanel("Load a .tpk/.tpkx file", "", "tpk,tpkx");
        //if (path.Length != 0)
        //{
        //    try
        //    {
        //        if (Path.GetExtension(path).Equals(".tpk", StringComparison.OrdinalIgnoreCase) || Path.GetExtension(path).Equals(".tpkx", System.StringComparison.OrdinalIgnoreCase))
        //        {
        //            fileBrowseButton.GetComponentInChildren<TextMeshProUGUI>().text = Path.GetFileName(path);
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        Debug.LogError("Failed to load file: " + ex.Message);
        //    }
        //}

        //source.text = path;
    }

    private void AddLayerToList(string _layerName, string _source, LayerTypes _type, int _index, bool _file = false, bool _active = false)
    {
        if (GlobalMapService.LAYER_DATA == null) return;

        GlobalMapService.LAYER_DATA.Add(new LayerData
        {
           layerId      = Guid.NewGuid().ToString(),
           layerName    = _layerName,
           layerSource  = _source,
           layerType    = _type,
           index        = _index,
           file         = _file,
           active       = _active
        });

        SaveChanged();
        RefreshLayerManagerList();
    }

    private void RefreshLayerManagerList()
    {
        if (listManagerContainer == null) return;

        listManagerContainer.DestroyAllChild();

        var settings = GameSettingsController.instance;
        if (settings != null)
        {
            settings.layerContainer.DestroyAllChild();
        }

        GlobalMapService.LAYER_DATA = GlobalMapService.LAYER_DATA.OrderBy(o => o.index).ToList();
        for (int i = 0; i < GlobalMapService.LAYER_DATA.Count; i++)
        {
            int index = i;
            AddLayerToLayerManager(index, GlobalMapService.LAYER_DATA[i]);

            // List Layer on "GameSettingsController"
            if (settings != null)
            {
                AddLayerToLayerManager(index, GlobalMapService.LAYER_DATA[i], settings.prefabListLayer, settings.layerContainer);
            }
        }
    }

    private void AddLayerToLayerManager(int _index, LayerData layer, GameObject prefabList = null, Transform listContainer = null)
    {
        var list = Instantiate((prefabList == null) ? prefabListManager : prefabList, (listContainer == null) ? listManagerContainer : listContainer);
        var metadata    = list.GetMetadata();

        if (metadata == null) return;

        list.name = "LAYER_SERVICE_" + _index;  

        if(_index <= 0)
        {
            metadata.GetMetadataButton("btn-up").gameObject.SetActive(false);
        }

        if(_index >= GlobalMapService.LAYER_DATA.Count - 1)
        {
            metadata.GetMetadataButton("btn-down").gameObject.SetActive(false);
        }

        var listToggle = list.GetComponent<Toggle>();
        if (listToggle != null)
        {
            listToggle.group = list.GetComponentInParent<ToggleGroup>();
        }

        var listMenuToggler = list.GetComponent<MenuToggler>();
        if (listMenuToggler != null)
        {
            listMenuToggler.onToggleOn.AddListener(delegate { OnEditCurrentLayer(_index); });
            listMenuToggler.onToggleOff.AddListener(delegate { AfterSubmit.Invoke(); });
        }

        var listToggleActive = metadata.GetMetadataToggle("toggle");
        if(listToggleActive != null)
        {
            listToggleActive.isOn = layer.active;
            listToggleActive.onValueChanged.AddListener(delegate { ToggleLayer(_index, true); });
        }

        var listLabel = metadata.GetMetadataLabel("name");
        if (listLabel != null)
        {
            listLabel.text = layer.layerName;
        }

        var listButtonUp = metadata.GetMetadataButton("btn-up");
        if(listButtonUp != null)
        {
            listButtonUp.onClick.AddListener(delegate { MoveLayerUp(_index, true); RefreshLayerManagerList(); });
        }

        var listButtonDown = metadata.GetMetadataButton("btn-down");
        if(listButtonDown != null)
        {
            listButtonDown.onClick.AddListener(delegate { MoveLayerDown(_index, true); RefreshLayerManagerList(); });
        }
    }

    private LayerTypes GetLayerTypeByValue(int index)
    {
        switch (index)
        {
            case 0:
                return LayerTypes.ArcGISImageLayer;
            case 1:
                return LayerTypes.ArcGIS3DObjectSceneLayer;
            case 2:
                return LayerTypes.ArcGISIntegratedMeshLayer;
            case 3:
                return LayerTypes.ArcGISVectorTileLayer;
            default:
                return LayerTypes.ArcGISImageLayer;
        }
    }

    private int GetLayerIndexByType(LayerTypes type)
    {
        switch (type)
        {
            case LayerTypes.ArcGISImageLayer:
                return 0;
            case LayerTypes.ArcGIS3DObjectSceneLayer:
                return 1;
            case LayerTypes.ArcGISIntegratedMeshLayer:
                return 2;
            case LayerTypes.ArcGISVectorTileLayer:
                return 3;
            default:
                return 0;
        }
    }

    private void SaveChanged()
    {
        // Serialize and Write Changed into Layer Document
        var _XMLSerialize = new XmlSerializer(typeof(List<LayerData>));
        using (var writer = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), layerFile)))
        {
            _XMLSerialize.Serialize(writer, GlobalMapService.LAYER_DATA);
        }
    }
    #endregion

    #region ACTION
    private void ToggleLayer(int _index, bool saveChange = false)
    {
        GlobalMapService.LAYER_DATA[_index].active = !GlobalMapService.LAYER_DATA[_index].active;

        if (saveChange) { SaveChanged(); }
    }

    private void MoveLayerUp(int _index, bool saveChange = false)
    {
        GlobalMapService.LAYER_DATA[_index].index -= 1;
        GlobalMapService.LAYER_DATA[_index - 1].index += 1;

        if (saveChange) { SaveChanged(); }
    }

    private void MoveLayerDown(int _index, bool saveChange = false)
    {
        GlobalMapService.LAYER_DATA[_index].index += 1;
        GlobalMapService.LAYER_DATA[_index + 1].index -= 1;

        if (saveChange) { SaveChanged(); }
    }

    public static void SetUseLayerService(bool state)
    {
        GlobalMapService.useLayerService = state;
    }
    #endregion
}
