using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

using Wargaming.Core.GlobalParam;

public class SceneController : MonoBehaviour
{
    [Header("SCENE DATA")]
    public string loginScene;
    public string tfgGameplayScene;
    public string wgsGameplayScene;
    public string editorScene;
    public string assetScene;

    public static GameObject activeLoading;

    [Header("Konfigurasi")]
    public LoadingEffect loader;

    #region INSTANCE
    public static SceneController Instance;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(Instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public async Task<bool> LoadScene(string sceneName, string loaderName = null, string loadingAnimationStartAnim = null)
    {
        activeLoading = Instantiate(loader.gameObject, gameObject.transform);

        activeLoading.GetComponent<Animator>().Play(loadingAnimationStartAnim == null ? "LoadingScreenStart" : loadingAnimationStartAnim);                  // Play Start Animation

        await Task.Delay(2000);
        if (sceneName == tfgGameplayScene || sceneName == wgsGameplayScene || sceneName == editorScene || sceneName == assetScene)
        {
            activeLoading.GetComponent<Metadata>().FindParameter("operation").GetComponent<TextMeshProUGUI>().text = "Operation: " + SkenarioAktif.NAMA_SKENARIO;

            if (StartupConfig.settings.ENABLE_ASSET_PACKAGE)
            {
                await AssetPackageController.Instance.loadPackageAlutsista();
                await AssetPackageController.Instance.InitPasukanVariant();
            }

            if (StartupConfig.settings.ENABLE_DATAPACK)
            {
                await DatapackPackageController.Instance.loadDatapack();
                await DatapackPackageController.Instance.loadDatapackTemplate();
            }
        }

        var scene = SceneManager.LoadSceneAsync(sceneName);                                                                                                 // Load The Scene
        scene.allowSceneActivation = false;                                                                                                                 // Disable Scene Activation When It's Not Loaded Yet

        // Progress Bar Respond
        do
        {
            await Task.Delay(100);
            //_loaderUI[selectedIndex].transform.FindChild("ProgressBar");
        } while (scene.progress < 0.9f);

        await Task.Delay(1000);
        scene.allowSceneActivation = true;

        return true;
    }

    public async void DestroyLoading(string loadingAnimationEndAnim = null)
    {
        if (activeLoading == null) return;

        await Task.Delay(2000);
        activeLoading.GetComponent<Animator>().Play(loadingAnimationEndAnim == null ? "LoadingScreenEnd" : loadingAnimationEndAnim);                        // Play End Animation

        await Task.Delay(2000);
        Destroy(activeLoading);                                                                                                                           // Destroy UI Loading Setelah Selesai (wait for 2 seconds first)
    }

    public async Task ExecuteLoadingToScene(string nextScene, string returnScene = null, bool DestroyLoadingOnFinish = false)
    {
        LoadingController.instance.ShowLoading("loading-blur", null);
        await Task.Delay(2000);

        if (returnScene != null) SceneLoad.returnTo = returnScene;

        if (DestroyLoadingOnFinish)
        {
            if (await Instance.LoadScene(nextScene, null, "FadeIn"))
            {
                DestroyLoading("FadeOut");
            };

            await Task.Delay(250);
            LoadingController.instance.HideLoading();
        }
    }
}
