using UnityEngine;

using Esri.ArcGISMapsSDK.Components;

using Wargaming.Components.Playback;
using Wargaming.Core;
using static Wargaming.Core.GlobalParam.EntityHelper;
using System.Threading.Tasks;
using WGSBundling.Core;

public class FollowCamController : MonoBehaviour
{
    public Transform target;

    public enum UpdateSettings { UPDATE, FIXED_UPDATE, LATE_UPDATE }
    [Header("Update Settings")]
    public UpdateSettings updateSettings = UpdateSettings.LATE_UPDATE;

    [Header("Settings")]
    public Vector2 shipRotLimit;
    public Vector2 aircraftRotLimit;
    public Vector2 vehicleRotLimit;
    public Vector2 zoomLimit;
    public float zoomSensitive;

    [Header("Cam Offset")]
    public float offset = 2;

    [Header("Jiggle Effects")]
    public bool camJiggle = false;
    public float jiggleOffset = 1f;

    // [System.NonSerialized] public ObjekSatuan _target;

    [SerializeField] private Vector3 lastPosition;
    private Vector3 direction;

    [SerializeField] private Vector3 movement;
    private Vector3 rotation;
    private Vector2 rotLimit;
    public AnimationCurve vMovement;
    public AnimationCurve hMovement;
    public Vector3 jigglePos;
    public float timing;

    public GameObject camNode;

    public void init()
    {
        transform.position = target.position + (target.forward * 10);

        camNode = transform.GetChild(0).gameObject;
        //_target = transform.parent.GetComponent<ObjekSatuan>();

        if(target.GetComponent<WGSBundleCore>().bundleType == WGSBundleCore.BundleType.AIR)
        {
            camJiggle = true;
        }

        //if (_target == null)
        //{
        //    Debug.LogError("Unfollow object because followed target is null");
        //    Camera.main.GetComponent<WargamingCam>().UnfollowAny();
        //}

        movement.z = 10;

        direction = new Vector3(0, 0, (target.transform.position - transform.position).magnitude);
        lastPosition = Input.mousePosition;

        switch (target.GetComponent<ObjekSatuan>().kategori)
        {
            case KategoriSatuan.VEHICLE:
                rotLimit = new Vector2(vehicleRotLimit.x * -1, vehicleRotLimit.y * -1);
                break;
            case KategoriSatuan.SHIP:
                rotLimit = new Vector2(shipRotLimit.x * -1, shipRotLimit.y * -1);
                break;
            case KategoriSatuan.AIRCRAFT:
                rotLimit = new Vector2(aircraftRotLimit.x * -1, aircraftRotLimit.y * -1);
                break;
            case KategoriSatuan.INFANTRY:
                rotLimit = new Vector2(vehicleRotLimit.x * -1, vehicleRotLimit.y * -1);
                break;
            default:
                rotLimit = new Vector2(vehicleRotLimit.x * -1, vehicleRotLimit.y * -1);
                break;
        }

        vMovement = new AnimationCurve(
            new Keyframe(0, 0),
            new Keyframe(5, -jiggleOffset),
            new Keyframe(15, jiggleOffset),
            new Keyframe(20, 0)
        );

        hMovement = new AnimationCurve(
            new Keyframe(0, 0),
            new Keyframe(5, jiggleOffset),
            new Keyframe(10, 0),
            new Keyframe(15, -jiggleOffset),
            new Keyframe(20, 0)
        );

        //WargamingCam.instance.enabled = false;
        //await Task.Delay(100);
        //WargamingCam.instance.enabled = true;
    }

    void Update()
    {
        transform.position = WargamingCam.instance.transform.position;

        if (updateSettings != UpdateSettings.UPDATE) return;
        Refresh();
    }

    private void FixedUpdate()
    {
        if (updateSettings != UpdateSettings.FIXED_UPDATE) return;
        Refresh();
    }

    private void LateUpdate()
    {
        if (updateSettings != UpdateSettings.LATE_UPDATE) return;
        Refresh();
    }

    private void Refresh()
    {
        if (WargamingCam.instance.onRebase) return;

        Vector3 mouseDelta = Input.mousePosition - lastPosition;

        if (!UICanvasSettings.IsRaycastingWithUI())
        {
            if (Input.GetMouseButton(0))
                movement += new Vector3(mouseDelta.x * 0.1f, mouseDelta.y * 0.05f, 0F);
                movement.z += Input.GetAxis("Mouse ScrollWheel") * -2.5F * zoomSensitive;
        }

        rotation += movement;
        rotation.x = rotation.x % 360.0f;
        CamRotation();

        //direction.z = Mathf.Clamp(direction.z - movement.z * 50F, 15F, 180F);
        direction.z = Mathf.Clamp(movement.z + direction.z, zoomLimit.x, zoomLimit.y);
        camNode.transform.position = target.transform.position + Quaternion.Euler(180F - rotation.y, rotation.x, 0) * direction;
        camNode.transform.LookAt(target.transform.position);

        camNode.transform.position = new Vector3(camNode.transform.position.x, camNode.transform.position.y + offset, camNode.transform.position.z);

        lastPosition = Input.mousePosition;
        movement *= 0.5F;

        zoomLimit = new Vector2(5, zoomLimit.y);

        if(target.GetComponentInParent<ObjekSatuan>().kategori == KategoriSatuan.AIRCRAFT)
        {
            CamJiggleHandler();
        }
    }

    private void CamRotation()
    {
        rotation.y = Mathf.Clamp(rotation.y, rotLimit.x, rotLimit.y);
        rotation.z = 0;
    }

    public void CamJiggleHandler()
    {
        if (!camJiggle) return;

        float camAlt = Mathf.Clamp((float) (WargamingCam.instance.GetComponent<ArcGISLocationComponent>().Position.Z / 100), 0, 1);

        timing += Time.deltaTime;
        if (timing > 20)
        {
            timing = 0;
        }

        jigglePos = new Vector3(vMovement.Evaluate(timing * camAlt), hMovement.Evaluate(timing * camAlt), -2);
        camNode.transform.localPosition = camNode.transform.localPosition + jigglePos;
    }
}
