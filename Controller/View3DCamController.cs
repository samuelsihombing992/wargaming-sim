using UnityEngine;
using UnityEngine.UI;

using Wargaming.Core;

public class View3DCamController : MonoBehaviour
{
    [Header("Layer Parent")]
    public GameObject objToggle3D;
    public ScrollRect raycasterScrollview;
    
    [Header("Settings")]
    public Vector2 zoomLimit;
    public float zoomSensitive = 1;

    public Vector2 rotLimit;

    public GameObject _target;

    private Vector2 camNodePos;
    private Vector3 lastPosition;
    private Vector3 direction;

    private Vector3 movement;
    private Vector3 rotation;

    private GameObject camNode;
    private float raycasterScrollviewSensitivity;

    public void init(GameObject target, GameObject _objToggle3D, ScrollRect _raycasterScrollview)
    {
        objToggle3D = _objToggle3D;
        raycasterScrollview = _raycasterScrollview;
        _target = target;

        camNode = GetComponentInChildren<Camera>().gameObject;
        camNodePos = new Vector2(100, 200);

        direction = new Vector3(0, 0, (_target.transform.position - transform.position).magnitude);
        lastPosition = Input.mousePosition;

        if(raycasterScrollview != null)
        {
            raycasterScrollviewSensitivity = raycasterScrollview.scrollSensitivity;
        }

        OnCamUpdate();
    }

    private void Update()
    {
        if (_target == null) return;

        var raycasting = UICanvasSettings.GetGraphicRaycastData();
        bool isRaycasting = false;

        if (raycasting.Count <= 0) return;
        for (int i = 0; i < raycasting.Count; i++)
        {
            if (raycasting[i].gameObject == objToggle3D)
            {
                isRaycasting = true;
                i = raycasting.Count;
            }
        }

        if (!isRaycasting) return;

        OnCamUpdate();
    }

    private void OnCamUpdate()
    {
        toggleRaycasterScrollview(false);

        Vector3 mouseDelta = Input.mousePosition - lastPosition;
        if (Input.GetMouseButton(0))
            movement += new Vector3(mouseDelta.x * 0.5f, mouseDelta.y * 0.25f, 0F);

        if (camNode.GetComponent<Camera>().orthographic)
        {
            var camNodeZoom = camNode.GetComponent<Camera>().orthographicSize;
            camNode.GetComponent<Camera>().orthographicSize = Mathf.Clamp((camNodeZoom += Input.GetAxis("Mouse ScrollWheel") * -2.5F * zoomSensitive), zoomLimit.x, zoomLimit.y);
        }
        else
        {
            movement.z += Input.GetAxis("Mouse ScrollWheel") * -2.5F * zoomSensitive;
        }

        rotation += movement;
        rotation.x = rotation.x % 360.0f;
        CamRotation();

        direction.z = Mathf.Clamp(movement.z + direction.z, camNodePos.x, camNodePos.y);
        camNode.transform.position = _target.transform.position + Quaternion.Euler(180F - rotation.y, rotation.x, 0) * direction;
        camNode.transform.LookAt(_target.transform.position);

        lastPosition = Input.mousePosition;
        movement *= 0.5F;

        camNodePos = new Vector2(5, camNodePos.y);

        toggleRaycasterScrollview(true);
    }

    private void CamRotation()
    {
        rotation.y = Mathf.Clamp(rotation.y, rotLimit.x, rotLimit.y);
    }

    private void toggleRaycasterScrollview(bool state)
    {
        if (raycasterScrollview == null) return;

        raycasterScrollview.scrollSensitivity = (state) ? raycasterScrollviewSensitivity : 0;
        raycasterScrollview.enabled = state;
    }
}
