using System.Collections.Generic;

using UnityEngine;

using Esri.ArcGISMapsSDK.Components;

using Wargaming.Core.Network.Plotting;
using Wargaming.Core.ScenarioEditor.Drawing;
using Wargaming.Core.GlobalParam.HelperDataPlotting;
using Wargaming.Core.GlobalParam.HelperPlotting;
using Wargaming.Core.GlobalParam.HelperDataAlutsista;
using Wargaming.Core.GlobalParam;

public class SaveDocumentController : MonoBehaviour
{
    public string convertedSatuan = "";
    public string convertedDrawing = "";
    List<ObjekTrash> trashCollections = new List<ObjekTrash>();

    public async void SaveDocument()
    {
        if(SessionUser.id == -99)
        {
            Debug.LogError("User ID Not Found To Saving Document! - (user = " + SessionUser.id + ")");
            return;
        }

        trashCollections = new List<ObjekTrash>();

        if (EntityController.instance == null) return;
        SaveDocumentSatuan();
        SaveDocumentDrawing();

        CollectPlottingTrash();

        await WargamingAPI.SavePlotting(convertedSatuan, convertedDrawing, trashCollections);
        Debug.Log("Document SAVED...");
    }

    public void SaveDocumentSatuan()
    {
        Debug.Log("Saving Satuan to Database...");
        List<List<object>> objekSatuan = new List<List<object>>();

        convertedSatuan = "[";

        for (int i=0; i < EntityController.instance.SATUAN.Count; i++)
        {
            var satuan = EntityController.instance.SATUAN[i];
            string jsonStyle = ObjekSatuanStyle.ToString(satuan.style);
            jsonStyle = ConvertToJSONString(jsonStyle);

            string jsonInfo = ObjekSatuanInfo.ToString(satuan.info);
            jsonInfo = ConvertToJSONString(jsonInfo);

            string symbol = "\"<div style='text-align:center'><span style='font-weight: bolder; margin-top:-15px; width: " + satuan.symbol.width + "px;color: " + PlottingHelper.GetColorName(satuan.symbol.warna) + "; font-size: 21px; font-family: " + satuan.symbol.fontFamily + "'>" + satuan.symbol.fontIndex + "</span></div>\"";

            var objLocation = satuan.gameObject.GetComponent<ArcGISLocationComponent>();
            if (objLocation == null) continue;

            objekSatuan.Add(new List<object>());
            objekSatuan[i].Add(satuan.userID);
            objekSatuan[i].Add(satuan.symbolID);
            objekSatuan[i].Add("\"" + satuan.documentID + "\"");
            objekSatuan[i].Add("\"" + satuan.nama + "\"");
            objekSatuan[i].Add(objLocation.Position.Y);
            objekSatuan[i].Add(objLocation.Position.X);
            objekSatuan[i].Add(symbol);
            objekSatuan[i].Add(jsonStyle);
            objekSatuan[i].Add(jsonInfo);
            objekSatuan[i].Add((satuan.id_kegiatan == null || satuan.id_kegiatan == "") ? "null" : satuan.id_kegiatan);
            objekSatuan[i].Add((satuan.isi_logistik == null || satuan.isi_logistik == "") ? "null" : satuan.isi_logistik);

            convertedSatuan += "[";
            for (int j = 0; j < objekSatuan[i].Count; j++)
            {
                convertedSatuan += objekSatuan[i][j];

                if(j < objekSatuan[i].Count - 1) convertedSatuan += ",";
            }
            
            if(i < EntityController.instance.SATUAN.Count - 1)
            {
                convertedSatuan += "],";
            }
            else
            {
                convertedSatuan += "]";
            }
        }

        convertedSatuan += "]";
    }

    public void SaveDocumentDrawing()
    {
        Debug.Log("Saving Drawing to Database...");
        List<List<object>> objekDrawing = new List<List<object>>();

        convertedDrawing = "[";

        for (int i = 0; i < EntityController.instance.DRAWING.Count; i++)
        {
            var drawing = EntityController.instance.DRAWING[i];
            string jsonProperty = EntityDrawingProperty.ToString(drawing.properties);
            jsonProperty = ConvertToJSONString(jsonProperty);

            string jsonGeometry = null;

            // Get Geometry Data By DrawingType
            if(drawing.type == DrawingHelper.DrawingType.POLYLINE)
            {

                var geometry = new EntityDrawingGeometryPoyline();
                geometry.type = drawing.type;

                geometry.coordinates = new List<List<double>>();
                for(int j=0; j < drawing.geometry.Count; j++)
                {
                    var geometryCoordinate = new List<double>();
                    geometryCoordinate.Add(drawing.geometry[j].GetComponent<ArcGISLocationComponent>().Position.X);
                    geometryCoordinate.Add(drawing.geometry[j].GetComponent<ArcGISLocationComponent>().Position.Y);

                    geometry.coordinates.Add(geometryCoordinate);
                }

                jsonGeometry = EntityDrawingGeometryPoyline.ToString(geometry);
                jsonGeometry = ConvertToJSONString(jsonGeometry);
            }
            else if(drawing.type == Wargaming.Core.GlobalParam.DrawingHelper.DrawingType.CIRCLE)
            {

            }
            else
            {

            }

            objekDrawing.Add(new List<object>());
            objekDrawing[i].Add(drawing.userID);
            objekDrawing[i].Add("\"" + drawing.documentID + "\"");
            objekDrawing[i].Add("\"" + drawing.nama + "\"");
            objekDrawing[i].Add(jsonGeometry);
            objekDrawing[i].Add(jsonProperty);
            objekDrawing[i].Add("\"" + EntityController.instance.DrawingTypeToString(drawing.type) + "\"");

            convertedDrawing += "[";
            for (int j = 0; j < objekDrawing[i].Count; j++)
            {
                convertedDrawing += objekDrawing[i][j];

                if (j < objekDrawing[i].Count - 1) convertedDrawing += ",";
            }

            if (i < EntityController.instance.DRAWING.Count - 1)
            {
                convertedDrawing += "],";
            }
            else
            {
                convertedDrawing += "]";
            }
        }

        convertedDrawing += "]";
    }

    public void CollectPlottingTrash()
    {
        for (int i = 0; i < PlotSatuanController.instance.trashData.Count; i++)
        {
            trashCollections.Add(PlotSatuanController.instance.trashData[i]);
        }

        for (int i = 0; i < PlotDrawingController.instance.trashData.Count; i++)
        {
            trashCollections.Add(PlotDrawingController.instance.trashData[i]);
        }
    }

    private string ConvertToJSONString(string _data)
    {
        _data = _data.Replace("\"", "\\\"");
        _data = _data.Replace("\\\\", "\\\\\\");
        _data = "\"" + _data + "\"";

        return _data;
    }
}
