using System.Threading.Tasks;
using System.Collections.Generic;

using UnityEngine;

using Crest;
using TMPro;

using Wargaming.Core.GlobalParam;
using Wargaming.Components.DebugComponent;
using Wargaming.Components.Playback;
using Wargaming.Components.Colyseus;
using Wargaming.Core.Network;
using Wargaming.TacticalMap.Component.Controller;
using Wargaming.Components.Controller;
using Esri.HPFramework;
using Esri.ArcGISMapsSDK.Components;

namespace Wargaming.Core.TFG
{
    public class GameplayTFGController : MonoBehaviour
    {
        [Header("UI Group")]
        public RectTransform mainUI;
        public RectTransform tacticalMapUI;
        public RectTransform hudUI;
        public RectTransform modalUI;
        public RectTransform logoutUI;

        [Header("UI Modal Mode")]
        public Animator modalMode;

        #region INSTANCE
        public static GameplayTFGController instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private async void Start()
        {
            if (DebugPlayGame.instance != null)
            {
                var state = await DebugPlayGame.instance.Init();
                if (!state)
                {
                    Debug.LogError("FAILED TO PLAY ON DEBUG MODE!");
                    return;
                }

                prepareTFGGameplay(DebugPlayGame.instance.GetRoomActive());
            }
        }

        // Prepare Client Gameplay
        public async void prepareTFGGameplay(string room)
        {
            //EnvironmentsController.SetSeaLevel(
            //    (double)StartupConfig.settings.DEFAULT_OCEAN_HEIGHT - 1
            //);

            modalMode.Play("FadeOut");

            Debug.Log("Preparing Gameplay...");
            LoadingController.UpdateGameplayLoading("Preparing Gameplay...");
            await Task.Delay(1000);

            // Set username and bagian to any text object with "label-username" and "label-asisten" tag
            setLabelUserAsisten();

            // Set Timetable data
            Debug.Log("Load Timetable Kegiatan...");
            LoadingController.UpdateGameplayLoading("Load Timetable Kegiatan...");
            var kegiatan = await TimetableController.instance.initTimetable();

            Debug.Log("Prepare Playback Timing...");
            LoadingController.UpdateGameplayLoading("Prepare Playback Timing...", 0.05f);
            PlaybackController.instance.InitPlayback(kegiatan);

            if (room == "sync")
            {
                // ROOM TFG SYNC
                Debug.Log("Starting Multipalyer Session...");

                Destroy(GetComponent<ColyseusTFGSingleController>());

                // -- JOIN COLYSEUS KHUSUS UNTUK GAMEPLAY MULTIPLAYER
                //Join or Create ROOM Colyseus dan masuk sebagai KOGAS(X)
                if (ColyseusTFGController.instance != null)
                {
                    Debug.Log("Joining CB...");
                    LoadingController.UpdateGameplayLoading("Joining Room...", 0.05f);
                    await Task.Delay(1000);

                    await ColyseusTFGController.instance.PrepareClient();
                }
            }
            else if (room == "unsync")
            {
                // ROOM TFG UN-SYNC
                Debug.Log("Starting Multipalyer (non-sync) Session...");

                Destroy(GetComponent<ColyseusTFGController>());

                // -- JOIN COLYSEUS KHUSUS UNTUK GAMEPLAY MULTIPLAYER NON-SYNC (TFG-SINGLE)
                //Join or Create ROOM Colyseus dan masuk sebagai KOGAS(X)
                LoadingController.UpdateGameplayLoading("Joining Room...", 0.05f);
                await Task.Delay(1000);

                await ColyseusTFGSingleController.instance.PrepareClient();
            }
            else
            {
                // TFG SINGLEPLAYER (OFFLINE)
                Debug.Log("Creating offline session...");

                Destroy(GetComponent<ColyseusTFGController>());
                Destroy(GetComponent<ColyseusTFGSingleController>());

                LoadingController.UpdateGameplayLoading("Joining Singleplayer Room...", 0.05f);
                await Task.Delay(1000);
            }

            if (SessionUser.id != 1)
            {
                Debug.Log("Load CB...");
                LoadingController.UpdateGameplayLoading("Load CB...");

                PlayerData.PLAYERS = new List<PData>();

                // Add User Into List
                PlayerData.AddPlayer(new PData
                {
                    id = SessionUser.id,
                    name = SessionUser.name,
                    nama_asisten = SessionUser.nama_asisten,
                    id_bagian = SessionUser.id_bagian,
                    jenis_user = SessionUser.jenis_user
                });

                // Prepare Load Data Alutsista dari CB Aktif Kogas
                await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, DataCB.GetYourCB().nama_document);
                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
            }

            //await EntityController.instance.loadEntityFromCB(21, 2, 80, "CB-3");
            //await EntityController.instance.loadEntityFromCB(18, 2, 80, "CB-2");
            //await EntityController.instance.loadEntityFromCB(72, 2, 80, "CB-1");
            //await EntityController.instance.loadEntityFromCB(30, 2, 80, "CB-4");
            //await EntityController.instance.loadEntityFromCB(51, 2, 80, "CB-1");
            //await EntityController.instance.loadEntityFromCB(124, 2, 80, "CB-1");

            SceneController.Instance.DestroyLoading("FadeOut");
        }

        private void setLabelUserAsisten()
        {
            foreach (var label in GameObject.FindGameObjectsWithTag("label-username"))
            {
                label.GetComponent<TextMeshProUGUI>().text = SessionUser.name;
            }

            foreach (var label in GameObject.FindGameObjectsWithTag("label-asisten"))
            {
                label.GetComponent<TextMeshProUGUI>().text = SessionUser.nama_asisten;
            }
        }

        public async void logout()
        {
            LoadingController.instance.ShowLoading("loading-rt", "Logging Out...");
            await Task.Delay(500);
            await WargamingAPI.RequestLogout();

            LoadingController.instance.HideLoading();
            await Task.Delay(500);

            if (await SceneController.Instance.LoadScene((SceneLoad.returnTo != null) ? SceneLoad.returnTo : SceneController.Instance.loginScene, null, "FadeIn"))
            {
                SceneController.Instance.DestroyLoading("FadeOut");
                await Task.Delay(1000);

                Destroy(this);
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                ToggleLogoutMenu();
            }
            else if (Input.GetKeyDown(KeyCode.M))
            {
                TacticalMapController.instance.toggler.isOn = !TacticalMapController.instance.toggler.isOn;
            }
        }

        public void RefreshRoute()
        {
            var missions = MissionController.instance.OBJ_MISSION;
            for (int i=0; i < missions.Count; i++)
            {
                missions[i].gameObject.SetActive(false);
                missions[i].gameObject.SetActive(true);
            }
        }

        public void ToggleLogoutMenu()
        {
            bool isOnLogoutMenu = false;

            if (logoutUI.GetComponent<CanvasGroup>().alpha == 1 && logoutUI.GetComponent<CanvasGroup>().interactable)
            {
                isOnLogoutMenu = true;
            }

            if (!isOnLogoutMenu)
            {
                logoutUI.gameObject.SetActive(false);
                logoutUI.gameObject.SetActive(true);
            }

            logoutUI.GetComponent<Animator>().Play((isOnLogoutMenu) ? "SlideOut" : "SlideIn");

            mainUI.GetComponent<Animator>().Play((!isOnLogoutMenu) ? "FadeOut" : "FadeIn");
            tacticalMapUI.GetComponent<Animator>().Play((!isOnLogoutMenu) ? "FadeOut" : "FadeIn");
            modalUI.GetComponent<Animator>().Play((!isOnLogoutMenu) ? "FadeOut" : "FadeIn");
            hudUI.GetComponent<Animator>().Play((!isOnLogoutMenu) ? "FadeOut" : "FadeIn");
        }
    }
}
