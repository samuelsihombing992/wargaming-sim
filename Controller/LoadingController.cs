using System;

using UnityEngine;
using UnityEngine.UI;

using TMPro;

public class LoadingController : MonoBehaviour
{
    private Metadata metadata;
    private GameObject activeLoading;

    #region INSTANCE
    public static LoadingController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        metadata = GetComponent<Metadata>();
    }

    /// <summary>
    /// Play or Show Selected Loading Component
    /// </summary>
    /// <param name="name">Name of Loading Component you want to load</param>
    /// <param name="message">Custom Message on Loading Component (optional)</param>
    /// <param name="animation">Custom Animation to toggle Loading Component (optional)</param>
    /// <param name="messageComponent">Custom Message Component on Loading Component Metadata (optional)</param>
    public void ShowLoading(string name, string message = null, string animation = null, string messageComponent = null)
    {
        if (metadata != null)
        {
            activeLoading = metadata.FindParameter(name);
            activeLoading.name = name;

            activeLoading.GetComponent<Animator>().Play((animation != null) ? animation : "FadeIn");

            if (activeLoading.GetComponent<Metadata>() != null)
            {
                var objMessage = activeLoading.GetComponent<Metadata>().FindParameter((messageComponent != null) ? messageComponent : "message");

                if (message != null)
                {
                    objMessage.SetActive(true);
                    objMessage.GetComponent<TextMeshProUGUI>().text = message;
                }
                else
                {
                    objMessage.SetActive(false);
                }
            }
        }
        else
        {
            Debug.LogError("Loading Manager doesn't have any Metadata. Failed To Show Loading Component!");
        }
    }

    public async void HideLoading(string animation = null)
    {
        if (activeLoading == null) return;

        activeLoading.GetComponent<Animator>().Play((animation != null) ? animation : "FadeOut");
        activeLoading = null;
    }

    /// <summary>
    /// Change Loading Text After Each Prepare Progress Done
    /// </summary>
    /// <param name="message"></param>
    public static void UpdateGameplayLoading(string message, float progressAddded = -99)
    {
        // LEVEL MANAGER LOADING COMPONENT
        try
        {
            var lManagerMetadata = SceneController.activeLoading.GetComponent<Metadata>();

            // Set Loading Message
            lManagerMetadata.FindParameter("message").GetComponent<TextMeshProUGUI>().text = message;
            // Set Progressbar Amount
            lManagerMetadata.FindParameter("progress-bar").GetComponent<Image>().fillAmount += (progressAddded == -99) ? 0.1f : progressAddded;

        }
        catch (Exception e) { }
    }
}
