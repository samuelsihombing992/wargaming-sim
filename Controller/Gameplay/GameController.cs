using System.Threading.Tasks;
using System.Collections.Generic;

using UnityEngine;

using TMPro;

using Wargaming.Core;
using Wargaming.Core.Network;
using Wargaming.Core.GlobalParam;
using Wargaming.Components.Playback;
using Wargaming.Components.Colyseus;
using Wargaming.Components.DebugComponent;

using static LoadingControllerV2;
using Wargaming.Components.Controller;

public class GameController : MonoBehaviour
{
    [Header("UI Group")]
    public RectTransform mainUI;
    public RectTransform tacmapUI;
    public RectTransform modalUI;
    public RectTransform pauseUI;

    public GameObject modalMode;

    [System.NonSerialized] public static GameObject activeLoading;

    #region INSTANCE
    public static GameController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private async void Start()
    {
        if (DebugPlayGame.instance != null)
        {
            var state = await DebugPlayGame.instance.Init();
            if (!state)
            {
                Debug.LogError("FAILED TO PLAY ON DEBUG MODE!");
                return;
            }

            StartGameplay(DebugPlayGame.instance.GetRoomActive());
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePauseMenu();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            ToggleViewMode();
        }
    }

    public async void StartGameplay(string room)
    {
        EnvironmentsController.SetSeaLevel(StartupConfig.settings.DEFAULT_OCEAN_HEIGHT.GetValueOrDefault(2) - 1);
        EnvironmentsController.SetCloudsAltitude(StartupConfig.settings.DEFAULT_CLOUDS_HEIGHT.GetValueOrDefault(7000));

        UserController.instance.CreateUserList(SessionUser.id.Value, (SessionUser.asisten != 0) ? SessionUser.nama_asisten + " " : SessionUser.name);

        modalMode.PlayAnimation("FadeOut");

        Debug.Log("Preparing Gameplay...");
        activeLoading = ShowSideScreenLoading(LoadingScreenSide.SCREEN_SIDE_BOTTOM_RIGHT, "Preparing Gameplay...");
        await Task.Delay(1000);

        SetAnyUserAsistenLabel();

        Debug.Log("Load Timetable Kegiatan...");
        activeLoading = ShowSideScreenLoading(LoadingScreenSide.SCREEN_SIDE_BOTTOM_RIGHT, "Load Timetable Kegiatan...");
        var kegiatan = await TimetableController.instance.initTimetable();

        Debug.Log("Preparing Playback Timing...");
        activeLoading = ShowSideScreenLoading(LoadingScreenSide.SCREEN_SIDE_BOTTOM_RIGHT, "Preparing Playback Timing...");
        PlaybackController.instance.InitPlayback(kegiatan);

        CBManagerController.instance.CleanContainer();

        if (room == "sync")
        {
            JoinSyncRoom();
        }else if(room == "unsync")
        {
            JoinUnsyncRoom();
        }
        else
        {
            CBManagerController.instance.Init();

            StartSingleplayer();
        }
    }

    private async void JoinSyncRoom()
    {
        Debug.Log("Starting Multiplayer Session...");

        if(ColyseusTFGController.instance == null)
        {
            Debug.LogError("Failed To Join/Create Room. There is no ColyseusTFGController Script in Scene!");
            return;
        }

        Destroy(ColyseusTFGSingleController.instance.gameObject);
        Destroy(ColyseusWGSController.instance.gameObject);

        activeLoading = ShowSideScreenLoading(LoadingScreenSide.SCREEN_SIDE_BOTTOM_RIGHT, "Joining Room...");
        await Task.Delay(1000);

        await ColyseusTFGController.instance.PrepareClient();
    }

    private async void JoinUnsyncRoom()
    {
        Debug.Log("Starting Multiplayer Session...");

        if (ColyseusTFGController.instance == null)
        {
            Debug.LogError("Failed To Join/Create Room. There is no ColyseusTFGController Script in Scene!");
            return;
        }

        Destroy(ColyseusTFGController.instance.gameObject);
        Destroy(ColyseusWGSController.instance.gameObject);

        PlaybackController.instance.OnPlaybackAction("play");

        activeLoading = ShowSideScreenLoading(LoadingScreenSide.SCREEN_SIDE_BOTTOM_RIGHT, "Joining Room...");
        await Task.Delay(1000);

        await ColyseusTFGSingleController.instance.PrepareClient();
    }

    private async void StartSingleplayer()
    {
        Debug.Log("Starting Singleplayer...");

        Destroy(ColyseusTFGController.instance.gameObject);
        Destroy(ColyseusTFGSingleController.instance.gameObject);
        Destroy(ColyseusWGSController.instance.gameObject);

        activeLoading = ShowSideScreenLoading(LoadingScreenSide.SCREEN_SIDE_BOTTOM_RIGHT, "Starting Singleplayer...");
        await Task.Delay(1000);

        if(SessionUser.id != 1)
        {
            activeLoading = ShowSideScreenLoading(LoadingScreenSide.SCREEN_SIDE_BOTTOM_RIGHT, "Load CB...");

            PlayerData.PLAYERS = new List<PData>();

            // Add User Into List
            PlayerData.AddPlayer(new PData
            {
                id              = SessionUser.id,
                name            = SessionUser.name,
                nama_asisten    = SessionUser.nama_asisten,
                id_bagian       = SessionUser.id_bagian,
                jenis_user      = SessionUser.jenis_user
            });

            await EntityController.instance.loadEntityFromCB(
                SessionUser.id,
                SessionUser.id_kogas,
                SkenarioAktif.ID_SKENARIO,
                DataCB.GetYourCB().nama_document
            );
        }

        HideLoading(activeLoading);
    }

    public void SetAnyUserAsistenLabel()
    {
        // Set any objects tagged "label-username" and "label-asisten" to show current Username & Asisten
        foreach (var label in GameObject.FindGameObjectsWithTag("label-username"))
        {
            var labelText = label.GetComponent<TextMeshProUGUI>();
            if (labelText == null) continue;

            labelText.text = SessionUser.name;
        }

        foreach (var label in GameObject.FindGameObjectsWithTag("label-asisten"))
        {
            var labelText = label.GetComponent<TextMeshProUGUI>();
            if (labelText == null) continue;

            labelText.text = SessionUser.nama_asisten;
        }
    }

    public void RefreshRoute()
    {
        var missions = MissionController.instance.OBJ_MISSION;
        for (int i = 0; i < missions.Count; i++)
        {
            missions[i].gameObject.SetActive(false);
            missions[i].gameObject.SetActive(true);
        }
    }

    public async void Logout()
    {
        ShowOverlayLoading(
            LoadingOverlay.OVERLAY,
            pauseUI.transform,
            "Logging Out..."
        );

        await Task.Delay(500);
        await WargamingAPI.RequestLogout();

        if (await SceneController.Instance.LoadScene((SceneLoad.returnTo != null) ? SceneLoad.returnTo : SceneController.Instance.loginScene, null, "FadeIn"))
        {
            SceneController.Instance.DestroyLoading("FadeOut");
            await Task.Delay(1000);

            Destroy(this);
        }
    }

    public void TogglePauseMenu()
    {
        bool active = false;
        if (pauseUI.GetComponent<CanvasGroup>().alpha == 1 && pauseUI.GetComponent<CanvasGroup>().interactable)
        {
            active = true;
        }

        if (!active)
        {
            pauseUI.gameObject.SetActive(false);
            pauseUI.gameObject.SetActive(true);
        }

        pauseUI.gameObject.PlayAnimation((active) ? "SlideOut" : "SlideIn", 2);

        mainUI.gameObject.PlayAnimation((!active) ? "FadeOut" : "FadeIn");
        tacmapUI.gameObject.PlayAnimation((!active) ? "FadeOut" : "FadeIn");
        modalUI.gameObject.PlayAnimation((!active) ? "FadeOut" : "FadeIn");
    }

    private void ToggleViewMode()
    {
        if (pauseUI.GetComponent<CanvasGroup>().alpha == 1 && pauseUI.GetComponent<CanvasGroup>().interactable) return;

        bool active = false;
        if (mainUI.GetComponent<CanvasGroup>().alpha == 1 && mainUI.GetComponent<CanvasGroup>().interactable)
        {
            active = true;
        }

        mainUI.gameObject.PlayAnimation((active) ? "FadeOut" : "FadeIn");
    }
}
