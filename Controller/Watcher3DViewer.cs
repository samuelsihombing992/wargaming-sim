using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Watcher3DViewer : MonoBehaviour
{
    public Transform parent;
    public Transform references;

    void Update()
    {
        if (references == null || parent == null) return;

        parent.localRotation = references.localRotation;
    }
}
