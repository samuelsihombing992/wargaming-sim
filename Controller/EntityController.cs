#region IMPORTS
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using Newtonsoft.Json.Linq;
using TMPro;
using Shapes;
using Battlehub.RTCommon;
using SickscoreGames.HUDNavigationSystem;
using FIMSpace.GroundFitter;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;
using GPUInstancer;

using Esri.HPFramework;
using Esri.GameEngine.Geometry;
using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;

using Wargaming.Core;
using Wargaming.Core.Network;
using Wargaming.Core.GlobalParam;
using Wargaming.Components.Colyseus;
using Wargaming.Components.Controller;
using Wargaming.Components.Playback;
using Wargaming.Core.GlobalParam.HelperColyseusTFG;
using Wargaming.Core.GlobalParam.HelperDataAlutsista;
using Wargaming.Core.GlobalParam.HelperPlotting;
using Wargaming.TacticalMap.Component.Controller;
using WGSBundling.Core;
using WGSBundling.Bundles.AircraftEngine;
using WGSBundling.Bundles.Animated;
using WGSBundling.Bundles.CargoHold;
using WGSBundling.Bundles.DynamicWave;
using WGSBundling.Bundles.HeliRotor;
using WGSBundling.Bundles.Sound;
using WGSBundling.Bundles.VFX;
using WGSBundling.Bundles.WheeledVehicle;
using static Wargaming.Core.GlobalParam.DrawingHelper;
using static Wargaming.Core.GlobalParam.EntityHelper;
using Wargaming.Core.ScenarioEditor.Drawing;
using Esri.ArcGISMapsSDK.Utils.Math;
using WGSBundling.Bundles.Aircraft;
using Wargaming.Components.Walker;
using WGSBundling.Bundles.Helipad;
#endregion

public class EntityController : MonoBehaviour
{
    [Header("Map")]
    //public ArcGISMapComponent map;
    //public Transform rootMap;
    public Transform satuanContainer;
    public Transform guidanceContainer;
    public Transform altitudeGuidanceContainer;


    [Header("GPU Instancer")]
    public GPUInstancerPrefabManager instancerManager;

    [Header("Data Entity")]
    public List<ObjekSatuan> SATUAN         = new List<ObjekSatuan>();

    public List<ObjekSatuan> VEHICLE        = new List<ObjekSatuan>();
    public List<ObjekSatuan> SHIP           = new List<ObjekSatuan>();
    public List<ObjekSatuan> SUBMARINE      = new List<ObjekSatuan>();
    public List<ObjekSatuan> AIRCRAFT       = new List<ObjekSatuan>();
    public List<ObjekSatuan> HELICOPTER     = new List<ObjekSatuan>();
    public List<ObjekSatuan> INFANTRY       = new List<ObjekSatuan>();

    public List<ObjekRadar> RADAR           = new List<ObjekRadar>();
    public List<ObjekDrawing> DRAWING       = new List<ObjekDrawing>();

    public List<ParatroopsWalker> DATA_PARATROOPS = new List<ParatroopsWalker>();

    public bool tacmapOrientation = true;
    //[NonSerialized] public bool onLoadProcess = false;

    [Header("Entity Data References")]
    public AnimationCurve altitudeMotion;
    public AnimationCurve takeoffMotion;
    public AnimationCurve landingMotion;

    [Header("Aircraft Flight Motion")]
    public AnimationCurve aircraftFlightMotion;
    public AnimationCurve aircraftTakeoffMotion;
    public AnimationCurve aircraftLandingMotion;
    public AnimationCurve helipadTakeoffStep;
    public AnimationCurve helipadLandingStep;
    public float aircraftMaxAlt = 8000;

    [Header("Helicopter Flight Motion")]
    public AnimationCurve heliFlightMotion;
    public AnimationCurve heliTakeoffMotion;
    public AnimationCurve heliLandingMotion;
    public float heliMaxAlt = 3048;

    private List<GPUInstancerPrefab> allocatedSatuan = new List<GPUInstancerPrefab>();

    [NonSerialized] public bool OnLoadCB = false;


    #region INSTANCE
    public static EntityController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    /// <summary>
    /// Load & Spawn Entity CB Terpilih dari Database
    /// </summary>
    /// 
    public async Task loadEntityFromCB(long? id_user, long? id_kogas, long? id_scenario, string nama_document, bool isScenarioEditor = false)
    {
        if (OnLoadCB) return;
        OnLoadCB = true;

        // Load Data CB dari Database
        var plotting = await WargamingAPI.loadDataCB(id_user, id_kogas, id_scenario, nama_document);

        // Load Data Terkait Document CB Terpilih
        await loadDocumentCB(JArrayExtended.setJArrayResult(plotting, 17));

        // Spawn Entity Satuan
        await loadEntitySatuan(JArrayExtended.setJArrayResult(plotting, 0));

        // Configure Bundle Package Data (Wave Generator, Sound, VFX, Cargo Door, Wheels, Animated Object, ETC) - ONLY FOR GAMEPLAY
       if (!isScenarioEditor) configureBundlePackage(SATUAN);

        // Spawn Entity Radar
        //await loadEntityRadar(JArrayExtended.setJArrayResult(plotting, 7));

        // Spawn Entity Drawing
        await loadDrawing(JArrayExtended.setJArrayResult(plotting, 10), isScenarioEditor);

        // Spawn Entity Text
        await loadText(JArrayExtended.setJArrayResult(plotting, 11), isScenarioEditor);

        // Spawn Rute Satuan
        await loadMisiSatuan(JArrayExtended.setJArrayResult(plotting, 13), isScenarioEditor);

        // Expose To Editor (Scenario Editor Mode Only)
        if (isScenarioEditor) await exposeToEditor(SATUAN);

        //InitGPUInstancer();

        //for (int i = 0; i < DATA_SATUAN.Count; i++)
        //{
        //    if (DATA_SATUAN[i].GetComponent<EntityWalkerV2>().missions == null)
        //    {
        //        EmbarkEntity(DATA_SATUAN[i].gameObject);
        //    }
        //    else if (DATA_SATUAN[i].GetComponent<EntityWalkerV2>().missions.Count <= 0)
        //    {
        //        EmbarkEntity(DATA_SATUAN[i].gameObject);
        //    }
        //}

        if (ListEntityManager.instance) ListEntityManager.instance.RefreshData();

        // Load CB Selesai, Set Player yang sedang di Load "isLoaded" = true
        if(ColyseusTFGController.instance != null)
        {
            for (int i = 0; i < PlayerData.PLAYERS.Count; i++)
            {
                Debug.Log(PlayerData.PLAYERS[i].id + " | " + id_user.Value);
                if (PlayerData.PLAYERS[i].id == id_user.Value)
                {
                    PlayerData.PLAYERS[i].isLoaded = true;
                    break;
                }
            }
        }

        OnLoadCB = false;

        if (ColyseusTFGController.instance != null)
        {
            // Cek Next Player untuk di load
            for (int i = 0; i < PlayerData.PLAYERS.Count; i++)
            {
                var selectedPlayer = PlayerData.PLAYERS[i];

                Debug.Log("Check ID Player " + selectedPlayer.id);
                if (!selectedPlayer.isLoaded && selectedPlayer.cbAktif != null)
                {
                    //Debug.Log("Check ID Player " + selectedPlayer.id);
                    //Debug.Log("Check ID Player " + selectedPlayer.cbAktif);
                    //Debug.Log("Check ID Player " + selectedPlayer.cbAktif.nama_document);
                    //Debug.Log("Check ID Player " + selectedPlayer.cbAktif.id_kogas);

                    await loadEntityFromCB(selectedPlayer.id, selectedPlayer.cbAktif.id_kogas, SkenarioAktif.ID_SKENARIO, selectedPlayer.cbAktif.nama_document);
                    return;
                }
            }
        }

        //if (ColyseusTFGController.instance != null)
        //{
        //    var tempUser = ColyseusTFGController.instance.TEMP_USER;
        //    tempUser.Remove(id_user.Value);

        //    if (tempUser.Count > 0)
        //    {
        //        await loadEntityFromCB(tempUser[0], id_kogas, id_scenario, nama_document);
        //        return;
        //    }
        //} else if (ColyseusTFGSingleController.instance != null)
        //{
        //    var tempUser = ColyseusTFGSingleController.instance.TEMP_USER;
        //    tempUser.Remove(id_user.Value);

        //    if (tempUser.Count > 0)
        //    {
        //        await loadEntityFromCB(tempUser[0], id_kogas, id_scenario, nama_document);
        //        return;
        //    }
        //}
        //else if (ColyseusWGSController.instance != null)
        //{

        //}

        LoadingController.instance.HideLoading();
    }

    public async Task<bool> loadDocumentCB(JArray data)
    {
        LoadingController.instance.ShowLoading("loading-rt", "Loading Document...");
        if (!JArrayExtended.checkingJArrayData(data)) return false;

        Debug.Log("Prepare Document Data ");
        Debug.Log(data[0].ToString());

        var document = EntityDocument.FromJson(data[0]);
        SkenarioAktif.ID_DOCUMENT = document.id_document;

        return true;
    }

    public async Task<bool> loadEntitySatuan(JArray data)
    {
        if (!StartupConfig.settings.ENABLE_LOAD_SATUAN) return true;

        LoadingController.instance.ShowLoading("loading-rt", "Loading Satuan...");
        if (!JArrayExtended.checkingJArrayData(data)) return false;

        for (int i = 0; i < data.Count; i++)
        {
            Debug.Log("Spawn Satuan " + i);
            Debug.Log(data[i].ToString());

            var satuan = ObjekSatuanHelper.FromJson(data[i]);
            await spawnEntitySatuan(satuan);
        }

        return true;
    }

    public async Task<bool> loadMisiSatuan(JArray data, bool isScenarioEditor = false)
    {
        if (!StartupConfig.settings.ENABLE_LOAD_SATUAN) return true;
        if (isScenarioEditor) return true;

        LoadingController.instance.ShowLoading("loading-rt", "Loading Misi...");
        if (!JArrayExtended.checkingJArrayData(data)) return false;

        for (int i = 0; i < data.Count; i++)
        {
            Debug.Log("Spawn Misi " + i);
            Debug.Log(data[i].ToString());

            var mission = MisiSatuan.FromJson(data[i].ToString());
            //await spawnMisiSatuan(mission);

            await spawnRootMisiSatuan(mission);
        }

        return true;
    }

    //public async Task spawnEntitySatuan(EntitySatuan satuan)
    public async Task spawnEntitySatuan(ObjekSatuanHelper satuan)
    {
        if (satuan.style.grup == 10)
        {
            satuan.kategori = KategoriSatuan.INFANTRY;
            if (satuan.tipe_tni == null) await SetDetailSatuan(satuan);
        }
        else
        {
            if (satuan.tipe_tni == null) await SetDetailSatuan(satuan);
        }

        var objPosition = new ArcGISPoint(satuan.lng_x, satuan.lat_y, setSatuanHeight(satuan), ArcGISSpatialReference.WGS84());
        var objRotation = new ArcGISRotation(satuan.info.heading, 90, 0);

        // Create Entity Berdasarkan Object 3D-nya
        var OBJ     = CreateEntity(objPosition, objRotation, AssetPackageController.Instance.FindAsset(satuan.path_object_3d, satuan.kategori, satuan.nama).gameObject, satuanContainer);
        ChangePasukanSkin(OBJ, satuan);

        OBJ.name    = satuan.nama;
        OBJ.tag     = "entity";
        OBJ.layer   = LayerMask.NameToLayer("Entities");

        GenerateBoundingBox(OBJ);

        #region SPAWN ROOT WALKER
        //var rootWalker = PrepareRootWalker(satuan, OBJ, objPosition, objRotation);
        PrepareRootWalker(satuan, OBJ, objPosition, objRotation);
        #endregion

        // Get Satuan Author's Data
        var findAuthorData = PlayerData.PLAYERS.FindIndex(x => x.id == satuan.id_user);

        PData authorData = null;
        if (findAuthorData != -1) authorData = PlayerData.PLAYERS[findAuthorData];

        // Setup Data Satuan
        //var objData             = OBJ.AddComponent<DataSatuan>();
        //objData.isActive        = true;
        //objData.author          = satuan.id_user;
        //objData.authorData      = (authorData != null) ? authorData : null;
        //objData.id              = satuan.id_satuan;
        //objData.kategori        = satuan.kategori;
        ////objData.type                = GetTypeSatuan(OBJ.GetComponent<WGSBundleCore>().bundleType);
        //objData.namaSatuan      = satuan.data_info.nama_satuan;
        //objData.noSatuan        = satuan.data_info.nomer_satuan;
        //objData.descSatuan      = satuan.data_style.keterangan;
        //objData.fontTaktis      = satuan.data_style.font_taktis;
        //objData.simbolTaktis    = ((char)satuan.data_style.font_ascii).ToString();
        //objData.simbolTaktisColor = getFontTaktisColor(satuan.data_info.warna);
        //objData.hudElement = CreateHUDElementV2(
        //    OBJ.transform,
        //    objData.simbolTaktisColor,
        //    satuan.data_info.nama_satuan,
        //    getFontTaktis(satuan.data_style.font_taktis),
        //    getFontTaktis(satuan.data_style.font_taktis + "_BG"),
        //    objData.simbolTaktis
        //);

        if (OBJ.GetComponent<WGSBundleCore>().submersible) { satuan.kategori = KategoriSatuan.SUBMARINE; }

        var objData                 = OBJ.AddComponent<ObjekSatuan>();
        objData.userID              = satuan.id_user.GetValueOrDefault(-99);
        objData.documentID          = satuan.dokumen.GetValueOrDefault(-99);
        objData.symbolID            = satuan.symbolID;
        objData.nama                = satuan.nama;
        objData.kategori            = satuan.kategori;
        objData.style               = satuan.style;
        objData.info                = satuan.info;
        //objData.weapon            = prepareSatuanWeapon(satuan.info.weapon);
        objData.id_kegiatan         = satuan.id_kegiatan;
        objData.isi_logistik        = satuan.isi_logistik;

        var objSymbol               = new ObjekSymbol();
        objSymbol.width             = (int)satuan.info.size;
        objSymbol.warna             = satuan.info.warna;
        objSymbol.fontFamily        = satuan.style.fontTaktis;
        objSymbol.fontIndex         = getFontCharacter(satuan.style.fontIndex);

        objData.symbol              = objSymbol;

        objData.hudElement = CreateHUDElementV2(
            OBJ.GetComponent<WGSBundleCore>().parent.transform,
            objData.symbol.warna,
            satuan.info.namaSatuan,
            getFontTaktis(objData.symbol.fontFamily.Replace("_BLUE","").Replace("_RED", "")),
            getFontTaktis(objData.symbol.fontFamily.Replace("_BLUE", "").Replace("_RED", "") + "_BG"),
            objData.symbol.fontIndex
        );

        SATUAN.Add(objData);
        switch (satuan.kategori)
        {
            case KategoriSatuan.VEHICLE:
                VEHICLE.Add(objData);
                break;
            case KategoriSatuan.SHIP:
                SHIP.Add(objData);
                break;
            case KategoriSatuan.SUBMARINE:
                SUBMARINE.Add(objData);
                break;
            case KategoriSatuan.AIRCRAFT:
                AIRCRAFT.Add(objData);
                break;
            case KategoriSatuan.HELICOPTER:
                HELICOPTER.Add(objData);
                break;
            case KategoriSatuan.INFANTRY:
                INFANTRY.Add(objData);
                break;
        }

        //objWalker.tacmapMarker = spawnTacticalMapSymbol(obj_position, objData);

        Sprite imageSatuan = OBJ.GetComponent<WGSBundleCore>().icon;

        // GAMEPLAY SCENE ONLY
        //var splineController = OBJ.AddComponent<SplineController>();
        //splineController.OrientationAxis = OrientationAxisEnum.Right;
        //splineController.Clamping = CurvyClamping.Clamp;
        //splineController.MotionConstraints = MotionConstraints.FreezeRotationZ;
        //splineController.PlayAutomatically = true;
        //splineController.UpdateIn = CurvyUpdateMethod.LateUpdate;

        //var objWalker = OBJ.AddComponent<EntityWalkerV2>();

        #region CREATE ROOT SPLINE
        //var splineController = rootWalker.AddComponent<SplineController>();
        var splineController = OBJ.AddComponent<SplineController>();

        splineController.OrientationAxis = OrientationAxisEnum.Right;
        splineController.Clamping = CurvyClamping.Clamp;
        splineController.PlayAutomatically = true;
        splineController.UpdateIn = CurvyUpdateMethod.Update;
        #endregion

        if (OBJ.GetComponent<WGSBundleCore>().bundleType == WGSBundleCore.BundleType.LAND)
        {
            splineController.MotionConstraints = MotionConstraints.FreezePositionY | MotionConstraints.FreezeRotationZ | MotionConstraints.FreezeRotationX;
            //var terrainFollower = OBJ.AddComponent<FollowTerrain>();

            //GenerateTerrainFollower(OBJ, FIMSpace.Basics.EFUpdateClock.LateUpdate);
            GenerateTerrainFollower(OBJ, FIMSpace.Basics.EFUpdateClock.Update);
        }
        else if (OBJ.GetComponent<WGSBundleCore>().bundleType == WGSBundleCore.BundleType.AIR)
        {
            splineController.MotionConstraints = MotionConstraints.FreezePositionY | MotionConstraints.FreezeRotationZ | MotionConstraints.FreezeRotationX;
            //var terrainFollower = OBJ.AddComponent<FollowTerrain>();

            //GenerateTerrainFollower(OBJ);
        }else if(OBJ.GetComponent<WGSBundleCore>().bundleType == WGSBundleCore.BundleType.SEA)
        {
            splineController.MotionConstraints = MotionConstraints.FreezeRotationZ;
        }

        #region CREATE ROOT SYNC
        //var rootSync            = OBJ.AddComponent<RootSync>();
        //rootSync.rootTransform  = rootWalker.GetComponent<HPTransform>();

        //rootSync.Init();
        #endregion

        //InitRootWalker(rootWalker, satuan);
        InitRootWalker(OBJ, satuan);

        // Add to List
        if (ListEntityManager.instance != null)
        {
            ListEntityManager.instance.AddData(objData);
        }

        //AssetPackageController.ConfigSoundModule(OBJ.GetComponent<NodeSound>());
        //AssetPackageController.ConfigVFXModule(OBJ.GetComponent<NodeVFX>());

        //if (!isScenarioEditor)
        //{

        //}
        //else
        //{

        //}

        //if (OBJ.GetComponent<ShipBundleData>() != null)
        //{
        //    OBJ.AddComponent<ShipFoamWatcher>();
        //    imageSatuan = OBJ.GetComponent<ShipBundleData>().image;
        //}
        //else if(OBJ.GetComponent<AircraftBundleData>() != null)
        //{
        //    AssetPackageController.Instance.ConfigAircraftParticles(OBJ.GetComponent<AircraftBundleData>());
        //    imageSatuan = OBJ.GetComponent<AircraftBundleData>().image;

        //    if(OBJ.GetComponent<AircraftBundleData>().turnLimit != 0)
        //    {
        //        objWalker.guidance = CreateEntity(obj_position, Vector3.zero, AssetPackageController.Instance.prefabGuidance.gameObject).GetComponent<SplineController>();
        //        objWalker.syncRoll = true;
        //        objWalker.sensitivity = 10;
        //        objWalker.guidanceDistance = 1000;
        //    }
        //}
        //else if(OBJ.GetComponent<VehicleBundleData>() != null)
        //{
        //    imageSatuan = OBJ.GetComponent<VehicleBundleData>().image;
        //    splineController.MotionConstraints = MotionConstraints.FreezePositionY | MotionConstraints.FreezeRotationZ;


        //    //splineController.MotionConstraints = MotionConstraints.FreezePositionY;

        //    //splineController.MotionConstraints = MotionConstraints.FreezePositionX
        //    OBJ.AddComponent<FollowTerrain>();
        //}

        //DATA_SATUAN.Add(objData);

        //switch (satuan.kategori)
        //{
        //    case KategoriSatuan.VEHICLE:
        //        DATA_VEHICLE.Add(objData);
        //        break;
        //    case KategoriSatuan.SHIP:
        //        DATA_SHIP.Add(objData);
        //        break;
        //    case KategoriSatuan.AIRCRAFT:
        //        DATA_AIRCRAFT.Add(objData);
        //        break;
        //    case KategoriSatuan.INFANTRY:
        //        DATA_INFANTRY.Add(objData);
        //        DATA_VEHICLE.Add(objData);
        //        break;
        //}

        // Set Scale Satuan From AppConfig
        //OBJ.transform.SetScaleByType(objData.kategori);



        //if (ListEntityController.instance == null) return;
        //if (ListEntityController.instance.listEntity != null)
        //{
        //    var newList = new ListEntityData()
        //    {
        //        entity = OBJ,
        //        fontTaktis = getFontTaktis(objData.style.fontTaktis),
        //        simbolTaktis = objData.style.fontIndex,
        //        image = imageSatuan,
        //        id = objData.id,
        //        nama = objData.namaSatuan,
        //        keterangan = objData.descSatuan,
        //        kategori = satuan.kategori,
        //        cellSize = 0
        //    };

        //    ListEntityController.instance.listEntity.data.Add(newList);
        //    ListEntityController.instance.listEntity.tempData.Add(newList);
        //}
    }

    public GameObject CreateEntity(Vector3 position, Vector3 rotation, GameObject obj, Transform parent = null)
    {
        var newObj = Instantiate(obj, (parent != null) ? parent : EnvironmentsController.GetMapComponent().transform);

        var locationComp = newObj.AddComponent<ArcGISLocationComponent>();
        locationComp.Position = new ArcGISPoint(position.x, position.y, position.z + 3, ArcGISSpatialReference.WGS84());
        locationComp.Rotation = new ArcGISRotation(rotation.x, rotation.y, rotation.z);

        var objCore = newObj.GetComponent<WGSBundleCore>();
        if (objCore != null)
        {
            objCore.parent.localRotation = Quaternion.Euler(objCore.origin);
        }

        return newObj;
    }

    public void ChangePasukanSkin(GameObject obj, ObjekSatuanHelper satuan)
    {
        if (satuan.kategori == KategoriSatuan.INFANTRY)
        {
            if (satuan.path_object_3d == null) return;

            if (obj.GetComponent<PasukanAnim>() != null)
            {
                for (int i = 0; i < obj.GetComponent<PasukanAnim>().ListOrangan.Count; i++)
                {
                    obj.GetComponent<PasukanAnim>().ListOrangan[i].GetComponent<SkinChange>().ChangeSkin(satuan.path_object_3d);
                    //Debug.Log("Change Skin to " + satuan.path_object_3d);
                }
            }
        }
    }

    public GameObject CreateEntity(ArcGISPoint position, ArcGISRotation rotation, GameObject obj, Transform parent = null)
    {
        var newObj = Instantiate(obj, (parent != null) ? parent : EnvironmentsController.GetMapComponent().transform);

        var locationComp = newObj.AddComponent<ArcGISLocationComponent>();
        locationComp.Position = position;
        locationComp.Rotation = rotation;

        var objCore = newObj.GetComponent<WGSBundleCore>();
        if (objCore != null)
        {
            objCore.parent.localRotation = Quaternion.Euler(objCore.origin);
        }

        return newObj;
    }

    public GameObject CreateEntity(ArcGISLocationComponent locationComponent, GameObject obj, Transform parent = null)
    {
        var newObj = Instantiate(obj, (parent != null) ? parent : EnvironmentsController.GetMapComponent().transform);

        var locationComp = newObj.AddComponent<ArcGISLocationComponent>();
        locationComp.Position = locationComponent.Position;
        locationComp.Rotation = locationComponent.Rotation;

        var objCore = newObj.GetComponent<WGSBundleCore>();
        if (objCore != null)
        {
            objCore.parent.localRotation = Quaternion.Euler(objCore.origin);
        }

        return newObj;
    }

    public GameObject CreateEntity(ArcGISPoint position, Vector3 rotation, GameObject obj, Transform parent = null)
    {
        var newObj = Instantiate(obj, (parent != null) ? parent : EnvironmentsController.GetMapComponent().transform);

        var locationComp = newObj.AddComponent<ArcGISLocationComponent>();
        locationComp.Position = position;
        locationComp.Rotation = new ArcGISRotation(rotation.x, rotation.y, rotation.z);

        var objCore = newObj.GetComponent<WGSBundleCore>();
        if (objCore != null)
        {
            objCore.parent.localRotation = Quaternion.Euler(objCore.origin);
        }

        return newObj;
    }

    //public TypeSatuan getTypeSatuan(GameObject obj)
    //{
    //    if (obj.GetComponent<ShipBundleData>() != null)
    //    {
    //        return (obj.GetComponent<ShipBundleData>().type == ShipBundleData.shipBundleType.SURFACE) ? TypeSatuan.SURFACE : TypeSatuan.SUB;
    //    }
    //    else if (obj.GetComponent<AircraftBundleData>() != null)
    //    {
    //        var bundleData = obj.GetComponent<AircraftBundleData>();

    //        if (bundleData.nodeEngine == null) return TypeSatuan.JET;
    //        if (bundleData.nodeEngine.Count <= 0)
    //        {
    //            return TypeSatuan.JET;
    //        }
    //        else
    //        {
    //            if(bundleData.nodeEngine[0].type == AircraftBundleData.BundleNodeEngine.propsType.PROPS)
    //            {
    //                return TypeSatuan.PROPS;
    //            }
    //            else
    //            {
    //                return TypeSatuan.JET;
    //            }
    //        }

    //        if(bundleData.nodeRotor == null) return TypeSatuan.JET;
    //        if (bundleData.nodeRotor.Count <= 0)
    //        {
    //            return TypeSatuan.JET;
    //        }
    //        else
    //        {
    //            return TypeSatuan.HELICOPTER;
    //        }
    //    }
    //    else if (obj.GetComponent<VehicleBundleData>() != null)
    //    {
    //        if(obj.GetComponent<VehicleBundleData>().tireType == VehicleBundleData.tType.DEFAULT)
    //        {
    //            return TypeSatuan.DEFAULT;
    //        }
    //        else
    //        {
    //            return TypeSatuan.TRACKS;
    //        }
    //    }else if(obj.GetComponent<ShipBundleData>() != null)
    //    {
    //        if(obj.GetComponent<ShipBundleData>().type == ShipBundleData.shipBundleType.SUB)
    //        {
    //            return TypeSatuan.SUB;
    //        }
    //        else
    //        {
    //            return TypeSatuan.SURFACE;
    //        }
    //    }

    //    return TypeSatuan.DEFAULT;
    //}

    public HUDNavigationElement CreateHUDElement(Transform parent, Color color, string label_nama = null, TMP_FontAsset font_taktis = null, string simbol_taktis = null)
    {
        var HUD = Instantiate(AssetPackageController.Instance.prefabHUD, parent).GetComponent<HUDNavigationElement>();
        HUD.name = "HUD_ELEMENT";
        HUD.tag = "simbol-taktis";

        HUD.Prefabs.IndicatorPrefab = AssetPackageController.Instance.prefabIndicator;

        HUD.OnElementReady.AddListener(delegate { onHUDReady(HUD, label_nama, font_taktis, simbol_taktis, color); });

        return HUD;
    }

    public HUDNavigationElement CreateHUDElementV2(Transform parent, Color color, string label, TMP_FontAsset fontTaktis = null, TMP_FontAsset fontTaktisBG = null, string simbolTaktis = null)
    {
        //var bounding = GetBoundingBox(parent.GetComponent<WGSBundleCore>().parent.gameObject);

        var HUD = Instantiate(AssetPackageController.Instance.prefabHUD, parent).GetComponent<HUDNavigationElement>();
        HUD.name = "HUD_ELEMENT";
        HUD.tag = "simbol-taktis";
        //HUD.transform.localPosition = new Vector3(0, bounding.extents.y, 0);

        HUD.Prefabs.IndicatorPrefab = AssetPackageController.Instance.prefabIndicator;
        HUD.OnElementReady.AddListener(delegate { onHUDReadyV2(HUD, label, fontTaktis, fontTaktisBG, simbolTaktis, color); });
        return HUD;

    }

    public void onHUDReady(HUDNavigationElement HUD, string nama, TMP_FontAsset font_taktis, string simbol_taktis, Color warna)
    {
        // Resetting data HUDElement setiap kali reload element
        HUD.Indicator.ChangeLabelEntity(nama);
        HUD.Indicator.ToggleCustomTransform("simbol_taktis", true);
        HUD.Indicator.ToggleCustomTransform("simbol_taktis_bg", true);
        HUD.Indicator.ChangeSimbolTaktis(font_taktis, simbol_taktis);

        HUD.Indicator.ChangeLabelBackground(warna);
    }

    public void onHUDReadyV2(HUDNavigationElement HUD, string nama, TMP_FontAsset fontTaktis, TMP_FontAsset fontTaktisBG, string simbolTaktis, Color warna)
    {
        // Resetting data HUDElement setiap kali reload element
        HUD.Indicator.ChangeLabelEntity(nama);
        HUD.Indicator.ToggleCustomTransform("simbol_taktis", true);
        HUD.Indicator.ToggleCustomTransform("simbol_taktis_bg", true);
        HUD.Indicator.ChangeSimbolTaktis(fontTaktis, simbolTaktis, fontTaktisBG);

        if (fontTaktisBG != null)
        {
            HUD.Indicator.ChangeSimbolTaktisBackground(warna);
        }
        else
        {
            HUD.Indicator.ChangeSimbolTaktisColor(warna);
        }
    }

    public OnlineMapsMarker3D spawnTacticalMapSymbol(Vector3 coordinate, ObjekSatuan satuan)
    {
        var tacmapMarker = OnlineMapsMarker3DManager.CreateItem(coordinate, AssetPackageController.Instance.prefabHUDTacmap.gameObject);

        var HUD = tacmapMarker.instance.GetComponent<HUDNavigationElement>();
        HUD.name = "tacmap_" + satuan.nama;
        HUD.tag = "simbol-taktis-tacmap";

        HUD.Prefabs.IndicatorPrefab = AssetPackageController.Instance.prefabIndicatorTacmap;

        Sprite iconTaktis = null;
        switch (satuan.GetComponent<WGSBundleCore>().bundleType)
        {
            case WGSBundleCore.BundleType.LAND:
                iconTaktis = TacticalMapController.instance.iconVehicle;
                break;
            case WGSBundleCore.BundleType.SEA:
                iconTaktis = TacticalMapController.instance.iconShip;
                break;
            case WGSBundleCore.BundleType.AIR:
                iconTaktis = TacticalMapController.instance.iconAircraft;
                break;
            default:
                iconTaktis = TacticalMapController.instance.iconVehicle;
                break;
        }

        //var iconTaktis = (satuan.GetComponent<WGSBundleCore>().bundleType != null) ? TacticalMapController.instance.iconAircraft : (satuan.GetComponent<ShipBundleData>() != null) ? TacticalMapController.instance.iconShip : TacticalMapController.instance.iconVehicle;

        HUD.OnElementReady.AddListener(delegate { onHUDTacmapReady(HUD, satuan.nama, iconTaktis); });

        satuan.hudElementTacmap = HUD;

        return tacmapMarker;
    }

    public void onHUDTacmapReady(HUDNavigationElement HUD, string nama, Sprite iconTaktis)
    {
        // Resetting data HUDElement Tactical Map setiap kali reload element
        HUD.Indicator.ChangeLabelEntity(nama);
        HUD.Indicator.ChangeIcon(iconTaktis);
    }

    private Sprite getIconByEntityType(JenisKendaraan type)
    {
        return null;
        switch (type)
        {
            case JenisKendaraan.DEFAULT:

                break;
            case JenisKendaraan.TRACKS:

                break;
            case JenisKendaraan.SURFACE:

                break;
            case JenisKendaraan.SUB:

                break;
            case JenisKendaraan.JET:

                break;
            case JenisKendaraan.PROPS:

                break;
            case JenisKendaraan.HELICOPTER:

                break;
            default:

                break;
        }
    }

    private string getFontCharacter(long index)
    {
        return ((char)index).ToString();
    }

    private Color getFontTaktisColor(string warna)
    {
        Color colorParse;
        if (warna == "blue")
        {
            if (ColorUtility.TryParseHtmlString("#81E0FF96", out colorParse)) {
                return colorParse;
            }

            return Color.blue;
        } else if (warna == "red")
        {
            if (ColorUtility.TryParseHtmlString("#FF808196", out colorParse)) {
                return colorParse;
            }

            return Color.red;
        }
        else if (warna == "" || warna == null)
        {
            return Color.white;
        }
        else
        {
            if (ColorUtility.TryParseHtmlString(warna, out colorParse))
            {
                return colorParse;
            }
            else
            {
                return Color.black;
            }
        }
    }

    public KategoriSatuan GetJenisSatuan(string _type)
    {
        switch (_type)
        {
            case "vehicle": return KategoriSatuan.VEHICLE;
            case "ship": return KategoriSatuan.SHIP;
            case "aircraft": return KategoriSatuan.AIRCRAFT;
            case "pasukan": return KategoriSatuan.INFANTRY;
            default: return KategoriSatuan.VEHICLE;
        }
    }
    private GameObject GetAssetSatuan(ObjekSatuanHelper satuan)
    {
        // Dapatkan Asset 3D Satuan dari Asset Package Berdasarkan Nama Satuan atau Nama Object 3D Satuannya
        int index = -1;
        if (satuan.path_object_3d != null)
        {
            index = AssetPackageController.Instance.ASSETS_NAME.IndexOf(satuan.path_object_3d.ToLower());
        }
        else
        {
            index = AssetPackageController.Instance.ASSETS_NAME.IndexOf(satuan.info.namaSatuan.ToLower());
        }

        // Jika Tidak Terdapat Asset 3D dalam Asset Package, Gunakan Asset Default Berdasarkan Jenis Satuannya
        if (index == -1)
        {
            return
                satuan.kategori == KategoriSatuan.VEHICLE ? AssetPackageController.Instance.defaultVehicle :
                satuan.kategori == KategoriSatuan.SHIP ? AssetPackageController.Instance.defaultShip :
                satuan.kategori == KategoriSatuan.AIRCRAFT ? AssetPackageController.Instance.defaultAircraft :
                satuan.kategori == KategoriSatuan.HELICOPTER ? AssetPackageController.Instance.defaultHelicopter :
                satuan.kategori == KategoriSatuan.INFANTRY ? AssetPackageController.Instance.defaultPasukan :

                AssetPackageController.Instance.defaultVehicle;
        }

        return AssetPackageController.Instance.ASSETS[index].gameObject;
    }

    //private async Task SetDetailSatuan(EntitySatuan satuan)
    private async Task SetDetailSatuan(ObjekSatuanHelper satuan)
    {
        var data = await WargamingAPI.GetDetailSatuan(satuan.symbolID.ToString(), satuan.style.grup.ToString());
        if (data == null) return;

        switch (satuan.style.grup)
        {
            case 1:
                // Jika Jenis Satuan = Angkatan Darat
                var detailDarat = DetailSatuanDarat.FromJson(data);

                satuan.tipe_tni = detailDarat.tipe_tni;
                satuan.kategori = KategoriSatuan.VEHICLE;
                satuan.path_object_3d = detailDarat.OBJ.model3D;
                break;
            case 2:
                // Jika Jenis Satuan = Angkatan Laut
                var detailLaut = DetailSatuanLaut.FromJson(data);

                satuan.tipe_tni = detailLaut.tipe_tni;
                satuan.kategori = KategoriSatuan.SHIP;
                satuan.path_object_3d = detailLaut.OBJ.model3D;
                break;
            case 3:
                // Jika Jenis Satuan = Angkatan Udara
                var detailUdara = DetailSatuanUdara.FromJson(data);

                satuan.tipe_tni = detailUdara.tipe_tni;
                satuan.path_object_3d = detailUdara.OBJ.model3D;

                if (detailUdara.OBJ.type.HasValue)
                {
                    satuan.kategori = (detailUdara.OBJ.type != 6) ? KategoriSatuan.AIRCRAFT : KategoriSatuan.HELICOPTER;
                }
                else
                {
                    satuan.kategori = KategoriSatuan.AIRCRAFT;
                }

                break;
            case 10:
                var detailPasukan = DetailSatuanDarat.FromJson(data);

                satuan.tipe_tni = detailPasukan.tipe_tni;
                satuan.path_object_3d = detailPasukan.OBJ.modelTitan;

                break;
        }
    }
    //private float setSatuanHeight(EntitySatuan satuan)
    private float setSatuanHeight(ObjekSatuanHelper satuan)
    {

        switch (satuan.kategori)
        {
            case KategoriSatuan.VEHICLE:
                return 0;
            case KategoriSatuan.SHIP:
                return StartupConfig.settings.DEFAULT_OCEAN_HEIGHT.GetValueOrDefault(2);
            case KategoriSatuan.AIRCRAFT:
                return 0;
            //return StartupConfig.settings.DEFAULT_AIRCRAFT_ALTITUDE.GetValueOrDefault(4000);
            case KategoriSatuan.INFANTRY:
                return 0;
            default:
                return 0;
        }
    }

    private static Bounds GetBoundingBox(GameObject obj)
    {
        var renderers = obj.GetComponentsInChildren<Renderer>();
        if (renderers.Length == 0) return new Bounds(obj.transform.position, Vector3.zero);

        var b = renderers[0].bounds;
        foreach (Renderer r in renderers)
        {
            b.Encapsulate(r.bounds);
        }
        return b;
    }

    public static void GenerateBoundingBox(GameObject obj)
    {
        if (obj.GetComponent<BoxCollider>() != null || obj.GetComponent<MeshCollider>() != null) return;

        var bounding = GetBoundingBox(obj);

        var collider = obj.AddComponent<BoxCollider>();
        collider.center = bounding.center - obj.transform.position;
        collider.size = bounding.size;
    }

    public static void GenerateTerrainFollower(GameObject obj, FIMSpace.Basics.EFUpdateClock updateMode = FIMSpace.Basics.EFUpdateClock.Update)
    {
        if (obj.GetComponent<FGroundFitter>() != null) return;

        var terrainFollower = obj.AddComponent<FGroundFitter>();
        terrainFollower.FittingSpeed            = 5;
        terrainFollower.TotalSmoother           = 0;
        terrainFollower.GlueToGround            = true;
        terrainFollower.RaycastHeightOffset     = 10000;
        terrainFollower.RaycastCheckRange       = 20000;
        terrainFollower.UpdateClock             = updateMode;
    }

    private List<List<ObjekSatuanWeapon>> prepareSatuanWeapon(string json)
    {
        if (json == null || json == "") return new List<List<ObjekSatuanWeapon>>();

        Debug.Log(json);

        var result = ObjekSatuanWeapon.FromJson(json);
        return result;

        //try
        //{
            
        //}catch(Exception e)
        //{
        //    var result = ObjekSatuanWeapon.FromJson2(json);
        //    List<List<ObjekSatuanWeapon>> listResult = new List<List<ObjekSatuanWeapon>>();
        //    listResult.Add(result);

        //    return listResult;
        //}
    }

    public async Task<bool> spawnMisiSatuan(MisiSatuan mission)
    {
        if (mission == null) return false;
        if (mission.data_properties == null) return false;

        if(mission.jenis == "embar3d" || mission.jenis == "debar3d")
        {
            //await setupMisiEmbarkasiDebarkasi(mission);
            Debug.Log("Misi Embar3D Found");
            await SetupEmbarkDebarkRoute(mission);
            return true;
        }

        if (mission.data_properties.jalur == null) return false;

        // Create Route Path Points
        List<Vector3> GENERATE_PATH_JALUR = new List<Vector3>();

        // Add Jalur Antara Misi Sebelumnya ke Titik Awal Pada Misi Selanjutnya
        var satuan = SATUAN.FirstOrDefault(x => x.nama == mission.id_object);
        if (satuan == null) return false;

        //var satuanWalker    = satuan.GetComponent<EntityWalkerV2>();
        var satuanData      = satuan.GetComponent<ObjekSatuan>();

        //if (satuanWalker == null) return false;

        for (int j = 0; j < mission.data_properties.jalur.Count; j++)
        {
            float altitude = mission.data_properties.jalur[j].alt.GetValueOrDefault(3);
            switch (satuanData.kategori)
            {
                case KategoriSatuan.SHIP:
                    altitude = StartupConfig.settings.DEFAULT_OCEAN_HEIGHT.GetValueOrDefault(3);
                    break;
                case KategoriSatuan.AIRCRAFT:
                    altitude = StartupConfig.settings.DEFAULT_AIRCRAFT_ALTITUDE.GetValueOrDefault(4000);
                    break;
                case KategoriSatuan.HELICOPTER:
                    altitude = 3048;
                    break;
            }

            GENERATE_PATH_JALUR.Add(new Vector3(mission.data_properties.jalur[j].lng, mission.data_properties.jalur[j].lat, altitude));
        }

        // Create Mission Object and Data
        var route = await MissionController.instance.AddNewRouteV4(GENERATE_PATH_JALUR, satuan.gameObject, mission);
        if (route != null)
        {
            // Add misi ke dalam data misi pada object satuannya

            var satuanWalker = satuan.GetComponent<RootWalkerBase>();
            satuanWalker.missions.Add(route);

            //satuanWalker.missions.Add(route);
            //satuanWalker.init();

            route.gameObject.SetActive(false);

            await Task.Delay(10);
            route.gameObject.SetActive(true);

            //return true;
            //if(satuan.GetComponent<NodeAircraftEngine>() != null)
            //{
            //    var satuanPos = satuan.GetComponent<ArcGISLocationComponent>().Position;

            //    var refTempTakeoffLanding = CreateEntity(new Vector3((float)satuanPos.X, (float)satuanPos.Y, (float)satuanPos.Z), Vector3.zero, AssetPackageController.Instance.prefabGuidance.gameObject).GetComponent<SplineController>();
            //    refTempTakeoffLanding.Spline = route.GetComponent<CurvySpline>();
            //    refTempTakeoffLanding.AbsolutePosition = 2512;

            //    await Task.Delay(100);
            //    var tempRefTakeoffPos = refTempTakeoffLanding.GetComponent<ArcGISLocationComponent>().Position;
            //    var tempRefTakeoffAlt = await WargamingAPI.OpenTopoDataRequest((float)tempRefTakeoffPos.Y, (float)tempRefTakeoffPos.X);

            //    refTempTakeoffLanding.AbsolutePosition = refTempTakeoffLanding.Length - 2512;

            //    await Task.Delay(100);
            //    var tempRefLandingPos = refTempTakeoffLanding.GetComponent<ArcGISLocationComponent>().Position;
            //    //var tempRefLandingAlt = await WargamingAPI.OpenTopoDataRequest((float)tempRefLandingPos.Y, (float)tempRefLandingPos.X);


            //    route.takeoffAlt = tempRefTakeoffAlt.results[0].elevation;
            //    route.landingAlt = 15;

            //    Destroy(refTempTakeoffLanding.gameObject);

            //    //tempRefTakeoff.GetComponent<ArcGISLocationComponent>().Position = new ArcGISPoint(tempRefTakeoffPos.X, tempRefTakeoffPos.Y, tempRefTakeoffAlt.results[0].elevation);
            //}

            // Setup state embarkasi & debarkasi di awal (agar satuan hilang jika status sedang embarkasi & vice-versa)
            //if(route.jenis == MissionWalker.RouteType.Embarkasi)
            //{
            //    EmbarkEntity(satuanWalker.gameObject);
            //}
            //else if(route.jenis == MissionWalker.RouteType.Debarkasi)
            //{
            //    DebarkEntity(satuanWalker.gameObject);
            //}

            //route.GetComponent<CurvySpline>().orientationRefresh = false;
        }

        return true;
    }

    public async Task<bool> spawnRootMisiSatuan(MisiSatuan mission)
    {
        if (mission == null) return false;
        //if (mission.data_properties == null) return false;

        //if (mission.data_properties.jalur == null) return false;

        if (mission.data_properties.typeMisi == "embar3d" || mission.data_properties.typeMisi == "debar3d")
        {
            //await setupMisiEmbarkasiDebarkasi(mission);
            //Debug.Log("Misi Embar3D Found");
            await SetupEmbarkDebarkRoute(mission);
            return true;
        }

        if(mission.data_properties.typeMisi == "landingOnShip")
        {
            //Debug.Log("Misi Landing On Ship Found");
            //await SetupTakeoffLandingOnTarget(mission);

            //return true;
        }

        if(mission.jenis == "linud")
        {
            Debug.Log("Misi LINUD Found");

            await SetupLinudRoute(mission);
            return true;
        }else if(mission.jenis == "blo")
        {
            Debug.Log("Misi BLO Found");

            return true;
        }

        // Create Route Path Points
        List<Vector3> GENERATE_PATH_JALUR = new List<Vector3>();

        // Add Jalur Antara Misi Sebelumnya ke Titik Awal Pada Misi Selanjutnya
        var satuan = SATUAN.FirstOrDefault(x => x.nama == mission.id_object);
        if (satuan == null) return false;

        //var rootTransform = satuan.GetComponent<RootSync>().rootTransform;
        var satuanData = satuan.GetComponent<ObjekSatuan>();

        //if (rootTransform == null) return false;
        if (satuan == null) return false;

        for (int j = 0; j < mission.data_properties.jalur.Count; j++)
        {
            float altitude = mission.data_properties.jalur[j].alt.GetValueOrDefault(3);
            switch (satuanData.kategori)
            {
                case KategoriSatuan.SHIP:
                    altitude = StartupConfig.settings.DEFAULT_OCEAN_HEIGHT.GetValueOrDefault(3);
                    break;
                case KategoriSatuan.AIRCRAFT:
                    altitude = StartupConfig.settings.DEFAULT_AIRCRAFT_ALTITUDE.GetValueOrDefault(4000);
                    break;
                case KategoriSatuan.HELICOPTER:
                    altitude = 3048;
                    break;
            }

            GENERATE_PATH_JALUR.Add(new Vector3(mission.data_properties.jalur[j].lng, mission.data_properties.jalur[j].lat, altitude));
        }

        // Create Mission Object and Data
        var route = await MissionController.instance.AddNewRouteV4(GENERATE_PATH_JALUR, satuan.gameObject, mission);
        if (route != null)
        {
            // Add misi ke dalam data misi pada object satuannya
            satuan.GetComponent<RootWalkerBase>().missions.Add(route);
            //rootTransform.GetComponent<RootWalkerBase>().missions.Add(route);

            route.gameObject.SetActive(false);

            await Task.Delay(10);
            route.gameObject.SetActive(true);
        }

        return true;
    }

    public async Task<bool> SetupEmbarkDebarkRoute(MisiSatuan mission)
    {
        var satuan = SATUAN.FirstOrDefault(x => x.nama == mission.data_properties.objek);
        if (satuan == null) return false;

        var target = SATUAN.FirstOrDefault(x => x.nama == mission.id_object);
        if (target == null || target.GetComponent<NodeCargoHold>() == null) return false;

        var walker = satuan.GetComponent<RootWalkerBase>();
        if (walker == null || walker.missions == null || walker.missions.Count <= 0) return false;

        var missionToOverwrite = walker.missions.FirstOrDefault(x => x.id == mission.data_properties.misiEmDeb);
        if (missionToOverwrite == null) return false;

        if(mission.data_properties.typeMisi == "embar3d")
        {
            await MissionController.instance.GenerateEmbarkasiPath(target.gameObject, walker.gameObject, missionToOverwrite);
        }
        else if(mission.data_properties.typeMisi == "debar3d")
        {
            await MissionController.instance.GenerateDebarkasiPath(target.gameObject, walker.gameObject, missionToOverwrite);
        }

        return true;
    }

    public async Task<bool> SetupTakeoffLandingOnTarget(MisiSatuan mission)
    {
        var satuan = SATUAN.FirstOrDefault(x => x.nama == mission.data_properties.objek);
        if (satuan == null) return false;

        var target = SATUAN.FirstOrDefault(x => x.nama == mission.id_object);
        if (target == null || target.GetComponent<NodeHelipad>() == null) return false;

        var walker = satuan.GetComponent<RootWalkerBase>();
        if (walker == null || walker.missions == null || walker.missions.Count <= 0) return false;

        var missionToOverwrite = walker.missions.FirstOrDefault(x => x.id == mission.data_properties.misiEmDeb);
        if (missionToOverwrite == null) return false;

        if (mission.data_properties.typeMisi == "landingOnShip")
        {
            await MissionController.instance.GenerateLandingOnTargetPath(target.gameObject, walker.gameObject, missionToOverwrite);
        }
        
        return true;
    }

    public async Task<bool> SetupLinudRoute(MisiSatuan mission)
    {
        await Task.Delay(10);

        var targetMission = MissionController.instance.OBJ_MISSION_V2.FirstOrDefault(x => x.missionId == mission.data_properties.misi_utama);
        if (targetMission == null) return false;

        for (int i=0; i < mission.data_properties.index_objek.Length; i++)
        {
            var paratroopMission    = await MissionController.instance.GenerateLinudPath(targetMission, mission, i);
            var paratrooper         = Instantiate(AssetPackageController.Instance.prefabParatrooper, satuanContainer);
            paratrooper.name        = "LINUD_" + targetMission + "_" + i;

            // Add misi ke dalam data misi pada object paratrooper-nya
            paratrooper.AddComponent<ParatroopWalker>();

            var paratroopSpline = paratrooper.AddComponent<SplineController>();
            paratroopSpline.OrientationAxis = OrientationAxisEnum.Forward;

            paratrooper.GetComponent<RootWalkerBase>().entity = paratrooper;
            paratrooper.GetComponent<RootWalkerBase>().missions.Add(paratroopMission);
            paratroopMission.gameObject.SetActive(false);

            await Task.Delay(10);
            paratroopMission.gameObject.SetActive(true);

            paratrooper.GetComponent<ParatroopWalker>().Init();
        }

        return true;
    }

    //public async Task<bool> setupMisiEmbarkasiDebarkasi(MisiSatuan mission)
    //{
    //    var satuan = SATUAN.FirstOrDefault(x => x.nama == mission.data_properties.objek);
    //    if (satuan == null) return false;

    //    var satuanParent = SATUAN.FirstOrDefault(x => x.nama == mission.id_object);
    //    if (satuanParent == null) return false;
    //    if (satuanParent.GetComponent<NodeCargoHold>() == null) return false;

    //    var entityWalker = satuan.GetComponent<EntityWalkerV2>();
    //    if (entityWalker == null) return false;

    //    for(int i=0; i < entityWalker.missions.Count; i++)
    //    {
    //        if (!entityWalker.missions[i].id.Equals(mission.data_properties.misiEmDeb)) continue;

    //        if (mission.jenis == "embar3d")
    //        {
    //            //var debarEmbarWalker = entityWalker.mission[i].gameObject.AddComponent<EmbarkWalker>();
    //            //entityWalker.missions[i].parent = satuanParent.GetComponent<NodeCargoHold>();
    //            await MissionController.instance.GenerateEmbarkasiPath(satuanParent.gameObject, entityWalker.gameObject, entityWalker.missions[i]);

    //            //debarEmbarWalker.objParent = satuanParent.gameObject;
    //            //debarEmbarWalker.objWalker = satuan.gameObject;
    //        }
    //        else if (mission.jenis == "debar3d")
    //        {
    //            //var debarEmbarWalker        = entityWalker.mission[i].gameObject.AddComponent<DebarkWalker>();

    //            //entityWalker.missions[i].parent = satuanParent.GetComponent<NodeCargoHold>();
    //            await MissionController.instance.GenerateDebarkasiPath(satuanParent.gameObject, entityWalker.gameObject, entityWalker.missions[i]);

    //            //debarEmbarWalker.objParent  = satuanParent.gameObject;
    //            //debarEmbarWalker.objWalker  = satuan.gameObject;
    //        }
    //    }

    //    return true;
    //}

    /// <summary>
    /// Create Paratroops Unit With It's Own Drop Route
    /// </summary>
    /// <param name="parent"></param>
    /// <param name="dataPenerjunan"></param>
    public void CreateParatroopsUnit(EntityWalkerV2 parent, SendPenerjunan dataPenerjunan)
    {
        var parentMissions = parent.missions;
        MissionWalker selectedMission = null;

        // Ambil kegiatan dari parentnya yang akan digunakan sebagai referensi lokasi tempat drop objectnya
        var waktuPenerjunan = DateTime.Parse(dataPenerjunan.startTime);
        for (int i = parentMissions.Count-1; i >= 0; i--)
        {
            if(waktuPenerjunan >= parentMissions[i].DATA[0].datetime)
            {
                selectedMission = parentMissions[i];
                i = 0;
                continue;
            }
        }

        if (selectedMission == null) return;

        var startDate   = selectedMission.DATA[0].datetime;
        var endDate     = selectedMission.DATA[selectedMission.DATA.Count - 1].datetime;
        var totalTime   = (endDate - startDate).TotalSeconds;

        // Spawn object paratroops dan konfigurasi datanya
        for (int i=0; i < dataPenerjunan.obj_list.Length; i++)
        {
            var objName = "paratroops_obj_" + dataPenerjunan.obj_list[i].id + "_" + dataPenerjunan.id_induk;

            // Jika object paratroops sudah di generate, skip...    
            if (DATA_PARATROOPS.FindIndex(x => x.name == objName) != -1) continue;

            // Jarak waktu antar object penerjunan (jika lebih dari 1) ditambah (X * 2) milisecond berdasarkan kecepatan parentnya
            //waktuPenerjunan = waktuPenerjunan.AddMilliseconds(selectedMission.speed);
            dataPenerjunan.startTime = waktuPenerjunan.ToString();

            //Debug.Log((selectedMission.speed * 2) + (selectedMission.speed * (i + 1)));

            var distanceMiliseconds = selectedMission.speed / 4;
            waktuPenerjunan = waktuPenerjunan.AddMilliseconds((distanceMiliseconds < 100) ? 100 : distanceMiliseconds);

            // Kalkulasi posisi absolute dari object parentnya di mana object akan berada ketika waktu penerjunan masuk
            var progress    = ((waktuPenerjunan.Subtract(startDate).TotalSeconds) / totalTime);
            var absPos      = (float)(selectedMission.GetComponent<CurvySpline>().Length * (progress * 100) / 100);

            var objParatroops   = CreateEntity(parent.GetComponent<ArcGISLocationComponent>(), AssetPackageController.Instance.prefabParatrooper);
            objParatroops.name  = objName;
            objParatroops.tag   = "entity";

            var paratroopsSpline                = objParatroops.AddComponent<SplineController>();
            paratroopsSpline.Spline             = selectedMission.GetComponent<CurvySpline>();
            paratroopsSpline.AbsolutePosition   = absPos;
            paratroopsSpline.MotionConstraints  = MotionConstraints.FreezePositionY;

            var objParentData           = parent.GetComponent<ObjekSatuan>();

            //var objData                 = objParatroops.AddComponent<DataSatuan>();
            //objData.isActive            = true;
            //objData.author              = objParentData.author;
            //objData.id                  = objName;
            //objData.kategori            = KategoriSatuan.INFANTRY;
            ////objData.type                = GetTypeSatuan(OBJ.GetComponent<WGSBundleCore>().bundleType);
            //objData.namaSatuan          = dataPenerjunan.obj_list[i].name;
            //objData.descSatuan          = dataPenerjunan.obj_list[i].keterangan;
            //objData.fontTaktis          = dataPenerjunan.obj_list[i].font_taktis;
            //objData.simbolTaktis        = ((char)dataPenerjunan.obj_list[i].font_ascii).ToString();
            //objData.simbolTaktisColor   = objParentData.simbolTaktisColor;
            //objData.hudElement          = CreateHUDElement(
            //    objParatroops.transform,
            //    objData.simbolTaktisColor,
            //    objData.namaSatuan,
            //    getFontTaktis(objData.fontTaktis),
            //    objData.simbolTaktis
            //);

            var objData = objParatroops.AddComponent<ObjekSatuan>();
            objData.userID = objParentData.userID;
            objData.documentID = objParentData.documentID;
            objData.nama = objName;
            objData.kategori = KategoriSatuan.INFANTRY;
            objData.symbolID = -99; // UNKNOWN

            objData.style = new ObjekSatuanStyle
            {
                fontIndex = dataPenerjunan.obj_list[i].font_ascii.GetValueOrDefault(0),
                fontTaktis = dataPenerjunan.obj_list[i].font_taktis,
                grup = long.Parse(dataPenerjunan.obj_list[i].grup),
                keterangan = dataPenerjunan.obj_list[i].keterangan
            };

            objData.symbol = new ObjekSymbol
            {
                fontIndex = ((char)dataPenerjunan.obj_list[i].font_ascii).ToString(),
                fontFamily = dataPenerjunan.obj_list[i].font_taktis,
                warna = objParentData.symbol.warna
            };

            objData.info = new ObjekSatuanInfo
            {
                kecepatan = dataPenerjunan.obj_list[i].speed_pasukan.GetValueOrDefault(0),
                jenisKecepatan = SpeedHelper.speedType.kmph,
                namaSatuan = dataPenerjunan.obj_list[i].name,
                warna = objParentData.symbol.warna,
                keterangan = dataPenerjunan.obj_list[i].keterangan,
                personil = dataPenerjunan.obj_list[i].jumlah_bintara.GetValueOrDefault(0) + dataPenerjunan.obj_list[i].jumlah_tamtama.GetValueOrDefault(0) + dataPenerjunan.obj_list[i].jumlah_perwira.GetValueOrDefault(0),
                detailPersonil = new ObjekSatuanPersonil
                {
                    bintara = dataPenerjunan.obj_list[i].jumlah_bintara.GetValueOrDefault(0),
                    tamtama = dataPenerjunan.obj_list[i].jumlah_tamtama.GetValueOrDefault(0),
                    perwira = dataPenerjunan.obj_list[i].jumlah_perwira.GetValueOrDefault(0)
                },
                useInTfg = true,
                hideInWgs = false
            };

            objData.hudElement = CreateHUDElementV2(
                objParatroops.transform,
                objData.symbol.warna,
                objData.nama,
                getFontTaktis(objData.symbol.fontFamily),
                getFontTaktis(objData.symbol.fontFamily + "_BG"),
                objData.symbol.fontIndex
            );

            //objWalker.tacmapMarker      = spawnTacticalMapSymbol(obj_position, objData);

            objData.hudElement.transform.parent = objParatroops.GetComponent<WGSBundleCore>().parent;
            objData.hudElement.transform.localPosition = Vector3.zero;

            var objParatroopsController = objParatroops.AddComponent<ParatroopsWalker>();
            objParatroopsController.Init(parent, waktuPenerjunan.AddMilliseconds(selectedMission.speed));

            DATA_PARATROOPS.Add(objParatroopsController);
        }
    }

    public void EmbarkEntity(GameObject satuan)
    {
        Debug.Log("Embark Entiity " + satuan.name);

        satuan.GetComponent<WGSBundleCore>().parent.gameObject.SetActive(false);

        var dynamicWave = satuan.GetComponent<NodeDynamicWave>();
        //if (dynamicWave != null)
        //{
        //    for(int i=0; i < dynamicWave.nodes.Count; i++)
        //    {
        //        if (dynamicWave.nodes[i].node == null) continue;
        //        dynamicWave.nodes[i].node.gameObject.SetActive(false);
        //    }
        //}

        //if (satuan.GetComponent<ShipBundleData>() != null)
        //{
        //    if (satuan.GetComponent<ShipBundleData>().nodeWakeGenerator != null)
        //    {
        //        satuan.GetComponent<ShipBundleData>().nodeWakeGenerator.SetActive(false);
        //    }
        //}

        //if (satuan.GetComponent<VehicleBundleData>() != null)
        //{
        //    satuan.GetComponent<VehicleBundleData>().parent.SetActive(false);
        //}
        //else if (satuan.GetComponent<ShipBundleData>() != null)
        //{
        //    satuan.GetComponent<ShipBundleData>().parent.SetActive(false);
        //    satuan.GetComponent<DataSatuan>().hudElement.SetIndicatorActive(false);
        //}
        //else if (satuan.GetComponent<AircraftBundleData>() != null)
        //{
        //    satuan.GetComponent<AircraftBundleData>().parent.SetActive(false);
        //}

        // Toggle off Simbol Taktis
        satuan.GetComponent<ObjekSatuan>().hudElement.showIndicator = false;

        // Toggle off Marker Tactical Map
        //satuan.GetComponent<ObjekSatuan>().hudElementTacmap.SetIndicatorActive(false);
    }

    public void DebarkEntity(GameObject satuan)
    {
        Debug.Log("Debark Entiity " + satuan.name);

        satuan.GetComponent<WGSBundleCore>().parent.gameObject.SetActive(true);

        var dynamicWave = satuan.GetComponent<NodeDynamicWave>();
        //if (dynamicWave != null)
        //{
        //    for (int i = 0; i < dynamicWave.nodes.Count; i++)
        //    {
        //        if (dynamicWave.nodes[i].node == null) continue;
        //        dynamicWave.nodes[i].node.gameObject.SetActive(true);
        //    }
        //}

        //if (satuan.GetComponent<ShipBundleData>() != null)
        //{
        //    if (satuan.GetComponent<ShipBundleData>().nodeWakeGenerator != null)
        //    {
        //        satuan.GetComponent<ShipBundleData>().nodeWakeGenerator.SetActive(true);
        //    }
        //}

        //if (satuan.GetComponent<VehicleBundleData>() != null)
        //{
        //    satuan.GetComponent<VehicleBundleData>().parent.SetActive(true);
        //}
        //else if (satuan.GetComponent<ShipBundleData>() != null)
        //{
        //    satuan.GetComponent<ShipBundleData>().parent.SetActive(true);
        //}
        //else if (satuan.GetComponent<AircraftBundleData>() != null)
        //{
        //    satuan.GetComponent<AircraftBundleData>().parent.SetActive(true);
        //}

        // Toggle On Simbol Taktis
        satuan.GetComponent<ObjekSatuan>().hudElement.showIndicator = true;

        // Toggle On Marker Tactical Map
        //satuan.GetComponent<ObjekSatuan>().hudElementTacmap.SetIndicatorActive(true);
    }

    public void RemoveEntityFromCB(string id_user)
    {
        List<string> entityDelete = new List<string>();
        List<ObjekSatuan> objSatuanToDelete = new List<ObjekSatuan>();
        List<ObjekDrawing> objDrawingToDelete = new List<ObjekDrawing>();

        for (int i=0; i < SATUAN.Count; i++)
        {
            if(SATUAN[i].userID != long.Parse(id_user)) continue;

            entityDelete.Add(SATUAN[i].nama);
            objSatuanToDelete.Add(SATUAN[i]);

            var ruteSatuan = SATUAN[i].GetComponent<RootWalkerBase>().missions;
            if(ruteSatuan != null)
            {
                for (int j = 0; j < ruteSatuan.Count; j++)
                {
                    MissionController.instance.OBJ_MISSION_V2.Remove(ruteSatuan[j]);
                    Destroy(ruteSatuan[j].gameObject);
                }
            }
        }

        for(int i=0; i < DRAWING.Count; i++)
        {
            if (DRAWING[i].userID != long.Parse(id_user)) continue;

            objDrawingToDelete.Add(DRAWING[i]);
        }

        for(int i=0; i < objSatuanToDelete.Count; i++)
        {
            SATUAN.Remove(objSatuanToDelete[i]);
            VEHICLE.Remove(objSatuanToDelete[i]);
            SHIP.Remove(objSatuanToDelete[i]);
            SUBMARINE.Remove(objSatuanToDelete[i]);
            AIRCRAFT.Remove(objSatuanToDelete[i]);
            HELICOPTER.Remove(objSatuanToDelete[i]);
            INFANTRY.Remove(objSatuanToDelete[i]);

            Destroy(objSatuanToDelete[i].gameObject);
        }

        for(int i=0; i < objDrawingToDelete.Count; i++)
        {
            DRAWING.Remove(objDrawingToDelete[i]);
        }

        ListEntityManager.instance.RemoveData(entityDelete);
    }

    public void configureBundlePackage(List<ObjekSatuan> listSatuan)
    {
        for(int i=0; i < listSatuan.Count; i++)
        {
            //var OBJ = listSatuan[i].GetComponent<RootSync>();
            var OBJ = listSatuan[i].gameObject;
            //var RootWalker = OBJ.rootTransform.GetComponent<RootWalker>();
            //var OBJLocation = OBJ.GetComponent<ArcGISLocationComponent>();

            if (listSatuan[i].kategori == KategoriSatuan.SHIP || listSatuan[i].kategori == KategoriSatuan.SUBMARINE)
            {
                // var rootWalker = OBJ.rootTransform.GetComponent<SRootWalker>();
                var rootWalker = OBJ.GetComponent<SRootWalker>();

                var walkerSound = OBJ.gameObject.AddComponent<WalkerSound>();
                walkerSound.walker = rootWalker;
                walkerSound.type = WalkerSound.EntityType.Ship;
            }
            else if (listSatuan[i].kategori == KategoriSatuan.AIRCRAFT || listSatuan[i].kategori == KategoriSatuan.HELICOPTER)
            {
                //var rootWalker = OBJ.rootTransform.GetComponent<ARootWalker>();
                var rootWalker = OBJ.GetComponent<ARootWalker>();
                rootWalker.guidance = Instantiate(AssetPackageController.Instance.prefabRootGuidance, guidanceContainer).GetComponent<SplineController>();
                rootWalker.guidance.name = "Guidance_" + OBJ.name;
                rootWalker.guidance.MotionConstraints = MotionConstraints.FreezePositionY;

                rootWalker.guidance.transform.SetRootOrientation(new Vector3(0, 90, 0));

                rootWalker.sensitivity = 10;
                rootWalker.guidanceDistance = 1000;
                rootWalker.groundFitter = Instantiate(AssetPackageController.Instance.prefabAltitudeGuidance, altitudeGuidanceContainer).GetComponent<FGroundFitter>();
                rootWalker.groundFitter.name = "GroundFitter_" + OBJ.name;

                GenerateTerrainFollower(rootWalker.gameObject);
                rootWalker.rootGroundFitter = rootWalker.gameObject.GetComponent<FGroundFitter>();

                var walkerSound = OBJ.gameObject.AddComponent<WalkerSound>();
                walkerSound.walker = rootWalker;
                walkerSound.type = WalkerSound.EntityType.Aircraft;
            }
            else if (listSatuan[i].kategori == KategoriSatuan.VEHICLE)
            {
                //var rootWalker = OBJ.rootTransform.GetComponent<LRootWalker>();
                var rootWalker = OBJ.GetComponent<LRootWalker>();

                var walkerSound = OBJ.gameObject.AddComponent<WalkerSound>();
                walkerSound.walker = rootWalker;
                walkerSound.type = WalkerSound.EntityType.Vehicle;

                //var rootWalker = OBJ.rootTransform.GetComponent<RootWalker>();

                //RootWalker.guidance = Instantiate(AssetPackageController.Instance.prefabRootGuidance, guidanceContainer).GetComponent<SplineController>();
                //RootWalker.sensitivity = 10;
                //RootWalker.guidanceDistance = 1000;
                //RootWalker.guidance.MotionConstraints = MotionConstraints.FreezePositionY;

                //RootWalker.groundAdjuster = Instantiate(AssetPackageController.Instance.prefabAltitudeGuidance, altitudeGuidanceContainer).GetComponent<FGroundFitter>();

                //var wings = RootWalker.gameObject.AddComponent<DebugAircraftWing>();

                //wings.follower = RootWalker.guidance.transform;
                //wings.node = listSatuan[i].GetComponent<NodeAircraft>();
            }


            // Setup Aircraft or Helicopter Guidance
            //if (RootWalker != null && (listSatuan[i].kategori == KategoriSatuan.AIRCRAFT || listSatuan[i].kategori == KategoriSatuan.HELICOPTER))
            //{
            //    //RootWalker.guidance = Instantiate(AssetPackageController.Instance.prefabRootGuidance, guidanceContainer).GetComponent<SplineController>();
            //    //RootWalker.sensitivity = 10;
            //    //RootWalker.guidanceDistance = 1000;
            //    //RootWalker.guidance.MotionConstraints = MotionConstraints.FreezePositionY;

            //    //RootWalker.groundAdjuster = Instantiate(AssetPackageController.Instance.prefabAltitudeGuidance, altitudeGuidanceContainer).GetComponent<FGroundFitter>();

            //    //var wings = RootWalker.gameObject.AddComponent<DebugAircraftWing>();

            //    //wings.follower = RootWalker.guidance.transform;
            //    //wings.node = listSatuan[i].GetComponent<NodeAircraft>();
            //}

            // Setup Dynamic Wave Module
            AssetPackageController.ConfigDynamicWaveModule(OBJ.GetComponent<NodeDynamicWave>());
            //if (OBJ.GetComponent<NodeDynamicWave>() != null) OBJ.AddComponent<ShipFoamWatcher>();

            // -- Wheeled Vehicle Module --
            //AssetPackageController.ConfigWheelModule(OBJ.GetComponent<NodeWheeledVehicle>());

            // -- Animated Module --
            AssetPackageController.ConfigAnimatedModule(OBJ.GetComponent<NodeAnimated>());

            // -- Heli Rotor Module --
            AssetPackageController.ConfigHeliRotorModule(OBJ.GetComponent<NodeHeliRotor>());

            // -- Aircraft Module
            AssetPackageController.ConfigAircraftModule(OBJ.GetComponent<NodeAircraft>());

            // -- Aircraft Engine Module
            //AssetPackageController.ConfigAircraftEngineModule(OBJ.GetComponent<NodeAircraft>());

            // Setup VFX Module
            AssetPackageController.ConfigVFXModule(OBJ.GetComponent<NodeVFX>());

            // -- Cargo Hold Module --
            AssetPackageController.ConfigCargoHoldModule(OBJ.GetComponent<NodeCargoHold>());

            // Setup Engine Sound Module
            AssetPackageController.ConfigSoundModule(OBJ.gameObject);
        }
    }


    public async Task<bool> loadEntityRadar(JArray data)
    {
        LoadingController.instance.ShowLoading("loading-rt", "Loading Radar...");
        if (!JArrayExtended.checkingJArrayData(data)) return false;

        for (int i = 0; i < data.Count; i++)
        {
            Debug.Log("Spawn Radar " + i);
            Debug.Log(data[i].ToString());

            var satuan = ObjekRadarHelper.FromJson(data[i]);
            await spawnEntityRadar(satuan);
        }

        return true;
    }

    public async Task spawnEntityRadar(ObjekRadarHelper radar)
    {
        var objPosition = new ArcGISPoint(radar.lng_x, radar.lat_y, 0, ArcGISSpatialReference.WGS84());
        var objRotation = new ArcGISRotation(0, 90, 0);

        // Create Entity Berdasarkan Object 3D-nya
        var OBJ = CreateEntity(objPosition, objRotation, AssetPackageController.Instance.prefabRadar);
        OBJ.GetComponent<HPTransform>().LocalScale = new Unity.Mathematics.float3(radar.info.radius);

        OBJ.name = radar.nama;
        OBJ.tag = "entity";
        //OBJ.layer = LayerMask.NameToLayer("Entities");

        // Get Radar Author's Data
        var findAuthorData = PlayerData.PLAYERS.FindIndex(x => x.id == radar.id_user);

        PData authorData = null;
        if (findAuthorData != -1) authorData = PlayerData.PLAYERS[findAuthorData];

        var objData = OBJ.AddComponent<ObjekRadar>();
        objData.userID = radar.id_user.GetValueOrDefault(-99);
        objData.documentID = radar.dokumen.GetValueOrDefault(-99);
        //objData.symbolID = radar.symbolID;
        objData.nama = radar.nama;

        Debug.Log(radar.info);

        objData.jenis = radar.info.jenisRadar;

        var objInfo = new ObjekRadarInfo();
        objInfo.nama = radar.info.nama;
        objInfo.desc = radar.info.desc;
        objInfo.radius = radar.info.radius;
        objInfo.size = NumberConvertionHelper.ConvertToInt(radar.info.size);
        objInfo.warna = radar.info.warna;
        objInfo.author = radar.info.idUser.ToString();
        //objInfo.id_symbol = ???
        objInfo.jenis = objData.jenis;
        objInfo.ammo = radar.info.ammo;
        objInfo.newIndex = NumberConvertionHelper.ConvertToInt(radar.info.newIndex);
        objInfo.armor = radar.info.armor;
        objData.info = objInfo;

        var objSymbol = new ObjekSymbol();
        objSymbol.width = NumberConvertionHelper.ConvertToInt(radar.info.size);
        objSymbol.warna = objInfo.warna;
        //objSymbol.fontFamily = SATUAN;
        //objSymbol.fontIndex = getFontCharacter(satuan.style.fontIndex);
        //objData.symbol = objSymbol;

        //objData.hudElement = CreateHUDElementV2(
        //    OBJ.transform,
        //    objData.symbol.warna,
        //    satuan.info.namaSatuan,
        //    getFontTaktis(objData.symbol.fontFamily),
        //    getFontTaktis(objData.symbol.fontFamily + "_BG"),
        //    objData.symbol.fontIndex
        //);

        RADAR.Add(objData);

        //Sprite imageRadar = OBJ.GetComponent<WGSBundleCore>().icon;
        GenerateTerrainFollower(OBJ);

        // Add to List
        if (ListEntityManager.instance != null)
        {
            //ListEntityManager.instance.AddData(objData);
        }

        //OBJ.transform.localScale = new Vector3(radar.info.radius, radar.info.radius, radar.info.radius);
    }

    public JenisRadar GetJenisRadar(string _type)
    {
        switch (_type.ToLower())
        {
            case "biasa": return JenisRadar.BIASA;
            case "arhanud": return JenisRadar.ARHANUD;
            default: return JenisRadar.BIASA;
        }
    }

    public async Task<bool> loadText(JArray data, bool editorMode)
    {
        if (!StartupConfig.settings.ENABLE_LOAD_DRAWING) return true;

        LoadingController.instance.ShowLoading("loading-rt", "Loading Map Label...");
        if (!JArrayExtended.checkingJArrayData(data)) return false;

        for (int i = 0; i < data.Count; i++)
        {
            Debug.Log("Spawn Text " + i);
            Debug.Log(data[i].ToString());

            var textDrawing = ObjekTextHelper.FromJson(data[i]);
            if (editorMode)
            {
                //await spawnDrawing(textDrawing, editorMode);
            }
            else
            {
                //await spawnEntityDrawing(textDrawing);
            }
        }

        return true;
    }



    public async Task<bool> loadDrawing(JArray data, bool editorMode)
    {
        if (!StartupConfig.settings.ENABLE_LOAD_DRAWING) return true;
        //if (!editorMode) return true;

        LoadingController.instance.ShowLoading("loading-rt", "Loading Drawing...");
        if (!JArrayExtended.checkingJArrayData(data)) return false;

        for (int i = 0; i < data.Count; i++)
        {
            Debug.Log("Spawn Drawing " + i);
            Debug.Log(data[i].ToString());

            var drawing = ObjekDrawingHelper.FromJson(data[i]);
            if (editorMode)
            {
                await spawnDrawing(drawing, editorMode);
            }
            else
            {
                await spawnEntityDrawing(drawing);
            }
        }

        //DrawDist.instance.Init();

        return true;
    }

    public async Task spawnPolyline(ObjekDrawingHelper drawing, bool editorMode)
    {
        var id = (!editorMode) ? drawing.nama : drawing.nama;
        if (drawing == null) return;

        var properties  = drawing.properties;
        var geometry    = drawing.geometryPolyline.coordinates;

        var OBJ     = Instantiate(AssetPackageController.Instance.prefabPolyline.gameObject, EnvironmentsController.GetMapComponent().transform);
        OBJ.name    = id;
        OBJ.tag     = "tools";

        var color   = (!editorMode) ? properties.warna : properties.warna;
        color.a     = 1f;

        var objPolyline         = OBJ.GetComponent<Polyline>();
        objPolyline.Color       = color;
        objPolyline.Thickness   = (!editorMode) ? properties.weight : properties.weight;

        var DATA        = OBJ.AddComponent<ObjekDrawing>();
        SetDrawingGeneral(DATA, id, DrawingType.POLYLINE);

        DATA.properties = SetPolylineDrawProperty(properties.warna, properties.weight, properties.opacity, id);
        DRAWING.Add(DATA);

        // Create Segments
        for(int i=0; i < geometry.Count; i++)
        {
            int index = i;

            var newSegment = CreateNewDrawSegment(
                OBJ.transform,
                new ArcGISPoint(geometry[i][0], geometry[i][1], 0, ArcGISSpatialReference.WGS84())
            );

            newSegment.GetComponent<Cuboid>().Color = drawing.properties.warna;

            // Create New Segment's HUDElement
            var HUD = newSegment.GetComponent<HUDNavigationElement>();
            HUD.Prefabs.IndicatorPrefab = AssetPackageController.Instance.prefabIndicator;
            HUD.OnElementReady.AddListener(
                delegate {
                    HUD.Indicator.ChangeLabelEntity(index.ToString(), Color.black);
                }
            );

            newSegment.name = "SEGMENT_" + i;
            DATA.geometry.Add(newSegment);

            var segmentEditor = newSegment.GetComponent<ExposeToEditor>();
            if (!editorMode)
            {
                Destroy(segmentEditor);
                Destroy(newSegment.GetComponent<Cuboid>());
            }
            else
            {
                var plotController = PlotDrawingController.instance;
                if (plotController == null) continue;

                segmentEditor.Selected.AddListener(delegate
                {
                    if (!DrawingToolsController.instance.isOn) return;

                    plotController.Reset();
                    plotController.ChangeDrawingMode(DATA.type, false, "");
                    plotController.AddVertexData(DATA);

                    plotController.selectedDrawing = DATA;
                    plotController.FillFormData();
                });

                newSegment.GetComponent<Cuboid>().Color = properties.warna;
            }

            await Task.Delay(10);

            objPolyline.Closed         = false;
            objPolyline.meshOutOfDate  = true;

            if (i == 0)
            {
                objPolyline.SetPointPosition(0, OBJ.transform.GetChild(i).position);
            }
            else
            {
                objPolyline.AddPoint(OBJ.transform.GetChild(i).position);
            }
        }

        //GenerateBoundingBox(OBJ);
    }

    public void SetDrawingGeneral(ObjekDrawing OBJ, string id, DrawingType type)
    {
        OBJ.userID = SessionUser.id.GetValueOrDefault(-99);
        OBJ.documentID = NumberConvertionHelper.ConvertToLong(SkenarioAktif.ID_DOCUMENT);
        OBJ.nama = id;
        OBJ.type = type;
        OBJ.geometry = new List<GameObject>();
    }

    public EntityDrawingProperty SetPolylineDrawProperty(Color color, float weight, float opacity, string id_point)
    {
        var property = new EntityDrawingProperty();

        property.stroke = true;
        property.warna  = color;
        property.weight = weight;
        property.opacity = opacity;
        property.fill = false;
        property.clickable = true;
        property.dashArray = "0,0";
        property.lineJoin = "round";
        property.transform = true;
        property.id_point = id_point;

        return property;
    }

    public async Task spawnEntityDrawing(ObjekDrawingHelper drawing)
    {
        var properties = drawing.properties;
        if (properties == null) return;

        if(drawing.type == DrawingType.POLYLINE)
        {
            if(drawing.geometryPolyline != null)
            {
                await DrawingController.SpawnObjPolyline(drawing);
            }

        }else if(drawing.type == DrawingType.POLYGON)
        {
            await DrawingController.SpawnObjPolygon(drawing);
        }else if(drawing.type == DrawingType.RECTANGLE)
        {
            await DrawingController.SpawnObjRectangle(drawing);
        }else if(drawing.type == DrawingType.CIRCLE)
        {
            await DrawingController.SpawnObjCircle(drawing);
        }
    }

    public async Task spawnDrawing(ObjekDrawingHelper drawing, bool editorMode)
    {
        var properties = drawing.properties;
        if (properties == null) return;

        if(drawing.type == DrawingType.POLYLINE)
        {
            // Create Polyline Object
            await spawnPolyline(drawing, editorMode);
        }









        return;

        if (drawing.properties.showArea) drawing.type = DrawingType.RECTANGLE;

        if(drawing.properties.arrowType != DrawingArrowType.UNDEFINED)
        {
            drawing.type = DrawingType.ARROW;
        }

        ObjekDrawing objData = null;

        if (drawing.type == DrawingType.POLYLINE)
        {
            var coordinates = drawing.geometryPolyline.coordinates;

            // Create Polyline Object
            var OBJ = Instantiate(AssetPackageController.Instance.prefabPolyline, EnvironmentsController.GetMapComponent().transform);
            OBJ.name = drawing.nama;
            OBJ.tag = "entity";

            var color = drawing.properties.warna;
            color.a = Mathf.Clamp(drawing.properties.opacity,0,1);

            var objPolyline         = OBJ.GetComponent<Polyline>();
            objPolyline.Color       = color;
            objPolyline.Thickness   = drawing.properties.weight;

            objData = OBJ.gameObject.AddComponent<ObjekDrawing>();
            objPolyline.SetPointPosition(0, Vector3.zero);

            for (int i = 0; i < coordinates.Count; i++)
            {
                var newSegment = CreateNewDrawSegment(
                    OBJ.transform,
                    new ArcGISPoint(coordinates[i][0], coordinates[i][1], 0, ArcGISSpatialReference.WGS84())
                );

                newSegment.GetComponent<Cuboid>().Color = drawing.properties.warna;

                newSegment.name = "SEGMENT_" + i;
                objData.geometry.Add(newSegment);
            }

            await Task.Delay(10);
            objPolyline.meshOutOfDate = true;

            for (int i = 0; i < OBJ.transform.childCount; i++)
            {
                if (i == 0)
                {
                    objPolyline.SetPointPosition(0, OBJ.transform.GetChild(i).position);
                }
                else
                {
                    objPolyline.AddPoint(OBJ.transform.GetChild(i).position);
                }
            }
        }
        else if (drawing.type == DrawingType.POLYGON)
        {
            var drawCoordinates = drawing.geometryPolygon.coordinates;

            // Create Polygon Object
            var OBJ = Instantiate(AssetPackageController.Instance.prefabPolygon, EnvironmentsController.GetMapComponent().transform);
            OBJ.name = drawing.nama;
            OBJ.tag = "entity";

            var color = drawing.properties.warna;
            color.a = Mathf.Clamp(drawing.properties.opacity,0,1);

            var objOutline = OBJ.GetComponent<ObjekDrawPolygon>().outline;
            objOutline.Color = color;
            objOutline.Thickness = drawing.properties.weight;

            var fillColor = drawing.properties.fillColor;
            fillColor.a = Mathf.Clamp(drawing.properties.fillOpacity,0,1);

            var objPolygon = OBJ.GetComponent<ObjekDrawPolygon>().polygon;
            objPolygon.Color = fillColor;

            objData = OBJ.gameObject.AddComponent<ObjekDrawing>();
            objOutline.SetPointPosition(0, Vector3.zero);
            objPolygon.SetPointPosition(0, Vector3.zero);

            for (int i = 0; i < drawing.geometryPolygon.coordinates[0].Count; i++)
            {
                var newSegment = CreateNewDrawSegment(
                    OBJ.segments,
                    new ArcGISPoint(drawCoordinates[0][i][0], drawCoordinates[0][i][1], 0, ArcGISSpatialReference.WGS84())
                );

                objData.geometry.Add(newSegment);
            }

            await Task.Delay(10);
            objOutline.meshOutOfDate = true;
            objPolygon.meshOutOfDate = true;

            for (int i = 0; i < OBJ.segments.childCount - 1; i++)
            {
                if (i == 0)
                {
                    objPolygon.SetPointPosition(0, new Vector2(OBJ.segments.GetChild(i).position.x, OBJ.segments.GetChild(i).position.z));
                    objOutline.SetPointPosition(0, OBJ.segments.GetChild(i).position);
                }
                else
                {
                    objPolygon.AddPoint(new Vector2(OBJ.segments.GetChild(i).position.x, OBJ.segments.GetChild(i).position.z));
                    objOutline.AddPoint(OBJ.segments.GetChild(i).position);
                }
            }
        }
        else if (drawing.type == DrawingType.CIRCLE)
        {

            //var drawCoordinates = drawing.geometryPoint.coordinates;

            //var objPosition = new ArcGISPoint(drawCoordinates[0], drawCoordinates[1], 0, ArcGISSpatialReference.WGS84());

            //// Create Circle Object
            //var OBJ = CreateEntity(objPosition, objRotation, AssetPackageController.Instance.prefabCircle.gameObject, EnvironmentsController.GetMapComponent().transform);
            //OBJ.name = drawing.nama;
            //OBJ.tag = "entity";
            //objData = OBJ.gameObject.AddComponent<ObjekDrawing>();

            //var OBJCircle = OBJ.transform.GetChild(1).GetComponent<Disc>();
            //var OBJOutline = OBJ.transform.GetChild(0).GetComponent<Disc>();

            //SetCircleColor(OBJCircle, drawing.properties.fillColor, drawing.properties.fillOpacity);
            //SetCircleColor(OBJOutline, drawing.properties.warna, drawing.properties.opacity);

            //OBJOutline.Thickness = drawing.properties.weight;

            //OBJCircle.Radius = (float)drawing.properties.radius;
            //OBJOutline.Radius = (float)drawing.properties.radius + (drawing.properties.weight);
        }
        else if(drawing.type == DrawingType.RECTANGLE)
        {
            //var drawCoordinates = drawing.geometryPolygon.coordinates;

            //// Create Polygon Object
            //var OBJ = Instantiate(AssetPackageController.Instance.prefabRectangle, EnvironmentsController.GetMapComponent().transform);
            //OBJ.name = drawing.nama;
            //OBJ.tag = "entity";
            //objData = OBJ.gameObject.AddComponent<ObjekDrawing>();

            //var OBJPolygon = OBJ.polygon;
            //var OBJOutline = OBJ.outline;

            //OBJPolygon.SetPointPosition(0, Vector3.zero);
            //OBJOutline.SetPointPosition(0, Vector3.zero);

            //for (int i = 0; i < drawing.geometryPolygon.coordinates[0].Count; i++)
            //{
            //    var newSegment = CreateNewDrawSegment(
            //        new ArcGISPoint(drawCoordinates[0][i][0], drawCoordinates[0][i][1], 0, ArcGISSpatialReference.WGS84()),
            //        objRotation, OBJ.segments, drawing.nama, i
            //    );

            //    objData.geometry.Add(newSegment);
            //}

            //await Task.Delay(10);
            //OBJPolygon.meshOutOfDate = true;
            //OBJOutline.meshOutOfDate = true;

            //for (int i = 0; i < OBJ.segments.childCount - 1; i++)
            //{
            //    if (i == 0)
            //    {
            //        OBJPolygon.SetPointPosition(0, new Vector2(OBJ.segments.GetChild(i).position.x, OBJ.segments.GetChild(i).position.z));
            //        OBJOutline.SetPointPosition(0, OBJ.segments.GetChild(i).position);
            //    }
            //    else
            //    {
            //        OBJPolygon.AddPoint(new Vector2(OBJ.segments.GetChild(i).position.x, OBJ.segments.GetChild(i).position.z));
            //        OBJOutline.AddPoint(OBJ.segments.GetChild(i).position);
            //    }
            //}

            //SetPolygonColor(OBJPolygon, drawing.properties.fillColor, drawing.properties.fillOpacity);
            //SetPolylineColor(OBJOutline, drawing.properties.warna, drawing.properties.opacity);
            //OBJOutline.Thickness = drawing.properties.weight;
        }
        else if(drawing.type == DrawingType.ARROW)
        {
            //var drawCoordinates = drawing.geometryPolygon.coordinates;

            //// Create Polygon Object
            //var OBJ = Instantiate(AssetPackageController.Instance.prefabArrow, EnvironmentsController.GetMapComponent().transform);
            //OBJ.name = drawing.nama;
            //OBJ.tag = "entity";
            //objData = OBJ.gameObject.AddComponent<ObjekDrawing>();

            //var OBJPolygon = OBJ.polygon;
            //var OBJOutline = OBJ.outline;

            //OBJPolygon.SetPointPosition(0, Vector3.zero);
            //OBJOutline.SetPointPosition(0, Vector3.zero);

            //for (int i = 0; i < drawing.geometryPolygon.coordinates[0].Count; i++)
            //{
            //    var newSegment = CreateNewDrawSegment(
            //        new ArcGISPoint(drawCoordinates[0][i][0], drawCoordinates[0][i][1], 0, ArcGISSpatialReference.WGS84()),
            //        objRotation, OBJ.segments, drawing.nama, i
            //    );

            //    objData.geometry.Add(newSegment);
            //}

            //await Task.Delay(10);
            //OBJPolygon.meshOutOfDate = true;
            //OBJOutline.meshOutOfDate = true;

            //for (int i = 0; i < OBJ.segments.childCount - 1; i++)
            //{
            //    if (i == 0)
            //    {
            //        OBJPolygon.SetPointPosition(0, new Vector2(OBJ.segments.GetChild(i).position.x, OBJ.segments.GetChild(i).position.z));
            //        OBJOutline.SetPointPosition(0, OBJ.segments.GetChild(i).position);
            //    }
            //    else
            //    {
            //        OBJPolygon.AddPoint(new Vector2(OBJ.segments.GetChild(i).position.x, OBJ.segments.GetChild(i).position.z));
            //        OBJOutline.AddPoint(OBJ.segments.GetChild(i).position);
            //    }
            //}

            //SetPolygonColor(OBJPolygon, drawing.properties.fillColor, drawing.properties.fillOpacity);
            //SetPolylineColor(OBJOutline, drawing.properties.warna, drawing.properties.opacity);
            //OBJOutline.Thickness = drawing.properties.weight;
        }

        // SETUP OBJEK DRAWING DATA
        objData.userID = drawing.id_user.GetValueOrDefault(-99);
        objData.documentID = drawing.dokumen.GetValueOrDefault(-99);
        objData.nama = drawing.nama;
        objData.type = drawing.type;
        objData.properties = drawing.properties;
    }

    public GameObject CreateNewDrawSegment(Transform _parent, ArcGISPoint _location)
    {
        var newSegment = CreateEntity(_location, new Vector3(0, 90, 0), AssetPackageController.Instance.prefabSegmentSelector.gameObject, _parent);
        return newSegment;
    }

    public void SetCircleColor(Disc obj, Color color, float opacity = 1f)
    {
        if (obj == null) return;

        Color objCircleColor = color;
        objCircleColor.a = Mathf.Clamp(opacity, 0f, 1f);

        obj.Color = objCircleColor;
    }

    public void SetPolygonColor(Polygon obj, Color color, float opacity = 1f)
    {
        if (obj == null) return;

        Color objPolygonColor = color;
        objPolygonColor.a = Mathf.Clamp(opacity, 0f, 1f);

        obj.Color = objPolygonColor;
    }

    public void SetPolylineColor(Polyline obj, Color color, float opacity = 1f)
    {
        if (obj == null) return;

        Color objOutlineColor = color;
        objOutlineColor.a = Mathf.Clamp(opacity, 0f, 1f);

        obj.Color = objOutlineColor;
    }

    public DrawingType StringToDrawingType(string type)
    {
        switch (type)
        {
            case "LineString":
                return DrawingType.POLYLINE;
            case "Polygon":
                return DrawingType.POLYGON;
            case "Point":
                return DrawingType.CIRCLE;
            case "circle":
                return DrawingType.CIRCLE;
            default:
                return DrawingType.POLYLINE;
        }
    }

    public string DrawingTypeToString(DrawingType type, bool typeOnGeometry = false)
    {
        switch (type)
        {
            case DrawingType.POLYLINE:
                return "LineString";
            case DrawingType.POLYLINE_CURVE:
                return "LineString";
            case DrawingType.POLYGON:
                return "Polygon";
            case DrawingType.CIRCLE:
                return (typeOnGeometry) ? "Point" : "circle";
            default:
                return "LineString";
        }
    }

    public DrawingArrowType StringToDrawingArrowType(string type)
    {
        switch (type)
        {
            case "arrow":
                return DrawingArrowType.ARROW;
            case "arrowLengkung":
                return DrawingArrowType.ARROW_DOWN;
            case "arrowLengkungAtas":
                return DrawingArrowType.ARROW_UP;
            default:
                return DrawingArrowType.UNDEFINED;
        }
    }

    public string DrawingArrowTypeToString(DrawingArrowType type)
    {
        switch (type)
        {
            case DrawingArrowType.ARROW:
                return "arrow";
            case DrawingArrowType.ARROW_DOWN:
                return "arrowLengkung";
            case DrawingArrowType.ARROW_UP:
                return "arrowLengkungAtas";
            default:
                return "arrow";
        }
    }

    public GameObject PrepareRootWalker(ObjekSatuanHelper satuan, GameObject OBJ, ArcGISPoint objPosition, ArcGISRotation objRotation)
    {
        switch (satuan.kategori)
        {
            case KategoriSatuan.VEHICLE:
                OBJ.AddComponent<LRootWalker>();
                break;
            case KategoriSatuan.SHIP:
                OBJ.AddComponent<SRootWalker>();
                break;
            case KategoriSatuan.SUBMARINE:
                OBJ.AddComponent<SRootWalker>();
                break;
            case KategoriSatuan.AIRCRAFT:
                OBJ.AddComponent<ARootWalker>();
                break;
            case KategoriSatuan.HELICOPTER:
                OBJ.AddComponent<ARootWalker>();
                break;
            case KategoriSatuan.INFANTRY:
                OBJ.AddComponent<LRootWalker>();
                break;
            default: OBJ.AddComponent<RootWalker>(); break;
        }

        OBJ.GetComponent<RootWalkerBase>().entity = OBJ;

        return OBJ;
    }

    private void InitRootWalker(GameObject rootWalker, ObjekSatuanHelper objSatuan)
    {
        switch (objSatuan.kategori)
        {
            case KategoriSatuan.VEHICLE:
                rootWalker.GetComponent<LRootWalker>().Init();
                break;
            case KategoriSatuan.SHIP:
                rootWalker.GetComponent<SRootWalker>().Init();
                break;
            case KategoriSatuan.SUBMARINE:
                rootWalker.GetComponent<SRootWalker>().Init();
                break;
            case KategoriSatuan.AIRCRAFT:
                rootWalker.GetComponent<ARootWalker>().Init();
                break;
            case KategoriSatuan.HELICOPTER:
                rootWalker.GetComponent<ARootWalker>().Init();
                break;
            case KategoriSatuan.INFANTRY:
                rootWalker.GetComponent<LRootWalker>().Init();
                break;
            default: rootWalker.GetComponent<RootWalker>().Init(); break;
        }
    }

    #region GPU INSTANCER
    public void InitGPUInstancer()
    {
        if(instancerManager == null)
        {
            Debug.LogError("Failed to Initialized GPU Instancer, No Instancer Prefab Manager Found!");
            return;
        }

        if(SATUAN != null || SATUAN.Count > 0)
        {
            for(int i=0; i < SATUAN.Count; i++)
            {
                var instancerPrefab = SATUAN[i].GetComponent<GPUInstancerPrefab>();
                if (instancerPrefab == null || allocatedSatuan.Contains(instancerPrefab)) continue;

                allocatedSatuan.Add(instancerPrefab);
            }

            GPUInstancerAPI.RegisterPrefabInstanceList(instancerManager, allocatedSatuan);
        }

        GPUInstancerAPI.InitializeGPUInstancer(instancerManager);
    }
    #endregion

    #region SCENARIO EDITOR ONLY
    public async Task exposeToEditor(List<ObjekSatuan> listSatuan)
    {
        for(int i=0; i < listSatuan.Count; i++)
        {
            var OBJData     = listSatuan[i];
            var OBJ         = OBJData.gameObject;

            var OBJGroundFitter = OBJ.GetComponent<FGroundFitter>();

            GenerateBoundingBox(OBJ);

            var editorTransform = OBJ.AddComponent<ExposeToEditor>();
            editorTransform.CanTransform = true;
            editorTransform.CanDuplicate = true;
            editorTransform.CanDelete = true;
            editorTransform.ShowSelectionGizmo = true;
            editorTransform.Selected = new ExposeToEditorUnityEvent();
            editorTransform.Unselected = new ExposeToEditorUnityEvent();

            editorTransform.Selected.AddListener(delegate { PlotSatuanController.instance.SelectSatuan(OBJData); });
            if(OBJGroundFitter != null)
            {
                editorTransform.Selected.AddListener(delegate { OBJGroundFitter.enabled = false; });
            }

            editorTransform.Unselected.AddListener(delegate { PlotTransformController.instance.UpdateGroundFitterRotation(OBJ); });
            editorTransform.Unselected.AddListener(delegate { PlotSatuanController.instance.UnselectSatuan(); });

            //if (OBJ.GetComponent<WGSBundleCore>().bundleType == WGSBundleCore.BundleType.LAND)
            //{
            //    GenerateTerrainFollower(OBJ);
            //}

            // Destroy UNUSED SCRIPT COMPONENTS FOR SCENARIO EDITOR
            Destroy(OBJ.GetComponent<EntityWalkerV2>());
            Destroy(OBJ.GetComponent<SplineController>());
        }
    }
    #endregion
}
