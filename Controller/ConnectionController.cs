using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using TMPro;

using Wargaming.Core.GlobalParam;
using Wargaming.Core;
using Unity.VisualScripting;
using System;

public class ConnectionController : MonoBehaviour
{
    [Header("Config File")]
    public string serviceFile;

    [Header("Form Data")]
    public TMP_InputField connectionName;

    public TMP_InputField loginIP;
    public TMP_InputField loginPort;

    public TMP_InputField cbIP;
    public TMP_InputField cbPort;

    public TMP_InputField colyseusIP;
    public TMP_InputField colyseusPort;

    //public TMP_InputField loginConnection;
    //public TMP_InputField cbConnection;
    //public TMP_InputField colyseusConnection;

    public TextMeshProUGUI formTitle;
    public Button submitButton;
    public Button deleteButton;

    [Header("Notif")]
    public Transform formNotif;

    [Header("References")]
    [SerializeField] private GameObject prefabList;
    [SerializeField] private Transform listContainer;
    [SerializeField] private TextMeshProUGUI labelLogin;
    [SerializeField] private TextMeshProUGUI labelCb;
    [SerializeField] private TextMeshProUGUI labelColyseus;

    [Header("Events")]
    public UnityEvent OnCreateNew;
    public UnityEvent OnEditSelected;
    public UnityEvent AfterSubmit;

    private int? selected = null;
    private List<ServiceData> connections;

    #region INSTANCE
    public static ConnectionController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            connections = new List<ServiceData>();
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        LoadServiceFromFile();
    }

    #region API REQUEST
    private void LoadServiceFromFile()
    {
        // If Services File is not defined, throw error message
        if (serviceFile == null)
        {
            Debug.LogError("Error, Connection File is not defined. Please provide one!");
            return;
        }

        var path = Path.Combine(Directory.GetCurrentDirectory(), serviceFile);
        if (File.Exists(path))
        {
            // Use Current Connection Document if exists
            var _XMLSerialize = new XmlSerializer(typeof(List<ServiceData>));
            using (var reader = new StreamReader(path))
            {
                connections = (List<ServiceData>)_XMLSerialize.Deserialize(reader);
            }
        }
        else
        {
            AddConnectionToList(
                "Local Server",
                "http://localhost/eppkm/public",
                null,
                "http://localhost/eppkm_simulasi",
                null,
                "ws://localhost",
                "2567",
                true
            );

            RefreshLabel();
        }
        
        // Add List Connection Into ListView
        RefreshList();
    }
    #endregion

    #region FORM HANDLER
    private bool CheckForm()
    {
        if (connectionName.isEmpty() || loginIP.isEmpty() || cbIP.isEmpty() || colyseusIP.isEmpty())
        {
            OnInputError("Data belum lengkap.");
            return false;
        }

        return true;
    }

    public bool OnInputError(string message)
    {
        var label = formNotif.GetComponentInChildren<TextMeshProUGUI>();
        label.text = message;

        formNotif.gameObject.SetActive(true);
        return false;
    }

    private void SaveChanged()
    {
        // Serialize and Write Changed into Service Document
        var _XMLSerialize = new XmlSerializer(typeof(List<ServiceData>));
        using (var writer = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), serviceFile)))
        {
            _XMLSerialize.Serialize(writer, connections);
        }
    }

    private void AddConnectionToList(string _name, string _login, string _login_port, string _cb, string _cb_port, string _colyseus, string _colyseusPort, bool active = false)
    {
        if (connections == null) return;

        connections.Add(new ServiceData
        {
            ServiceName         = _name,
            ServiceLogin        = new[] { "http://", "https://" }.Any(c => _login.Contains(c)) ? _login : "http://" + _login,
            ServiceLoginPort    = _login_port,
            ServiceCB           = new[] { "http://", "https://" }.Any(c => _cb.Contains(c)) ? _cb : "http://" + _cb,
            ServiceCBPort       = _cb_port,
            ServerColyseus      = new[] { "ws://" }.Any(c => _colyseus.Contains(c)) ? _colyseus : "ws://" + _colyseus,
            ServerColyseusPort  = _colyseusPort,
            Active              = active
        });

        AfterSubmit.Invoke();

        RefreshList();
    }
     
    private void CreateNewConnection()
    {
        if (!CheckForm()) return;

        AddConnectionToList(
            connectionName.text,
            loginIP.text,
            loginPort.text,
            cbIP.text,
            cbPort.text,
            colyseusIP.text,
            colyseusPort.text
        );

        AfterSubmit.Invoke();

        SaveChanged();
    }

    private void EditConnection()
    {
        if (!selected.HasValue) return;
        if (!CheckForm()) return;

        connections[selected.Value] = new ServiceData
        {
            ServiceName             = connectionName.text,
            ServiceLogin            = new[] { "http://", "https://" }.Any(c => loginIP.text.Contains(c)) ? loginIP.text : "http://" + loginIP.text,
            ServiceLoginPort        = (loginPort.text == "") ? null : loginPort.text,
            ServiceCB               = new[] { "http://", "https://" }.Any(c => cbIP.text.Contains(c)) ? cbIP.text : "http://" + cbIP.text,
            ServiceCBPort           = (cbPort.text == "") ? null : cbPort.text,
            ServerColyseus          = new[] { "ws://" }.Any(c => colyseusIP.text.Contains(c)) ? colyseusIP.text : "ws://" + colyseusIP.text,
            ServerColyseusPort      = (colyseusPort.text == "") ? null : colyseusPort.text,
            Active                  = connections[selected.Value].Active
        };

        SaveChanged();

        AfterSubmit.Invoke();

        RefreshList();
    }

    public void DeleteConnection()
    {
        if (!selected.HasValue) return;
        if (connections.Count <= 1) return;

        connections.RemoveAt(selected.Value);
        selected = null;

        AfterSubmit.Invoke();

        SaveChanged();

        RefreshList();
        SelectConnection(0);
    }
    #endregion

    #region MODAL LIST HANDLER
    public void OnCreateNewConnection()
    {
        connectionName.reset();
        loginIP.reset();
        loginPort.reset();

        cbIP.reset();
        cbPort.reset();

        colyseusIP.reset();
        colyseusPort.reset("2567");

        formTitle.text = "Create Connection";

        formNotif.gameObject.SetActive(false);
        deleteButton.gameObject.SetActive(false);

        submitButton.GetComponentInChildren<TextMeshProUGUI>().text = "Create";
        submitButton.onClick.RemoveAllListeners();
        submitButton.onClick.AddListener(delegate { CreateNewConnection(); });

        OnCreateNew.Invoke();
    }

    public void OnEditConnection()
    {
        if (!selected.HasValue) return;
        var selectedConnection  = connections[selected.Value];

        connectionName.text     = selectedConnection.ServiceName;
        loginIP.text            = selectedConnection.ServiceLogin;
        loginPort.text          = selectedConnection.ServiceLoginPort;

        cbIP.text               = selectedConnection.ServiceCB;
        cbPort.text             = selectedConnection.ServiceCBPort;

        colyseusIP.text         = selectedConnection.ServerColyseus;
        colyseusPort.text       = selectedConnection.ServerColyseusPort;

        formTitle.text          = "Edit Connection";

        deleteButton.gameObject.SetActive(true);

        submitButton.GetComponentInChildren<TextMeshProUGUI>().text = "Edit";
        submitButton.onClick.RemoveAllListeners();
        submitButton.onClick.AddListener(delegate { EditConnection(); });

        OnEditSelected.Invoke();
    }

    public void RefreshList()
    {
        //Clean List Container
        listContainer.DestroyAllChild();

        for (int i = 0; i < connections.Count; i++)
        {
            int index = i;
            AddConnectionList(index, connections[i]);
        }
    }

    private void AddConnectionList(int index, ServiceData data)
    {
        var list = Instantiate(prefabList, listContainer).GetMetadata();

        list.GetMetadataLabel("name").text = data.ServiceName;
        list.GetMetadataToggle("selector").group = listContainer.GetComponent<ToggleGroup>();
        list.GetMetadataMenuToggler("selector").onToggleOn.AddListener(delegate { SelectConnection(index); });

        //list.GetMetadataButton("btn-select").onClick.AddListener(delegate { SelectConnection(index); });
        //list.GetMetadataButton("btn-edit").onClick.AddListener(delegate { OnEditConnection(index); });

        if (data.Active)
        {
            list.GetMetadataToggle("selector").isOn = true;

            selected = index;
            RefreshLabel();
        }
    }

    public void SelectConnection(int index)
    {
        if (selected.HasValue)
        {
            connections[selected.Value].Active = false;
        }

        selected = index;
        connections[selected.Value].Active = true;

        RefreshLabel();
        SaveChanged();
    }

    public void RefreshLabel()
    {
        var _con = connections[selected.Value];

        labelLogin.text     = (selected.HasValue) ? _con.ServiceLogin + ((_con.ServiceLoginPort != null) ? ":" + _con.ServiceLoginPort : "") : "No Data";
        labelCb.text        = (selected.HasValue) ? _con.ServiceCB + ((_con.ServiceCBPort != null) ? ":" + _con.ServiceCBPort : "") : "No Data";
        labelColyseus.text  = (selected.HasValue) ? _con.ServerColyseus + ((_con.ServerColyseusPort != null) ? ":" + _con.ServerColyseusPort : "") : "No Data";

        RefreshServiceData();
    }

    private void RefreshServiceData()
    {
        var _con = connections[selected.Value];

        ServerConfig.SERVICE_LOGIN = _con.ServiceLogin + ((_con.ServiceLoginPort != null) ? ":" + _con.ServiceLoginPort : "");
        ServerConfig.SERVICE_CB = _con.ServiceCB + ((_con.ServiceCBPort != null) ? ":" + _con.ServiceCBPort : "");
        ServerConfig.SERVER_COLYSEUS = _con.ServerColyseus + ((_con.ServerColyseusPort != null) ? ":" + _con.ServerColyseusPort : "");
    }
    #endregion
}
