using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using TMPro;
using Wargaming.Components.DebugComponent;
using Wargaming.Core.GlobalParam;
using static Wargaming.Core.GlobalParam.HelperDataPlotting.PlottingHelper;

namespace Wargaming.Core.ScenarioEditor
{
    public class ScenarioEditorController : MonoBehaviour
    {
        public MenuMode mode;

        //public bool onPlotSatuanMenu = false;
        //public bool onPlotPasukanMenu = false;
        //public bool onPlotMisiMenu = false;
        //public bool onPlotFormasiMenu = false;
        //public bool onPlotObstacleMenu = false;
        //public bool onPlotRadarMenu = false;
        //public bool onPlotKekuatanMenu = false;
        //public bool onPlotBungusMenu = false;
        //public bool onPlotSituasiMenu = false;
        //public bool onPlotLogistikMenu = false;

        #region INSTANCE
        public static ScenarioEditorController instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private async void Start()
        {
            if (DebugScenarioEditor.instance != null)
            {
                await DebugScenarioEditor.instance.Init();
            }

            DatapackEditor.instance.LoadAllDatapack();
            DatapackEditor.instance.InitFilterDatapack();

            prepareScenarioEditor();
        }

        // Prepare Client Gameplay
        public async void prepareScenarioEditor()
        {
            Debug.Log("Preparing Scenario Editor...");
            LoadingController.UpdateGameplayLoading("Preparing Scenario Editor...");
            await Task.Delay(1000);

            // Set username and bagian to any text object with "label-username" and "label-asisten" tag
            setLabelUserAsisten();

            // Set Timetable data
            Debug.Log("Load Timetable Kegiatan...");
            LoadingController.UpdateGameplayLoading("Load Timetable Kegiatan...");
            var kegiatan = await TimetableController.instance.initTimetable();

            if (SessionUser.id != 1)
            {
                Debug.Log("Loadi Plotting CB...");
                LoadingController.UpdateGameplayLoading("Load Plotting CB...");

                PlayerData.PLAYERS = new List<PData>();

                // Add User Into List
                PlayerData.AddPlayer(new PData
                {
                    id = SessionUser.id,
                    name = SessionUser.name,
                    nama_asisten = SessionUser.nama_asisten,
                    id_bagian = SessionUser.id_bagian,
                    jenis_user = SessionUser.jenis_user
                });

                // Prepare Load Data Alutsista dari CB Aktif Kogas
                await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, DataCB.GetYourCB().nama_document, true);

                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
                //await EntityController.instance.loadEntityFromCB(SessionUser.id, SessionUser.id_kogas, SkenarioAktif.ID_SKENARIO, CBSendiri.nama_document);
            }
        }

        private void setLabelUserAsisten()
        {
            foreach (var label in GameObject.FindGameObjectsWithTag("label-username"))
            {
                label.GetComponent<TextMeshProUGUI>().text = SessionUser.name;
            }

            foreach (var label in GameObject.FindGameObjectsWithTag("label-asisten"))
            {
                label.GetComponent<TextMeshProUGUI>().text = SessionUser.nama_asisten;
            }
        }
    }
}