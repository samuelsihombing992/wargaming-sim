using UnityEngine;
using UnityEngine.UI;

using EnhancedUI.EnhancedScroller;
using static Wargaming.Core.GlobalParam.EntityHelper;

public class ListEntityController : MonoBehaviour
{
    [Header("References")]
    public ListEntityScrollerController listEntity;

    [Header("Entity Satuan Group")]
    public Toggle vehicleToggle;
    public Toggle shipToggle;
    public Toggle submarineToggle;
    public Toggle aircraftToggle;
    public Toggle helicopterToggle;
    public Button toggleAll;

    #region INSTANCE
    public static ListEntityController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void ToggleEntity(string type)
    {
        return;
        listEntity.data.Clear();

        for(int i=0; i < listEntity.tempData.Count; i++)
        {
            if(type == "all")
            {
                listEntity.data.Add(listEntity.tempData[i]);
                continue;
            }
            else
            {
                if(listEntity.tempData[i].kategori == KategoriSatuan.VEHICLE && vehicleToggle.isOn)
                {
                    listEntity.data.Add(listEntity.tempData[i]);
                    continue;
                }

                if (listEntity.tempData[i].kategori == KategoriSatuan.SHIP && shipToggle.isOn)
                {
                    listEntity.data.Add(listEntity.tempData[i]);
                    continue;
                }

                if (listEntity.tempData[i].kategori == KategoriSatuan.AIRCRAFT && aircraftToggle.isOn)
                {
                    listEntity.data.Add(listEntity.tempData[i]);
                    continue;
                }
            }
        }

        Debug.Log(listEntity.data.Count + " | " + listEntity.tempData.Count);

        listEntity.GetComponent<EnhancedScroller>().ReloadData();

        listEntity.refreshContainer();
        listEntity.ResizeScroller();
    }

    public void ToggleAll()
    {
        bool state = true;
        if(vehicleToggle.isOn && shipToggle.isOn && submarineToggle.isOn && aircraftToggle.isOn && helicopterToggle.isOn)
        {
            state = false;
        }

        vehicleToggle.isOn = state;
        shipToggle.isOn = state;
        submarineToggle.isOn = state;
        aircraftToggle.isOn = state;
        helicopterToggle.isOn = state;
    }
}
