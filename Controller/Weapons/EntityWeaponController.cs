using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WGSBundling.Bundles.Weapons;

public class EntityWeaponController : MonoBehaviour
{
    /// <summary>
    /// Mode Penembakan Senjata
    /// - Visualize     = Mode Hanya visualisasi penembakan tanpa melakukan penembakan peluru
    /// - Firing        = Mode melakukan visualisasi dan penembakan peluru
    /// </summary>
    public enum WeaponMode { VISUALIZE, FULL }

    /// <summary>
    /// Mode Control Senjata
    /// - MANUAL        = Senjata ditembakkan secara manual (melalui aksi tombol)
    /// - AUTO          = Senjata ditembakkan secara otomatis (ketika kondisi terpenuhi)
    /// </summary>
    public enum WeaponControlMode { MANUAL, AUTO }

    [SerializeField] private WeaponMode mode;
    [SerializeField] private WeaponControlMode controlMode;

    [Header("Target")]
    public Transform target;
    private Transform _lastTarget;

    [Header("Weapon Settings")]
    [SerializeField] private NodeWeaponComponent weapon;

    [SerializeField] private Vector2 weaponRadius;
    [SerializeField] private Vector2 baseFov;
    [SerializeField] private Vector2 turretFov;
}
