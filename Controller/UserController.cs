using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;

public class UserController : MonoBehaviour
{
    [Header("References")]
    public GameObject prefabList;
    public Transform listContainer;

    [Header("Color Ref")]
    public Sprite currentUserColor;
    public Sprite defaultColor;

    #region INSTANCE
    public static UserController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        listContainer.DestroyAllChild();
    }

    public void CreateUserList(long idUser, string username)
    {
        var newList     = Instantiate(prefabList, listContainer);
        newList.name    = idUser.ToString();

        newList.GetMetadata().GetMetadataLabel("name").text = username;
        newList.GetComponent<Image>().sprite                = (SessionUser.id == idUser) ? currentUserColor : defaultColor;
    }

    public void RemoveUserList(string idUser)
    {
        foreach(Transform list in listContainer)
        {
            if(list.name == idUser)
            {
                Destroy(list.gameObject);
                break;
            }
        }
    }
}
