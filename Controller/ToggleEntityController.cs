using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using SickscoreGames.HUDNavigationSystem;
using Wargaming.TacticalMap.Component.Controller;

public class ToggleEntityController : MonoBehaviour
{
    [Header("Entity Satuan Group")]
    public Toggle vehicleToggle;
    public Toggle shipToggle;
    public Toggle submarineToggle;
    public Toggle aircraftToggle;
    public Toggle helicopterToggle;
    public Toggle infantryToggle;
    public Toggle drawingToggle;
    public Button simbolTaktisToggle;

    [Header("Entity - Others")]
    public Toggle radarToggle;
    public Toggle obstacleToggle;

    #region INSTANCE
    public static ToggleEntityController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void ToggleEntity(string type)
    {
        switch (type)
        {
            case "vehicle":
                toggleEntitySatuan(EntityController.instance.VEHICLE, vehicleToggle.isOn);
                break;
            case "ship":
                toggleEntitySatuan(EntityController.instance.SHIP, shipToggle.isOn);
                break;
            case "aircraft":
                toggleEntitySatuan(EntityController.instance.AIRCRAFT, aircraftToggle.isOn);
                break;
            case "helicopter":
                toggleEntitySatuan(EntityController.instance.HELICOPTER, helicopterToggle.isOn);
                break;
            case "submarine":
                toggleEntitySatuan(EntityController.instance.SUBMARINE, submarineToggle.isOn);
                break;
            case "pasukan":
                toggleEntitySatuan(EntityController.instance.INFANTRY, infantryToggle.isOn);
                break;
            case "drawing":
                toggleDrawing(EntityController.instance.DRAWING, drawingToggle.isOn);
                break;
        }
    }

    private void toggleEntitySatuan(List<ObjekSatuan> data, bool state)
    {
        for (int i = 0; i < data.Count; i++)
        {
            data[i].gameObject.SetActive(state);
            //data[i].hudElementTacmap.showIndicator = state;
        }

        ToggleSimbolTaktis(simbolTaktisToggle.GetComponent<MenuTogglerMultistep>());
    }

    private void toggleDrawing(List<ObjekDrawing> data, bool state)
    {
        for (int i = 0; i < data.Count; i++)
        {
            data[i].gameObject.SetActive(state);
        }
    }

    //private void toggleEntitySatuan(List<DataSatuan> data, bool state)
    //{
    //    for (int i = 0; i < data.Count; i++)
    //    {
    //        data[i].gameObject.SetActive(state);
    //        data[i].hudElementTacmap.showIndicator = state;
    //    }

    //    ToggleSimbolTaktis(simbolTaktisToggle.GetComponent<MenuTogglerMultistep>());
    //}

    public void ToggleSimbolTaktis(MenuTogglerMultistep step)
    {
        if (step == null) return;
        //if (TacticalMapController.instance.isOn) return;

        foreach (GameObject data in GameObject.FindGameObjectsWithTag("simbol-taktis"))
        {
            var HUDElement = data.GetComponent<HUDNavigationElement>();
            if (HUDElement == null) continue;

            if (HUDElement.Indicator == null) continue;

            switch (step.activeIndex)
            {
                case 0:
                    HUDElement.showIndicator = true;
                    HUDElement.Indicator.ToggleCustomTransform("label_nama", false);
                    HUDElement.Indicator.ToggleCustomTransform("simbol_taktis", true);
                    break;
                case 1:
                    HUDElement.showIndicator = true;
                    HUDElement.Indicator.ToggleCustomTransform("label_nama", true);
                    HUDElement.Indicator.ToggleCustomTransform("simbol_taktis", false);
                    break;
                case 2:
                    HUDElement.showIndicator = true;
                    HUDElement.Indicator.ToggleCustomTransform("label_nama", true);
                    HUDElement.Indicator.ToggleCustomTransform("simbol_taktis", true);
                    break;
                default:
                    HUDElement.showIndicator = false;
                    break;
            }
        }
    }
}
