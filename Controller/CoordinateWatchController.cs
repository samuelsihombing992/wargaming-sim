using UnityEngine;

using TMPro;
using CoordinateSharp;

using Esri.ArcGISMapsSDK.Components;
using Wargaming.Core;
using WGSBundling.Core;

public class CoordinateWatchController : MonoBehaviour
{
    [Header("References")]
    public TextMeshProUGUI lat;
    public TextMeshProUGUI lng;
    public TextMeshProUGUI mgrs;
    public TextMeshProUGUI utm;
    public TextMeshProUGUI altitude;

    [Header("Entity Info")]
    public GameObject entityInfoGroup;
    public TextMeshProUGUI entityName;
    public TextMeshProUGUI heading;
    public TextMeshProUGUI tilt;
    public TextMeshProUGUI bank;

    private ArcGISLocationComponent mainCam;
    //private WGSBundleCore followedObj;
    //private ArcGISLocationComponent locationComponent;

    private EagerLoad el;

    #region INSTANCE
    public static CoordinateWatchController instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;

            mainCam = Camera.main.GetComponentInParent<ArcGISLocationComponent>();
            if (mainCam == null) enabled = false;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    private void Start()
    {
        el = new EagerLoad(EagerLoadType.UTM_MGRS);
        entityInfoGroup.SetActive(false);
    }

    private void Update()
    {
        SyncCoordinate();
        SyncAltitude();
        SyncFollowedObject();
    }

    private void SyncCoordinate()
    {
        if ((lat && mainCam.Position.X.ToString() != lat.text) || (lng && mainCam.Position.Y.ToString() != lng.text))
        {
            Coordinate c = new Coordinate(mainCam.Position.Y, mainCam.Position.X, System.DateTime.Now, el);

            lat.text = c.Latitude.ToString();
            lng.text = c.Longitude.ToString();
            mgrs.text = c.MGRS.ToString();
            utm.text = c.UTM.ToString();
        }
    }

    private void SyncAltitude()
    {
        if (altitude && mainCam.Position.Z.ToString() != altitude.text)
        {
            altitude.text = Mathf.Floor((float)mainCam.Position.Z) + " M";
        }
    }

    private void SyncFollowedObject()
    {
        var followedObject = WargamingCam.instance.followedObj;

        if (followedObject == null)
        {
            if (entityInfoGroup.activeSelf) entityInfoGroup.SetActive(false);
            return;
        }

        if (!entityInfoGroup.activeSelf) entityInfoGroup.SetActive(true);

        var followedObjectCore = followedObject.GetComponent<WGSBundleCore>();

        entityName.text     = followedObject.GetComponent<ObjekSatuan>().info.namaSatuan;
        heading.text        = (int)followedObject.Rotation.Heading + "�";

        if (followedObjectCore.parent == null)
        {
            bank.text = "0�";
            tilt.text = "0�";
        }
        else
        {
            bank.text = (int)followedObjectCore.parent.rotation.z + "�";
            tilt.text = (int)followedObjectCore.parent.rotation.x + "�";
        }
    }

    //public void initEntityInfo()
    //{
    //    entityInfoGroup.SetActive(true);

    //    locationComponent = Camera.main.GetComponentInParent<WargamingCam>().followedObj;
    //    followedObj = Camera.main.GetComponentInParent<WargamingCam>().followedObj.GetComponent<WGSBundleCore>();
    //}

    //public void destroyEntityInfo()
    //{
    //    entityInfoGroup.SetActive(false);
    //    followedObj = null;
    //}
}
