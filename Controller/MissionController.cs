using System.Collections.Generic;
using System.Threading.Tasks;

using UnityEngine;

using Wargaming.Core.GlobalParam.HelperPlotting;
using FluffyUnderware.Curvy.Controllers;
using System;
using Esri.ArcGISMapsSDK.Components;
using Wargaming.Core.GlobalParam;
using FluffyUnderware.Curvy;
using Esri.HPFramework;
using Wargaming.Components.CargoHold;
using WGSBundling.Bundles.CargoHold;
using static Wargaming.Core.GlobalParam.GlobalHelper;
using Esri.GameEngine.Geometry;
using Wargaming.Core;
using Wargaming.Components.Walker;
using FluffyUnderware.DevTools.Extensions;
using System.Reflection;
using WGSBundling.Bundles.Helipad;
using UnityEngine.Assertions.Must;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Unity.Mathematics;
using System.Linq;

namespace Wargaming.Components.Controller
{
    public class MissionController : MonoBehaviour
    {
        [Header("Settings")]
        public CurvyInterpolation routeInterpolation = CurvyInterpolation.Bezier;

        [Header("Prefabs")]
        public GameObject prefabRoute;
        public GameObject prefabNode;

        public GameObject prefabRouteV2;
        public GameObject prefabNodeV2;

        public GameObject prefabRouteV3;

        [Header("Container")]
        public Transform missionContainer;
        public Transform rootMissionController;

        public List<GameObject> sst = new List<GameObject>();

        [Header("Data")]
        public List<MissionWalker> OBJ_MISSION = new List<MissionWalker>();
        public List<RootMissionWalker> OBJ_MISSION_V2 = new List<RootMissionWalker>();

        #region INSTANCE
        public static MissionController instance;
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private bool SetTakeoffState(GameObject entity, MisiSatuan mission)
        {
            var data = entity.GetComponent<ObjekSatuan>();
            if (data == null || mission == null) return false;

            if(!mission.data_properties.takeOff && !mission.data_properties.landing)
            {
                if(data.kategori == EntityHelper.KategoriSatuan.AIRCRAFT)
                {
                    return StartupConfig.settings.AIRCRAFT_DEF_TAKEOFF_LANDING;
                }
                else if(data.kategori == EntityHelper.KategoriSatuan.HELICOPTER)
                {
                    return StartupConfig.settings.HELI_DEF_TAKEOFF_LANDING;
                }
                else
                {
                    return false;
                }
            }

            return mission.data_properties.takeOff;
        }

        private bool SetLandingState(GameObject entity, MisiSatuan mission)
        {
            var data = entity.GetComponent<ObjekSatuan>();
            if (data == null) return false;

            if (!mission.data_properties.takeOff && !mission.data_properties.landing)
            {
                if (data.kategori == EntityHelper.KategoriSatuan.AIRCRAFT)
                {
                    return StartupConfig.settings.AIRCRAFT_DEF_TAKEOFF_LANDING;
                }
                else if (data.kategori == EntityHelper.KategoriSatuan.HELICOPTER)
                {
                    return StartupConfig.settings.HELI_DEF_TAKEOFF_LANDING;
                }
                else
                {
                    return false;
                }
            }

            return mission.data_properties.landing;
        }

        public async Task<RootMissionWalker> AddNewRouteV3(List<Vector3> PATH, GameObject entity, MisiSatuan mission)
        {
            var route = Instantiate(prefabRouteV2, rootMissionController).GetComponent<RootMissionWalker>();
            var routeSpline = route.GetComponent<CurvySpline>();

            routeSpline.Interpolation = routeInterpolation;

            route.transform.SetRootCoordinate(PATH[0]);
            route.transform.SetRootOrientation(new Vector3(0, 90f, 0));

            // Set Data Misi
            route.name          = mission.id_mission;
            route.id            = mission.id;
            route.missionId     = mission.id_mission;
            route.jenis         = GetRootJenisRoute(route.gameObject, mission.jenis);
            route.speed         = SpeedHelper.GetKecepatanByType(mission.data_properties.kecepatan, mission.data_properties.type);
            route.takeoff       = SetTakeoffState(entity, mission);
            route.landing       = SetLandingState(entity, mission);

            List<Vector3> TEMP_PATH_NODE = new List<Vector3>();

            for (int i = 0; i < PATH.Count; i++)
            {
                //if (i != 0 && i != (PATH.Count - 1) && routeInterpolation == CurvyInterpolation.BSpline)
                if (i != 0 && i != (PATH.Count - 1))
                {
                    // Set First Segment Divider
                    double firstSegmentDistance = OnlineMapsUtils.DistanceBetweenPointsD(PATH[i - 1], PATH[i]);

                    // Set Last Segment Divider
                    double lastSegmentDistance = OnlineMapsUtils.DistanceBetweenPointsD(PATH[i], PATH[i + 1]);
                    double length = 0;

                    if (firstSegmentDistance < lastSegmentDistance)
                    {
                        length = firstSegmentDistance;
                    }
                    else
                    {
                        length = lastSegmentDistance;
                    }

                    if (length * 1000 < 200)
                    {
                        SpawnNewRootSegment(route, PATH[i]);
                        TEMP_PATH_NODE.Add(PATH[i]);

                        continue;
                    }

                    length = (length * 1000) / 25;

                    // Set First Segment Divider
                    var xDist = PATH[i - 1].x - PATH[i].x;
                    var yDist = PATH[i - 1].y - PATH[i].y;
                    var zDist = PATH[i - 1].z - PATH[i].z;

                    var dist = Math.Sqrt(xDist * xDist + yDist * yDist);
                    var fractionOfTotal = (length / 10000) / dist;

                    Vector3 newPos = new Vector3(
                        (float)(PATH[i].x + xDist * fractionOfTotal),
                        (float)(PATH[i].y + yDist * fractionOfTotal),
                        (float)(PATH[i].z + zDist * fractionOfTotal));

                    SpawnNewRootSegment(route, newPos);
                    TEMP_PATH_NODE.Add(newPos);

                    SpawnNewRootSegment(route, PATH[i]);
                    TEMP_PATH_NODE.Add(PATH[i]);

                    //// Set Last Segment Divider
                    xDist = PATH[i + 1].x - PATH[i].x;
                    yDist = PATH[i + 1].y - PATH[i].y;
                    zDist = PATH[i + 1].z - PATH[i].z;

                    dist = Math.Sqrt(xDist * xDist + yDist * yDist);
                    fractionOfTotal = (length / 10000) / dist;

                    newPos = new Vector3(
                        (float)(PATH[i].x + xDist * fractionOfTotal),
                        (float)(PATH[i].y + yDist * fractionOfTotal),
                        (float)(PATH[i].z + zDist * fractionOfTotal));

                    SpawnNewRootSegment(route, newPos);
                    TEMP_PATH_NODE.Add(newPos);
                }
                else
                {
                    if (i == 0 && entity.GetComponent<ObjekSatuan>().kategori == EntityHelper.KategoriSatuan.HELICOPTER && route.takeoff)
                    {
                        // Generate Additional Takeoff Path
                        SpawnNewRootSegment(route, new Vector3(PATH[i].x, PATH[i].y, PATH[i].z - 1000));
                        TEMP_PATH_NODE.Add(new Vector3(PATH[i].x, PATH[i].y, PATH[i].z - 1000));
                        continue;
                    }

                    //SpawnNewRootSegment(route, PATH[i]);

                    if (i == PATH.Count - 1 && entity.GetComponent<ObjekSatuan>().kategori == EntityHelper.KategoriSatuan.HELICOPTER && route.landing)
                    {
                        // Generate Additional Landing Path
                        SpawnNewRootSegment(route, new Vector3(PATH[i].x, PATH[i].y, PATH[i].z - 1000));
                        TEMP_PATH_NODE.Add(new Vector3(PATH[i].x, PATH[i].y, PATH[i].z - 1000));
                        continue;
                    }

                    SpawnNewRootSegment(route, PATH[i]);
                    TEMP_PATH_NODE.Add(PATH[i]);
                }
            }

            // Setup Kalkulasi Waktu dan Coordinate Misi
            var dateAtNode = mission.startDate;

            for (int i = 0; i < TEMP_PATH_NODE.Count; i++)
            {
                //if(i == 0 || i == TEMP_PATH_NODE.Count - 1)
                //{
                //     Setup Helicopter Takeoff & Landing Time Offset
                //    if ((route.takeoff && entity.GetComponent<ObjekSatuan>().kategori == EntityHelper.KategoriSatuan.HELICOPTER) ||
                //        (route.landing && entity.GetComponent<ObjekSatuan>().kategori == EntityHelper.KategoriSatuan.HELICOPTER))
                //    {
                //        dateAtNode = dateAtNode.AddSeconds(10);
                //    }
                //}

                if (i != 0)
                {
                    var node1 = TEMP_PATH_NODE[i - 1];
                    var node2 = TEMP_PATH_NODE[i]; 

                    if (node1.x == node2.x && node1.y == node2.y)
                    {
                        double distance = 0;
                        if (node1.z >= node2.z)
                        {
                            distance = node1.z - node2.z;
                        }
                        else
                        {
                            distance = node2.z - node1.z;
                        }

                        // Total step time
                        double totalTime = (distance / 1000) / route.speed * 3600;
                        dateAtNode = dateAtNode.AddSeconds(totalTime);
                    }
                    else
                    {
                        double dx, dy;
                        OnlineMapsUtils.DistanceBetweenPoints(node1.x, node1.y, node2.x, node2.y, out dx, out dy);

                        double stepDistance = Math.Sqrt(dx * dx + dy * dy);

                        // Total step time
                        double totalTime = stepDistance / route.speed * 3600;
                        dateAtNode = dateAtNode.AddSeconds(totalTime);
                    }
                }

                route.AddNewPos(dateAtNode.ToString(), new Vector3((float)TEMP_PATH_NODE[i].x, (float)TEMP_PATH_NODE[i].y, (float)TEMP_PATH_NODE[i].z));
            }

            OBJ_MISSION_V2.Add(route);

            return route;
        }

        public async Task<RootMissionWalker> AddNewRouteV4(List<Vector3> PATH, GameObject entity, MisiSatuan mission)
        {
            var route       = Instantiate(prefabRouteV3, rootMissionController).GetComponent<RootMissionWalker>();
            var routeSpline = route.GetComponent<CurvySpline>();

            routeSpline.Interpolation = routeInterpolation;

            route.transform.SetRootCoordinate(PATH[0]);
            route.transform.SetRootOrientation(new Vector3(0, 90f, 0));

            // Set Data Misi
            route.name          = mission.id_mission;
            route.id            = mission.id;
            route.missionId     = mission.id_mission;
            route.jenis         = GetRootJenisRoute(route.gameObject, mission.jenis);
            route.speed         = SpeedHelper.GetKecepatanByType(mission.data_properties.kecepatan, mission.data_properties.type);
            route.takeoff       = SetTakeoffState(entity, mission);
            route.landing       = SetLandingState(entity, mission);

            List<Vector3> TEMP_PATH_NODE    = new List<Vector3>();
            List<GameObject> TEMP_NODE      = new List<GameObject>();

            for (int i = 0; i < PATH.Count; i++)
            {
                //if (i != 0 && i != (PATH.Count - 1) && routeInterpolation == CurvyInterpolation.BSpline)
                if (i != 0 && i != (PATH.Count - 1))
                {
                    // Set First Segment Divider
                    double firstSegmentDistance = OnlineMapsUtils.DistanceBetweenPointsD(PATH[i - 1], PATH[i]);

                    // Set Last Segment Divider
                    double lastSegmentDistance = OnlineMapsUtils.DistanceBetweenPointsD(PATH[i], PATH[i + 1]);
                    double length = 0;

                    if (firstSegmentDistance < lastSegmentDistance)
                    {
                        length = firstSegmentDistance;
                    }
                    else
                    {
                        length = lastSegmentDistance;
                    }

                    if (length * 1000 < 200)
                    {
                        TEMP_NODE.Add(SpawnNewRootSegmentV2(PATH[i]));
                        TEMP_PATH_NODE.Add(PATH[i]);

                        continue;
                    }

                    length = (length * 1000) / 25;

                    // Set First Segment Divider
                    var xDist = PATH[i - 1].x - PATH[i].x;
                    var yDist = PATH[i - 1].y - PATH[i].y;
                    var zDist = PATH[i - 1].z - PATH[i].z;

                    var dist = Math.Sqrt(xDist * xDist + yDist * yDist);
                    var fractionOfTotal = (length / 10000) / dist;

                    Vector3 newPos = new Vector3(
                        (float)(PATH[i].x + xDist * fractionOfTotal),
                        (float)(PATH[i].y + yDist * fractionOfTotal),
                        (float)(PATH[i].z + zDist * fractionOfTotal));

                    TEMP_NODE.Add(SpawnNewRootSegmentV2(newPos));
                    TEMP_PATH_NODE.Add(newPos);

                    TEMP_NODE.Add(SpawnNewRootSegmentV2(PATH[i]));
                    TEMP_PATH_NODE.Add(PATH[i]);

                    //// Set Last Segment Divider
                    xDist = PATH[i + 1].x - PATH[i].x;
                    yDist = PATH[i + 1].y - PATH[i].y;
                    zDist = PATH[i + 1].z - PATH[i].z;

                    dist = Math.Sqrt(xDist * xDist + yDist * yDist);
                    fractionOfTotal = (length / 10000) / dist;

                    newPos = new Vector3(
                        (float)(PATH[i].x + xDist * fractionOfTotal),
                        (float)(PATH[i].y + yDist * fractionOfTotal),
                        (float)(PATH[i].z + zDist * fractionOfTotal));

                    TEMP_NODE.Add(SpawnNewRootSegmentV2(newPos));
                    TEMP_PATH_NODE.Add(newPos);
                }
                else
                {
                    if (i == 0 && entity.GetComponent<ObjekSatuan>().kategori == EntityHelper.KategoriSatuan.HELICOPTER && route.takeoff)
                    {
                        // Generate Additional Takeoff Path
                        TEMP_NODE.Add(SpawnNewRootSegmentV2(new Vector3(PATH[i].x, PATH[i].y, PATH[i].z - 1000)));
                        TEMP_PATH_NODE.Add(new Vector3(PATH[i].x, PATH[i].y, PATH[i].z - 1000));
                        continue;
                    }

                    //SpawnNewRootSegment(route, PATH[i]);

                    if (i == PATH.Count - 1 && entity.GetComponent<ObjekSatuan>().kategori == EntityHelper.KategoriSatuan.HELICOPTER && route.landing)
                    {
                        // Generate Additional Landing Path
                        TEMP_NODE.Add(SpawnNewRootSegmentV2(new Vector3(PATH[i].x, PATH[i].y, PATH[i].z - 1000)));
                        TEMP_PATH_NODE.Add(new Vector3(PATH[i].x, PATH[i].y, PATH[i].z - 1000));
                        continue;
                    }

                    TEMP_NODE.Add(SpawnNewRootSegmentV2(PATH[i]));
                    TEMP_PATH_NODE.Add(PATH[i]);
                }
            }

            await Task.Delay(10);
            for(int i=0; i < TEMP_NODE.Count; i++)
            {
                Destroy(TEMP_NODE[i].GetComponent<HPTransform>());
                TEMP_NODE[i].transform.SetParent(route.transform);
            }

            // Setup Kalkulasi Waktu dan Coordinate Misi
            var dateAtNode = mission.startDate;

            for (int i = 0; i < TEMP_PATH_NODE.Count; i++)
            {
                //if(i == 0 || i == TEMP_PATH_NODE.Count - 1)
                //{
                //     Setup Helicopter Takeoff & Landing Time Offset
                //    if ((route.takeoff && entity.GetComponent<ObjekSatuan>().kategori == EntityHelper.KategoriSatuan.HELICOPTER) ||
                //        (route.landing && entity.GetComponent<ObjekSatuan>().kategori == EntityHelper.KategoriSatuan.HELICOPTER))
                //    {
                //        dateAtNode = dateAtNode.AddSeconds(10);
                //    }
                //}

                if (i != 0)
                {
                    var node1 = TEMP_PATH_NODE[i - 1];
                    var node2 = TEMP_PATH_NODE[i];

                    if (node1.x == node2.x && node1.y == node2.y)
                    {
                        double distance = 0;
                        if (node1.z >= node2.z)
                        {
                            distance = node1.z - node2.z;
                        }
                        else
                        {
                            distance = node2.z - node1.z;
                        }

                        // Total step time
                        double totalTime = (distance / 1000) / route.speed * 3600;
                        dateAtNode = dateAtNode.AddSeconds(totalTime);
                    }
                    else
                    {
                        double dx, dy;
                        OnlineMapsUtils.DistanceBetweenPoints(node1.x, node1.y, node2.x, node2.y, out dx, out dy);

                        double stepDistance = Math.Sqrt(dx * dx + dy * dy);

                        // Total step time
                        double totalTime = stepDistance / route.speed * 3600;
                        dateAtNode = dateAtNode.AddSeconds(totalTime);
                    }
                }

                route.AddNewPos(dateAtNode.ToString(), new Vector3((float)TEMP_PATH_NODE[i].x, (float)TEMP_PATH_NODE[i].y, (float)TEMP_PATH_NODE[i].z));
            }

            OBJ_MISSION_V2.Add(route);

            return route;
        }

        public async Task<MissionWalker> AddNewRoute(List<Vector3> PATH, GameObject obj_satuan, MisiSatuan mission)
        {
            // Setup Default Satuan to Always Takeoff OR Landing If Enabled on StartupConfig
            var dataSatuan = obj_satuan.GetComponent<ObjekSatuan>();
            if(dataSatuan != null)
            {
                if(dataSatuan.kategori == EntityHelper.KategoriSatuan.AIRCRAFT && StartupConfig.settings.AIRCRAFT_DEF_TAKEOFF_LANDING)
                {
                    mission.data_properties.takeOff = StartupConfig.settings.AIRCRAFT_DEF_TAKEOFF_LANDING;
                    mission.data_properties.landing = StartupConfig.settings.AIRCRAFT_DEF_TAKEOFF_LANDING;
                }else if(dataSatuan.kategori == EntityHelper.KategoriSatuan.HELICOPTER && StartupConfig.settings.HELI_DEF_TAKEOFF_LANDING)
                {
                    mission.data_properties.takeOff = StartupConfig.settings.HELI_DEF_TAKEOFF_LANDING;
                    mission.data_properties.landing = StartupConfig.settings.HELI_DEF_TAKEOFF_LANDING;
                }
            }

            // Instantiate Misi Baru
            var route       = Instantiate(prefabRoute, missionContainer).GetComponent<MissionWalker>();
            var routeSpline = route.GetComponent<CurvySpline>();

            routeSpline.Interpolation = routeInterpolation;

            // Set Lokasi dan Orientasi Node Parent Misi
            route.transform.SetCoordinate(PATH[0]);
            route.transform.SetOrientation(new Vector3(0, 90f, 0));

            // Set Data Misi
            route.name          = mission.id_mission;
            route.id            = mission.id;
            route.missionId     = mission.id_mission;
            route.jenis         = GetJenisRoute(route.gameObject, mission.jenis);
            route.speed         = SpeedHelper.GetKecepatanByType(mission.data_properties.kecepatan, mission.data_properties.type);
            route.objEntity     = obj_satuan.GetComponent<SplineController>();
            route.takeoffStart  = mission.data_properties.takeOff;
            route.landingEnd    = mission.data_properties.landing;

            //if (route.name == "mission_155_satuan_14_21_906_1679368267653")
            //{
            //    route.END_AT_PARENT = GameObject.Find("satuan_14_21_53").transform;
            //    route.END_PARENT_NODE = 0;
            //}

            //if (route.name == "mission_781_satuan_15_21_780_1679891713087")
            //{
            //    route.START_AT_PARENT = GameObject.Find("satuan_15_21_199").transform;
            //    route.START_PARENT_NODE = 0;

            //    route.END_AT_PARENT = GameObject.Find("satuan_15_21_713").transform;
            //    route.END_PARENT_NODE = 0;
            //}

            // Setup Kalkulasi Waktu dan Coordinate Misi
            var dateAtNode = mission.startDate;
            if(route.START_AT_PARENT != null) dateAtNode = dateAtNode.AddSeconds(10);

            List<ArcGISLocationComponent> TEMP_PATH_NODE = new List<ArcGISLocationComponent>();
            for (int i = 0; i < PATH.Count; i++)
            {
                if(i != 0)
                {
                    if(PATH[i - 1].x == PATH[i].x && PATH[i - 1].y == PATH[i].y)
                    {
                        float distance = 0;
                        if (PATH[i - 1].z >= PATH[i].z){
                            distance = PATH[i - 1].z - PATH[i].z;
                        }
                        else
                        {
                            distance = PATH[i].z - PATH[i - 1].z;
                        }

                        // Total step time
                        double totalTime = (distance / 1000) / route.speed * 3600;
                        dateAtNode = dateAtNode.AddSeconds(totalTime);
                    }
                    else
                    {
                        double dx, dy;
                        OnlineMapsUtils.DistanceBetweenPoints(PATH[i - 1].x, PATH[i - 1].y, PATH[i].x, PATH[i].y, out dx, out dy);

                        double stepDistance = Math.Sqrt(dx * dx + dy * dy);

                        // Total step time
                        double totalTime = stepDistance / route.speed * 3600;
                        dateAtNode = dateAtNode.AddSeconds(totalTime);
                    }
                }

                route.AddNewPos(dateAtNode.ToString(), PATH[i]);

                if((i == 00 && route.takeoffStart) || (i == PATH.Count - 1 && route.landingEnd))
                {
                    PATH[i] = new Vector3(PATH[i].x, PATH[i].y, PATH[i].z - 1000);
                }

                GameObject splineNode = SpawnNewSegment(route, PATH[i]);

                //var splineNode = Instantiate(prefabNode, route.transform);
                //if ((i == 0 && route.takeoffStart) || (i == PATH.Count - 1 && route.landingEnd))
                //{
                //    PATH[i] = new Vector3(PATH[i].x, PATH[i].y, PATH[i].z - 1000);
                //}

                //splineNode.transform.SetCoordinate(PATH[i]);
                //splineNode.transform.SetOrientation(new Vector3(0, 0, -90));

                //if (i != 0 && i != PATH.Count - 1)
                //{
                //    //splineNode.GetComponent<CurvySplineSegment>().AutoBakeOrientation = true;
                //}

                TEMP_PATH_NODE.Add(splineNode.GetComponent<ArcGISLocationComponent>());

                //splineNode.transform.SetParent(route.transform);
            }

            GenerateAdditionalPath(route);

            await Task.Delay(10);
            // Remove ArcGISLocationComponent from each node
            for (int i = 0; i < TEMP_PATH_NODE.Count; i++)
            {
                Destroy(TEMP_PATH_NODE[i]);
            }
                
            OBJ_MISSION.Add(route);

            return route;
        }

        public async Task<MissionWalker> AddNewRouteV2(List<Vector3> PATH, GameObject obj_satuan, MisiSatuan mission)
        {
            // Instantiate Misi Baru
            var route       = Instantiate(prefabRoute, missionContainer).GetComponent<MissionWalker>();
            var routeSpline = route.GetComponent<CurvySpline>();

            routeSpline.Interpolation = routeInterpolation;

            // Set Lokasi dan Orientasi Node Parent Misi
            route.transform.SetCoordinate(PATH[0]);
            route.transform.SetOrientation(new Vector3(0, 90f, 0));

            // Set Data Misi
            route.name          = mission.id_mission;
            route.id            = mission.id;
            route.missionId     = mission.id_mission;
            route.jenis         = GetJenisRoute(route.gameObject, mission.jenis);
            route.speed         = SpeedHelper.GetKecepatanByType(mission.data_properties.kecepatan, mission.data_properties.type);
            route.objEntity     = obj_satuan.GetComponent<SplineController>();

            List<ArcGISLocationComponent> TEMP_PATH_NODE = new List<ArcGISLocationComponent>();
            for (int i=0; i < PATH.Count; i++)
            {
                if(i != 0 && i != (PATH.Count - 1) && routeInterpolation == CurvyInterpolation.BSpline)
                {
                    // Set First Segment Divider
                    double firstSegmentDistance = OnlineMapsUtils.DistanceBetweenPointsD(PATH[i - 1], PATH[i]);

                    // Set Last Segment Divider
                    double lastSegmentDistance = OnlineMapsUtils.DistanceBetweenPointsD(PATH[i], PATH[i + 1]);
                    double length = 0;

                    if(firstSegmentDistance < lastSegmentDistance)
                    {
                        length = firstSegmentDistance;
                    }
                    else
                    {
                        length = lastSegmentDistance;
                    }

                    if (length * 1000 < 200)
                    {
                        TEMP_PATH_NODE.Add(SpawnNewSegment(route, PATH[i]).GetComponent<ArcGISLocationComponent>());
                        continue;
                    }

                    length = (length * 1000) / 25;

                    //length = 20;

                    // Set First Segment Divider
                    var xDist = PATH[i - 1].x - PATH[i].x;
                    var yDist = PATH[i - 1].y - PATH[i].y;
                    var zDist = PATH[i - 1].z - PATH[i].z;

                    var dist = Math.Sqrt(xDist * xDist + yDist * yDist);
                    var fractionOfTotal = (length / 10000) / dist;

                    Vector3 newPos = new Vector3(
                        (float)(PATH[i].x + xDist * fractionOfTotal),
                        (float)(PATH[i].y + yDist * fractionOfTotal),
                        (float)(PATH[i].z + zDist * fractionOfTotal));

                    TEMP_PATH_NODE.Add(SpawnNewSegment(route, newPos).GetComponent<ArcGISLocationComponent>());
                    TEMP_PATH_NODE.Add(SpawnNewSegment(route, PATH[i]).GetComponent<ArcGISLocationComponent>());

                    //// Set Last Segment Divider
                    xDist = PATH[i + 1].x - PATH[i].x;
                    yDist = PATH[i + 1].y - PATH[i].y;
                    zDist = PATH[i + 1].z - PATH[i].z;

                    dist = Math.Sqrt(xDist * xDist + yDist * yDist);
                    fractionOfTotal = (length / 10000) / dist;

                    newPos = new Vector3(
                        (float)(PATH[i].x + xDist * fractionOfTotal),
                        (float)(PATH[i].y + yDist * fractionOfTotal),
                        (float)(PATH[i].z + zDist * fractionOfTotal));

                    TEMP_PATH_NODE.Add(SpawnNewSegment(route, newPos).GetComponent<ArcGISLocationComponent>());

                    //var xDist = (PATH[i - 1].x + PATH[i].x) / 2;
                    //var yDist = (PATH[i - 1].y + PATH[i].y) / 2;
                    //var zDist = (PATH[i - 1].z + PATH[i].z) / 2;

                    //SpawnNewSegment(route, new Vector3(xDist, yDist, zDist));
                }
                else
                {
                    SpawnNewSegment(route, PATH[i]);
                }
            }

            await Task.Delay(10);

            // Setup Kalkulasi Waktu dan Coordinate Misi
            var dateAtNode = mission.startDate;
            for (int i=0; i < TEMP_PATH_NODE.Count; i++)
            {
                if (i != 0)
                {
                    if (TEMP_PATH_NODE[i-1].Position.X == TEMP_PATH_NODE[i].Position.X && TEMP_PATH_NODE[i-1].Position.Y == TEMP_PATH_NODE[i].Position.Y)
                    {
                        double distance = 0;
                        if (TEMP_PATH_NODE[i-1].Position.Z >= TEMP_PATH_NODE[i].Position.Z)
                        {
                            distance = TEMP_PATH_NODE[i - 1].Position.Z - TEMP_PATH_NODE[i].Position.Z;
                        }
                        else
                        {
                            distance = TEMP_PATH_NODE[i].Position.Z - TEMP_PATH_NODE[i - 1].Position.Z;
                        }

                        // Total step time
                        double totalTime = (distance / 1000) / route.speed * 3600;
                        dateAtNode = dateAtNode.AddSeconds(totalTime);
                    }
                    else
                    {
                        double dx, dy;
                        OnlineMapsUtils.DistanceBetweenPoints(TEMP_PATH_NODE[i - 1].Position.X, TEMP_PATH_NODE[i - 1].Position.Y, TEMP_PATH_NODE[i].Position.X, TEMP_PATH_NODE[i].Position.Y, out dx, out dy);

                        double stepDistance = Math.Sqrt(dx * dx + dy * dy);

                        // Total step time
                        double totalTime = stepDistance / route.speed * 3600;
                        dateAtNode = dateAtNode.AddSeconds(totalTime);
                    }
                }

                route.AddNewPos(dateAtNode.ToString(), new Vector3((float)TEMP_PATH_NODE[i].Position.X, (float)TEMP_PATH_NODE[i].Position.Y, (float)TEMP_PATH_NODE[i].Position.Z));
            }

            await Task.Delay(10);
            for (int i = 0; i < TEMP_PATH_NODE.Count - 1; i++)
            {
                Destroy(TEMP_PATH_NODE[i]);
            }

            //var currentSegment = routeSpline.FirstSegment;
            //for (int i=0; i < PATH.Count; i++)
            //{
            //    if (i != 0 && i != (PATH.Count - 1) && routeInterpolation == CurvyInterpolation.BSpline)
            //    {
            //        Debug.Log("Subdivided");
            //        routeSpline.Subdivide(currentSegment, routeSpline.transform.GetChild(i).GetComponent<CurvySplineSegment>());
            //    }

            //    currentSegment = routeSpline.transform.GetChild(i).GetComponent<CurvySplineSegment>();
            //}

            OBJ_MISSION.Add(route);

            return route;

            // Setup Kalkulasi Waktu dan Coordinate Misi
            dateAtNode = mission.startDate;

            //List<ArcGISLocationComponent> TEMP_PATH_NODE = new List<ArcGISLocationComponent>();
            for(int i=0; i < PATH.Count; i++)
            {
                if(i != 0)
                {
                    double distance = OnlineMapsUtils.DistanceBetweenPointsD(PATH[i - 1], PATH[i]);
                    dateAtNode = dateAtNode.AddSeconds((distance / route.speed * 3600));
                }

                route.AddNewPos(dateAtNode.ToString(), PATH[i]);
                TEMP_PATH_NODE.Add(SpawnNewSegment(route, PATH[i]).GetComponent<ArcGISLocationComponent>());
            }

            await Task.Delay(10);
            for(int i=0; i < TEMP_PATH_NODE.Count; i++)
            {
                Destroy(TEMP_PATH_NODE[i]);
            }

            OBJ_MISSION.Add(route);
            return route;
        }

        /// <summary>
        /// Path Tambahan untuk
        /// - embarkasi & debarkasi
        /// - helicopter landing to and takeoff from ship
        /// - air refueling
        /// </summary>
        /// <param name="route"></param>
        private void GenerateAdditionalPath(MissionWalker route)
        {
            if (route.END_AT_PARENT) GenerateHeliLandOnShipPath(route);
        }

        /// <summary>
        /// Generate Path Tambahan untuk Landing Helicopter di atas kapal
        /// NOTE : Akan menggunakan 3 segment terakhir pada rute jika
        /// - jumlah segment pada rute >= 5
        /// - tiap segment pada 3 segment terakhir tidak berjarak > 10000m dari posisi landing kapal
        /// </summary>
        /// <param name="route"></param>
        /// <returns></returns>
        private void GenerateHeliLandOnShipPath(MissionWalker route)
        {
            var lastSegmentData = route.DATA[route.DATA.Count - 1];
            if (route.transform.childCount < 5)
            {

                for (int i = 0; i < 3; i++)
                {
                    SpawnNewSegment(route, Vector3.zero);
                    route.AddNewPos(lastSegmentData.time, lastSegmentData.coordinate);
                }

                return;
            }

            // Jika tidak memenuhi syarat, generate 3 segment baru
            for (int i = 1; i < 4; i++)
            {
                if (Vector3.Distance(route.transform.GetChild(i).position, route.END_AT_PARENT.position) > 10000)
                {
                    SpawnNewSegment(route, Vector3.zero);
                    route.AddNewPos(lastSegmentData.time, lastSegmentData.coordinate);
                }
            }
        }

        //public async Task GenerateDebarkasiPath(GameObject objParent, GameObject objWalker, MissionWalker misiWalker)
        //{
        //    // Set darimana object harus di-debarkasi
        //    misiWalker.insideEntity = objParent.transform;

        //    // Ignore jika parent tidak memiliki module CargoHold
        //    if (objParent.GetComponent<NodeCargoHold>() == null) return;

        //    // Cek Misi dari parent yang paling mendekati misi debarkasi walker
        //    MissionWalker misiParent = null;
        //    var parentMissions = objParent.GetComponent<EntityWalkerV2>().missions;

        //    for(int i=0; i < parentMissions.Count; i++)
        //    {
        //        if(parentMissions[i].DATA[parentMissions[i].DATA.Count - 1].datetime <= misiWalker.DATA[0].datetime)
        //        {
        //            misiParent = parentMissions[i];
        //            i = parentMissions.Count;
        //        }
        //    }

        //    // Jika Tidak ada misi mendekati, ignore
        //    if (misiParent == null) return;

        //    // Instantiate parent sebagai referensi sementara untuk menentukan titik embarkasi / debarkasinya
        //    var tempParent = Instantiate(objParent, objParent.transform.parent);
        //    Destroy(tempParent.GetComponent<EntityWalkerV2>());

        //    var tempSpline      = tempParent.GetComponent<SplineController>();
        //    tempSpline.Spline   = misiParent.GetComponent<CurvySpline>();
        //    tempSpline.RelativePosition = 1;

        //    await Task.Delay(10);
        //    var firstToFirst = Vector3.Distance(misiParent.transform.GetChild(0).position, misiWalker.transform.GetChild(0).position);
        //    var lastToFirst = Vector3.Distance(misiParent.transform.GetChild(misiParent.transform.childCount - 1).position, misiWalker.transform.GetChild(0).position);

        //    if (firstToFirst < lastToFirst)
        //    {
        //        tempSpline.RelativePosition = 0;
        //    }
        //    else
        //    {
        //        tempSpline.RelativePosition = 1;
        //    }

        //    await Task.Delay(10);
        //    var tempCargoHold   = tempParent.GetComponent<NodeCargoHold>();

        //    float distance = -99;
        //    int selectedDoor = -99;

        //    for (int i=0; i < tempCargoHold.doors.Count; i++)
        //    {
        //        var parentDoorActive = tempCargoHold.doors[i];

        //        var entryExitPoint  = parentDoorActive.paths[parentDoorActive.paths.Count - 1];
        //        var newDistance     = Vector3.Distance(entryExitPoint.transform.position, misiWalker.transform.GetChild(0).position);
            
        //        if(distance == -99)
        //        {
        //            distance        = newDistance;
        //            selectedDoor    = i;
        //            continue;
        //        }

        //        if(distance > newDistance)
        //        {
        //            distance        = newDistance;
        //            selectedDoor    = i;
        //        }
        //    }

        //    await Task.Delay(10);
        //    if (selectedDoor == -99) return;
        //    if (objWalker == null) return;

        //    misiWalker.cargoDoorInAction    = selectedDoor;

        //    var doorData = tempCargoHold.doors[selectedDoor];
        //    List<MissionWalkerParam> newPaths = new List<MissionWalkerParam>();

        //    // Generate path baru mengikuti node yang tersedia di tempCargoHold
        //    for (int i = 0; i < doorData.paths.Count; i++)
        //    {
        //        var pathNode = doorData.paths[i];

        //        var tempRoutePoint = Instantiate(pathNode.gameObject, tempParent.transform.parent);
        //        tempRoutePoint.transform.position = pathNode.transform.position;

        //        var tempRouteLocation = tempRoutePoint.AddComponent<ArcGISLocationComponent>();
                
        //        await Task.Delay(10);
        //        tempRouteLocation.Position = 
        //            new ArcGISPoint(
        //                tempRouteLocation.Position.X,
        //                tempRouteLocation.Position.Y,
        //                tempRouteLocation.Position.Z,
        //                ArcGISSpatialReference.WGS84()
        //            );

        //        var newPos      = new MissionWalkerParam();
        //        newPos.time     = DateTime.Now.ToString();
        //        newPos.datetime = DateTime.Now;
        //        newPos.coordinate = 
        //            new Vector3(
        //                (float)tempRouteLocation.Position.X,
        //                (float)tempRouteLocation.Position.Y,
        //                (float)tempRouteLocation.Position.Z
        //            );

        //        newPaths.Add(newPos);

        //        await Task.Delay(10);
        //        Destroy(tempRoutePoint.gameObject);
        //    }

        //    // Tambahkan path dari misi original walker
        //    for (int i = 0; i < misiWalker.DATA.Count; i++)
        //    {
        //        newPaths.Add(misiWalker.DATA[i]);
        //    }

        //    // Rekalkulasi ulang waktu misi setelah digabungkan dengan node path baru
        //    var dateAtNode = misiWalker.DATA[0].datetime.AddSeconds(10);
        //    for (int i = 0; i < newPaths.Count; i++)
        //    {
        //        double dx, dy;

        //        if (i > 0 && i < (newPaths.Count - 1))
        //        {
        //            var newPathPos = newPaths[i].coordinate;
        //            var newPathPosBefore = newPaths[i - 1].coordinate;

        //            OnlineMapsUtils.DistanceBetweenPoints(newPathPos.x, newPathPos.y, newPathPosBefore.x, newPathPosBefore.y, out dx, out dy);

        //            double stepDistance = Math.Sqrt(dx * dx + dy * dy);

        //            //Total step time
        //            double totalTime = stepDistance / misiWalker.speed * 3600;
        //            dateAtNode = dateAtNode.AddSeconds(totalTime);
        //        }

        //        newPaths[i].datetime = dateAtNode;
        //        newPaths[i].time = dateAtNode.ToString();
        //    }

        //    // Create node baru pada path misi original
        //    for (int i = doorData.paths.Count - 1; i >= 0; i--)
        //    {
        //        var splineNode = Instantiate(prefabNode, misiWalker.transform);
        //        splineNode.transform.SetCoordinate(newPaths[i].coordinate);
        //        splineNode.transform.SetOrientation(new Vector3(0, 0, -90));
        //        splineNode.transform.SetAsFirstSibling();
        //    }

        //    Destroy(tempParent.gameObject);

        //    // Clean data node misi lama menjadi dengan yang baru (hanya memasukkan data baru setelah di generate ulang)
        //    misiWalker.DATA.Clear();
        //    misiWalker.DATA = newPaths;
        //}

        public async Task GenerateLandingOnTargetPath(GameObject target, GameObject satuan, RootMissionWalker overwriteMission)
        {
            Debug.Log("Generate Misi Landing On Ship 3D");

            // Set kemana object harus mendarat
            overwriteMission.jenis  = RootMissionWalker.RouteType.Pergerakan;
            overwriteMission.target = target.transform;

            // Cek Overwritten Mission diposisi mana (progress) object akan landing
            RootMissionWalker targetMission = target.GetComponent<RootWalkerBase>().GetActiveMission(overwriteMission.DATA[0].datetime);
            if (targetMission == null) return;

            var targetMissionProgress = target.GetComponent<RootWalkerBase>().GetActiveMissionNode(targetMission, overwriteMission.DATA[0].datetime);

            var TEMP_TARGET = Instantiate(target, EnvironmentsController.GetMapComponent().transform).GetComponent<SplineController>();
            Destroy(TEMP_TARGET.GetComponent<ARootWalker>());
            Destroy(TEMP_TARGET.GetComponent<SRootWalker>());
            Destroy(TEMP_TARGET.GetComponent<LRootWalker>());

            TEMP_TARGET.Spline = targetMission.GetComponent<CurvySpline>();
            TEMP_TARGET.RelativePosition = targetMissionProgress.GetValueOrDefault(0);

            await Task.Delay(10);
            var targetHelipad = TEMP_TARGET.GetComponent<NodeHelipad>();
            if (targetHelipad == null || targetHelipad.nodes == null || targetHelipad.nodes.Count <= 0) return;

            var helipadPaths = targetHelipad.nodes[0].nodeHoverArea;
            if (helipadPaths == null) return;

            List<MissionWalkerParam> NEW_OVERWRITTEN_PATH = new List<MissionWalkerParam>();
            // Tambahkan path dari misi original walker (kecuali node terakhir karena akan di replace)
            for (int i = 0; i < overwriteMission.DATA.Count - 1; i++)
            {
                NEW_OVERWRITTEN_PATH.Add(overwriteMission.DATA[i]);
            }

            var dateAtNode = NEW_OVERWRITTEN_PATH[NEW_OVERWRITTEN_PATH.Count - 1].datetime;

            // Tambahkan path dari path masuk ramp target
            List<GameObject> extendedSegment = new List<GameObject>();
            for(int i=0; i < 2; i++)
            {
                var rampSegment = Instantiate(prefabNodeV2, overwriteMission.transform);
                Destroy(rampSegment.GetComponent<HPTransform>());

                if(i == 0)
                {
                    rampSegment.transform.position = helipadPaths.position;
                }
                else
                {
                    rampSegment.transform.position = rampSegment.transform.position + (rampSegment.transform.forward * 100);
                }

                var distance = Vector3.Distance(overwriteMission.transform.GetChild(overwriteMission.transform.childCount - 2).transform.position, rampSegment.transform.position);
                double totalTime = (distance / 1000) / overwriteMission.speed * 3600;

                dateAtNode = dateAtNode.AddSeconds(totalTime);

                NEW_OVERWRITTEN_PATH.Add(new MissionWalkerParam
                {
                    coordinate = rampSegment.transform.position,
                    datetime = dateAtNode,
                    time = dateAtNode.ToString()
                });

                extendedSegment.Add(rampSegment);
            }

            Destroy(TEMP_TARGET.gameObject);

            await Task.Delay(10);
            for(int i=0; i < extendedSegment.Count; i++)
            {
                extendedSegment[i].AddComponent<HPTransform>();
            }

            overwriteMission.DATA = NEW_OVERWRITTEN_PATH;
        }

        //public async Task GenerateEmbarkasiPath(GameObject objParent, GameObject objWalker, MissionWalker misiWalker)
        public async Task GenerateEmbarkasiPath(GameObject target, GameObject satuan, RootMissionWalker overwriteMission)
        {
            Debug.Log("Generate Misi Embar3D");

            // Set kemana object harus di-embarkasi
            overwriteMission.target = target.transform;

            // Cek Overwritten Mission diposisi mana (progress) target akan meng-embarkasi satuan
            RootMissionWalker targetMission = target.GetComponent<RootWalkerBase>().GetActiveMission(overwriteMission.DATA[0].datetime);
            if (targetMission == null) return;

            var targetMissionProgress = target.GetComponent<RootWalkerBase>().GetActiveMissionNode(targetMission, overwriteMission.DATA[0].datetime);

            //Debug.Log("Progress : " + targetMissionProgress);

            // Instantiate guidance (untuk diambil spline controller-nya) sebagai referensi sementara untuk menentukan titik embarkasi
            var TEMP_TARGET                 = Instantiate(target, EnvironmentsController.GetMapComponent().transform).GetComponent<SplineController>();
            Destroy(TEMP_TARGET.GetComponent<ARootWalker>());
            Destroy(TEMP_TARGET.GetComponent<SRootWalker>());
            Destroy(TEMP_TARGET.GetComponent<LRootWalker>());

            //if(targetMission != target.GetComponent<RootWalkerBase>().missions[0])
            //{
            //    TEMP_TARGET.Spline = targetMission.GetComponent<CurvySpline>();
            //    TEMP_TARGET.RelativePosition = targetMissionProgress.GetValueOrDefault(0);
            //}

            TEMP_TARGET.Spline = targetMission.GetComponent<CurvySpline>();
            TEMP_TARGET.RelativePosition = targetMissionProgress.GetValueOrDefault(0);

            await Task.Delay(10);
            var targetRamp = TEMP_TARGET.GetComponent<NodeCargoHold>();
            if (targetRamp == null || targetRamp.doors == null || targetRamp.doors.Count <= 0) return;

            var rampPaths = targetRamp.doors[0].paths;
            if (rampPaths == null) return;

            List<MissionWalkerParam> NEW_OVERWRITTEN_PATH = new List<MissionWalkerParam>();

            // Tambahkan path dari misi original walker
            for (int i=0; i < overwriteMission.DATA.Count; i++)
            {
                NEW_OVERWRITTEN_PATH.Add(overwriteMission.DATA[i]);
            }

            var dateAtNode = NEW_OVERWRITTEN_PATH[NEW_OVERWRITTEN_PATH.Count - 1].datetime;

            // Tambahkan path dari path masuk ramp target
            List<GameObject> extendedSegment = new List<GameObject>();
            for (int i = (rampPaths.Count - 1); i >= 0; i--)
            {
                var rampSegment = Instantiate(prefabNodeV2, overwriteMission.transform);
                Destroy(rampSegment.GetComponent<HPTransform>());

                rampSegment.transform.position  = rampPaths[i].position;

                var distance        = Vector3.Distance(overwriteMission.transform.GetChild(overwriteMission.transform.childCount - 2).transform.position, rampSegment.transform.position);
                double totalTime    = (distance / 1000) / overwriteMission.speed * 3600;

                dateAtNode          = dateAtNode.AddSeconds(totalTime);

                NEW_OVERWRITTEN_PATH.Add(new MissionWalkerParam
                {
                    coordinate  = rampSegment.transform.position,
                    datetime    = dateAtNode,
                    time        = dateAtNode.ToString()
                });

                extendedSegment.Add(rampSegment);
            }

            Destroy(TEMP_TARGET.gameObject);

            await Task.Delay(10);
            for(int i=0; i < extendedSegment.Count; i++)
            {
                extendedSegment[i].AddComponent<HPTransform>();
            }

            overwriteMission.DATA = NEW_OVERWRITTEN_PATH;

            //// Set kemana object harus di-embarkasi
            //misiWalker.insideEntity = objParent.transform;

            //// Ignore jika parent tidak memiliki module CargoHold
            //if (objParent.GetComponent<NodeCargoHold>() == null) return;

            //// Cek Misi dari parent yang paling mendekati misi debarkasi walker
            //MissionWalker misiParent = null;
            //var parentMissions = objParent.GetComponent<EntityWalkerV2>().missions;

            //for (int i = 0; i < parentMissions.Count; i++)
            //{
            //    if (parentMissions[i].DATA[parentMissions[i].DATA.Count - 1].datetime <= misiWalker.DATA[0].datetime)
            //    {
            //        misiParent = parentMissions[i];
            //        i = parentMissions.Count;
            //    }
            //}

            //// Jika Tidak ada misi mendekati, ignore
            //if (misiParent == null) return;

            //// Instantiate parent sebagai referensi sementara untuk menentukan titik embarkasi / debarkasinya
            //var tempParent = Instantiate(objParent, objParent.transform.parent);
            //Destroy(tempParent.GetComponent<EntityWalkerV2>());

            //var tempSpline = tempParent.GetComponent<SplineController>();
            //tempSpline.Spline = misiParent.GetComponent<CurvySpline>();
            ////tempSpline.RelativePosition = 0;

            //await Task.Delay(10);
            //var lastToLast = Vector3.Distance(misiParent.transform.GetChild(misiParent.transform.childCount - 1).position, misiWalker.transform.GetChild(misiWalker.transform.childCount - 1).position);
            //var lastToFirst = Vector3.Distance(misiParent.transform.GetChild(0).position, misiWalker.transform.GetChild(misiWalker.transform.childCount - 1).position);

            //if(lastToFirst > lastToLast)
            //{
            //    tempSpline.RelativePosition = 1;
            //}
            //else
            //{
            //    tempSpline.RelativePosition = 0;
            //}

            //await Task.Delay(10);
            //var tempCargoHold = tempParent.GetComponent<NodeCargoHold>();

            //float distance = -99;
            //int selectedDoor = -99;

            //for (int i = 0; i < tempCargoHold.doors.Count; i++)
            //{
            //    var parentDoorActive = tempCargoHold.doors[i];

            //    var entryExitPoint = parentDoorActive.paths[parentDoorActive.paths.Count - 1];
            //    var newDistance = Vector3.Distance(entryExitPoint.transform.position, misiWalker.transform.GetChild(misiWalker.transform.childCount - 1).position);

            //    if (distance == -99)
            //    {
            //        distance = newDistance;
            //        selectedDoor = i;
            //        continue;
            //    }

            //    if (distance > newDistance)
            //    {
            //        distance = newDistance;
            //        selectedDoor = i;
            //    }
            //}

            //await Task.Delay(10);
            //if (selectedDoor == -99) return;
            //if (objWalker == null) return;

            //misiWalker.cargoDoorInAction = selectedDoor;

            //var doorData = tempCargoHold.doors[selectedDoor];
            //List<MissionWalkerParam> newPaths = new List<MissionWalkerParam>();

            //// Tambahkan path dari misi original walker
            //for (int i = 0; i < misiWalker.DATA.Count; i++)
            //{
            //    newPaths.Add(misiWalker.DATA[i]);
            //}

            //// Generate path baru mengikuti node yang tersedia di tempCargoHold
            //for (int i = (doorData.paths.Count - 1); i >= 0; i--)
            //{
            //    var pathNode = doorData.paths[i];

            //    var tempRoutePoint = Instantiate(pathNode.gameObject, tempParent.transform.parent);
            //    tempRoutePoint.transform.position = pathNode.transform.position;

            //    var tempRouteLocation = tempRoutePoint.AddComponent<ArcGISLocationComponent>();

            //    await Task.Delay(10);
            //    tempRouteLocation.Position =
            //        new ArcGISPoint(
            //            tempRouteLocation.Position.X,
            //            tempRouteLocation.Position.Y,
            //            tempRouteLocation.Position.Z,
            //            ArcGISSpatialReference.WGS84()
            //        );

            //    var newPos = new MissionWalkerParam();
            //    newPos.time = DateTime.Now.ToString();
            //    newPos.datetime = DateTime.Now;
            //    newPos.coordinate =
            //        new Vector3(
            //            (float)tempRouteLocation.Position.X,
            //            (float)tempRouteLocation.Position.Y,
            //            (float)tempRouteLocation.Position.Z
            //        );

            //    newPaths.Add(newPos);

            //    await Task.Delay(10);
            //    //Destroy(tempRoutePoint.gameObject);
            //}

            //// Rekalkulasi node path tambahan
            //var dateAtNode = misiWalker.DATA[misiWalker.DATA.Count - 1].datetime;
            //for (int i = misiWalker.DATA.Count; i < newPaths.Count; i++)
            //{
            //    double dx, dy;

            //    if (i < (newPaths.Count - 1))
            //    {
            //        var newPathPos = newPaths[i].coordinate;
            //        var newPathPosBefore = newPaths[i - 1].coordinate;

            //        OnlineMapsUtils.DistanceBetweenPoints(newPathPos.x, newPathPos.y, newPathPosBefore.x, newPathPosBefore.y, out dx, out dy);

            //        double stepDistance = Math.Sqrt(dx * dx + dy * dy);

            //        //Total step time
            //        double totalTime = stepDistance / misiWalker.speed * 3600;
            //        dateAtNode = dateAtNode.AddSeconds(totalTime);
            //    }

            //    newPaths[i].datetime = dateAtNode;
            //    newPaths[i].time = dateAtNode.ToString();
            //}

            //// Create node baru pada path misi original
            //for (int i = misiWalker.DATA.Count; i < newPaths.Count; i++)
            //{
            //    var splineNode = Instantiate(prefabNode, misiWalker.transform);
            //    splineNode.transform.SetCoordinate(newPaths[i].coordinate);
            //    splineNode.transform.SetOrientation(new Vector3(0, 0, -90));
            //    splineNode.transform.SetAsLastSibling();
            //}

            //Destroy(tempParent.gameObject);

            //// Clean data node misi lama menjadi dengan yang baru (hanya memasukkan data baru setelah di generate ulang)
            //misiWalker.DATA.Clear();
            //misiWalker.DATA = newPaths;
        }

        public async Task<RootMissionWalker> GenerateLinudPath(RootMissionWalker targetMission, MisiSatuan mission, float offset = 0)
        {
            // Dibuatkan TEMP Object untuk menentukan posisi terjun object pada targetMission
            var TEMP_TARGET                 = Instantiate(AssetPackageController.Instance.rootWalker, EnvironmentsController.GetMapComponent().transform).AddComponent<SplineController>();

            var indexMission                = (mission.data_properties.point_posisi == 0) ? 0 : mission.data_properties.point_posisi + (2 * (mission.data_properties.point_posisi - 1));

            TEMP_TARGET.Spline              = targetMission.GetComponent<CurvySpline>();
            TEMP_TARGET.RelativePosition    = TEMP_TARGET.Spline.SegmentToTF(TEMP_TARGET.Spline[indexMission.GetValueOrDefault(0)]);

            TEMP_TARGET.AbsolutePosition    = TEMP_TARGET.AbsolutePosition + (float) ((targetMission.speed / 4) * offset);

            await Task.Delay(10);
            var route                       = Instantiate(prefabRouteV2, rootMissionController).GetComponent<RootMissionWalker>();
            var routeSpline                 = route.GetComponent<CurvySpline>();

            routeSpline.Interpolation       = routeInterpolation;

            var target          = EntityController.instance.SATUAN.FirstOrDefault(x => x.nama == mission.id_object).GetComponent<ARootWalker>();

            // Ubah Limit Ketinggian Pesawat yang menerjunkan menjadi maksimal 4000M
            target.altitudeLimit = 3048;

            // Set Data Misi
            route.name          = "LINUD_" + mission.id_mission;
            route.id            = mission.id;
            route.missionId     = "LINUD_" + mission.id_mission;
            route.jenis         = GetRootJenisRoute(route.gameObject, mission.jenis);
            route.speed         = 20;
            route.target        = target.transform;

            List<Vector3> TEMP_PATH_NODE = new List<Vector3>();

            ArcGISPoint routePositon;
            ArcGISRotation routeRotation;

            await Task.Delay(10);
            var tempTargetHPTransform   = TEMP_TARGET.GetComponent<HPTransform>();
            tempTargetHPTransform.UniversePosition = 
                new double3(
                    tempTargetHPTransform.UniversePosition.x + UnityEngine.Random.Range(-25.0f, 25.0f),
                    tempTargetHPTransform.UniversePosition.y,
                    tempTargetHPTransform.UniversePosition.z + UnityEngine.Random.Range(-25.0f, 25.0f)
                );

            tempTargetHPTransform.ToLocationComponent(out routePositon, out routeRotation);

            var segmentCoordinate = new Vector3((float)routePositon.X, (float)routePositon.Y, (float)routePositon.Z);

            SpawnNewRootSegment(route, segmentCoordinate);
            TEMP_PATH_NODE.Add(segmentCoordinate);

            var lastNode = SpawnNewRootSegment(route, new Vector3(segmentCoordinate.x, segmentCoordinate.y, segmentCoordinate.z - 1000));
            TEMP_PATH_NODE.Add(new Vector3(segmentCoordinate.x, segmentCoordinate.y, segmentCoordinate.z - 1000));

            EntityController.GenerateTerrainFollower(lastNode);

            // Setup Kalkulasi Waktu dan Coordinate Misi
            var dateAtNode      = GetMissionTimeByProgress(targetMission, TEMP_TARGET).Value.AddMilliseconds(100);
            route.AddNewPos(dateAtNode.ToString(), TEMP_PATH_NODE[0]);

            var startNode       = TEMP_PATH_NODE[0];
            var landingNode     = TEMP_PATH_NODE[1];

            var distance        = startNode.z - landingNode.z;
            double totalTime    = (distance / 1000) / route.speed * 3600;

            dateAtNode          = dateAtNode.AddSeconds(totalTime);

            route.AddNewPos(dateAtNode.ToString(), TEMP_PATH_NODE[1]);

            //for (int i = 0; i < TEMP_PATH_NODE.Count; i++)
            //{
            //    if (i != 0)
            //    {
            //        var node1 = TEMP_PATH_NODE[i - 1];
            //        var node2 = TEMP_PATH_NODE[i];

            //        if (node1.x == node2.x && node1.y == node2.y)
            //        {
            //            double distance = 0;
            //            if (node1.z >= node2.z)
            //            {
            //                distance = node1.z - node2.z;
            //            }
            //            else
            //            {
            //                distance = node2.z - node1.z;
            //            }

            //            // Total step time
            //            double totalTime = (distance / 1000) / route.speed * 3600;
            //            dateAtNode = dateAtNode.AddSeconds(totalTime);
            //        }
            //        else
            //        {
            //            double dx, dy;
            //            OnlineMapsUtils.DistanceBetweenPoints(node1.x, node1.y, node2.x, node2.y, out dx, out dy);

            //            double stepDistance = Math.Sqrt(dx * dx + dy * dy);

            //            // Total step time
            //            double totalTime = stepDistance / route.speed * 3600;
            //            dateAtNode = dateAtNode.AddSeconds(totalTime);
            //        }
            //    }

            //    route.AddNewPos(dateAtNode.ToString(), new Vector3((float)TEMP_PATH_NODE[i].x, (float)TEMP_PATH_NODE[i].y, (float)TEMP_PATH_NODE[i].z));
            //}

            OBJ_MISSION_V2.Add(route);

            Destroy(TEMP_TARGET.gameObject);

            return route;
        }

        public async Task GenerateDebarkasiPath(GameObject target, GameObject satuan, RootMissionWalker overwriteMission)
        {
            Debug.Log("Generate Misi Embar3D");

            // Set darimana object harus di-debarkasi
            overwriteMission.target = target.transform;

            // Cek Overwritten Mission diposisi mana (progress) target akan meng-debarkasi satuan
            RootMissionWalker targetMission = target.GetComponent<RootWalkerBase>().GetActiveMission(overwriteMission.DATA[0].datetime);
            if (targetMission == null) return;

            var targetMissionProgress = target.GetComponent<RootWalkerBase>().GetActiveMissionNode(targetMission, overwriteMission.DATA[0].datetime);

            var TEMP_TARGET = Instantiate(target, EnvironmentsController.GetMapComponent().transform).GetComponent<SplineController>();
            Destroy(TEMP_TARGET.GetComponent<ARootWalker>());
            Destroy(TEMP_TARGET.GetComponent<SRootWalker>());
            Destroy(TEMP_TARGET.GetComponent<LRootWalker>());

            TEMP_TARGET.Spline = targetMission.GetComponent<CurvySpline>();
            TEMP_TARGET.RelativePosition = targetMissionProgress.GetValueOrDefault(0);

            await Task.Delay(10);
            var targetRamp = TEMP_TARGET.GetComponent<NodeCargoHold>();
            if (targetRamp == null || targetRamp.doors == null || targetRamp.doors.Count <= 0) return;

            var rampPaths = targetRamp.doors[0].paths;
            if (rampPaths == null) return;

            List<MissionWalkerParam> NEW_OVERWRITTEN_PATH = new List<MissionWalkerParam>();

            var dateAtNode = overwriteMission.DATA[0].datetime;

            // Tambahkan path dari path keluar ramp target
            List<GameObject> extendedSegment = new List<GameObject>();
            for (int i = (rampPaths.Count - 1); i >= 0; i--)
            {
                var rampSegment = Instantiate(prefabNodeV2, overwriteMission.transform);
                Destroy(rampSegment.GetComponent<HPTransform>());

                rampSegment.transform.SetAsFirstSibling();

                rampSegment.transform.position = rampPaths[i].position;
            }
            
            for (int i=0; i < rampPaths.Count; i++)
            {
                //var rampSegment = Instantiate(prefabNodeV2, overwriteMission.transform);
                //Destroy(rampSegment.GetComponent<HPTransform>());

                // rampSegment.transform.position = rampPaths[i].position;

                var rampSegment = overwriteMission.transform.GetChild(i).gameObject;

                if(i > 0)
                {
                    var distance        = Vector3.Distance(overwriteMission.transform.GetChild(overwriteMission.transform.childCount - 2).transform.position, rampSegment.transform.position);
                    double totalTime    = (distance / 1000) / overwriteMission.speed * 3600;

                    dateAtNode          = dateAtNode.AddSeconds(totalTime);
                }

                NEW_OVERWRITTEN_PATH.Add(new MissionWalkerParam
                {
                    coordinate = rampSegment.transform.position,
                    datetime = dateAtNode,
                    time = dateAtNode.ToString()
                });

                extendedSegment.Add(rampSegment);
            }

            // Tambahkan path dari misi original walker
            for (int i = 0; i < overwriteMission.DATA.Count; i++)
            {
                var distance = Vector3.Distance(NEW_OVERWRITTEN_PATH[NEW_OVERWRITTEN_PATH.Count - 1].coordinate, overwriteMission.transform.GetChild(i).transform.position);
                double totalTime = (distance / 1000) / overwriteMission.speed * 3600;

                dateAtNode = dateAtNode.AddSeconds(totalTime);

                overwriteMission.DATA[i].datetime   = dateAtNode;
                overwriteMission.DATA[i].time = dateAtNode.ToString();

                NEW_OVERWRITTEN_PATH.Add(overwriteMission.DATA[i]);
            }

            Destroy(TEMP_TARGET.gameObject);

            await Task.Delay(10);
            for (int i = 0; i < extendedSegment.Count; i++)
            {
                extendedSegment[i].AddComponent<HPTransform>();
            }

            overwriteMission.DATA = NEW_OVERWRITTEN_PATH;
        }

        private MissionWalker.RouteType GetJenisRoute(GameObject objRoute, string jenis)
        {
            Debug.Log(jenis);

            switch (jenis)
            {
                case "pergerakan":
                    return MissionWalker.RouteType.Pergerakan;
                case "embar":
                    objRoute.AddComponent<CargoHoldController>();

                    return MissionWalker.RouteType.Embarkasi;
                case "debar":
                    objRoute.AddComponent<CargoHoldController>();

                    return MissionWalker.RouteType.Debarkasi;
                case "paratroopsTFG":
                    return MissionWalker.RouteType.Penerjunan_TFG;
                case "paratroopsWGS":
                    return MissionWalker.RouteType.Penerjunan_WGS;
                default: return MissionWalker.RouteType.Pergerakan;
            }
        }

        private RootMissionWalker.RouteType GetRootJenisRoute(GameObject objRoute, string jenis)
        {
            switch (jenis)
            {
                case "pergerakan":
                    return RootMissionWalker.RouteType.Pergerakan;
                case "embar":
                    objRoute.AddComponent<CargoHoldController>();

                    return RootMissionWalker.RouteType.Embarkasi;
                case "debar":
                    objRoute.AddComponent<CargoHoldController>();

                    return RootMissionWalker.RouteType.Debarkasi;
                case "linud":
                    return RootMissionWalker.RouteType.Linud;
                case "paratroopsTFG":
                    return RootMissionWalker.RouteType.Penerjunan_TFG;
                case "paratroopsWGS":
                    return RootMissionWalker.RouteType.Penerjunan_WGS;
                    break;
                default: return RootMissionWalker.RouteType.Pergerakan;
            }
        }

        public void RebaseMission()
        {
            for (int i = 0; i < OBJ_MISSION.Count - 1; i++)
            {
                OBJ_MISSION[i].GetComponent<CurvySpline>().SyncSplineFromHierarchy();
            }
        }

        private GameObject SpawnNewSegment(MissionWalker route, Vector3 PATH)
        {
            var newSegment = Instantiate(prefabNode, route.transform);

            newSegment.transform.SetCoordinate(PATH);
            newSegment.transform.SetOrientation(new Vector3(0, 0, -90));

            return newSegment;
        }

        private GameObject SpawnNewRootSegment(RootMissionWalker route, Vector3 PATH)
        {
            var newSegment = Instantiate(prefabNodeV2, route.transform);

            newSegment.transform.SetRootCoordinate(PATH);
            newSegment.transform.SetRootOrientation(new Vector3(0, 0, -90f));

            return newSegment;
        }

        private GameObject SpawnNewRootSegmentV2(Vector3 PATH)
        {
            var newSegment = Instantiate(prefabNodeV2, missionContainer);

            newSegment.transform.SetRootCoordinate(PATH);
            newSegment.transform.SetRootOrientation(new Vector3(0, 0, -90f));

            return newSegment;
        }

        private DateTime? GetMissionTimeByProgress(RootMissionWalker mission, SplineController splineMission)
        {
            if (mission.DATA.Count <= 0) return null;

            var percentageDiff = splineMission.Length / splineMission.AbsolutePosition;
            TimeSpan missionTimespan = mission.DATA[mission.DATA.Count - 1].datetime - mission.DATA[0].datetime;

            return mission.DATA[0].datetime.AddMilliseconds((missionTimespan.TotalMilliseconds / percentageDiff));
        }

        private float? GetMissionProgress(RootMissionWalker mission, DateTime time)
        {
            if (mission.DATA.Count <= 0) return null;

            if (time < mission.GetMissionTime(0))
            {
                return 0;
            }
            else if (time > mission.GetLastNode().datetime)
            {
                return 1;
            }
            else
            {
                var startDate   = mission.GetFirstNode().datetime;
                var endDate     = mission.GetLastNode().datetime;
                var totalTime   = (endDate - startDate).TotalSeconds;

                var tempProgress = ((time.Subtract(startDate).TotalSeconds) / totalTime);

                return (float)(mission.GetComponent<CurvySpline>().Length * (tempProgress * 100) / 100);
            }
        }
    }
}