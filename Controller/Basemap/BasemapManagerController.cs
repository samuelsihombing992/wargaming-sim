using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

using TMPro;
using Esri.ArcGISMapsSDK.Components;

using Wargaming.Core;
using Wargaming.Core.GlobalParam;

public class BasemapManagerController : MonoBehaviour
{
    [Header("Config File")]
    public string basemapFile;

    [Header("Form Data")]
    [SerializeField] private TMP_InputField basemapName;
    [SerializeField] private TMP_InputField source;
    [SerializeField] private TMP_Dropdown sourceType;
    [SerializeField] private TMP_Dropdown basemapType;
    [SerializeField] private TMP_Dropdown status;
    [SerializeField] private Toggle setAsDefault;

    [SerializeField] private Button submitButton;
    [SerializeField] private Button fileBrowseButton;
    [SerializeField] private Button deleteButton;

    [Header("Manager References")]
    [SerializeField] private Transform listManagerContainer;
    [SerializeField] private GameObject prefabListManager;

    [Header("Events")]
    public UnityEvent OnCreate;
    public UnityEvent OnEdit;
    public UnityEvent AfterSubmit;

    public int? selected = null;

    private void Start()
    {
        // Load BASEMAP Data from "File"
        if (GlobalMapService.BASEMAP_DATA == null)
        {
            //new Basemaps();
            GlobalMapService.BASEMAP_DATA = new List<BasemapData>();
        }

        LoadBasemapFromFile();

        gameObject.SetActive(false);
    }

    #region REQUEST
    private void LoadBasemapFromFile()
    {
        // If Basemap File is not defined, throw error message
        if (basemapFile == null)
        {
            Debug.LogError("Error, Basemap Service File is not defined. Please provide one!");
            return;
        }

        var path = Path.Combine(Directory.GetCurrentDirectory(), basemapFile);
        if (File.Exists(path))
        {
            // Use Current Connection Document if exists
            var _XMLSerialize = new XmlSerializer(typeof(List<BasemapData>));
            using (var reader = new StreamReader(path))
            {
                GlobalMapService.BASEMAP_DATA = (List<BasemapData>)_XMLSerialize.Deserialize(reader);
            }
        }
        else
        {
            // Creating New Sample
            AddBasemapToList("Physical", "https://services.arcgisonline.com/ArcGIS/rest/services/World_Physical_Map/MapServer", BasemapTypes.ImageLayer, false, true, false);
            AddBasemapToList("Street Map", "https://services.arcgisonline.com/ArcGIS/rest/services/World_Street_Map/MapServer", BasemapTypes.ImageLayer, false, true, false);
            AddBasemapToList("World Topographic", "https://services.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer", BasemapTypes.ImageLayer, false, true, false);
            AddBasemapToList("Shaded Relief", "https://services.arcgisonline.com/ArcGIS/rest/services/World_Shaded_Relief/MapServer", BasemapTypes.ImageLayer, false, true, false);
            AddBasemapToList("National Geographic", "https://services.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer", BasemapTypes.ImageLayer, false, true, false);
            AddBasemapToList("World Ocean Base", "https://services.arcgisonline.com/ArcGIS/rest/services/Ocean/World_Ocean_Base/MapServer", BasemapTypes.ImageLayer, false, true, false);
            AddBasemapToList("Light Gray Canvas", "https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer", BasemapTypes.ImageLayer, false, true, false);
            AddBasemapToList("Dark Gray Canvas", "https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Dark_Gray_Base/MapServer", BasemapTypes.ImageLayer, false, true, false);
        }

        // Add List Content Into ListView
        RefreshBasemapManagerList();
    }
    #endregion

    #region SETTINGS_BASEMAP_MANAGER
    public void ResetBasemapManagerForm()
    {
        selected = null;

        basemapName.reset();
        source.reset();
        sourceType.reset();
        basemapType.reset();
        status.reset();

        setAsDefault.isOn = false;
    }

    public void OnCreateNewBasemap()
    {
        ResetBasemapManagerForm();

        fileBrowseButton.GetComponentInChildren<TextMeshProUGUI>().text = "Browse File...";

        OnCreate.Invoke();
    }

    private void OnEditCurrentBasemap(int? _index)
    {
        ResetBasemapManagerForm();

        if (!_index.HasValue) return;

        selected = _index.Value;
        var basemap = GlobalMapService.BASEMAP_DATA[selected.Value];

        basemapName.text    = basemap.basemapName;
        source.text         = basemap.basemapSource;
        basemapType.value   = GetBasemapIndexByType(basemap.basemapType);
        status.value        = (basemap.active) ? 0 : 1;
        setAsDefault.isOn   = basemap.is_default;

        if (!basemap.file)
        {
            sourceType.value = 0;
        }

        fileBrowseButton.GetComponentInChildren<TextMeshProUGUI>().text = "Browse File...";

        OnEdit.Invoke();
    }

    public void OnSubmit()
    {
        if (basemapName.text == "" || source.text == "") return;

        var basemap = (selected.HasValue) ? GlobalMapService.BASEMAP_DATA[selected.Value] : new BasemapData();

        basemap.basemapId       = (selected.HasValue) ? basemap.basemapId : Guid.NewGuid().ToString();
        basemap.basemapName     = basemapName.text;
        basemap.basemapSource   = source.text;
        basemap.file            = (sourceType.value == 0) ? false : true;
        basemap.basemapType     = GetBasemapTypeByValue(basemapType.value);
        basemap.active          = (status.value == 0) ? true : false;
        basemap.is_default      = setAsDefault.isOn;

        if (!selected.HasValue)
        {
            GlobalMapService.BASEMAP_DATA.Add(basemap);
        }

        SaveChanged();
        RefreshBasemapManagerList();

        //if (basemap.is_default != setAsDefault.isOn)
        //{
        //    SetBasemapToDefault(selected.Value);
        //}

        AfterSubmit.Invoke();
    }

    public void DeleteBasemap()
    {
        if (!selected.HasValue) return;

        GlobalMapService.BASEMAP_DATA.RemoveAt(selected.Value);

        SaveChanged();
        RefreshBasemapManagerList();

        AfterSubmit.Invoke();
    }

    public void OnSourceTypeChange()
    {
        source.reset();

        if(sourceType.value == 0)
        {
            source.transform.parent.gameObject.SetActive(true);
            fileBrowseButton.transform.parent.gameObject.SetActive(false);
        }
        else
        {
            source.transform.parent.gameObject.SetActive(false);
            fileBrowseButton.transform.parent.gameObject.SetActive(true);
        }
    }

    public void BrowseBasemapFile()
    {
        //string path = EditorUtility.OpenFilePanel("Load a .tpk/.tpkx file", "", "tpk,tpkx");
        //if (path.Length != 0)
        //{
        //    try
        //    {
        //        if (Path.GetExtension(path).Equals(".tpk", StringComparison.OrdinalIgnoreCase) || Path.GetExtension(path).Equals(".tpkx", System.StringComparison.OrdinalIgnoreCase))
        //        {
        //            fileBrowseButton.GetComponentInChildren<TextMeshProUGUI>().text = Path.GetFileName(path);
        //        }
        //    }
        //    catch (System.Exception ex)
        //    {
        //        Debug.LogError("Failed to load file: " + ex.Message);
        //    }
        //}

        //source.text = path;
    }

    private void AddBasemapToList(string _basemapName, string _source, BasemapTypes _type, bool _file = false, bool _active = false, bool _isDefault = false)
    {
        if (GlobalMapService.BASEMAP_DATA == null) return;

        GlobalMapService.BASEMAP_DATA.Add(new BasemapData
        {
            basemapId       = Guid.NewGuid().ToString(),
            basemapName     = _basemapName,
            basemapSource   = _source,
            basemapType     = _type,
            file            = _file,
            is_default      = _isDefault,
            active          = _active
        });

        SaveChanged();
        RefreshBasemapManagerList();
    }

    private void RefreshBasemapManagerList()
    {
        if (listManagerContainer == null) return;

        listManagerContainer.DestroyAllChild();

        var settings = GameSettingsController.instance;
        if (settings != null)
        {
            settings.basemapContainer.DestroyAllChild();
        }

        for (int i = 0; i < GlobalMapService.BASEMAP_DATA.Count; i++)
        {
            AddBasemapToBasemapManager(i, GlobalMapService.BASEMAP_DATA[i]);

            // List Basemap on "GameSettingsController"
            if (settings != null)
            {
                AddBasemapToBasemapManager(i, GlobalMapService.BASEMAP_DATA[i], settings.prefabListBasemap, settings.basemapContainer);
            }
        }
    }

    private void AddBasemapToBasemapManager(int _index, BasemapData basemap, GameObject prefabList = null, Transform listContainer = null)
    {   
        var list = Instantiate((prefabList == null) ? prefabListManager : prefabList, (listContainer == null) ? listManagerContainer : listContainer);
        var metadata = list.GetMetadata();

        if (metadata == null) return;

        list.name = "BASEMAP_SERVICE_" + _index;

        var listToggle = list.GetComponent<Toggle>();
        if (listToggle != null)
        {
            listToggle.group = list.GetComponentInParent<ToggleGroup>();
        }

        var listMenuToggler = list.GetComponent<MenuToggler>();
        if (listMenuToggler != null)
        {
            listMenuToggler.onToggleOn.AddListener(delegate { OnEditCurrentBasemap(_index); });
            listMenuToggler.onToggleOff.AddListener(delegate { AfterSubmit.Invoke(); });
        }

        var listToggleActive = metadata.GetMetadataToggle("toggle");
        if (listToggleActive != null)
        {
            listToggleActive.isOn = basemap.active;
            listToggleActive.onValueChanged.AddListener(delegate { ToggleBasemap(_index, true); });
        }

        var listLabel = metadata.GetMetadataLabel("name");
        if (listLabel != null)
        {
            listLabel.text = basemap.basemapName;
        }
    }

    private BasemapTypes GetBasemapTypeByValue(int index)
    {
        switch (index)
        {
            case 0:
                return BasemapTypes.ImageLayer;
            case 1:
                return BasemapTypes.Basemap;
            case 2:
                return BasemapTypes.VectorTileLayer;
            default:
                return BasemapTypes.ImageLayer;
        }
    }

    private int GetBasemapIndexByType(BasemapTypes type)
    {
        switch (type)
        {
            case BasemapTypes.ImageLayer:
                return 0;
            case BasemapTypes.Basemap:
                return 1;
            case BasemapTypes.VectorTileLayer:
                return 2;
            default:
                return 0;
        }
    }

    private void SaveChanged()
    {
        // Serialize and Write Changed into Basemap Document
        var _XMLSerialize = new XmlSerializer(typeof(List<BasemapData>));
        using (var writer = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), basemapFile)))
        {
            _XMLSerialize.Serialize(writer, GlobalMapService.BASEMAP_DATA);
        }
    }
    #endregion

    #region ACTION
    private void ToggleBasemap(int _index, bool saveChange = false)
    {
        GlobalMapService.BASEMAP_DATA[_index].active = !GlobalMapService.BASEMAP_DATA[_index].active;

        if (saveChange) { SaveChanged(); }
    }

    private void SetBasemapToDefault(int _index, bool saveChange = false)
    {
        for(int i=0; i < GlobalMapService.BASEMAP_DATA.Count; i++)
        {
            if (GlobalMapService.BASEMAP_DATA[i].is_default)
            {
                GlobalMapService.BASEMAP_DATA[i].is_default = false;
                break;
            }
        }

        GlobalMapService.BASEMAP_DATA[_index].is_default = true;

        if (saveChange) { SaveChanged(); }
    }

    public static void SetUseBasemapService(bool state)
    {
        GlobalMapService.useBasemapService = state;
        
    }
    #endregion
}
