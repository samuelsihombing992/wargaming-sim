using System;
using System.Xml.Serialization;

using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Security;

[Serializable]
public class BasemapData
{
    public enum BasemapService { ARCGIS }

    [XmlAttribute("attrid")]
    public string basemapId { get; set; }
    [XmlAttribute("name")]
    public string basemapName { get; set; }
    [XmlElement("source")]
    public string basemapSource { get; set; }
    [XmlElement("file")]
    public bool file { get; set; }
    [XmlElement("type")]
    public BasemapTypes basemapType { get; set; }
    [XmlElement("is_default")]
    public bool is_default { get; set; }
    [XmlElement("active")]
    public bool active { get; set; }
}