using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using TMPro;
using System.Threading.Tasks;

public class LoadingControllerV2 : MonoBehaviour
{
    public enum LoadingOverlay {
        OVERLAY,
        OVERLAY_NO_BACKGROUND,
    }

    public enum LoadingScreenSide
    {
        SCREEN_SIDE_TOP_LEFT,
        SCREEN_SIDE_TOP_RIGHT,
        SCREEN_SIDE_BOTTOM_LEFT,
        SCREEN_SIDE_BOTTOM_RIGHT
    }

    [Header("Loading Data")]
    public GameObject overlay;
    public GameObject topLeft;
    public GameObject topRight;
    public GameObject bottomLeft;
    public GameObject bottomRight;

    private static List<GameObject> loadingComponent = new List<GameObject>();

    private static GameObject _overlay;
    private static GameObject _topLeft;
    private static GameObject _topRight;
    private static GameObject _bottomLeft;
    private static GameObject _bottomRight;

    #region INSTANCE
    public static LoadingControllerV2 instance;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;

            _overlay = overlay;
            _topLeft = topLeft;
            _topRight = topRight;
            _bottomLeft = bottomLeft;
            _bottomRight = bottomRight;

            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    //private void Awake()
    //{
    //    _overlay        = overlay;
    //    _topLeft        = topLeft;
    //    _topRight       = topRight;
    //    _bottomLeft     = bottomLeft;
    //    _bottomRight    = bottomRight;
    //}

    public static GameObject ShowOverlayLoading(LoadingOverlay type, Transform parent, string message = null, string customAnim = null)
    {
        var loading = Instantiate(_overlay, parent);

        loading.GetComponent<Metadata>().FindParameter("background").SetActive(type != LoadingOverlay.OVERLAY_NO_BACKGROUND);
        
        var messageComponent = loading.GetComponent<Metadata>().FindParameter("message");
        messageComponent.SetActive(message != null);

        if (message != null)
        {
            messageComponent.GetComponent<TextMeshProUGUI>().text = message;
        }

        loadingComponent.Add(loading);
        loading.GetComponent<Animator>().Play((customAnim != null) ? customAnim : "FadeIn");

        return loading;
    }

    public static GameObject ShowSideScreenLoading(LoadingScreenSide type, string message = null, string customAnim = null)
    {
        var loading = Instantiate(GetSreenSideLoading(type), instance.transform);

        var messageComponent = loading.GetComponent<Metadata>().FindParameter("message");
        messageComponent.SetActive(message != null);

        if (message != null)
        {
            messageComponent.GetComponent<TextMeshProUGUI>().text = message;
        }

        loadingComponent.Add(loading);
        loading.GetComponent<Animator>().Play((customAnim != null) ? customAnim : "FadeIn");

        return loading;
    }

    public static async void HideLoading(GameObject obj, string customAnim = null)
    {
        obj.GetComponent<Animator>().Play((customAnim != null) ? customAnim : "FadeOut");
        loadingComponent.Remove(obj);

        await Task.Delay(2000);
        Destroy(obj);
    }

    private static GameObject GetSreenSideLoading(LoadingScreenSide type)
    {
        switch (type)
        {
            case LoadingScreenSide.SCREEN_SIDE_TOP_LEFT:
                return _topLeft;
            case LoadingScreenSide.SCREEN_SIDE_TOP_RIGHT:
                return _topRight;
            case LoadingScreenSide.SCREEN_SIDE_BOTTOM_LEFT:
                return _bottomLeft;
            case LoadingScreenSide.SCREEN_SIDE_BOTTOM_RIGHT:
                return _bottomRight;
            default: return _topRight;
        }
    }
}
