using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsFinder : MonoBehaviour
{
    [SerializeField] Bounds boundingData;

    public Bounds GetBoundingBox(GameObject obj)
    {
        var renderers = obj.GetComponentsInChildren<Renderer>();
        if (renderers.Length == 0) return new Bounds(obj.transform.position, Vector3.zero);

        var b = renderers[0].bounds;
        foreach (Renderer r in renderers)
        {
            b.Encapsulate(r.bounds);
        }
        return b;
    }

    // Update is called once per frame
    void Update()
    {
        boundingData = GetBoundingBox(gameObject);
    }
}
