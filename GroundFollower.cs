using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundFollower : MonoBehaviour
{
    public GameObject carModel;
    public Transform raycastPoint;
    private float hoverHeight = 1.0f;
    private float speed = 20.0f;
    private float terrainHeight;
    private float rotationAmount;
    private RaycastHit hit;
    private Vector3 pos;
    private Vector3 forwardDirection;

    void Update()
    {
        // Keep at specific height above terrain
        pos = transform.position;

        // Rotate to align with terrain
        Physics.Raycast(raycastPoint.position, Vector3.down, out hit);
        transform.up -= (transform.up - hit.normal) * 0.1f;

        // Rotate with input
        rotationAmount = Input.GetAxis("Horizontal") * 120.0f;
        rotationAmount *= Time.deltaTime;
        carModel.transform.Rotate(0.0f, rotationAmount, 0.0f);
        // Move forward
        forwardDirection = carModel.transform.forward;
        transform.position += forwardDirection * Time.deltaTime * speed;
    }
}
