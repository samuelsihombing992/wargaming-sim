using UnityEngine;
using UnityEngine.UI;

namespace Wargaming.Components.UI
{
    public class UI3D : MonoBehaviour
    {
        public RawImage viewer;
        public Transform projectorContainer;
        public View3DCamController projectorCam;

        private GameObject OBJ;

        public void Init(GameObject obj, Vector3? customPos = null, Vector3? customRot = null)
        {
            if (obj == null) return;

            CleanContainer();

            // Spawn clone of selected object satuan into Viewer 3D container
            var objClone = Instantiate(obj, projectorContainer);
            OBJ = objClone;

            objClone.transform.position = customPos.GetValueOrDefault(Vector3.zero);
            objClone.transform.rotation = Quaternion.Euler(customRot.GetValueOrDefault(new Vector3(-90,0,0)));
            objClone.SetLayerRecursively(10);

            // Initialize viewer3D cam
            var camViewer3D = Instantiate(projectorCam, projectorContainer).GetComponent<View3DCamController>();
            camViewer3D.transform.position = Vector3.zero;

            camViewer3D.init(OBJ, viewer.gameObject, null);

            //camViewer3D.init(OBJ, viewer.gameObject, );
        }

        private void CleanContainer()
        {
            foreach (Transform child in projectorContainer)
            {
                Destroy(child.gameObject);
            }
        }
    }
}