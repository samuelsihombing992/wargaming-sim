using Esri.ArcGISMapsSDK.Components;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Wargaming.Components.Playback;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;
using Wargaming.Tacview;
using WGSBundling.Core;

namespace Wargaming.Tacview.CSV
{
    public class TacviewCSVRecorder : MonoBehaviour
    {
        [SerializeField] public float syncTime = 0.4f; // Record every X-Time
        public int multiplier = 0;

        [SerializeField] public float syncPeriod = 0.0f;
        [SerializeField] public bool resyncTime = false;

        public string filename = "";

        public List<TacviewCSVData> rec = new List<TacviewCSVData>();

        private void Awake()
        {
            multiplier = 0;
        }

        private void Start()
        {
            var dir = Directory.GetCurrentDirectory() + "/Tacmap/CSV Record";
            filename = dir + "/" + "Record-" + SkenarioAktif.NAMA_SKENARIO + ".csv";
        }

        private void Update()
        {
            if (EntityController.instance.SATUAN.Count <= 0) return;

            if (syncPeriod > syncTime) resyncTime = true;
            syncPeriod += Time.deltaTime;

            if (!resyncTime) return;
            if (WargamingCam.instance.onRebase) return;

            var timestamp = new DateTimeOffset(PlaybackController.CURRENT_TIME).ToUnixTimeSeconds();

            for (int i=0; i < EntityController.instance.SATUAN.Count; i++)
            {
                var satuan = EntityController.instance.SATUAN[i];
                var location = satuan.GetComponent<ArcGISLocationComponent>();

                if (satuan == null || location == null) continue;

                var core = satuan.GetComponent<WGSBundleCore>();

                rec.Add(new TacviewCSVData
                {
                    Altitude = location.Position.Z,
                    Latitude = location.Position.Y,
                    Longtitude = location.Position.X,
                    Pitch = core.parent.eulerAngles.x,
                    Yaw = location.Rotation.Heading,
                    Roll = core.parent.eulerAngles.z,
                    Timestamp = timestamp,

                    Type = "Air+FixedWing",
                    Color = "Group",
                    Coalition = "Allies",
                    Name = satuan.nama
                    //Time = (time * PlaybackController.instance.speedFactor).ToString()
                    //Time = PlaybackController.CURRENT_TIME.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'")
                });
            }

            multiplier++;

            syncPeriod = 0;
            resyncTime = false;
        }

        //private void Start()
        //{
        //    multiplier = 0;

        //    var dir = Directory.GetCurrentDirectory() + "/Tacmap/CSV Record";
        //    if (!Directory.Exists(dir)) { Directory.CreateDirectory(dir); }

        //    filename = dir + "/" + gameObject.name + "[Blue].csv";
        //}

        //private void Update()
        //{
        //    var locationComponent = GetComponent<ArcGISLocationComponent>();

        //    if (locationComponent == null) { enabled = false; return; }

        //    if (syncPeriod > syncTime) resyncTime = true;
        //    syncPeriod += Time.deltaTime;

        //    if (!resyncTime) return;
        //    if (WargamingCam.instance.onRebase) return;

        //    var timestamp = new DateTimeOffset(PlaybackController.CURRENT_TIME).ToUnixTimeSeconds();

        //    var time = ((syncTime * 10) * multiplier);

        //    rec.Add(new TacviewCSVData
        //    {
        //        Altitude = locationComponent.Position.Z,
        //        Latitude = locationComponent.Position.Y,
        //        Longtitude = locationComponent.Position.X,
        //        Pitch = GetComponent<WGSBundleCore>().parent.eulerAngles.x,
        //        Yaw = locationComponent.Rotation.Heading,
        //        Roll = GetComponent<WGSBundleCore>().parent.eulerAngles.z,
        //        //Time = (time * PlaybackController.instance.speedFactor).ToString()
        //        Timestamp = timestamp,
        //        //Time = PlaybackController.CURRENT_TIME.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'")
        //    });

        //    multiplier++;

        //    syncPeriod = 0;
        //    resyncTime = false;
        //}
    }
}