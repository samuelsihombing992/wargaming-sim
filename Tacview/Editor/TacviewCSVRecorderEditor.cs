using System.Collections.Generic;

using UnityEngine;

using Wargaming.Tacview.CSV;
using Wargaming.Tacview;
using System.IO;
using System.Globalization;
using WGSBundling.Core;
using System;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(TacviewCSVRecorder))]
public class TacviewCSVRecorderEditor : Editor
{
    #region Variables
    protected TacviewCSVRecorder targetRecorder;

    SerializedProperty _rec;
    #endregion

    void OnEnable()
    {
        targetRecorder = (TacviewCSVRecorder)target;

        _rec = serializedObject.FindProperty("rec");
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Wargaming - Tacview CSV Recorder (V1)");
        EditorGUILayout.LabelField("By: Aryaputra BIP");

        EditorGUILayout.Space(5);
        if (targetRecorder.rec.Count >= 1)
        {
            var startTime   = DateTimeOffset.FromUnixTimeMilliseconds((long) targetRecorder.rec[0].Timestamp).DateTime;
            var lastTime    = DateTimeOffset.FromUnixTimeMilliseconds((long) targetRecorder.rec[targetRecorder.rec.Count - 1].Timestamp).DateTime;

            EditorGUILayout.LabelField("Record Time : " + lastTime.Subtract(startTime).ToString("hh\\:mm\\:ss"));
        }
        else
        {
            EditorGUILayout.LabelField("Record Time : 00:00:00");
        }

        EditorGUILayout.Space(5);
        if (GUILayout.Button("Export Record"))
        {
            WriteCSV();
        }

        if (GUILayout.Button("Reset"))
        {
            targetRecorder.rec = new List<TacviewCSVData>();
        }
    }

    public void WriteCSV()
    {
        if(targetRecorder.rec.Count > 0)
        {
            TextWriter tw = new StreamWriter(targetRecorder.filename, false);
            tw.WriteLine("Timestamp,Longitude,Latitude,Altitude,Roll (deg),Pitch (deg),Yaw (deg), Type, Color, Coalition, Name");

            //tw = new StreamWriter(targetRecorder.filename, false);
            for(int i=0; i < targetRecorder.rec.Count; i++)
            {
                var recorderSelected = targetRecorder.rec[i];

                tw.WriteLine(
                    recorderSelected.Timestamp + "," +
                    //recorderSelected.Timestamp + "," +
                    recorderSelected.Longtitude + "," +
                    recorderSelected.Latitude + "," +
                    recorderSelected.Altitude + "," +
                    recorderSelected.Roll + "," +
                    recorderSelected.Pitch + "," +
                    recorderSelected.Yaw + "," +

                    recorderSelected.Type + "," +
                    recorderSelected.Color + "," +
                    recorderSelected.Coalition + "," +
                    recorderSelected.Name
                );
            }
            tw.Close();
        }
    }
}
#endif