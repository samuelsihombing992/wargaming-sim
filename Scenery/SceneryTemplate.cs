namespace Wargaming.Components.Scenery
{
    using System.Collections.Generic;

    using Unity.Mathematics;
    using UnityEngine;

    public class SceneryTemplate : MonoBehaviour
    {
        public string sceneryName;
        public bool activated = false;

        public List<PlacementObject> sceneryAssets = new List<PlacementObject>();
    }

    [System.Serializable]
    public class SceneryObject
    {
        public GameObject obj;

        public double3 location;
        public double3 rotation;
    }
}