namespace Wargaming.Components.Scenery
{
    using System.Collections;
    using System.Collections.Generic;

    using UnityEngine;

    using TMPro;
    using UIWidgets;

    using Wargaming.Components.UI;

    public class SceneryEditor : MonoBehaviour
    {
        [Header("Form Data")]
        public TMP_InputField sceneryName;

        [Header("Model Viewer")]
        public UI3D modelViewer;

        [Header("Asset Browser")]
        public TreeView assetList;
        public TMP_InputField searchAsset;
        public TMP_InputField filterAsset;

        [Header("Container")]
        public Transform sceneryContainer;
        public Transform sceneryTempContainer;

        private GameObject selectedAsset;
        private SceneryTemplate selectedTemplate;

        private ObservableList<TreeNode<TreeViewItem>> ASSETS;

        #region INSTANCE
        public static SceneryEditor instance;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        private void Start()
        {
            
        }

        #region FORM INPUT

        #endregion

        #region ASSET LOADER
        public void InitAssetFilter()
        {
            List<string> options = new List<string> { "ALL" };
        }
        #endregion
    }
}