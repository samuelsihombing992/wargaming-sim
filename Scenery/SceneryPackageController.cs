namespace Wargaming.Components.Scenery
{
    using System.IO;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using UnityEngine;
    using UnityEngine.UI;

    using TMPro;
    using System;
    using System.Xml.Serialization;

    public class SceneryPackageController : MonoBehaviour
    {
        [Header("Format")]
        public string sceneryFormat     = "scenery";
        public string templateFormat    = "tsc";

        [Header("DATA")]
        public List<string> SCENERY_NAME = new List<string>();

        [SerializeField]
        public List<GameObject> SCENERY_DATA = new List<GameObject>();

        [SerializeField]
        public List<SceneryTemplate> SCENERY_TEMPLATE = new List<SceneryTemplate>();

        [Header("Sample Template")]
        [SerializeField] private SceneryTemplate sampleTemplate;

        #region INSTANCE
        public static SceneryPackageController Instance;
        void Start()
        {
            if (Instance == null)
            {
                Instance = this;
                Instance.transform.parent = null;
                DontDestroyOnLoad(Instance);
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        public async Task LoadAllScenery()
        {
            Debug.Log(StartupConfig.GetSceneryDirectory());

            if (!Directory.Exists(StartupConfig.GetSceneryDirectory()))
            {
                Debug.LogError("Directory tidak ditemukan!");
                return;
            }

            string[] bundleFolder = Directory.GetDirectories(StartupConfig.GetSceneryDirectory());

            #region LOADING SETUP
            // Set Loading Message & Progress
            Metadata loadingMessage = (SceneController.activeLoading) ? SceneController.activeLoading.GetComponent<Metadata>() : null;
            Image loadingProgress = null;

            if (loadingMessage)
            {
                loadingMessage.FindParameter("message").GetComponent<TextMeshProUGUI>().text = "Loading Scenery...";

                loadingProgress = loadingMessage.FindParameter("progress-bar").GetComponent<Image>();
                loadingProgress.fillAmount = 0.25f;
            }
            #endregion

            if (bundleFolder.Length == 0)
            {
                Debug.Log("No Scenery Data Found");

                if (loadingProgress != null) loadingProgress.fillAmount += 0.75f;
                return;
            }

            // LOADING ALL SCENERY ASSETS
            for (int i = 0; i < bundleFolder.Length; i++)
            {
                string[] bundleFiles = Directory.GetFiles(Path.Combine(bundleFolder[i]), "*." + sceneryFormat);

                int bundleFinishedLoad = 0;
                if (bundleFiles.Length >= 0)
                {
                    // Load and instantiate seluruh pack asset ke "Don't Destroy" scene
                    for (int j = 0; j < bundleFiles.Length; j++)
                    {
                        if (loadingProgress != null) loadingProgress.fillAmount += ((0.5f / bundleFiles.Length) / bundleFolder.Length);

                        try
                        {
                            bundleFinishedLoad += await SyncLoad(bundleFiles[j], Path.Combine(bundleFolder[i]));
                        }
                        catch (Exception e)
                        {
                            Debug.LogError("Failed To Load Scenery " + bundleFiles[j]);
                            Debug.LogError(e.Message);
                        }
                    }
                }
                else
                {
                    if (loadingProgress != null) loadingProgress.fillAmount += (0.5f / bundleFolder.Length);
                }
            }
        }

        #region SYNC LOAD
        private async Task<int> SyncLoad(string file, string url)
        {
            var fileStream = new FileStream(Path.Combine(url, file), FileMode.Open, FileAccess.Read);
            var BundleLoaded = AssetBundle.LoadFromStreamAsync(fileStream);

            while (!BundleLoaded.isDone) await Task.Yield();

            if (BundleLoaded.assetBundle == null)
            {
                Debug.LogError("Failed to load Scenery!");
                return 0;
            }

            var AssetInQueue = BundleLoaded.assetBundle.LoadAllAssetsAsync<GameObject>();
            while (!AssetInQueue.isDone) await Task.Yield();

            for (int i = 0; i < AssetInQueue.allAssets.Length; i++)
            {
                var GO = AssetInQueue.allAssets[i] as GameObject;
            }
            
            BundleLoaded.assetBundle.Unload(false);
            return 1;
        }
        #endregion

        #region XML ATTRIBUTES
        [Serializable]
        public class SceneryTemplate
        {
            [XmlAttribute("scenery_name")]
            public string sceneryName { get; set; }
            [XmlAttribute("activated")]
            public bool activated { get; set; }
            [XmlElement("assets")]
            public List<PlacementObject> sceneryAssets { get; set; }
        }

        [System.Serializable]
        public class PlacementObjectExport
        {
            [XmlAttribute("objID")]
            public string objID { get; set; }

            [XmlAttribute("lng")]
            public double lng { get; set; }
            [XmlAttribute("lat")]
            public double lat { get; set; }
            [XmlAttribute("alt")]
            public double alt { get; set; }

            [XmlAttribute("heading")]
            public double heading { get; set; }
            [XmlAttribute("pitch")]
            public double pitch { get; set; }
            [XmlAttribute("yaw")]
            public double yaw { get; set; }
        }
        #endregion
    }

}