# Wargaming Sim (Maxxima)


## Introduction

Wargaming Simulator **MAIN SOURCE** for `SESKO TNI`, `SESKO AL`, `SESKO AD` and `SEKKAU`.


## First Setup

Before installation, make sure you hold your copy of the `Wargaming Simulator` source before proceeding next. **Contact** your supervisor for more details.

- Locate your `Assets/_Wargaming/Scripts` and then send new pull request.
- That's it.

## Push New Changes

Every Member of the team will had their own **Branch** which they can submit new feature and changes before merged into **Main Branch**. If you didn't have your branch yet, you can **Contact** your supervisor.

Only the code can be submitted and if you make any changes in your scene or asset component you still need to give the copy in `.package` manually to every member of the team.

## Merge to Main Branch

Merge request will be proceed once every month `depend on changes need it maybe faster or longer`. Your code will be analyzed first before merged into **Main Branch**.


## Copyrights

Copyright © 2023, Maxxima Innovative Engineering. All Rights Reserved.
