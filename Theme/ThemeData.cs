using System.Collections.Generic;

using UnityEngine;
using static ImageLoader;

public class ThemeData : MonoBehaviour
{
    public List<ImageParameter> images = new List<ImageParameter>();
}
