using UnityEngine;

using FIMSpace.GroundFitter;

using Wargaming.Core;

using WGSBundling.Core;
using Esri.HPFramework;
using Unity.Mathematics;
using UnityEngine.UIElements;
using FluffyUnderware.Curvy.Controllers;

namespace Wargaming.Components.Walker
{
    public class ParatroopWalker : RootWalkerBase
    {
        private WGSBundleCore bundleCore;
        private FGroundFitter groundFitter;

        public Transform startPoint;
        public Transform endPoint;

        public void Init()
        {
            InitBase();
                
            startPoint      = missions[0].transform.GetChild(0);
            endPoint        = missions[0].transform.GetChild(1);

            bundleCore      = entity.GetComponent<WGSBundleCore>();
            spline          = GetComponent<SplineController>();
            groundFitter    = endPoint.GetComponent<FGroundFitter>();

            bundleCore.parent.gameObject.SetActive(false);
        }

        private void Update()
        {
            if (spline == null || missions == null || missions.Count <= 0 || WargamingCam.instance.onRebase) return;
            if (!IsOnPlay()) return;

            GetTiming(null);
            GetActiveMission();

            GetActiveMissionNode();
            CheckSplineWithActiveMission(spline);

            UpdateMovement(bundleCore);

            if (moving)
            {
                startPoint.position = new Vector3(startPoint.position.x, missions[0].target.transform.position.y, startPoint.position.z);
                if (!groundFitter.enabled) { groundFitter.enabled = true; }
            }
            else
            {
                if (groundFitter.enabled) { groundFitter.enabled = false; }
            }
        }
    }
}