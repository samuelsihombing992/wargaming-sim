using Esri.ArcGISMapsSDK.Components;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wargaming.Components.Controller;
using Wargaming.Components.Playback;
using Wargaming.Core.GlobalParam.HelperColyseusTFG;
using Wargaming.Core.GlobalParam.HelperPlotting;
using WGSBundling.Core;

public class ParatroopsWalker : MonoBehaviour
{
    public EntityWalkerV2 parent;

    public SplineController splineController;
    public MissionWalker paratroopMission;
    public FollowTerrain terrainFollower;

    public int CURRENT_NODE = 0;
    public double progress = 0;

    private WGSBundleCore bundlingCore;
    private ObjekSatuan dataSatuan;
    private SendPenerjunan dataPenerjunan;

    private bool isMoving = false;
    private bool isOnHide = false;
    private bool isGenerated = false;
    private double oldGuidanceAltitude = 0;

    private float pathGenerateTime = 0f;
    private float pathGeneratePeriode = 0.0f;
    private bool isTimeToGenerate = false;

    private DateTime startTime;

    public void Init(EntityWalkerV2 _parent, DateTime _startTime)
    {
        parent = _parent;
        bundlingCore = GetComponent<WGSBundleCore>();
        dataSatuan = GetComponent<ObjekSatuan>();
        splineController = GetComponent<SplineController>();
        terrainFollower = EntityController.instance.CreateEntity(_parent.GetComponent<ArcGISLocationComponent>(), AssetPackageController.Instance.prefabGuidance.gameObject).AddComponent<FollowTerrain>();

        bundlingCore.parent.gameObject.SetActive(false);
        dataSatuan.hudElement.showIndicator = false;

        startTime = _startTime;
    }

    // Generate path penerjunan setelah turn update pertama
    // * Path tidak dapat di generate pada saat Init karena lokasi dari object ArcgisLocationComponent parentnya tidak akan langsung terupdate di frame yang sama namun di frame selanjutnya *
    public async void GeneratePath()
    {
        if (pathGeneratePeriode > pathGenerateTime) isTimeToGenerate = true;
        pathGeneratePeriode += Time.deltaTime;

        if (!isTimeToGenerate) return;
        isGenerated = true;

        var locationComponent = GetComponent<ArcGISLocationComponent>();

        var randomXRange = UnityEngine.Random.Range(-20, 20);
        var randomZRange = UnityEngine.Random.Range(-20, 20);

        List<Vector3> routePath = new List<Vector3>();
        routePath.Add(new Vector3((float)locationComponent.Position.X, (float)locationComponent.Position.Y, parent.altitudeMotion.Evaluate(splineController.AbsolutePosition)));
        routePath.Add(new Vector3((float)locationComponent.Position.X, (float)locationComponent.Position.Y, 0));

        MisiSatuanProperties dataMisiProperties = new MisiSatuanProperties
        {
            altitude = locationComponent.Position.Z,
            heading = locationComponent.Rotation.Heading,
            kecepatan = "60",
            type = "km"
        };

        MisiSatuan dataMisi = new MisiSatuan
        {
            id = name.ToString(),
            id_mission = "mission_" + name,
            jenis = "paratroopsTFG",
            startDate = startTime,
            data_properties = dataMisiProperties
        };

        var newRoute = await MissionController.instance.AddNewRoute(routePath, gameObject, dataMisi);
        if (newRoute == null)
        {
            Debug.LogError("Failed to create Paratroops : " + name);
            Destroy(gameObject);
            return;
        }

        splineController.AbsolutePosition = 0;
        splineController.MotionConstraints = MotionConstraints.FreezeRotationX | MotionConstraints.FreezeRotationZ | MotionConstraints.FreezePositionX | MotionConstraints.FreezePositionZ;
        splineController.Spline = newRoute.GetComponent<CurvySpline>();
        splineController.Spline.Interpolation = CurvyInterpolation.Linear;

        splineController.transform.position = new Vector3(splineController.transform.position.x + randomXRange, splineController.transform.position.y, splineController.transform.position.z + randomZRange);
        splineController.transform.rotation = Quaternion.Euler(splineController.transform.rotation.x, splineController.transform.rotation.y, 0);

        paratroopMission = newRoute;
    }

    private void Update()
    {
        if (splineController == null) return;

        if (!PlaybackController.IsPlaying()) return;

        if (!isGenerated) GeneratePath();
        if (paratroopMission == null) return;

        var PLAYBACK_TIME = PlaybackController.CURRENT_TIME;

        var missionNodes = paratroopMission.DATA;
        bool inTime = false;

        if (PLAYBACK_TIME < missionNodes[0].datetime)
        {
            CURRENT_NODE = 0;
            progress = 0;
        }
        else if (PLAYBACK_TIME > missionNodes[missionNodes.Count - 1].datetime)
        {
            CURRENT_NODE = missionNodes.Count - 1;
            progress = 1;
        }
        else
        {
            for (int i = 0; i < paratroopMission.DATA.Count - 2; i++)
            {
                var nodeTime = paratroopMission.DATA[i].datetime;
                var nextNodeTime = paratroopMission.DATA[i + 1].datetime;

                if (PLAYBACK_TIME >= nodeTime && PLAYBACK_TIME < nextNodeTime)
                {
                    // IF Waktu Playback sedang berada di antara 2 titik node
                    CURRENT_NODE = i;
                    i = paratroopMission.DATA.Count - 1;
                }
            }

            inTime = true;
        }

        if (!inTime && progress == 0)
        {
            isMoving = false;

            splineController.RelativePosition = 0;
            terrainFollower.enabled = false;
        }
        else if (!inTime && progress == 1)
        {
            isMoving = false;

            splineController.RelativePosition = 1;
            terrainFollower.enabled = false;
        }
        else
        {
            isMoving = true;

            var startDate = paratroopMission.DATA[0].datetime;
            var endDate = paratroopMission.DATA[paratroopMission.DATA.Count - 1].datetime;
            var totalTime = (endDate - startDate).TotalSeconds;

            progress = ((PLAYBACK_TIME.Subtract(startDate).TotalSeconds) / totalTime);
            splineController.AbsolutePosition = (float)(splineController.Spline.Length * (progress * 100) / 100);

            terrainFollower.enabled = true;
            if (terrainFollower.altitude != oldGuidanceAltitude)
            {
                var dropNodePosition = splineController.Spline.transform.GetChild(1);
                dropNodePosition.position = new Vector3(dropNodePosition.position.x, (float)terrainFollower.altitude, dropNodePosition.position.z);

                oldGuidanceAltitude = terrainFollower.altitude;
            }
        }

        SyncEntityToggleState();
    }

    private void SyncEntityToggleState()
    {
        bool state = false;
        if (isMoving && isOnHide)
        {
            isOnHide = false;
            state = true;
        }
        else if (!isMoving && !isOnHide)
        {
            isOnHide = true;
            state = false;
        }
        else
        {
            return;
        }

        bundlingCore.parent.gameObject.SetActive(state);
        dataSatuan.hudElement.showIndicator = state;
        //dataSatuan.hudElementTacmap.showIndicator = state;
    }
}