using Esri.ArcGISMapsSDK.Components;
using Esri.GameEngine.Geometry;
using UnityEngine;
using Wargaming.Components.Playback;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;
using WGSBundling.Bundles.CargoHold;
using WGSBundling.Core;

namespace Wargaming.Components.Walker
{
    public class SRootWalker : RootWalkerBase
    {
        private WGSBundleCore bundleCore;
        private ObjekSatuan objSatuan;

        public AnimationCurve soundCurve;

        public void Init()
        {
            InitBase();

            bundleCore = entity.GetComponent<WGSBundleCore>();
            objSatuan = entity.GetComponent<ObjekSatuan>();

            player = PlayerData.PLAYERS.Find(x => x.id == objSatuan.userID);
        }

        private void Update()
        {
            if (spline == null || missions == null || missions.Count <= 0 || WargamingCam.instance.onRebase) return;
            if (!IsOnPlay()) return;

            UpdateCurrentAltitude();

            GetTiming(objSatuan);
            GetActiveMission();

            GetActiveMissionNode();

            if (CheckSplineWithActiveMission(spline))
            {
                GenerateMotionProfile();
            }

            UpdateMovement(bundleCore);

            //var distToCam = Vector3.Distance(WargamingCam.instance.transform.position, entity.transform.position);
            //if (distToCam > 5000) return;

            UpdateSound();
            //UpdateRamp();
        }

        private void LateUpdate()
        {
            if (!moving && activeMission == null)
            {
                var entityLocation = entity.GetComponent<ArcGISLocationComponent>();

                entityLocation.Position = new ArcGISPoint(startPos.X, startPos.Y, (currentAltitude != -99) ? currentAltitude : startPos.Z, startPos.SpatialReference);
                entityLocation.Rotation = startRot;
            }
        }

        private void GenerateMotionProfile()
        {
            soundCurve = new AnimationCurve(
                    new Keyframe(0, 0),
                    new Keyframe(75, 1f),
                    new Keyframe(spline.Length - 75, 1),
                    new Keyframe(spline.Length, 0)
                );
        }

        private void UpdateSound()
        {
            var walkerSound = entity.GetComponent<WalkerSound>();
            if (walkerSound == null || soundCurve == null) return;

            walkerSound.pitch = soundCurve.Evaluate(spline.AbsolutePosition);
        }

        private void UpdateRamp()
        {
            var nodeRamp = entity.GetComponent<NodeCargoHold>();
            if (missions.Count <= 0 || nodeRamp == null || nodeRamp.doors == null || nodeRamp.doors.Count <= 0) return;

            var nodeCargoDoor = nodeRamp.doors[0];

            if(activeMission == null)
            {
                if (missions[0].DATA == null || missions[0].DATA.Count <= 0) return;
                if (missions[0].jenis == RootMissionWalker.RouteType.Pergerakan)
                {
                    if (nodeCargoDoor.state)
                    {
                        nodeCargoDoor.state = false;

                        var doorAniamtor = nodeCargoDoor.obj.GetComponent<Animator>();
                        if (doorAniamtor == null) return;

                        doorAniamtor.Play("Normal", 0, 0);
                        return;
                    }
                }

                if(CURRENT_TIME <= missions[0].DATA[0].datetime.AddSeconds(-10) && !nodeCargoDoor.state)
                {
                    nodeCargoDoor.state = true;

                    var doorAniamtor = nodeCargoDoor.obj.GetComponent<Animator>();
                    if (doorAniamtor == null) return;

                    doorAniamtor.Play("RampUp");
                    doorAniamtor.speed = Mathf.Clamp(PlaybackController.instance.speedFactor / 2, 1, 10);
                }
            }
            else
            {
                if (activeMission.DATA == null || activeMission.DATA.Count <= 0) return;
                if (missions[0].jenis == RootMissionWalker.RouteType.Pergerakan)
                {
                    if (nodeCargoDoor.state)
                    {
                        nodeCargoDoor.state = false;

                        var doorAniamtor = nodeCargoDoor.obj.GetComponent<Animator>();
                        if (doorAniamtor == null) return;

                        doorAniamtor.Play("Normal", 0, 0);
                        return;
                    }
                }

                if (CURRENT_TIME <= activeMission.DATA[0].datetime.AddSeconds(-10) && !nodeCargoDoor.state)
                {
                    var doorAniamtor = nodeCargoDoor.obj.GetComponent<Animator>();
                    if (doorAniamtor == null) return;

                    doorAniamtor.Play("RampDown");
                    doorAniamtor.speed = Mathf.Clamp(PlaybackController.instance.speedFactor / 2, 1, 10);

                    nodeCargoDoor.state = true;
                }
            }
        }
    }
}