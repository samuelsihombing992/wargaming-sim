using System.Linq;
using UnityEngine;

using Wargaming.Core;
using WGSBundling.Bundles.Sound;

public class WalkerSound : MonoBehaviour
{
    public enum EntityType { Vehicle, Ship, Submarine, Aircraft, Helicopter }
    public EntityType type;

    public RootWalkerBase walker;
    public NodeSound nodeSound;
    public Transform cam;

    public AnimationCurve movingCurve;

    private float frontVolume;
    private float backVolume;
    private float sideVolume;

    public float pitch = 1;

    private void Update()
    {
        if (nodeSound == null || nodeSound.entitySounds == null || nodeSound.type != NodeSound.SoundType.Entity || walker == null) return;

        if (WargamingCam.instance.camFollower != null)
        {
            cam = WargamingCam.instance.camFollower.GetChild(0);
        }
        else
        {
            cam = WargamingCam.instance.transform;
        }

        if(type == EntityType.Vehicle)
        {
            UpdateVehicleSounds();
        }
        else if(type == EntityType.Ship)
        {
            UpdateShipSounds();
        }else if(type == EntityType.Aircraft)
        {
            UpdateAircraftSounds();
        }
    }

    private void UpdateVehicleSounds()
    {
        if (Vector3.Distance(cam.position, transform.position) > 5000) return;
        if (movingCurve == null || movingCurve.keys.Length <= 3) return;

        if (walker.spline.AbsolutePosition >= movingCurve.keys[1].time && walker.spline.AbsolutePosition <= movingCurve.keys[2].time)
        {
            // On Moving
            for (int i = 0; i < nodeSound.entitySounds.move.Count; i++)
            {
                var sound = nodeSound.entitySounds.move[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (!sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(true);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.idle.Count; i++)
            {
                var sound = nodeSound.entitySounds.idle[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(false);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.start.Count; i++)
            {
                var sound = nodeSound.entitySounds.start[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(false);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.stop.Count; i++)
            {
                var sound = nodeSound.entitySounds.stop[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(false);
                }
            }
        }
        else if (walker.spline.AbsolutePosition < movingCurve.keys[1].time)
        {
            // On Start Moving
            for (int i = 0; i < nodeSound.entitySounds.move.Count; i++)
            {
                var sound = nodeSound.entitySounds.move[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                audioSource.volume = movingCurve.Evaluate(walker.spline.AbsolutePosition);

                if (!sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(true);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.start.Count; i++)
            {
                var sound = nodeSound.entitySounds.start[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                //audioSource.volume = 1 - movingCurve.Evaluate(walker.spline.AbsolutePosition);

                if (!sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(true);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.stop.Count; i++)
            {
                var sound = nodeSound.entitySounds.stop[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(false);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.idle.Count; i++)
            {
                var sound = nodeSound.entitySounds.idle[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(false);
                }
            }
        }
        else if(walker.spline.AbsolutePosition > movingCurve.keys[3].time)
        {
            // On End Moving
            for (int i = 0; i < nodeSound.entitySounds.move.Count; i++)
            {
                var sound = nodeSound.entitySounds.move[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                audioSource.volume = movingCurve.Evaluate(walker.spline.AbsolutePosition);

                if (!sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(true);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.stop.Count; i++)
            {
                var sound = nodeSound.entitySounds.stop[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                //audioSource.volume = 1 - movingCurve.Evaluate(walker.spline.AbsolutePosition);

                if (!sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(true);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.idle.Count; i++)
            {
                var sound = nodeSound.entitySounds.idle[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(false);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.start.Count; i++)
            {
                var sound = nodeSound.entitySounds.start[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(false);
                }
            }
        }
        else
        {
            // On Idle
            for (int i = 0; i < nodeSound.entitySounds.idle.Count; i++)
            {
                var sound = nodeSound.entitySounds.idle[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (!sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(true);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.start.Count; i++)
            {
                var sound = nodeSound.entitySounds.start[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(false);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.move.Count; i++)
            {
                var sound = nodeSound.entitySounds.move[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(false);
                }
            }

            for (int i = 0; i < nodeSound.entitySounds.stop.Count; i++)
            {
                var sound = nodeSound.entitySounds.stop[i];
                var audioSource = sound.node.GetComponent<AudioSource>();

                if (sound.node.gameObject.activeSelf)
                {
                    sound.node.gameObject.SetActive(false);
                }
            }
        }
    }

    private void UpdateShipSounds()
    {
        if (Vector3.Distance(cam.position, transform.position) > 5000) return;

        for (int i = 0; i < nodeSound.entitySounds.move.Count; i++)
        {
            var sound = nodeSound.entitySounds.move[i];
            var audioSource = sound.node.GetComponent<AudioSource>();

            if(i == 0)
            {
                audioSource.volume = pitch;
            }
            else
            {
                audioSource.pitch = pitch;
            }
        }
    }

    private void UpdateAircraftSounds()
    {
        if (Vector3.Distance(cam.position, transform.position) > 5000) return;

        Vector3 localPoint = transform.InverseTransformPoint(cam.transform.position);
        var rot = (Mathf.Atan2(localPoint.x, localPoint.z) * Mathf.Rad2Deg) + 180;

        if(rot >= 0 && rot < 180)
        {
            float fv = (rot / 90) * 100;
            float bv = ((rot - 90) / 90) * 100;

            frontVolume = Mathf.Clamp(bv / 100, 0, 1);
            backVolume  = Mathf.Clamp(1 - (fv / 100), 0, 1);
        }
        else
        {
            float fv = ((rot - 270) / 90) * 100;
            float bv = ((rot - 180) / 90) * 100;

            frontVolume = Mathf.Clamp(1 - (bv / 100), 0, 1);
            backVolume  = Mathf.Clamp((fv / 100), 0, 1);
            //frontSound.volume = frontVolume;
        }

        if (frontVolume > 0)
        {
            sideVolume = 1 - frontVolume;
        }
        else
        {
            sideVolume = 1 - backVolume;
        }

        if (nodeSound.entitySounds == null) return;

        for (int i = 0; i < nodeSound.entitySounds.move.Count; i++)
        {
            var sound = nodeSound.entitySounds.move[i];
           

            var audioSource = sound.node.GetComponent<AudioSource>();
            audioSource.pitch = pitch;

            if (sound.activeSide == NodeSoundData.SoundSide.Front)
            {
                audioSource.volume = frontVolume;
            }
            else if (sound.activeSide == NodeSoundData.SoundSide.Side)
            {
                audioSource.volume = sideVolume;
            }
            else if (sound.activeSide == NodeSoundData.SoundSide.Back)
            {
                audioSource.volume = backVolume;
            }
        }
    }
}