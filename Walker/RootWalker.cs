using System;
using System.Collections.Generic;

using UnityEngine;
using Unity.Mathematics;

using Wargaming.Components.Colyseus;
using Wargaming.Components.Playback;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;

using Esri.HPFramework;

using FIMSpace.GroundFitter;

using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;

using WGSBundling.Bundles.Aircraft;
using WGSBundling.Core;

using static Wargaming.Core.GlobalParam.GlobalHelper;

public class RootWalker : MonoBehaviour
{
    private SplineController spline;

    [Header("Entity")]
    public GameObject entity;

    public bool gearDown = true;

    [Header("Data Misi")]
    public List<RootMissionWalker> missions = new List<RootMissionWalker>();
    public RootMissionWalker activeMission;

    [Header("Movement Profiler")]
    public AnimationCurve altitudeProfile;
    public AnimationCurve takeoffPitch;
    public AnimationCurve landingPitch;
    public AnimationCurve guidanceDist;
    public AnimationCurve landCurve;
    public AnimationCurve soundCurve;
    private float distanceToTakeoffLand;
    private float takeoffLandingLength;
    public float distToCam;

    private WGSBundleCore bundleCore;
    public ObjekSatuan objSatuan;

    private HPTransform hpTransform;

    [NonSerialized] public SplineController guidance;
    [NonSerialized] public FGroundFitter groundAdjuster;
    [NonSerialized] public float guidanceDistance;
    [NonSerialized] public float sensitivity;

    private DateTime CURRENT_TIME;
    private double progress = 0;
    [SerializeField] private bool moving = false;

    //public float timeOffset = 0;

    private float rollChange;

    public void Init()
    {
        if(spline == null) { spline = GetComponent<SplineController>(); }

        bundleCore      = entity.GetComponent<WGSBundleCore>();
        objSatuan       = entity.GetComponent<ObjekSatuan>();
        hpTransform     = GetComponent<HPTransform>();

        
    }

    private void Update()
    {
        if (spline == null) return;
        if (missions == null || missions.Count <= 0) return;
        if (WargamingCam.instance.onRebase) return;

        if (!PlaybackController.IsPlaying()) return;

        CURRENT_TIME = SyncMultiplayerTiming();

        activeMission   = GetActiveMission();
        var activeNode  = GetActiveMissionNode();

        if(spline.Spline != activeMission.GetComponent<CurvySpline>())
        {
            spline.Spline   = activeMission.GetComponent<CurvySpline>();
            if(guidance != null)
            {
                guidance.Spline = activeMission.GetComponent<CurvySpline>();
            }

            GenerateAircraftProfile();
            GenerateHelicopterProfile();
        }

        if (!moving && progress == 0)
        {
            spline.RelativePosition = 0;
            if(guidance != null)
            {
                if (objSatuan.kategori == EntityHelper.KategoriSatuan.HELICOPTER && activeMission.takeoff)
                {
                    guidance.AbsolutePosition = Mathf.Clamp(spline.AbsolutePosition, 250, spline.Length);
                }
                else
                {
                    guidance.AbsolutePosition = Mathf.Clamp(spline.AbsolutePosition, 0, spline.Length);
                }
            }
        }
        else if(!moving && progress == 1)
        {
            spline.RelativePosition = 1;
            if(guidance != null)
            {
                guidance.AbsolutePosition = spline.AbsolutePosition;
            }
        }
        else
        {
            if (!spline.enabled)
            {
                spline.enabled = true;
            }

            var startDate   = activeMission.DATA[0].datetime;
            var endDate     = activeMission.DATA[activeMission.DATA.Count - 1].datetime;
            var totalTime = (endDate - startDate).TotalSeconds;

            progress = ((CURRENT_TIME.Subtract(startDate).TotalSeconds) / totalTime);
            spline.AbsolutePosition = (float) (spline.Spline.Length * (progress * 100) / 100);

            if (guidance != null)
            {
                if (objSatuan.kategori == EntityHelper.KategoriSatuan.HELICOPTER && activeMission.takeoff)
                {
                    guidance.AbsolutePosition = Mathf.Clamp(spline.AbsolutePosition + guidanceDistance, 200, spline.Length);
                }
                else
                {
                    guidance.AbsolutePosition = Mathf.Clamp(spline.AbsolutePosition + guidanceDistance, 0, spline.Length);
                }

                guidanceDistance    = (activeMission.speed < 100) ? 100 : (float)activeMission.speed;
                sensitivity         = int.Parse(guidanceDistance.ToString().Substring(0, 1));
            }
        }

        if(spline.Spline != null && altitudeProfile != null)
        {
            transform.position = new Vector3(
                transform.position.x,
                altitudeProfile.Evaluate(spline.AbsolutePosition),
                transform.position.z
            );
        }

        if(groundAdjuster != null)
        {
            var groundAdjusterHPTransform = groundAdjuster.GetComponent<HPTransform>();

            groundAdjusterHPTransform.UniversePosition = new double3(
                hpTransform.UniversePosition.x,
                groundAdjusterHPTransform.UniversePosition.y,
                hpTransform.UniversePosition.z
            );
            //groundAdjusterHPTransform.UniverseRotation = hpTransform.UniverseRotation;
        }

        if(guidance != null && guidance.Spline != null)
        {
            float pitchval = 0;
            if(spline.AbsolutePosition < altitudeProfile.keys[2].time && takeoffPitch != null)
            {
                pitchval = (objSatuan.kategori == EntityHelper.KategoriSatuan.HELICOPTER) ? takeoffPitch.Evaluate(guidance.AbsolutePosition) : -takeoffPitch.Evaluate(guidance.AbsolutePosition);
            }else if(spline.AbsolutePosition < altitudeProfile.keys[altitudeProfile.keys.Length - 1].time && landingPitch != null)
            {
                pitchval = -landingPitch.Evaluate(guidance.AbsolutePosition);
            }

            guidance.transform.position = new Vector3(
                guidance.transform.position.x,
                altitudeProfile.Evaluate(guidance.AbsolutePosition) - pitchval,
                guidance.transform.position.z
            );
        }

        UpdateAltitudeProfile();
        UpdateEntityPosition();

        distToCam = Vector3.Distance(WargamingCam.instance.transform.position, entity.transform.position);
        if (distToCam > 5000) return;

        UpdateRollAndPitch();
        UpdateGear();
        UpdateSound();

        //SyncEntityPosition();
        //SyncAltitudeGuidance();
        //SyncRoll();
    }

    private void UpdateAltitudeProfile()
    {
        if (altitudeProfile == null) return;

        var altSegmentOne = altitudeProfile.keys[1];
        var altSegmentLastTwo = altitudeProfile.keys[altitudeProfile.keys.Length - 3];

        if (spline.AbsolutePosition < altSegmentOne.time || spline.AbsolutePosition > altSegmentLastTwo.time)
        {
            //var terrainFollowerHPTransform = groundAdjuster.GetComponent<HPTransform>();
            var alt = (float) EnvironmentsController.GetMapComponent().GetComponent<HPRoot>().RootUniversePosition.y + groundAdjuster.transform.position.y;

            if(objSatuan.kategori == EntityHelper.KategoriSatuan.AIRCRAFT)
            {
                var takeoffOne = altitudeProfile.keys[0];
                takeoffOne.value = alt;
                altitudeProfile.MoveKey(0, takeoffOne);

                var takeoffTwo = altSegmentOne;
                takeoffTwo.value = alt;
                altitudeProfile.MoveKey(1, takeoffTwo);

                var landingTwo = altitudeProfile.keys[altitudeProfile.keys.Length - 2];
                landingTwo.value = alt;
                altitudeProfile.MoveKey(altitudeProfile.keys.Length - 2, landingTwo);

                //var landingThree = altitudeProfile.keys[altitudeProfile.keys.Length - 3];
                //landingThree.value = alt + 10;
                //altitudeProfile.MoveKey(altitudeProfile.keys.Length - 3, landingThree);

                var landingOne = altitudeProfile.keys[altitudeProfile.keys.Length - 1];
                landingOne.value = alt;
                altitudeProfile.MoveKey(altitudeProfile.keys.Length - 1, landingOne);
            }
            else if(objSatuan.kategori == EntityHelper.KategoriSatuan.HELICOPTER)
            {
                double diffInSeconds = CURRENT_TIME.Subtract(activeMission.DATA[activeMission.DATA.Count - 1].datetime).TotalSeconds;

                var ntest = alt + landCurve.Evaluate((float)diffInSeconds);

                var takeoffOne = altitudeProfile.keys[0];
                takeoffOne.value = alt;
                altitudeProfile.MoveKey(0, takeoffOne);

                var landingOne = altitudeProfile.keys[altitudeProfile.keys.Length - 3];
                landingOne.value = ntest;
                altitudeProfile.MoveKey(altitudeProfile.keys.Length - 1, landingOne);
            }
        }
    }

    private void UpdateRollAndPitch()
    {
        if (guidance == null) return;

        var currentRot  = transform.localRotation.eulerAngles.y;
        var guidanceRot = guidance.transform.localRotation.eulerAngles.y;

        rollChange = ((currentRot - guidanceRot) * -1) * sensitivity;

        var newHeading = Quaternion.Euler(bundleCore.origin.x, bundleCore.origin.y, rollChange);

        bundleCore.parent.localRotation = Quaternion.Lerp(bundleCore.parent.localRotation, newHeading, Time.deltaTime * PlaybackController.instance.speedFactor);
    }

    private void UpdateEntityPosition()
    {
        var entityHPTransform = entity.GetComponent<HPTransform>();

        if(altitudeProfile != null)
        {
            entityHPTransform.UniversePosition = new double3(
                hpTransform.UniversePosition.x,
                altitudeProfile.Evaluate(spline.AbsolutePosition),
                hpTransform.UniversePosition.z
            );
        }
        else
        {
            entityHPTransform.UniversePosition = hpTransform.UniversePosition;
        }

        if(guidance != null) { transform.LookAt(guidance.transform); }
        entity.GetComponent<HPTransform>().UniverseRotation = hpTransform.UniverseRotation;
    }

    private void UpdateGear()
    {
        var nodeAircraft = entity.GetComponent<NodeAircraft>();
        if (nodeAircraft == null) return;

        var splinePos       = spline.AbsolutePosition;

        var takeoffStart    = altitudeProfile.keys[1].time + 500;
        var takeoffEnd      = altitudeProfile.keys[2].time;

        var landingStart = altitudeProfile.keys[altitudeProfile.keys.Length - 3].time;
        var landingEnd = altitudeProfile.keys[altitudeProfile.keys.Length - 3].time + 500;

        if (splinePos > takeoffStart && splinePos < takeoffEnd)
        {
            // Enter takeoff state
            var nodeGear = entity.GetComponent<NodeGearTest>();

            if (gearDown)
            {
                nodeGear.frontGear.Play("GearClose");
                nodeGear.leftGear.Play("LeftGearClose");
                nodeGear.rightGear.Play("RightGearClose");

                gearDown = false;
            }
        }
        else if (splinePos > landingEnd && splinePos < spline.Length)
        {
            // Enter landing state
            var nodeGear = entity.GetComponent<NodeGearTest>();

            if (!gearDown)
            {
                nodeGear.frontGear.Play("GearOpen");
                nodeGear.leftGear.Play("LeftGearOpen");
                nodeGear.rightGear.Play("RightGearOpen");

                gearDown = true;
            }
        }
        else
        {
            if (splinePos > takeoffStart && splinePos < landingStart && gearDown)
            {
                var nodeGear = entity.GetComponent<NodeGearTest>();

                nodeGear.frontGear.PlayInFixedTime("Normal", 0, 0);
                nodeGear.leftGear.PlayInFixedTime("Normal", 0, 0);
                nodeGear.rightGear.PlayInFixedTime("Normal", 0, 0);

                gearDown = false;
            }
            else if (splinePos < takeoffStart && !gearDown)
            {
                var nodeGear = entity.GetComponent<NodeGearTest>();

                nodeGear.frontGear.PlayInFixedTime("Normal", 0, 0);
                nodeGear.leftGear.PlayInFixedTime("Normal", 0, 0);
                nodeGear.rightGear.PlayInFixedTime("Normal", 0, 0);

                gearDown = true;
            }else if(splinePos > landingEnd && !gearDown)
            {
                var nodeGear = entity.GetComponent<NodeGearTest>();

                nodeGear.frontGear.PlayInFixedTime("Normal", 0, 0);
                nodeGear.leftGear.PlayInFixedTime("Normal", 0, 0);
                nodeGear.rightGear.PlayInFixedTime("Normal", 0, 0);

                gearDown = true;
            }
            else if(splinePos > landingStart && gearDown)
            {
                var nodeGear = entity.GetComponent<NodeGearTest>();

                nodeGear.frontGear.PlayInFixedTime("Normal", 0, 0);
                nodeGear.leftGear.PlayInFixedTime("Normal", 0, 0);
                nodeGear.rightGear.PlayInFixedTime("Normal", 0, 0);

                gearDown = false;
            }
        }
    }

    private void UpdateSound()
    {
        var soundModule = entity.GetComponent<DebugAircraftSound>();
        if (soundModule == null) return;

        var pitch = soundCurve.Evaluate(spline.AbsolutePosition);

        soundModule.frontSound.pitch = pitch;
        soundModule.sideSound.pitch = pitch;
        soundModule.backSound.pitch = pitch;
    }

    private void SyncEntityPosition()
    {
        var hpTransform = GetComponent<HPTransform>();

        entity.GetComponent<HPTransform>().UniversePosition = new double3(hpTransform.UniversePosition.x, transform.position.y, hpTransform.UniversePosition.z);
        entity.GetComponent<HPTransform>().UniverseRotation = hpTransform.UniverseRotation;
    }

    //private void LateUpdate()
    //{
    //    if (spline == null) return;
    //    if (missions == null || missions.Count <= 0) return;
    //    if (WargamingCam.instance.onRebase) return;

    //    if (!PlaybackController.IsPlaying()) return;

    //    //var terrainFollowerHPTransform = groundAdjuster.GetComponent<HPTransform>();
    //    //alt = (float) terrainFollowerHPTransform.UniversePosition.y;

    //    //SyncRoll();
    //    //SyncTerrainFollower();

    //    return;
    //    SyncAircraftTerrainFollower();
    //    SyncHeliTerrainFollower();
    //}

    private DateTime SyncMultiplayerTiming()
    {
        if(ColyseusTFGSingleController.instance != null)
        {
            return objSatuan.player.timeMovement.GetValueOrDefault();
        }
        else
        {
            return PlaybackController.CURRENT_TIME;
        }
    }

    private RootMissionWalker GetActiveMission()
    {
        for(int i = (missions.Count - 1); i >= 0; i--)
        {
            if(CURRENT_TIME >= missions[i].DATA[0].datetime)
            {
                return missions[i];
            }
        }

        return missions[0];
    }

    private MissionWalkerParam GetActiveMissionNode()
    {
        if(CURRENT_TIME < activeMission.DATA[0].datetime)
        {
            progress = 0;
            moving = false;

            return activeMission.DATA[0];
        }else if(CURRENT_TIME > activeMission.DATA[activeMission.DATA.Count - 1].datetime)
        {
            progress = 1;
            moving = false;

            return activeMission.DATA[activeMission.DATA.Count - 1];
        }
        else
        {
            for(int i=0; i < activeMission.DATA.Count - 2; i++)
            {
                var nodeTime        = activeMission.DATA[i].datetime;
                var nextNodeTime    = activeMission.DATA[i + 1].datetime;

                if(CURRENT_TIME >= nodeTime && CURRENT_TIME < nextNodeTime)
                {
                    // IF Waktu Playback sedang berada di antara 2 titik node
                    moving = true;
                    return activeMission.DATA[i];
                }
            }

            return activeMission.DATA[activeMission.DATA.Count - 2];
        }
    }

    #region MOTION PROFILER
    private void GenerateAircraftProfile()
    {
        if (objSatuan.kategori != EntityHelper.KategoriSatuan.AIRCRAFT) return;

        // Motion takeoff dan landing akan mengambil 20% dari panjang rute aktif (40% jalur takeoff dan landing, 60% jalur flight)
        // Maksimal Takeoff / Landing Length = 40000
        // takeoffLandingLength = (length * 20) / 100;
        distanceToTakeoffLand = 1000;

        takeoffLandingLength = Mathf.Clamp(((spline.Length * 20) / 100), 0, 10000);
        var landingLength = Mathf.Clamp((spline.Length * 30) / 100, 0, 10000);

        var altitude = (0.2f * spline.Length);

        var maxAltitude = StartupConfig.settings.MAX_AIRCRAFT_ALTITUDE.GetValueOrDefault(10000);
        altitude = Mathf.Clamp(altitude, 0, maxAltitude);

        var startAltitude = (activeMission.takeoff) ? 0 : altitude;
        var finalAltitude = (activeMission.landing) ? 0 : altitude;

        altitudeProfile = new AnimationCurve(
            new Keyframe(0, startAltitude),
            new Keyframe(distanceToTakeoffLand, startAltitude),
            new Keyframe(takeoffLandingLength, altitude),
            new Keyframe(spline.Length - landingLength, altitude),
            //new Keyframe(spline.Length - (distanceToTakeoffLand + 500), (activeMission.landing) ? 15 : altitude),
            new Keyframe(spline.Length - distanceToTakeoffLand, finalAltitude),
            new Keyframe(spline.Length, finalAltitude)
        );

        soundCurve = new AnimationCurve(
            new Keyframe(0, 1),
            new Keyframe(distanceToTakeoffLand / 4, 1.5f),
            new Keyframe(takeoffLandingLength, 1.5f),
            new Keyframe(takeoffLandingLength + (distanceToTakeoffLand / 4), 1.25f),
            new Keyframe(spline.Length - distanceToTakeoffLand - (distanceToTakeoffLand / 4), 1.25f),
            new Keyframe(spline.Length - distanceToTakeoffLand, 1),
            new Keyframe(spline.Length, 1)
        );

        guidanceDist = new AnimationCurve(
            new Keyframe(0, 0),
            new Keyframe(takeoffLandingLength, 100),
            new Keyframe(spline.Length - takeoffLandingLength, 100),
            new Keyframe(spline.Length, 0)
        );

        // Motion Pitch ketika Takeoff
        if (activeMission.takeoff)
        {
            takeoffPitch = new AnimationCurve(
                new Keyframe(0, 0),
                new Keyframe(distanceToTakeoffLand, 0),
                new Keyframe(distanceToTakeoffLand + (distanceToTakeoffLand / 2), 50),
                new Keyframe(distanceToTakeoffLand * 2, 0)
            );
        }

        // Motion Pitch ketika Landing
        if (activeMission.landing)
        {
            landingPitch = new AnimationCurve(
                new Keyframe(spline.Length - landingLength + distanceToTakeoffLand, 0),
                new Keyframe(spline.Length - landingLength + (distanceToTakeoffLand * 2), 70),
                new Keyframe(spline.Length - distanceToTakeoffLand - landingLength, 0),
                new Keyframe(spline.Length - distanceToTakeoffLand, 60),
                new Keyframe(spline.Length - distanceToTakeoffLand + (distanceToTakeoffLand / 1.5f), 0)
            );
        }
    }

    private void GenerateHelicopterProfile()
    {
        if (objSatuan.kategori != EntityHelper.KategoriSatuan.HELICOPTER) return;

        // Motion takeoff dan landing akan mengambil 20% dari panjang rute aktif (40% jalur takeoff dan landing, 60% jalur flight)
        // Maksimal Takeoff / Landing Length = 40000
        distanceToTakeoffLand = 4000;

        takeoffLandingLength = Mathf.Clamp(((spline.Length * 30) / 100), 0, 4000);

        var altitude = (0.5f * spline.Length);

        var maxAltitude = 5048;
        altitude = Mathf.Clamp(altitude, 0, maxAltitude);

        var startAltitude = (activeMission.takeoff) ? 0 : altitude;
        var finalAltitude = (activeMission.landing) ? 0 : altitude;

        altitudeProfile = new AnimationCurve(
            new Keyframe(0, startAltitude),
            new Keyframe(takeoffLandingLength, altitude),
            new Keyframe(spline.Length - takeoffLandingLength, altitude),
            new Keyframe(spline.Length, finalAltitude + 50)
        );

        guidanceDist = new AnimationCurve(
            new Keyframe(0, 0),
            new Keyframe(takeoffLandingLength, 100),
            new Keyframe(spline.Length - takeoffLandingLength, 100),
            new Keyframe(spline.Length, 0)
        );

        // Motion Pitch ketika Takeoff
        if (activeMission.takeoff)
        {
            takeoffPitch = new AnimationCurve(  
                new Keyframe(0, 0),
                new Keyframe((12.5f * takeoffLandingLength) / 100, (2.4f * altitude) / 100),
                new Keyframe((25f * takeoffLandingLength) / 100, (10f * altitude) / 100),
                new Keyframe((70f * takeoffLandingLength) / 100, (12f * altitude) / 100),
                new Keyframe(takeoffLandingLength, 50)
            );
        }

        // Motion Pitch ketika Landing
        if (activeMission.landing)
        {
            landingPitch = new AnimationCurve(
                new Keyframe(spline.Length - takeoffLandingLength, 50),
                new Keyframe(spline.Length - (70f * takeoffLandingLength) / 100, (12f * altitude) / 100),
                new Keyframe(spline.Length - (25f * takeoffLandingLength) / 100, (10f * altitude) / 100),
                new Keyframe(spline.Length - (12.5f * takeoffLandingLength) / 100, (2.4f * altitude) / 100),
                new Keyframe(spline.Length, 0)
            );

            landCurve = new AnimationCurve(new Keyframe(2, 100), new Keyframe(12, 0));
        }
        else
        {
            landCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(12, 0));
        }
    }
    #endregion
}
