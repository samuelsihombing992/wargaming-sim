using Esri.ArcGISMapsSDK.Components;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;
using Esri.HPFramework;
using FIMSpace.GroundFitter;
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.UIElements;
using Wargaming.Components.Colyseus;
using Wargaming.Components.Playback;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;
using WGSBundling.Bundles.Aircraft;
using WGSBundling.Bundles.Sound;
using WGSBundling.Bundles.VFX;
using WGSBundling.Core;

namespace Wargaming.Components.Walker
{
    public class ARootWalker : RootWalkerBase
    {
        public SplineController guidance;

        private WGSBundleCore bundleCore;
        private NodeAircraft bundleAircraft;

        private ObjekSatuan objSatuan;

        public FGroundFitter groundFitter;
        public FGroundFitter rootGroundFitter;

        public AnimationCurve altitudeProfile;
        public AnimationCurve guidanceAltitudeProfile;

        public AnimationCurve guidanceCurve;
        public AnimationCurve takeoffPitch;
        public AnimationCurve landingPitch;
        public AnimationCurve landingCurve;
        public AnimationCurve soundCurve;

        private float takeoffLandingLength;
        public float guidanceDistance;
        public float sensitivity;
        public float pitchval;

        public float pitchLimit     = 20;
        public float altitudeLimit  = 5000;

        public void Init()
        {
            InitBase();

            bundleCore      = entity.GetComponent<WGSBundleCore>();
            bundleAircraft  = entity.GetComponent<NodeAircraft>();

            objSatuan       = entity.GetComponent<ObjekSatuan>();
            altitudeLimit   = StartupConfig.settings.MAX_AIRCRAFT_ALTITUDE.GetValueOrDefault(10000);

            player          = PlayerData.PLAYERS.Find(x => x.id == objSatuan.userID);
        }

        private void Update()
        {
            if (spline == null || missions == null || missions.Count <= 0 || WargamingCam.instance.onRebase)
            {
                if (!WargamingCam.instance.onRebase && rootGroundFitter != null && !rootGroundFitter.enabled)
                {
                    rootGroundFitter.enabled = true;
                }

                return;
            }

            if (!IsOnPlay())
            {
                if(rootGroundFitter != null && rootGroundFitter != null && !rootGroundFitter.enabled && !moving)
                {
                    rootGroundFitter.enabled = true;
                }

                return;
            }

            GetTiming(objSatuan);
            GetActiveMission();

            GetActiveMissionNode();

            if (CheckSplineWithActiveMission(spline))
            {
                //if (guidance != null) {
                //    guidance.Spline     = spline.Spline;
                //    guidanceDistance    = (activeMission.speed < 100) ? 100 : (float)activeMission.speed / 2;
                //}
            }

            if (moving)
            {
                groundFitter.UpAxisRotation = transform.rotation.eulerAngles.y;
            }

            if (moving && rootGroundFitter != null && rootGroundFitter.enabled)
            {
                rootGroundFitter.enabled = false;
            }

            if (guidance != null && activeMission != null)
            {
                guidanceDistance = (activeMission.speed < 100) ? 100 : (float)activeMission.speed / 2;
                if (CheckSplineWithActiveMission(guidance))
                {
                    GenerateMotionProfile();
                }
            }

            UpdateMovement(bundleCore);

            if (groundFitter != null && moving)
            {
                UpdateGroundFitter(groundFitter);
            }

            if(rootGroundFitter != null && !moving)
            {
                rootGroundFitter.enabled = true;
            }

            if (UpdateGuidanceMovement(guidance, guidanceDistance, objSatuan))
            {
                if(objSatuan.kategori == EntityHelper.KategoriSatuan.HELICOPTER)
                {
                    guidanceDistance = (activeMission.speed < 100) ? 100 : (float)activeMission.speed / 2;
                    sensitivity = int.Parse(guidanceDistance.ToString().Substring(0, 1));
                }
                else
                {
                    guidanceDistance = (activeMission.speed < 100) ? 100 : (float)activeMission.speed * 2;
                    sensitivity = int.Parse(guidanceDistance.ToString().Substring(0, 1)) * 4;
                }
            }

            if(spline != null && spline.Spline != null && altitudeProfile != null)
            {
                UpdateAltitude(altitudeProfile.Evaluate(spline.AbsolutePosition));

                if (guidance != null && guidance.Spline != null && altitudeProfile.keys.Length >= 3)
                {
                    pitchval = 0;
                    if (spline.AbsolutePosition < altitudeProfile.keys[2].time && takeoffPitch != null)
                    {
                        pitchval = (objSatuan.kategori == EntityHelper.KategoriSatuan.HELICOPTER) ? takeoffPitch.Evaluate(guidance.AbsolutePosition) : -takeoffPitch.Evaluate(guidance.AbsolutePosition);
                    }
                    else if (spline.AbsolutePosition < altitudeProfile.keys[altitudeProfile.keys.Length - 1].time && landingPitch != null)
                    {
                        pitchval = -landingPitch.Evaluate(guidance.AbsolutePosition);
                    }

                    if (guidanceAltitudeProfile != null)
                    {
                        if ((spline.RelativePosition <= 0 || spline.RelativePosition >= 1) && !moving)
                        {
                            UpdateGuidancePosition(guidance.transform, guidanceAltitudeProfile.Evaluate(guidance.AbsolutePosition), entity);
                        }
                        else
                        {
                            UpdateGuidancePosition(guidance.transform, guidanceAltitudeProfile.Evaluate(guidance.AbsolutePosition));
                        }
                    }
                    else
                    {
                        if ((spline.RelativePosition <= 0 || spline.RelativePosition >= 1) && !moving)
                        {
                            UpdateGuidancePosition(guidance.transform, altitudeProfile.Evaluate(guidance.AbsolutePosition) - pitchval, entity);
                        }
                        else
                        {
                            UpdateGuidancePosition(guidance.transform, altitudeProfile.Evaluate(guidance.AbsolutePosition) - pitchval);
                        }
                    }
                }
            }

            //float? pitchLimit = null;
            //if (objSatuan.kategori == EntityHelper.KategoriSatuan.HELICOPTER)
            //{
            //    pitchLimit = 20;
            //}

            UpdateAltitudeProfile();

            if(guidance != null && moving)
            {
                transform.LookAt(guidance.transform);
            }

            //UpdateEntityPosition(entity, altitudeProfile.Evaluate(spline.AbsolutePosition), guidance.transform, pitchLimit);

            //var distToCam = Vector3.Distance(WargamingCam.instance.transform.position, entity.transform.position);
            //if (distToCam > 5000) return;

            UpdateRollAndPitch();
            UpdateGear();
            UpdateSound();
            UpdateVFX();
        }

        //private void LateUpdate()
        //{
        //    if (spline == null || missions == null || missions.Count <= 0 || WargamingCam.instance.onRebase) return;
        //    if (!IsOnPlay()) return;

        //    UpdateCurrentAltitude();

        //    //if (groundFitter != null && WargamingCam.instance.onRebase)
        //    //{
        //    //    UpdateGroundFitter(groundFitter);
        //    //}
        //}

        private void LateUpdate()
        {
            //UpdateCurrentAltitude();

            if (!moving && activeMission == null)
            {
                UpdateCurrentAltitude();

                if (rootGroundFitter != null)
                {
                    rootGroundFitter.UpAxisRotation = (float)startRot.Heading;
                }

                if(groundFitter != null)
                {
                    groundFitter.UpAxisRotation = (float)startRot.Heading;

                    guidance.GetComponent<HPTransform>().UniverseRotation = Quaternion.Euler(0, 90, 0);
                }

                var entityLocation = entity.GetComponent<ArcGISLocationComponent>();

                entityLocation.Position = new ArcGISPoint(startPos.X, startPos.Y, (currentAltitude != -99) ? currentAltitude : startPos.Z, startPos.SpatialReference);
                //entityLocation.Rotation = new ArcGISRotation(startRot.Heading, 90, 0);
            }
        }

        public void GenerateMotionProfile()
        {
            if(objSatuan.kategori == EntityHelper.KategoriSatuan.AIRCRAFT)
            {
                // Generate Motion Profile Untuk Objek Pesawat

                // Motion takeoff dan landing akan mengambil 20% dari panjang rute aktif (40% jalur takeoff dan landing, 60% jalur flight)
                // Maximal Takeoff / Landing Length = 40000
                var takeoffLength = 1000;
                //var landingLength = Mathf.Clamp((spline.Length * 30) / 100, 0, 40000);
                var landingLength = Mathf.Clamp((spline.Length * 25) / 100, 0, 40000);

                takeoffLandingLength = (spline.Length * 20) / 100;

                // Maximum Ketinggian Pesawat (10000m)
                var altitude = (0.2f * takeoffLandingLength);

                takeoffLandingLength = Mathf.Clamp(takeoffLandingLength, 0, 40000);

                altitude = Mathf.Clamp(altitude, 0, altitudeLimit);

                var startAltitude = (activeMission.takeoff) ? 0 : altitude;
                var finalAltitude = (activeMission.landing) ? 0 : altitude;

                altitudeProfile = new AnimationCurve(
                    new Keyframe(0, startAltitude),
                    new Keyframe(takeoffLength, startAltitude),
                    new Keyframe(takeoffLandingLength, altitude),
                    new Keyframe(spline.Length - landingLength, altitude),
                    new Keyframe(spline.Length - takeoffLength, finalAltitude),
                    new Keyframe(spline.Length, finalAltitude)
                );

                soundCurve = new AnimationCurve(
                    new Keyframe(0, 1),
                    new Keyframe(200, 1.5f),
                    new Keyframe(takeoffLandingLength, 1.5f),
                    new Keyframe(takeoffLandingLength + (takeoffLandingLength / 4), 1.25f),
                    new Keyframe(spline.Length - takeoffLandingLength - (takeoffLandingLength / 4), 1.25f),
                    new Keyframe(spline.Length - takeoffLength, 1),
                    new Keyframe(spline.Length, 1)
                );

                // Motion Pitch ketika Takeoff
                if (activeMission.takeoff)
                {
                    takeoffPitch = new AnimationCurve(
                        new Keyframe(0, 0),
                        new Keyframe(takeoffLength, 0),
                        new Keyframe(takeoffLength + (takeoffLength / 2), 50),
                        new Keyframe(takeoffLength * 2, 0)
                    );
                }

                // Motion Pitch ketika Landing
                if (activeMission.landing)
                {
                    landingPitch = new AnimationCurve(
                        new Keyframe(spline.Length - landingLength + takeoffLength, 0),
                        new Keyframe(spline.Length - landingLength + (takeoffLength * 2), 70),
                        new Keyframe(spline.Length - takeoffLength - landingLength, 0),
                        new Keyframe(spline.Length - takeoffLength, 60),
                        new Keyframe(spline.Length - takeoffLength + (takeoffLength / 1.5f), 0)
                    );
                }
            }
            else if(objSatuan.kategori == Core.GlobalParam.EntityHelper.KategoriSatuan.HELICOPTER)
            {
                // Generate Motion Profile Untuk Objek Helicopter
                var takeoffLength = 4000;
                var landingLength = Mathf.Clamp((spline.Length * 20) / 100, 0, 4000);

                takeoffLandingLength = Mathf.Clamp((spline.Length * 30) / 100, 0, 4000);

                var altitude = (0.5f * takeoffLandingLength);

                altitude = Mathf.Clamp(altitude, 0, 5048);

                var startAltitude = (activeMission.takeoff) ? 0 : altitude;
                var finalAltitude = (activeMission.landing) ? 0 : altitude;

                altitudeProfile = new AnimationCurve(
                    new Keyframe(0, startAltitude),
                    new Keyframe(takeoffLandingLength, altitude),
                    new Keyframe(spline.Length - takeoffLandingLength, altitude),
                    new Keyframe(spline.Length, finalAltitude + 50)
                );

                soundCurve = new AnimationCurve(
                    new Keyframe(0, 0.9f),
                    new Keyframe(100, 1.1f),
                    new Keyframe(takeoffLandingLength, 1f),
                    //new Keyframe(takeoffLandingLength + (takeoffLandingLength / 4), 1.1f),
                    new Keyframe(spline.Length - takeoffLandingLength, 1f),
                    //new Keyframe(spline.Length - takeoffLandingLength + 100, 1f),
                    new Keyframe(spline.Length - 100, 0.9f),
                    new Keyframe(spline.Length, 0.9f)
                );

                guidanceAltitudeProfile = new AnimationCurve(
                    new Keyframe(guidanceDistance * 2, startAltitude),
                    //new Keyframe(takeoffLandingLength + guidanceDistance, altitude - 15),
                    //new Keyframe(spline.Length - takeoffLandingLength + (guidanceDistance * 2), altitude - 15),
                    new Keyframe(takeoffLandingLength + guidanceDistance, altitude),
                    new Keyframe(spline.Length - takeoffLandingLength + (guidanceDistance * 2), altitude),
                    new Keyframe(spline.Length, finalAltitude + 50)
                );

                // Motion Pitch ketika Takeoff
                if (activeMission.takeoff)
                {
                    takeoffPitch = new AnimationCurve(
                        new Keyframe(0, 0),
                        new Keyframe(takeoffLength, 0),
                        new Keyframe(takeoffLength + (takeoffLength / 2), 50),
                        new Keyframe(takeoffLength * 2, 0)
                    );
                }


                // Motion Pitch ketika Landing
                if (activeMission.landing)
                {
                    landingPitch = new AnimationCurve(
                        new Keyframe(spline.Length - landingLength + takeoffLength, 0),
                        new Keyframe(spline.Length - landingLength + (takeoffLength * 2), 70),
                        new Keyframe(spline.Length - takeoffLength - landingLength, 0),
                        new Keyframe(spline.Length - takeoffLength, 60),
                        new Keyframe(spline.Length - takeoffLength + (takeoffLength / 1.5f), 0)
                    );

                    landingCurve = new AnimationCurve(
                        new Keyframe(spline.Length - takeoffLandingLength, 50),
                        new Keyframe(spline.Length - (70f * takeoffLandingLength) / 100, (12f * altitude) / 100),
                        new Keyframe(spline.Length - (25f * takeoffLandingLength) / 100, (10f * altitude) / 100),
                        new Keyframe(spline.Length - (12.5f * takeoffLandingLength) / 100, (2.4f * altitude) / 100),
                        new Keyframe(spline.Length, 0)
                    );

                    landingCurve = new AnimationCurve(new Keyframe(2, 100), new Keyframe(12, 0));
                }
                else
                {
                    landingCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(12, 0));
                }
            }
        }

        private void UpdateAltitudeProfile()
        {
            if (altitudeProfile == null || activeMission == null || activeMission.DATA.Count <= 0) return;

            var segmentOne      = altitudeProfile.keys[1];
            var segmentLastTwo  = altitudeProfile.keys[altitudeProfile.keys.Length - 3];

            if (spline.AbsolutePosition < segmentOne.time || spline.AbsolutePosition > segmentLastTwo.time)
            {
                //var alt = (float) EnvironmentsController.GetMapComponent().GetComponent<HPRoot>().RootUniversePosition.y + groundFitter.transform.position.y;
                var alt = groundFitter.transform.position.y;

                if (objSatuan.kategori == EntityHelper.KategoriSatuan.AIRCRAFT)
                {
                    if (activeMission.takeoff)
                    {
                        var takeoffOne = altitudeProfile.keys[0];
                        takeoffOne.value = alt;
                        altitudeProfile.MoveKey(0, takeoffOne);

                        var takeoffTwo = segmentOne;
                        takeoffTwo.value = alt;
                        altitudeProfile.MoveKey(1, takeoffTwo);
                    }

                    if (activeMission.landing)
                    {
                        var landingTwo = altitudeProfile.keys[altitudeProfile.keys.Length - 2];
                        landingTwo.value = alt;
                        altitudeProfile.MoveKey(altitudeProfile.keys.Length - 2, landingTwo);

                        var landingOne = altitudeProfile.keys[altitudeProfile.keys.Length - 1];
                        landingOne.value = alt;
                        altitudeProfile.MoveKey(altitudeProfile.keys.Length - 1, landingOne);
                    }
                }
                else if(objSatuan.kategori == EntityHelper.KategoriSatuan.HELICOPTER)
                {
                    double diffInSeconds = CURRENT_TIME.Subtract(activeMission.DATA[activeMission.DATA.Count - 1].datetime).TotalSeconds;

                    var ntest = alt + landingCurve.Evaluate((float)diffInSeconds);

                    if (activeMission.takeoff)
                    {
                        var takeoffOne = altitudeProfile.keys[0];
                        takeoffOne.value = alt;
                        altitudeProfile.MoveKey(0, takeoffOne);
                    }

                    if (activeMission.landing)
                    {
                        var landingOne = altitudeProfile.keys[altitudeProfile.keys.Length - 3];
                        landingOne.value = ntest;
                        altitudeProfile.MoveKey(altitudeProfile.keys.Length - 1, landingOne);
                    }

                    if(guidanceAltitudeProfile != null)
                    {
                        if (activeMission.takeoff)
                        {
                            var takeOffGuidanceOne = guidanceAltitudeProfile.keys[0];
                            takeOffGuidanceOne.value = alt;
                            guidanceAltitudeProfile.MoveKey(0, takeOffGuidanceOne);
                        }

                        if (activeMission.landing)
                        {
                            var landingGuidanceTwo = guidanceAltitudeProfile.keys[guidanceAltitudeProfile.keys.Length - 1];
                            landingGuidanceTwo.value = ntest;
                            guidanceAltitudeProfile.MoveKey(guidanceAltitudeProfile.keys.Length - 1, landingGuidanceTwo);
                        }
                    }
                }
            }
        }

        private void UpdateRollAndPitch()
        {
            if (guidance == null) return;

            var currentRot = transform.localRotation.eulerAngles.y;
            var guidanceRot = guidance.transform.localRotation.eulerAngles.y;

            float rollChange = 0;
            if(moving)
            {
                rollChange = ((currentRot - guidanceRot) * -1) * sensitivity;

                if (bundleCore.bankingRate != 0)
                {
                    if (rollChange < -bundleCore.bankingRate)
                    {
                        rollChange = -bundleCore.bankingRate;
                    }
                    else if (rollChange > bundleCore.bankingRate)
                    {
                        rollChange = bundleCore.bankingRate;
                    }
                }
            }
            else
            {
                rollChange = 0;
            }

            var newHeading = Quaternion.Euler(bundleCore.origin.x, bundleCore.origin.y, bundleCore.origin.z + rollChange);

            bundleCore.parent.localRotation = Quaternion.Lerp(bundleCore.parent.localRotation, newHeading, Time.deltaTime * PlaybackController.instance.speedFactor);
        }

        private void UpdateGear()
        {
            if (bundleAircraft == null || altitudeProfile == null) return;

            var splinePos       = spline.AbsolutePosition;

            var takeoffStart    = altitudeProfile.keys[1].time + 500;
            var takeoffEnd      = altitudeProfile.keys[2].time;

            var nodeGear        = bundleAircraft.nodeGear;

            var landingStart    = altitudeProfile.keys[altitudeProfile.keys.Length - 3].time;
            var landingEnd      = altitudeProfile.keys[altitudeProfile.keys.Length - 3].time + (altitudeProfile.keys[altitudeProfile.keys.Length - 2].time - altitudeProfile.keys[altitudeProfile.keys.Length - 3].time);

            var landingFinal    = altitudeProfile.keys[altitudeProfile.keys.Length - 2].time;

            if (splinePos > takeoffStart && splinePos < takeoffEnd && activeMission.takeoff)
            {
                // Enter takeoff state

                if (bundleAircraft.properties.gearDown)
                {
                    for(int i=0; i < nodeGear.Count; i++)
                    {
                        if (nodeGear[i].useCustomAnim)
                        {
                            if (nodeGear[i].support == null || nodeGear[i].support.GetComponent<Animator>() == null) continue;

                            nodeGear[i].support.GetComponent<Animator>().Play(nodeGear[i].customAnimName + "Close");
                        }
                        else
                        {
                            nodeGear[i].support.GetComponent<Animator>().Play(nodeGear[i].presetAnim + "Close");
                        }

                        nodeGear[i].support.GetComponent<Animator>().speed = Mathf.Clamp(PlaybackController.instance.speedFactor / 2, 1, 10);
                    }

                    bundleAircraft.properties.gearDown = false;
                }
            }
            else if (splinePos > landingEnd && splinePos < spline.Length && activeMission.landing)
            {
                // Enter landing state

                if (!bundleAircraft.properties.gearDown)
                {
                    for(int i=0; i < bundleAircraft.nodeGear.Count; i++)
                    {
                        if (nodeGear[i].useCustomAnim)
                        {
                            if (nodeGear[i].support == null || nodeGear[i].support.GetComponent<Animator>() == null) continue;

                            nodeGear[i].support.GetComponent<Animator>().Play(nodeGear[i].customAnimName + "Open");
                        }
                        else
                        {
                            nodeGear[i].support.GetComponent<Animator>().Play(nodeGear[i].presetAnim + "Open");
                        }

                        nodeGear[i].support.GetComponent<Animator>().speed = Mathf.Clamp(PlaybackController.instance.speedFactor / 2, 1, 10);
                    }

                    bundleAircraft.properties.gearDown = true;
                }
            }
            else
            {
                if (splinePos > takeoffStart && splinePos < landingStart && bundleAircraft.properties.gearDown)
                {
                    for (int i = 0; i < nodeGear.Count; i++)
                    {
                        nodeGear[i].support.GetComponent<Animator>().Play("Normal", 0, 0);
                        nodeGear[i].support.GetComponent<Animator>().speed = Mathf.Clamp(PlaybackController.instance.speedFactor / 2, 1, 10);
                    }

                    bundleAircraft.properties.gearDown = false;
                }
                else if (splinePos < takeoffStart && !bundleAircraft.properties.gearDown)
                {
                    for (int i = 0; i < nodeGear.Count; i++)
                    {
                        nodeGear[i].support.GetComponent<Animator>().Play("Normal", 0, 0);
                        nodeGear[i].support.GetComponent<Animator>().speed = Mathf.Clamp(PlaybackController.instance.speedFactor / 2, 1, 10);
                    }

                    bundleAircraft.properties.gearDown = true;
                }
                else if (splinePos > landingEnd && !bundleAircraft.properties.gearDown)
                {
                    for (int i = 0; i < nodeGear.Count; i++)
                    {
                        nodeGear[i].support.GetComponent<Animator>().Play("Normal", 0, 0);
                        nodeGear[i].support.GetComponent<Animator>().speed = Mathf.Clamp(PlaybackController.instance.speedFactor / 2, 1, 10);
                    }

                    bundleAircraft.properties.gearDown = true;
                }
                else if (splinePos > landingStart && bundleAircraft.properties.gearDown)
                {
                    for (int i = 0; i < nodeGear.Count; i++)
                    {
                        nodeGear[i].support.GetComponent<Animator>().Play("Normal", 0, 0);
                        nodeGear[i].support.GetComponent<Animator>().speed = Mathf.Clamp(PlaybackController.instance.speedFactor / 2, 1, 10);
                    }

                    bundleAircraft.properties.gearDown = false;
                }
            }
        }

        private void UpdateSound()
        {
            var walkerSound = entity.GetComponent<WalkerSound>();
            if (walkerSound == null || soundCurve == null) return;

            walkerSound.pitch = soundCurve.Evaluate(spline.AbsolutePosition);
        }

        private void UpdateVFX()
        {
            var vfx = entity.GetComponent<NodeVFX>();
            if (vfx == null || vfx.nodes == null) return;

            for(int i=0; i < vfx.nodes.Count; i++)
            {
                if (vfx.nodes[i].node == null || vfx.nodes[i].node.childCount <= 0) continue;

                var node = vfx.nodes[i];
                if(node.objVFX != null & node.node.GetChild(0).gameObject.activeSelf != moving)
                {
                    node.node.GetChild(0).gameObject.SetActive(moving);
                    continue;
                }

                if(node.node.GetChild(0).gameObject.activeSelf != moving)
                {
                    node.node.GetChild(0).gameObject.SetActive(moving);
                }
            }
        }
    }
}
