using Esri.ArcGISMapsSDK.Components;
using Esri.GameEngine.Geometry;
using Esri.HPFramework;
using FIMSpace.GroundFitter;
using UnityEngine;
using Wargaming.Components.Colyseus;
using Wargaming.Components.Playback;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;
using WGSBundling.Bundles.Vehicle;
using WGSBundling.Bundles.WheeledVehicle;
using WGSBundling.Core;

namespace Wargaming.Components.Walker
{
    public class LRootWalker : RootWalkerBase
    {
        private WGSBundleCore bundleCore;
        private NodeVehicle bundleVehicle;
        private PasukanAnim pasukanAnim;

        private ObjekSatuan objSatuan;

        private AnimationCurve soundCurve;

        private FGroundFitter groundFitter;
        //public Vector3 velocity;
        //public Vector3 prevPos;
        //public float fwdDotProduct;

        //public float hh;

        public float velocity = 0;
        public Vector3 lastPos;
        //public bool sync = true;

        public void Init()
        {
            InitBase();

            bundleCore      = entity.GetComponent<WGSBundleCore>();
            bundleVehicle   = entity.GetComponent<NodeVehicle>();
            pasukanAnim     = entity.GetComponent<PasukanAnim>();

            objSatuan       = entity.GetComponent<ObjekSatuan>();
            groundFitter    = entity.GetComponent<FGroundFitter>();

            player = PlayerData.PLAYERS.Find(x => x.id == objSatuan.userID);
        }

        private void Update()
        {
            if (spline == null || missions == null || missions.Count <= 0 || WargamingCam.instance.onRebase) return;
            if (!IsOnPlay()) return;

            GetTiming(objSatuan);
            GetActiveMission();

            //if (activeMission == null) return;

            GetActiveMissionNode();

            if (CheckSplineWithActiveMission(spline))
            {
                GenerateMotionProfile();
            }

            if (moving)
            {
                groundFitter.UpAxisRotation = transform.rotation.eulerAngles.y;
            }

            //groundFitter.sst = transform.rotation.eulerAngles.y;

            //var alt = (float) EnvironmentsController.GetMapComponentRoot().RootUniversePosition.y + entity.transform.position.y;

            UpdateMovement(bundleCore);
            //UpdateGroundFitter(groundFitter);

            var bundleParentRot = bundleCore.parent.transform.rotation;

            //bundleCore.parent.transform.rotation = Quaternion.Euler(bundleParentRot.eulerAngles.x + bundleCore.origin.x, bundleParentRot.eulerAngles.y + bundleCore.origin.y, bundleParentRot.eulerAngles.z + bundleCore.parent.eulerAngles.z);
            //UpdateEntityPosition(entity, alt);

            //var distToCam = Vector3.Distance(WargamingCam.instance.transform.position, entity.transform.position);
            //if (distToCam > 5000) return;

            UpdateWheelMovement();
            UpdateWalking();
            UpdateSound();
        }

        private void LateUpdate()
        {
            UpdateCurrentAltitude();

            if (!moving && activeMission == null)
            {
                if (groundFitter != null)
                {
                    groundFitter.UpAxisRotation = (float)startRot.Heading;
                }

                var entityLocation = entity.GetComponent<ArcGISLocationComponent>();

                entityLocation.Position = new ArcGISPoint(startPos.X, startPos.Y, (currentAltitude != -99) ? currentAltitude : startPos.Z, startPos.SpatialReference);
            }

            //if (spline == null || missions == null || missions.Count <= 0 || WargamingCam.instance.onRebase) return;
            //if (!IsOnPlay()) return;

            //currentAltitude = (Double) GetComponent<ArcGISLocationComponent>().Position.Z;

            //entity.transform.rotation = Quaternion.Euler(entity.transform.rotation.x, transform.rotation.y, entity.transform.rotation.z);
        }

        private void UpdateWheelMovement()
        {
            if (bundleVehicle == null) return;

            if(lastPos == null) { lastPos = entity.transform.position; }

            velocity = Vector3.Distance(entity.transform.position, lastPos) / Time.deltaTime * PlaybackController.instance.speedFactor;
            lastPos = entity.transform.position;

            if (bundleVehicle.type == NodeVehicle.VehicleType.Wheeled)
            {
                for (int i = 0; i < bundleVehicle.nodeWheel.Count; i++)
                {
                    if (bundleVehicle.nodeWheel[i].node == null) continue;

                    bundleVehicle.nodeWheel[i].node.transform.Rotate(Vector3.left, velocity / bundleVehicle.nodeWheel[i].radius);
                }
            }else if(bundleVehicle.type == NodeVehicle.VehicleType.Tracked)
            {
                var leftTrackGroup = bundleVehicle.nodeTrackLeft.trackWheel;
                for(int i=0; i < leftTrackGroup.Count; i++)
                {
                    if (leftTrackGroup[i].node == null) continue;

                    leftTrackGroup[i].node.transform.Rotate(Vector3.left, velocity / leftTrackGroup[i].radius);
                }

                var rightTrackGroup = bundleVehicle.nodeTrackRight.trackWheel;
                for (int i = 0; i < rightTrackGroup.Count; i++)
                {
                    if (rightTrackGroup[i].node == null) continue;

                    rightTrackGroup[i].node.transform.Rotate(Vector3.left, velocity / rightTrackGroup[i].radius);
                }
            }
        }

        private void UpdateWalking()
        {
            if (pasukanAnim == null) return;

            if(moving != pasukanAnim.IsMoving)
            {
                if (moving)
                {
                    pasukanAnim.PlayMoveAnimation();
                }
                else
                {
                    pasukanAnim.StopMoveAnimation();
                }
            }

            if (moving)
            {
                if (pasukanAnim.animSpeed != Mathf.Clamp(PlaybackController.instance.speedFactor * (float)activeMission.speed, 0, 25))
                {
                    pasukanAnim.SetSpeed(Mathf.Clamp(PlaybackController.instance.speedFactor * (float)activeMission.speed, 0, 25));
                }
            }
            else
            {
                if(pasukanAnim.animSpeed != 0)
                {
                    pasukanAnim.SetSpeed(0);
                }
            }
        }

        private void GenerateMotionProfile()
        {
            entity.GetComponent<WalkerSound>().movingCurve = new AnimationCurve(
                    new Keyframe(0, 0),
                    new Keyframe(10, 1f),
                    new Keyframe(spline.Length - 10, 1),
                    new Keyframe(spline.Length, 0)
                );

            soundCurve = new AnimationCurve(
                    new Keyframe(0, 1),
                    new Keyframe(100, 1.5f),
                    new Keyframe(spline.Length - 100, 1.5f),
                    new Keyframe(spline.Length, 1)
                );
        }

        private void UpdateSound()
        {
            var walkerSound = entity.GetComponent<WalkerSound>();
            if (walkerSound == null || soundCurve == null) return;

            walkerSound.pitch = soundCurve.Evaluate(spline.AbsolutePosition);
        }
    }
}