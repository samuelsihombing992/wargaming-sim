using System;
using System.Collections.Generic;

using UnityEngine;
using Unity.Mathematics;

using Esri.HPFramework;

using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;

using FIMSpace.GroundFitter;

using Wargaming.Components.Colyseus;
using Wargaming.Components.Playback;
using Wargaming.Core;
using Wargaming.Core.GlobalParam;
using static UnityEngine.EventSystems.EventTrigger;
using static Wargaming.Core.GlobalParam.GlobalHelper;
using static UnityEngine.GraphicsBuffer;
using UnityEngine.Analytics;
using WGSBundling.Core;
using SickscoreGames.HUDNavigationSystem;
using static Unity.Burst.Intrinsics.X86;
using Esri.ArcGISMapsSDK.Utils.GeoCoord;
using Esri.GameEngine.Geometry;
using Esri.ArcGISMapsSDK.Components;
using Wargaming.Components.Walker;

public class RootWalkerBase : MonoBehaviour
{
    [Header("Entity")]
    public GameObject entity;
    public PData player;

    public SplineController spline;

    [Header("Data Misi")]
    public List<RootMissionWalker> missions = new List<RootMissionWalker>();
    public RootMissionWalker activeMission;

    public float DistanceToUpdate = 5000;

    public HPTransform rootHP;

    [NonSerialized] public ArcGISPoint startPos;
    [NonSerialized] public ArcGISRotation startRot;

    [NonSerialized] public DateTime CURRENT_TIME;
    private DateTime lastTiming;

    public bool moving = false;
    public double progress = 0;

    public double currentAltitude = -99;

    public void InitStartCoordinate()
    {
        if(GetComponent<ArcGISLocationComponent>() != null)
        {
            startPos = GetComponent<ArcGISLocationComponent>().Position;
            startRot = new ArcGISRotation(GetComponent<ArcGISLocationComponent>().Rotation.Heading, 90, 0);
        }
    }

    public void InitBase()
    {
        InitStartCoordinate();

        spline = GetComponent<SplineController>();
        rootHP = GetComponent<HPTransform>();
    }

    //private void LateUpdate()
    //{
    //    if (!moving || activeMission == null)
    //    {
    //        activeMission = null;
    //        spline.Spline = null;

    //        var entityLocation = entity.GetComponent<ArcGISLocationComponent>();

    //        entityLocation.Position = new ArcGISPoint(startPos.X, startPos.Y, (currentAltitude != -99) ? currentAltitude : startPos.Z, startPos.SpatialReference);
    //        entityLocation.Rotation = startRot;
    //    }
    //}

    public void GetTiming(ObjekSatuan objSatuan)
    {
        if (ColyseusTFGSingleController.instance != null)
        {
            // CURRENT_TIME = objSatuan.player.timeMovement.GetValueOrDefault();
            if (player == null) return;

            CURRENT_TIME = player.timeMovement.GetValueOrDefault();
        }
        else
        {
            CURRENT_TIME = PlaybackController.CURRENT_TIME;
        }
    }

    public bool IsOnPlay()
    {
        if(ColyseusTFGSingleController.instance != null)
        {
            var timing = player.timeMovement.GetValueOrDefault();
            if (lastTiming != timing)
            {
                lastTiming = timing;
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return PlaybackController.IsPlaying();
        }
    }

    public void GetActiveMission()
    {
        if (missions == null) return;

        bool onMission = false;
        for (int i = (missions.Count - 1); i >= 0; i--)
        {
            if (missions[i].DATA.Count < 2) continue;
            if (CURRENT_TIME < missions[i].DATA[0].datetime) continue;

            activeMission = missions[i];
            onMission = true;
            break;
        }

        if (!onMission)
        {
            moving = false;
            activeMission = null;
            spline.Spline = null;

           

            //var entityLocation = entity.GetComponent<ArcGISLocationComponent>();

            //entityLocation.Position = new ArcGISPoint(startPos.X, startPos.Y, (currentAltitude != -99) ? currentAltitude : startPos.Z, startPos.SpatialReference);

            //if (GetComponent<SRootWalker>() != null)
            //{
            //    entityLocation.Rotation = startRot;
            //}
        }
    }

    public RootMissionWalker GetActiveMission(DateTime time)
    {
        if (missions == null) return null;
        for (int i = (missions.Count - 1); i >= 0; i--)
        {
            if (missions[i].DATA.Count < 2) continue;
            if (time < missions[i].DATA[0].datetime) continue;

            return missions[i];
        }

        return missions[0];
    }

    //public void GetActiveMission()
    //{
    //    //for (int i = (missions.Count - 1); i >= 0; i--)
    //    //{
    //    //    if (missions[i].DATA.Count <= 0) continue;

    //    //    if (CURRENT_TIME >= missions[i].DATA[0].datetime)
    //    //    {
    //    //        activeMission = missions[i];
    //    //        break;
    //    //    }
    //    //}

    //    //activeMission = missions[0];

    //    if(missions == null || missions.Count <= 0)
    //    {
    //        activeMission = null;
    //        return;
    //    }

    //    var firstMission = missions[0];
    //    var lastMission = missions[missions.Count - 1];

    //    if (firstMission.DATA == null || firstMission.DATA.Count <= 0 || lastMission.DATA == null || lastMission.DATA.Count <= 0)
    //    {
    //        activeMission = null;
    //        return;
    //    }

    //    if(CURRENT_TIME < firstMission.DATA[0].datetime)
    //    {
    //        activeMission = firstMission;
    //        return;
    //    }else if(CURRENT_TIME > lastMission.DATA[lastMission.DATA.Count - 1].datetime)
    //    {
    //        activeMission = lastMission;
    //        return;
    //    }

    //    for(int i = 0; i < missions.Count - 1; i++)
    //    {
    //        var selectedMission = missions[i];

    //        if (selectedMission.DATA == null || selectedMission.DATA.Count <= 0) continue;

    //        if(CURRENT_TIME >= selectedMission.DATA[0].datetime && CURRENT_TIME <= selectedMission.DATA[selectedMission.DATA.Count - 1].datetime)
    //        {
    //            activeMission = selectedMission;
    //            break;
    //        }
    //    }
    //}

    public MissionWalkerParam GetActiveMissionNode()
    {
        if (activeMission == null) return null;
        if (activeMission.DATA.Count <= 0) return null;

        if (CURRENT_TIME < activeMission.GetMissionTime(0))
        {
            progress = 0;
            moving = false;

            return activeMission.GetFirstNode();
        }else if(CURRENT_TIME > activeMission.GetLastNode().datetime){
            progress = 1;
            moving = false;

            return activeMission.GetLastNode();
        }
        else
        {
            moving = true;

            for (int i=0; i < activeMission.DATA.Count - 2; i++)
            {
                var nodeTime        = activeMission.GetMissionTime(i);
                var nextNodeTime    = activeMission.GetMissionTime(i + 1);

                //Debug.Log(nodeTime.ToString("HH:mm:ss") + " | " + CURRENT_TIME.ToString("HH:mm:ss") + " | " + nextNodeTime.ToString("HH:mm:ss"));

                if (CURRENT_TIME >= nodeTime && CURRENT_TIME < nextNodeTime)
                {
                    // IF Waktu Playback sedang berada di antara 2 titik node
                    return activeMission.DATA[i];
                }
            }

            return activeMission.GetLastNode(0);
        }
    }

    public float? GetActiveMissionNode(RootMissionWalker mission, DateTime time)
    {
        if (mission.DATA.Count <= 0) return null;

        //Debug.Log(time.ToString("HH:mm:ss") + " | " + mission.GetMissionTime(0).ToString("HH:mm:ss") + " | " + mission.GetLastNode().datetime);

        if(time < mission.GetMissionTime(0))
        {
            return 0;
        }else if(time > mission.GetLastNode().datetime)
        {
            return 1;
        }
        else
        {
            var startDate       = mission.GetFirstNode().datetime;
            var endDate         = mission.GetLastNode().datetime;
            var totalTime       = (endDate - startDate).TotalSeconds;

            var tempProgress    = ((time.Subtract(startDate).TotalSeconds) / totalTime);

            return (float) (mission.GetComponent<SplineController>().Spline.Length * (tempProgress * 100) / 100);
        }
    }

    public bool CheckSplineWithActiveMission(SplineController _spline)
    {
        if (activeMission == null) return false;

        var activeMissionSpline = activeMission.GetComponent<CurvySpline>();
        if (_spline.Spline != activeMissionSpline)
        {
            _spline.Spline = activeMissionSpline;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool UpdateMovement(WGSBundleCore core = null)
    {
        if (activeMission == null || activeMission.DATA == null || activeMission.DATA.Count < 2)
        {
            if (core == null || core.parent == null) return false;

            var entityParent = core.parent.gameObject;

            if(missions == null || missions.Count <= 0)
            {
                if (!entityParent.activeSelf)
                {
                    entityParent.SetActive(true);
                }
            }
            else
            {
                if ((missions[0].jenis == RootMissionWalker.RouteType.Debarkasi || missions[0].jenis == RootMissionWalker.RouteType.Linud || missions[0].jenis == RootMissionWalker.RouteType.CDS) && entityParent.activeSelf)
                {
                    entityParent.SetActive(false);
                }
            }

            return false;
        }

        if(!moving && progress == 0)
        {
            spline.RelativePosition = 0;

            if (core == null || core.parent == null) return false;

            var entityParent = core.parent.gameObject;

            /// Jika Satuan Sedang Berhenti di Titik Awal Misi & Satuan Memiliki Jenis Misi Debarkasi - Set Default Menjadi Hidden
            if (activeMission.jenis == RootMissionWalker.RouteType.Debarkasi || activeMission.jenis == RootMissionWalker.RouteType.Linud || activeMission.jenis == RootMissionWalker.RouteType.CDS)
            {
                if (entityParent.activeSelf)
                {
                    entityParent.SetActive(false);
                }
            }else if (!entityParent.activeSelf)
            {
                entityParent.SetActive(true);
            }

            return false;
        }
        else if(!moving && progress == 1)
        {
            spline.RelativePosition = 1;

            if (core == null || core.parent == null) return false;

            var entityParent = core.parent.gameObject;

            /// Jika Satuan Sedang Berhenti di Titik Akhir Misi & Satuan Memiliki Jenis Misi Embarkasi - Set Default Menjadi Hidden
            if (activeMission.jenis == RootMissionWalker.RouteType.Embarkasi || activeMission.jenis == RootMissionWalker.RouteType.Linud || activeMission.jenis == RootMissionWalker.RouteType.CDS)
            {
                entityParent.SetActive(false);
            }
            else if (!entityParent.activeSelf)
            {
                entityParent.SetActive(true);
            }

            return false;
        }
        else
        {
            if (!spline.enabled) { spline.enabled = true; }

            if(core != null && core.parent != null && !core.parent.gameObject.activeSelf)
            {
                core.parent.gameObject.SetActive(true);
            }

            var startDate               = activeMission.GetFirstNode().datetime;
            var endDate                 = activeMission.GetLastNode().datetime;
            var totalTime               = (endDate - startDate).TotalSeconds;

            progress                    = ((CURRENT_TIME.Subtract(startDate).TotalSeconds) / totalTime);

            spline.AbsolutePosition     = (float)(spline.Length * (progress * 100) / 100);

            return true;
        }
    }

    //public bool UpdateMovement(WGSBundleCore core = null)
    //{
    //    if (activeMission == null) return false;

    //    if(!moving && progress == 0)
    //    {
    //        spline.RelativePosition = 0;

    //        if(core != null && core.parent != null)
    //        {
    //            if(activeMission.jenis == RootMissionWalker.RouteType.Debarkasi)
    //            {
    //                if (core.parent.gameObject.activeSelf) {
    //                    core.parent.gameObject.SetActive(false);
    //                }
    //            }
    //            else
    //            {
    //                if (!core.parent.gameObject.activeSelf)
    //                {
    //                    core.parent.gameObject.SetActive(true);
    //                }
    //            }
    //        }
    //        return false;
    //    }
    //    else if(!moving && progress == 1)
    //    {
    //        spline.RelativePosition = 1;
    //        if (core != null && core.parent != null)
    //        {
    //            if(activeMission.jenis == RootMissionWalker.RouteType.Embarkasi)
    //            {
    //                if (core.parent.gameObject.activeSelf)
    //                {
    //                    core.parent.gameObject.SetActive(false);
    //                }
    //            }
    //            else
    //            {
    //                if (!core.parent.gameObject.activeSelf)
    //                {
    //                    core.parent.gameObject.SetActive(true);
    //                }
    //            }
    //        }
    //        return false;
    //    }
    //    else
    //    {
    //        if (!spline.enabled) { spline.enabled = true; }
    //        if(core != null)
    //        {
    //            if (!core.parent.gameObject.activeSelf)
    //            {
    //                core.parent.gameObject.SetActive(true);
    //            }
    //        }

    //        if(activeMission.DATA == null || activeMission.DATA.Count <= 0)
    //        {
    //            return false;
    //        }

    //        var startDate       = activeMission.GetFirstNode().datetime;
    //        var endDate         = activeMission.GetLastNode().datetime;
    //        var totalTime       = (endDate - startDate).TotalSeconds;

    //        progress            = ((CURRENT_TIME.Subtract(startDate).TotalSeconds) / totalTime);

    //        spline.AbsolutePosition = (float) (spline.Length * (progress * 100) / 100);

    //        return true;
    //    }
    //}

    public bool UpdateGuidanceMovement(SplineController guidance, float dist, ObjekSatuan objSatuan)
    {
        if (guidance.Spline == null || objSatuan == null || activeMission == null) return false;

        if (!moving && progress == 0)
        {
            if(objSatuan.kategori == EntityHelper.KategoriSatuan.HELICOPTER && activeMission.takeoff)
            {
                guidance.AbsolutePosition = Mathf.Clamp(spline.AbsolutePosition, 250f, spline.Length);
            }
            else
            {
                guidance.AbsolutePosition = Mathf.Clamp(spline.AbsolutePosition, 0, spline.Length);
            }

            return false;
        }
        else if (!moving && progress == 1)
        {
            guidance.AbsolutePosition = spline.AbsolutePosition;
            return false;
        }
        else
        {
            if(objSatuan.kategori == EntityHelper.KategoriSatuan.HELICOPTER && activeMission.takeoff)
            {
                guidance.AbsolutePosition = Mathf.Clamp(spline.AbsolutePosition + dist, dist, spline.Length);
            }
            else
            {
                guidance.AbsolutePosition = Mathf.Clamp(spline.AbsolutePosition + dist, 0, spline.Length);
            }

            return true;
        }
    }

    public void UpdateAltitude(float altitude)
    {
        transform.position = new Vector3(transform.position.x, altitude, transform.position.z);
    }

    public void UpdateCurrentAltitude()
    {
        currentAltitude = GetComponent<ArcGISLocationComponent>().Position.Z;
    }

    public void UpdateGroundFitter(FGroundFitter groundFitter)
    {
        if (WargamingCam.instance.onRebase) return;
        if (groundFitter == null) return;


        var hpTransform = groundFitter.GetComponent<HPTransform>();
        if (hpTransform == null) return;

        hpTransform.UniversePosition = new double3(rootHP.UniversePosition.x, hpTransform.UniversePosition.y, rootHP.UniversePosition.z);
        currentAltitude = hpTransform.UniversePosition.y;
    }

    public void UpdateEntityPosition(GameObject entity, float? altitude = null, Transform guidance = null, float? pitchLimit = null, bool syncRotation = false, bool horizontalFix = false)
    {
        var hpTransform = entity.GetComponent<HPTransform>();
        if (hpTransform == null) return;

        if (altitude.HasValue)
        {
            hpTransform.UniversePosition = new double3(
                rootHP.UniversePosition.x,
                altitude.GetValueOrDefault(0),
                rootHP.UniversePosition.z
            );
        }
        else
        {
            hpTransform.UniversePosition = rootHP.UniversePosition;
        }
                
        if(guidance != null)
        {
            //var entityRot = Quaternion.LookRotation(guidance.position);
            transform.LookAt(guidance);

            //if (pitchLimit.HasValue)
            //{
            //    if(guidance.position - transform.position != Vector3.zero)
            //    {
            //        var entityRot = Quaternion.LookRotation(guidance.position - transform.position);
            //        var xPos = entityRot.eulerAngles.x;

            //        if(xPos < pitchLimit.Value * -1)
            //        {
            //            xPos = pitchLimit.Value * -1;
            //        }else if(xPos > pitchLimit.Value)
            //        {
            //            xPos = pitchLimit.Value;
            //        }

            //        entityRot.eulerAngles = new Vector3(
            //            xPos,
            //            entityRot.eulerAngles.y,
            //            entityRot.eulerAngles.z
            //        );

            //        transform.rotation = Quaternion.Slerp(transform.rotation, entityRot, Time.deltaTime * PlaybackController.instance.speedFactor);
            //    }
            //    else
            //    {
            //        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.identity, Time.deltaTime * PlaybackController.instance.speedFactor);
            //    }
            //}
            //else
            //{
            //    transform.LookAt(guidance);
            //}

            if (!horizontalFix)
            {
                hpTransform.UniverseRotation = rootHP.UniverseRotation;
            }
            else
            {
                hpTransform.LocalRotation = new quaternion(rootHP.LocalRotation.value.x, 0, rootHP.LocalRotation.value.z, rootHP.LocalRotation.value.w);
            }

            //transform.rotation = entityRot;
        }
        else
        {
            if (syncRotation)
            {
                hpTransform.UniverseRotation = rootHP.UniverseRotation;
            }
        }

        //entity.transform.rotation = rootHP.UniverseRotation;


        //hpTransform.UniverseRotation = rootHP.UniverseRotation;
    }

    public void UpdateGuidancePosition(Transform guidance, float? altitude = null, GameObject entity = null)
    {
        if (entity == null)
        {
            guidance.transform.position = new Vector3(guidance.position.x, altitude.GetValueOrDefault(guidance.position.y), guidance.position.z);

        }
        else
        {
            guidance.transform.position = new Vector3(guidance.position.x, altitude.GetValueOrDefault(guidance.position.y), guidance.position.z) + this.transform.forward * 4;
        }
    }

    public bool OnUpdateDistance(GameObject entity)
    {
       if(Vector3.Distance(WargamingCam.instance.transform.position, entity.transform.position) > 5000)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}

public static class RootWalkerHelper
{
    public static DateTime GetMissionTime(this RootMissionWalker _mission, int index)
    {
        return _mission.DATA[index].datetime;
    }

    public static MissionWalkerParam GetFirstNode(this RootMissionWalker _mission, int offset = 0)
    {
        if (_mission.DATA.Count <= 0) return null;
        return _mission.DATA[0 + offset];
    }

    public static MissionWalkerParam GetLastNode(this RootMissionWalker _mission, int offset = 0)
    {
        if (_mission.DATA.Count <= 0) return null;
        return _mission.DATA[_mission.DATA.Count - 1 - offset];
    }
}