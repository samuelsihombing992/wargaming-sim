using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wargaming.Components.Colyseus;
using Wargaming.Components.Playback;
using Wargaming.Core.GlobalParam;

public class TimesetWalker : MonoBehaviour
{
    [Header("Entity")]
    public PData player;
    public GameObject obj;

    public DateTime? startDate;
    public DateTime? endDate;

    public string sDate;
    public string eDate;


    private DateTime CURRENT_TIME;

    public void Init(GameObject _obj, DateTime _startDate, DateTime _endDate)
    {
        obj         = _obj;
        startDate   = _startDate;
        endDate     = _endDate;

        sDate = startDate.ToString();
        eDate = endDate.ToString();
    }

    private void Update()
    {
        if (obj == null || !startDate.HasValue || !endDate.HasValue || endDate.Value <= startDate.Value) return;

        GetTiming();
        if(CURRENT_TIME >= startDate && CURRENT_TIME <= endDate)
        {
            if (!obj.activeSelf) { obj.SetActive(true); }
            return;
        }

        if (obj.activeSelf)
        {
            obj.SetActive(false);
        }
    }

    public void GetTiming()
    {
        if (ColyseusTFGSingleController.instance != null)
        {
            if (player == null) return;

            CURRENT_TIME = player.timeMovement.GetValueOrDefault();
        }
        else
        {
            CURRENT_TIME = PlaybackController.CURRENT_TIME;
        }
    }
}
